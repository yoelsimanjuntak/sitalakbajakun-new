<?php
$numStatToday = $this->db->where('DATE_FORMAT(Timestamp, "%Y-%m-%d")=', date('Y-m-d'))->count_all_results(TBL__LOGS);
$numStatMonthly = $this->db->where('DATE_FORMAT(Timestamp, "%Y-%m")=', date('Y-m'))->count_all_results(TBL__LOGS);
$numStatAnnualy = $this->db->where('DATE_FORMAT(Timestamp, "%Y")=', date('Y'))->count_all_results(TBL__LOGS);
$numStatTotal = $this->db->count_all_results(TBL__LOGS);
?>
<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="x-ua-compatible" content="ie=edge">

    <title><?=!empty($title) ? $title.' | '.$this->setting_web_name : $this->setting_web_name?></title>

    <!-- Font Awesome Icons -->
    <link rel="stylesheet" href="<?=base_url()?>assets/tbs/css/font-awesome.min.css" />
    <link rel="stylesheet" href="<?=base_url()?>assets/tbs/fontawesome-pro/web/css/all.min.css" />

    <!-- Theme style -->
    <link rel="stylesheet" href="<?=base_url()?>assets/themes/adminlte-new/dist/css/adminlte.min.css">

    <!-- Ionicons -->
    <link href="<?=base_url()?>assets/tbs/css/ionicons.min.css" rel="stylesheet" type="text/css" />
    <link rel="icon" type="image/png" href="<?=MY_IMAGEURL.$this->setting_web_logo?>" />

    <!-- Select 2 -->
    <link rel="stylesheet" href="<?=base_url()?>assets/themes/adminlte-new/plugins/select2/css/select2.min.css">
    <link rel="stylesheet" href="<?=base_url()?>assets/themes/adminlte-new/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css">

    <!-- JQUERY -->
    <script src="<?=base_url()?>assets/themes/adminlte-new/plugins/jQuery/jquery-2.2.3.min.js"></script>
    <script src="<?=base_url()?>assets/themes/adminlte-new/plugins/modernizr/modernizr.js"></script>

    <script src="<?=base_url()?>assets/themes/adminlte-new/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="<?=base_url()?>assets/themes/adminlte-new/dist/js/adminlte.js"></script>

    <script src="<?=base_url()?>assets/themes/adminlte-new/plugins/select2/js/select2.full.min.js"></script>

    <!-- DataTables -->
    <link rel="stylesheet" href="<?= base_url() ?>assets/datatable/media/css/dataTables.bootstrap.min.css">
    <link rel="stylesheet" href="<?=base_url()?>assets/themes/adminlte-new/plugins/datatables-bs4/css/dataTables.bootstrap4.css">

    <script type="text/javascript" src="<?=base_url()?>assets/datatable/media/js/jquery.dataTables.min.js?ver=1"></script>
    <script type="text/javascript" src="<?=base_url()?>assets/datatable/media/js/dataTables.bootstrap.min.js"></script>
    <script src="<?=base_url()?>assets/themes/adminlte-new/plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>


    <!-- datatable reorder _ buttons ext + resp + print -->
    <script type="text/javascript" src="<?=base_url()?>assets/datatable/media/js/ColReorderWithResize.js"></script>
    <script type="text/javascript" src="<?=base_url()?>assets/datatable/ext/buttons/dataTables.buttons.min.js"></script>
    <script type="text/javascript" src="<?=base_url()?>assets/datatable/ext/buttons/buttons.bootstrap.min.js"></script>
    <script type="text/javascript" src="<?=base_url()?>assets/datatable/ext/buttons/buttons.print.min.js"></script>
    <script type="text/javascript" src="<?=base_url()?>assets/datatable/ext/buttons/buttons.print.min.js"></script>
    <link href="<?=base_url()?>assets/datatable/ext/buttons/buttons.bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="<?=base_url()?>assets/datatable/ext/responsive/css/responsive.bootstrap.min.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="<?=base_url()?>assets/datatable/ext/jszip/jszip.min.js"></script>
    <script type="text/javascript" src="<?=base_url()?>assets/datatable/ext/pdfmake/build/pdfmake.min.js"></script>
    <script type="text/javascript" src="<?=base_url()?>assets/datatable/ext/pdfmake/build/vfs_fonts.js"></script>
    <script type="text/javascript" src="<?=base_url()?>assets/datatable/ext/responsive/js/dataTables.responsive.min.js"></script>
    <script type="text/javascript" src="<?=base_url()?>assets/datatable/ext/buttons/buttons.html5.min.js"></script>

    <!-- Toastr -->
    <link rel="stylesheet" href="<?=base_url()?>assets/themes/adminlte-new/plugins/toastr/toastr.min.css">
    <script src="<?=base_url()?>assets/themes/adminlte-new/plugins/toastr/toastr.min.js"></script>

    <!-- daterange picker -->
    <link rel="stylesheet" href="<?=base_url()?>assets/themes/adminlte-new/plugins/daterangepicker/daterangepicker.css">

    <!-- my css -->
    <!--<link rel="stylesheet" href="<?=base_url()?>assets/css/styles.css">
    <link rel="stylesheet" href="<?=base_url()?>assets/css/my.css">-->

    <!-- Block UI -->
    <script type="text/javascript" src="<?=base_url() ?>assets/js/jquery.blockUI.js"></script>
    <script type="text/javascript" src="<?=base_url()?>assets/template/js/jquery.validate.min.js"></script>
    <script type="text/javascript" src="<?=base_url()?>assets/template/js/function.js"></script>
    <script type="text/javascript" src="<?=base_url()?>assets/template/js/jquery.form.js"></script>

    <script>
        $(document).ready(function() {
          $("select").not('.no-select2, .custom-select').select2({ width: 'resolve', theme: 'bootstrap4' });

          toastr.options = {
            "closeButton": false,
            "debug": false,
            "newestOnTop": false,
            "progressBar": false,
            "positionClass": "toast-top-right",
            "preventDuplicates": false,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
          };

            $('.btn-login', $('.login-section')).click(function() {
                var form = $('#login-form');
                $(form).ajaxSubmit({
                    dataType: 'json',
                    type : 'post',
                    success : function(data){
                        $(form).find('btn').attr('disabled',false);
                        if(data.error != 0){
                            toastr.error(data.error);
                        }else{
                            location.reload();
                        }
                    },
                    error : function(a,b,c){
                        toastr.error('Response Error');
                    }
                });
            });
        });
    </script>
    <style>
    @media (min-width: 576px) {
      #footer-section::after {
        background: rgba(0, 0, 0, 0) url(<?=MY_IMAGEURL?>bglogin.png) no-repeat scroll center center / 100% auto;
        content: "";
        height: 100%;
        left: 0;
        opacity: 0.1;
        position: absolute;
        top: 0;
        width: 100%;
        z-index: -1;
      }
    }
    #footer-section table td {
      border-top: none !important;
    }
    .nowrap {
      white-space: nowrap !important;
    }
    ul.pagination {
      margin-top: 20px !important;
    }
    .break {
      flex-basis: 100%;
      height: 0;
    }
    td.dt-body-right {
      text-align: right !important;
    }
    td.dt-body-center {
      text-align: center; !important;
    }
    </style>
</head>
<body class="hold-transition layout-top-nav layout-navbar-fixed">
<div class="wrapper">

    <!-- Navbar -->
    <nav class="main-header navbar navbar-expand navbar-light navbar-white">
      <div class="container">
          <a href="<?=site_url()?>" class="navbar-brand">
            <img src="<?=MY_IMAGEURL.$this->setting_web_logo?>" alt="Logo" class="brand-image elevation" style="opacity: .8">
            <span class="brand-text font-weight-light d-none d-sm-inline-block"><?=$this->setting_web_name?></span>
          </a>
          <ul class="navbar-nav ml-auto">
            <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" aria-expanded="true">
                INFORMASI PUBLIK
              </a>
              <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
                <a href="#" class="dropdown-item">
                  Hasil Evaluasi AKIP
                  <span class="float-right text-muted text-sm"><i class="fas fa-chevron-right"></i></span>
                </a>
                <a href="<?=site_url('site/home/info-jabatan')?>" class="dropdown-item">
                  Informasi Jabatan
                  <span class="float-right text-muted text-sm"><i class="fas fa-chevron-right"></i></span>
                </a>
              </div>
            </li>
            <!--<li class="nav-item dropdown">
              <a id="dropdownSubMenu1" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="nav-link dropdown-toggle">INFORMASI PUBLIK</a>
              <ul aria-labelledby="dropdownSubMenu1" class="dropdown-menu border-0 shadow">
                <li><a href="#" class="dropdown-item">Hasil Evaluasi AKIP</a></li>
                <li><a href="<?=site_url('site/home/info-jabatan')?>" class="dropdown-item">Informasi Jabatan</a></li>
              </ul>
            </li>-->
            <li class="nav-item d-none d-sm-inline-block">
              <a href="#" class="nav-link">FAQ</a>
            </li>
            <li class="nav-item d-none d-sm-inline-block">
              <a href="#" class="nav-link">KONTAK</a>
            </li>
          </ul>
      </div>
    </nav>
    <div class="content-wrapper">
      <?=$content?>
    </div>
    <div id="footer-section" style="position: relative; z-index:9; border-top: 1px solid #dee2e6">
      <div class="content" style="padding: 0 .5rem">
        <div class="container pt-3">
          <div class="row">
            <div class="col-lg-5 pr-3 pl-3">
              <p class="d-flex align-items-center">
                <img src="<?=MY_IMAGEURL.$this->setting_web_logo?>" class="mr-2" style="height: 80px" />
                <img src="<?=MY_IMAGEURL.'img-berakhlak.png'?>" class="mr-2" style="height: 50px" />
                <img src="<?=MY_IMAGEURL.'img-bangga-melayani-bangsa.png'?>" style="height: 50px" />
              </p>
              <p style="text-align: justify">
                <b><?=$this->setting_org_name?></b><br  />
                <?=$this->setting_org_address?>
              </p>
              <p style="text-align: justify">
                Telp: <?=$this->setting_org_phone?>, Fax: <?=$this->setting_org_fax?><br  />
                Email: <?=$this->setting_org_mail?>
              </p>

            </div>
            <!--<div class="col-lg-2 pr-3">
              <img src="<?=MY_IMAGEURL.'img-berakhlak.png'?>" class="mb-3" style="width: 100%" /><br />
              <img src="<?=MY_IMAGEURL.'img-bangga-melayani-bangsa.png'?>" style="width: 100%" />
            </div>-->
            <div class="col-lg-4" style="padding: 0.5rem">
              <h6 class="font-weight-bold">LINK TERKAIT</h6>
              <ul class="todo-list ui-sortable text-sm">
                <li class="active mb-2" style="border: 2px solid #17a2b8">
                  <span class="text d-block m-0">
                    <a class="text-info" href="https://tebingtinggikota.go.id/" target="_blank">PEMERINTAH KOTA TEBING TINGGI<span class="pull-right"><i class="far fa-chevron-right"></i></span></a>
                  </span>
                </li>
                <li class="active mb-2" style="border: 2px solid #17a2b8">
                  <span class="text d-block m-0">
                    <a class="text-info" href="http://setda.tebingtinggikota.go.id/" target="_blank">SEKRETARIAT DAERAH KOTA<span class="pull-right"><i class="far fa-chevron-right"></i></span></a>
                  </span>
                </li>
                <li class="active" style="border: 2px solid #17a2b8">
                  <span class="text d-block m-0">
                    <a class="text-info" href="http://organisasi.tebingtinggikota.go.id/" target="_blank">BAGIAN ORGANISASI<span class="pull-right"><i class="far fa-chevron-right"></i></span></a>
                  </span>
                </li>
              </ul>
            </div>
            <div class="col-lg-3" style="padding: 0.5rem">
              <h6 class="font-weight-bold">STATISTIK PENGUNJUNG <small class="font-italic"><?=date('d-m-Y')?></small></h6>
              <div class="card card-widget widget-user-2">
                <div class="card-body p-0">
                  <ul class="nav flex-column text-sm" style="border: 2px solid #17a2b8 !important">
                    <li class="nav-item">
                      <span class="nav-link">PER HARI <strong class="float-right"><?=number_format($numStatToday)?></strong></span>
                    </li>
                    <li class="nav-item">
                      <span class="nav-link">PER BULAN <strong class="float-right"><?=number_format($numStatMonthly)?></strong></span>
                    </li>
                    <li class="nav-item">
                      <span class="nav-link">PER TAHUN <strong class="float-right"><?=number_format($numStatAnnualy)?></strong></span>
                    </li>
                    <li class="nav-item">
                      <span class="nav-link">TOTAL <strong class="float-right"><?=number_format($numStatTotal)?></strong></span>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
            <div class="clearfix"></div>
          </div>
        </div>
      </div>
    </div>
    <footer class="main-footer">
      <div class="container">
        <div class="float-right d-none d-sm-inline">
          Strongly developed by <b>Partopi Tao</b>
        </div>
        <strong>Copyright &copy; <?=date("Y")?> <?=$this->setting_web_name?></strong>&nbsp;<sup><?=$this->setting_web_version?></sup>
      </div>
    </footer>
    </div>
    </body>
    </html>
