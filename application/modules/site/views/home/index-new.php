
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Portal <?=$this->setting_web_name?></title>
  <meta name="description" content="Portal <?=$this->setting_web_name?>" />
  <meta name="keywords" content="Aplikasi, Pemerintah, Tebing Tinggi, Organisasi, Teknologi Informasi, Portal" />
  <meta name="author" content="Partopi Tao" />
  <link rel="stylesheet" type="text/css" href="<?=base_url()?>/assets/themes/homepage/css/demo.css" />
  <link rel="stylesheet" type="text/css" href="<?=base_url()?>/assets/themes/homepage/css/common.css" />
  <link rel="stylesheet" type="text/css" href="<?=base_url()?>/assets/themes/homepage/css/style5.css" />
  <link href="https://fonts.googleapis.com/css?family=Signika" rel="stylesheet">
  <link rel="shortcut icon" type="image/png" href="<?=MY_IMAGEURL.$this->setting_web_logo?>" />
  <script type="text/javascript" src="<?=base_url()?>/assets/themes/homepage/js/modernizr.js"></script>
  <style>
  body::after {
    background: url('<?=MY_IMAGEURL.'footer-map-bg.png'?>');
    content: '';
    height: 100%;
    left: 0;
    opacity: 0.05;
    position: absolute;
    top: 0;
    width: 100%;
    z-index: -1;
  }
  </style>
</head>
    <!--body style="background-image: url('images/bth.jpg'); background-size: cover; background-attachment: fixed; background-repeat: no-repeat;" -->
    <body style="background: none !important">
        <div class="container">
			<header style="color: red; padding-top: 30px; border-bottom:2px dotted #E64848 !important">
				<h1><strong>PORTAL <span style="color: #E64848;"><?=$this->setting_web_name?></span></strong></h1>
				<h2><?=$this->setting_web_desc?></h2>
			</header>
      <audio autoplay loop>
			<source src="batanghari2111.mp3" type="audio/ogg">
      </audio>
			<section class="">
				<ul class="ch-grid">
          <li style="color: green;">
						<a href="<?=site_url('sakipv2/home/index')?>">
						<div class="ch-item">
							<div class="ch-info-wrap">
								<div class="ch-info">
									<div class="ch-info-front" style="background-image: url('<?=MY_IMAGEURL.'icon-esakip.png'?>'); background-size: cover"></div>
									<div class="ch-info-back">
										<h3 style="font-size: 8pt">Sistem Akuntabilitas Kinerja Instansi Pemerintah</h3>
									</div>
								</div>
							</div>
						</div><br>
						</a>
						<big>e-SAKIP</big>
					</li>
          <li style="color: green;">
						<a href="<?=site_url('ajbk/user/login')?>">
						<div class="ch-item">
							<div class="ch-info-wrap">
								<div class="ch-info">
									<div class="ch-info-front" style="background-image: url('<?=MY_IMAGEURL.'icon-anjab.png'?>'); background-size: cover"></div>
									<div class="ch-info-back">
										<h3 style="font-size: 8pt">Analisis Jabatan dan Analisis Beban Kerja</h3>
									</div>
								</div>
							</div>
						</div><br>
						</a>
						<big>e-ANJAB & ABK</big>
					</li>
          <li style="color: green;">
						<a href="<?=site_url('rb/user/login')?>">
						<div class="ch-item">
							<div class="ch-info-wrap">
								<div class="ch-info">
									<div class="ch-info-front" style="background-image: url('<?=MY_IMAGEURL.'icon-rb.png'?>'); background-size: cover"></div>
									<div class="ch-info-back">
										<h3 style="font-size: 8pt">Percepatan Pelaksanaan Reformasi Birokrasi</h3>
									</div>
								</div>
							</div>
						</div><br>
						</a>
						<big>e-RB</big>
					</li>
          <li style="color: green;">
						<a href="<?=site_url('lke/user/login')?>">
						<div class="ch-item">
							<div class="ch-info-wrap">
								<div class="ch-info">
									<div class="ch-info-front" style="background-image: url('<?=MY_IMAGEURL.'icon-lke.png'?>'); background-size: cover"></div>
									<div class="ch-info-back">
										<h3 style="font-size: 8pt">Lembar Kerja Evaluasi Akuntabilitas Kinerja</h3>
									</div>
								</div>
							</div>
						</div><br>
						</a>
						<big>e-EVATAS</big>
					</li>
          <li style="color: green;">
						<a href="<?=site_url('eform/user/login')?>">
						<div class="ch-item">
							<div class="ch-info-wrap">
								<div class="ch-info">
									<div class="ch-info-front" style="background-image: url('<?=MY_IMAGEURL.'icon-kuisioner.png'?>'); background-size: cover"></div>
									<div class="ch-info-back">
										<h3 style="font-size: 8pt">Sistem Informasi Kuisioner Online</h3>
									</div>
								</div>
							</div>
						</div><br>
						</a>
						<big>e-KUISIONER</big>
					</li>
          <li style="color: green;">
						<a href="<?=site_url('probis/user/login')?>">
						<div class="ch-item">
							<div class="ch-info-wrap">
								<div class="ch-info">
									<div class="ch-info-front" style="background-image: url('<?=MY_IMAGEURL.'icon-mesop.png'?>'); background-size: cover"></div>
									<div class="ch-info-back">
										<h3 style="font-size: 8pt">Portal Manajemen Evaluasi SOP</h3>
									</div>
								</div>
							</div>
						</div><br>
						</a>
						<big>ME SOP</big>
					</li>
          <li style="color: green;">
						<a href="<?=site_url('penting/user/login')?>">
						<div class="ch-item">
							<div class="ch-info-wrap">
								<div class="ch-info">
									<div class="ch-info-front" style="background-image: url('<?=MY_IMAGEURL.'icon-penting.png'?>'); background-size: cover"></div>
									<div class="ch-info-back">
										<h3 style="font-size: 8pt">Sistem Informasi Penurunan Angka Stunting</h3>
									</div>
								</div>
							</div>
						</div><br>
						</a>
						<big>SI PENTING</big>
					</li>
          <li style="color: green;">
						<a href="https://sikemas.tebingtinggikota.go.id/">
						<div class="ch-item">
							<div class="ch-info-wrap">
								<div class="ch-info">
									<div class="ch-info-front" style="background-image: url('<?=MY_IMAGEURL.'icon-sikemas.png'?>'); background-size: cover"></div>
									<div class="ch-info-back">
										<h3 style="font-size: 8pt">Sistem Informasi Kepuasan Masyarakat</h3>
									</div>
								</div>
							</div>
						</div><br>
						</a>
						<big>SIKEMAS</big>
					</li>
          <li style="color: green;">
						<a href="<?=site_url('site/home/faq')?>">
						<div class="ch-item">
							<div class="ch-info-wrap">
								<div class="ch-info">
									<div class="ch-info-front" style="background-image: url('<?=MY_IMAGEURL.'icon-faq.png'?>'); background-size: cover"></div>
									<div class="ch-info-back">
										<h3 style="font-size: 8pt">Layanan Pengaduan dan Tanya Jawab</h3>
									</div>
								</div>
							</div>
						</div><br>
						</a>
						<big>PENGADUAN</big>
					</li>
          <li style="color: green;">
						<a href="<?=site_url('site/user/login')?>">
						<div class="ch-item">
							<div class="ch-info-wrap">
								<div class="ch-info">
									<div class="ch-info-front" style="background-image: url('<?=MY_IMAGEURL.'icon-control.png'?>'); background-size: cover"></div>
									<div class="ch-info-back">
										<h3 style="font-size: 8pt">Panel Kontrol Administrator Sistem</h3>
									</div>
								</div>
							</div>
						</div><br>
						</a>
						<big>CONTROL PANEL</big>
					</li>
				</ul>
			</section>

			<header>
			<p style="color: #555; padding-top: 30px; border-top:2px dotted #E64848 !important " >
				<strong>BAGIAN ORGANISASI</strong> <br /> Sekretariat Daerah Kota <br /> Pemerintah Kota Tebing Tinggi<br><br>
				<img src="<?=MY_IMAGEURL.'logo.png'?>"	width="75" style="margin-bottom: 10px; "><br />
        <img src="<?=MY_IMAGEURL.'img-berakhlak.png'?>"	width="75" style="margin-bottom: 10px; ">&nbsp;
        <img src="<?=MY_IMAGEURL.'img-bangga-melayani-bangsa.png'?>"	width="75" style="margin-bottom: 10px; ">
			</p>
			</header>
        </div>
    </body>
</html>
