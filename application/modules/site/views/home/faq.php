<div class="content-header">
  <div class="container">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1 class="m-0 font-weight-light"><?= $title ?></h1>
      </div>
      <div class="col-sm-6 d-none d-sm-inline-block">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="<?=site_url()?>">Beranda</a></li>
          <li class="breadcrumb-item active">FAQ</li>
        </ol>
      </div>
    </div>
  </div>
</div>
<div class="content">
  <div class="container">
    <div class="row">
      <div class="col-sm-6">
        <div class="card">
          <div class="card-header">
            <h5 class="card-title m-0 text-muted font-italic">Silakan isi form pengaduan dibawah ini.</h5>
          </div>
          <form id="form-main" action="<?=current_url()?>" method="post">
            <div class="card-body">
              <div class="form-group mb-0">
                <div class="row">
                  <div class="col-sm-6 mb-3">
                    <label>Nama Lengkap</label>
                    <input type="text" class="form-control" placeholder="Nama Lengkap" name="<?=COL_NMNAMA?>" required />
                  </div>
                  <div class="col-sm-6 mb-3">
                    <label>Kontak</label>
                    <input type="text" class="form-control" placeholder="No. HP / Whatsapp / Email" name="<?=COL_NMKONTAK?>" />
                  </div>
                  <div class="col-sm-12 mb-3">
                    <label>Aduan / Keluhan / Pertanyaan</label>
                    <textarea class="form-control" placeholder="Uraikan Aduan / Keluhan Pertanyaan anda.." name="<?=COL_NMKETERANGAN?>" required></textarea>
                  </div>
                </div>
              </div>
            </div>
            <div class="card-footer">
              <button type="submit" class="btn btn-info"><i class="far fa-check-circle"></i> SUBMIT</button>
            </div>
          </form>
        </div>
      </div>
      <div class="col-sm-6 d-none d-sm-inline-block">
        <img src="<?=MY_IMAGEURL.'contact-us.png'?>" alt="#">
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
$(document).ready(function(){
  $('#form-main').validate({
    submitHandler: function(form) {
      var btnSubmit = $('button[type=submit]', form);
      btnSubmit.attr('disabled', true);
      $(form).ajaxSubmit({
        dataType: 'json',
        type : 'post',
        success: function(res) {
          if(res.error != 0) {
            toastr.error(res.error);
          } else {
            toastr.success(res.success);
            setTimeout(function(){
              location.reload();
            }, 1500);
          }
        },
        error: function() {
          toastr.error('Maaf, telah terjadi kesalahan pada server.');
        },
        complete: function() {
          btnSubmit.attr('disabled', false);
        }
      });

      return false;
    }
  });
});
</script>
