<?php
$data = array();
$i = 0;
$sumUraian=0;
$sumBeban=0;
$sumPegawai=0;
$sumBezetting=0;
foreach ($res as $d) {
  $txtUnit = '<li style="list-style-type: disclosure-closed">'.$d[COL_NM_SUB_UNIT].'</li>';
  if(!empty($d[COL_NM_BID])) {
    $txtUnit .= '<li style="list-style-type: disclosure-closed">'.$d[COL_NM_BID].'</li>';
  }
  if(!empty($d[COL_NM_SUBBID])) {
    $txtUnit .= '<li style="list-style-type: disclosure-closed">'.$d[COL_NM_SUBBID].'</li>';
  }
  $res[$i] = array(
    anchor('site/home/cetak_jabatan/'.$d[COL_KD_JABATAN],'<i class="fa fa-print text-success"></i>', array('target'=>'_blank')),
    $d[COL_NM_JABATAN].(!empty($d[COL_ID_JABATAN])?'<br /><small>Kode: '.$d[COL_ID_JABATAN].'</small>':''),
    //$d[COL_KD_TYPE]==JABATAN_TYPE_STRUKTURAL?'Struktural':($d[COL_KD_TYPE]==JABATAN_TYPE_FUNGSIONAL?'Fungsional':'??'),
    '<ul class="m-0 pl-3 text-sm">'.$txtUnit.'</ul>',
    number_format($d["Uraian"], 0),
    number_format($d["Beban"], 0),
    number_format($d["Pegawai"], 0),
    ($d["Bezetting"]!=null?number_format($d["Bezetting"], 0):'-'),
  );
  $i++;
  $sumUraian+=$d["Uraian"];
  $sumBeban+=$d["Beban"];
  $sumPegawai+=$d["Pegawai"];
  $sumBezetting+=($d["Bezetting"]!=null?$d["Bezetting"]:0);
}
$data = json_encode($res);
 ?>
 <form id="form-bezetting-partial" method="post" action="#">
   <table id="list-bezetting-partial" class="table table-bordered table-hover">
     <thead>
       <tr>
         <th>#</th>
         <th>Jabatan</th>
         <th>Unit Kerja</th>
         <th>Uraian Tugas</th>
         <th>ABK (Jam)</th>
         <th>ABK (Pegawai)</th>
         <th>Bezetting Pegawai</th>
       </tr>
     </thead>
     <tfoot>
       <tr>
         <th colspan="3" class="text-right">TOTAL</th>
         <th class="text-right"><?=number_format($sumUraian)?></th>
         <th class="text-right"><?=number_format($sumBeban)?></th>
         <th class="text-right"><?=number_format($sumPegawai)?></th>
         <th class="text-right"><?=number_format($sumBezetting)?></th>
       </tr>
     </tfoot>
   </table>
 </form>
 <script type="text/javascript">
 $(document).ready(function() {
   var dataTable = $('#list-bezetting-partial').dataTable({
     "autoWidth" : false,
     "aaData": <?=$data?>,
     "scrollY" : '60vh',
     "scrollX": "120%",
     "iDisplayLength": 100,
     "aLengthMenu": [[100, 1000, 5000, -1], [100, 1000, 5000, "Semua"]],
     "dom":"R<'row'<'col-sm-4'l><'col-sm-8'f>><'row'<'col-sm-12'tr>><'row'<'col-sm-5'i><'col-sm-7'p>>",
     "buttons": ['copyHtml5','excelHtml5','csvHtml5','pdfHtml5'],
     "ordering": false,
     "columnDefs":[
       {targets: [0,4,3,5,6], className:'dt-body-right'},
       {targets: [0], width:'40px'},
       {targets: [2], width:'300px'},
       {targets: [3,4,5,6], width:'20px'}
     ],
     /*"aoColumns": [
       {"sTitle": "#", "sWidth":"40px"},
       {"sTitle": "Jabatan"},
       {"sTitle": "Unit Kerja", "sWidth":"300px"},
       {"sTitle": "Uraian Tugas", "sWidth":"20px"},
       {"sTitle": "ABK (Jam)", "sWidth":"20px"},
       {"sTitle": "ABK (Pegawai)", "sWidth":"20px"},
       {"sTitle": "Bezetting Pegawai", "sWidth":"20px"}
     ],*/
     "fnCreatedRow" : function( nRow, aData, iDataIndex) {
       $('.modal-popup, .modal-popup-edit', $(nRow)).click(function(){
         var a = $(this);
         var bezetting = $(this).data('jumlah');
         var tahun = $(this).data('tahun');
         var editor = $("#modal-editor");

         $('[name=<?=COL_JLH_PEGAWAI?>]', editor).val(bezetting);
         if(tahun) $('[name=<?=COL_TAHUN?>]', editor).val(tahun);
         editor.modal("show");
         $(".btn-ok", editor).unbind('click').click(function() {
           var dis = $(this);
           dis.html("Loading...").attr("disabled", true);
           $('#form-editor').ajaxSubmit({
             dataType: 'json',
             url : a.attr('href'),
             success : function(data){
               if(data.error==0){
                 toastr.success(data.success);
               }else{
                 toastr.error(data.error);
               }
             },
             error: function() {
               toastr.error('Server error.');
             },
             complete: function() {
               dis.html("Simpan").attr("disabled", false);
               editor.modal("hide");
               setTimeout(function(){ RefreshData(); }, 1000);
             }
           });
         });
         return false;
       });
     }
   });
 });
 </script>
