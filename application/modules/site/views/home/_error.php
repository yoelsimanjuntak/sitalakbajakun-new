<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
    </div>
  </div>
</div>
<section class="content mt-5 pt-5">
  <div class="error-page">
    <h2 class="headline text-danger"> 404</h2>
    <div class="error-content">
      <h3><i class="fa fa-warning text-danger"></i> PAGE NOT FOUND</h3>
      <p>
        Mohon maaf, laman yang anda tuju tidak ditemukan. Mungkin sedang terjadi kesalahan pada sistem.
        Silakan coba beberapa saat lagi, atau hubungi Administrator.
      </p>
      <p>
        <a href="<?=site_url()?>" class="btn btn-danger btn-sm"><i class="far fa-home"></i> KE MENU UTAMA</a>
      </p>
    </div>
  </div>
</section>
