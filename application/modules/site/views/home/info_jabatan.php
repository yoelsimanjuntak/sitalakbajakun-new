<?php
$ruser = GetLoggedUser();

$ropd = array();
$rbid = array();
$rsubbid = array();
if($ruser[COL_ROLEID] == ROLEKADIS || $ruser[COL_ROLEID] == ROLEKABID || $ruser[COL_ROLEID] == ROLEKASUBBID) {
  $strOrg = explode('.', $ruser[COL_COMPANYID]) ;
  $ropd = $this->db->where(array(
    COL_KD_URUSAN=>$strOrg[0],
    COL_KD_BIDANG=>$strOrg[1],
    COL_KD_UNIT=>$strOrg[2],
    COL_KD_SUB=>$strOrg[3]
  ))->get(TBL_AJBK_UNIT)->row_array();

  if($ruser[COL_ROLEID] == ROLEKABID || $ruser[COL_ROLEID] == ROLEKASUBBID) {
    $rbid = $this->db->where(array(
      COL_KD_URUSAN=>$strOrg[0],
      COL_KD_BIDANG=>$strOrg[1],
      COL_KD_UNIT=>$strOrg[2],
      COL_KD_SUB=>$strOrg[3],
      COL_KD_BID=>$strOrg[4]
    ))->get(TBL_AJBK_UNIT_BID)->row_array();
  }

  if($ruser[COL_ROLEID] == ROLEKASUBBID) {
    $rsubbid = $this->db->where(array(
      COL_KD_URUSAN=>$strOrg[0],
      COL_KD_BIDANG=>$strOrg[1],
      COL_KD_UNIT=>$strOrg[2],
      COL_KD_SUB=>$strOrg[3],
      COL_KD_BID=>$strOrg[4],
      COL_KD_SUBBID=>$strOrg[5]
    ))->get(TBL_AJBK_UNIT_SUBBID)->row_array();
  }
}
 ?>
<div class="content-header">
  <div class="container">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h3 class="m-0 text-dark"><?= $title ?></h3>
      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="<?=site_url()?>">Home</a></li>
          <li class="breadcrumb-item active"><?=$title?></li>
        </ol>
      </div>
    </div>
  </div>
</div>
<section class="content">
  <div class="container">
    <div class="row">
      <div class="col-sm-12">
        <div class="card card-default">
          <div class="card-header">
            <h3 class="card-title">FILTER</h3>
            <div class="card-tools">
              <!--<button type="button" class="btn btn-tool" data-card-widget="maximize"><i class="fas fa-expand"></i></button>-->
              <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
            </div>
          </div>
          <div class="card-body">
            <?=form_open(current_url(),array('role'=>'form','id'=>'filter-form','class'=>'form-horizontal'))?>
            <div class="form-group row">
                <label class="control-label col-sm-2">Madya</label>
                <div class="col-sm-8">
                    <input type="text" class="form-control" value="<?=$this->setting_org_name?>" readonly />
                </div>
            </div>
            <div class="form-group row">
                <label class="control-label col-sm-2">Pratama</label>
                <div class="col-sm-8">
                  <?php
                  if(!empty($ropd)) {
                    ?>
                    <input type="hidden" name="KdOPD" value="<?=$ropd[COL_UNIQ]?>" />
                    <input type="text" class="form-control" value="<?=$ropd[COL_NM_SUB_UNIT]?>" readonly />
                    <?php
                  } else {
                    ?>
                    <select name="KdOPD" class="form-control">
                      <?=GetCombobox("SELECT * FROM ajbk_unit", COL_UNIQ, COL_NM_SUB_UNIT, null, true, false, '-- Semua --')?>
                    </select>
                    <?php
                  }
                   ?>
                </div>
            </div>
            <div class="form-group row">
                <label class="control-label col-sm-2">Administrator</label>
                <div class="col-sm-8">
                  <?php
                  if(!empty($rbid)) {
                    ?>
                    <input type="hidden" name="KdBidang" value="<?=$rbid[COL_UNIQ]?>" />
                    <input type="text" class="form-control" value="<?=$rbid[COL_NM_BID]?>" readonly />
                    <?php
                  } else {
                    ?>
                    <select name="KdBidang" class="form-control"></select>
                    <?php
                  }
                   ?>
                </div>
            </div>
            <div class="form-group row">
                <label class="control-label col-sm-2">Pengawas</label>
                <div class="col-sm-8">
                  <?php
                  if(!empty($rsubbid)) {
                    ?>
                    <input type="hidden" name="KdSubBidang" value="<?=$rsubbid[COL_UNIQ]?>" />
                    <input type="text" class="form-control" value="<?=$rsubbid[COL_NM_SUBBID]?>" readonly />
                    <?php
                  } else {
                    ?>
                    <select name="KdSubBidang" class="form-control"></select>
                    <?php
                  }
                   ?>
                </div>
            </div>
            <div class="form-group row">
              <label class="control-label col-sm-2">Tahun</label>
              <div class="col-sm-2">
                <select name=<?=COL_TAHUN?> class="form-control">
                  <?php
                  for($i=date('Y')-5; $i<=date('Y')+5; $i++) {
                    ?>
                    <option value="<?=$i?>" <?=$i==date('Y')?'selected':''?>><?=$i?></option>
                    <?php
                  }
                  ?>
                </select>
              </div>
            </div>
            <?=form_close()?>
          </div>
        </div>

        <div id="card-data" class="card card-default">
          <div class="card-header">
            <h3 class="card-title">
              DAFTAR JABATAN
            </h3>
            <div class="card-tools">
              <!--<?=anchor('ajbk/jabatan/add','<i class="fa fa-plus"></i> BEZETTING',array('class'=>'btn btn-tool text-primary'))?>-->
              <button type="button" class="btn btn-tool" data-card-widget="maximize"><i class="fas fa-expand"></i></button>
              <!--<button type="button" class="btn btn-tool btn-refresh"><i class="fas fa-sync-alt"></i></button>-->
            </div>
          </div>
          <div class="card-body">

          </div>
          <div class="overlay" style="display: none" >
            <i class="fas fa-2x fa-sync-alt fa-spin"></i>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<script type="text/javascript">
function RefreshData() {
  var card = $('#card-data');
  var elOPD = $('[name=KdOPD]');
  var elBid = $('[name=KdBidang]');
  var elSubbid = $('[name=KdSubBidang]');
  var elTahun = $('[name=Tahun]');

  $('.overlay', card).toggle();
  $('.card-body', card).load('<?=site_url('site/home/info-jabatan-partial')?>', {KdOPD: elOPD.val(), KdBidang: elBid.val(), KdSubBidang: elSubbid.val(), Tahun: elTahun.val()}, function() {
    $('.overlay', card).toggle();
    /*$('html, body').animate({
      scrollTop: card.offset().top
    }, 2000);*/
  });
}
$(document).ready(function() {
  $('[name=KdOPD],[name=KdBidang],[name=KdSubBidang],[name=Tahun]').change(function() {
    RefreshData();
  });
  $('[name=KdOPD]').change(function() {
    dis = $(this);
    $('select[name=KdBidang]').load('<?=site_url("ajbk/ajax/get-opt-bidang")?>', {KdOPD: dis.val()}, function() {
      $("select[name=KdBidang]").select2({ width: 'resolve', theme: 'bootstrap4' });
    });
  }).trigger('change');
  $('[name=KdBidang]').change(function() {
    dis = $(this);
    $('select[name=KdSubBidang]').load('<?=site_url("ajbk/ajax/get-opt-subbidang")?>', {KdBid: dis.val()}, function() {

    });
  }).trigger('change');
  $('[name=KdSubBidang]').trigger('change');
});
</script>
