<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1 class="m-0 text-dark"><?= $title ?> <small> Form</small></h1>
      </div><!-- /.col -->
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="<?=site_url()?>"><i class="fa fa-dashboard"></i> Home</a></li>
          <li class="breadcrumb-item"><a href="<?=site_url('user/index')?>"> Users</a></li>
          <li class="breadcrumb-item active"><?=$edit?'Edit':'Add'?></li>
        </ol>
      </div>
    </div>
  </div>
</div>
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-sm-12">
        <div class="card card-primary">
            <div class="card-body">
                <div style="display: none" class="alert alert-danger errorBox">
                    <i class="fa fa-ban"></i> Error :
                    <span class="errorMsg"></span>
                </div>
                <?php
                if($this->input->get('error') == 1){
                    ?>
                    <div class="alert alert-danger alert-dismissible">
                        <i class="fa fa-ban"></i>
                        <span class="">Data gagal disimpan, silahkan coba kembali</span>
                    </div>
                <?php
                }
                if(validation_errors()){
                    ?>
                    <div class="alert alert-danger alert-dismissible">
                        <i class="fa fa-ban"></i>
                        <?=validation_errors()?>
                    </div>
                <?php
                }
                if(!empty($upload_errors)) {
                    ?>
                    <div class="alert alert-danger alert-dismissible">
                        <i class="fa fa-ban"></i>
                        <?=$upload_errors?>
                    </div>
                <?php
                }
                ?>

                <?=form_open_multipart(current_url(),array('role'=>'form','id'=>'userForm','class'=>'form-horizontal'))?>
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group row">
                            <label class="control-label col-sm-4">Username</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="<?=COL_USERNAME?>" value="<?= $edit ? $data[COL_USERNAME] : ""?>" <?=$edit?"disabled":""?> required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="control-label col-sm-4">Email</label>
                            <div class="col-sm-8">
                                <input type="email" class="form-control" name="<?=COL_EMAIL?>" value="<?= $edit ? $data[COL_EMAIL] : ""?>" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="control-label col-sm-4">Nama</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="<?=COL_NAME?>" value="<?= $edit ? $data[COL_NAME] : ""?>" required>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                      <?php
                      if(!$edit) {
                          ?>
                          <div class="form-group row">
                              <label class="control-label col-sm-4">Password</label>
                              <div class="col-sm-8">
                                  <input type="password" class="form-control" name="<?=COL_PASSWORD?>" >
                              </div>
                          </div>
                          <div class="form-group row">
                              <label class="control-label col-sm-4">Confirm Password</label>
                              <div class="col-sm-8">
                                  <input type="password" class="form-control" name="RepeatPassword" >
                              </div>
                          </div>
                      <?php
                      }
                      ?>
                        <div class="form-group row">
                            <label class="control-label col-sm-4">Role</label>
                            <div class="col-sm-8">
                                <select name="<?=COL_ROLEID?>" class="form-control" required>
                                    <option value="">Select Role</option>
                                    <?=GetCombobox("SELECT * FROM _roles", COL_ROLEID, COL_ROLENAME, (!empty($data[COL_ROLEID]) ? $data[COL_ROLEID] : null))?>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-sm-12">
                        <div class="form-group row">
                            <label class="control-label col-sm-2">OPD</label>
                            <div class="col-sm-8">
                                <div class="input-group">
                                    <?php
                                    $nmSub = "";
                                    if($edit) {
                                        $compID = $data[COL_COMPANYID];
                                        if(count(explode(".", $compID)) >= 4) {
                                            $arrOPD = explode(".", $compID);

                                            $eplandb = $this->load->database("eplan", true);
                                            $eplandb->where(COL_KD_URUSAN, $arrOPD[0]);
                                            $eplandb->where(COL_KD_BIDANG, $arrOPD[1]);
                                            $eplandb->where(COL_KD_UNIT, $arrOPD[2]);
                                            $eplandb->where(COL_KD_SUB, $arrOPD[3]);
                                            $subunit = $eplandb->get("ref_sub_unit")->row_array();
                                            if($subunit) {
                                                $nmSub = $subunit["Nm_Sub_Unit"];
                                            }
                                        }
                                    }

                                    ?>
                                    <input type="text" class="form-control" name="text-opd" value="<?= $edit && count($arrOPD) >= 4 ? $arrOPD[0].".".$arrOPD[1].".".$arrOPD[2].".".$arrOPD[3]." ".$nmSub : ""?>" readonly>
                                    <input type="hidden" name="<?=COL_KD_URUSAN?>" value="<?= $edit && count($arrOPD) >= 4 ? $arrOPD[0] : ""?>" required   >
                                    <input type="hidden" name="<?=COL_KD_BIDANG?>" value="<?= $edit && count($arrOPD) >= 4 ? $arrOPD[1] : ""?>" required   >
                                    <input type="hidden" name="<?=COL_KD_UNIT?>" value="<?= $edit && count($arrOPD) >= 4 ? $arrOPD[2] : ""?>" required   >
                                    <input type="hidden" name="<?=COL_KD_SUB?>" value="<?= $edit && count($arrOPD) >= 4 ? $arrOPD[3] : ""?>" required   >
                                    <div class="input-group-append">
                                        <button type="button" class="btn btn-default btn-flat btn-browse-opd" data-toggle="modal" data-target="#browseOPD" data-toggle="tooltip" data-placement="top" title="Pilih OPD"><i class="fa fa-ellipsis-h"></i></button>
                                        <button type="button" class="btn btn-default btn-browse-del" title="Hapus"><i class="fa fa-times"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="control-label col-sm-2">Bidang</label>
                            <div class="col-sm-8">
                                <div class="input-group">
                                    <?php
                                    $nmBid = "";
                                    if($edit) {
                                        $compID = $data[COL_COMPANYID];
                                        if(count(explode(".", $compID)) > 4) {
                                            $arrOPD = explode(".", $compID);
                                            $this->db->where(COL_KD_URUSAN, $arrOPD[0]);
                                            $this->db->where(COL_KD_BIDANG, $arrOPD[1]);
                                            $this->db->where(COL_KD_UNIT, $arrOPD[2]);
                                            $this->db->where(COL_KD_SUB, $arrOPD[3]);
                                            $this->db->where(COL_KD_BID, $arrOPD[4]);
                                            $bid = $this->db->get(TBL_SAKIP_MBID)->row_array();
                                            if($bid) {
                                                $nmBid = $bid[COL_NM_BID];
                                            }
                                        }
                                    }
                                    ?>
                                    <input type="text" class="form-control" name="text-bid" value="<?= $edit && count($arrOPD) > 4 ? $arrOPD[4].". ".$nmBid : ""?>" readonly>
                                    <input type="hidden" name="<?=COL_KD_BID?>" value="<?= $edit && count($arrOPD) > 4 ? $arrOPD[4] : ""?>" required   >
                                    <div class="input-group-append">
                                      <button type="button" class="btn btn-default btn-browse-bid" data-toggle="modal" data-target="#browseBid" data-toggle="tooltip" data-placement="top" title="Pilih Bidang"><i class="fa fa-ellipsis-h"></i></button>
                                      <button type="button" class="btn btn-default btn-browse-del" title="Hapus"><i class="fa fa-times"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="control-label col-sm-2">Sub Bidang</label>
                            <div class="col-sm-8">
                                <div class="input-group">
                                    <?php
                                    $nmSubBid = "";
                                    if($edit) {
                                        $compID = $data[COL_COMPANYID];
                                        if(count(explode(".", $compID)) > 5) {
                                            $arrOPD = explode(".", $compID);
                                            $this->db->where(COL_KD_URUSAN, $arrOPD[0]);
                                            $this->db->where(COL_KD_BIDANG, $arrOPD[1]);
                                            $this->db->where(COL_KD_UNIT, $arrOPD[2]);
                                            $this->db->where(COL_KD_SUB, $arrOPD[3]);
                                            $this->db->where(COL_KD_BID, $arrOPD[4]);
                                            $this->db->where(COL_KD_SUBBID, $arrOPD[5]);
                                            $subbid = $this->db->get(TBL_SAKIP_MSUBBID)->row_array();
                                            if($subbid) {
                                                $nmSubBid = $subbid[COL_NM_SUBBID];
                                            }
                                        }
                                    }
                                    ?>
                                    <input type="text" class="form-control" name="text-subbid" value="<?= $edit && count($arrOPD) > 5 ? $arrOPD[4].".".$arrOPD[5].". ".$nmSubBid : ""?>" readonly>
                                    <input type="hidden" name="<?=COL_KD_SUBBID?>" value="<?= $edit && count($arrOPD) > 5 ? $arrOPD[5] : ""?>" required   >
                                    <div class="input-group-append">
                                        <button type="button" class="btn btn-default btn-flat btn-browse-subbid" data-toggle="modal" data-target="#browseSubBid" data-toggle="tooltip" data-placement="top" title="Pilih Sub Bidang"><i class="fa fa-ellipsis-h"></i></button>
                                        <button type="button" class="btn btn-default btn-browse-del" title="Hapus"><i class="fa fa-times"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-md-12">
                        <div class="form-group row">
                          <a href="<?=site_url('user/index')?>" class="btn btn-default"><i class="fa fa-arrow-left"></i>&nbsp;&nbsp;Kembali ke Daftar</a>&nbsp;
                          <button type="submit" class="btn btn-primary">Simpan</button>
                        </div>
                    </div>
                </div>
                <?=form_close()?>
            </div>
        </div>
      </div>
    </div>
  </div>
</section>
<div class="modal fade" id="browseOPD" tabindex="-1" role="dialog">
  <div class="modal-dialog">
      <div class="modal-content">
          <div class="modal-header">
              <h4 class="modal-title">Browse</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          </div>
          <div class="modal-body">
              ...
          </div>
          <div class="modal-footer">
              <button type="button" class="btn btn-default btn-flat pull-right" data-dismiss="modal">Close</button>
          </div>
      </div>
  </div>
</div>
<div class="modal fade" id="browseBid" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Browse</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                ...
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default btn-flat pull-right" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="browseSubBid" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Browse</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                ...
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default btn-flat pull-right" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
$('.modal').on('hidden.bs.modal', function (event) {
  $(this).find(".modal-body").empty();
});

$('.btn-browse-del').on('click', function (event) {
  var dis = $(this);
  dis.closest('.form-group').find('input').val('');
});


    $('#browseOPD').on('show.bs.modal', function (event) {
        var modalBody = $(".modal-body", $("#browseOPD"));
        $(this).removeData('bs.modal');
        modalBody.html("<p style='font-style: italic'>Loading..</p>");
        modalBody.load("<?=site_url("sakip/ajax/browse-opd")?>", function () {
            $("[name=selID][type=hidden]", modalBody).unbind().change(function () {
                var kdSub = $(this).val().split('|');
                $("[name=Kd_Urusan]").val(kdSub[0]);
                $("[name=Kd_Bidang]").val(kdSub[1]);
                $("[name=Kd_Unit]").val(kdSub[2]);
                $("[name=Kd_Sub]").val(kdSub[3]);
            });
            $("[name=selText][type=hidden]", modalBody).unbind().change(function () {
                $("[name=text-opd]").val($(this).val());
            });
        });
    });
    $('#browseBid').on('show.bs.modal', function (event) {
        var modalBody = $(".modal-body", $("#browseBid"));
        $(this).removeData('bs.modal');
        var kdUrusan = $("[name=Kd_Urusan]").val();
        var kdBidang = $("[name=Kd_Bidang]").val();
        var kdUnit = $("[name=Kd_Unit]").val();
        var kdSub = $("[name=Kd_Sub]").val();

        if(!kdUrusan || !kdBidang || !kdUnit || !kdSub) {
            modalBody.html("<p style='font-style: italic'>Silakan pilih OPD terlebih dahulu!</p>");
            return;
        }

        modalBody.html("<p style='font-style: italic'>Loading..</p>");
        modalBody.load("<?=site_url("sakip/ajax/browse-bid")?>"+"?Kd_Urusan="+kdUrusan+"&Kd_Bidang="+kdBidang+"&Kd_Unit="+kdUnit+"&Kd_Sub="+kdSub, function () {
            $("[name=selID][type=hidden]", modalBody).unbind().change(function () {
                $("[name=Kd_Bid]").val($(this).val());
            });
            $("[name=selText][type=hidden]", modalBody).unbind().change(function () {
                $("[name=text-bid]").val($(this).val());
            });
        });
    });
    $('#browseSubBid').on('show.bs.modal', function (event) {
        var modalBody = $(".modal-body", $("#browseSubBid"));
        $(this).removeData('bs.modal');

        var kdUrusan = $("[name=Kd_Urusan]").val();
        var kdBidang = $("[name=Kd_Bidang]").val();
        var kdUnit = $("[name=Kd_Unit]").val();
        var kdSub = $("[name=Kd_Sub]").val();
        var kdBid = $("[name=Kd_Bid]").val();

        if(!kdUrusan || !kdBidang || !kdUnit || !kdSub || !kdBid) {
            modalBody.html("<p style='font-style: italic'>Silakan pilih Bidang OPD terlebih dahulu!</p>");
            return;
        }

        modalBody.html("<p style='font-style: italic'>Loading..</p>");
        modalBody.load("<?=site_url("sakip/ajax/browse-subbid")?>"+"?Kd_Urusan="+kdUrusan+"&Kd_Bidang="+kdBidang+"&Kd_Unit="+kdUnit+"&Kd_Sub="+kdSub+"&Kd_Bid="+kdBid, function () {
            $("[name=selID][type=hidden]", modalBody).unbind().change(function () {
                $("[name=Kd_Subbid]").val($(this).val());
            });
            $("[name=selText][type=hidden]", modalBody).unbind().change(function () {
                $("[name=text-subbid]").val($(this).val());
            });
        });
    });
</script>
