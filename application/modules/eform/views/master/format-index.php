<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h3 class="m-0 text-dark font-weight-light"><?=strtoupper($title)?></h3>
      </div>
      <div class="col-sm-6 text-right">
        <a href="<?=site_url('lke/master/format-ajax/add')?>" class="btn btn-sm btn-primary btn-popup-form" data-title="TAMBAH FORMAT LKE"><i class="far fa-plus-circle"></i> TAMBAH</a>
      </div>
    </div>
  </div>
</div>
<section class="content">
  <div class="container-fluid">
    <div class="row align-items-stretch">
      <?php
      if(!empty($res)) {
        foreach($res as $dat) {
          ?>
          <div class="col-12 col-sm-6 d-flex align-items-stretch">
            <div class="card w-100">
              <?php
              if($dat[COL_FORMISACTIVE]!=1) {
                ?>
                <div class="overlay" style="z-index: 0 !important"></div>
                <?php
              }
              ?>
              <div class="card-header">
                <h3 class="card-title font-weight-bold"><?=$dat[COL_FORMNAMA]?></h3>
                <div class="card-tools">
                  <?php
                  if($dat[COL_FORMISACTIVE]==1) {
                    ?>
                    <a href="<?=site_url('lke/master/format-ajax/status/'.$dat[COL_UNIQ])?>" class="btn-tool text-danger btn-popup-confirm" data-title="Apakah anda yakin?" data-stat="0"><i class="far fa-exclamation-circle"></i> SEMBUNYIKAN</a>
                    <?php
                  } else {
                    ?>
                    <a href="<?=site_url('lke/master/format-ajax/status/'.$dat[COL_UNIQ])?>" class="btn-tool text-info btn-popup-confirm" data-title="Apakah anda yakin?" data-stat="1"><i class="far fa-check-circle"></i> AKTIFKAN</a>
                    <?php
                  }
                  ?>
                  <a href="<?=site_url('lke/master/format-ajax/edit/'.$dat[COL_UNIQ])?>" class="btn-tool text-success btn-popup-form " data-title="UBAH FORMAT LKE"><i class="far fa-cog"></i> UBAH</a>
                  <a href="<?=site_url('lke/master/format-detail/'.$dat[COL_UNIQ])?>" class="btn-tool text-primary" data-title="KELOLA"><i class="far fa-list"></i> KELOLA</a>
                </div>
              </div>
              <div class="card-body">
                <h6 class="font-weight-bold">LKE <?=$dat[COL_FORMTYPE]?></h6>
                <p class="m-0"><?=$dat[COL_FORMDESC]?></p>
              </div>
            </div>
          </div>
          <?php
        }
      } else {
        ?>
        <div class="col-md-12">
          <div class="card">
            <div class="card-body">
              <p class="text-center mb-0 font-italic">
                BELUM ADA DATA TERSEDIA
              </p>
            </div>
          </div>
        </div>
        <?php
      }
      ?>
    </div>
  </div>
</section>
<div class="modal fade" id="modal-form" tabindex="-1" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="overlay d-none justify-content-center align-items-center">
        <i class="fas fa-2x fa-spinner fa-spin"></i>
      </div>
        <div class="modal-header">
          <h5 class="modal-title"></h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true"><i class="fas fa-close"></i></span>
          </button>
        </div>
        <div class="modal-body">
        </div>
        <div class="modal-footer d-block">
          <div class="row">
            <div class="col-lg-12 text-center">
              <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="far fa-times-circle"></i>&nbsp;BATAL</button>
              <button type="submit" class="btn btn-primary"><i class="far fa-plus-circle"></i>&nbsp;SUBMIT</button>
            </div>
          </div>
        </div>
    </div>
  </div>
</div>
<script type="text/javascript">
var modalForm = $('#modal-form');
$(document).ready(function() {
  modalForm.on('hidden.bs.modal', function (e) {
    $('.modal-body', modalForm).empty();
    $('.modal-title', modalForm).html('');
  });

  $('.btn-popup-form').click(function() {
    var url = $(this).attr('href');
    var title = $(this).data('title');
    if(url) {
      if(title) {
        $('.modal-title', modalForm).html(title);
      }

      modalForm.modal('show');
      $('.modal-body', modalForm).html('<p class="font-italic mb-0 text-center"><i class="far fa-circle-notch fa-spin"></i>&nbsp;MEMUAT...</p>');
      $('.modal-body', modalForm).load(url, function(){
        $('button[type=submit]', modalForm).unbind('click').click(function(){
          $('form', modalForm).submit();
        });
      });
    }

    return false;
  });
  $('.btn-popup-confirm').click(function() {
    var url = $(this).attr('href');
    var title = $(this).data('title');
    var stat = $(this).data('stat');
    if(url && title) {
      if(confirm(title)) {
        $.post(url, {status: stat}, function(data) {
          data=JSON.parse(data);
          console.log(data);
          if(data.error!=0) {
            toastr.error(data.error);
          } else {
            location.reload();
          }
        });
      }
    }

    return false;
  });
});
</script>
