<form id="form-lke" action="<?=current_url()?>" method="post">
  <input type="hidden" name="<?=COL_FORMCATEGORY?>" value="AKIP" />
  <div class="form-group">
    <label>Nama</label>
    <input type="text" name="<?=COL_FORMNAMA?>" class="form-control" value="<?=!empty($data)?$data[COL_FORMNAMA]:''?>" />
  </div>
  <div class="form-group">
    <label>Jenis</label>
    <select name="<?=COL_FORMTYPE?>" class="form-control" style="width: 100%">
      <option value="INSTANSI" <?=!empty($data)&&$data[COL_FORMTYPE]=='INSTANSI'?'selected':''?>>INSTANSI</option>
      <option value="UNIT" <?=!empty($data)&&$data[COL_FORMTYPE]=='UNIT'?'selected':''?>>UNIT</option>
    </select>
  </div>
  <div class="form-group">
    <label>Keterangan</label>
    <textarea name="<?=COL_FORMDESC?>" class="form-control"><?=!empty($data)?$data[COL_FORMDESC]:''?></textarea>
  </div>
</form>
<script type="text/javascript">
$(document).ready(function(){
  $('#form-lke').validate({
    submitHandler: function(form) {
      var modal = $(form).closest('modal');
      if(modal) {
        var btnSubmit = $('button[type=submit]', modal);
        var txtSubmit = btnSubmit.innerHTML;
        btnSubmit.html('<i class="fad fa-circle-notch fa-spin"></i>');
        btnSubmit.attr('disabled', true);
      }

      $(form).ajaxSubmit({
        dataType: 'json',
        type : 'post',
        success: function(res) {
          if(res.error != 0) {
            toastr.error(res.error);
          } else {
            toastr.success(res.success);
            setTimeout(function(){
              location.reload();
            }, 1000);
          }
        },
        error: function() {
          toastr.error('SERVER ERROR');
        },
        complete: function() {
          btnSubmit.html(txtSubmit);
          btnSubmit.attr('disabled', false);
        }
      });

      return false;
    }
  });
});
</script>
