<?php
$rdetail = $this->db
->where(COL_FORMID, $data[COL_UNIQ])
->where(COL_FORMIDPARENT, null)
->order_by(COL_FORMSEQ)
->get(TBL_LKE_FORM_DETAIL)->result_array();
?>
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h3 class="m-0 text-dark font-weight-light"><?=strtoupper($title)?></h3>
      </div>
    </div>
  </div>
</div>
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-sm-12">
        <div class="card card-indigo">
          <div class="card-header">
            <h3 class="card-title"><?=$data[COL_FORMNAMA]?></h3>
            <div class="card-tools">
              <?=anchor('lke/master/format-detail-form/add/'.$data[COL_UNIQ],'<i class="fa fa-plus-circle"></i> KOMPONEN',array('class'=>'btn btn-tool btn-popup-form', 'data-title'=>'TAMBAH KOMPONEN'))?>
            </div>
          </div>
          <div class="card-body p-0">
            <table class="table table-bordered">
              <thead>
                <tr>
                  <th style="width: 10px; white-space: nowrap">NO.</th>
                  <th style="width: 30%">KOMPONEN / SUB KOMPONEN</th>
                  <th style="width: 50px; white-space: nowrap">BOBOT</th>
                  <th>KRITERIA</th>
                  <th style="width: 10px; white-space: nowrap; text-align: center; padding-right: .75rem !important">#</th>
                </tr>
              </thead>
              <tbody>
                <?php
                if(!empty($rdetail)) {
                  foreach ($rdetail as $d) {
                    $crit = array();
                    if(!empty($d[COL_FORMCRITERIA])) $crit = explode(";", $d[COL_FORMCRITERIA]);
                    ?>
                    <tr>
                      <td><?=$d[COL_FORMSEQ]?></td>
                      <td style="width: 30%"><?=$d[COL_FORMQUEST]?></td>
                      <td class="text-right"><?=number_format($d[COL_FORMWEIGHT],2)?></td>
                      <td>
                        <?php
                        if(!empty($crit)) {
                          ?>
                          <ul style="padding-left: 1rem">
                            <?php
                            foreach($crit as $c) {
                              echo '<li class="text-sm">'.$c.'</li>';
                            }
                            ?>
                          </ul>
                          <?php
                        } else {
                          echo '-';
                        }
                        ?>
                      </td>
                      <td style="width: 10px; white-space: nowrap; text-align: right; padding-right: .75rem !important">
                        <a href="<?=site_url('lke/master/format-detail-load/'.$d[COL_UNIQ])?>" class="text-success btn-expand" data-id="<?=$d[COL_UNIQ]?>" data-toggle="tooltip" data-title="DAFTAR SUB KOMPONEN">
                          <i class="far fa-chevron-circle-down"></i>
                        </a>
                        <a href="<?=site_url('lke/master/format-detail-form/add/'.$data[COL_UNIQ].'/'.$d[COL_UNIQ])?>" class="text-primary btn-popup-form" data-toggle="tooltip" data-title="TAMBAH SUB KOMPONEN">
                          <i class="far fa-plus-circle"></i>
                        </a>
                        <a href="<?=site_url('lke/master/format-detail-delete/'.$data[COL_UNIQ])?>" class="text-danger btn-popup-confirm" data-toggle="tooltip" data-title="HAPUS">
                          <i class="far fa-times-circle"></i>
                        </a>
                      </td>
                    </tr>
                    <?php
                  }
                } else {
                  ?>
                  <tr>
                    <td colspan="4">
                      <p class="m-0 text-center font-italic">DATA TIDAK TERSEDIA</p>
                    </td>
                  </tr>
                  <?php
                }
                ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>

    </div>
  </div>
</section>
<div class="modal fade" id="modal-form" tabindex="-1" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="overlay d-none justify-content-center align-items-center">
        <i class="fas fa-2x fa-spinner fa-spin"></i>
      </div>
        <div class="modal-header">
          <h5 class="modal-title"></h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true"><i class="fas fa-close"></i></span>
          </button>
        </div>
        <div class="modal-body">
        </div>
        <div class="modal-footer d-block">
          <div class="row">
            <div class="col-lg-12 text-center">
              <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="far fa-times-circle"></i>&nbsp;BATAL</button>
              <button type="submit" class="btn btn-primary"><i class="far fa-plus-circle"></i>&nbsp;SUBMIT</button>
            </div>
          </div>
        </div>
    </div>
  </div>
</div>
<script type="text/javascript">
var modalForm = $('#modal-form');

function initRowEvents(el) {
  $('.btn-popup-form', el).click(function() {
    var url = $(this).attr('href');
    var title = $(this).data('title');
    if(url) {
      if(title) {
        $('.modal-title', modalForm).html(title);
      }

      modalForm.modal('show');
      $('.modal-body', modalForm).html('<p class="font-italic mb-0 text-center"><i class="far fa-circle-notch fa-spin"></i>&nbsp;MEMUAT...</p>');
      $('.modal-body', modalForm).load(url, function(){
        $(".uang", modalForm).number(true, 0, '.', ',');
        $('button[type=submit]', modalForm).unbind('click').click(function(){
          $('form', modalForm).submit();
        });
      });
    }

    return false;
  });
  $('.btn-popup-confirm', el).click(function() {
    var url = $(this).attr('href');
    var title = $(this).data('title');
    var stat = $(this).data('stat');
    if(url && title) {
      if(confirm(title)) {
        $.post(url, {status: stat}, function(data) {
          data=JSON.parse(data);
          console.log(data);
          if(data.error!=0) {
            toastr.error(data.error);
          } else {
            location.reload();
          }
        });
      }
    }

    return false;
  });

  $('.btn-expand', el).click(function() {
    var dis = $(this);
    var url = $(this).attr('href');
    var row = $(this).closest('tr');

    if(row.hasClass('expanded')) {
      var id = $(this).data('id');

      var iconExp = $('i.fa-chevron-circle-up', dis);
      if(iconExp.length > 0) {
        $('tr[data-parent-id='+id+']').hide();
        dis.html('<i class="far fa-chevron-circle-down"></i>');
      } else {
        $('tr[data-parent-id='+id+']').show();
        dis.html('<i class="far fa-chevron-circle-up"></i>');
      }
      console.log(iconExp);

    } else {
      var nrow = null;
      if(url) {
        $.get(url, function(data) {
          nrow = $(data).insertAfter(row);
          if(nrow) {
            initRowEvents($(nrow));
          }

          row.addClass('expanded');
        });
      }
      dis.html('<i class="far fa-chevron-circle-up"></i>');
    }
    return false;
  });
}

$(document).ready(function() {
  modalForm.on('hidden.bs.modal', function (e) {
    $('.modal-body', modalForm).empty();
    $('.modal-title', modalForm).html('');
  });

  initRowEvents($(document));
});
</script>
