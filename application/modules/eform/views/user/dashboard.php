<?php
$ruser = GetLoggedUser();
?>
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h3 class="m-0 text-dark font-weight-light"><?=$title?></h3>
      </div>
    </div>
  </div>
</div>
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-sm-12">
        <?php
        if($ruser[COL_ROLEID]==ROLEADMIN || $ruser[COL_ROLEID]==ROLEKUISIONER) {
          $rquest = $this->db
          ->select('eform_tquest.*, skpd.UnitNama, uc.Name')
          ->where(COL_QUESTEND.' is not null')
          ->join(TBL__USERINFORMATION.' uc','uc.'.COL_USERNAME." = ".TBL_EFORM_TQUEST.".".COL_CREATEDBY,"left")
          ->join(TBL_EFORM_MUNIT.' skpd','skpd.'.COL_UNITID." = ".TBL_EFORM_TQUEST.".".COL_IDSKPD,"left")
          ->where(COL_QUESTISDELETED, 0)
          ->order_by(TBL_EFORM_TQUEST.".".COL_QUESTEND, 'desc')
          ->order_by("skpd.".COL_UNITNAMA, 'asc')
          ->limit(10)
          ->get(TBL_EFORM_TQUEST)
          ->result_array();
          ?>
          <div class="card card-info">
            <div class="card-header">
              <div class="card-title font-weight-bold">
                DAFTAR KUISIONER TERISI
              </div>
            </div>
            <div class="card-body p-0">
              <table class="table table-hover">
                <thead>
                  <tr>
                    <th>JUDUL KUISIONER</th>
                    <th>SKPD</th>
                    <th>DIISI PADA</th>
                    <th>AKSI</th>
                  </tr>
                </thead>
                <tbody>
                  <?php
                  if(!empty($rquest)) {
                    foreach($rquest as $q) {
                      ?>
                      <tr>
                        <td><?=$q[COL_QUESTNAME]?></td>
                        <td><?=$q[COL_UNITNAMA]?></td>
                        <td class="text-right"><?=date('Y-m-d H:i', strtotime($q[COL_QUESTEND]))?></td>
                        <td style="width: 50px; white-space: nowrap"><a href="<?=site_url('eform/data/result/'.$q[COL_UNIQ])?>" class="btn btn-xs btn-primary btn-edit"><i class="fas fa-arrow-circle-right"></i>&nbsp; LIHAT HASIL</a></td>
                      </tr>
                      <?php
                    }
                  } else {
                    ?>
                    <tr>
                      <td colspan="4" class="text-center font-italic">BELUM ADA DATA UNTUK SAAT INI.</td>
                    </tr>
                    <?php

                  }

                  ?>
                </tbody>
              </table>
            </div>
          </div>
          <?php
        } else {
          $rquest = $this->db
          ->select('eform_tquest.*, skpd.UnitNama, uc.Name')
          ->where(COL_QUESTEND.' is null')
          ->where(COL_IDSKPD, $ruser[COL_SKPDID])
          ->join(TBL__USERINFORMATION.' uc','uc.'.COL_USERNAME." = ".TBL_EFORM_TQUEST.".".COL_CREATEDBY,"left")
          ->join(TBL_EFORM_MUNIT.' skpd','skpd.'.COL_UNITID." = ".TBL_EFORM_TQUEST.".".COL_IDSKPD,"left")
          ->where(COL_QUESTISDELETED, 0)
          ->order_by(TBL_EFORM_TQUEST.".".COL_CREATEDON, 'desc')
          ->order_by("skpd.".COL_UNITNAMA, 'asc')
          ->get(TBL_EFORM_TQUEST)
          ->result_array()
          ?>
          <div class="card card-info">
            <div class="card-header">
              <div class="card-title font-weight-bold">
                DAFTAR KUISIONER YANG BELUM DIISI
              </div>
            </div>
            <div class="card-body p-0">
              <table class="table table-hover">
                <tbody>
                  <?php
                  if(!empty($rquest)) {
                    foreach($rquest as $q) {
                      ?>
                      <tr>
                        <td><?=$q[COL_QUESTNAME]?></td>
                        <td style="width: 50px; white-space: nowrap"><a href="<?=site_url('eform/data/form/'.$q[COL_UNIQ])?>" class="btn btn-xs btn-primary btn-edit"><i class="fas fa-arrow-circle-right"></i>&nbsp; ISI KUISIONER</a></td>
                      </tr>
                      <?php
                    }
                  } else {
                    ?>
                    <tr>
                      <td colspan="2" class="text-center font-italic">BELUM ADA DATA UNTUK SAAT INI.</td>
                    </tr>
                    <?php

                  }

                  ?>
                </tbody>
              </table>
            </div>
          </div>
          <?php
        }
        ?>

      </div>
    </div>
  </div>
</section>
