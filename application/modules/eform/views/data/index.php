<?php $ruser = GetLoggedUser(); ?>
<style>
#datalist_filter {
  text-align: left !important;
  display: inline-block !important;
}
#datalist_filter label {
  font-weight: 700;
}
div.filtering .select2-container {
  display: inline-block !important;
}
div.filtering input.form-control {
  display: inline-block !important;
}
</style>
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-12">
        <h3 class="m-0 text-dark font-weight-light"><?=strtoupper($title)?></h3>
      </div>
    </div>
  </div>
</div>
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-sm-12">
          <div class="card card-default">
            <div class="card-header">
              <div class="card-tools">
                <?php
                if ($ruser[COL_ROLEID]==ROLEADMIN || $ruser[COL_ROLEID]==ROLEKUISIONER) {
                  ?>
                  <a href="<?=site_url('eform/data/add'.(!empty($id)?'/'.$id:''))?>" type="button" class="btn btn-tool btn-add-data text-info"><i class="fas fa-plus"></i>&nbsp;TAMBAH</a>
                  <?php
                }
                ?>
                <button type="button" class="btn btn-tool btn-refresh-data"><i class="fas fa-sync-alt"></i>&nbsp;REFRESH</button>
              </div>
            </div>
            <div class="card-body">
              <form id="dataform" method="post" action="#">
                <table id="datalist" class="table table-bordered table-hover table-condensed">
                  <thead>
                    <tr>
                      <th class="text-center" style="width: 10px">AKSI</th>
                      <th>KUISIONER</th>
                      <th>STATUS</th>
                      <th>DIINPUT PADA</th>
                      <th>DISUBMIT PADA</th>
                    </tr>
                  </thead>
                  <tbody></tbody>
                </table>
              </form>
            </div>
          </div>
      </div>
    </div>
  </div>
</section>
<div id="dom-filter" class="d-none">
  <select class="form-control" name="filterStatus" style="width: 200px">
    <option value="">-- SEMUA STATUS --</option>
    <option value="NEW">BARU</option>
    <option value="COMPLETE">SELESAI</option>
  </select>
  <?php
  if($ruser[COL_ROLEID]!=ROLEADMIN && $ruser[COL_ROLEID]!=ROLEKUISIONER && $ruser[COL_ROLEID]!=ROLEGUEST) {
    $ropd = $this->db
    ->where(COL_UNITID, $ruser[COL_SKPDID])
    ->get(TBL_EFORM_MUNIT)
    ->row_array();
    ?>
    <input type="hidden" name="filterSkpd" value="<?=!empty($ropd)?$ropd[COL_UNITID]:'-'?>" />
    <?php
  } else {
    ?>
    <select class="form-control" name="filterSkpd" style="width: 600px">
      <?=GetCombobox("SELECT * FROM eform_munit where UnitIsAktif=1", COL_UNITID, COL_UNITNAMA, null, true, false, '-- SEMUA SKPD --')?>
    </select>
    <?php
  }
  ?>
</div>
<div class="modal fade" id="modal-editor" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
    </div>
  </div>
</div>
<script type="text/javascript">
$(document).ready(function() {
  var cols = [];
  var coldef = [];
  coldef = [{"targets":[0], "className":'nowrap text-center'}, {"targets":[3,4], "className":'nowrap dt-body-right'}];
  cols = [
    {"orderable": false,"width": "10px"},
    {"orderable": true},
    {"orderable": false},
    {"orderable": true,"width": "50px"},
    {"orderable": true,"width": "50px"}
  ]
  var modalData = $('#modal-editor');

  var dt = $('#datalist').dataTable({
    "autoWidth" : false,
    "processing": true,
    "serverSide": true,
    "ajax": {
      "url": "<?=(!empty($id)?site_url('eform/data/index-load/'.$id):site_url('eform/data/index-load'))?>",
      "type": 'POST',
      "data": function(data){
          /*var dateFrom = $('[name=filterDateFrom]', $('.datefilter')).val();
          var dateTo = $('[name=filterDateTo]', $('.datefilter')).val();
          var idSupplier = $('[name=filterIdSupplier]', $('.datefilter')).val();

          data.dateFrom = dateFrom;
          data.dateTo = dateTo;
          data.idSupplier = idSupplier;*/
          var skpd = $('[name=filterSkpd]', $('.filtering')).val();
          var stat = $('[name=filterStatus]', $('.filtering')).val();
          data.Skpd = skpd;
          data.Status = stat;
       }
    },
    "scrollY" : '40vh',
    "scrollX": "200%",
    "iDisplayLength": 100,
    //"aLengthMenu": [[100, 1000, 5000, -1], [100, 1000, 5000, "Semua"]],
    //"dom":"R<'row'<'col-sm-8'l><'col-sm-4'f>><'row'<'col-sm-12'tr>><'row'<'col-sm-5'i><'col-sm-7'p>>",
    "dom":"R<'row'<'col-sm-12 d-flex'f<'filtering'>>><'row'<'col-sm-12'tr>><'row'<'col-sm-4'l><'col-sm-4'i><'col-sm-4'p>><'clear'>",
    //"buttons": ['copyHtml5','excelHtml5','csvHtml5','pdfHtml5'],
    "oLanguage": {
      "sSearch": "FILTER "
    },
    "order": [[ 3, "desc" ]],
    "columnDefs": coldef,
    "columns": cols,
    "createdRow": function(row, data, dataIndex) {
      $('.btn-action', $(row)).click(function() {
        var url = $(this).attr('href');
        var confirmtxt = ($(this).data('confirm') || 'Apakah anda yakin?');
        if(confirm(confirmtxt)) {
          $.get(url, function(res) {
            if(res.error != 0) {
              toastr.error(res.error);
            } else {
              toastr.success(res.success);
            }
          }, "json").done(function() {
            dt.DataTable().ajax.reload();
          }).fail(function() {
            toastr.error('SERVER ERROR');
          });
        }
        return false;
      });
      $('.btn-view', $(row)).click(function() {
        var href = $(this).attr('href');
        $('.modal-content', modalData).load(href, function() {
          modalData.modal('show');
        });
        return false;
      });
    },
    "initComplete": function(settings, json) {
      $('input[type=search]', $('#datalist_filter')).removeClass('form-control-sm').attr('placeholder', 'Keyword');
    }
  });
  $("div.filtering").html($('#dom-filter').html()).addClass('d-inline-block ml-2');

  $('.btn-refresh-data').click(function() {
    dt.DataTable().ajax.reload();
  });
  $('input,select', $("div.filtering")).change(function() {
    dt.DataTable().ajax.reload();
  });

  $('.btn-add-data').click(function() {
    var href = $(this).attr('href');
    $('.modal-content', modalData).load(href, function() {
      $("select", modalData).not('.no-select2, .custom-select').select2({ width: 'resolve', theme: 'bootstrap4' });
      modalData.modal('show');

      $('form', modalData).validate({
        submitHandler: function(form) {
          var btnSubmit = $('button[type=submit]', $(form));
          var txtSubmit = btnSubmit[0].innerHTML;
          btnSubmit.html('<i class="fad fa-circle-notch fa-spin"></i>');
          $(form).ajaxSubmit({
            url: href,
            dataType: 'json',
            type : 'post',
            success: function(res) {
              if(res.error != 0) {
                toastr.error(res.error);
              } else {
                toastr.success(res.success);
                dt.DataTable().ajax.reload();
                modalData.modal('hide');
              }
            },
            error: function() {
              toastr.error('SERVER ERROR');
            },
            complete: function() {
              btnSubmit.html(txtSubmit);
            }
          });
          return false;
        }
      });
    });
    return false;
  });
});
</script>
