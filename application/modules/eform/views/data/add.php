<form id="form-editor" method="post" action="#">
<div class="modal-header">
  <h5 class="modal-title">BUAT KUISIONER</h5>
  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
      <span aria-hidden="true"><i class="fa fa-close"></i></span>
  </button>
</div>
<div class="modal-body">
  <div class="form-group">
    <label>FORMAT</label>
    <?php
    $rcat = $this->db->where(COL_UNIQ, $id)->get(TBL_EFORM_MQUEST)->row_array();
    if(!empty($id) || empty($rcat)) {
      ?>
      <input type="hidden" name="<?=COL_IDQUEST?>" value="<?=$id?>" />
      <p class="font-italic"><?=$rcat[COL_QUESTNAME]?></p>
      <?php
    } else {
      ?>
      <select class="form-control" name="<?=COL_IDQUEST?>" style="width: 100%" required>
        <?=GetCombobox("SELECT * FROM eform_mquest ORDER BY QuestName", COL_UNIQ, COL_QUESTNAME)?>
      </select>
      <?php
    }
    ?>
  </div>
  <div class="form-group">
    <label>SKPD</label>
    <?php
    if(empty($ropd)) {
      ?>
      <select class="form-control" name="<?=COL_IDSKPD?>" style="width: 100%">
        <?=GetCombobox("SELECT * FROM eform_munit where UnitIsAktif = 1 ORDER BY UnitNama", COL_UNITID, COL_UNITNAMA, null, true, false, '-- SEMUA SKPD --')?>
      </select>
      <?php
    } else {
      ?>
      <input type="hidden" name="<?=COL_IDSKPD?>" value="<?=$ropd[COL_SKPDID]?>" />
      <input type="text" class="form-control" value="<?=$ropd[COL_SKPDNAMA]?>" readonly />
      <?php
    }
    ?>
  </div>
  <div class="form-group">
    <label>JUDUL</label>
    <textarea class="form-control" name="<?=COL_QUESTNAME?>"></textarea>
  </div>
</div>
<div class="modal-footer">
  <button type="button" class="btn btn-outline-danger" data-dismiss="modal"><i class="far fa-times-circle"></i>&nbsp;BATAL</button>
  <button type="submit" class="btn btn-outline-primary btn-ok"><i class="far fa-arrow-circle-right"></i>&nbsp;LANJUT</button>
</div>
</form>
