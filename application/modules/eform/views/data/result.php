<?php
$badgeStat = 'badge-secondary';
$badgeEval = '';

if(!empty($rquest[COL_QUESTEND])) $badgeStat = 'badge-success';
if($rquest[COL_QUESTCATEGORY]='PEKPPP') {
  if($rquest['QuestDet'] == $rquest['QuestEval']) {
    $badgeEval = '<span class="badge badge-info">FINAL</span>';
  } else if($rquest['QuestEval']>0) {
    $badgeEval = '<span class="badge badge-info">EVALUASI</span>';
  }
}
?>
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h3 class="m-0 text-dark font-weight-light"><?=$title?></h3>
      </div>
      <div class="col-sm-6 text-right">
        <a href="<?=site_url('eform/data/index')?>" class="btn btn-sm btn-secondary"><i class="far fa-arrow-circle-left"></i> KEMBALI</a>&nbsp;
        <a href="<?=site_url('eform/data/review/'.$rquest[COL_UNIQ])?>" class="btn btn-sm btn-info"><i class="far fa-info-circle"></i> LIHAT ISIAN</a>
      </div>
    </div>
  </div>
</div>
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-sm-12">
        <div class="card">
          <div class="card-body p-0">
            <table class="table table-bordered">
              <tbody>
                <tr>
                  <td class="nowrap" style="width: 10px">JUDUL</td>
                  <td class="nowrap" style="width: 10px">:</td>
                  <td class="font-weight-bold"><?=$rquest[COL_QUESTNAME]?></td>
                </tr>
                <tr>
                  <td class="nowrap" style="width: 10px">SKPD</td>
                  <td class="nowrap" style="width: 10px">:</td>
                  <td class="font-weight-bold"><?=$rquest[COL_UNITNAMA]?></td>
                </tr>
                <tr>
                  <td class="nowrap" style="width: 10px">STATUS</td>
                  <td class="nowrap" style="width: 10px">:</td>
                  <td class="font-weight-bold"><?='<span class="badge '.$badgeStat.'">'.(!empty($rquest[COL_QUESTEND])?'SELESAI':'DRAFT').'</span> '.$badgeEval?></td>
                </tr>
                <?php
                if($rquest[COL_QUESTCATEGORY]=='PEKPPP' && !empty($rquest[COL_QUESTEND])) {
                  ?>
                  <tr>
                    <td class="nowrap" style="width: 10px">DISUBMIT PADA</td>
                    <td class="nowrap" style="width: 10px">:</td>
                    <td class="font-weight-bold"><?=date('Y-m-d H:i', strtotime($rquest[COL_QUESTEND]))?></td>
                  </tr>
                  <?php
                }
                ?>
              </tbody>
            </table>
          </div>
        </div>

        <?php
        if($rquest[COL_QUESTCATEGORY]=='EVKIP') {
          $rcategory = $this->db
          ->order_by(COL_UNIQ, 'asc')
          ->group_by(COL_EVKIPCATEGORY)
          ->get(TBL_EFORM_EVKIPSKOR)
          ->result_array();
          ?>
          <div class="card">
            <div class="card-body p-0">
              <table class="table table-bordered">
                <thead>
                  <tr>
                    <th>DIMENSI</th>
                    <th>SKOR</th>
                    <th>DEVIASI</th>
                  </tr>
                </thead>
                <tbody>
                  <?php
                  $grand = 0;
                  foreach($rcategory as $cat) {
                    $sum = 0;
                    $summaks = 0;
                    $rcategorysub = $this->db
                    ->where(COL_EVKIPCATEGORY, $cat[COL_EVKIPCATEGORY])
                    ->order_by(COL_UNIQ, 'asc')
                    ->group_by(COL_EVKIPCATEGORYSUB)
                    ->get(TBL_EFORM_EVKIPSKOR)
                    ->result_array();
                    ?>
                    <tr>
                      <td colspan="3" class="font-weight-bold bg-info"><?=$cat[COL_EVKIPCATEGORY]?></td>
                    </tr>
                    <?php
                    foreach($rcategorysub as $sub) {
                      $rskor = $this->db
                      ->select_sum(COL_QUESTSCORE)
                      ->where(COL_IDQUEST, $rquest[COL_UNIQ])
                      ->where(COL_QUESTCATEGORY, $sub[COL_EVKIPCATEGORY])
                      ->where(COL_QUESTCATEGORYSUB, $sub[COL_EVKIPCATEGORYSUB])
                      ->order_by(COL_QUESTNO, 'asc')
                      ->get(TBL_EFORM_TQUESTDETAIL)
                      ->row_array();
                      $sum += $rskor[COL_QUESTSCORE];
                      $summaks += $sub[COL_EVKIPSKORMAKS];
                      $grand += $rskor[COL_QUESTSCORE];
                      ?>
                      <tr>
                        <td><?=$sub[COL_EVKIPCATEGORYSUB]?></td>
                        <td class="text-right"><?=number_format($rskor[COL_QUESTSCORE],3)?></td>
                        <td class="text-right"><?=number_format((($sub[COL_EVKIPSKORMAKS]-$rskor[COL_QUESTSCORE])/$sub[COL_EVKIPSKORMAKS])*100)?>%</td>
                      </tr>
                      <?php
                    }
                    ?>
                    <tr>
                      <td class="font-weight-bold">TOTAL</td>
                      <td class="text-right font-weight-bold"><?=number_format($sum,3)?></td>
                      <td class="text-right"><strong><?=number_format((($summaks-$sum)/$summaks)*100)?></strong>%</td>
                    </tr>
                    <?php
                  }

                  $rsummary = $this->db
                  ->where(COL_EVKIPSKORFROM.' <= ', $grand)
                  ->order_by(COL_EVKIPSKORFROM, 'desc')
                  ->get(TBL_EFORM_EVKIPSUMMARY)
                  ->row_array();
                  ?>
                  <tr>
                    <td class="text-right font-weight-bold" style="vertical-align: middle">NILAI KOMPOSIT</td>
                    <td class="text-right font-weight-bold" colspan="2"><?=number_format($grand,3)?></td>
                  </tr>
                  <?php
                  if(!empty($rsummary)) {
                    ?>
                    <tr>
                      <td class="text-right font-weight-bold" style="vertical-align: middle">PREDIKAT</td>
                      <td class="text-left font-weight-bold" colspan="2"><?=$rsummary[COL_EVKIPSKORSUMMARY]?></td>
                    </tr>
                    <tr>
                      <td class="text-right font-weight-bold" style="vertical-align: middle">KONDISI DIMENSI STRUKTUR DAN PROSES</td>
                      <td class="text-left font-weight-bold" colspan="2"><?=$rsummary[COL_EVKIPSKORREMARKS2]?></td>
                    </tr>
                    <tr>
                      <td class="text-right font-weight-bold" style="vertical-align: middle">KEMAMPUAN AKOMODASI KEBUTUHAN INTERNAL DAN ADAPTASI LINGKUNGAN EKSTERNAL</td>
                      <td class="text-left font-weight-bold" colspan="2"><?=$rsummary[COL_EVKIPSKORREMARKS3]?></td>
                    </tr>
                    <tr>
                      <td class="text-right font-weight-bold" style="vertical-align: middle">KEKURANGAN</td>
                      <td class="text-left font-weight-bold" colspan="2"><?=$rsummary[COL_EVKIPSKORREMARKS4]?></td>
                    </tr>
                    <tr>
                      <td class="text-right font-weight-bold" style="vertical-align: middle">KETERANGAN</td>
                      <td class="text-justify" colspan="2" style="max-width: 300px">
                        <?=$rsummary[COL_EVKIPSKORREMARKS]?>
                      </td>
                    </tr>
                    <?php
                  }
                  ?>
                </tbody>
              </table>
            </div>
          </div>
          <?php
        } else if($rquest[COL_QUESTCATEGORY]=='PEKPPP') {
          $rcategory = $this->db
          ->select('QuestCategory, SUM(QuestScore) as QuestScore, SUM(QuestEvaluationScore) as QuestEvaluationScore, COUNT(*) as QuestNum')
          ->where(COL_IDQUEST, $rquest[COL_UNIQ])
          ->order_by(COL_QUESTNO, 'asc')
          ->group_by(COL_QUESTCATEGORY)
          ->get(TBL_EFORM_TQUESTDETAIL)
          ->result_array();
          ?>
          <div class="card">
            <div class="card-body p-0">
              <table class="table table-striped table-bordered">
                <thead>
                  <tr>
                    <th style="text-align: left; vertical-align: middle">ASPEK</th>
                    <th style="text-align: center; width: 100px; vertical-align: middle">Jlh. Pertanyaan</th>
                    <th style="text-align: center; width: 100px; vertical-align: middle">Skor Penilaian Mandiri</th>
                    <th style="text-align: center; width: 100px; vertical-align: middle">Skor Evaluasi</th>
                    <th style="text-align: center; width: 100px; vertical-align: middle">Nilai</th>
                  </tr>
                </thead>
                <tbody>
                  <?php
                  $sumVal = 0;
                  foreach($rcategory as $cat) {
                    $val = 0;
                    $rbobot = $this->db
                    ->where(COL_PEKPPP_ASPEK, $cat[COL_QUESTCATEGORY])
                    ->get(TBL_EFORM_PEKPPPMATRIKS)
                    ->row_array();
                    if(!empty($rbobot) && !empty($cat[COL_QUESTEVALUATIONSCORE])) {
                      $val = $cat[COL_QUESTEVALUATIONSCORE]*$rbobot[COL_PEKPPP_BOBOT];
                      $sumVal += $val;
                    }
                    ?>
                    <tr>
                      <td><?=$cat[COL_QUESTCATEGORY]?><span class="text-sm font-italic pull-right">Bobot: <?=!empty($rbobot)?number_format($rbobot[COL_PEKPPP_BOBOT]*100).'%':'-'?></span></td>
                      <td class="text-center"><?=number_format($cat['QuestNum'])?></td>
                      <td class="text-center"><?=number_format($cat[COL_QUESTSCORE])?></td>
                      <td class="text-center"><?=!empty($cat[COL_QUESTEVALUATIONSCORE])?number_format($cat[COL_QUESTEVALUATIONSCORE]):'-'?></td>
                      <td class="text-center font-weight-bold"><?=!empty($val)?number_format($val,2):'-'?></td>
                    </tr>
                    <?php
                  }
                  ?>
                </tbody>
                <tfoot>
                  <tr>
                    <th colspan="4" class="text-right">TOTAL</th>
                    <td class="text-center font-weight-bold"><?=!empty($sumVal)?number_format($sumVal,2):'-'?></td>
                  </tr>
                </tfoot>
              </table>
            </div>
          </div>
          <?php
        }
        ?>
      </div>
    </div>
  </div>
</section>
