<?php
$ruser = GetLoggedUser();
$rcategory = $this->db
->where(COL_IDQUEST, $rquest[COL_UNIQ])
->order_by(COL_QUESTNO, 'asc')
->group_by(COL_QUESTCATEGORY)
->get(TBL_EFORM_TQUESTDETAIL)
->result_array();
?>
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h3 class="m-0 text-dark font-weight-light"><?=$title?></h3>
      </div>
      <div class="col-sm-6 text-right">
        <a href="<?=site_url('eform/data/index/'.$rquest[COL_IDQUEST])?>" class="btn btn-sm btn-secondary"><i class="far fa-arrow-circle-left"></i> KEMBALI</a>
        <!--<button type="button" class="btn btn-sm btn-primary <?=!$isEditable?'disabled':'btn-submit'?>" data-mode="save" data-prompt="Anda yakin ingin menyimpan Kuisioner ini? Kuisioner yang disimpan akan tetap memiliki status sebagai DRAFT."><i class="far fa-save"></i> SIMPAN</button>-->
        <?php
        if($ruser[COL_ROLEID]==ROLEADMIN||$ruser[COL_ROLEID]==ROLEKUISIONER) {
          ?>
          <button type="button" class="btn btn-sm btn-primary btn-submit" data-mode="save" data-prompt="Anda yakin ingin menyimpan perubahan di Kuisioner ini?"><i class="far fa-save"></i> SIMPAN</button>
          <?php
        } else {
          ?>
          <button type="button" class="btn btn-sm btn-primary <?=!$isEditable?'disabled':'btn-submit'?>" data-mode="save" data-prompt="Anda yakin ingin menyimpan perubahan di Kuisioner ini?"><i class="far fa-save"></i> SIMPAN</button>
          <button type="button" class="btn btn-sm btn-success <?=!$isEditable?'disabled':'btn-submit'?>" data-mode="submit" data-prompt="Anda yakin ingin mengirimkan Kuisioner ini? Kuisioner yang sudah dikirimkan akan diteruskan ke Administrator."><i class="far fa-check-circle"></i> SUBMIT</button>
          <?php
        }
        ?>
      </div>
    </div>
  </div>
</div>
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-sm-12">
        <div class="card">
          <div class="card-body p-0">
            <table class="table table-bordered">
              <tbody>
                <tr>
                  <td class="nowrap" style="width: 10px">JUDUL</td>
                  <td class="nowrap" style="width: 10px">:</td>
                  <td class="font-weight-bold"><?=$rquest[COL_QUESTNAME]?></td>
                </tr>
                <tr>
                  <td class="nowrap" style="width: 10px">SKPD</td>
                  <td class="nowrap" style="width: 10px">:</td>
                  <td class="font-weight-bold"><?=$rquest[COL_UNITNAMA]?></td>
                </tr>
                <?php
                if(!empty($rquest[COL_QUESTEND])) {
                  ?>
                  <tr>
                    <td class="nowrap" style="width: 10px">DISUBMIT PADA</td>
                    <td class="nowrap" style="width: 10px">:</td>
                    <td class="font-weight-bold"><?=date('Y-m-d H:i', strtotime($rquest[COL_QUESTEND]))?></td>
                  </tr>
                  <?php
                }
                ?>
              </tbody>
            </table>
          </div>
        </div>
        <form id="form-quest" action="<?=site_url('eform/data/form/'.$rquest[COL_UNIQ])?>" method="post">
          <input type="hidden" name="mode" value="save" />
          <?php
          if($rquest[COL_QUESTCATEGORY]=='EVKIP' || $rquest[COL_QUESTCATEGORY]=='PP' || $rquest[COL_QUESTCATEGORY]=='PEKPPP') {
            if(!empty($rcategory)) {
              foreach($rcategory as $cat) {
                $rsub = $this->db
                ->where(TBL_EFORM_TQUESTDETAIL.'.'.COL_IDQUEST, $rquest[COL_UNIQ])
                ->where(TBL_EFORM_TQUESTDETAIL.'.'.COL_QUESTCATEGORY, $cat[COL_QUESTCATEGORY])
                ->order_by(TBL_EFORM_TQUESTDETAIL.'.'.COL_QUESTNO, 'asc')
                ->group_by(TBL_EFORM_TQUESTDETAIL.'.'.COL_QUESTCATEGORYSUB)
                ->get(TBL_EFORM_TQUESTDETAIL)
                ->result_array();
                ?>
                <div class="card <?=!$cat[COL_QUESTISPRE]?'collapsed-card':''?>">
                  <div class="card-header">
                    <h5 class="card-title font-weight-bold"><?=!empty($cat[COL_QUESTCATEGORY])?$cat[COL_QUESTCATEGORY]:'PERTANYAAN'?></h5>
                    <?php
                    if(!$cat[COL_QUESTISPRE]) {
                      ?>
                      <div class="card-tools">
                        <button type="button" class="btn btn-tool text-info" data-card-widget="collapse"><i class="fas fa-plus"></i></button>
                      </div>
                      <?php
                    }
                    ?>
                  </div>
                  <div class="card-body p-0">
                    <table class="table table-bordered">
                      <tbody>
                        <?php
                        foreach($rsub as $sub) {
                          $rquestdet = $this->db
                          ->select('eform_mquestdetail.QuestRemarks, eform_mquestdetail.QuestRemarks2, eform_tquestdetail.*')
                          ->join(TBL_EFORM_TQUEST,TBL_EFORM_TQUEST.'.'.COL_UNIQ." = ".TBL_EFORM_TQUESTDETAIL.".".COL_IDQUEST,"left")
                          ->join(TBL_EFORM_MQUESTDETAIL,TBL_EFORM_MQUESTDETAIL.'.'.COL_IDQUEST." = ".TBL_EFORM_TQUEST.".".COL_IDQUEST." and ".TBL_EFORM_MQUESTDETAIL.".".COL_QUESTNO." = ".TBL_EFORM_TQUESTDETAIL.".".COL_QUESTNO,"left")
                          ->where(TBL_EFORM_TQUESTDETAIL.'.'.COL_IDQUEST, $rquest[COL_UNIQ])
                          ->where(TBL_EFORM_TQUESTDETAIL.'.'.COL_QUESTCATEGORY, $cat[COL_QUESTCATEGORY])
                          ->where(TBL_EFORM_TQUESTDETAIL.'.'.COL_QUESTCATEGORYSUB, $sub[COL_QUESTCATEGORYSUB])
                          ->order_by(TBL_EFORM_TQUESTDETAIL.'.'.COL_QUESTNO, 'asc')
                          ->get(TBL_EFORM_TQUESTDETAIL)
                          ->result_array();
                          ?>
                          <?php
                          if(!empty($sub[COL_QUESTCATEGORYSUB])) {
                            ?>
                            <tr>
                              <td colspan="4" class="font-weight-bold"><?=$sub[COL_QUESTCATEGORYSUB]?></td>
                            </tr>
                            <?php
                          }
                          ?>

                          <?php
                          foreach($rquestdet as $d) {
                            ?>
                            <tr>
                              <td class="text-right" style="width: 10px"><?=$d[COL_QUESTNO]?>.</td>
                              <td <?=$d[COL_QUESTTYPE]=='TEXT'&&(!$d[COL_QUESTISPRE])?'colspan="2"':''?>>
                                <?=$d[COL_QUESTTEXT]?>
                                <?php
                                if($d[COL_QUESTTYPE]=='TEXT') {
                                  if($d[COL_QUESTISPRE]) {

                                  } else {
                                    ?>
                                    <div class="form-group mt-2">
                                      <label>JAWABAN</label>
                                      <textarea name="QuestResponse__<?=$d[COL_UNIQ]?>" class="form-control" <?=!$isEditable?'disabled':''?>><?=!empty($d[COL_QUESTRESPONSE])?$d[COL_QUESTRESPONSE]:''?></textarea>
                                    </div>
                                    <?php
                                    if(($ruser[COL_ROLEID]==ROLEADMIN || $ruser[COL_ROLEID]==ROLEKUISIONER) && !empty($rquest[COL_QUESTEND])) {
                                      ?>
                                      <div class="form-group mt-2">
                                        <label>PENILAIAN <small><a href="#" data-name="btn-refresh" data-url="<?=site_url('eform/data/score/'.$d[COL_UNIQ])?>">refresh</a></small></label>
                                        <div class="row">
                                          <div class="col-sm-8">
                                            <table  class="table table-bordered table-condensed" data-name="table-score" data-url="<?=site_url('eform/data/score/'.$d[COL_UNIQ])?>">
                                              <thead>
                                                <tr>
                                                  <th>Penilai</th>
                                                  <th style="width: 100px; white-space: nowrap">Skor</th>
                                                  <th style="width: 10px; white-space: nowrap">Waktu Penilaian</th>
                                                </tr>
                                              </thead>
                                              <tbody>

                                              </tbody>
                                              <tfoot>
                                                <tr>
                                                  <td class="text-right"><label class="control-label mb-0">Penilaian Anda</label></th>
                                                  <th colspan="2" style="width: 100px; white-space: nowrap">
                                                    <div class="input-group">
                                                      <input type="text" class="form-control form-control-sm text-right" data-url="<?=site_url('eform/data/score-add/'.$d[COL_UNIQ])?>" />
                                                      <span class="input-group-append">
                                                        <a href="<?=site_url('eform/data/score-add/'.$d[COL_UNIQ])?>" data-name="btn-score" data-target="<?=site_url('eform/data/score/'.$d[COL_UNIQ])?>" class="btn btn-sm btn-primary"><i class="far fa-check-circle"></i></a>
                                                      </span>
                                                    </div>
                                                  </th>
                                                </tr>
                                              </tfoot>
                                            </table>
                                          </div>
                                        </div>

                                        <!--<div class="row">
                                          <div class="col-sm-3">
                                            <input type="text" class="form-control uang text-right" value="<?=!empty($d[COL_QUESTEVALUATION])?$d[COL_QUESTEVALUATION]:''?>" />
                                          </div>
                                        </div>-->
                                        <!--<textarea name="QuestEvaluation__<?=$d[COL_UNIQ]?>" class="form-control"></textarea>-->
                                      </div>
                                      <?php
                                    }
                                  }
                                } else if($d[COL_QUESTTYPE]=='MUL') {
                                  $arropt = json_decode($d[COL_QUESTOPTION]);
                                  ?>
                                  <div class="mt-4">
                                    <?php
                                    foreach($arropt as $opt) {
                                      $labelFor = 'QuestResponse__'.$d[COL_UNIQ].'_'.$opt->Opt;
                                      ?>
                                      <div class="row align-items-top">
                                        <div class="ml-2">
                                          <input type="radio" id="QuestResponse__<?=$d[COL_UNIQ]?>_<?=$opt->Opt?>" name="QuestResponse__<?=$d[COL_UNIQ]?>" value="<?=$opt->Opt?>" <?=!$isEditable?'disabled':''?> <?=isset($d[COL_QUESTRESPONSE])&&$d[COL_QUESTRESPONSE]==$opt->Opt?'checked':''?> />
                                        </div>
                                        <?php
                                        if($rquest[COL_QUESTCATEGORY]=='PEKPPP' && !empty($rquest[COL_QUESTEND])) {
                                          $labelFor = 'QuestEvaluation__'.$d[COL_UNIQ].'_'.$opt->Opt;
                                          ?>
                                          <div class="ml-2">
                                            <input type="radio" id="QuestEvaluation__<?=$d[COL_UNIQ]?>_<?=$opt->Opt?>" name="QuestEvaluation__<?=$d[COL_UNIQ]?>" value="<?=$opt->Opt?>" <?=isset($d[COL_QUESTEVALUATION])&&$d[COL_QUESTEVALUATION]==$opt->Opt?'checked':''?> />
                                          </div>
                                          <?php
                                        }
                                        ?>
                                        <div class="col-sm-10">
                                          <label for="<?=$labelFor?>" class="text-sm"><?=$opt->Txt?></label>
                                        </div>
                                      </div>
                                      <?php
                                    }
                                    ?>
                                  </div>
                                  <?php
                                }
                                ?>
                                <?php
                                if(!empty($d[COL_QUESTREMARKS])) {
                                  ?>
                                  <p class="mt-4 font-italic text-sm">Keterangan: <br /><?=$d[COL_QUESTREMARKS]?></p>
                                  <?php
                                }

                                if(!empty($d[COL_QUESTREMARKS2])) {
                                  $rremarks = json_decode($d[COL_QUESTREMARKS2]);
                                  ?>
                                  <div class="card card-primary">
                                    <div class="card-header">
                                      <h4 class="card-title">
                                        <a data-toggle="collapse" href="#collapse_<?=$d[COL_UNIQ]?>">
                                          <i class="far fa-info-circle"></i>&nbsp;<?=isset($rremarks->Title)?$rremarks->Title:'Rincian'?>
                                        </a>
                                      </h4>
                                    </div>
                                    <div id="collapse_<?=$d[COL_UNIQ]?>" class="panel-collapse in collapse">
                                      <div class="card-body p-2 pt-3 text-sm">
                                        <?php
                                        if(isset($rremarks->Text)) {
                                          ?>
                                          <p><?=$rremarks->Text?></p>
                                          <?php
                                        }

                                        if(isset($rremarks->Items) && is_array($rremarks->Items)) {
                                          ?>
                                          <ol>
                                            <?php
                                            foreach($rremarks->Items as $i) {
                                              ?>
                                              <li><?=$i?></li>
                                              <?php
                                            }
                                            ?>
                                          </ol>
                                          <?php
                                        }
                                        ?>
                                      </div>
                                    </div>
                                  </div>
                                  <?php
                                }
                                ?>

                              </td>
                              <?php
                              if($d[COL_QUESTISPRE]) {
                                ?>
                                <td style="width: 10px; white-space: nowrap">:</td>
                                <td>
                                  <input name="QuestResponse__<?=$d[COL_UNIQ]?>" class="form-control form-control-sm" <?=!$isEditable?'disabled':''?> value="<?=!empty($d[COL_QUESTRESPONSE])?$d[COL_QUESTRESPONSE]:''?>" />
                                </td>
                                <?php
                              }
                              ?>
                              <?php
                              if($d[COL_QUESTTYPE]=='MUL' && false) {
                                $arropt = json_decode($d[COL_QUESTOPTION]);
                                ?>
                                <td>
                                  <!--<select name="QuestResponse__<?=$d[COL_UNIQ]?>" style="width: 100%" <?=!$isEditable?'disabled':''?>>
                                    <?php
                                    foreach($arropt as $opt) {
                                      ?>
                                      <option value="<?=$opt->Opt?>" <?=!empty($d[COL_QUESTRESPONSE])&&$d[COL_QUESTRESPONSE]==$opt->Opt?'selected':''?>><?=$opt->Txt?></option>
                                      <?php
                                    }
                                    ?>
                                  </select>-->
                                  <?php
                                  foreach($arropt as $opt) {
                                    ?>
                                    <div class="row align-items-top">
                                      <div class="ml-2">
                                        <input type="radio" id="QuestResponse__<?=$d[COL_UNIQ]?>_<?=$opt->Opt?>" name="QuestResponse__<?=$d[COL_UNIQ]?>" value="<?=$opt->Opt?>" <?=!$isEditable?'disabled':''?> <?=!empty($d[COL_QUESTRESPONSE])&&$d[COL_QUESTRESPONSE]==$opt->Opt?'checked':''?> />
                                      </div>
                                      <div class="col-sm-10">
                                        <label for="QuestResponse__<?=$d[COL_UNIQ]?>_<?=$opt->Opt?>" class="text-sm"><?=$opt->Txt?></label>
                                      </div>
                                    </div>
                                    <?php
                                  }
                                  ?>
                                </td>
                                <?php
                              }
                              ?>
                              <?php
                              if(!$d[COL_QUESTISPRE]) {
                                ?>
                                <td>
                                  <textarea name="QuestFile__<?=$d[COL_UNIQ]?>" class="form-control" <?=!$isEditable?'disabled':''?> placeholder="Link bukti dukung / Google Drive" style="width: 300px !important" rows="4"><?=!empty($d[COL_QUESTFILE])?$d[COL_QUESTFILE]:''?></textarea>
                                </td>
                                <?php
                              }
                              ?>
                            </tr>
                            <?php
                          }
                        }
                        ?>
                      </tbody>
                    </table>
                  </div>
                </div>
                <?php
              }
            }
          } else if($rquest[COL_QUESTCATEGORY]=='AKPD') {
            ?>
            <div class="card collapsed-card">
              <div class="card-header">
                <h5 class="card-title font-weight-bold"><?=!empty($cat[COL_QUESTCATEGORY])?$cat[COL_QUESTCATEGORY]:'PERTANYAAN'?></h5>
                <div class="card-tools">
                  <button type="button" class="btn btn-tool text-info" data-card-widget="collapse"><i class="fas fa-plus"></i></button>
                </div>
              </div>
              <div class="card-body p-0">
                <table class="table table-bordered">
                  <tbody>
                    <?php
                    $rquestdet = $this->db
                    ->where(COL_IDQUEST, $rquest[COL_UNIQ])
                    ->order_by(COL_QUESTNO, 'asc')
                    ->get(TBL_EFORM_TQUESTDETAIL)
                    ->result_array();
                    ?>
                    <?php
                    $n=0;
                    foreach($rquestdet as $d) {
                      $arropt = array();
                      $rowspan = 1;

                      if($d[COL_QUESTTYPE]=='MUL') {
                        $arropt = json_decode($d[COL_QUESTOPTION]);
                        $rowspan = count($arropt);
                      }
                      ?>
                      <tr>
                        <td class="text-right" style="width: 10px" <?=$rowspan>1?'rowspan="'.($rowspan+1).'"':''?>><?=$d[COL_QUESTNO]?>.</td>
                        <td colspan="2">
                          <?=$d[COL_QUESTTEXT]?>
                          <?php
                          if($d[COL_QUESTTYPE]=='TEXT') {
                            ?>
                            <div class="form-group mt-2">
                              <label>JAWABAN</label>
                              <textarea name="QuestResponse__<?=$d[COL_UNIQ]?>" class="form-control" <?=!$isEditable?'disabled':''?>><?=!empty($d[COL_QUESTRESPONSE])?$d[COL_QUESTRESPONSE]:''?></textarea>
                            </div>
                            <?php
                            if($ruser[COL_ROLEID]==ROLEADMIN && !empty($rquest[COL_QUESTEND])) {
                              ?>
                              <div class="form-group mt-2">
                                <label>PENILAIAN TIM</label>
                                <textarea name="QuestEvaluation__<?=$d[COL_UNIQ]?>" class="form-control"><?=!empty($d[COL_QUESTEVALUATION])?$d[COL_QUESTEVALUATION]:''?></textarea>
                              </div>
                              <?php
                            }
                          }
                          ?>
                        </td>
                        <td <?=$rowspan>1?'rowspan="'.($rowspan+1).'"':''?>>
                          <textarea name="QuestFile__<?=$d[COL_UNIQ]?>" class="form-control" <?=!$isEditable?'disabled':''?> placeholder="Link bukti dukung / Google Drive" style="width: 300px !important" rows="4"><?=!empty($d[COL_QUESTFILE])?$d[COL_QUESTFILE]:''?></textarea>
                        </td>
                      </tr>
                      <?php
                      $opt_ = 0;
                      foreach($arropt as $opt) {
                        ?>
                        <tr <?=!empty($rquest[COL_QUESTEND])&&!empty($d[COL_QUESTRESPONSE])&&$d[COL_QUESTRESPONSE]==$opt->Opt?'class="bg-success"':''?>>
                          <td style="vertical-align: middle">
                            <input type="radio" value="<?=$opt->Opt?>" <?=!empty($d[COL_QUESTRESPONSE])&&$d[COL_QUESTRESPONSE]==$opt->Opt?'checked':''?> id="chk_<?=$d[COL_UNIQ]?>_<?=$opt_?>" name="QuestResponse__<?=$d[COL_UNIQ]?>" <?=!$isEditable?'disabled':''?> />
                          </td>
                          <td>
                            <label for="chk_<?=$d[COL_UNIQ]?>_<?=$opt_?>" class="mb-0"><?=$opt->Opt?></label><br />
                            <label for="chk_<?=$d[COL_UNIQ]?>_<?=$opt_?>" class="font-weight-normal font-italic mb-0"><?=$opt->Txt?></label>
                          </td>
                        </tr>
                        <?php
                        $opt_++;
                      }
                      $n++;
                    }
                    ?>
                  </tbody>
                </table>
              </div>
            </div>
            <?php
          }
          ?>
          <?php
          if(!empty($rquest[COL_QUESTEND])) {
            ?>
            <div class="card">
              <div class="card-body p-0">
                <div class="card-header">
                  <h5 class="card-title font-weight-bold">CATATAN</h5>
                </div>
                <div class="card-body p-0">
                  <textarea name="QuestRemarks" class="form-control" <?=$ruser[COL_ROLEID]!=ROLEADMIN&&!$isEditable?'disabled':''?> placeholder="Catatan Akhir Penilai" style="width: 100% !important" rows="4"><?=!empty($rquest[COL_QUESTREMARKS])?$rquest[COL_QUESTREMARKS]:''?></textarea>
                </div>
              </div>
            </div>
            <?php
          }
          ?>

        </form>
      </div>
    </div>
  </div>
</section>
<script type="text/javascript">
function refreshScore(el, url) {
  $(el).html('<tr><td style="text-align: center;">Loading...</td></tr>')
  $(el).load(url, function(){});
}

$(document).ready(function(){
  $('#form-quest').validate({
    submitHandler: function(form) {
      var btnSubmit = $('button.btn-submit', form);
      btnSubmit.attr('disabled', true);

      $(form).ajaxSubmit({
        dataType: 'json',
        type : 'post',
        success: function(res) {
          if(res.error != 0) {
            toastr.error(res.error);
          } else {
            toastr.success(res.success);
            if(res.data.redirect) {
              setTimeout(function(){
                location.href = res.data.redirect;
              }, 1000);
            }

          }
        },
        error: function() {
          toastr.error('SERVER ERROR');
        },
        complete: function() {
          btnSubmit.attr('disabled', false);
        }
      });

      return false;
    }
  });

  $('.btn-submit').click(function() {
    var mode = $(this).data('mode');
    var prompt = $(this).data('prompt');
    if(confirm(prompt)) {
      if(mode) {
        $('input[name=mode]', $('#form-quest')).val(mode);
        $('#form-quest').submit();
      }
    } else {
      return false;
    }
  });

  var elScore = $('[data-name=table-score]');
  for(var i=0; i<=elScore.length; i++) {
    var url = $(elScore[i]).data('url');
    refreshScore($('tbody', $(elScore[i])), url);
  }

  $('[data-name=btn-refresh]').click(function(){
    var url = $(this).data('url');
    var el = $('tbody', $('[data-name=table-score][data-url="'+url+'"]'));
    refreshScore(el, url);
    return false;
  });

  $('[data-name=btn-score]').click(function(){
    var url = $(this).attr('href');
    var target = $(this).data('target');
    var score = $('input[data-url="'+url+'"]').val();
    var el = $('tbody', $('[data-name=table-score][data-url="'+target+'"]'));

    $.post(url, {LogValue: score}, function(res) {
      refreshScore(el, target);
      $('input[data-url="'+url+'"]').val('');
    });
    return false;
  });
});
</script>
