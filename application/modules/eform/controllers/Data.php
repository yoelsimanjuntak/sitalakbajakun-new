<?php
class Data extends MY_Controller {
  function __construct() {
      parent::__construct();
      if(!IsLogin()) {
        redirect(site_url('eform/user/login'));
      }
  }

  public function index($id='') {
    $ruser = GetLoggedUser();
    $data['title'] = 'Daftar Kuisioner';
    $data['id'] = $id;

    if(!empty($id)) {
      $rquest = $this->db
      ->where(COL_UNIQ, $id)
      ->get(TBL_EFORM_MQUEST)
      ->row_array();
      if(!empty($rquest)) {
        $data['title'] = $rquest[COL_QUESTNAME];
      }
    }
    $this->template->load('_layouts/main', 'eform/data/index', $data, FALSE, TRUE);
  }

  public function index_load($id='') {
    $start = $_POST['start'];
    $rowperpage = $_POST['length'];
    $skpd = !empty($_POST['Skpd'])?$_POST['Skpd']:null;
    $stat = !empty($_POST['Status'])?$_POST['Status']:null;
    /*$IdSupplier = !empty($_POST['idSupplier'])?$_POST['idSupplier']:null;
    $dateFrom = !empty($_POST['dateFrom'])?$_POST['dateFrom']:date('Y-m-01');
    $dateTo = !empty($_POST['dateTo'])?$_POST['dateTo']:date('Y-m-d');*/

    $ruser = GetLoggedUser();
    $orderdef = array(COL_CREATEDON=>'desc');
    $orderables = array(null,COL_QUESTNAME,null,COL_CREATEDON,COL_QUESTEND);
    $cols = array(COL_QUESTNAME);

    if($ruser[COL_ROLEID]!=ROLEADMIN&&$ruser[COL_ROLEID]!=ROLEKUISIONER&&$ruser[COL_ROLEID]!=ROLEGUEST) {
      $this->db->where(TBL_EFORM_TQUEST.'.'.COL_IDSKPD, $ruser[COL_SKPDID]);
    }
    if(!empty($id) && $id != '') {
      $this->db->where(TBL_EFORM_TQUEST.'.'.COL_IDQUEST, $id);
    }

    $queryAll = $this->db
    ->join(TBL__USERINFORMATION.' uc','uc.'.COL_USERNAME." = ".TBL_EFORM_TQUEST.".".COL_CREATEDBY,"left")
    ->join(TBL_EFORM_MUNIT.' skpd','skpd.'.COL_UNITID." = ".TBL_EFORM_TQUEST.".".COL_IDSKPD,"left")
    ->where(COL_QUESTISDELETED, 0)
    ->get(TBL_EFORM_TQUEST);

    $i = 0;
    foreach($cols as $item){
      if($item == COL_CREATEDBY) $item = TBL_EFORM_TQUEST.'.'.COL_CREATEDBY;
      if($item == COL_SKPDNAMA) $item = 'skpd.'.COL_UNITNAMA;
      if(!empty($_POST['search']['value'])){
        if($i===0) {
          $this->db->group_start();
          $this->db->like($item, $_POST['search']['value']);
        } else {
          $this->db->or_like($item, $_POST['search']['value']);
        }
        if(count($cols) - 1 == $i){
          $this->db->group_end();
        }
      }
      $i++;
    }

    if(!empty($_POST['order'])){
      $order = $orderables[$_POST['order']['0']['column']];
      if ($order==COL_CREATEDON) {
        $order = TBL_EFORM_TQUEST.".".COL_CREATEDON;
      }

      $this->db->order_by($order, $_POST['order']['0']['dir']);
    } else if(!empty($orderdef)){
      $order = $orderdef;
      $this->db->order_by(key($order), $order[key($order)]);
    }

    if($ruser[COL_ROLEID]!=ROLEADMIN&&$ruser[COL_ROLEID]!=ROLEKUISIONER&&$ruser[COL_ROLEID]!=ROLEGUEST) {
      $this->db->where(TBL_EFORM_TQUEST.'.'.COL_IDSKPD, $ruser[COL_SKPDID]);
    }
    if(!empty($skpd)) {
      $this->db->where(TBL_EFORM_TQUEST.'.'.COL_IDSKPD, $skpd);
    }
    if(!empty($stat)) {
      if($stat=='NEW') $this->db->where(TBL_EFORM_TQUEST.'.'.COL_QUESTEND.' is null');
      if($stat=='COMPLETE') $this->db->where(TBL_EFORM_TQUEST.'.'.COL_QUESTEND.' is not null');
    }
    if(!empty($id) && $id != '') {
      $this->db->where(TBL_EFORM_TQUEST.'.'.COL_IDQUEST, $id);
    }

    $q = $this->db
    ->select('eform_tquest.*, skpd.UnitNama, uc.Name, (select count(*) from eform_tquestdetail det1 where det1.IdQuest=eform_tquest.Uniq) as QuestDet, (select count(*) from eform_tquestdetail det2 where det2.IdQuest=eform_tquest.Uniq and det2.QuestEvaluation is not null) as QuestEval')
    ->join(TBL__USERINFORMATION.' uc','uc.'.COL_USERNAME." = ".TBL_EFORM_TQUEST.".".COL_CREATEDBY,"left")
    ->join(TBL_EFORM_MUNIT.' skpd','skpd.'.COL_UNITID." = ".TBL_EFORM_TQUEST.".".COL_IDSKPD,"left")
    ->order_by(TBL_EFORM_TQUEST.".".COL_CREATEDON, 'desc')
    ->order_by("skpd.".COL_UNITNAMA, 'asc')
    ->where(COL_QUESTISDELETED, 0)
    ->get_compiled_select(TBL_EFORM_TQUEST, FALSE);
    $rec = $this->db->query($q." LIMIT $rowperpage OFFSET $start");
    $data = [];

    foreach($rec->result_array() as $r) {
      $isEditable=true;
      $badgeStat = 'badge-secondary';
      $badgeEval = '';
      if(!empty($r[COL_QUESTEND])) $badgeStat = 'badge-success';
      if(!empty($r[COL_QUESTEND])) $isEditable = false;

      if($r[COL_QUESTCATEGORY]='PEKPPP') {
        if($r['QuestDet'] == $r['QuestEval']) {
          $badgeEval = ' <span class="badge badge-info">FINAL</span>';
        } else if($r['QuestEval']>0) {
          $badgeEval = ' <span class="badge badge-info">EVALUASI</span>';
        }
      }

      $data[] = array(
        '<a href="'.site_url('eform/data/delete/'.$r[COL_UNIQ]).'" class="btn btn-xs btn-danger btn-action '.(!$isEditable?'disabled':'').'" title="Hapus" data-confirm="Apakah anda yakin ingin menghapus kuisioner ini?"><i class="fas fa-trash"></i></a>&nbsp;'.
        '<a href="'.site_url('eform/data/reset/'.$r[COL_UNIQ]).'" class="btn btn-xs btn-warning btn-action '.(!$isEditable?'':'disabled').'" title="Reset" data-confirm="Apakah anda yakin ingin me-reset? Reset akan menghapus isian kuisioner sebelumnya."><i class="fas fa-sync"></i></a>&nbsp;'.
        '<a href="'.site_url('eform/data/form/'.$r[COL_UNIQ]).'" class="btn btn-xs btn-primary btn-edit"><i class="fas fa-arrow-circle-right" title="Rincian"></i></a>',
        $r[COL_QUESTNAME].'<br /><small class="font-italic">'.$r[COL_UNITNAMA].'</small>',
        '<span class="badge '.$badgeStat.'">'.(!empty($r[COL_QUESTEND])?'SELESAI':'DRAFT').'</span>'.$badgeEval,
        date('Y-m-d H:i', strtotime($r[COL_CREATEDON])),
        !empty($r[COL_QUESTEND]) ? date('Y-m-d H:i', strtotime($r[COL_QUESTEND])) : '-'
      );
    }

    $result = array(
      "draw" => $_POST['draw'],
      "recordsFiltered" => $this->db->query($q)->num_rows(),
      "recordsTotal" => $queryAll->num_rows(),
      "data" => $data,
    );

    echo json_encode($result);
    exit();
  }

  public function add($id='') {
    $ruser = GetLoggedUser();
    $ropd = array();
    if($ruser[COL_ROLEID]!=ROLEADMIN&&$ruser[COL_ROLEID]!=ROLEKUISIONER) {
      $ropd = $this->db
      ->where(COL_UNITID, $ruser[COL_SKPDID])
      ->get(TBL_EFORM_MUNIT)
      ->row_array();
    }

    $data['id'] = $id;
    $data['ropd'] = $ropd;
    if(!empty($_POST)) {
      $rform = $this->db
      ->where(COL_UNIQ, $this->input->post(COL_IDQUEST))
      ->get(TBL_EFORM_MQUEST)
      ->row_array();
      if(empty($rform)) {
        ShowJsonError('Parameter tidak valid!');
        exit();
      }

      $rdet = $this->db
      ->where(COL_IDQUEST, $this->input->post(COL_IDQUEST))
      ->get(TBL_EFORM_MQUESTDETAIL)
      ->result_array();
      if(empty($rdet)) {
        ShowJsonError('Parameter tidak valid!');
        exit();
      }

      $det = array();
      $dat = array();
      $this->db->trans_begin();
      try {
        if(empty($this->input->post(COL_IDSKPD))) {
          $rskpd =  $this->db
          ->where(COL_UNITISAKTIF, 1)
          ->get(TBL_EFORM_MUNIT)
          ->result_array();

          foreach($rskpd as $skpd) {
            $det = array();
            $dat = array(
              COL_IDQUEST=>$this->input->post(COL_IDQUEST),
              COL_IDSKPD=>$skpd[COL_UNITID],
              COL_QUESTNAME=>$this->input->post(COL_QUESTNAME),
              COL_QUESTCATEGORY=>$rform[COL_QUESTCATEGORY],

              COL_CREATEDBY=>$ruser[COL_USERNAME],
              COL_CREATEDON=>date('Y-m-d H:i:s')
            );

            $res = $this->db->insert(TBL_EFORM_TQUEST, $dat);
            if(!$res) {
              $err = $this->db->error();
              throw new Exception($err['message']);
            }

            $idQuest = $this->db->insert_id();
            foreach($rdet as $d) {
              $det[] = array(
                COL_IDQUEST=>$idQuest,
                COL_QUESTNO=>$d[COL_QUESTNO],
                COL_QUESTTYPE=>$d[COL_QUESTTYPE],
                COL_QUESTTEXT=>$d[COL_QUESTTEXT],
                COL_QUESTOPTION=>$d[COL_QUESTOPTION],
                COL_QUESTCATEGORY=>$d[COL_QUESTCATEGORY],
                COL_QUESTCATEGORYSUB=>$d[COL_QUESTCATEGORYSUB],
                COL_QUESTISPRE=>$d[COL_QUESTISPRE]
              );
            }

            $res = $this->db->insert_batch(TBL_EFORM_TQUESTDETAIL, $det);
            if(!$res) {
              $err = $this->db->error();
              throw new Exception($err['message']);
            }
          }
        } else {
          $dat = array(
            COL_IDQUEST=>$this->input->post(COL_IDQUEST),
            COL_IDSKPD=>$this->input->post(COL_IDSKPD),
            COL_QUESTNAME=>$this->input->post(COL_QUESTNAME),
            COL_QUESTCATEGORY=>$rform[COL_QUESTCATEGORY],

            COL_CREATEDBY=>$ruser[COL_USERNAME],
            COL_CREATEDON=>date('Y-m-d H:i:s')
          );

          $res = $this->db->insert(TBL_EFORM_TQUEST, $dat);
          if(!$res) {
            $err = $this->db->error();
            throw new Exception($err['message']);
          }

          $idQuest = $this->db->insert_id();
          foreach($rdet as $d) {
            $det[] = array(
              COL_IDQUEST=>$idQuest,
              COL_QUESTNO=>$d[COL_QUESTNO],
              COL_QUESTTYPE=>$d[COL_QUESTTYPE],
              COL_QUESTTEXT=>$d[COL_QUESTTEXT],
              COL_QUESTOPTION=>$d[COL_QUESTOPTION],
              COL_QUESTCATEGORY=>$d[COL_QUESTCATEGORY],
              COL_QUESTCATEGORYSUB=>$d[COL_QUESTCATEGORYSUB],
              COL_QUESTISPRE=>$d[COL_QUESTISPRE]
            );
          }

          $res = $this->db->insert_batch(TBL_EFORM_TQUESTDETAIL, $det);
          if(!$res) {
            $err = $this->db->error();
            throw new Exception($err['message']);
          }
        }

        $this->db->trans_commit();
        ShowJsonSuccess('Kuisioner berhasil ditambahkan.');

      } catch(Exception $ex) {
        $this->db->trans_rollback();
        ShowJsonError($ex->getMessage());
        exit();
      }

    } else {
      $this->load->view('eform/data/add', $data);
    }
  }

  public function form($id) {
    $ruser = GetLoggedUser();
    $rquest = $this->db
    ->select('eform_tquest.*, skpd.UnitNama')
    ->join(TBL_EFORM_MUNIT.' skpd','skpd.'.COL_UNITID." = ".TBL_EFORM_TQUEST.".".COL_IDSKPD,"left")
    ->where(COL_UNIQ, $id)
    ->get(TBL_EFORM_TQUEST)
    ->row_array();

    if(empty($rquest)) {
      ShowJsonError('Parameter tidak valid!');
      exit();
    }

    $isEditable=true;
    if($ruser[COL_ROLEID]==ROLEGUEST) $isEditable = false;

    if(!empty($_POST)) {
      $mode = $this->input->post("mode");
      if(!$isEditable) {
        ShowJsonError('Maaf, LKE ini tidak dapat diperbarui.');
        exit();
      }

      $rquestdet = $this->db
      ->where(COL_IDQUEST, $id)
      ->order_by(COL_QUESTNO, 'asc')
      ->get(TBL_EFORM_TQUESTDETAIL)
      ->result_array();

      $this->db->trans_begin();
      try {
        foreach($rquestdet as $q) {
          $opt = json_decode($q[COL_QUESTOPTION]);
          $resp = $this->input->post("QuestResponse__".$q[COL_UNIQ]);
          $respfile = $this->input->post("QuestFile__".$q[COL_UNIQ]);
          $respeval = $this->input->post("QuestEvaluation__".$q[COL_UNIQ]);
          $score = 0;
          $scoreEval = 0;

          if(isset($opt) && is_array($opt)) {
            foreach($opt as $o) {
              if(!empty($rquest[COL_QUESTEND])) {
                if(strtolower($o->Opt) == strtolower($respeval)) {
                  $scoreEval = toNum($o->Val);
                  break;
                }
              } else {
                if(strtolower($o->Opt) == strtolower($resp)) {
                  $score = toNum($o->Val);
                  break;
                }
              }
            }
          }


          if(isset($resp)) {
            $res = $this->db
            ->where(COL_UNIQ, $q[COL_UNIQ])
            ->update(TBL_EFORM_TQUESTDETAIL, array(COL_QUESTRESPONSE=>!isset($resp)?null:$resp,COL_QUESTFILE=>empty($respfile)?null:$respfile, COL_QUESTSCORE=>$score));
            if(!$res) {
              $err = $this->db->error();
              throw new Exception($err['message']);
            }
          } else if(isset($respeval)) {
            $res = $this->db
            ->where(COL_UNIQ, $q[COL_UNIQ])
            ->update(TBL_EFORM_TQUESTDETAIL, array(COL_QUESTEVALUATION=>!isset($respeval)?null:$respeval, COL_QUESTEVALUATIONSCORE=>$scoreEval));
            if(!$res) {
              $err = $this->db->error();
              throw new Exception($err['message']);
            }
          }
        }

        if(empty($rquest[COL_QUESTEND]) && $mode=="submit") {
          $res = $this->db
          ->where(COL_UNIQ, $id)
          ->update(TBL_EFORM_TQUEST, array(COL_QUESTEND=>date('Y-m-d H:i:s')));

          if(!$res) {
            $err = $this->db->error();
            throw new Exception($err['message']);
          }
        } else {
          $res = $this->db
          ->where(COL_UNIQ, $id)
          ->update(TBL_EFORM_TQUEST, array(COL_QUESTREMARKS=>empty($this->input->post(COL_QUESTREMARKS))?null:$this->input->post(COL_QUESTREMARKS)));

          if(!$res) {
            $err = $this->db->error();
            throw new Exception($err['message']);
          }
        }

        $redir = site_url('eform/data/index/'.$rquest[COL_IDQUEST]);
        if(empty($rquest[COL_QUESTEND]) && $mode=="submit") {
          $redir = site_url('eform/data/result/'.$id);
        }
        $this->db->trans_commit();
        ShowJsonSuccess('Submit Berhasil!', array('redirect'=>$redir));
      } catch(Exception $ex) {
        $this->db->trans_rollback();
        ShowJsonError($ex->getMessage());
        exit();
      }

    } else {
      if(!empty($rquest[COL_QUESTEND])) {
        redirect('eform/data/result/'.$id);
      }

      $data['title'] = 'FORM KUISIONER';
      $data['isEditable'] = $isEditable;
      $data['rquest'] = $rquest;
      $this->template->load('_layouts/main', 'eform/data/form', $data, FALSE, TRUE);
    }
  }

  public function result($id) {
    $ruser = GetLoggedUser();
    $rquest = $this->db
    ->select('eform_tquest.*, skpd.UnitNama, (select count(*) from eform_tquestdetail det1 where det1.IdQuest=eform_tquest.Uniq) as QuestDet, (select count(*) from eform_tquestdetail det2 where det2.IdQuest=eform_tquest.Uniq and det2.QuestEvaluation is not null) as QuestEval')
    ->join(TBL_EFORM_MUNIT.' skpd','skpd.'.COL_UNITID." = ".TBL_EFORM_TQUEST.".".COL_IDSKPD,"left")
    ->where(TBL_EFORM_TQUEST.'.'.COL_UNIQ, $id)
    ->get(TBL_EFORM_TQUEST)
    ->row_array();

    if(empty($rquest)) {
      ShowJsonError('Parameter tidak valid!');
      exit();
    }

    $data['title'] = 'HASIL KUISIONER';
    $data['rquest'] = $rquest;
    $this->template->load('_layouts/main', 'eform/data/result', $data, FALSE, TRUE);
  }

  public function review($id) {
    $ruser = GetLoggedUser();
    $rquest = $this->db
    ->select('eform_tquest.*, skpd.UnitNama')
    ->join(TBL_EFORM_MUNIT.' skpd','skpd.'.COL_UNITID." = ".TBL_EFORM_TQUEST.".".COL_IDSKPD,"left")
    ->where(COL_UNIQ, $id)
    ->get(TBL_EFORM_TQUEST)
    ->row_array();

    if(empty($rquest)) {
      ShowJsonError('Parameter tidak valid!');
      exit();
    }

    $data['title'] = 'ISIAN KUISIONER';
    $data['isEditable'] = false;
    $data['rquest'] = $rquest;
    $this->template->load('_layouts/main', 'eform/data/form', $data, FALSE, TRUE);
  }

  public function delete($id) {
    $ruser = GetLoggedUser();
    $rdata = $this->db->where(COL_UNIQ, $id)->get(TBL_EFORM_TQUEST)->row_array();
    if(empty($rdata)) {
      ShowJsonError('Parameter tidak valid!');
      exit();
    }

    $res = $this->db->where(COL_UNIQ, $id)->update(TBL_EFORM_TQUEST, array(COL_QUESTISDELETED=>1));
    if(!$res) {
      $err = $this->db->error();
      ShowJsonError($err['message']);
      exit();
    }

    ShowJsonSuccess('BERHASIL DIHAPUS');
  }

  public function reset($id) {
    $ruser = GetLoggedUser();
    $rdata = $this->db->where(COL_UNIQ, $id)->get(TBL_EFORM_TQUEST)->row_array();
    if(empty($rdata)) {
      ShowJsonError('Parameter tidak valid!');
      exit();
    }

    $this->db->trans_begin();
    try {
      $res = $this->db
      ->where(COL_UNIQ, $id)
      ->update(TBL_EFORM_TQUEST, array(COL_QUESTEND=>null));
      if(!$res) {
        $err = $this->db->error();
        throw new Exception($err['message']);
      }
      $res = $this->db
      ->where(COL_IDQUEST, $id)
      ->update(TBL_EFORM_TQUESTDETAIL, array(COL_QUESTRESPONSE=>null,COL_QUESTSCORE=>null,COL_QUESTEVALUATION=>null,COL_QUESTEVALUATIONSCORE=>null));
      if(!$res) {
        $err = $this->db->error();
        throw new Exception($err['message']);
      }

      $this->db->trans_commit();
      ShowJsonSuccess('Berhasil me-reset kuisioner.');
    } catch(Exception $ex) {
      $this->db->trans_rollback();
      ShowJsonError($ex->getMessage());
      exit();
    }
  }

  public function score($id) {
    $res = $this->db
    ->join(TBL__USERINFORMATION.' uc','uc.'.COL_USERNAME." = ".TBL_EFORM_TQUESTLOG.".".COL_CREATEDBY,"left")
    ->where(COL_IDQUEST, $id)
    ->order_by(COL_CREATEDON, 'desc')
    ->get(TBL_EFORM_TQUESTLOG)
    ->result_array();

    if(!empty($res)) {
      $html = "";
      foreach($res as $r) {
        $_name = $r[COL_NAME];
        $_val = number_format($r[COL_LOGVALUE]);
        $_date = date('Y-m-d H:i', strtotime($r[COL_CREATEDON]));
        $html .= @"
        <tr>
          <td>$_name</td>
          <td class='text-right'>$_val</td>
          <td class='text-right'>$_date</td>
        </tr>
        ";
      }
      echo $html;
    } else {
      echo @"
      <tr>
        <td colspan='3'>
          <p class='text-danger text-sm text-center font-italic m-0'>Belum ada penilaian diberikan.</p>
        </td>
      </tr>
      ";
    }
  }

  public function score_add($id) {
    $ruser = GetLoggedUser();
    $rdata = $this->db
    ->where(COL_IDQUEST, $id)
    ->where(COL_CREATEDBY, $ruser[COL_USERNAME])
    ->get(TBL_EFORM_TQUESTLOG)
    ->row_array();

    if(!empty($_POST)) {
      if(!empty($rdata)) {
        $res = $this->db
        ->where(COL_IDQUEST, $id)
        ->where(COL_CREATEDBY, $ruser[COL_USERNAME])
        ->update(TBL_EFORM_TQUESTLOG,
        array(
          COL_LOGVALUE=>$this->input->post(COL_LOGVALUE),
          COL_CREATEDBY=>$ruser[COL_USERNAME],
          COL_CREATEDON=>date('Y-m-d H:i:s')
        ));
        if(!$res) {
          $err = $this->db->error();
          ShowJsonError($err['message']);
          exit();
        }

        ShowJsonSuccess('Berhasil.');
      } else {
        $res = $this->db->insert(TBL_EFORM_TQUESTLOG,
        array(
          COL_IDQUEST=>$id,
          COL_LOGTYPE=>'SCORING',
          COL_LOGNAME=>'SCORING',
          COL_LOGVALUE=>$this->input->post(COL_LOGVALUE),
          COL_CREATEDBY=>$ruser[COL_USERNAME],
          COL_CREATEDON=>date('Y-m-d H:i:s')
        ));
        if(!$res) {
          $err = $this->db->error();
          ShowJsonError($err['message']);
          exit();
        }

        ShowJsonSuccess('Berhasil.');
      }
    }
  }
}
?>
