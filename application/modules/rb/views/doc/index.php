<?php
$ruser = GetLoggedUser();

$rOptSkpd = $this->db
->where(COL_SKPDISAKTIF, 1)
->order_by(COL_SKPDURUSAN, 'asc')
->order_by(COL_SKPDBIDANG, 'asc')
->order_by(COL_SKPDUNIT, 'asc')
->order_by(COL_SKPDSUBUNIT, 'asc')
->get(TBL_SAKIPV2_SKPD)
->result_array();

$getSkpd = '';

if(!empty($_GET['idSKPD'])) $getSkpd = $_GET['idSKPD'];
else if(!empty($rOptSkpd)) $getSkpd = $rOptSkpd[0][COL_SKPDID];

if($ruser[COL_ROLEID]!=ROLEADMIN && $ruser[COL_ROLEID]!=ROLEGUEST) {
  $getSkpd=$ruser[COL_SKPDID];
}

$arrData = array();
if($nounit) {
  $getSkpd = null;
  $this->db->where('IdSkpd is NULL');
}

if(!empty($getSkpd) && !$nounit) {
  $this->db->where(COL_IDSKPD, $getSkpd);
}

$arrData = $this->db
->where(COL_DOCTYPE, $type)
->order_by(COL_DOCTAHUN)
->order_by(COL_CREATEDON)
->get(TBL_SAKIPV2_SKPD_DOC)
->result_array();

$i = 0;
$res = array();
foreach ($arrData as $d) {
  $urlDoc = MY_UPLOADURL.$d[COL_DOCURL];
  $link = '-';
  if($ruser[COL_ROLEID] != ROLEGUEST) {
    $link = @
    '<a href="'.site_url('rb/doc/ajax-form/edit/'.$d[COL_DOCID]).'" class="btn btn-primary btn-xs btn-edit-doc"><i class="far fa-edit"></i></a>&nbsp;
    <a href="'.site_url('rb/doc/ajax-change/delete/'.$d[COL_DOCID]).'" class="btn btn-danger btn-xs btn-del-doc" data-prompt="YAKIN INGIN MENGHAPUS?"><i class="far fa-times-circle"></i></a>&nbsp;
    <a href="'.$urlDoc.'" target="_blank" class="btn btn-info btn-xs"><i class="far fa-download"></i></a>';
  }
  $res[$i] = array(
    date('Y-m-d H:i', strtotime($d[COL_CREATEDON])),
    $d[COL_DOCNAME],
    $d[COL_DOCTAHUN],
    strlen($d[COL_DOCREMARKS]) > 40 ? substr($d[COL_DOCREMARKS], 0, 40) . "..." : $d[COL_DOCREMARKS],
    $link
  );
  $i++;
}
$jsonData = json_encode($res);
?>
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h3 class="m-0 font-weight-light"><?=$title?></h3>
      </div>
      <?php
      if(!empty($navs)) {
        ?>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <?php
            foreach($navs as $n) {
              if(!empty($n['link'])) {
                ?>
                <li class="breadcrumb-item"><a href="<?=$n['link']?>"><?=$n['text']?></a></li>
                <?php
              } else {
                ?>
                <li class="breadcrumb-item active"><?=$n['text']?></li>
                <?php
              }
            }
            ?>
          </ol>
        </div>
        <?php
      }
      ?>
    </div>
  </div>
</div>
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-12">
        <div class="card card-outline card-secondary">
          <div class="card-header p-0 border-0">
            <table class="table table-bordered mb-0">
              <thead>
                <?php
                if(!$nounit) {
                  ?>
                  <tr>
                    <td>
                      <div class="form-group row mb-0">
                        <label class="control-label col-lg-2">SKPD :</label>
                        <div class="col-lg-10">
                          <?php
                          if($ruser[COL_ROLEID] == ROLEADMIN || $ruser[COL_ROLEID] == ROLEGUEST) {
                            ?>
                            <select class="form-control" name="filterSkpd">
                              <?php
                              foreach($rOptSkpd as $opt) {
                                $isSelected = '';
                                if(!empty($getSkpd) && $opt[COL_SKPDID]==$getSkpd) {
                                  $isSelected='selected';
                                }
                                ?>
                                <option value="<?=current_url().'?idSKPD='.$opt[COL_SKPDID]?>" <?=$isSelected?>>
                                  <?=$opt[COL_SKPDURUSAN].'.'.$opt[COL_SKPDBIDANG].'.'.$opt[COL_SKPDUNIT].'.'.$opt[COL_SKPDSUBUNIT].' - '.strtoupper($opt[COL_SKPDNAMA])?>
                                </option>
                                <?php
                              }
                              ?>
                            </select>
                            <?php
                          } else {
                            $ropd = $this->db
                            ->where(COL_SKPDID, $ruser[COL_SKPDID])
                            ->get(TBL_SAKIPV2_SKPD)
                            ->row_array();
                            ?>
                            <p class="font-italic font-weight-bold mb-0" style="line-height: 2; text-decoration: underline">
                              <?=strtoupper($ropd[COL_SKPDNAMA])?>
                            </p>
                            <?php
                          }
                          ?>
                        </div>
                      </div>
                    </td>
                  </tr>
                  <?php
                }
                ?>

              </thead>
            </table>
          </div>
          <div class="card-body">
            <form id="dataform" method="post" action="#">
                <table id="datalist" class="table table-bordered table-hover">

                </table>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<div class="modal fade" id="modalFormDoc" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">DOKUMEN <?=$title?></h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true"><i class="fas fa-close"></i></span>
          </button>
        </div>
        <div class="modal-body">
        </div>
        <div class="modal-footer d-block">
          <div class="row">
            <div class="col-lg-12 text-center">
              <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="far fa-times-circle"></i>&nbsp;BATAL</button>
              <button type="submit" class="btn btn-primary btn-submit"><i class="far fa-plus-circle"></i>&nbsp;SUBMIT</button>
            </div>
          </div>
        </div>
    </div>
  </div>
</div>
<script type="text/javascript">
var modalFormDoc = $('#modalFormDoc');
$(document).ready(function(){
  $('select[name=filterSkpd]').change(function(){
    var url = $(this).val();
    location.href = url;
  });

  modalFormDoc.on('hidden.bs.modal', function (e) {
    $('.modal-body', modalFormDoc).empty();
  });

  var dataTable = $('#datalist').dataTable({
    "autoWidth" : false,
    "aaData": <?=$jsonData?>,
    //"bJQueryUI": true,
    //"aaSorting" : [[5,'desc']],
    "scrollY" : '40vh',
    "scrollX": "100%",
    "iDisplayLength": 100,
    "aLengthMenu": [[100, 1000, 5000, -1], [100, 1000, 5000, "Semua"]],
    "dom":"R<'row'<'col-sm-8'B><'col-sm-4'f>><'row'<'col-sm-12'tr>><'row'<'col-sm-5'i><'col-sm-7'p>>",
    "buttons": [{
                text: '<i class="far fa-plus-circle"></i>&nbsp;TAMBAH',
                className: 'btn btn-sm font-weight-bold',
                action: function ( e, dt, node, config ) {
                  var url = '<?=site_url('rb/doc/ajax-form/add/'.$type.(!empty($getSkpd)?'/'.$getSkpd:''))?>';
                  modalFormDoc.modal('show');
                  $('.modal-body', modalFormDoc).html('<p class="font-italic mb-0 text-center"><i class="far fa-circle-notch fa-spin"></i>&nbsp;MEMUAT...</p>');
                  $('.modal-body', modalFormDoc).load(url, function(){
                    $('button[type=submit]', modalFormDoc).unbind('click').click(function(){
                      $('form', modalFormDoc).submit();
                    });
                  });
                }
            }],
    "order": [[0, "desc" ]],
    "columnDefs":[
      {targets: [0], className:'dt-body-right nowrap'},
      {targets: [2,4], className:'nowrap'}
    ],
    "aoColumns": [
        {"sTitle": "WAKTU UNGGAH"},
        {"sTitle": "JUDUL"},
        {"sTitle": "TAHUN"},
        {"sTitle": "KETERANGAN","bSortable":false},
        {"sTitle": "AKSI", "sWidth":"10px","bSortable":false},
    ],
    "rowCallback": function(row, data) {
      $('.btn-edit-doc', row).click(function() {
        var url = $(this).attr('href');
        modalFormDoc.modal('show');
        $('.modal-body', modalFormDoc).html('<p class="font-italic mb-0 text-center"><i class="far fa-circle-notch fa-spin"></i>&nbsp;MEMUAT...</p>');
        $('.modal-body', modalFormDoc).load(url, function(){
          $('button[type=submit]', modalFormDoc).unbind('click').click(function(){
            $('form', modalFormDoc).submit();
          });
        });
        return false;
      });

      $('.btn-del-doc', row).click(function() {
        var url = $(this).attr('href');
        var prompt = $(this).data('prompt');
        swal({
          title: "APAKAH ANDA YAKIN?",
          text: prompt,
          icon: "warning",
          buttons: [
            'BATAL',
            'YAKIN'
          ],
        }).then(function(isConfirm) {
          if (isConfirm) {
            $.ajax({
              url: url,
              method: "GET",
              dataType: "json"
            }).success(function(res) {
              if(res.error) {
                swal({
                  title: 'ERROR',
                  text: res.error,
                  icon: 'error',
                  buttons:false
                });
              } else {
                location.reload();
              }
            }).fail(function() {
              swal({
                title: 'SERVER ERROR',
                text: 'Terjadi kesalahan pada server. Silakan coba kembali.',
                icon: 'error',
                buttons:false
              });
            }).done(function() {

            });

          } else {

          }
        })
        return false;
      });
    }
  });
});
</script>
