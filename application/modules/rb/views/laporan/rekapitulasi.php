<?php
$ruser = GetLoggedUser();
$rOptRenja = array();
if($ruser[COL_ROLEID]!=ROLEADMIN && $ruser[COL_ROLEID]!=ROLEGUEST) {
  $this->db->where(TBL_RB_RENJA.'.'.COL_SKPDID, $ruser[COL_SKPDID]);
}

$rOptRenja = $this->db
->join(TBL_SAKIPV2_SKPD.' opd','opd.'.COL_SKPDID." = ".TBL_RB_RENJA.".".COL_SKPDID,"left")
->where(COL_ISDELETED, 0)
->where(COL_NMKATEGORI, strtoupper($tipe))
->order_by(COL_TAHUN, 'desc')
->order_by(COL_NMTYPE, 'asc')
->get(TBL_RB_RENJA)
->result_array();

$rrenja = array();
?>
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h3 class="m-0 text-dark font-weight-light"><?=$title?></h3>
      </div>
    </div>
  </div>
</div>
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-sm-12">
        <div class="card card-default">
          <form method="get" action="<?=current_url()?>">
            <div class="card-body">
              <div class="form-group row">
                <label class="control-label col-lg-2">DOK. RENCANA AKSI</label>
                <div class="col-sm-6">
                  <select class="form-control" name="id">
                    <?php
                    foreach($rOptRenja as $opt) {
                      $isSelected = '';
                      if(!empty($_GET['id']) && $_GET['id']==$opt[COL_UNIQ]) {
                        $isSelected='selected';
                      }
                      ?>
                      <option value="<?=$opt[COL_UNIQ]?>" <?=$isSelected?>>
                        <?=$opt[COL_TAHUN].' - '.(!empty($opt[COL_SKPDNAMA])?strtoupper($opt[COL_SKPDNAMA]).(!empty($opt[COL_NMKETERANGAN])?' : '.$opt[COL_NMKETERANGAN]:''):$opt[COL_NMKETERANGAN])?>
                      </option>
                      <?php
                    }
                    ?>
                  </select>
                </div>
                <div class="col-sm-4">
                  <button type="submit" class="btn btn-primary"><i class="far fa-chevron-circle-right"></i>&nbsp;LIHAT</button>
                  <?php
                  if(!empty($_GET['id'])) {
                    ?>
                    <a href="<?=current_url().'?id='.$_GET['id'].'&download=1'?>" target="_blank" class="btn btn-success"><i class="far fa-download"></i>&nbsp;DOWNLOAD</a>
                    <?php
                  }
                  ?>
                </div>
              </div>
            </div>
          </form>
        </div>
        <?php
        if(!empty($_GET['id'])) {
          $rrenja = $this->db
          ->join(TBL_SAKIPV2_SKPD.' opd','opd.'.COL_SKPDID." = ".TBL_RB_RENJA.".".COL_SKPDID,"left")
          ->where(COL_UNIQ, $_GET['id'])
          ->get(TBL_RB_RENJA)
          ->row_array();
          if(!empty($rrenja)) {
            ?>
            <div class="card card-default">
              <div class="card-header">
                <h6 class="font-weight-bold text-center">
                  REKAPITULASI CAPAIAN<br />
                  REFORMASI BIROKRASI <?=$rrenja[COL_NMKATEGORI]?><?=!empty($rrenja[COL_SKPDNAMA])?'<br />'.strtoupper($rrenja[COL_SKPDNAMA]):''?>
                </h6>
              </div>
              <div class="card-body p-0">
                <div class="row">
                  <div class="col-sm-12">
                    <?=$this->load->view('rb/laporan/rekapitulasi-partial', array('rrenja'=>$rrenja ), true)?>
                  </div>
                </div>
              </div>
            </div>
            <?php
          }
        }
        ?>
      </div>
    </div>
  </div>
</section>
