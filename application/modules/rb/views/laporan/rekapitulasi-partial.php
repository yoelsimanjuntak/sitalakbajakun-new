<?php
$rperubahan = $this->db
->where(COL_IDRENJA, $rrenja[COL_UNIQ])
->order_by(COL_UNIQ)
->get(TBL_RB_RENJAPERUBAHAN)
->result_array();

?>
<?php
if(isset($download)) {
  header("Content-type: application/vnd-ms-excel");
  header("Content-Disposition: attachment; filename=Rekapitulasi RB ".date('YmdHi').".xls");
}
?>
<table class="table table-bordered table-responsive tbl-det mb-0" style="border-right: none !important; border-left: none !important">
  <thead class="text-sm bg-teal">
    <tr>
      <th class="text-center" style="vertical-align: middle" rowspan="2">RENCANA AKSI</th>
      <th class="text-center" style="vertical-align: middle" rowspan="2">INDIKATOR</th>
      <th class="text-center" style="vertical-align: middle; width: 10px; white-space: nowrap" rowspan="2">SATUAN</th>
      <th class="text-center" style="vertical-align: middle; width: 10px; white-space: nowrap" colspan="5">TARGET</th>
      <th class="text-center" style="vertical-align: middle; width: 10px; white-space: nowrap" colspan="5">ANGGARAN</th>
      <th class="text-center" style="vertical-align: middle; width: 10px; white-space: nowrap" colspan="4">CAPAIAN</th>
      <th class="text-center" style="vertical-align: middle; width: 10px; white-space: nowrap" colspan="4">BELANJA</th>
    </tr>
    <tr>
      <!--TARGET-->
      <th class="text-center" style="vertical-align: middle; width: 10px; white-space: nowrap">TW. I</th>
      <th class="text-center" style="vertical-align: middle; width: 10px; white-space: nowrap">TW. II</th>
      <th class="text-center" style="vertical-align: middle; width: 10px; white-space: nowrap">TW. III</th>
      <th class="text-center" style="vertical-align: middle; width: 10px; white-space: nowrap">TW. IV</th>
      <th class="text-center" style="vertical-align: middle; width: 10px; white-space: nowrap"><?=$rrenja[COL_TAHUN]?></th>
      <!--ANGGARAN-->
      <th class="text-center" style="vertical-align: middle; width: 10px; white-space: nowrap">TW. I</th>
      <th class="text-center" style="vertical-align: middle; width: 10px; white-space: nowrap">TW. II</th>
      <th class="text-center" style="vertical-align: middle; width: 10px; white-space: nowrap">TW. III</th>
      <th class="text-center" style="vertical-align: middle; width: 10px; white-space: nowrap">TW. IV</th>
      <th class="text-center" style="vertical-align: middle; width: 10px; white-space: nowrap"><?=$rrenja[COL_TAHUN]?></th>
      <!--CAPAIAN-->
      <th class="text-center" style="vertical-align: middle; width: 10px; white-space: nowrap">TW. I</th>
      <th class="text-center" style="vertical-align: middle; width: 10px; white-space: nowrap">TW. II</th>
      <th class="text-center" style="vertical-align: middle; width: 10px; white-space: nowrap">TW. III</th>
      <th class="text-center" style="vertical-align: middle; width: 10px; white-space: nowrap">TW. IV</th>
      <!--BELANJA-->
      <th class="text-center" style="vertical-align: middle; width: 10px; white-space: nowrap">TW. I</th>
      <th class="text-center" style="vertical-align: middle; width: 10px; white-space: nowrap">TW. II</th>
      <th class="text-center" style="vertical-align: middle; width: 10px; white-space: nowrap">TW. III</th>
      <th class="text-center" style="vertical-align: middle; width: 10px; white-space: nowrap">TW. IV</th>
    </tr>
  </thead>
  <tbody class="text-sm">
    <?php
    foreach($rperubahan as $per) {
      $arrIndikator = array();
      if(!empty($per[COL_NMINDIKATOR])) {
        $arrIndikator = explode(",", $per[COL_NMINDIKATOR]);
      }

      $rkegiatan = array();
      if($rrenja[COL_NMTYPE]=='INSTANSI') {
        $rkegiatan = $this->db
        ->select('rb_renjadet.NmTahapan')
        ->join(TBL_RB_RENJAPERUBAHAN.' per','per.'.COL_UNIQ." = ".TBL_RB_RENJADET.".".COL_IDPERUBAHAN,"left")
        ->join(TBL_RB_RENJA.' ren','ren.'.COL_UNIQ." = ".TBL_RB_RENJADET.".".COL_IDRENJA,"left")
        ->where('ren.'.COL_TAHUN, $rrenja[COL_TAHUN])
        //->where('ren.'.COL_NMTYPE, 'UNIT')
        ->where('ren.'.COL_NMKATEGORI, $rrenja[COL_NMKATEGORI])
        ->where('per.'.COL_NMPERUBAHAN, $per[COL_NMPERUBAHAN])
        ->order_by('rb_renjadet.'.COL_NMTAHAPAN)
        ->group_by('rb_renjadet.'.COL_NMTAHAPAN)
        ->get(TBL_RB_RENJADET)
        ->result_array();
      } else {
        $rkegiatan = $this->db
        ->where(COL_IDPERUBAHAN, $per[COL_UNIQ])
        ->get(TBL_RB_RENJADET)
        ->result_array();
      }

      ?>
      <tr>
        <td colspan="21" class="font-weight-bold"><?=$per[COL_NMPERUBAHAN]?><?=!empty($arrIndikator)?' - <small class="font-italic">'.implode($arrIndikator, ", ").'</small>':''?></td>
      </tr>
      <?php
      foreach($rkegiatan as $r) {
        $q = '';
        $numTarget = '';
        $nmSatuan = '';
        $nmIndikator = '';

        $numAnggaranTw1 = 0;
        $numAnggaranTw2 = 0;
        $numAnggaranTw3 = 0;
        $numAnggaranTw4 = 0;
        $numBelanjaTw1 = 0;
        $numBelanjaTw2 = 0;
        $numBelanjaTw3 = 0;
        $numBelanjaTw4 = 0;
        if($rrenja[COL_NMTYPE]=='INSTANSI') {
          $rkeg = $this->db
          ->join(TBL_RB_RENJA.' ren','ren.'.COL_UNIQ." = ".TBL_RB_RENJADET.".".COL_IDRENJA,"left")
          ->where('ren.'.COL_ISDELETED, 0)
          ->where('ren.'.COL_TAHUN, $rrenja[COL_TAHUN])
          //->where('ren.'.COL_NMTYPE, 'INSTANSI')
          ->where('ren.'.COL_NMKATEGORI, $rrenja[COL_NMKATEGORI])
          ->where(TBL_RB_RENJADET.'.'.COL_NMTAHAPAN, $r[COL_NMTAHAPAN])
          ->order_by('ren.'.COL_NMTYPE)
          ->get(TBL_RB_RENJADET)
          ->row_array();
          $q = $this->db->last_query();
          if(!empty($rkeg)) {
            $numTarget = $rkeg[COL_NMTARGET];
            $nmSatuan = $rkeg[COL_NMSATUAN];
            $nmIndikator = $rkeg[COL_NMINDIKATOR];
          }

          $rmonevTW1 = $this->db
          ->join(TBL_RB_RENJADET.' det','det.'.COL_UNIQ." = ".TBL_RB_RENJAMONEV.".".COL_IDTAHAPAN,"left")
          ->join(TBL_RB_RENJAPERUBAHAN.' per','per.'.COL_UNIQ." = det.".COL_IDPERUBAHAN,"left")
          ->join(TBL_RB_RENJA.' ren','ren.'.COL_UNIQ." = det.".COL_IDRENJA,"left")
          ->where('ren.'.COL_ISDELETED, 0)
          ->where('ren.'.COL_TAHUN, $rrenja[COL_TAHUN])
          //->where('ren.'.COL_NMTYPE, 'INSTANSI')
          ->where('ren.'.COL_NMKATEGORI, $rrenja[COL_NMKATEGORI])
          ->where('per.'.COL_NMPERUBAHAN, $per[COL_NMPERUBAHAN])
          ->where('det.'.COL_NMTAHAPAN, $r[COL_NMTAHAPAN])
          ->where('MonevPeriod in (1,2,3)')
          ->order_by('ren.'.COL_NMTYPE)
          ->get(TBL_RB_RENJAMONEV)
          ->row_array();
          $rmonevTW2 = $this->db
          ->join(TBL_RB_RENJADET.' det','det.'.COL_UNIQ." = ".TBL_RB_RENJAMONEV.".".COL_IDTAHAPAN,"left")
          ->join(TBL_RB_RENJAPERUBAHAN.' per','per.'.COL_UNIQ." = det.".COL_IDPERUBAHAN,"left")
          ->join(TBL_RB_RENJA.' ren','ren.'.COL_UNIQ." = det.".COL_IDRENJA,"left")
          ->where('ren.'.COL_ISDELETED, 0)
          ->where('ren.'.COL_TAHUN, $rrenja[COL_TAHUN])
          //->where('ren.'.COL_NMTYPE, 'INSTANSI')
          ->where('ren.'.COL_NMKATEGORI, $rrenja[COL_NMKATEGORI])
          ->where('per.'.COL_NMPERUBAHAN, $per[COL_NMPERUBAHAN])
          ->where('det.'.COL_NMTAHAPAN, $r[COL_NMTAHAPAN])
          ->where('MonevPeriod in (4,5,6)')
          ->order_by('ren.'.COL_NMTYPE)
          ->get(TBL_RB_RENJAMONEV)
          ->row_array();
          $rmonevTW3 = $this->db
          ->join(TBL_RB_RENJADET.' det','det.'.COL_UNIQ." = ".TBL_RB_RENJAMONEV.".".COL_IDTAHAPAN,"left")
          ->join(TBL_RB_RENJAPERUBAHAN.' per','per.'.COL_UNIQ." = det.".COL_IDPERUBAHAN,"left")
          ->join(TBL_RB_RENJA.' ren','ren.'.COL_UNIQ." = det.".COL_IDRENJA,"left")
          ->where('ren.'.COL_ISDELETED, 0)
          ->where('ren.'.COL_TAHUN, $rrenja[COL_TAHUN])
          //->where('ren.'.COL_NMTYPE, 'INSTANSI')
          ->where('ren.'.COL_NMKATEGORI, $rrenja[COL_NMKATEGORI])
          ->where('per.'.COL_NMPERUBAHAN, $per[COL_NMPERUBAHAN])
          ->where('det.'.COL_NMTAHAPAN, $r[COL_NMTAHAPAN])
          ->where('MonevPeriod in (7,8,9)')
          ->order_by('ren.'.COL_NMTYPE)
          ->get(TBL_RB_RENJAMONEV)
          ->row_array();
          $rmonevTW4 = $this->db
          ->join(TBL_RB_RENJADET.' det','det.'.COL_UNIQ." = ".TBL_RB_RENJAMONEV.".".COL_IDTAHAPAN,"left")
          ->join(TBL_RB_RENJAPERUBAHAN.' per','per.'.COL_UNIQ." = det.".COL_IDPERUBAHAN,"left")
          ->join(TBL_RB_RENJA.' ren','ren.'.COL_UNIQ." = det.".COL_IDRENJA,"left")
          ->where('ren.'.COL_ISDELETED, 0)
          ->where('ren.'.COL_TAHUN, $rrenja[COL_TAHUN])
          //->where('ren.'.COL_NMTYPE, 'INSTANSI')
          ->where('ren.'.COL_NMKATEGORI, $rrenja[COL_NMKATEGORI])
          ->where('per.'.COL_NMPERUBAHAN, $per[COL_NMPERUBAHAN])
          ->where('det.'.COL_NMTAHAPAN, $r[COL_NMTAHAPAN])
          ->where('MonevPeriod in (10,11,12)')
          ->order_by('ren.'.COL_NMTYPE)
          ->get(TBL_RB_RENJAMONEV)
          ->row_array();

          $rsumAnggaran = $this->db
          ->select('sum(BudgetTW1) as BudgetTW1,sum(BudgetTW2) as BudgetTW2,sum(BudgetTW3) as BudgetTW3,sum(BudgetTW4) as BudgetTW4')
          ->join(TBL_RB_RENJA.' ren','ren.'.COL_UNIQ." = ".TBL_RB_RENJADET.".".COL_IDRENJA,"left")
          ->where(TBL_RB_RENJADET.'.'.COL_NMTAHAPAN, $r[COL_NMTAHAPAN])
          ->where('ren.'.COL_ISDELETED, 0)
          ->where('ren.'.COL_TAHUN, $rrenja[COL_TAHUN])
          ->get(TBL_RB_RENJADET)
          ->row_array();

          $rsumBelanja1 = $this->db
          ->select('sum(MonevBelanja) as MonevBelanja')
          ->join(TBL_RB_RENJA.' ren','ren.'.COL_UNIQ." = ".TBL_RB_RENJAMONEV.".".COL_IDRENJA,"left")
          ->join(TBL_RB_RENJADET.' det','det.'.COL_UNIQ." = ".TBL_RB_RENJAMONEV.".".COL_IDTAHAPAN,"left")
          ->where('det.'.COL_NMTAHAPAN, $r[COL_NMTAHAPAN])
          ->where('ren.'.COL_ISDELETED, 0)
          ->where('ren.'.COL_TAHUN, $rrenja[COL_TAHUN])
          ->where('MonevPeriod in (1,2,3)')
          ->get(TBL_RB_RENJAMONEV)
          ->row_array();
          $rsumBelanja2 = $this->db
          ->select('sum(MonevBelanja) as MonevBelanja')
          ->join(TBL_RB_RENJA.' ren','ren.'.COL_UNIQ." = ".TBL_RB_RENJAMONEV.".".COL_IDRENJA,"left")
          ->join(TBL_RB_RENJADET.' det','det.'.COL_UNIQ." = ".TBL_RB_RENJAMONEV.".".COL_IDTAHAPAN,"left")
          ->where('det.'.COL_NMTAHAPAN, $r[COL_NMTAHAPAN])
          ->where('ren.'.COL_ISDELETED, 0)
          ->where('ren.'.COL_TAHUN, $rrenja[COL_TAHUN])
          ->where('MonevPeriod in (4,5,6)')
          ->get(TBL_RB_RENJAMONEV)
          ->row_array();
          $rsumBelanja3 = $this->db
          ->select('sum(MonevBelanja) as MonevBelanja')
          ->join(TBL_RB_RENJA.' ren','ren.'.COL_UNIQ." = ".TBL_RB_RENJAMONEV.".".COL_IDRENJA,"left")
          ->join(TBL_RB_RENJADET.' det','det.'.COL_UNIQ." = ".TBL_RB_RENJAMONEV.".".COL_IDTAHAPAN,"left")
          ->where('det.'.COL_NMTAHAPAN, $r[COL_NMTAHAPAN])
          ->where('ren.'.COL_ISDELETED, 0)
          ->where('ren.'.COL_TAHUN, $rrenja[COL_TAHUN])
          ->where('MonevPeriod in (7,8,9)')
          ->get(TBL_RB_RENJAMONEV)
          ->row_array();
          $rsumBelanja4 = $this->db
          ->select('sum(MonevBelanja) as MonevBelanja')
          ->join(TBL_RB_RENJA.' ren','ren.'.COL_UNIQ." = ".TBL_RB_RENJAMONEV.".".COL_IDRENJA,"left")
          ->join(TBL_RB_RENJADET.' det','det.'.COL_UNIQ." = ".TBL_RB_RENJAMONEV.".".COL_IDTAHAPAN,"left")
          ->where('det.'.COL_NMTAHAPAN, $r[COL_NMTAHAPAN])
          ->where('ren.'.COL_ISDELETED, 0)
          ->where('ren.'.COL_TAHUN, $rrenja[COL_TAHUN])
          ->where('MonevPeriod in (10,11,12)')
          ->get(TBL_RB_RENJAMONEV)
          ->row_array();

          $numAnggaranTw1 = !empty($rsumAnggaran[COL_BUDGETTW1])?$rsumAnggaran[COL_BUDGETTW1]:0;
          $numAnggaranTw2 = !empty($rsumAnggaran[COL_BUDGETTW2])?$rsumAnggaran[COL_BUDGETTW2]:0;
          $numAnggaranTw3 = !empty($rsumAnggaran[COL_BUDGETTW3])?$rsumAnggaran[COL_BUDGETTW3]:0;
          $numAnggaranTw4 = !empty($rsumAnggaran[COL_BUDGETTW4])?$rsumAnggaran[COL_BUDGETTW4]:0;
          $numBelanjaTw1 = !empty($rsumBelanja1[COL_MONEVBELANJA])?$rsumBelanja1[COL_MONEVBELANJA]:0;
          $numBelanjaTw2 = !empty($rsumBelanja2[COL_MONEVBELANJA])?$rsumBelanja2[COL_MONEVBELANJA]:0;
          $numBelanjaTw3 = !empty($rsumBelanja3[COL_MONEVBELANJA])?$rsumBelanja3[COL_MONEVBELANJA]:0;
          $numBelanjaTw4 = !empty($rsumBelanja4[COL_MONEVBELANJA])?$rsumBelanja4[COL_MONEVBELANJA]:0;
        } else {
          $numTarget = $r[COL_NMTARGET];
          $nmSatuan = $r[COL_NMSATUAN];
          $nmIndikator = $r[COL_NMINDIKATOR];

          $rmonevTW1 = $this->db
          ->where(COL_IDTAHAPAN, $r[COL_UNIQ])
          ->where('MonevPeriod in (1,2,3)')
          ->get(TBL_RB_RENJAMONEV)
          ->row_array();
          $rmonevTW2 = $this->db
          ->where(COL_IDTAHAPAN, $r[COL_UNIQ])
          ->where('MonevPeriod in (4,5,6)')
          ->get(TBL_RB_RENJAMONEV)
          ->row_array();
          $rmonevTW3 = $this->db
          ->where(COL_IDTAHAPAN, $r[COL_UNIQ])
          ->where('MonevPeriod in (7,8,9)')
          ->get(TBL_RB_RENJAMONEV)
          ->row_array();
          $rmonevTW4 = $this->db
          ->where(COL_IDTAHAPAN, $r[COL_UNIQ])
          ->where('MonevPeriod in (10,11,12)')
          ->get(TBL_RB_RENJAMONEV)
          ->row_array();

          $numAnggaranTw1 = !empty($r[COL_BUDGETTW1])?$r[COL_BUDGETTW1]:0;
          $numAnggaranTw2 = !empty($r[COL_BUDGETTW2])?$r[COL_BUDGETTW2]:0;
          $numAnggaranTw3 = !empty($r[COL_BUDGETTW3])?$r[COL_BUDGETTW3]:0;
          $numAnggaranTw4 = !empty($r[COL_BUDGETTW4])?$r[COL_BUDGETTW4]:0;
          $numBelanjaTw1 = !empty($rmonevTW1)?$rmonevTW1[COL_MONEVBELANJA]:0;
          $numBelanjaTw2 = !empty($rmonevTW2)?$rmonevTW2[COL_MONEVBELANJA]:0;
          $numBelanjaTw3 = !empty($rmonevTW3)?$rmonevTW3[COL_MONEVBELANJA]:0;
          $numBelanjaTw4 = !empty($rmonevTW4)?$rmonevTW4[COL_MONEVBELANJA]:0;
        }
        ?>
        <tr>
          <td style="vertical-align: middle; padding-left: 2.5rem !important"><?=$r[COL_NMTAHAPAN]?></td>
          <td style="vertical-align: middle"><?=$nmIndikator?></td>
          <td style="vertical-align: middle;">
            <?=strtoupper($nmSatuan)?>
          </td>
          <td class="text-center" style="vertical-align: middle; width: 10px; white-space: nowrap">
            <?=!empty($rmonevTW1)?$rmonevTW1[COL_MONEVTARGET]:''?>
          </td>
          <td class="text-center" style="vertical-align: middle; width: 10px; white-space: nowrap">
            <?=!empty($rmonevTW2)?$rmonevTW2[COL_MONEVTARGET]:''?>
          </td>
          <td class="text-center" style="vertical-align: middle; width: 10px; white-space: nowrap">
            <?=!empty($rmonevTW3)?$rmonevTW3[COL_MONEVTARGET]:''?>
          </td>
          <td class="text-center" style="vertical-align: middle; width: 10px; white-space: nowrap">
            <?=!empty($rmonevTW4)?$rmonevTW4[COL_MONEVTARGET]:''?>
          </td>
          <td class="text-center" style="vertical-align: middle; width: 10px; white-space: nowrap">
            <?=$numTarget?>
          </td>
          <td class="text-center" style="vertical-align: middle; width: 10px; white-space: nowrap">
            <?=$numAnggaranTw1?>
          </td>
          <td class="text-center" style="vertical-align: middle; width: 10px; white-space: nowrap">
            <?=$numAnggaranTw2?>
          </td>
          <td class="text-center" style="vertical-align: middle; width: 10px; white-space: nowrap">
            <?=$numAnggaranTw3?>
          </td>
          <td class="text-center" style="vertical-align: middle; width: 10px; white-space: nowrap">
            <?=$numAnggaranTw4?>
          </td>
          <td class="text-center" style="vertical-align: middle; width: 10px; white-space: nowrap">
            <?=$numAnggaranTw1+$numAnggaranTw2+$numAnggaranTw3+$numAnggaranTw4?>
          </td>
          <td class="text-center" style="vertical-align: middle; width: 10px; white-space: nowrap">
            <?=!empty($rmonevTW1)?$rmonevTW1[COL_MONEVCAPAIAN]:''?>
          </td>
          <td class="text-center" style="vertical-align: middle; width: 10px; white-space: nowrap">
            <?=!empty($rmonevTW2)?$rmonevTW2[COL_MONEVCAPAIAN]:''?>
          </td>
          <td class="text-center" style="vertical-align: middle; width: 10px; white-space: nowrap">
            <?=!empty($rmonevTW3)?$rmonevTW3[COL_MONEVCAPAIAN]:''?>
          </td>
          <td class="text-center" style="vertical-align: middle; width: 10px; white-space: nowrap">
            <?=!empty($rmonevTW4)?$rmonevTW4[COL_MONEVCAPAIAN]:''?>
          </td>
          <td class="text-center" style="vertical-align: middle; width: 10px; white-space: nowrap">
            <?=$numBelanjaTw1?>
          </td>
          <td class="text-center" style="vertical-align: middle; width: 10px; white-space: nowrap">
            <?=$numBelanjaTw2?>
          </td>
          <td class="text-center" style="vertical-align: middle; width: 10px; white-space: nowrap">
            <?=$numBelanjaTw3?>
          </td>
          <td class="text-center" style="vertical-align: middle; width: 10px; white-space: nowrap">
            <?=$numBelanjaTw4?>
          </td>
        </tr>
        <?php
      }
    }
    ?>
  </tbody>
</table>
