<?php
$rperubahan = $this->db
->where(COL_IDRENJA, $rrenja[COL_UNIQ])
->order_by(COL_UNIQ)
->get(TBL_RB_RENJAPERUBAHAN)
->result_array();

?>
<div class="row">
  <div class="col-sm-12">
    <?php
    if($tipe=='bulan' && !empty($_GET['Period'])) {
      ?>
      <table class="table table-bordered tbl-det mb-0" style="border-right: none !important; border-left: none !important" style="width: 100%">
        <thead class="text-sm bg-teal">
          <tr>
            <th class="text-center" style="vertical-align: middle">RENCANA AKSI</th>
            <th class="text-center" style="vertical-align: middle">INDIKATOR</th>
            <th class="text-center" style="vertical-align: middle">OUTPUT</th>
            <th class="text-center" style="vertical-align: middle; width: 10px; white-space: nowrap">SATUAN</th>
            <th class="text-center" style="vertical-align: middle; width: 10px; white-space: nowrap">TARGET</th>
            <th class="text-center" style="vertical-align: middle; width: 10px; white-space: nowrap">CAPAIAN</th>
            <th>CATATAN</th>
          </tr>
        </thead>
        <tbody class="text-sm">
          <?php
          foreach($rperubahan as $per) {
            $rkegiatan = $this->db
            ->where(COL_IDPERUBAHAN, $per[COL_UNIQ])
            ->get(TBL_RB_RENJADET)
            ->result_array();
            ?>
            <tr>
              <td colspan="7" class="font-weight-bold"><?=$per[COL_NMPERUBAHAN]?></td>
            </tr>
            <?php
            foreach($rkegiatan as $r) {
              $txtPeriod = $r[COL_PERIODTARGET];
              $arrPeriod = array();
              if(!empty($txtPeriod)) {
                $arrPeriod = explode(",", $txtPeriod);
              }
              if(!in_array($_GET['Period'], $arrPeriod)) continue;

              $rmonev = $this->db
              ->where(COL_IDTAHAPAN, $r[COL_UNIQ])
              ->where(COL_MONEVPERIOD, $_GET['Period'])
              ->get(TBL_RB_RENJAMONEV)
              ->row_array();
              ?>
              <tr>
                <td style="vertical-align: middle; padding-left: 2.5rem !important"><?=$r[COL_NMTAHAPAN]?></td>
                <td style="vertical-align: middle"><?=$r[COL_NMINDIKATOR]?></td>
                <td style="vertical-align: middle"><?=$r[COL_NMOUTPUT]?></td>
                <td style="vertical-align: middle; width: 10px; white-space: nowrap">
                  <?=strtoupper($r[COL_NMSATUAN])?>
                </td>
                <td class="text-center" style="vertical-align: middle; width: 10px; white-space: nowrap">
                  <?=$r[COL_NMTARGET]?>
                </td>
                <td class="text-center" style="vertical-align: middle; width: 10px; white-space: nowrap">
                  <?=!empty($rmonev)?$rmonev[COL_MONEVCAPAIAN]:'-'?>
                </td>
                <td style="vertical-align: middle">
                  <?=!empty($rmonev)?$rmonev[COL_MONEVKETERANGAN]:'-'?>
                </td>
              </tr>
              <?php
            }
          }
          ?>
        </tbody>
      </table>
      <?php
    } else {
      ?>
      <table class="table table-bordered tbl-det mb-0" style="border-right: none !important; border-left: none !important" style="width: 100%">
        <thead class="text-sm bg-teal">
          <tr>
            <th rowspan="2" class="text-center" style="vertical-align: middle">RENCANA AKSI / INDIKATOR / OUTPUT</th>
            <th rowspan="2" class="text-center" style="vertical-align: middle; width: 10px; white-space: nowrap">SATUAN</th>
            <th rowspan="2" class="text-center" style="vertical-align: middle; width: 10px; white-space: nowrap">TARGET</th>
            <th colspan="12" class="text-center" style="vertical-align: middle; width: 10px; white-space: nowrap">CAPAIAN</th>
            <tr>
              <?php
              for($i=1;$i<=12;$i++) {
                ?>
                <th style="width: 10px; white-space: nowrap; padding-left: .75rem !important; padding-right: .75rem !important"><?=str_pad($i,2,'0', STR_PAD_LEFT)?></th>
                <?php
              }
              ?>
            </tr>
          </tr>
        </thead>
        <tbody class="text-sm">
          <?php
          foreach($rperubahan as $per) {
            $rkegiatan = $this->db
            ->where(COL_IDPERUBAHAN, $per[COL_UNIQ])
            ->get(TBL_RB_RENJADET)
            ->result_array();
            ?>
            <tr>
              <td colspan="15" class="font-weight-bold"><?=$per[COL_NMPERUBAHAN]?></td>
            </tr>
            <?php
            foreach($rkegiatan as $r) {
              ?>
              <tr>
                <td style="vertical-align: middle; padding-left: 2.5rem !important"><?=$r[COL_NMTAHAPAN]?></td>
                <td style="vertical-align: middle; width: 10px; white-space: nowrap">
                  <?=strtoupper($r[COL_NMSATUAN])?>
                </td>
                <td class="text-center" style="vertical-align: middle; width: 10px; white-space: nowrap">
                  <?=$r[COL_NMTARGET]?>
                </td>
                <?php
                for($i=1;$i<=12;$i++) {
                  $rmonev = $this->db
                  ->where(COL_IDTAHAPAN, $r[COL_UNIQ])
                  ->where(COL_MONEVPERIOD, $i)
                  ->get(TBL_RB_RENJAMONEV)
                  ->row_array();
                  ?>
                  <td style="vertical-align: middle; width: 10px; white-space: nowrap; padding-left: .75rem !important; padding-right: .75rem !important"><?=!empty($rmonev)?$rmonev[COL_MONEVCAPAIAN]:'-'?></td>
                  <?php
                }
                ?>
              </tr>
              <?php
            }
          }
          ?>
        </tbody>
      </table>
      <?php
    }
    ?>
  </div>
</div>
