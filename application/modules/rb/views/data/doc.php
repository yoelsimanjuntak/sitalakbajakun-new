<style>
label[for="file"].error {
  display: none !important;
}
</style>

<?php
$rdoc = $this->db
->where(COL_IDRENJA, $data[COL_UNIQ])
->where(COL_MONEVPERIOD, $period)
->order_by(COL_CREATEDON, 'desc')
->get(TBL_RB_DOC)
->result_array();
?>
<div class="modal-header">
  <h5 class="modal-title">DOKUMEN <?=strtoupper($title)?></h5>
  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
    <span aria-hidden="true"><i class="fas fa-close"></i></span>
  </button>
</div>
<div class="modal-body">
  <div class="row">
    <div class="col-sm-12">
      <table class="table table-bordered">
        <thead>
          <tr>
            <th>NO.</th>
            <th>NAMA</th>
            <th>TANGGAL</th>
            <th>AKSI</th>
          </tr>
        </thead>
        <tbody>
          <?php
          if(!empty($rdoc)) {
            $no=1;
            foreach($rdoc as $r) {
              ?>
              <tr>
                <td class="text-right" style="width: 10px; white-space: nowrap"><?=$no?>.</td>
                <td>
                  <a href="<?=MY_UPLOADURL.$r[COL_DOCURL]?>" target="_blank"><?=$r[COL_DOCNAME]?></a>
                </td>
                <td class="text-right" style="width: 10px; white-space: nowrap"><?=date('d-m-Y H:i', strtotime($r[COL_CREATEDON]))?></td>
                <td class="text-center" style="width: 10px; white-space: nowrap">
                  <a href="<?=site_url('rb/data/doc-delete/'.$r[COL_UNIQ])?>" class="btn btn-xs btn-danger btn-doc-delete"><i class="far fa-times-circle"></i></a>
                </td>
              </tr>
              <?php
              $no++;
            }
          } else {
            ?>
            <tr>
              <td colspan="4" class="font-italic text-center">
                BELUM ADA DATA
              </td>
            </tr>
            <?php
          }
          ?>
        </tbody>
      </table>
    </div>
  </div>

  <div class="row mt-3">
    <div class="col-sm-12">
      <h5 class="font-weight-bold" style="text-decoration: underline">UNGGAH DOKUMEN BARU</h5>
      <form id="form-upload" action="<?=current_url()?>" class="form-horizontal">
        <div class="form-group">
          <label class="text-sm">NAMA DOKUMEN</label>
          <input type="text" class="form-control" placeholder="NAMA DOKUMEN" name="<?=COL_DOCNAME?>" required />
        </div>
        <div class="form-group">
          <label class="text-sm">UNGGAH</label>
          <div id="div-attachment">
            <div class="input-group mb-2">
              <div class="input-group-prepend">
                <span class="input-group-text"><i class="fad fa-paperclip"></i></span>
              </div>
              <div class="custom-file">
                <input type="file" class="custom-file-input" name="file" accept="image/*,application/pdf,application/vnd.ms-excel,.xlsx">
                <label class="custom-file-label" for="file">PILIH FILE</label>
              </div>
            </div>
            <p class="text-sm text-muted mb-0 font-italic">
              <strong>CATATAN:</strong><br />
              - Besar file / dokumen maksimum <strong>5 MB</strong><br />
              - Jenis file / dokumen yang diperbolehkan hanya dalam format <strong>JPG / JPEG</strong>, <strong>PNG</strong>, <strong>PDF</strong>, <strong>DOC / DOCX</strong>, <strong>XLS / XLSX</strong>
            </p>
          </div>
        </div>
        <div class="form-group" style="border-top: 1px solid #dedede">
          <div class="row">
            <div class="col-lg-12 pt-3">
              <button type="submit" class="btn btn-primary btn-sm btn-submit"><i class="far fa-check-circle"></i>&nbsp;SUBMIT</button>
            </div>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>
<script type="text/javascript">
$(document).ready(function(){
  bsCustomFileInput.init();
});
</script>
