<?php
$arrPeriod = array();
if(isset($data)) {
  $arrPeriod = explode(",", $data[COL_PERIODTARGET]);
}

$arrgrp = $this->db
->select(COL_NMKEGIATAN)
->where(COL_IDPERUBAHAN, $rperubahan[COL_UNIQ])
->group_by(COL_NMKEGIATAN)
->get(TBL_RB_RENJADET)
->result_array();
?>
<form id="form-detail" method="post" action="<?=current_url()?>">
<div class="modal-header">
  <h5 class="modal-title"><?=$mode=='edit'?'UBAH':'TAMBAH'?> RENCANA AKSI</h5>
  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
      <span aria-hidden="true"><i class="fa fa-close"></i></span>
  </button>
</div>
<div class="modal-body">
  <div class="row">
    <div class="col-sm-12">
      <?php
      if($rperubahan[COL_NMKATEGORI]=='GENERAL') {
        ?>
        <div class="form-group">
          <label>KEGIATAN UTAMA</label>
          <select name="<?=COL_NMKEGIATAN?>" class="form-control no-select2 with-tags" style="width: 100%">
            <?php
            foreach($arrgrp as $grp) {
              ?>
              <option value="<?=$grp[COL_NMKEGIATAN]?>" <?=isset($data)&&$data[COL_NMKEGIATAN]==$grp[COL_NMKEGIATAN]?'selected':''?>><?=$grp[COL_NMKEGIATAN]?></option>
              <?php
            }
            ?>
          </select>
          <p class="text-sm text-muted font-italic m-0 mt-2">NB: Kegiatan utama dapat ditambahkan jika belum tersedia pada opsi diatas.</p>
        </div>
        <?php
      }
      ?>
      <?php
      if($rperubahan[COL_NMKATEGORI]=='TEMATIK' && $rrenja[COL_NMTYPE]=='UNIT') {
        $rrenjinduk = $this->db
        ->select(TBL_RB_RENJAPERUBAHAN.'.*',TBL_RB_RENJA.'.'.COL_IDRENJA)
        ->join(TBL_RB_RENJA,TBL_RB_RENJA.'.'.COL_UNIQ." = ".TBL_RB_RENJAPERUBAHAN.".".COL_IDRENJA,"inner")
        ->where(TBL_RB_RENJA.'.'.COL_TAHUN, $rrenja[COL_TAHUN])
        ->where(TBL_RB_RENJA.'.'.COL_NMTYPE, 'INSTANSI')
        ->where(TBL_RB_RENJAPERUBAHAN.'.'.COL_NMPERUBAHAN, $rperubahan[COL_NMPERUBAHAN])
        ->get(TBL_RB_RENJAPERUBAHAN)
        ->row_array();

        $_idRenja = !empty($rrenjinduk[COL_IDRENJA])?$rrenjinduk[COL_IDRENJA]:null;
        $_idPerubahan = !empty($rrenjinduk[COL_UNIQ])?$rrenjinduk[COL_UNIQ]:null;
        ?>
        <div class="form-group">
          <label>KEGIATAN UTAMA</label>
          <select class="form-control" name="<?=COL_NMKEGIATAN?>" style="width: 100%" >
            <?=GetCombobox("SELECT * FROM rb_renjadet where IdRenja = $_idRenja and IdPerubahan = $_idPerubahan ORDER BY NmTahapan", COL_UNIQ, COL_NMTAHAPAN, (isset($data)?$data[COL_NMKEGIATAN]:null), true, false, '-- Pilih --')?>
          </select>
          <p class="text-sm text-muted font-italic m-0 mt-2">NB: Kegiatan utama dipilih berdasarkan kegiatan yang tertuang dalam Rencana Aksi RB Tematik Tingkat Kabupaten / Kota.</p>
        </div>
        <?php
      }
      ?>
      <div class="row">
        <div class="col-sm-6">
          <div class="form-group">
            <label><?=$rperubahan[COL_NMKATEGORI]=='GENERAL'?'RENCANA AKSI':'RENCANA AKSI'?></label>
            <textarea name="<?=COL_NMTAHAPAN?>" class="form-control" placeholder="<?=$rperubahan[COL_NMKATEGORI]=='GENERAL'?'Tahapan / Kegiatan':'Sasaran Rencana Aksi Tematik'?>"><?=isset($data)?$data[COL_NMTAHAPAN]:''?></textarea>
          </div>
        </div>
        <div class="col-sm-6">
          <div class="form-group">
            <label>INDIKATOR / OUTPUT</label>
            <textarea name="<?=COL_NMINDIKATOR?>" class="form-control" placeholder="Indikator"><?=isset($data)?$data[COL_NMINDIKATOR]:''?></textarea>
          </div>
        </div>
        <!--<div class="col-sm-6">
          <div class="form-group">
            <label>OUTPUT</label>
            <textarea name="<?=COL_NMOUTPUT?>" class="form-control" placeholder="Output Kegiatan"><?=isset($data)?$data[COL_NMOUTPUT]:''?></textarea>
          </div>
        </div>-->
      </div>
      <div class="row">
        <?php
        if($rperubahan[COL_NMKATEGORI]=='GENERAL' && $rrenja[COL_NMTYPE]=='INSTANSI') {
          ?>
          <div class="col-sm-6">
            <div class="row">
              <div class="col-sm-12">
                <div class="form-group">
                  <label>KOORDINATOR</label>
                  <input type="text" class="form-control" name="<?=COL_NMPENANGGUNGJAWAB?>" placeholder="Cth: Sekretariat Daerah" value="<?=isset($data)?$data[COL_NMPENANGGUNGJAWAB]:''?>" />
                </div>
              </div>
            </div>
          </div>
          <div class="col-sm-6">
            <div class="row">
              <div class="col-sm-12">
                <div class="form-group">
                  <label>PELAKSANA</label>
                  <input type="text" class="form-control" name="<?=COL_NMPELAKSANA?>" placeholder="Cth: Seluruh OPD" value="<?=isset($data)?$data[COL_NMPELAKSANA]:''?>" />
                </div>
              </div>
            </div>
          </div>
          <?php
        }
        ?>
        <?php
        if($rperubahan[COL_NMKATEGORI]=='GENERAL' && $rrenja[COL_NMTYPE]=='UNIT') {
          ?>
          <div class="col-sm-6">
            <div class="row">
              <div class="col-sm-12">
                <div class="form-group">
                  <label>TIPE PENGUKURAN</label>
                  <select name="<?=COL_TIPEPENGUKURAN?>" class="form-control" style="width: 100%">
                    <option value="Kuantitatif" <?=isset($data)&&$data[COL_TIPEPENGUKURAN]=='Kuantitatif'?'selected':''?>>Kuantitatif</option>
                    <option value="Kualitatif" <?=isset($data)&&$data[COL_TIPEPENGUKURAN]=='Kualitatif'?'selected':''?>>Kualitatif</option>
                  </select>
                </div>
              </div>
            </div>
          </div>
          <?php
        }
        ?>
        <div class="col-sm-6">
          <div class="form-group">
            <label>SATUAN</label>
            <input type="text" class="form-control" name="<?=COL_NMSATUAN?>" placeholder="Satuan" value="<?=isset($data)?$data[COL_NMSATUAN]:''?>" />
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-12">
          <div class="row">
            <div class="col-sm-6 pt-2 pr-4" style="background: #dedede">
              <h6 class="font-weight-bold text-center">KONDISI <?=!empty($rrenja)?$rrenja[COL_TAHUN]-1:'N-1'?></h6>
              <div class="form-group">
                <div class="row">
                  <label class="control-label col-sm-4">TARGET</label>
                  <div class="col-sm-8">
                    <input type="text" class="form-control" name="<?=COL_NUMN1TARGET?>" value="<?=isset($data)?$data[COL_NUMN1TARGET]:''?>" />
                  </div>
                </div>
              </div>
              <div class="form-group">
                <div class="row">
                  <label class="control-label col-sm-4">CAPAIAN</label>
                  <div class="col-sm-8">
                    <input type="text" class="form-control" name="<?=COL_NUMN1CAPAIAN?>" value="<?=isset($data)?$data[COL_NUMN1CAPAIAN]:''?>" />
                  </div>
                </div>
              </div>
            </div>
            <div class="col-sm-6 pt-2 pr-4" style="background: #20c997">
              <h6 class="font-weight-bold text-center">TARGET</h6>
              <div class="form-group">
                <div class="row">
                  <label class="control-label col-sm-4"><?=!empty($rrenja)?$rrenja[COL_TAHUN]:'TAHUNAN'?></label>
                  <div class="col-sm-8">
                    <input type="text" class="form-control" name="<?=COL_NMTARGET?>" value="<?=isset($data)?$data[COL_NMTARGET]:''?>" />
                  </div>
                </div>
              </div>
              <div class="form-group">
                <div class="row">
                  <label class="control-label col-sm-4"><?=!empty($rrenja)?$rrenja[COL_TAHUN]+1:'TAHUN N+1'?></label>
                  <div class="col-sm-8">
                    <input type="text" class="form-control" name="<?=COL_NUMN2TARGET?>" value="<?=isset($data)?$data[COL_NUMN2TARGET]:''?>" />
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <?php
      if($rrenja[COL_NMTYPE]=='UNIT') {
        ?>
        <div class="row mt-3">
          <div class="col-sm-12">
            <h6 class="font-weight-bold text-center">ANGGARAN</h6>
          </div>
          <div class="col-sm-12 pt-3" style="background: #03a9f4">
            <div class="row">
              <div class="col-sm-6 pr-4">
                <div class="form-group">
                  <div class="row">
                    <label class="control-label col-sm-4">TW. I</label>
                    <div class="col-sm-8">
                      <input type="text" class="form-control uang text-right" name="<?=COL_BUDGETTW1?>" placeholder="Alok. Anggaran TW. I" value="<?=isset($data)?$data[COL_BUDGETTW1]:''?>" />
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <div class="row">
                    <label class="control-label col-sm-4">TW. II</label>
                    <div class="col-sm-8">
                      <input type="text" class="form-control uang text-right" name="<?=COL_BUDGETTW2?>" placeholder="Alok. Anggaran TW. II" value="<?=isset($data)?$data[COL_BUDGETTW2]:''?>" />
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-sm-6 pr-4">
                <div class="form-group">
                  <div class="row">
                    <label class="control-label col-sm-4">TW. III</label>
                    <div class="col-sm-8">
                      <input type="text" class="form-control uang text-right" name="<?=COL_BUDGETTW3?>" placeholder="Alok. Anggaran TW. III" value="<?=isset($data)?$data[COL_BUDGETTW3]:''?>" />
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <div class="row">
                    <label class="control-label col-sm-4">TW. IV</label>
                    <div class="col-sm-8">
                      <input type="text" class="form-control uang text-right" name="<?=COL_BUDGETTW4?>" placeholder="Alok. Anggaran TW. IV" value="<?=isset($data)?$data[COL_BUDGETTW4]:''?>" />
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <?php
      }
      ?>
      <div class="form-group mt-3">
        <label>WAKTU PELAKSANAAN</label>
        <div class="row">
          <div class="col-sm-3">
            <label><input type="checkbox" name="Period[]" value="1" <?=!empty($arrPeriod)&&in_array("1", $arrPeriod)?'checked':''?> />&nbsp;&nbsp;JAN</label><br />
            <label><input type="checkbox" name="Period[]" value="5" <?=!empty($arrPeriod)&&in_array("5", $arrPeriod)?'checked':''?> />&nbsp;&nbsp;MEI</label><br />
            <label><input type="checkbox" name="Period[]" value="9" <?=!empty($arrPeriod)&&in_array("9", $arrPeriod)?'checked':''?> />&nbsp;&nbsp;SEP</label><br />
          </div>
          <div class="col-sm-3">
            <label><input type="checkbox" name="Period[]" value="2" <?=!empty($arrPeriod)&&in_array("2", $arrPeriod)?'checked':''?> />&nbsp;&nbsp;FEB</label><br />
            <label><input type="checkbox" name="Period[]" value="6" <?=!empty($arrPeriod)&&in_array("6", $arrPeriod)?'checked':''?> />&nbsp;&nbsp;JUN</label><br />
            <label><input type="checkbox" name="Period[]" value="10" <?=!empty($arrPeriod)&&in_array("10", $arrPeriod)?'checked':''?> />&nbsp;&nbsp;OKT</label><br />
          </div>
          <div class="col-sm-3">
            <label><input type="checkbox" name="Period[]" value="3" <?=!empty($arrPeriod)&&in_array("3", $arrPeriod)?'checked':''?> />&nbsp;&nbsp;MAR</label><br />
            <label><input type="checkbox" name="Period[]" value="7" <?=!empty($arrPeriod)&&in_array("7", $arrPeriod)?'checked':''?> />&nbsp;&nbsp;JUL</label><br />
            <label><input type="checkbox" name="Period[]" value="11" <?=!empty($arrPeriod)&&in_array("11", $arrPeriod)?'checked':''?> />&nbsp;&nbsp;NOV</label><br />
          </div>
          <div class="col-sm-3">
            <label><input type="checkbox" name="Period[]" value="4" <?=!empty($arrPeriod)&&in_array("4", $arrPeriod)?'checked':''?> />&nbsp;&nbsp;APR</label><br />
            <label><input type="checkbox" name="Period[]" value="8" <?=!empty($arrPeriod)&&in_array("8", $arrPeriod)?'checked':''?> />&nbsp;&nbsp;AGT</label><br />
            <label><input type="checkbox" name="Period[]" value="12" <?=!empty($arrPeriod)&&in_array("12", $arrPeriod)?'checked':''?> />&nbsp;&nbsp;DES</label><br />
          </div>
        </div>
      </div>
      <?php
      if($rperubahan[COL_NMKATEGORI]=='TEMATIK') {
        ?>
        <div class="form-group">
          <label>KETERKAITAN KEGIATAN DENGAN MASYARAKAT</label>
          <div class="row">
            <div class="col-sm-12">
              <label class="font-weight-normal"><input type="radio" name="<?=COL_NMKETERKAITAN?>" value="Langsung" <?=isset($data)&&$data[COL_NMKETERKAITAN]=='Langsung'?'checked':''?> />&nbsp;&nbsp;Langsung</label><br />
              <label class="font-weight-normal"><input type="radio" name="<?=COL_NMKETERKAITAN?>" value="Tidak Langsung" <?=isset($data)&&$data[COL_NMKETERKAITAN]=='Tidak Langsung'?'checked':''?> />&nbsp;&nbsp;Tidak Langsung</label>
            </div>
          </div>
        </div>
        <?php
      }
      ?>
    </div>
  </div>

</div>
<div class="modal-footer">
  <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal"><i class="far fa-times-circle"></i>&nbsp;BATAL</button>
  <button type="submit" class="btn btn-primary btn-sm"><i class="far fa-check-circle"></i>&nbsp;SIMPAN</button>
</div>
</form>
<script type="text/javascript">
$("select").not('.no-select2, .custom-select').select2({ width: 'resolve', theme: 'bootstrap4' });
$("[name=NmKegiatan].with-tags").select2({
  width: 'resolve',
  theme: 'bootstrap4',
  tags: true
});
$(".uang").number(true, 0, '.', ',');
</script>
