<?php
$rperubahan = $this->db
->where(COL_IDRENJA, $data[COL_UNIQ])
->order_by(COL_UNIQ)
->get(TBL_RB_RENJAPERUBAHAN)
->result_array();
?>
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h3 class="m-0 text-dark font-weight-light"><?=$title?></h3>
      </div>
      <div class="col-sm-6 text-right">
        <a href="<?=site_url('rb/data/index/'.strtolower($data[COL_NMTYPE]))?>" class="btn btn-sm btn-secondary"><i class="far fa-arrow-circle-left"></i> KEMBALI</a>
      </div>
    </div>
  </div>
</div>
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-sm-12">
        <?php
        $no=1;
        foreach($rperubahan as $d) {
          $arrKegiatan = array();
          if(!empty($d[COL_NMKEGIATAN])) {
            $arrKegiatan = explode(";", $d[COL_NMKEGIATAN]);
          }

          $rtahapan = $this->db
          ->where(COL_IDRENJA, $data[COL_UNIQ])
          ->where(COL_IDPERUBAHAN, $d[COL_UNIQ])
          ->get(TBL_RB_RENJADET)
          ->result_array();

          $arrTahapan = array();
          foreach($rtahapan as $r) {
            $obj = new stdClass();
            $obj->Uraian = $r[COL_NMTAHAPAN];
            $obj->Period = $r[COL_PERIODTARGET];
            $obj->Target = $r[COL_NMTARGET];
            $obj->Satuan = $r[COL_NMSATUAN];
            $arrTahapan[] = $obj;
          }
          ?>
          <div class="card collapsed-card" data-id="<?=$d[COL_UNIQ]?>">
            <div class="card-header">
              <h5 class="card-title"><?=$no?>. <strong><?=$d[COL_NMPERUBAHAN]?></strong></h5>
              <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-plus"></i>
                </button>
              </div>
            </div>
            <div class="card-body p-0">
              <div class="row">
                <div class="col-sm-12">
                  <?php
                  if(!empty($arrKegiatan)) {
                    ?>
                    <ul class="font-italic mt-2">
                      <?php
                      foreach($arrKegiatan as $k) {
                        ?>
                        <li><?=$k?></li>
                        <?php
                      }
                      ?>
                    </ul>
                    <?php
                  }
                  ?>
                </div>
              </div>
              <form id="form-perubahan-<?=$d[COL_UNIQ]?>" action="<?=current_url()?>">
                <input type="hidden" name="<?=COL_IDPERUBAHAN?>" value="<?=$d[COL_UNIQ]?>" />
                <!--<input type="hidden" name="ArrTahapan" value="<?=htmlspecialchars(json_encode($arrTahapan))?>" />-->
                <!--<table class="table table-striped mb-0">
                  <tr>
                    <td style="width: 100px; white-space: nowrap">Kegiatan</td>
                    <td style="width: 10px; white-space: nowrap">:</td>
                    <td class="text-sm font-italic">
                      <?php
                      if(!empty($arrKegiatan)) {
                        ?>
                        <ul style="padding-left: 10px !important">
                          <?php
                          foreach($arrKegiatan as $k) {
                            ?>
                            <li><?=$k?></li>
                            <?php
                          }
                          ?>
                        </ul>
                        <?php
                      }
                      ?>
                    </td>
                  </tr>
                  <tr>
                    <td style="width: 100px; white-space: nowrap">Output</td>
                    <td style="width: 10px; white-space: nowrap">:</td>
                    <td>
                      <input type="text" class="form-control" name="<?=COL_NMOUTPUT?>" value="<?=$d[COL_NMOUTPUT]?>" />
                    </td>
                  </tr>
                  <tr>
                    <td style="width: 100px; white-space: nowrap">Penanggung Jawab</td>
                    <td style="width: 10px; white-space: nowrap">:</td>
                    <td>
                      <input type="text" class="form-control" name="<?=COL_NMPENANGGUNGJAWAB?>" value="<?=$d[COL_NMPENANGGUNGJAWAB]?>" />
                    </td>
                  </tr>
                  <tr>
                    <td style="width: 100px; white-space: nowrap">Indikator</td>
                    <td style="width: 10px; white-space: nowrap">:</td>
                    <td>
                      <input type="text" class="form-control" name="<?=COL_NMKRITERIA?>" value="<?=$d[COL_NMKRITERIA]?>" />
                    </td>
                  </tr>
                </table>-->
                <div class="row">
                  <div class="col-sm-12">
                    <table class="table table-bordered tbl-det mb-0" style="border-right: none !important; border-left: none !important">
                      <thead class="text-sm bg-teal">
                        <tr>
                          <th rowspan="2" class="text-center" style="vertical-align: middle">RENCANA AKSI</th>
                          <th rowspan="2" class="text-center" style="vertical-align: middle">INDIKATOR</th>
                          <th rowspan="2" class="text-center" style="vertical-align: middle">OUTPUT</th>
                          <th rowspan="2" class="text-center" style="vertical-align: middle">TARGET</th>
                          <th rowspan="2" class="text-center" style="vertical-align: middle">SATUAN</th>
                          <th colspan="12" class="text-center" style="vertical-align: middle">WAKTU PELAKSANAAN</th>
                          <th rowspan="2" class="text-center" style="vertical-align: middle" style="width: 10px; white-space: nowrap">#</th>
                        </tr>
                        <tr>
                          <?php
                          for($i=1;$i<=12;$i++) {
                            ?>
                            <th style="width: 10px; white-space: nowrap"><?=str_pad($i,2,'0', STR_PAD_LEFT)?></th>
                            <?php
                          }
                          ?>
                        </tr>
                      </thead>
                      <tbody class="text-sm">

                      </tbody>
                      <!--<tfoot>
                        <tr>
                          <td colspan="2">
                            <input type="text" name="DetUraian" class="form-control form-control-sm" placeholder="Uraian Tahapan" />
                            <div class="form-group mt-2">
                              <div class="row">
                                <label class="control-label col-sm-2 offset-sm-2 text-sm">TARGET</label>
                                <div class="col-sm-4">
                                  <input type="text" name="DetTarget" class="form-control form-control-sm" placeholder="Target" />
                                </div>
                                <div class="col-sm-4">
                                  <input type="text" name="DetSatuan" class="form-control form-control-sm" placeholder="Satuan" />
                                </div>
                              </div>
                            </div>
                          </td>
                          <td style="text-align: center; vertical-align: middle"><input type="checkbox" name="DetPeriod_1" /></td>
                          <td style="text-align: center; vertical-align: middle"><input type="checkbox" name="DetPeriod_2" /></td>
                          <td style="text-align: center; vertical-align: middle"><input type="checkbox" name="DetPeriod_3" /></td>
                          <td style="text-align: center; vertical-align: middle"><input type="checkbox" name="DetPeriod_4" /></td>
                          <td style="text-align: center; vertical-align: middle"><input type="checkbox" name="DetPeriod_5" /></td>
                          <td style="text-align: center; vertical-align: middle"><input type="checkbox" name="DetPeriod_6" /></td>
                          <td style="text-align: center; vertical-align: middle"><input type="checkbox" name="DetPeriod_7" /></td>
                          <td style="text-align: center; vertical-align: middle"><input type="checkbox" name="DetPeriod_8" /></td>
                          <td style="text-align: center; vertical-align: middle"><input type="checkbox" name="DetPeriod_9" /></td>
                          <td style="text-align: center; vertical-align: middle"><input type="checkbox" name="DetPeriod_10" /></td>
                          <td style="text-align: center; vertical-align: middle"><input type="checkbox" name="DetPeriod_11" /></td>
                          <td style="text-align: center; vertical-align: middle"><input type="checkbox" name="DetPeriod_12" /></td>
                          <td style="text-align: center; vertical-align: middle; white-space: nowrap">
                            <button type="button" class="btn btn-xs btn-outline-primary btn-add-detail"><i class="far fa-plus-circle"></i>&nbsp;TAMBAH</button>
                          </td>
                        </tr>
                      </tfoot>-->
                    </table>
                  </div>
                </div>
              </form>
            </div>
            <div class="card-footer">
              <!--<button type="button" class="btn btn-primary btn-submit btn-sm"><i class="far fa-check-circle"></i>&nbsp;SIMPAN</button>-->
              <button type="button" class="btn btn-primary btn-add-detail btn-sm"><i class="far fa-plus-circle"></i>&nbsp;TAMBAH KEGIATAN</button>
            </div>
          </div>
          <?php
          $no++;
        }
        ?>
      </div>
    </div>
  </div>
</section>
<script type="text/javascript">
function delTahapan(idx, form) {
  var arrtahapan = $('[name=ArrTahapan]', form).val();
  if (arrtahapan) {
    arrtahapan = JSON.parse(arrtahapan);
  } else {
    arrtahapan = [];
  }
  if (arrtahapan) {
    if (arrtahapan[idx] !== null) {
      arrtahapan.splice(idx, 1);
    }
  }
  $('[name=ArrTahapan]', form).val(JSON.stringify(arrtahapan)).trigger('change');
}
$(document).ready(function(){
  /*$('[name=ArrTahapan]').change(function(){
    var form = $(this).closest('form');

    if(form) {
      var arrtahapan = $(this).val();
      if (arrtahapan && JSON.parse(arrtahapan).length>0) {
        var html = '';
        arrtahapan = JSON.parse(arrtahapan);

        for(i=0; i<arrtahapan.length; i++) {
          var tahap = arrtahapan[i].Uraian;
          var target = '-';
          if(arrtahapan[i].Target && arrtahapan[i].Satuan) {
            target = arrtahapan[i].Target+' ('+arrtahapan[i].Satuan+')';
          }
          var period = [];
          if(arrtahapan[i].Period) {
            period = arrtahapan[i].Period.split(",");
          }

          html += '<tr>';
          html += '<td>'+tahap+'</td>';
          html += '<td style="text-align: center; white-space: nowrap">'+target+'</td>';
          html += '<td style="text-align: center; vertical-align: middle">'+(period.includes('1')?'<i class="far fa-check-circle"></i>':'-')+'</td>';
          html += '<td style="text-align: center; vertical-align: middle">'+(period.includes('2')?'<i class="far fa-check-circle"></i>':'-')+'</td>';
          html += '<td style="text-align: center; vertical-align: middle">'+(period.includes('3')?'<i class="far fa-check-circle"></i>':'-')+'</td>';
          html += '<td style="text-align: center; vertical-align: middle">'+(period.includes('4')?'<i class="far fa-check-circle"></i>':'-')+'</td>';
          html += '<td style="text-align: center; vertical-align: middle">'+(period.includes('5')?'<i class="far fa-check-circle"></i>':'-')+'</td>';
          html += '<td style="text-align: center; vertical-align: middle">'+(period.includes('6')?'<i class="far fa-check-circle"></i>':'-')+'</td>';
          html += '<td style="text-align: center; vertical-align: middle">'+(period.includes('7')?'<i class="far fa-check-circle"></i>':'-')+'</td>';
          html += '<td style="text-align: center; vertical-align: middle">'+(period.includes('8')?'<i class="far fa-check-circle"></i>':'-')+'</td>';
          html += '<td style="text-align: center; vertical-align: middle">'+(period.includes('9')?'<i class="far fa-check-circle"></i>':'-')+'</td>';
          html += '<td style="text-align: center; vertical-align: middle">'+(period.includes('10')?'<i class="far fa-check-circle"></i>':'-')+'</td>';
          html += '<td style="text-align: center; vertical-align: middle">'+(period.includes('11')?'<i class="far fa-check-circle"></i>':'-')+'</td>';
          html += '<td style="text-align: center; vertical-align: middle">'+(period.includes('12')?'<i class="far fa-check-circle"></i>':'-')+'</td>';
          html += '<td style="text-align: center; vertical-align: middle; white-space: nowrap"><button type="button" class="btn btn-xs btn-outline-danger btn-del-detail" data-idx='+i+'><i class="far fa-times-circle"></i>&nbsp;HAPUS</button></td>';
          html += '</tr>';
        }
        $('.tbl-det>tbody', form).html(html);
        $('.btn-del-detail', $('.tbl-det>tbody', form)).click(function(){
          var idx = $(this).data('idx');
          delTahapan(idx, form);
        });
      } else {
        $('.tbl-det>tbody', form).html('<tr><td colspan="15" class="text-sm text-center font-italic">KOSONG</td></tr>');
      }
    }
  }).trigger('change');*/

  $('.btn-add-detail').click(function(){
    var form = $(this).closest('form');
    if(form) {
      var valTahapan = $('[name=ArrTahapan]', form).val();
      var arrTahapan = [];
      if (valTahapan) {
        arrTahapan = JSON.parse(valTahapan);
      }
      var detUraian = $('[name=DetUraian]', form).val();
      var detTarget = $('[name=DetTarget]', form).val();
      var detSatuan = $('[name=DetSatuan]', form).val();
      var arrPeriod = [];
      for(var i=1; i<=12; i++) {
        if($('[name=DetPeriod_'+i+']').is(':checked')) {
          arrPeriod.push(i);
        }
      }

      arrTahapan.push({Uraian: detUraian, Target: detTarget, Satuan: detSatuan, Period: arrPeriod.join(",")});
      $('[name=ArrTahapan]', form).val(JSON.stringify(arrTahapan)).trigger('change');
      $('[name=DetUraian]', form).val("");
      $('[name=DetTarget]', form).val("");
      $('[name=DetSatuan]', form).val("");
      $('[name^=DetPeriod_]', form).prop("checked", false);
    }
  });

  $('.btn-submit').click(function(){
    var card = $(this).closest('.card');
    if(card) {
      $('form',card).submit();
    }
  });

  var form = $('form');
  if(form) {
    for(var i=0; i<form.length; i++) {
      $(form[i]).validate({
        submitHandler: function(form) {
          var btnSubmit = $('button.btn-submit', form.closest('card'));
          btnSubmit.attr('disabled', true);
          $(form).ajaxSubmit({
            dataType: 'json',
            type : 'post',
            success: function(res) {
              if(res.error != 0) {
                toastr.error(res.error);
              } else {
                toastr.success(res.success);
                /*if(res.data.redirect) {
                  setTimeout(function(){
                    location.href = res.data.redirect;
                  }, 1000);
                }*/
                location.reload();
              }
            },
            error: function() {
              toastr.error('SERVER ERROR');
            },
            complete: function() {
              btnSubmit.attr('disabled', false);
            }
          });

          return false;
        }
      });
    }
  }

});
</script>
