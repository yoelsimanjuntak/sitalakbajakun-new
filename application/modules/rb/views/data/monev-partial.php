<?php
$arrKegiatan = array();
if(!empty($rdata[COL_NMKEGIATAN])) {
  $arrKegiatan = explode(";", $rdata[COL_NMKEGIATAN]);
}

$rrenja = $this->db
->where(COL_UNIQ, $rdata[COL_IDRENJA])
->get(TBL_RB_RENJA)
->row_array();

$rdet_grp = $this->db
->select(COL_NMKEGIATAN)
->where(COL_IDRENJA, $rdata[COL_IDRENJA])
->where(COL_IDPERUBAHAN, $rdata[COL_UNIQ])
->group_by(COL_NMKEGIATAN)
->get(TBL_RB_RENJADET)
->result_array();
?>
<div class="row">
  <div class="col-sm-12">
    <table class="table table-bordered tbl-det mb-0" style="border-right: none !important; border-left: none !important; width: 100%">
      <thead class="text-sm text-center">
        <tr>
          <!--<th style="width: 10px; white-space: nowrap">#</th>-->
          <th>KEGIATAN UTAMA / RINCIAN KEGIATAN</th>
          <th>INDIKATOR / OUTPUT</th>
          <th style="width: 10px; white-space: nowrap">SATUAN</th>
          <th style="width: 10px;">TARGET AKHIR</th>
          <th style="width: 10px;">TARGET PERIODE</th>
          <th style="width: 10px; white-space: nowrap">CAPAIAN</th>
          <th>CATATAN</th>
          <th style="width: 10px; white-space: nowrap">AKSI</th>
        </tr>
      </thead>
      <tbody class="text-sm">
        <?php
        if(!empty($rdet_grp)) {
          foreach($rdet_grp as $grp) {
            ?>

            <?php
            if(!empty($grp[COL_NMKEGIATAN])) {
              $nmKeg = '';
              if($rrenja[COL_NMKATEGORI]=='TEMATIK'&&$rrenja[COL_NMTYPE]=='UNIT') {
                $rrenjinduk = $this->db
                ->where(COL_UNIQ, $grp[COL_NMKEGIATAN])
                ->get(TBL_RB_RENJADET)
                ->row_array();
                if(!empty($rrenjinduk)) {
                  $nmKeg = $rrenjinduk[COL_NMTAHAPAN];
                }

              } else {
                $nmKeg = $grp[COL_NMKEGIATAN];
              }
              ?>
              <tr>
                <td colspan="8" class="font-weight-bold"><?=$nmKeg?></td>
              </tr>
              <?php
            }
            ?>

            <?php

            $rtahapan = $this->db
            ->where(COL_IDRENJA, $rdata[COL_IDRENJA])
            ->where(COL_IDPERUBAHAN, $rdata[COL_UNIQ])
            ->where(COL_NMKEGIATAN, $grp[COL_NMKEGIATAN])
            ->get(TBL_RB_RENJADET)
            ->result_array();

            foreach($rtahapan as $r) {
              $txtPeriod = $r[COL_PERIODTARGET];
              $arrPeriod = array();
              if(!empty($txtPeriod)) {
                $arrPeriod = explode(",", $txtPeriod);
              }

              $rmonev = $this->db
              ->where(COL_IDTAHAPAN, $r[COL_UNIQ])
              ->where(COL_MONEVPERIOD, $period)
              ->get(TBL_RB_RENJAMONEV)
              ->row_array();

              //if(!in_array($period, $arrPeriod)) continue;

              ?>
              <tr <?=!in_array($period, $arrPeriod)?/*'class="text-muted"'*/'':''?>>
                <!--<td style="text-align: center; vertical-align: middle"><?=in_array($period, $arrPeriod)?'<i class="far fa-check-circle"></i>':'<i class="far fa-times-circle"></i>'?></td>-->
                <td><?=$r[COL_NMTAHAPAN]?></td>
                <td><?=$r[COL_NMINDIKATOR]?></td>
                <td style="text-align: left; vertical-align: top; white-space: nowrap"><?=!empty($r[COL_NMSATUAN])?strtoupper($r[COL_NMSATUAN]):'-'?></td>
                <td style="text-align: center; vertical-align: top; white-space: nowrap"><?=!empty($r[COL_NMTARGET])?$r[COL_NMTARGET]:'-'?></td>
                <td style="text-align: center; vertical-align: top">
                  <?=!empty($rmonev)&&!empty($rmonev[COL_MONEVTARGET])?$rmonev[COL_MONEVTARGET]:'-'?>
                </td>
                <td style="text-align: center; vertical-align: top">
                  <?=!empty($rmonev)&&!empty($rmonev[COL_MONEVCAPAIAN])?$rmonev[COL_MONEVCAPAIAN]:'-'?>
                </td>
                <td style="text-align: left; vertical-align: top">
                  <?=!empty($rmonev)&&!empty($rmonev[COL_MONEVKETERANGAN])?$rmonev[COL_MONEVKETERANGAN]:'-'?>
                  <?=!empty($rmonev)&&!empty($rmonev[COL_MONEVEVIDENCE])?'<br /><br /><small class="font-italic"><strong>Evidence:</strong><br />'.$rmonev[COL_MONEVEVIDENCE].'</small>':''?>
                </td>
                <td style="text-align: center; vertical-align: top; white-space: nowrap">
                  <a href="<?=site_url('rb/data/monev-form/'.$r[COL_UNIQ].'/'.$period)?>" class="btn btn-xs btn-outline-primary btn-entry"><i class="far fa-pencil"></i>&nbsp;ENTRI</a>
                  <a href="<?=site_url('rb/data/monev-doc/'.$r[COL_UNIQ].'/'.$period)?>" class="btn btn-xs btn-outline-success btn-doc"><i class="far fa-paperclip"></i>&nbsp;LAMPIRAN</a>
                </td>
              </tr>
              <?php
            }
          }
        } else {
          ?>

          <?php
        }
        ?>
      </tbody>
    </table>
  </div>
</div>
