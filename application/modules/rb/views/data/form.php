<?php
$ruser = GetLoggedUser();
$rperubahan = $this->db
->where(COL_IDRENJA, $data[COL_UNIQ])
->order_by(COL_UNIQ)
->get(TBL_RB_RENJAPERUBAHAN)
->result_array();


?>
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-8">
        <h5 class="m-0 text-dark font-weight-light"><?=$title?></h5>
      </div>
      <div class="col-sm-4 text-right">
        <a href="<?=site_url('rb/data/index/'.strtolower($data[COL_NMTYPE]).'/'.strtolower($data[COL_NMKATEGORI]))?>" class="btn btn-sm btn-secondary"><i class="far fa-arrow-circle-left"></i> KEMBALI</a>
      </div>
    </div>
  </div>
</div>
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-sm-12">
        <?php
        $no=1;
        foreach($rperubahan as $d) {
          $arrKegiatan = array();
          if(!empty($d[COL_NMINDIKATOR])) {
            $arrKegiatan = explode(",", $d[COL_NMINDIKATOR]);
          }

          $rtahapan = $this->db
          ->where(COL_IDRENJA, $data[COL_UNIQ])
          ->where(COL_IDPERUBAHAN, $d[COL_UNIQ])
          ->get(TBL_RB_RENJADET)
          ->result_array();
          ?>
          <div class="card <?=count($rperubahan)>1?'collapsed-card':''?>" data-id="<?=$d[COL_UNIQ]?>">
            <div class="card-header">
              <h6 class="card-title" style="font-size: 12pt">
                <?=count($rperubahan)>1?$no.'. ':''?><strong><?=$d[COL_NMPERUBAHAN]?></strong><?=!empty($arrKegiatan)?' - <small class="font-italic">'.implode($arrKegiatan, ", ").'</small>':''?>
              </h6>
              <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-<?=count($rperubahan)>1?'plus':'minus'?>"></i>
                </button>
              </div>
            </div>
            <div class="card-body p-0">
              <?php
              if($d[COL_NMKATEGORI]=='TEMATIK' && $data[COL_NMTYPE]=='INSTANSI') {
                ?>
                <div class="row">
                  <div class="col-sm-6">
                    <form class="form-ajax" action="<?=site_url('rb/data/save-change-renja/'.$d[COL_UNIQ])?>" method="POST">
                      <div class="form-group mt-2 pl-4 pr-4">
                        <label>Koordinator</label>
                        <select class="form-control" name="<?=COL_IDSKPD?>" style="width: 100%" required>
                          <?=GetCombobox("SELECT * FROM sakipv2_skpd where SkpdIsAktif = 1 ORDER BY SkpdNama", COL_SKPDID, COL_SKPDNAMA, (!empty($d[COL_IDSKPD])?$d[COL_IDSKPD]:null), true, false, '-- Pilih Unit --')?>
                        </select>
                      </div>
                    </form>
                  </div>
                </div>
                <?php
              } else if($d[COL_NMKATEGORI]=='TEMATIK' && $data[COL_NMTYPE]=='UNIT') {
                $rrenjinduk = $this->db
                ->join(TBL_RB_RENJA,TBL_RB_RENJA.'.'.COL_UNIQ." = ".TBL_RB_RENJAPERUBAHAN.".".COL_IDRENJA,"inner")
                ->where(TBL_RB_RENJA.'.'.COL_TAHUN, $data[COL_TAHUN])
                ->where(TBL_RB_RENJA.'.'.COL_NMTYPE, 'INSTANSI')
                ->where(TBL_RB_RENJAPERUBAHAN.'.'.COL_NMPERUBAHAN, $d[COL_NMPERUBAHAN])
                ->get(TBL_RB_RENJAPERUBAHAN)
                ->row_array();

                $nmSkpd = '-';
                if(!empty($rrenjinduk) && !empty($rrenjinduk[COL_IDSKPD])) {
                  $rskpd = $this->db
                  ->where(COL_SKPDID, $rrenjinduk[COL_IDSKPD])
                  ->get(TBL_SAKIPV2_SKPD)
                  ->row_array();
                  if(!empty($rskpd)) {
                    $nmSkpd = $rskpd[COL_SKPDNAMA];
                  }
                }
                ?>
                <div class="row">
                  <div class="col-sm-6">
                    <form class="form-ajax" action="<?=site_url('rb/data/save-change-renja/'.$d[COL_UNIQ])?>" method="POST">
                      <div class="form-group mt-2 pl-4 pr-4">
                        <label class="m-0">Koordinator:</label>
                        <p class="m-0 font-italic"><?=$nmSkpd?></p>
                      </div>
                    </form>
                  </div>
                </div>
                <?php
              }
              ?>
              <div class="row">
                <div class="col-sm-12">
                  <table class="table table-bordered tbl-det mb-0" style="border-right: none !important; border-left: none !important" style="width: 100%">
                    <thead class="text-sm bg-teal">
                      <tr>
                        <th rowspan="2" class="text-center" style="vertical-align: middle">KEGIATAN UTAMA / RENCANA AKSI</th>
                        <th rowspan="2" class="text-center" style="vertical-align: middle">INDIKATOR / OUTPUT</th>
                        <!--<th rowspan="2" class="text-center" style="vertical-align: middle">OUTPUT</th>-->
                        <th rowspan="2" class="text-center" style="vertical-align: middle">TARGET</th>
                        <th rowspan="2" class="text-center" style="vertical-align: middle">SATUAN</th>
                        <th colspan="12" class="text-center" style="vertical-align: middle">WAKTU PELAKSANAAN</th>
                        <th rowspan="2" class="text-center" style="vertical-align: middle; width: 10px; white-space: nowrap; text-align: center; padding-left: .75rem !important; padding-right: .75rem !important">OPSI</th>
                      </tr>
                      <tr>
                        <?php
                        for($i=1;$i<=12;$i++) {
                          ?>
                          <th style="width: 10px; white-space: nowrap; padding-left: .75rem !important; padding-right: .75rem !important"><?=str_pad($i,2,'0', STR_PAD_LEFT)?></th>
                          <?php
                        }
                        ?>
                      </tr>
                    </thead>
                    <tbody class="text-sm">

                    </tbody>
                  </table>
                </div>
              </div>
            </div>
            <div class="card-footer">
              <?php
              if($ruser[COL_ROLEID]!=ROLEGUEST) {
                ?>
                <button type="button" class="btn btn-primary btn-add-detail btn-sm" data-url="<?=site_url('rb/data/detail-add/'.$d[COL_UNIQ])?>"><i class="far fa-plus-circle"></i>&nbsp;TAMBAH</button>
                <?php
              }
              ?>

              <button type="button" class="btn btn-secondary btn-refresh-detail btn-sm" data-url="<?=site_url('rb/data/detail-load/'.$d[COL_UNIQ])?>">
                <i class="far fa-refresh"></i>&nbsp;REFRESH
              </button>
              <a href="<?=site_url('rb/data/cetak/'.$data[COL_UNIQ]).'?idPerubahan='.$d[COL_NMPERUBAHAN]?>" target="_blank" class="btn btn-success btn-sm">
                <i class="far fa-print"></i>&nbsp;CETAK
              </a>
            </div>

          </div>
          <?php
          $no++;
        }
        ?>
      </div>
    </div>
  </div>
</section>
<div class="modal fade" id="modal-detail" role="dialog">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">

    </div>
  </div>
</div>
<script type="text/javascript">
var modalDetail = $("#modal-detail");

$(document).ready(function(){
  $('.btn-refresh-detail').click(function(){
    var url = $(this).data('url');
    var card = $(this).closest('.card');
    var overlay = $('<div class="overlay"><i class="far fa-2x fa-refresh fa-spin"></i></div>');
    if(url) {
      card.append(overlay);
      $('.tbl-det>tbody', card).load(url, function(){
        overlay.remove();

        $('.btn-edit-detail', card).click(function(){
          var url = $(this).data('url');
          var overlay = $('<div class="overlay"><i class="far fa-2x fa-refresh fa-spin"></i></div>');
          if(url) {
            $('.modal-content', modalDetail).load(url, function(response, status, xhr){
              if(status == "error") {
                toastr.error('MAAF, TELAH TERJADI KESALAHAN SERVER');
                return false;
              }

              modalDetail.modal('show');
              $('form', modalDetail).validate({
                submitHandler: function(form) {
                  var btnSubmit = $('button[type=submit]', form);
                  btnSubmit.attr('disabled', true);
                  $(form).ajaxSubmit({
                    dataType: 'json',
                    type : 'post',
                    success: function(res) {
                      if(res.error != 0) {
                        toastr.error(res.error);
                      } else {
                        toastr.success(res.success);
                        $('.btn-refresh-detail', card).click();
                        modalDetail.modal('hide');
                      }
                    },
                    error: function() {
                      toastr.error('SERVER ERROR');
                    },
                    complete: function() {
                      btnSubmit.attr('disabled', false);
                    }
                  });

                  return false;
                }
              });
            });
          }
        });

        $('.btn-delete-detail', card).click(function(){
          var url = $(this).data('url');
          var overlay = $('<div class="overlay"><i class="far fa-2x fa-refresh fa-spin"></i></div>');
          if(url) {
            if(confirm('Apakah anda yakin menghapus kegiatan ini?')) {
              $.ajax({
                url: url,
                type: 'GET',
                success: function(data){
                  data = JSON.parse(data);
                  if(data.error==0) toastr.success(data.success);
                  else toastr.error(data.error);
                },
                error: function(data) {
                  toastr.error('MAAF, TELAH TERJADI KESALAHAN SERVER');
                  return false;
                },
                complete: function(data) {
                  $('.btn-refresh-detail', card).click();
                }
              });
            }
          }
        });
      });
    }
  }).trigger('click');

  $('.btn-add-detail').click(function(){
    var url = $(this).data('url');
    var card = $(this).closest('.card');
    var overlay = $('<div class="overlay"><i class="far fa-2x fa-refresh fa-spin"></i></div>');
    if(url) {
      $('.modal-content', modalDetail).load(url, function(response, status, xhr){
        if(status == "error") {
          toastr.error('MAAF, TELAH TERJADI KESALAHAN SERVER');
          return false;
        }

        modalDetail.modal('show');
        $('form', modalDetail).validate({
          submitHandler: function(form) {
            var btnSubmit = $('button[type=submit]', form);
            btnSubmit.attr('disabled', true);
            $(form).ajaxSubmit({
              dataType: 'json',
              type : 'post',
              success: function(res) {
                if(res.error != 0) {
                  toastr.error(res.error);
                } else {
                  toastr.success(res.success);
                  $('.btn-refresh-detail', card).click();
                  modalDetail.modal('hide');
                }
              },
              error: function() {
                toastr.error('SERVER ERROR');
              },
              complete: function() {
                btnSubmit.attr('disabled', false);
              }
            });

            return false;
          }
        });
      });
    }
  });

  $('[name=IdSkpd]').change(function(){
    var form = $(this).closest('form');
    if (form) {
      $(form).ajaxSubmit({
        dataType: 'json',
        type : 'post',
        success: function(res) {
          if(res.error != 0) {
            toastr.error(res.error);
          } else {
            toastr.success(res.success);
          }
        },
        error: function() {
          toastr.error('SERVER ERROR');
        },
        complete: function() {

        }
      });
    }
  });
});
</script>
