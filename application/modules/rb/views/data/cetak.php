<html>
<head>
  <title><?=$title?></title>
  <style>
  body {
    font-family: 'Source Sans Pro','Helvetica Neue',Helvetica,Arial,sans-serif;
  }
  table {
    width: 100%;
    border-collapse: collapse;
    margin-bottom: 0 !important;
  }
  table, th, td {
    border: 1px solid black;
  }
  th, td {
    padding: 5px;
  }
  </style>
</head>
<body>
  <table width="100%" style="border: 0 !important">
    <tr>
      <td colspan="2" style="text-align: center; vertical-align: middle; border: 0 !important">
        <h4>RENCANA AKSI REFORMASI BIROKRASI</h4>
      </td>
    </tr>
  </table>
  <hr />
  <br />
  <?php
  if ($data[COL_NMTYPE]=='INSTANSI') {
    ?>
    <table width="100%" style="border: 0 !important; font-size: 10pt !important">
      <tr>
        <td style="vertical-align: top; width: 100px; white-space: nowrap; border: 0 !important">TAHUN</td>
        <td style="vertical-align: top; width: 10px; border: 0 !important">:</td>
        <td style="vertical-align: top; font-weight: bold; border: 0 !important"><?=$data[COL_TAHUN]?></td>
      </tr>
      <tr>
        <td style="vertical-align: top; width: 100px; white-space: nowrap; border: 0 !important">JUDUL</td>
        <td style="vertical-align: top; width: 10px; border: 0 !important">:</td>
        <td style="vertical-align: top; font-weight: bold; border: 0 !important"><?=$data[COL_NMKETERANGAN]?></td>
      </tr>
    </table>
    <?php
  } else {
    ?>
    <table width="100%" style="border: 0 !important; font-size: 10pt !important">
      <tr>
        <td style="vertical-align: top; width: 200px; white-space: nowrap; border: 0 !important">TAHUN</td>
        <td style="vertical-align: top; width: 10px; border: 0 !important">:</td>
        <td style="vertical-align: top; font-weight: bold; border: 0 !important"><?=$data[COL_TAHUN]?></td>
      </tr>
      <tr>
        <td style="vertical-align: top; width: 200px; white-space: nowrap; border: 0 !important">UNIT KERJA</td>
        <td style="vertical-align: top; width: 10px; border: 0 !important">:</td>
        <td style="vertical-align: top; font-weight: bold; border: 0 !important"><?=!empty($data[COL_SKPDNAMA])?$data[COL_SKPDNAMA]:$this->setting_org_name?></td>
      </tr>
    </table>
    <?php
  }
  ?>
  <br />
  <?php
  $no=1;
  foreach($det as $d) {
    ?>
    <table width="100%" border="1" style="font-size: 10pt !important; margin-bottom: 20px">
      <tr>
        <td style="font-weight: bold; background: #dedede" colspan="7"><?=$no.'. '.$d[COL_NMPERUBAHAN]?></td>
      </tr>
      <tr>
        <td style="vertical-align: top; width: 10px; white-space: nowrap; font-size: 8pt; font-weight: bold; font-style: italic">NO.</td>
        <td style="vertical-align: top; font-size: 8pt; font-weight: bold; font-style: italic">KEGIATAN UTAMA / RINCIAN KECIATAN</td>
        <td style="vertical-align: top; font-size: 8pt; font-weight: bold; font-style: italic">INDIKATOR / OUTPUT</td>
        <?php
        if($data[COL_NMKATEGORI]=='TEMATIK') {
          ?>
          <td style="vertical-align: top; white-space: nowrap; font-size: 8pt; font-weight: bold; font-style: italic">ANGGARAN</td>
          <?php
        }
        ?>
        <!--<td style="vertical-align: top; width: 50px; font-size: 8pt; font-weight: bold; font-style: italic">OUTPUT</td>-->
        <td style="vertical-align: top; width: 10px; white-space: nowrap; font-size: 8pt; font-weight: bold; font-style: italic">TARGET</td>
        <td style="vertical-align: top; width: 10px; white-space: nowrap; font-size: 8pt; font-weight: bold; font-style: italic">SATUAN</td>
        <td style="vertical-align: top; font-size: 8pt; font-weight: bold; font-style: italic">PELAKSANAAN (BULAN)</td>
        <?php
        if($data[COL_NMKATEGORI]=='TEMATIK'&&$data[COL_NMTYPE]=='INSTANSI') {

        }
        ?>
      </tr>
      <?php
      $rgrp = $this->db
      ->where(COL_IDPERUBAHAN, $d[COL_UNIQ])
      ->group_by(COL_NMKEGIATAN)
      ->get(TBL_RB_RENJADET)
      ->result_array();

      foreach($rgrp as $grp) {
        $rtahap = $this->db
        ->where(COL_IDPERUBAHAN, $d[COL_UNIQ])
        ->where(COL_NMKEGIATAN, $grp[COL_NMKEGIATAN])
        ->get(TBL_RB_RENJADET)
        ->result_array();
        ?>

        <?php
        if(!empty($grp[COL_NMKEGIATAN])) {
          $nmKeg = '';
          if($data[COL_NMKATEGORI]=='TEMATIK'&&$data[COL_NMTYPE]=='UNIT') {
            $rrenjinduk = $this->db
            ->where(COL_UNIQ, $grp[COL_NMKEGIATAN])
            ->get(TBL_RB_RENJADET)
            ->row_array();
            if(!empty($rrenjinduk)) {
              $nmKeg = $rrenjinduk[COL_NMTAHAPAN];
            }

          } else {
            $nmKeg = $grp[COL_NMKEGIATAN];
          }

          ?>
          <tr>
            <td style="vertical-align: top; width: 10px; font-size: 8pt; font-weight: bold;" colspan="7"><?=$nmKeg?></td>
          </tr>
          <?php
        }
        ?>

        <?php
        $not=1;
        foreach ($rtahap as $t) {
          $period = explode(",", $t[COL_PERIODTARGET]);
          $period = implode(", ", $period);

          $numAnggaran = (!empty($t[COL_BUDGETTW1])?$t[COL_BUDGETTW1]:0)+(!empty($t[COL_BUDGETTW2])?$t[COL_BUDGETTW2]:0)+(!empty($t[COL_BUDGETTW3])?$t[COL_BUDGETTW3]:0)+(!empty($t[COL_BUDGETTW4])?$t[COL_BUDGETTW4]:0);
          if($data[COL_NMKATEGORI]=='TEMATIK'&&$data[COL_NMTYPE]=='INSTANSI') {
            $uniq_ = $t[COL_UNIQ];
            $tahun_ = $data[COL_TAHUN];
            $qbudget = @"
            select sum(IFNULL(BudgetTW1,0)+IFNULL(BudgetTW2,0)+IFNULL(BudgetTW3,0)+IFNULL(BudgetTW4,0)) as NumAnggaran from rb_renjadet det
            left join rb_renja ren on ren.Uniq = det.IdRenja
            where det.NmKegiatan=$uniq_ and ren.Tahun=$tahun_ and ren.NmType='UNIT'
            ";
            $rbudget = $this->db->query($qbudget)->row_array();
            if(!empty($rbudget)) {
              $numAnggaran=$rbudget['NumAnggaran'];
            }
          }
          ?>
          <tr>
            <td style="vertical-align: top; width: 10px; white-space: nowrap; font-size: 8pt; text-align: right; font-style: italic" <?=$data[COL_NMKATEGORI]=='TEMATIK'&&$data[COL_NMTYPE]=='INSTANSI'?'rowspan="3"':''?>><?=$not.'.'?></td>
            <td style="vertical-align: top; font-size: 8pt; font-style: italic;"><?=$t[COL_NMTAHAPAN]?></td>
            <td style="vertical-align: top; font-size: 8pt; font-style: italic;"><?=$t[COL_NMINDIKATOR]?></td>
            <?php
            if($data[COL_NMKATEGORI]=='TEMATIK') {
              ?>
              <td style="vertical-align: top; white-space: nowrap; font-size: 8pt; text-align: right; font-style: italic">Rp. <?=number_format($numAnggaran)?></td>
              <?php
            }
            ?>
            <!--<td style="vertical-align: top; font-size: 8pt; font-style: italic;"><?=$t[COL_NMOUTPUT]?></td>-->
            <td style="white-space: nowrap; vertical-align: top; font-size: 8pt; text-align: right; font-style: italic"><?=$t[COL_NMTARGET]?></td>
            <td style="white-space: nowrap; vertical-align: top; font-size: 8pt; font-style: italic;"><?=strtoupper($t[COL_NMSATUAN])?></td>
            <td style="vertical-align: top; font-size: 8pt; font-style: italic;"><?=$period?></td>
          </tr>
          <?php
          $not++;
          if($data[COL_NMKATEGORI]=='TEMATIK'&&$data[COL_NMTYPE]=='INSTANSI') {
            $uniq_ = $t[COL_UNIQ];
            $tahun_ = $data[COL_TAHUN];
            $qunit = @"
            select GROUP_CONCAT(SkpdNama SEPARATOR ', ') as Units from (
              select skpd.SkpdNama from rb_renjadet det
              left join rb_renja ren on ren.Uniq = det.IdRenja
              left join sakipv2_skpd skpd on skpd.SkpdId = ren.SkpdId
              where det.NmKegiatan=$uniq_ and ren.Tahun=$tahun_ and ren.NmType='UNIT'
              group by skpd.SkpdNama
            ) tbl
            ";
            $units = $this->db->query($qunit)->row_array();

            $qsubkeg = @"
            select sub.* from rb_renjadetsub sub
            left join rb_renjadet det on det.Uniq = sub.IdRenjaDet
            left join rb_renja ren on ren.Uniq = det.IdRenja
            where det.NmKegiatan=$uniq_ and ren.Tahun=$tahun_ and ren.NmType='UNIT'
            group by sub.SubkegKode
            ";
            $rsubkeg = $this->db->query($qsubkeg)->result_array();
            ?>
            <tr>
              <td colspan="6" style="vertical-align: top; font-size: 8pt; font-weight: bold">
                OPD: <?=!empty($units['Units'])?$units['Units']:'-'?>
              </td>
            </tr>
            <tr>
              <td colspan="6" style="vertical-align: top; font-size: 8pt;">
                <strong>Sub Kegiatan: <?=number_format(count($rsubkeg))?></strong>
                <?php
                if(!empty($rsubkeg)) {
                  ?>
                  <ul>
                    <?php
                    foreach($rsubkeg as $keg) {
                      ?>
                      <li style="font-size: 8pt; font-weight: normal !important"><strong><?=$keg[COL_SUBKEGKODE]?></strong> - <?=$keg[COL_SUBKEGURAIAN]?></li>
                      <?php
                    }
                    ?>
                  </ul>
                  <?php
                }
                ?>
              </td>
            </tr>
            <?php
          }
        }
      }
      $no++;
      ?>
    </table>
    <?php
  }
  ?>
</body>
</html>
