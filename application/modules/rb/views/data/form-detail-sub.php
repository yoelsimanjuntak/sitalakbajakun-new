<?php
$_idSKPD = $rrenja[COL_SKPDID];
$_tahun = $rrenja[COL_TAHUN];
$qdpa = @"
select subkeg.* from sakipv2_skpd_renstra_dpa dpa
left join sakipv2_skpd_renstra renstra on renstra.RenstraId = dpa.IdRenstra
left join sakipv2_bid_program prog on prog.IdDPA = dpa.DPAId
left join sakipv2_bid_kegiatan keg on keg.IdProgram = prog.ProgramId
left join sakipv2_subbid_subkegiatan subkeg on subkeg.IdKegiatan = keg.KegiatanId
left join sakipv2_bid bid on bid.BidId = prog.IdBid
left join sakipv2_subbid subbid on subbid.SubbidId = subkeg.IdSubbid
where
	renstra.IdSkpd=$_idSKPD
	and renstra.RenstraIsAktif=1
	and dpa.DPAIsAktif=1
	and dpa.DPATahun=$_tahun
  and bid.BidIsAktif=1
	and (subbid.SubbidId is null or subbid.SubbidIsAktif=1)
";
$rdpa = $this->db->query($qdpa)->result_array();

$rrenjasub = $this->db
->where(COL_IDRENJADET, $rdata[COL_UNIQ])
->order_by(COL_SUBKEGKODE)
->get(TBL_RB_RENJADETSUB)
->result_array();
?>
<div class="modal-header">
  <h5 class="modal-title">RINCIAN SUB KEGIATAN</h5>
  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
      <span aria-hidden="true"><i class="fa fa-close"></i></span>
  </button>
</div>
<div id="modal-detail" class="modal-body">
  <div class="row">
    <div class="col-sm-12">
      <p class="text-muted font-italic">Anda dapat menambahkan rincian sub kegiatan yang mendukung pelaksanaan kegiatan utama berdasarkan dokumen anggaran.</p>
    </div>
    <div class="col-sm-12">
      <form id="form-detail" method="post" action="<?=current_url().'?mode=add'?>">
        <div class="form-group">
          <div class="input-group">
            <select name="<?=COL_SUBKEGID?>" class="form-control">
              <?php
              foreach($rdpa as $r) {
                ?>
                <option value="<?=$r[COL_SUBKEGID]?>"><?=$r[COL_SUBKEGKODE]?> - <?=$r[COL_SUBKEGURAIAN]?></option>
                <?php
              }
              ?>
            </select>
            <span class="input-group-append">
              <button type="button" id="btn-detail-add" class="btn btn-primary"><i class="far fa-plus-circle"></i></button>
            </span>
          </div>
        </div>
      </form>
      <table id="tbl-subkeg" class="table table-bordered table-condensed table-sm">
        <thead>
          <tr>
            <th style="width: 100px; white-space: nowrap">Kode</th>
            <th>Uraian</th>
            <th style="width: 50px; white-space: nowrap">#</th>
          </tr>
        </thead>
        <tbody></tbody>
      </table>
    </div>
  </div>
</div>
<script type="text/javascript">
var thisForm = $('#modal-detail');
var thisTable = $('#tbl-subkeg');

function detailLoad() {
  $("tbody", thisTable).html('<tr><td colspan="3">Memuat ...</td></tr>');
  $("tbody", thisTable).load('<?=current_url().'?mode=load'?>', function(){
    $('.btn-del-sub', thisTable).click(function(){
      var url = $(this).attr('href');
      if(confirm('Apakah anda yakin?')) {
        $.get(url, function(res) {
          if(res.error != 0) {
            toastr.error(res.error);
          } else {
            toastr.success(res.success);
          }
        }, "json").done(function() {
          detailLoad();
        }).fail(function() {
          toastr.error('SERVER ERROR');
        });
      }
      return false;
    });
  });
}
$(document).ready(function() {
  $("select", thisForm).not('.no-select2, .custom-select').select2({ width: 'resolve', theme: 'bootstrap4' });
  $(".uang", thisForm).number(true, 0, '.', ',');
  detailLoad();

  $('#btn-detail-add', thisForm).click(function(){
    var thisBtn = $(this);

    thisBtn.attr('disabled', true);
    $('#form-detail', thisForm).ajaxSubmit({
      dataType: 'json',
      success : function(data){
        if(data.error==0) {
          detailLoad();
        } else {
          toastr.error(data.error);
        }
      },
      error : function(xhr){
        toastr.error('Terjadi kesalahan di sisi server. Silakan coba beberapa saat lagi atau hubungi Administrator.');
      },
      complete: function(data) {
        thisBtn.attr('disabled', false);
      }
    });
  });
});

</script>
