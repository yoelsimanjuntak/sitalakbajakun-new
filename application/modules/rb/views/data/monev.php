<?php
$rperubahan = $this->db
->where(COL_IDRENJA, $data[COL_UNIQ])
->order_by(COL_UNIQ)
->get(TBL_RB_RENJAPERUBAHAN)
->result_array();
$period = $this->input->get('Period');
?>
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h3 class="m-0 text-dark font-weight-light">
          <?=$title?>
        </h3>
      </div>
      <div class="col-sm-6 text-right">
        <a href="<?=site_url('rb/data/index/'.strtolower($data[COL_NMTYPE]).'/'.strtolower($data[COL_NMKATEGORI]))?>" class="btn btn-sm btn-secondary"><i class="far fa-arrow-circle-left"></i> KEMBALI</a>
      </div>
    </div>
  </div>
</div>
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-sm-12">
        <div class="card">
          <div class="card-header">
            <h5 class="card-title">PERIODE MONEV</h5>
          </div>
          <div class="card-body">
            <!--<div class="row">
              <div class="col-sm-1">
                <a href="<?=current_url().'?Period=1'?>" class="btn btn-sm btn-block <?=$period==1?'btn-primary disabled':'btn-outline-primary'?>">JAN</a>
              </div>
              <div class="col-sm-1">
                <a href="<?=current_url().'?Period=2'?>" class="btn btn-sm btn-block <?=$period==2?'btn-primary disabled':'btn-outline-primary'?>">FEB</a>
              </div>
              <div class="col-sm-1">
                <a href="<?=current_url().'?Period=3'?>" class="btn btn-sm btn-block <?=$period==3?'btn-primary disabled':'btn-outline-primary'?>">MAR</a>
              </div>
              <div class="col-sm-1">
                <a href="<?=current_url().'?Period=4'?>" class="btn btn-sm btn-block <?=$period==4?'btn-primary disabled':'btn-outline-primary'?>">APR</a>
              </div>
              <div class="col-sm-1">
                <a href="<?=current_url().'?Period=5'?>" class="btn btn-sm btn-block <?=$period==5?'btn-primary disabled':'btn-outline-primary'?>">MEI</a>
              </div>
              <div class="col-sm-1">
                <a href="<?=current_url().'?Period=6'?>" class="btn btn-sm btn-block <?=$period==6?'btn-primary disabled':'btn-outline-primary'?>">JUN</a>
              </div>
              <div class="col-sm-1">
                <a href="<?=current_url().'?Period=7'?>" class="btn btn-sm btn-block <?=$period==7?'btn-primary disabled':'btn-outline-primary'?>">JUL</a>
              </div>
              <div class="col-sm-1">
                <a href="<?=current_url().'?Period=8'?>" class="btn btn-sm btn-block <?=$period==8?'btn-primary disabled':'btn-outline-primary'?>">AGT</a>
              </div>
              <div class="col-sm-1">
                <a href="<?=current_url().'?Period=9'?>" class="btn btn-sm btn-block <?=$period==9?'btn-primary disabled':'btn-outline-primary'?>">SEP</a>
              </div>
              <div class="col-sm-1">
                <a href="<?=current_url().'?Period=10'?>" class="btn btn-sm btn-block <?=$period==10?'btn-primary disabled':'btn-outline-primary'?>">OKT</a>
              </div>
              <div class="col-sm-1">
                <a href="<?=current_url().'?Period=11'?>" class="btn btn-sm btn-block <?=$period==11?'btn-primary disabled':'btn-outline-primary'?>">NOV</a>
              </div>
              <div class="col-sm-1">
                <a href="<?=current_url().'?Period=12'?>" class="btn btn-sm btn-block <?=$period==12?'btn-primary disabled':'btn-outline-primary'?>">DES</a>
              </div>
            </div>-->
            <div class="row">
              <div class="col-sm-3">
                <a href="<?=current_url().'?Period=1'?>" class="btn btn-sm btn-block <?=$period==1?'btn-primary disabled':'btn-outline-primary'?>">TW I</a>
              </div>
              <div class="col-sm-3">
                <a href="<?=current_url().'?Period=4'?>" class="btn btn-sm btn-block <?=$period==4?'btn-primary disabled':'btn-outline-primary'?>">TW II</a>
              </div>
              <div class="col-sm-3">
                <a href="<?=current_url().'?Period=7'?>" class="btn btn-sm btn-block <?=$period==7?'btn-primary disabled':'btn-outline-primary'?>">TW III</a>
              </div>
              <div class="col-sm-3">
                <a href="<?=current_url().'?Period=10'?>" class="btn btn-sm btn-block <?=$period==10?'btn-primary disabled':'btn-outline-primary'?>">TW IV</a>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-sm-12">
        <?php
        $no=1;
        foreach($rperubahan as $d) {
          $arrKegiatan = array();
          if(!empty($d[COL_NMKEGIATAN])) {
            $arrKegiatan = explode(";", $d[COL_NMKEGIATAN]);
          }

          $rtahapan = $this->db
          ->where(COL_IDRENJA, $data[COL_UNIQ])
          ->where(COL_IDPERUBAHAN, $d[COL_UNIQ])
          ->get(TBL_RB_RENJADET)
          ->result_array();
          ?>
          <div class="card <?=count($rperubahan)>1?'collapsed-card':''?> card-monev" data-id="<?=$d[COL_UNIQ]?>" data-url="<?=site_url('rb/data/monev-partial/'.$d[COL_UNIQ].'/'.$period)?>">
            <div class="card-header">
              <h5 class="card-title"><?=count($rperubahan)>1?$no.'. ':''?><strong><?=$d[COL_NMPERUBAHAN]?></strong></h5>
              <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-<?=count($rperubahan)>1?'plus':'minus'?>"></i>
                </button>
              </div>
            </div>
            <div class="card-body p-0">

            </div>
            <!--<div class="card-footer">
              <p class="text-sm font-italic m-0">
                <strong>T</strong> : Target<br />
                <strong>C</strong> : Capaian
              </p>
            </div>-->
          </div>
          <?php
          $no++;
        }
        ?>
      </div>
    </div>
  </div>
</section>
<div class="modal fade" id="modal-monev" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">

    </div>
  </div>
</div>
<div class="modal fade" id="modal-doc" role="dialog">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">

    </div>
  </div>
</div>
<script type="text/javascript">
var modalMonev = $('#modal-monev');
var modalDoc = $('#modal-doc');
function monevRefresh(el) {
  var url = el.data('url');
  if (url) {
    $('.card-body', el).load(url, function(){
      $('.btn-entry', el).click(function(){
        var href = $(this).attr('href');
        $('.modal-content', modalMonev).load(href, function(){
          $(".uang",modalMonev).number(true, 0, '.', ',');
          modalMonev.modal('show');
          $('form', modalMonev).validate({
            submitHandler: function(form) {
              var btnSubmit = $('button[type=submit]', $(form));
              var txtSubmit = btnSubmit[0].innerHTML;
              btnSubmit.html('<i class="fad fa-circle-notch fa-spin"></i>');
              $(form).ajaxSubmit({
                url: href,
                dataType: 'json',
                type : 'post',
                success: function(res) {
                  if(res.error != 0) {
                    toastr.error(res.error);
                  } else {
                    toastr.success(res.success);
                    monevRefresh(el);
                    modalMonev.modal('hide');
                  }
                },
                error: function() {
                  toastr.error('SERVER ERROR');
                },
                complete: function() {
                  btnSubmit.html(txtSubmit);
                }
              });
              return false;
            }
          });
        });
        return false;
      });

      $('.btn-doc', el).click(function(){
        var href = $(this).attr('href');
        monevLoadDocs(href);
        return false;
      });
    });
  }
}

function monevLoadDocs(href) {
  $('.modal-content', modalDoc).load(href, function() {
    $("select", modalDoc).not('.no-select2, .custom-select').select2({ width: 'resolve', theme: 'bootstrap4' });
    modalDoc.modal('show');

    $('.btn-doc-delete', modalDoc).click(function(){
      var url = $(this).attr('href');
      if(confirm('Apakah anda yakin?')) {
        $.get(url, function(res) {
          if(res.error != 0) {
            toastr.error(res.error);
          } else {
            toastr.success(res.success);
            monevLoadDocs(href);
          }
        }, "json").done(function() {

        }).fail(function() {
          toastr.error('SERVER ERROR');
        });
      }

      return false;
    });
    $('form', modalDoc).validate({
      ignore: "input[type='file']",
      submitHandler: function(form) {
        var btnSubmit = $('button[type=submit]', $(form));
        var txtSubmit = btnSubmit[0].innerHTML;
        btnSubmit.html('<i class="fad fa-circle-notch fa-spin"></i>');
        $(form).ajaxSubmit({
          url: href,
          dataType: 'json',
          type : 'post',
          success: function(res) {
            if(res.error != 0) {
              toastr.error(res.error);
            } else {
              toastr.success(res.success);
              monevLoadDocs(href);
            }
          },
          error: function() {
            toastr.error('SERVER ERROR');
          },
          complete: function() {
            btnSubmit.html(txtSubmit);
          }
        });
        return false;
      }
    });
  });
}
$(document).ready(function(){
  var cards = $('.card-monev');

  cards.each(function(indx) {
    monevRefresh($(this));
  });

  modalMonev.on('hidden.bs.modal', function (e) {
    $('.modal-content', modalMonev).empty();
  });

  $('.btn-submit').click(function(){
    var card = $(this).closest('.card');
    if(card) {
      $('form',card).submit();
    }
  });

  $('[name=FilterPeriod]').change(function(){
    location.href = $(this).val();
  });

  $('form').validate({
    submitHandler: function(form) {
      var btnSubmit = $('button.btn-submit', form.closest('card'));
      btnSubmit.attr('disabled', true);

      $(form).ajaxSubmit({
        dataType: 'json',
        type : 'post',
        success: function(res) {
          if(res.error != 0) {
            toastr.error(res.error);
          } else {
            toastr.success(res.success);
            /*if(res.data.redirect) {
              setTimeout(function(){
                location.href = res.data.redirect;
              }, 1000);
            }*/
            location.reload();
          }
        },
        error: function() {
          toastr.error('SERVER ERROR');
        },
        complete: function() {
          btnSubmit.attr('disabled', false);
        }
      });

      return false;
    }
  });
});
</script>
