<?php
class Doc extends MY_Controller {
  function __construct() {
      parent::__construct();
      if(!IsLogin()) {
        redirect(site_url());
      }
  }

  public function showErrUnathorized() {
    $act = $this->router->fetch_method();
    if($this->input->is_ajax_request() && strpos($act, "_form")===false) {
      ShowJsonError('MAAF, ANDA TIDAK MEMILIKI HAK AKSES.');
      exit();
    }else{
      echo 'MAAF, ANDA TIDAK MEMILIKI HAK AKSES.';
      exit();
    }
  }

  public function index($type, $noUnit=0) {
    $data['title'] = strtoupper(str_replace("-"," ",$type));
    $data['type'] = $type;
    $data['nounit'] = $noUnit;
    $this->template->load('main', 'rb/doc/index', $data);
  }

  public function ajax_form($mode, $id, $idSKPD=null) {
    $ruser = GetLoggedUser();
    if($ruser[COL_ROLEID]!=ROLEADMIN && $ruser[COL_ROLEID]!=ROLEKADIS) {
      $this->showErrUnathorized();
    }

    if(!empty($_POST)) {
      $config['upload_path'] = MY_UPLOADPATH;
      $config['allowed_types'] = "jpg|jpeg|png|pdf|doc|docx|xls|xlsx";
      $config['max_size']	= 5120;
      $config['max_width']  = 4000;
      $config['max_height']  = 4000;
      $config['overwrite'] = FALSE;

      $this->load->library('upload',$config);

      if($mode=='add') {
        if(empty($id)) {
          ShowJsonError('PARAMETER TIDAK VALID!');
          exit();
        }

        $this->db->trans_begin();
        try {
          $rec = array(
            COL_DOCTYPE=>$id,
            COL_DOCNAME=>$this->input->post(COL_DOCNAME),
            COL_DOCREMARKS=>$this->input->post(COL_DOCREMARKS),
            COL_DOCTAHUN=>!empty($this->input->post(COL_DOCTAHUN))?$this->input->post(COL_DOCTAHUN):null,

            COL_CREATEDBY=>$ruser[COL_USERNAME],
            COL_CREATEDON=>date('Y-m-d H:i:s')
          );

          if(!empty($idSKPD)) {
            $rec[COL_IDSKPD]=$idSKPD;
          }

          if(!empty($_FILES)) {
            $res = $this->upload->do_upload('file');
            if(!$res) {
              $err = $this->upload->display_errors('', '');
              throw new Exception($err);
            }
            $upl = $this->upload->data();
            $rec[COL_DOCURL] = $upl['file_name'];
          }

          $res = $this->db->insert(TBL_SAKIPV2_SKPD_DOC, $rec);
          if(!$res) {
            $err = $this->db->error();
            throw new Exception($err['message']);
          }

          $this->db->trans_commit();
          ShowJsonSuccess('INPUT DATA BERHASIL');
          exit();
        } catch(Exception $ex) {
          $this->db->trans_rollback();
          ShowJsonError($ex->getMessage());
          exit();
        }
      } else if($mode == 'edit') {
        $rec = array(
          COL_DOCNAME=>$this->input->post(COL_DOCNAME),
          COL_DOCREMARKS=>$this->input->post(COL_DOCREMARKS),
          COL_DOCTAHUN=>!empty($this->input->post(COL_DOCTAHUN))?$this->input->post(COL_DOCTAHUN):null,

          COL_UPDATEDBY=>$ruser[COL_USERNAME],
          COL_UPDATEDON=>date('Y-m-d H:i:s')
        );

        if(!empty($idSKPD)) {
          $rec[COL_IDSKPD]=$idSKPD;
        }

        if(!empty($_FILES)) {
          $res = $this->upload->do_upload('file');
          /*if(!$res) {
            $err = $this->upload->display_errors('', '');
            throw new Exception($err);
          }*/
          if($res) {
            $upl = $this->upload->data();
            $rec[COL_DOCURL] = $upl['file_name'];
          }

        }

        $res = $this->db->where(COL_DOCID, $id)->update(TBL_SAKIPV2_SKPD_DOC, $rec);
        if(!$res) {
          $err = $this->db->error();
          throw new Exception($err['message']);
        }

        $this->db->trans_commit();
        ShowJsonSuccess('INPUT DATA BERHASIL');
        exit();
      }
      /*

      */
    } else {
      $data=array();
      if($mode=='edit') {
        $data['data'] = $this->db->where(COL_DOCID, $id)->get(TBL_SAKIPV2_SKPD_DOC)->row_array();
      }
      $this->load->view('rb/doc/form', $data);
    }
  }

  public function ajax_change($mode, $id)  {
    $ruser = GetLoggedUser();
    if($ruser[COL_ROLEID]!=ROLEADMIN && $ruser[COL_ROLEID]!=ROLEKADIS) {
      $this->showErrUnathorized();
    }

    if($mode=='delete') {
      $rdata = $this->db->where(COL_DOCID, $id)->get(TBL_SAKIPV2_SKPD_DOC)->row_array();
      $res = $this->db->where(COL_DOCID, $id)->delete(TBL_SAKIPV2_SKPD_DOC);
      if(!$res) {
        $err = $this->db->error();
        ShowJsonError($err['message']);
        exit();
      }

      if(!empty($rdata[COL_DOCURL]) && file_exists(MY_UPLOADPATH.$rdata[COL_DOCURL])) {
        unlink(MY_UPLOADPATH.$rdata[COL_DOCURL]);
      }
    } else {
      ShowJsonError('PARAMETER TIDAK VALID');
      exit();
    }

    ShowJsonSuccess('BERHASIL MENGUBAH DATA');
    exit();
  }
}
?>
