<?php
class Laporan extends MY_Controller {
  function __construct() {
      parent::__construct();
  }

  public function index($tipe) {
    $ruser = GetLoggedUser();
    $data['title'] = 'Laporan Monitoring dan Evaluasi RB Tahunan';
    $data['tipe'] = $tipe;
    $this->template->load('main', 'rb/laporan/index', $data);
  }

  public function rekapitulasi($tipe) {
    $ruser = GetLoggedUser();
    $data['title'] = 'Rekapitulasi Capaian Rencana Aksi RB';
    $data['tipe'] = $tipe;
    if(isset($_GET['id']) && isset($_GET['download']) && $_GET['download']==1) {
      $rrenja = $this->db
      ->join(TBL_SAKIPV2_SKPD.' opd','opd.'.COL_SKPDID." = ".TBL_RB_RENJA.".".COL_SKPDID,"left")
      ->where(COL_UNIQ, $_GET['id'])
      ->get(TBL_RB_RENJA)
      ->row_array();
      $this->load->view('rb/laporan/rekapitulasi-partial', array('rrenja'=>$rrenja,'download'=>1));
    } else {
      $this->template->load('main', 'rb/laporan/rekapitulasi', $data);
    }

  }
}
