<?php
$ruser = GetLoggedUser();

$rOptPmd = $this->db
->where(COL_PMDISAKTIF, 1)
->order_by(COL_PMDISAKTIF, 'desc')
->order_by(COL_PMDTAHUNMULAI, 'desc')
->get(TBL_SAKIPV2_PEMDA)
->result_array();

$rOptSkpd = $this->db
->where(COL_SKPDISAKTIF, 1)
->order_by(COL_SKPDURUSAN, 'asc')
->order_by(COL_SKPDBIDANG, 'asc')
->order_by(COL_SKPDUNIT, 'asc')
->order_by(COL_SKPDSUBUNIT, 'asc')
->get(TBL_SAKIPV2_SKPD)
->result_array();

$getPmd = '';
$getSkpd = '';
$getRenstra = '';
if(!empty($_GET['idPmd'])) $getPmd = $_GET['idPmd'];
else if(!empty($rOptPmd)) $getPmd = $rOptPmd[0][COL_PMDID];

if(!empty($_GET['idSKPD'])) $getSkpd = $_GET['idSKPD'];
else if(!empty($rOptSkpd)) $getSkpd = $rOptSkpd[0][COL_SKPDID];

if($ruser[COL_ROLEID]!=ROLEADMIN && $ruser[COL_ROLEID]!=ROLEGUEST) {
  $getSkpd=$ruser[COL_SKPDID];
}

$rOptRenstra = array();
if(!empty($getSkpd) && !empty($getPmd)) {
  $rOptRenstra = $this->db
  ->where(COL_IDPEMDA, $getPmd)
  ->where(COL_IDSKPD, $getSkpd)
  ->where(COL_RENSTRAISAKTIF, 1)
  ->order_by(COL_RENSTRAISAKTIF, 'desc')
  ->order_by(COL_RENSTRATAHUN, 'desc')
  ->order_by(COL_RENSTRAID, 'desc')
  ->get(TBL_SAKIPV2_SKPD_RENSTRA)
  ->result_array();
}

if(!empty($_GET['idRenstra'])) $getRenstra = $_GET['idRenstra'];
else if(!empty($rOptRenstra)) $getRenstra = $rOptRenstra[0][COL_RENSTRAID];

$rjabatan = array();
if(!empty($getRenstra)) {
  $rrenstra = $this->db
  ->where(COL_RENSTRAID, $getRenstra)
  ->get(TBL_SAKIPV2_SKPD_RENSTRA)
  ->row_array();

  if(!empty($rrenstra)) {
    $rjabatan = $this->db
    ->select('*, COALESCE(sakipv2_subbid.SubbidNama, sakipv2_bid.BidNama, skpd_.SkpdNama) as UnitNama')
    ->join(TBL_SAKIPV2_SUBBID,TBL_SAKIPV2_SUBBID.'.'.COL_SUBBIDID." = ".TBL_SAKIPV2_SUBBID_PELAKSANA.".".COL_IDSUBBID,"left")
    ->join(TBL_SAKIPV2_BID.' bid_','bid_.'.COL_BIDID." = (sakipv2_subbid_pelaksana.IdSubbid*-1-1000)","left")
    ->join(TBL_SAKIPV2_BID,TBL_SAKIPV2_BID.'.'.COL_BIDID." = COALESCE(sakipv2_subbid.IdBid, bid_.BidId)","left")
    ->join(TBL_SAKIPV2_SKPD.' skpd_','skpd_.'.COL_SKPDID." = (sakipv2_subbid_pelaksana.IdSubbid*-1-100)","left")

    ->where(TBL_SAKIPV2_BID.'.'.COL_IDRENSTRA, $getRenstra)
    ->or_where(TBL_SAKIPV2_SUBBID_PELAKSANA.'.'.COL_IDSUBBID, ($rrenstra[COL_IDSKPD]+100)*-1)
    ->get(TBL_SAKIPV2_SUBBID_PELAKSANA)
    ->result_array();

    //echo $rjabatan;
    //exit();
  }
}
?>
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h3 class="m-0 font-weight-light"><?=$title?></h3>
      </div>
      <?php
      if(!empty($navs)) {
        ?>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <?php
            foreach($navs as $n) {
              if(!empty($n['link'])) {
                ?>
                <li class="breadcrumb-item"><a href="<?=$n['link']?>"><?=$n['text']?></a></li>
                <?php
              } else {
                ?>
                <li class="breadcrumb-item active"><?=$n['text']?></li>
                <?php
              }
            }
            ?>
          </ol>
        </div>
        <?php
      }
      ?>
    </div>
  </div>
</div>
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-12">
        <div class="card card-outline card-secondary">
          <?php
          if(!empty($subtitle)) {
            ?>
            <div class="card-header">
              <h4 class="card-title"><?=$subtitle?></h4>
            </div>
            <?php
          }
          ?>
          <div class="card-body p-0">
            <table id="table-jabatan" class="table table-bordered">
              <thead>
                <tr>
                  <td colspan="6">
                    <div class="form-group row">
                      <label class="control-label col-lg-2">PERIODE PEMERINTAHAN</label>
                      <div class="col-lg-10">
                        <select class="form-control" name="filterPmd">
                          <?php
                          foreach($rOptPmd as $opt) {
                            $isSelected = '';
                            if(!empty($getPmd) && $opt[COL_PMDID]==$getPmd) {
                              $isSelected='selected';
                            } else if(empty($getPmd) && $opt[COL_PMDISAKTIF]) {
                              $isSelected='selected';
                            }
                            ?>
                            <option value="<?=site_url('sakipv2/individu/index').'?idPmd='.$opt[COL_PMDID].'&idSKPD='.$getSkpd?>" <?=$isSelected?>>
                              <?=$opt[COL_PMDTAHUNMULAI].' s.d '.$opt[COL_PMDTAHUNAKHIR].' - '.strtoupper($opt[COL_PMDPEJABAT])?>
                            </option>
                            <?php
                          }
                          ?>
                        </select>
                      </div>
                    </div>

                    <div class="form-group row">
                      <label class="control-label col-lg-2">SKPD</label>
                      <div class="col-lg-10">
                        <?php
                        if($ruser[COL_ROLEID] == ROLEADMIN || $ruser[COL_ROLEID] == ROLEGUEST) {
                          ?>
                          <select class="form-control" name="filterSkpd">
                            <?php
                            foreach($rOptSkpd as $opt) {
                              $isSelected = '';
                              if(!empty($getSkpd) && $opt[COL_SKPDID]==$getSkpd) {
                                $isSelected='selected';
                              }
                              ?>
                              <option value="<?=site_url('sakipv2/individu/index').'?idPmd='.$getPmd.'&idSKPD='.$opt[COL_SKPDID]?>" <?=$isSelected?>>
                                <?=$opt[COL_SKPDURUSAN].'.'.$opt[COL_SKPDBIDANG].'.'.$opt[COL_SKPDUNIT].'.'.$opt[COL_SKPDSUBUNIT].' - '.strtoupper($opt[COL_SKPDNAMA])?>
                              </option>
                              <?php
                            }
                            ?>
                          </select>
                          <?php
                        } else {
                          $ropd = $this->db
                          ->where(COL_SKPDID, $ruser[COL_SKPDID])
                          ->get(TBL_SAKIPV2_SKPD)
                          ->row_array();
                          ?>
                          <input type="text" name="filterSkpd" class="form-control font-weight-bold font-italic" value="<?=!empty($ropd)?strtoupper($ropd[COL_SKPDNAMA]):'-'?>" readonly />
                          <?php
                        }
                        ?>
                      </div>
                    </div>
                    <div class="form-group row">
                      <label class="control-label col-lg-2">RENSTRA SKPD</label>
                      <div class="col-lg-10">
                        <select class="form-control" name="filterRenstra">
                          <?php
                          foreach($rOptRenstra as $opt) {
                            $isSelected = '';
                            if(!empty($getRenstra) && $opt[COL_RENSTRAID]==$getRenstra) {
                              $isSelected='selected';
                            }
                            ?>
                            <option value="<?=site_url('sakipv2/individu/index').'?idPmd='.$getPmd.'&idSKPD='.$getSkpd.'&idRenstra='.$opt[COL_RENSTRAID]?>" <?=$isSelected?>>
                              <?=$opt[COL_RENSTRATAHUN].' - '.strtoupper($opt[COL_RENSTRAURAIAN])?>
                            </option>
                            <?php
                          }
                          ?>
                        </select>
                      </div>
                    </div>
                  </td>
                </tr>
                <tr>
                  <th>NOMENKLATUR</th>
                  <th>UNIT KERJA</th>
                  <th>NAMA PEGAWAI</th>
                  <th>SASARAN</th>
                  <th>IKU</th>
                  <th style="width: 100px; white-space: nowrap; text-align: center !important">AKSI</th>
                </tr>
              </thead>
              <tbody>
                <?php
                if(!empty($rjabatan)) {
                  foreach($rjabatan as $r) {
                    $arrTupoksi = json_decode($r[COL_PLSFUNGSI]);
                    $arrIKU = json_decode($r[COL_PLSIKU]);
                    ?>
                    <tr>
                      <td><?=strtoupper($r[COL_PLSNAMA])?></td>
                      <td class="text-sm font-italic">
                        <p class="text-sm font-italic mb-0">
                          <?=strtoupper($r['UnitNama'])?>
                        </p>
                      </td>
                      <td><?=!empty($r[COL_PLSNAMAPEGAWAI])?strtoupper($r[COL_PLSNAMAPEGAWAI]):'-' ?></td>
                      <td class="text-right"><?=number_format(count($arrTupoksi))?></td>
                      <td class="text-right"><?=number_format(count($arrIKU))?></td>
                      <td class="text-center" style="white-space: nowrap; vertical-align: middle">
                        <a href="<?=site_url('sakipv2/individu/ajax-form-individu/edit/'.$r[COL_PLSID].'/'.$getRenstra)?>" data-toggle="tooltip" data-placement="bottom" title="UBAH" class="btn btn-primary btn-sm btn-edit-individu"><i class="far fa-edit"></i></a>
                        <a href="<?=site_url('sakipv2/individu/ajax-change-individu/delete/'.$r[COL_PLSID])?>" data-toggle="tooltip" data-placement="bottom" title="HAPUS" class="btn btn-danger btn-sm btn-change-individu"><i class="far fa-times-circle"></i></a>
                      </td>
                    </tr>
                    <?php
                  }
                } else {
                  ?>
                  <tr>
                    <td colspan="6">
                      <p class="text-center font-italic mb-0">
                        BELUM ADA DATA
                      </p>
                    </td>
                  </tr>
                  <?php
                }
                ?>
              </tbody>
            </table>
          </div>
          <?php
          if(!empty($getRenstra)) {
            ?>
            <div class="card-footer">
              <a href="<?=site_url('sakipv2/individu/ajax-form-individu/add/'.$getRenstra)?>" class="btn btn-primary btn-add-individu font-weight-bold"><i class="far fa-plus-circle"></i>&nbsp;TAMBAH NOMENKLATUR</a>
            </div>
            <?php
          }
          ?>
        </div>
      </div>
    </div>
  </div>
</section>
<div class="modal fade" id="modalFormBidang" role="dialog">
  <div class="modal-dialog modal-lg" style="max-width: 1000px !important">
    <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">JAB. PELAKSANA / INDIVIDU</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true"><i class="fas fa-close"></i></span>
          </button>
        </div>
        <div class="modal-body">
        </div>
        <div class="modal-footer d-block">
          <div class="row">
            <div class="col-lg-12 text-center">
              <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="far fa-times-circle"></i>&nbsp;BATAL</button>
              <button type="submit" class="btn btn-primary btn-submit"><i class="far fa-plus-circle"></i>&nbsp;SUBMIT</button>
            </div>
          </div>
        </div>
    </div>
  </div>
</div>
<script type="text/javascript">
var modalFormBidang = $('#modalFormBidang');
$(document).ready(function(){
  $('select[name=filterRenstra],select[name=filterSkpd],select[name=filterPmd]').change(function(){
    var url = $(this).val();
    location.href = url;
  });

  modalFormBidang.on('hidden.bs.modal', function (e) {
    $('.modal-body', modalFormBidang).empty();
  });

  $('.btn-add-individu, .btn-edit-individu').click(function() {
    var url = $(this).attr('href');
    modalFormBidang.modal('show');
    $('.modal-body', modalFormBidang).html('<p class="font-italic mb-0 text-center"><i class="far fa-circle-notch fa-spin"></i>&nbsp;MEMUAT...</p>');
    $('.modal-body', modalFormBidang).load(url, function(){
      $('button[type=submit]', modalFormBidang).unbind('click').click(function(){
        $('form', modalFormBidang).submit();
      });
      $("select", modalFormBidang).not('.no-select2, .custom-select').select2({ width: 'resolve', theme: 'bootstrap4' });
    });

    return false;
  });

  $('.btn-change-individu').click(function() {
    var url = $(this).attr('href');
    var prompt = $(this).data('prompt');
    swal({
      title: "APAKAH ANDA YAKIN?",
      text: prompt,
      icon: "warning",
      buttons: [
        'BATAL',
        'YAKIN'
      ],
    }).then(function(isConfirm) {
      if (isConfirm) {
        $.ajax({
          url: url,
          method: "GET",
          dataType: "json"
        }).success(function(res) {
          if(res.error) {
            swal({
              title: 'ERROR',
              text: res.error,
              icon: 'error',
              buttons:false
            });
          } else {
            location.reload();
          }
        }).fail(function() {
          swal({
            title: 'SERVER ERROR',
            text: 'Terjadi kesalahan pada server. Silakan coba kembali.',
            icon: 'error',
            buttons:false
          });
        }).done(function() {

        });

      } else {

      }
    })
    return false;
  });
});
</script>
