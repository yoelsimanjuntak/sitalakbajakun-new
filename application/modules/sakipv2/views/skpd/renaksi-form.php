<div class="form-group">
  <div class="row">
    <div class="col-lg-12">
      <label>Uraian</label>
      <textarea class="form-control" name="<?=COL_RENAKSIURAIAN?>" placeholder="Rencana Aksi" required><?=!empty($data)?$data[COL_RENAKSIURAIAN]:''?></textarea>
    </div>
  </div>
</div>
<div class="form-group">
  <div class="row">
    <div class="col-lg-8">
      <label>Indikator</label>
      <input type="text" class="form-control" name="<?=COL_RENAKSIINDIKATOR?>" value="<?=!empty($data)?$data[COL_RENAKSIINDIKATOR]:''?>" required />
    </div>
    <div class="col-lg-4">
      <label>Satuan</label>
      <input type="text" class="form-control" name="<?=COL_RENAKSISATUAN?>" value="<?=!empty($data)?$data[COL_RENAKSISATUAN]:''?>" required />
    </div>
  </div>
</div>
<div class="form-group">
  <div class="row">
    <div class="col-lg-6">
      <label>Target</label>
      <input type="text" class="form-control" name="RenaksiTarget<?=$tw!=99?'TW'.$tw:''?>" value="<?=!empty($data)?$data['RenaksiTarget'.($tw!=99?'TW'.$tw:'')]:''?>" required />
    </div>
    <div class="col-lg-6">
      <label>Capaian</label>
      <input type="text" class="form-control" name="RenaksiCapaian<?=$tw!=99?'TW'.$tw:''?>" value="<?=!empty($data)?$data['RenaksiCapaian'.($tw!=99?'TW'.$tw:'')]:''?>" required />
    </div>
  </div>
</div>
<div class="form-group">
  <div class="row">
    <div class="col-lg-12">
      <label>Catatan / Lampiran</label>
      <textarea class="form-control" name="RenaksiRemarks<?=$tw!=99?'TW'.$tw:''?>" placeholder="Catatan / Lampiran / Bukti Dukung" required><?=!empty($data)?$data['RenaksiRemarks'.($tw!=99?'TW'.$tw:'')]:''?></textarea>
    </div>
  </div>
</div>
