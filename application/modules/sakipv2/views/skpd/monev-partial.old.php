<?php
if(empty($idSKPD) || empty($idRenstra) || empty($idDPA)) {
  ?>
  <p class="text-center font-italic font-weight-bold text-danger mb-0">
    DATA TIDAK DITEMUKAN
  </p>
  <?php
  exit();
}

$rpemda = $this->db
->where(COL_PMDISAKTIF, 1)
->get(TBL_SAKIPV2_PEMDA)
->row_array();
if(empty($rpemda)) {
  ?>
  <p class="text-center font-italic font-weight-bold text-danger mb-0">
    PERIODE PEMERINTAHAN AKTIF TIDAK DITEMUKAN
  </p>
  <?php
}

$rskpd = $this->db
->where(COL_SKPDID, $idSKPD)
->get(TBL_SAKIPV2_SKPD)
->row_array();
if(empty($rskpd)) {
  ?>
  <p class="text-center font-italic font-weight-bold text-danger mb-0">
    SKPD TIDAK DITEMUKAN
  </p>
  <?php
}

$rdpa = $this->db
->where(COL_DPAID, $idDPA)
->get(TBL_SAKIPV2_SKPD_RENSTRA_DPA)
->row_array();
if(empty($rskpd)) {
  ?>
  <p class="text-center font-italic font-weight-bold text-danger mb-0">
    DPA TIDAK DITEMUKAN
  </p>
  <?php
}

$rsasaran = $this->db
->join(TBL_SAKIPV2_SKPD_RENSTRA_TUJUAN,TBL_SAKIPV2_SKPD_RENSTRA_TUJUAN.'.'.COL_TUJUANID." = ".TBL_SAKIPV2_SKPD_RENSTRA_SASARAN.".".COL_IDTUJUAN,"left")
->join(TBL_SAKIPV2_SKPD_RENSTRA,TBL_SAKIPV2_SKPD_RENSTRA.'.'.COL_RENSTRAID." = ".TBL_SAKIPV2_SKPD_RENSTRA_TUJUAN.".".COL_IDRENSTRA,"left")
->where(COL_IDPEMDA, $rpemda[COL_PMDID])
->where(COL_IDSKPD, $idSKPD)
->where(COL_IDRENSTRA, $idRenstra)
->order_by(COL_TUJUANNO)
->order_by(COL_SASARANNO)
->get(TBL_SAKIPV2_SKPD_RENSTRA_SASARAN)
->result_array();

$rsubkeg = $this->db
->join(TBL_SAKIPV2_BID_KEGIATAN,TBL_SAKIPV2_BID_KEGIATAN.'.'.COL_KEGIATANID." = ".TBL_SAKIPV2_SUBBID_SUBKEGIATAN.".".COL_IDKEGIATAN,"left")
->join(TBL_SAKIPV2_BID_PROGRAM,TBL_SAKIPV2_BID_PROGRAM.'.'.COL_PROGRAMID." = ".TBL_SAKIPV2_BID_KEGIATAN.".".COL_IDPROGRAM,"left")
->join(TBL_SAKIPV2_SKPD_RENSTRA_DPA,TBL_SAKIPV2_SKPD_RENSTRA_DPA.'.'.COL_DPAID." = ".TBL_SAKIPV2_BID_PROGRAM.".".COL_IDDPA,"left")
->join(TBL_SAKIPV2_SKPD_RENSTRA,TBL_SAKIPV2_SKPD_RENSTRA.'.'.COL_RENSTRAID." = ".TBL_SAKIPV2_SKPD_RENSTRA_DPA.".".COL_IDRENSTRA,"left")
->where(COL_IDSKPD, $idSKPD)
->where(COL_IDDPA, $idDPA)
->order_by(COL_IDBID)
->order_by(COL_SUBKEGKODE)
->get(TBL_SAKIPV2_SUBBID_SUBKEGIATAN)
->result_array();
?>

<?php
if(!empty($modCetak)) {
  ?>
  <style>
  body {
    font-family: 'Source Sans Pro','Helvetica Neue',Helvetica,Arial,sans-serif;
  }
  th {
    background: #dedede !important;
  }
  th, td {
    font-size: 10pt !important;
    padding: 5px 10px;
  }
  table {
    width: 100%;
    border-collapse: collapse;
  }
  table, th, td {
    border: 1px solid black !important;
  }
  </style>
  <?php

} else {
  ?>
  <style>
  td, th {
    font-size: 10pt !important;
    padding: padding: 5px 10px !important;
  }
  td a {
    text-decoration-line: underline !important;
    text-decoration-style: dotted !important;
  }
  </style>
  <?php
}
?>
<h5 style="text-align: center; margin-top: 2rem; margin-bottom: 0 !important">
  <?=$title?><br />
  <?=strtoupper($rskpd[COL_SKPDNAMA])?><br />
  TAHUN <?=$rdpa[COL_DPATAHUN]?>
</h5>
<hr />
<?php
if(empty($modCetak)) {
  ?>
  <p class="font-weight-bold font-italic pl-3 pr-3 mb-0">
    PETUNJUK: <span class="text-danger">Klik pada nilai / angka di kolom RENCANA AKSI dan REALISASI untuk mengubah nilai / angka.</span>
  </p>
  <?php
}
?>
<div class="row" style="margin-top: 1rem">
  <div class="col-lg-12">
    <table class="table table-bordered">
      <thead>
        <tr>
          <th rowspan="2" style="width: 10px; white-space: nowrap">NO.</th>
          <th rowspan="2">URAIAN SASARAN / INDIKATOR</th>
          <th rowspan="2">SATUAN</th>
          <?php
          if(!empty($modCetak)) {
            if($modCetak=='RENCANA') {
              ?>
              <th class="bg-success disabled" colspan="6" style="text-align: center">RENCANA AKSI</th>
              <?php
            } else if($modCetak=='REALISASI') {
              ?>
              <th class="bg-success disabled" colspan="6" style="text-align: center">RENCANA AKSI</th>
              <th class="bg-success disabled" colspan="6" style="text-align: center">REALISASI</th>
              <?php
            }
          } else {
            ?>
            <th class="bg-primary disabled" colspan="6" style="text-align: center">RENCANA AKSI</th>
            <th class="bg-success disabled" colspan="6" style="text-align: center">REALISASI</th>
            <?php
          }
          ?>
        </tr>
        <tr>
          <?php
          if(!empty($modCetak) && $modCetak=='RENCANA') {
            ?>
            <th class="bg-primary disabled" style="white-space: nowrap">AKHIR</th>
            <th class="bg-primary disabled" style="white-space: nowrap"><?=$rdpa[COL_DPATAHUN]?></th>
            <th class="bg-primary disabled" style="white-space: nowrap">TW-I</th>
            <th class="bg-primary disabled" style="white-space: nowrap">TW-II</th>
            <th class="bg-primary disabled" style="white-space: nowrap">TW-III</th>
            <th class="bg-primary disabled" style="white-space: nowrap">TW-IV</th>
            <?php
          } else {
            ?>
            <th class="bg-primary disabled" style="white-space: nowrap">AKHIR</th>
            <th class="bg-primary disabled" style="white-space: nowrap"><?=$rdpa[COL_DPATAHUN]?></th>
            <th class="bg-primary disabled" style="white-space: nowrap">TW-I</th>
            <th class="bg-primary disabled" style="white-space: nowrap">TW-II</th>
            <th class="bg-primary disabled" style="white-space: nowrap">TW-III</th>
            <th class="bg-primary disabled" style="white-space: nowrap">TW-IV</th>
            <th class="bg-success disabled" style="white-space: nowrap">AKHIR</th>
            <th class="bg-success disabled" style="white-space: nowrap"><?=$rdpa[COL_DPATAHUN]?></th>
            <th class="bg-success disabled" style="white-space: nowrap">TW-I</th>
            <th class="bg-success disabled" style="white-space: nowrap">TW-II</th>
            <th class="bg-success disabled" style="white-space: nowrap">TW-III</th>
            <th class="bg-success disabled" style="white-space: nowrap">TW-IV</th>
            <?php
          }
          ?>
        </tr>
      </thead>
      <tbody>
        <?php
        if(!empty($rsasaran)) {
          $no=1;
          foreach($rsasaran as $s) {
            $rdet = $this->db
            ->where(COL_IDSASARAN, $s[COL_SASARANID])
            ->get(TBL_SAKIPV2_SKPD_RENSTRA_SASARANDET)
            ->result_array();
            ?>
            <tr>
              <td <?=!empty($rdet)?'rowspan="'.(count($rdet)+1).'"':''?> style="text-align: right; vertical-align: top"><?=$no?>.</td>
              <td colspan="<?=!empty($modCetak)?'8':'13'?>" style="font-style: italic; font-weight: bold"><?=strtoupper($s[COL_SASARANURAIAN])?></td>
            </tr>
            <?php
            $no++;
            foreach($rdet as $d) {
              $rval = $this->db
              ->where(COL_IDSASARANINDIKATOR, $d[COL_SSRINDIKATORID])
              ->where(COL_MONEVTAHUN, $rdpa[COL_DPATAHUN])
              ->order_by(COL_UNIQ, 'desc')
              ->get(TBL_SAKIPV2_SKPD_RENSTRA_SASARANMONEV)
              ->row_array();

              $hrefTargetRenstra = '<a class="btn-changeval" href="'.site_url('sakipv2/skpd/monev-ajax-change-sasaran/'.COL_SSRINDIKATORTARGET.'/'.$rdpa[COL_DPATAHUN].'/'.$d[COL_SSRINDIKATORID]).'" data-value="'.(!empty($d[COL_SSRINDIKATORTARGET])||$d[COL_SSRINDIKATORTARGET]!=null?$d[COL_SSRINDIKATORTARGET]:'').'">'.(!empty($d[COL_SSRINDIKATORTARGET])||$d[COL_SSRINDIKATORTARGET]!=null?$d[COL_SSRINDIKATORTARGET]:'N/A').'</a>';
              $hrefTargetTahun = '<a class="btn-changeval" href="'.site_url('sakipv2/skpd/monev-ajax-change-sasaran/'.COL_MONEVTARGET.'/'.$rdpa[COL_DPATAHUN].'/'.$d[COL_SSRINDIKATORID]).'" data-value="'.(!empty($rval)&&$rval[COL_MONEVTARGET]!=null?$rval[COL_MONEVTARGET]:'').'">'.(!empty($rval)&&$rval[COL_MONEVTARGET]!=null?$rval[COL_MONEVTARGET]:'N/A').'</a>';
              $hrefTargetTW1 = '<a class="btn-changeval" href="'.site_url('sakipv2/skpd/monev-ajax-change-sasaran/'.COL_MONEVTARGETTW1.'/'.$rdpa[COL_DPATAHUN].'/'.$d[COL_SSRINDIKATORID]).'" data-value="'.(!empty($rval)&&$rval[COL_MONEVTARGETTW1]!=null?$rval[COL_MONEVTARGETTW1]:'').'">'.(!empty($rval)&&$rval[COL_MONEVTARGETTW1]!=null?$rval[COL_MONEVTARGETTW1]:'N/A').'</a>';
              $hrefTargetTW2 = '<a class="btn-changeval" href="'.site_url('sakipv2/skpd/monev-ajax-change-sasaran/'.COL_MONEVTARGETTW2.'/'.$rdpa[COL_DPATAHUN].'/'.$d[COL_SSRINDIKATORID]).'" data-value="'.(!empty($rval)&&$rval[COL_MONEVTARGETTW2]!=null?$rval[COL_MONEVTARGETTW2]:'').'">'.(!empty($rval)&&$rval[COL_MONEVTARGETTW2]!=null?$rval[COL_MONEVTARGETTW2]:'N/A').'</a>';
              $hrefTargetTW3 = '<a class="btn-changeval" href="'.site_url('sakipv2/skpd/monev-ajax-change-sasaran/'.COL_MONEVTARGETTW3.'/'.$rdpa[COL_DPATAHUN].'/'.$d[COL_SSRINDIKATORID]).'" data-value="'.(!empty($rval)&&$rval[COL_MONEVTARGETTW3]!=null?$rval[COL_MONEVTARGETTW3]:'').'">'.(!empty($rval)&&$rval[COL_MONEVTARGETTW3]!=null?$rval[COL_MONEVTARGETTW3]:'N/A').'</a>';
              $hrefTargetTW4 = '<a class="btn-changeval" href="'.site_url('sakipv2/skpd/monev-ajax-change-sasaran/'.COL_MONEVTARGETTW4.'/'.$rdpa[COL_DPATAHUN].'/'.$d[COL_SSRINDIKATORID]).'" data-value="'.(!empty($rval)&&$rval[COL_MONEVTARGETTW4]!=null?$rval[COL_MONEVTARGETTW4]:'').'">'.(!empty($rval)&&$rval[COL_MONEVTARGETTW4]!=null?$rval[COL_MONEVTARGETTW4]:'N/A').'</a>';

              $hrefRealRenstra = '<a class="btn-changeval" href="'.site_url('sakipv2/skpd/monev-ajax-change-sasaran/'.COL_SSRINDIKATORREALISASI.'/'.$rdpa[COL_DPATAHUN].'/'.$d[COL_SSRINDIKATORID]).'" data-value="'.(!empty($d[COL_SSRINDIKATORREALISASI])||$d[COL_SSRINDIKATORREALISASI]!=null?$d[COL_SSRINDIKATORREALISASI]:'').'">'.(!empty($d[COL_SSRINDIKATORREALISASI])||$d[COL_SSRINDIKATORREALISASI]!=null?$d[COL_SSRINDIKATORREALISASI]:'N/A').'</a>';
              $hrefRealTahun = '<a class="btn-changeval" href="'.site_url('sakipv2/skpd/monev-ajax-change-sasaran/'.COL_MONEVREALISASI.'/'.$rdpa[COL_DPATAHUN].'/'.$d[COL_SSRINDIKATORID]).'" data-value="'.(!empty($rval)&&$rval[COL_MONEVREALISASI]!=null?$rval[COL_MONEVREALISASI]:'').'">'.(!empty($rval)&&$rval[COL_MONEVREALISASI]!=null?$rval[COL_MONEVREALISASI]:'N/A').'</a>';
              $hrefRealTW1 = '<a class="btn-changeval" href="'.site_url('sakipv2/skpd/monev-ajax-change-sasaran/'.COL_MONEVREALISASITW1.'/'.$rdpa[COL_DPATAHUN].'/'.$d[COL_SSRINDIKATORID]).'" data-value="'.(!empty($rval)&&$rval[COL_MONEVREALISASITW1]!=null?$rval[COL_MONEVREALISASI]:'').'">'.(!empty($rval)&&$rval[COL_MONEVREALISASITW1]!=null?$rval[COL_MONEVREALISASITW1]:'N/A').'</a>';
              $hrefRealTW2 = '<a class="btn-changeval" href="'.site_url('sakipv2/skpd/monev-ajax-change-sasaran/'.COL_MONEVREALISASITW2.'/'.$rdpa[COL_DPATAHUN].'/'.$d[COL_SSRINDIKATORID]).'" data-value="'.(!empty($rval)&&$rval[COL_MONEVREALISASITW2]!=null?$rval[COL_MONEVREALISASI]:'').'">'.(!empty($rval)&&$rval[COL_MONEVREALISASITW2]!=null?$rval[COL_MONEVREALISASITW2]:'N/A').'</a>';
              $hrefRealTW3 = '<a class="btn-changeval" href="'.site_url('sakipv2/skpd/monev-ajax-change-sasaran/'.COL_MONEVREALISASITW3.'/'.$rdpa[COL_DPATAHUN].'/'.$d[COL_SSRINDIKATORID]).'" data-value="'.(!empty($rval)&&$rval[COL_MONEVREALISASITW3]!=null?$rval[COL_MONEVREALISASI]:'').'">'.(!empty($rval)&&$rval[COL_MONEVREALISASITW3]!=null?$rval[COL_MONEVREALISASITW3]:'N/A').'</a>';
              $hrefRealTW4 = '<a class="btn-changeval" href="'.site_url('sakipv2/skpd/monev-ajax-change-sasaran/'.COL_MONEVREALISASITW4.'/'.$rdpa[COL_DPATAHUN].'/'.$d[COL_SSRINDIKATORID]).'" data-value="'.(!empty($rval)&&$rval[COL_MONEVREALISASITW4]!=null?$rval[COL_MONEVREALISASI]:'').'">'.(!empty($rval)&&$rval[COL_MONEVREALISASITW4]!=null?$rval[COL_MONEVREALISASITW4]:'N/A').'</a>';
              ?>
              <tr>
                <td><?=strtoupper($d[COL_SSRINDIKATORURAIAN])?></td>
                <td style="vertical-align: middle;"><?=$d[COL_SSRINDIKATORSATUAN]?></td>
                <?php
                if(!empty($modCetak)) {
                  if($modCetak=='RENCANA') {
                    ?>
                    <td class="bg-primary disabled" style="vertical-align: middle; font-weight: bold"><?=(!empty($d[COL_SSRINDIKATORTARGET])||$d[COL_SSRINDIKATORTARGET]!=null?$d[COL_SSRINDIKATORTARGET]:'N/A')?></td>
                    <td class="bg-primary disabled" style="vertical-align: middle; font-weight: bold"><?=(!empty($rval)&&$rval[COL_MONEVTARGET]!=null?$rval[COL_MONEVTARGET]:'N/A')?></td>
                    <td class="bg-primary disabled" style="vertical-align: middle; font-weight: bold"><?=(!empty($rval)&&$rval[COL_MONEVTARGETTW1]!=null?$rval[COL_MONEVTARGETTW1]:'N/A')?></td>
                    <td class="bg-primary disabled" style="vertical-align: middle; font-weight: bold"><?=(!empty($rval)&&$rval[COL_MONEVTARGETTW2]!=null?$rval[COL_MONEVTARGETTW2]:'N/A')?></td>
                    <td class="bg-primary disabled" style="vertical-align: middle; font-weight: bold"><?=(!empty($rval)&&$rval[COL_MONEVTARGETTW3]!=null?$rval[COL_MONEVTARGETTW3]:'N/A')?></td>
                    <td class="bg-primary disabled" style="vertical-align: middle; font-weight: bold"><?=(!empty($rval)&&$rval[COL_MONEVTARGETTW4]!=null?$rval[COL_MONEVTARGETTW4]:'N/A')?></td>
                    <?php
                  } else if($modCetak=='REALISASI') {
                    ?>
                    <td class="bg-primary disabled" style="vertical-align: middle; font-weight: bold"><?=(!empty($d[COL_SSRINDIKATORTARGET])||$d[COL_SSRINDIKATORTARGET]!=null?$d[COL_SSRINDIKATORTARGET]:'N/A')?></td>
                    <td class="bg-primary disabled" style="vertical-align: middle; font-weight: bold"><?=(!empty($rval)&&$rval[COL_MONEVTARGET]!=null?$rval[COL_MONEVTARGET]:'N/A')?></td>
                    <td class="bg-primary disabled" style="vertical-align: middle; font-weight: bold"><?=(!empty($rval)&&$rval[COL_MONEVTARGETTW1]!=null?$rval[COL_MONEVTARGETTW1]:'N/A')?></td>
                    <td class="bg-primary disabled" style="vertical-align: middle; font-weight: bold"><?=(!empty($rval)&&$rval[COL_MONEVTARGETTW2]!=null?$rval[COL_MONEVTARGETTW2]:'N/A')?></td>
                    <td class="bg-primary disabled" style="vertical-align: middle; font-weight: bold"><?=(!empty($rval)&&$rval[COL_MONEVTARGETTW3]!=null?$rval[COL_MONEVTARGETTW3]:'N/A')?></td>
                    <td class="bg-primary disabled" style="vertical-align: middle; font-weight: bold"><?=(!empty($rval)&&$rval[COL_MONEVTARGETTW4]!=null?$rval[COL_MONEVTARGETTW4]:'N/A')?></td>

                    <td class="bg-success disabled" style="vertical-align: middle; font-weight: bold"><?=(!empty($d[COL_SSRINDIKATORREALISASI])||$d[COL_SSRINDIKATORREALISASI]!=null?$d[COL_SSRINDIKATORREALISASI]:'N/A')?></td>
                    <td class="bg-success disabled" style="vertical-align: middle; font-weight: bold"><?=(!empty($rval)&&$rval[COL_MONEVREALISASI]!=null?$rval[COL_MONEVREALISASI]:'N/A')?></td>
                    <td class="bg-success disabled" style="vertical-align: middle; font-weight: bold"><?=(!empty($rval)&&$rval[COL_MONEVREALISASITW1]!=null?$rval[COL_MONEVREALISASI]:'N/A')?></td>
                    <td class="bg-success disabled" style="vertical-align: middle; font-weight: bold"><?=(!empty($rval)&&$rval[COL_MONEVREALISASITW2]!=null?$rval[COL_MONEVREALISASITW2]:'N/A')?></td>
                    <td class="bg-success disabled" style="vertical-align: middle; font-weight: bold"><?=(!empty($rval)&&$rval[COL_MONEVREALISASITW3]!=null?$rval[COL_MONEVREALISASITW3]:'N/A')?></td>
                    <td class="bg-success disabled" style="vertical-align: middle; font-weight: bold"><?=(!empty($rval)&&$rval[COL_MONEVREALISASITW4]!=null?$rval[COL_MONEVREALISASITW4]:'N/A')?></td>
                    <?php
                  }
                  ?>
                  <?php
                } else {
                  ?>
                  <td class="bg-primary disabled" style="vertical-align: middle; font-weight: bold"><?=$hrefTargetRenstra?></td>
                  <td class="bg-primary disabled" style="vertical-align: middle; font-weight: bold"><?=$hrefTargetTahun?></td>
                  <td class="bg-primary disabled" style="vertical-align: middle; font-weight: bold"><?=$hrefTargetTW1?></td>
                  <td class="bg-primary disabled" style="vertical-align: middle; font-weight: bold"><?=$hrefTargetTW2?></td>
                  <td class="bg-primary disabled" style="vertical-align: middle; font-weight: bold"><?=$hrefTargetTW3?></td>
                  <td class="bg-primary disabled" style="vertical-align: middle; font-weight: bold"><?=$hrefTargetTW4?></td>
                  <td class="bg-success disabled" style="vertical-align: middle; font-weight: bold"><?=$hrefRealRenstra?></td>
                  <td class="bg-success disabled" style="vertical-align: middle; font-weight: bold"><?=$hrefRealTahun?></td>
                  <td class="bg-success disabled" style="vertical-align: middle; font-weight: bold"><?=$hrefRealTW1?></td>
                  <td class="bg-success disabled" style="vertical-align: middle; font-weight: bold"><?=$hrefRealTW2?></td>
                  <td class="bg-success disabled" style="vertical-align: middle; font-weight: bold"><?=$hrefRealTW3?></td>
                  <td class="bg-success disabled" style="vertical-align: middle; font-weight: bold"><?=$hrefRealTW4?></td>
                  <?php
                }
                ?>
              </tr>
              <?php
            }
          }
        } else {
          ?>
          <tr>
            <td colspan="15">
              <p style="text-align: center; font-style: italic; margin-bottom: 0 !important">
                (KOSONG)
              </p>
            </td>
          </tr>
          <?php
        }
        ?>
      </tbody>
    </table>
  </div>
</div>
<div class="row" style="margin-top: 2rem">
  <div class="col-lg-12">
    <table class="table table-bordered" style="margin-bottom: 0 !important">
      <thead>
        <tr>
          <th rowspan="2" style="width: 10px; white-space: nowrap">KODE</th>
          <th rowspan="2">SUB KEGIATAN</th>
          <th rowspan="2" style="text-align: center">ANGGARAN<br /><?=$rdpa[COL_DPATAHUN]?></th>
          <?php
          if(!empty($modCetak)) {
            if($modCetak=='RENCANA') {
              ?>
              <th class="bg-primary disabled" colspan="4" style="text-align: center">RENCANA AKSI</th>
              <?php
            } else if($modCetak=='REALISASI') {
              ?>
              <th class="bg-primary disabled" colspan="4" style="text-align: center">RENCANA AKSI</th>
              <th class="bg-success disabled" colspan="4" style="text-align: center">REALISASI</th>
              <?php
            }
          } else {
            ?>
            <th class="bg-primary disabled" colspan="4" style="text-align: center">RENCANA AKSI</th>
            <th class="bg-success disabled" colspan="4" style="text-align: center">REALISASI</th>
            <?php
          }
          ?>
        </tr>
        <tr>
          <?php
          if(!empty($modCetak) && $modCetak=='RENCANA') {
            ?>
            <th class="bg-primary disabled" style="white-space: nowrap">TW-I</th>
            <th class="bg-primary disabled" style="white-space: nowrap">TW-II</th>
            <th class="bg-primary disabled" style="white-space: nowrap">TW-III</th>
            <th class="bg-primary disabled" style="white-space: nowrap">TW-IV</th>
            <?php
          } else {
            ?>
            <th class="bg-primary disabled" style="white-space: nowrap">TW-I</th>
            <th class="bg-primary disabled" style="white-space: nowrap">TW-II</th>
            <th class="bg-primary disabled" style="white-space: nowrap">TW-III</th>
            <th class="bg-primary disabled" style="white-space: nowrap">TW-IV</th>

            <th class="bg-success disabled" style="white-space: nowrap">TW-I</th>
            <th class="bg-success disabled" style="white-space: nowrap">TW-II</th>
            <th class="bg-success disabled" style="white-space: nowrap">TW-III</th>
            <th class="bg-success disabled" style="white-space: nowrap">TW-IV</th>
            <?php
          }
          ?>
        </tr>
      </thead>
      <tbody>
        <?php
        if(!empty($rsubkeg)) {
          $sumPagu = 0;
          $sumPaguTW1 = 0;
          $sumPaguTW2 = 0;
          $sumPaguTW3 = 0;
          $sumPaguTW4 = 0;

          $sumRealTW1 = 0;
          $sumRealTW2 = 0;
          $sumRealTW3 = 0;
          $sumRealTW4 = 0;
          foreach($rsubkeg as $s) {
            $hrefPaguTW1 = '<a class="btn-changeval" href="'.site_url('sakipv2/skpd/monev-ajax-change-anggaran/'.COL_SUBKEGPAGUTW1.'/'.$s[COL_SUBKEGID]).'" data-value="'.(!empty($s[COL_SUBKEGPAGUTW1])||$s[COL_SUBKEGPAGUTW1]!=null?$s[COL_SUBKEGPAGUTW1]:'').'">'.(!empty($s[COL_SUBKEGPAGUTW1])||$s[COL_SUBKEGPAGUTW1]!=null?number_format($s[COL_SUBKEGPAGUTW1]):'N/A').'</a>';
            $hrefPaguTW2 = '<a class="btn-changeval" href="'.site_url('sakipv2/skpd/monev-ajax-change-anggaran/'.COL_SUBKEGPAGUTW2.'/'.$s[COL_SUBKEGID]).'" data-value="'.(!empty($s[COL_SUBKEGPAGUTW2])||$s[COL_SUBKEGPAGUTW2]!=null?$s[COL_SUBKEGPAGUTW2]:'').'">'.(!empty($s[COL_SUBKEGPAGUTW2])||$s[COL_SUBKEGPAGUTW2]!=null?number_format($s[COL_SUBKEGPAGUTW2]):'N/A').'</a>';
            $hrefPaguTW3 = '<a class="btn-changeval" href="'.site_url('sakipv2/skpd/monev-ajax-change-anggaran/'.COL_SUBKEGPAGUTW3.'/'.$s[COL_SUBKEGID]).'" data-value="'.(!empty($s[COL_SUBKEGPAGUTW3])||$s[COL_SUBKEGPAGUTW3]!=null?$s[COL_SUBKEGPAGUTW3]:'').'">'.(!empty($s[COL_SUBKEGPAGUTW3])||$s[COL_SUBKEGPAGUTW3]!=null?number_format($s[COL_SUBKEGPAGUTW3]):'N/A').'</a>';
            $hrefPaguTW4 = '<a class="btn-changeval" href="'.site_url('sakipv2/skpd/monev-ajax-change-anggaran/'.COL_SUBKEGPAGUTW4.'/'.$s[COL_SUBKEGID]).'" data-value="'.(!empty($s[COL_SUBKEGPAGUTW4])||$s[COL_SUBKEGPAGUTW4]!=null?$s[COL_SUBKEGPAGUTW4]:'').'">'.(!empty($s[COL_SUBKEGPAGUTW4])||$s[COL_SUBKEGPAGUTW4]!=null?number_format($s[COL_SUBKEGPAGUTW4]):'N/A').'</a>';

            $hrefRealTW1 = '<a class="btn-changeval" href="'.site_url('sakipv2/skpd/monev-ajax-change-anggaran/'.COL_SUBKEGREALISASITW1.'/'.$s[COL_SUBKEGID]).'" data-value="'.(!empty($s[COL_SUBKEGREALISASITW1])||$s[COL_SUBKEGREALISASITW1]!=null?$s[COL_SUBKEGREALISASITW1]:'').'">'.(!empty($s[COL_SUBKEGREALISASITW1])||$s[COL_SUBKEGREALISASITW1]!=null?number_format($s[COL_SUBKEGREALISASITW1]):'N/A').'</a>';
            $hrefRealTW2 = '<a class="btn-changeval" href="'.site_url('sakipv2/skpd/monev-ajax-change-anggaran/'.COL_SUBKEGREALISASITW2.'/'.$s[COL_SUBKEGID]).'" data-value="'.(!empty($s[COL_SUBKEGREALISASITW2])||$s[COL_SUBKEGREALISASITW2]!=null?$s[COL_SUBKEGREALISASITW2]:'').'">'.(!empty($s[COL_SUBKEGREALISASITW2])||$s[COL_SUBKEGREALISASITW2]!=null?number_format($s[COL_SUBKEGREALISASITW2]):'N/A').'</a>';
            $hrefRealTW3 = '<a class="btn-changeval" href="'.site_url('sakipv2/skpd/monev-ajax-change-anggaran/'.COL_SUBKEGREALISASITW3.'/'.$s[COL_SUBKEGID]).'" data-value="'.(!empty($s[COL_SUBKEGREALISASITW3])||$s[COL_SUBKEGREALISASITW3]!=null?$s[COL_SUBKEGREALISASITW3]:'').'">'.(!empty($s[COL_SUBKEGREALISASITW3])||$s[COL_SUBKEGREALISASITW3]!=null?number_format($s[COL_SUBKEGREALISASITW3]):'N/A').'</a>';
            $hrefRealTW4 = '<a class="btn-changeval" href="'.site_url('sakipv2/skpd/monev-ajax-change-anggaran/'.COL_SUBKEGREALISASITW4.'/'.$s[COL_SUBKEGID]).'" data-value="'.(!empty($s[COL_SUBKEGREALISASITW4])||$s[COL_SUBKEGREALISASITW4]!=null?$s[COL_SUBKEGREALISASITW4]:'').'">'.(!empty($s[COL_SUBKEGREALISASITW4])||$s[COL_SUBKEGREALISASITW4]!=null?number_format($s[COL_SUBKEGREALISASITW4]):'N/A').'</a>';

            $sumPagu+=!empty($s[COL_SUBKEGPAGU])?$s[COL_SUBKEGPAGU]:0;
            $sumPaguTW1+=!empty($s[COL_SUBKEGPAGUTW1])?$s[COL_SUBKEGPAGUTW1]:0;
            $sumPaguTW2+=!empty($s[COL_SUBKEGPAGUTW2])?$s[COL_SUBKEGPAGUTW2]:0;
            $sumPaguTW3+=!empty($s[COL_SUBKEGPAGUTW3])?$s[COL_SUBKEGPAGUTW3]:0;
            $sumPaguTW4+=!empty($s[COL_SUBKEGPAGUTW4])?$s[COL_SUBKEGPAGUTW4]:0;

            $sumRealTW1+=!empty($s[COL_SUBKEGREALISASITW1])?$s[COL_SUBKEGREALISASITW1]:0;
            $sumRealTW2+=!empty($s[COL_SUBKEGREALISASITW2])?$s[COL_SUBKEGREALISASITW2]:0;
            $sumRealTW3+=!empty($s[COL_SUBKEGREALISASITW3])?$s[COL_SUBKEGREALISASITW3]:0;
            $sumRealTW4+=!empty($s[COL_SUBKEGREALISASITW4])?$s[COL_SUBKEGREALISASITW4]:0;
            ?>
            <tr>
              <td><?=$s[COL_SUBKEGKODE]?></td>
              <td><?=strtoupper($s[COL_SUBKEGURAIAN])?></td>
              <td style="white-space:nowrap; text-align:right; vertical-align:middle;"><?=number_format($s[COL_SUBKEGPAGU])?></td>
              <?php
              if(!empty($modCetak)) {
                if($modCetak=='RENCANA') {
                  ?>
                  <td style="white-space:nowrap; text-align:right; vertical-align:middle; font-weight:bold"><?=(!empty($s[COL_SUBKEGPAGUTW1])||$s[COL_SUBKEGPAGUTW1]!=null?number_format($s[COL_SUBKEGPAGUTW1]):'N/A')?></td>
                  <td style="white-space:nowrap; text-align:right; vertical-align:middle; font-weight:bold"><?=(!empty($s[COL_SUBKEGPAGUTW2])||$s[COL_SUBKEGPAGUTW2]!=null?number_format($s[COL_SUBKEGPAGUTW2]):'N/A')?></td>
                  <td style="white-space:nowrap; text-align:right; vertical-align:middle; font-weight:bold"><?=(!empty($s[COL_SUBKEGPAGUTW3])||$s[COL_SUBKEGPAGUTW3]!=null?number_format($s[COL_SUBKEGPAGUTW3]):'N/A')?></td>
                  <td style="white-space:nowrap; text-align:right; vertical-align:middle; font-weight:bold"><?=(!empty($s[COL_SUBKEGPAGUTW4])||$s[COL_SUBKEGPAGUTW4]!=null?number_format($s[COL_SUBKEGPAGUTW4]):'N/A')?></td>
                  <?php
                } else if($modCetak=='REALISASI') {
                  ?>
                  <td style="white-space:nowrap; text-align:right; vertical-align:middle; font-weight:bold"><?=(!empty($s[COL_SUBKEGPAGUTW1])||$s[COL_SUBKEGPAGUTW1]!=null?number_format($s[COL_SUBKEGPAGUTW1]):'N/A')?></td>
                  <td style="white-space:nowrap; text-align:right; vertical-align:middle; font-weight:bold"><?=(!empty($s[COL_SUBKEGPAGUTW2])||$s[COL_SUBKEGPAGUTW2]!=null?number_format($s[COL_SUBKEGPAGUTW2]):'N/A')?></td>
                  <td style="white-space:nowrap; text-align:right; vertical-align:middle; font-weight:bold"><?=(!empty($s[COL_SUBKEGPAGUTW3])||$s[COL_SUBKEGPAGUTW3]!=null?number_format($s[COL_SUBKEGPAGUTW3]):'N/A')?></td>
                  <td style="white-space:nowrap; text-align:right; vertical-align:middle; font-weight:bold"><?=(!empty($s[COL_SUBKEGPAGUTW4])||$s[COL_SUBKEGPAGUTW4]!=null?number_format($s[COL_SUBKEGPAGUTW4]):'N/A')?></td>

                  <td style="white-space:nowrap; text-align:right; vertical-align:middle; font-weight:bold"><?=(!empty($s[COL_SUBKEGREALISASITW1])||$s[COL_SUBKEGREALISASITW1]!=null?number_format($s[COL_SUBKEGREALISASITW1]):'N/A')?></td>
                  <td style="white-space:nowrap; text-align:right; vertical-align:middle; font-weight:bold"><?=(!empty($s[COL_SUBKEGREALISASITW2])||$s[COL_SUBKEGREALISASITW2]!=null?number_format($s[COL_SUBKEGREALISASITW2]):'N/A')?></td>
                  <td style="white-space:nowrap; text-align:right; vertical-align:middle; font-weight:bold"><?=(!empty($s[COL_SUBKEGREALISASITW3])||$s[COL_SUBKEGREALISASITW3]!=null?number_format($s[COL_SUBKEGREALISASITW3]):'N/A')?></td>
                  <td style="white-space:nowrap; text-align:right; vertical-align:middle; font-weight:bold"><?=(!empty($s[COL_SUBKEGREALISASITW4])||$s[COL_SUBKEGREALISASITW4]!=null?number_format($s[COL_SUBKEGREALISASITW4]):'N/A')?></td>
                  <?php
                }
              } else {
                ?>
                <td class="bg-primary disabled" style="white-space:nowrap; text-align:right; vertical-align:middle; font-weight:bold"><?=$hrefPaguTW1?></td>
                <td class="bg-primary disabled" style="white-space:nowrap; text-align:right; vertical-align:middle; font-weight:bold"><?=$hrefPaguTW2?></td>
                <td class="bg-primary disabled" style="white-space:nowrap; text-align:right; vertical-align:middle; font-weight:bold"><?=$hrefPaguTW3?></td>
                <td class="bg-primary disabled" style="white-space:nowrap; text-align:right; vertical-align:middle; font-weight:bold"><?=$hrefPaguTW4?></td>

                <td class="bg-success disabled" style="white-space:nowrap; text-align:right; vertical-align:middle; font-weight:bold"><?=$hrefRealTW1?></td>
                <td class="bg-success disabled" style="white-space:nowrap; text-align:right; vertical-align:middle; font-weight:bold"><?=$hrefRealTW2?></td>
                <td class="bg-success disabled" style="white-space:nowrap; text-align:right; vertical-align:middle; font-weight:bold"><?=$hrefRealTW3?></td>
                <td class="bg-success disabled" style="white-space:nowrap; text-align:right; vertical-align:middle; font-weight:bold"><?=$hrefRealTW4?></td>
                <?php
              }
              ?>
            </tr>
            <?php
          }
          ?>
          <tr>
            <td colspan="2" style="font-weight: bold; text-align: right">TOTAL</td>
            <td style="white-space:nowrap; text-align:right; vertical-align:middle; font-weight: bold;"><?=number_format($sumPagu)?></td>
            <?php
            if(!empty($modCetak)) {
              if($modCetak=='RENCANA') {
                ?>
                <td style="white-space:nowrap; text-align:right; vertical-align:middle; font-weight:bold"><?=number_format($sumPaguTW1)?></td>
                <td style="white-space:nowrap; text-align:right; vertical-align:middle; font-weight:bold"><?=number_format($sumPaguTW2)?></td>
                <td style="white-space:nowrap; text-align:right; vertical-align:middle; font-weight:bold"><?=number_format($sumPaguTW3)?></td>
                <td style="white-space:nowrap; text-align:right; vertical-align:middle; font-weight:bold"><?=number_format($sumPaguTW4)?></td>
                <?php
              } else if($modCetak=='REALISASI') {
                ?>
                <td style="white-space:nowrap; text-align:right; vertical-align:middle; font-weight:bold"><?=number_format($sumPaguTW1)?></td>
                <td style="white-space:nowrap; text-align:right; vertical-align:middle; font-weight:bold"><?=number_format($sumPaguTW2)?></td>
                <td style="white-space:nowrap; text-align:right; vertical-align:middle; font-weight:bold"><?=number_format($sumPaguTW3)?></td>
                <td style="white-space:nowrap; text-align:right; vertical-align:middle; font-weight:bold"><?=number_format($sumPaguTW4)?></td>

                <td style="white-space:nowrap; text-align:right; vertical-align:middle; font-weight:bold"><?=number_format($sumRealTW1)?></td>
                <td style="white-space:nowrap; text-align:right; vertical-align:middle; font-weight:bold"><?=number_format($sumRealTW2)?></td>
                <td style="white-space:nowrap; text-align:right; vertical-align:middle; font-weight:bold"><?=number_format($sumRealTW3)?></td>
                <td style="white-space:nowrap; text-align:right; vertical-align:middle; font-weight:bold"><?=number_format($sumRealTW4)?></td>
                <?php
              }
            } else {
              ?>
              <td class="bg-primary disabled" style="white-space:nowrap; text-align:right; vertical-align:middle; font-weight:bold"><?=number_format($sumPaguTW1)?></td>
              <td class="bg-primary disabled" style="white-space:nowrap; text-align:right; vertical-align:middle; font-weight:bold"><?=number_format($sumPaguTW2)?></td>
              <td class="bg-primary disabled" style="white-space:nowrap; text-align:right; vertical-align:middle; font-weight:bold"><?=number_format($sumPaguTW3)?></td>
              <td class="bg-primary disabled" style="white-space:nowrap; text-align:right; vertical-align:middle; font-weight:bold"><?=number_format($sumPaguTW4)?></td>

              <td class="bg-success disabled" style="white-space:nowrap; text-align:right; vertical-align:middle; font-weight:bold"><?=number_format($sumRealTW1)?></td>
              <td class="bg-success disabled" style="white-space:nowrap; text-align:right; vertical-align:middle; font-weight:bold"><?=number_format($sumRealTW2)?></td>
              <td class="bg-success disabled" style="white-space:nowrap; text-align:right; vertical-align:middle; font-weight:bold"><?=number_format($sumRealTW3)?></td>
              <td class="bg-success disabled" style="white-space:nowrap; text-align:right; vertical-align:middle; font-weight:bold"><?=number_format($sumRealTW4)?></td>
              <?php
            }
            ?>
          </tr>
          <?php
        } else {
          ?>
          <tr>
            <td colspan="11">
              <p style="text-align: center; font-style: italic; margin-bottom: 0 !important">
                (KOSONG)
              </p>
            </td>
          </tr>
          <?php
        }
        ?>
      </tbody>
    </table>
  </div>
</div>
<script type="text/javascript">
$(document).ready(function(){

});
</script>
