<?php
$rskpd = $this->db
->where(COL_SKPDISAKTIF, 1)
->order_by(COL_SKPDURUSAN, 'asc')
->order_by(COL_SKPDBIDANG, 'asc')
->order_by(COL_SKPDUNIT, 'asc')
->order_by(COL_SKPDSUBUNIT, 'asc')
->get(TBL_SAKIPV2_SKPD)
->result_array();
?>
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h3 class="m-0 font-weight-light"><?=$title?></h3>
      </div>
      <?php
      if(!empty($navs)) {
        ?>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <?php
            foreach($navs as $n) {
              if(!empty($n['link'])) {
                ?>
                <li class="breadcrumb-item"><a href="<?=$n['link']?>"><?=$n['text']?></a></li>
                <?php
              } else {
                ?>
                <li class="breadcrumb-item active"><?=$n['text']?></li>
                <?php
              }
            }
            ?>
          </ol>
        </div>
        <?php
      }
      ?>
    </div>
  </div>
</div>
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-12">
        <div class="card card-outline card-secondary">
          <?php
          if(!empty($subtitle)) {
            ?>
            <div class="card-header">
              <h4 class="card-title"><?=$subtitle?></h4>
              <div class="card-tools">
                <div class="input-group input-group-sm" style="width: 300px;">
                  <input type="text" id="filterSKPD" class="form-control float-right" placeholder="CARI...">
                </div>
              </div>
            </div>
            <?php
          }
          ?>
          <div class="card-body p-0">
            <table id="table-skpd" class="table table-bordered">
              <thead>
                <tr>
                  <th>KODE</th>
                  <th>NAMA / NAMA PIMPINAN</th>
                  <th style="white-space: nowrap; text-align: center !important">AKSI</th>
                </tr>
              </thead>
              <tbody>
                <?php
                if(!empty($rskpd)) {
                  foreach($rskpd as $r) {
                    ?>
                    <tr>
                      <td class="text-right filterable" style="width: 100px; white-space: nowrap">
                        <?=$r[COL_SKPDURUSAN].'.'.$r[COL_SKPDBIDANG].'.'.$r[COL_SKPDUNIT].'.'.$r[COL_SKPDSUBUNIT]?>
                      </td>
                      <td class="filterable">
                        <strong><?=strtoupper($r[COL_SKPDNAMA])?></strong><br /><small class="font-italic"><?=!empty($r[COL_SKPDNAMAPIMPINAN])?strtoupper($r[COL_SKPDNAMAPIMPINAN]):'-'?></small>
                      </td>
                      <td class="text-center" style="white-space: nowrap; vertical-align: middle">
                        <a href="<?=site_url('sakipv2/skpd/ajax-form-skpd/edit/'.$r[COL_SKPDID])?>" data-toggle="tooltip" data-placement="bottom" title="UBAH" class="btn btn-primary btn-sm btn-edit-skpd"><i class="far fa-edit"></i></a>
                        <a href="<?=site_url('sakipv2/skpd/index').'?opr=detail-skpd&id='.$r[COL_SKPDID]?>" data-toggle="tooltip" data-placement="bottom" title="TELUSURI" class="btn btn-info btn-sm"><i class="far fa-search"></i></a>
                      </td>
                    </tr>
                    <?php
                  }
                } else {
                  ?>
                  <tr>
                    <td colspan="3">
                      <p class="text-center font-italic mb-0">
                        BELUM ADA DATA
                      </p>
                    </td>
                  </tr>
                  <?php
                }
                ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<div class="modal fade" id="modalFormSkpd" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">SKPD</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true"><i class="fas fa-close"></i></span>
          </button>
        </div>
        <div class="modal-body">
        </div>
        <div class="modal-footer d-block">
          <div class="row">
            <div class="col-lg-12 text-center">
              <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="far fa-times-circle"></i>&nbsp;BATAL</button>
              <button type="submit" class="btn btn-primary btn-submit"><i class="far fa-plus-circle"></i>&nbsp;SUBMIT</button>
            </div>
          </div>
        </div>
    </div>
  </div>
</div>
<script type="text/javascript">
var modalFormSkpd = $('#modalFormSkpd');
$(document).ready(function() {
  modalFormSkpd.on('hidden.bs.modal', function (e) {
    $('.modal-body', modalFormSkpd).empty();
    $('.modal-title', modalFormSkpd).html('SKPD');
  });

  $('.btn-add-skpd, .btn-edit-skpd').click(function() {
    var url = $(this).attr('href');
    modalFormSkpd.modal('show');
    $('.modal-body', modalFormSkpd).html('<p class="font-italic mb-0 text-center"><i class="far fa-circle-notch fa-spin"></i>&nbsp;MEMUAT...</p>');
    $('.modal-body', modalFormSkpd).load(url, function(){
      $('button[type=submit]', modalFormSkpd).unbind('click').click(function(){
        $('form', modalFormSkpd).submit();
      });
    });
    return false;
  });

  $( "#filterSKPD" ).keyup(function() {
    $('tr', $('tbody', $('#table-skpd'))).removeClass('d-none');
    $('tr.empty', $('tbody', $('#table-skpd'))).remove();
    var keyword = $(this).val();
    var selected = [];
    var rows = $('tr', $('tbody', $('#table-skpd')));
    $.map(rows, function(row, i) {
      var cols = $('td', $(row));
      var kode = $(cols[0]).html().replace(/(<([^>]+)>)/gi, "").trim();
      var skpd = $(cols[1]).html().replace(/(<([^>]+)>)/gi, "").trim();
      if(!kode.toLowerCase().includes(keyword.toLowerCase()) && !skpd.toLowerCase().includes(keyword.toLowerCase())) {
        $(row).addClass('d-none');
      } else {
        selected.push($(row));
      }
    });

    if(selected.length == 0) {
      $('tbody', $('#table-skpd')).append('<tr class="empty"><td colspan="4" class="text-center font-italic">DATA TIDAK DITEMUKAN</td></tr>');
    }
  });
});
</script>
