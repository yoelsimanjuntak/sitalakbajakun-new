<?php
$rOptRenstra = $this->db
->where(COL_IDPEMDA, $rrenstra[COL_IDPEMDA])
->where(COL_IDSKPD, $rrenstra[COL_IDSKPD])
->order_by(COL_RENSTRATAHUN, 'desc')
->get(TBL_SAKIPV2_SKPD_RENSTRA)
->result_array();

$arrFungsi = array();
$arrIKU = array();
if(!empty($rrenstra[COL_RENSTRAFUNGSI])) $arrFungsi = json_decode($rrenstra[COL_RENSTRAFUNGSI]);
if(!empty($rrenstra[COL_RENSTRAIKU])) $arrIKU = json_decode($rrenstra[COL_RENSTRAIKU]);

$rtujuan = $this->db
->where(COL_IDRENSTRA, $rrenstra[COL_RENSTRAID])
->order_by(COL_TUJUANNO)
->get(TBL_SAKIPV2_SKPD_RENSTRA_TUJUAN)
->result_array();

$rdpa = $this->db
->where(COL_IDRENSTRA, $rrenstra[COL_RENSTRAID])
->order_by(COL_DPATAHUN, 'desc')
->order_by(COL_DPAID, 'desc')
->get(TBL_SAKIPV2_SKPD_RENSTRA_DPA)
->result_array();
?>
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-4">
        <h3 class="m-0 font-weight-light"><?=$title?></h3>
      </div>
      <?php
      if(!empty($navs)) {
        ?>
        <div class="col-sm-8">
          <ol class="breadcrumb float-sm-right">
            <?php
            foreach($navs as $n) {
              if(!empty($n['link'])) {
                ?>
                <li class="breadcrumb-item"><a href="<?=$n['link']?>"><?=$n['text']?></a></li>
                <?php
              } else {
                ?>
                <li class="breadcrumb-item active"><?=$n['text']?></li>
                <?php
              }
            }
            ?>
          </ol>
        </div>
        <?php
      }
      ?>
    </div>
  </div>
</div>
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-12">
        <div class="card card-outline card-secondary">
          <div class="card-header">
            <h4 class="card-title"><?=!empty($subtitle)?$subtitle:''?></h4>
          </div>
          <div class="card-body p-0">
            <table class="table">
              <tbody>
                <tr>
                  <td class="text-right" style="width: 150px; white-space: nowrap; vertical-align: middle">RENSTRA</td>
                  <td style="width: 10px; white-space: nowrap; vertical-align: middle">:</td>
                  <td class="font-weight-bold">
                    <select class="form-control" name="filterRenstra">
                      <?php
                      foreach($rOptRenstra as $opt) {
                        ?>
                        <option value="<?=site_url('sakipv2/skpd/index').'?opr=detail-renstra&id='.$opt[COL_RENSTRAID]?>" <?=$opt[COL_RENSTRAID]==$rrenstra[COL_RENSTRAID]?'selected':''?>>
                          <?=$opt[COL_RENSTRATAHUN].' - '.strtoupper($opt[COL_RENSTRAURAIAN])?>
                        </option>
                        <?php
                      }
                      ?>
                    </select>
                  </td>
                </tr>
                <tr>
                  <td class="text-right" style="width: 150px; white-space: nowrap; vertical-align: middle">STATUS</td>
                  <td style="width: 10px; white-space: nowrap; vertical-align: middle">:</td>
                  <td class="font-weight-bold">
                    <?=$rrenstra[COL_RENSTRAISAKTIF]==1?'<span data-toggle="tooltip" title="STATUS" class="badge bg-success">AKTIF</span>':'<span data-toggle="tooltip" title="STATUS" class="badge bg-secondary">INAKTIF</span>'?>
                  </td>
                </tr>
                <tr>
                  <td class="text-right" style="width: 150px; white-space: nowrap; vertical-align: middle">STRUKTUR ORGANISASI</td>
                  <td style="width: 10px; white-space: nowrap; vertical-align: middle">:</td>
                  <td class="font-weight-bold">
                    <?php
                    if(!empty($rrenstra[COL_RENSTRAORG])) {
                      ?>
                      <a href="<?=MY_IMAGEURL.$rrenstra[COL_RENSTRAORG]?>" target="_blank" class="btn btn-success btn-xs btn-view-struktur"><i class="far fa-eye"></i>&nbsp;&nbsp;LIHAT</a>
                      <?php
                    }
                    ?>
                    <button type="button" data-toggle="modal" data-target="#modalFormUpload" class="btn btn-primary btn-xs"><i class="far fa-upload"></i>&nbsp;&nbsp;UNGGAH</button>
                  </td>
                </tr>
                <tr>
                  <td class="text-right" style="width: 150px; white-space: nowrap;">TUGAS POKOK</td>
                  <td style="width: 10px; white-space: nowrap;">:</td>
                  <td class="font-weight-bold font-italic">
                    <?=!empty($rrenstra[COL_RENSTRATUGASPOKOK])?$rrenstra[COL_RENSTRATUGASPOKOK]:'(KOSONG)'?>
                  </td>
                </tr>
                <tr>
                  <td class="text-right" style="width: 150px; white-space: nowrap;">FUNGSI</td>
                  <td style="width: 10px; white-space: nowrap;">:</td>
                  <td class="font-weight-bold">
                    <?php
                    if(!empty($arrFungsi)) {
                      ?>
                      <ul class="todo-list ui-sortable mt-2" data-widget="todo-list">
                        <?php
                        foreach($arrFungsi as $r) {
                          ?>
                          <li>
                            <span class="text"><?=strtoupper($r)?></span>
                          </li>
                          <?php
                        }
                        ?>
                      </ul>
                      <?php
                    } else {
                      echo '<span class="font-italic">(KOSONG)</span>';
                    }
                    ?>
                  </td>
                </tr>
                <tr>
                  <td class="text-right" style="width: 150px; white-space: nowrap;">INDIKATOR KINERJA UTAMA</td>
                  <td style="width: 10px; white-space: nowrap;">:</td>
                  <td class="font-weight-bold">
                    <?php
                    if(!empty($arrIKU)) {
                      ?>
                      <ul class="todo-list ui-sortable mt-2" data-widget="todo-list">
                        <?php
                        $idx=0;
                        foreach($arrIKU as $r) {
                          ?>
                          <li>
                            <div class="row">
                              <div class="col-lg-10">
                                <span class="text"><?=strtoupper($r->Uraian)?></span>
                              </div>
                              <div class="col-lg-2">
                                <small class="font-italic">TARGET : <strong class="pull-right font-italic"><?=$r->Target.' ('.strtoupper($r->Satuan).')'?></strong></small>
                              </div>

                            </div>

                          </li>
                          <?php
                          $idx++;
                        }
                        ?>
                      </ul>
                      <?php
                    } else {
                      echo '<span class="font-italic">(KOSONG)</span>';
                    }
                    ?>
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
          <div class="card-footer d-block">
            <div class="row">
              <div class="col-lg-12 text-center">
                <button type="button" id="btnFormIKU" class="btn btn-primary font-weight-bold"><i class="far fa-edit"></i>&nbsp;UPDATE DATA</button>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-lg-6">
        <div class="card card-outline card-secondary">
          <div class="card-header">
            <h4 class="card-title">DAFTAR TUJUAN</h4>
          </div>
          <div class="card-body p-0">
            <table class="table table-bordered">
              <thead>
                <tr>
                  <th style="width: 10px; white-space: nowrap">NO</th>
                  <th>URAIAN TUJUAN</th>
                  <th class="text-center" style="width: 100px; white-space: nowrap">AKSI</th>
                </tr>
              </thead>
              <tbody>
                <?php
                if(!empty($rtujuan)) {
                  foreach($rtujuan as $r) {
                    ?>
                    <tr>
                      <td style="width: 10px; white-space: nowrap"><?=$r[COL_TUJUANNO]?></td>
                      <td><?=strtoupper($r[COL_TUJUANURAIAN])?></td>
                      <td class="text-center" style="white-space: nowrap">
                        <?php
                        if($rrenstra[COL_RENSTRAISAKTIF] == 1) {
                          ?>
                          <a href="<?=site_url('sakipv2/skpd/ajax-form-tujuan/edit/'.$r[COL_IDRENSTRA].'/'.$r[COL_TUJUANID])?>" data-toggle="tooltip" data-placement="bottom" title="UBAH" class="btn btn-primary btn-sm btn-edit-tujuan"><i class="far fa-edit"></i></a>
                          <a href="<?=site_url('sakipv2/skpd/ajax-change-tujuan/delete/'.$r[COL_TUJUANID])?>" data-toggle="tooltip" data-placement="bottom" title="HAPUS" data-prompt="Menghapus data tujuan akan otomatis menghapus data INDIKATOR dan SASARAN terkait." class="btn btn-danger btn-sm btn-change-tujuan"><i class="far fa-times-circle"></i></a>
                          <?php
                        }
                        ?>
                        <a href="<?=site_url('sakipv2/skpd/index').'?opr=detail-tujuan&id='.$r[COL_TUJUANID]?>" data-toggle="tooltip" data-placement="bottom" title="TELUSURI" class="btn btn-info btn-sm"><i class="far fa-search"></i></a>
                      </td>
                    </tr>
                    <?php
                  }
                } else {
                  ?>
                  <tr>
                    <td colspan="3">
                      <p class="text-center font-italic mb-0">
                        BELUM ADA DATA
                      </p>
                    </td>
                  </tr>
                  <?php
                }
                ?>
              </tbody>
            </table>
          </div>
          <?php
          if($rrenstra[COL_RENSTRAISAKTIF] == 1) {
            ?>
            <div class="card-footer">
              <a href="<?=site_url('sakipv2/skpd/ajax-form-tujuan/add/'.$rrenstra[COL_RENSTRAID])?>" class="btn btn-primary btn-add-tujuan font-weight-bold"><i class="far fa-plus-circle"></i>&nbsp;TAMBAH TUJUAN</a>
            </div>
            <?php
          }
          ?>
        </div>
      </div>
      <div class="col-lg-6">
        <div class="card card-outline card-secondary">
          <div class="card-header">
            <h4 class="card-title">DAFTAR DPA</h4>
          </div>
          <div class="card-body p-0">
            <table class="table table-bordered">
              <thead>
                <tr>
                  <th style="width: 100px; white-space: nowrap; text-align: center !important">TAHUN</th>
                  <th>KETERANGAN</th>
                  <th style="width: 100px; white-space: nowrap; text-align: center !important">STATUS</th>
                  <th style="width: 100px; white-space: nowrap; text-align: center !important">AKSI</th>
                </tr>
              </thead>
              <tbody>
                <?php
                if(!empty($rdpa)) {
                  foreach($rdpa as $r) {
                    ?>
                    <tr>
                      <td class="text-center" style="width: 100px; white-space: nowrap">
                        <?=$r[COL_DPATAHUN]?>
                      </td>
                      <td>
                        <?=$r[COL_DPAURAIAN]?>
                      </td>
                      <td style="white-space: nowrap;">
                        <?=$r[COL_DPAISAKTIF]==1?'<span class="badge bg-success">AKTIF</span>':'<span class="badge bg-secondary">INAKTIF</span>'?>
                      </td>
                      <td class="text-center" style="white-space: nowrap">
                        <?php
                        if($rrenstra[COL_RENSTRAISAKTIF] == 1) {
                          ?>
                          <a href="<?=site_url('sakipv2/skpd/ajax-form-dpa/edit/'.$r[COL_DPAID])?>" data-toggle="tooltip" data-placement="bottom" title="UBAH" class="btn btn-primary btn-sm btn-edit-dpa"><i class="far fa-edit"></i></a>
                          <?php
                          if($r[COL_DPAISAKTIF]==1) {
                            ?>
                            <button type="button" class="btn btn-secondary btn-sm" disabled><i class="far fa-check-circle"></i></button>
                            <?php
                          } else {
                            ?>
                            <a href="<?=site_url('sakipv2/skpd/ajax-change-dpa/activate/'.$r[COL_DPAID])?>" data-toggle="tooltip" data-placement="bottom" title="AKTIFKAN" data-prompt="Mengaktifkan data DPA SKPD akan otomatis me-NONAKTIF-kan data DPA SKPD lainnya yang berstatus AKTIF." class="btn btn-success btn-sm btn-change-dpa"><i class="far fa-check-circle"></i></a>
                            <?php
                          }
                          ?>
                          <a href="<?=site_url('sakipv2/skpd/ajax-form-dpa/duplicate/'.$r[COL_DPAID])?>" data-toggle="tooltip" data-placement="bottom" title="DUPLIKASI" class="btn btn-info btn-sm btn-duplicate-dpa"><i class="far fa-copy"></i></a>
                          <a href="<?=site_url('sakipv2/skpd/ajax-change-dpa/delete/'.$r[COL_DPAID])?>" data-toggle="tooltip" data-placement="bottom" title="HAPUS" data-prompt="DPA tidak dapat dihapus jika masih terdapat data turunan terkait (PROGRAM, KEGIATAN, SUB KEGIATAN) di dalamnya." class="btn btn-danger btn-sm btn-change-dpa"><i class="far fa-times-circle"></i></a>
                          <?php
                        } else {
                          ?>
                          -
                          <?php
                        }
                        ?>
                      </td>
                    </tr>
                    <?php
                  }
                } else {
                  ?>
                  <tr>
                    <td colspan="4">
                      <p class="text-center font-italic mb-0">
                        BELUM ADA DATA
                      </p>
                    </td>
                  </tr>
                  <?php
                }
                ?>
              </tbody>
            </table>
          </div>
          <?php
          if($rrenstra[COL_RENSTRAISAKTIF] == 1) {
            ?>
            <div class="card-footer">
              <a href="<?=site_url('sakipv2/skpd/ajax-form-dpa/add/'.$rrenstra[COL_RENSTRAID])?>" class="btn btn-primary btn-add-dpa font-weight-bold"><i class="far fa-plus-circle"></i>&nbsp;TAMBAH DPA</a>
            </div>
            <?php
          }
          ?>
        </div>
      </div>
    </div>
  </div>
</section>
<div class="modal fade" id="modalFormTujuan" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">FORM TUJUAN SKPD</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true"><i class="fas fa-close"></i></span>
          </button>
        </div>
        <div class="modal-body">
        </div>
        <div class="modal-footer d-block">
          <div class="row">
            <div class="col-lg-12 text-center">
              <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="far fa-times-circle"></i>&nbsp;BATAL</button>
              <button type="submit" class="btn btn-primary btn-submit"><i class="far fa-plus-circle"></i>&nbsp;SUBMIT</button>
            </div>
          </div>
        </div>
    </div>
  </div>
</div>
<div class="modal fade" id="modalFormDPA" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">Form DPA SKPD</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true"><i class="fas fa-close"></i></span>
          </button>
        </div>
        <div class="modal-body">
        </div>
        <div class="modal-footer d-block">
          <div class="row">
            <div class="col-lg-12 text-center">
              <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="far fa-times-circle"></i>&nbsp;BATAL</button>
              <button type="submit" class="btn btn-primary btn-submit"><i class="far fa-plus-circle"></i>&nbsp;SUBMIT</button>
            </div>
          </div>
        </div>
    </div>
  </div>
</div>
<div class="modal fade" id="modalFormIKU" role="dialog">
  <div class="modal-dialog modal-lg" style="max-width: 1000px !important">
    <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">FORM TUPOKSI & IKU</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true"><i class="fas fa-close"></i></span>
          </button>
        </div>
        <div class="modal-body">
          <form id="form-iku" action="<?=site_url('sakipv2/skpd/ajax-form-renstra/edit/'.$rrenstra[COL_RENSTRAID])?>" method="POST">
            <input type="hidden" name=<?=COL_RENSTRATAHUN?> value="<?=$rrenstra[COL_RENSTRATAHUN]?>" />
            <input type="hidden" name=<?=COL_RENSTRAURAIAN?> value="<?=$rrenstra[COL_RENSTRAURAIAN]?>" />
            <div class="form-group">
              <label>TUGAS POKOK</label>
              <textarea class="form-control" name="<?=COL_RENSTRATUGASPOKOK?>" placeholder="URAIAN TUGAS POKOK" required><?=$rrenstra[COL_RENSTRATUGASPOKOK]?></textarea>
            </div>
            <div class="form-group">
              <label>FUNGSI</label>
              <table id="tbl-fungsi" class="table table-bordered">
                <thead class="bg-default">
                  <tr>
                    <th>URAIAN FUNGSI</th>
                    <th class="text-center" style="width: 10px; white-space: nowrap">
                      <button type="button" id="btn-add-fungsi" class="btn btn-sm btn-primary" style="font-weight: bold"><i class="far fa-plus-circle"></i></button>
                    </th>
                  </tr>
                </thead>
                <tbody>
                  <?php
                  if(!empty($arrFungsi)) {
                    foreach($arrFungsi as $f) {
                      ?>
                      <tr>
                        <td>
                          <textarea class="form-control" name="FungsiUraian[]" placeholder="URAIAN FUNGSI"><?=$f?></textarea>
                        </td>
                        <td>
                          <button type="button" class="btn btn-sm btn-danger btn-del-fungsi" style="font-weight: bold"><i class="fa fa-minus"></i></button>
                        </td>
                      </tr>
                      <?php
                    }
                  } else {
                    ?>
                    <tr class="empty">
                      <td colspan="6">
                        <p class="font-italic mb-0 text-center">BELUM ADA DATA</p>
                      </td>
                    </tr>
                    <?php
                  }
                  ?>
                </tbody>
              </table>
            </div>

            <div class="form-group">
              <label>INDIKATOR KINERJA UTAMA</label>
              <table id="tbl-iku" class="table table-bordered">
                <thead class="bg-default">
                  <tr>
                    <th>INDIKATOR</th>
                    <th>SUMBER DATA</th>
                    <th>FORMULASI</th>
                    <th style="width: 120px; white-space: nowrap">SATUAN</th>
                    <th style="width: 120px; white-space: nowrap">TARGET</th>
                    <th class="text-center" style="width: 10px; white-space: nowrap">
                      <button type="button" id="btn-add-iku" class="btn btn-sm btn-primary" style="font-weight: bold"><i class="far fa-plus-circle"></i></button>
                    </th>
                  </tr>
                </thead>
                <tbody>
                  <?php
                  if(!empty($arrIKU)) {
                    foreach($arrIKU as $iku) {
                      ?>
                      <tr>
                        <td>
                          <textarea class="form-control" name="IKUUraian[]" placeholder="URAIAN INDIKATOR"><?=$iku->Uraian?></textarea>
                        </td>
                        <td>
                          <textarea class="form-control" name="IKUSumberData[]" placeholder="SUMBER DATA"><?=$iku->SumberData?></textarea>
                        </td>
                        <td>
                          <textarea class="form-control" name="IKUFormulasi[]" placeholder="FORMULASI"><?=$iku->Formulasi?></textarea>
                        </td>
                        <td>
                          <select class="form-control" name="IKUSatuan[]"><?=GetCombobox("SELECT * FROM ".TBL_SAKIP_MSATUAN." ORDER BY ".COL_NM_SATUAN, COL_NM_SATUAN, COL_NM_SATUAN, $iku->Satuan)?></select>
                        </td>
                        <td>
                          <input type="text" class="form-control" name="IKUTarget[]" value="<?=$iku->Target?>" />
                        </td>
                        <td>
                          <button type="button" class="btn btn-sm btn-danger btn-del-iku" style="font-weight: bold"><i class="fa fa-minus"></i></button>
                        </td>
                      </tr>
                      <?php
                    }
                  } else {
                    ?>
                    <tr class="empty">
                      <td colspan="6">
                        <p class="font-italic mb-0 text-center">BELUM ADA DATA</p>
                      </td>
                    </tr>
                    <?php
                  }
                  ?>
                </tbody>
              </table>
            </div>
          </form>
        </div>
        <div class="modal-footer d-block">
          <div class="row">
            <div class="col-lg-12 text-center">
              <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="far fa-times-circle"></i>&nbsp;BATAL</button>
              <button type="submit" class="btn btn-primary btn-submit"><i class="far fa-plus-circle"></i>&nbsp;SUBMIT</button>
            </div>
          </div>
        </div>
    </div>
  </div>
</div>
<div class="modal fade" id="modalFormDuplicate" tabindex="-1" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">Duplikasi DPA</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true"><i class="fas fa-close"></i></span>
          </button>
        </div>
        <div class="modal-body">
        </div>
        <div class="modal-footer d-block">
          <div class="row">
            <div class="col-lg-12 text-center">
              <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="far fa-times-circle"></i>&nbsp;BATAL</button>
              <button type="submit" class="btn btn-primary btn-submit"><i class="far fa-plus-circle"></i>&nbsp;SUBMIT</button>
            </div>
          </div>
        </div>
    </div>
  </div>
</div>
<div class="modal fade" id="modalFormUpload" tabindex="-1" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">Struktur Organisasi</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true"><i class="fas fa-close"></i></span>
          </button>
        </div>
        <div class="modal-body">
          <form method="post" action="<?=site_url('sakipv2/skpd/ajax-upload-struktur/'.$rrenstra[COL_RENSTRAID])?>" enctype="multipart/form-data">
            <div class="form-group">
              <label>UNGGAH</label>
              <div class="input-group mb-2">
                <div class="input-group-prepend">
                  <span class="input-group-text"><i class="fad fa-paperclip"></i></span>
                </div>
                <div class="custom-file">
                  <input type="file" class="custom-file-input" name="file" accept="image/*">
                  <label class="custom-file-label" for="file">PILIH FILE</label>
                </div>
              </div>
              <p class="text-sm text-muted mb-0 font-italic">
                <strong>CATATAN:</strong><br>
                - Besar file / dokumen maksimum <strong>5 MB</strong><br>
                - Jenis file / dokumen yang diperbolehkan hanya dalam format <strong>JPG / JPEG</strong>, <strong>PNG</strong>
              </p>
            </div>
            <div class="row">
              <div class="col-lg-12 text-center">
                <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="far fa-times-circle"></i>&nbsp;BATAL</button>
                <button type="submit" class="btn btn-primary btn-submit"><i class="far fa-plus-circle"></i>&nbsp;SUBMIT</button>
              </div>
            </div>
          </form>
        </div>
    </div>
  </div>
</div>
<div class="modal fade" id="modalImgStruktur" tabindex="-1" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">Struktur Organisasi</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true"><i class="fas fa-close"></i></span>
          </button>
        </div>
        <div class="modal-body p-0">
          <img src="" style="width: 100%" />
        </div>
    </div>
  </div>
</div>
<script type="text/javascript">
var modalFormTujuan = $('#modalFormTujuan');
var modalFormDPA = $('#modalFormDPA');
var modalFormIKU = $('#modalFormIKU');
var modalFormDuplicate = $('#modalFormDuplicate');
var modalFormUpload = $('#modalFormUpload');

function addRowIKU() {
  var html =' ';
  html += '<tr>';
  html += '<td>';
  html += '<textarea class="form-control" name="IKUUraian[]" placeholder="URAIAN INDIKATOR"></textarea>';
  html += '</td>';
  html += '<td>';
  html += '<textarea class="form-control" name="IKUSumberData[]" placeholder="SUMBER DATA"></textarea>';
  html += '</td>';
  html += '<td>';
  html += '<textarea class="form-control" name="IKUFormulasi[]" placeholder="FORMULASI"></textarea>';
  html += '</td>';
  html += '<td>';
  html += '<select class="form-control" name="IKUSatuan[]"><?=GetCombobox("SELECT * FROM ".TBL_SAKIP_MSATUAN." ORDER BY ".COL_NM_SATUAN, COL_NM_SATUAN, COL_NM_SATUAN)?></select>';
  html += '</td>';
  html += '<td>';
  html += '<input type="text" class="form-control" name="IKUTarget[]" />';
  html += '</td>';
  html += '<td>';
  html += '<button type="button" class="btn btn-sm btn-danger btn-del-iku" style="font-weight: bold"><i class="fa fa-minus"></i></button>';
  html += '</td>';
  html += '</tr>';

  var empEl = $('tr.empty', $('tbody', $('#tbl-iku')));
  if(empEl) {
    $('tr.empty', $('tbody', $('#tbl-iku'))).remove();
  }

  $('tbody', $('#tbl-iku')).append(html);
  $('.btn-del-iku', $('tbody', $('#tbl-iku'))).unbind('click').click(function() {
    var row = $(this).closest('tr');
    row.remove();
  });
  $("select", $('tbody tr:last', $('#tbl-iku'))).not('.no-select2, .custom-select').select2({ width: 'resolve', theme: 'bootstrap4' });
}

function addRowFungsi() {
  var html =' ';
  html += '<tr>';
  html += '<td>';
  html += '<textarea class="form-control" name="FungsiUraian[]" placeholder="URAIAN FUNGSI"></textarea>';
  html += '</td>';
  html += '<td>';
  html += '<button type="button" class="btn btn-sm btn-danger btn-del-fungsi" style="font-weight: bold"><i class="fa fa-minus"></i></button>';
  html += '</td>';
  html += '</tr>';

  var empEl = $('tr.empty', $('tbody', $('#tbl-fungsi')));
  if(empEl) {
    $('tr.empty', $('tbody', $('#tbl-fungsi'))).remove();
  }

  $('tbody', $('#tbl-fungsi')).append(html);
  $('.btn-del-fungsi', $('tbody', $('#tbl-fungsi'))).unbind('click').click(function() {
    var row = $(this).closest('tr');
    row.remove();
  });
  $("select", $('tbody tr:last', $('#tbl-fungsi'))).not('.no-select2, .custom-select').select2({ width: 'resolve', theme: 'bootstrap4' });
}

$(document).ready(function() {
  $('select[name=filterRenstra]').change(function(){
    var url = $(this).val();
    location.href = url;
  });

  modalFormTujuan.on('hidden.bs.modal', function (e) {
    $('.modal-body', modalFormTujuan).empty();
  });
  modalFormDPA.on('hidden.bs.modal', function (e) {
    $('.modal-body', modalFormDPA).empty();
  });

  $('#btnTambahTupoksi, #btnTambahIKU, .btn-edit-tupoksi, .btn-edit-iku').click(function(){
    var url = $(this).attr('href');
    var val = $(this).data('val');
    var label = $(this).data('label');
    swal({
      closeOnClickOutside: true,
      buttons: ['BATAL','SUBMIT'],
      text: label,
      content: {
        element: "input",
        attributes: {
          placeholder: label,
          type: "text",
          value: (val||'')
        }
      },
    }).then(function(val){
      if(val) {
        $.ajax({
          url: url,
          method: "POST",
          dataType: "json",
          data: {
            Uraian: val
          }
        }).success(function(res) {
          if(res.error) {
            swal({
              title: 'ERROR',
              text: res.error,
              icon: 'error',
              buttons:false
            });
          } else {
            location.reload();
          }
        }).fail(function() {
          swal({
            title: 'SERVER ERROR',
            text: 'Terjadi kesalahan pada server. Silakan coba kembali.',
            icon: 'error',
            buttons:false
          });
        }).done(function() {

        });
      }
    });

    return false;
  });

  $('.btn-del-tupoksi, .btn-del-iku').click(function() {
    var url = $(this).attr('href');
    swal({
      title: "APAKAH ANDA YAKIN?",
      icon: "warning",
      buttons: [
        'BATAL',
        'YAKIN'
      ],
    }).then(function(isConfirm) {
      if (isConfirm) {
        $.ajax({
          url: url,
          method: "GET",
          dataType: "json"
        }).success(function(res) {
          if(res.error) {
            swal({
              title: 'ERROR',
              text: res.error,
              icon: 'error',
              buttons:false
            });
          } else {
            location.reload();
          }
        }).fail(function() {
          swal({
            title: 'SERVER ERROR',
            text: 'Terjadi kesalahan pada server. Silakan coba kembali.',
            icon: 'error',
            buttons:false
          });
        }).done(function() {

        });

      } else {

      }
    })
    return false;
  });

  modalFormTujuan.on('hidden.bs.modal', function (e) {
    $('.modal-body', modalFormTujuan).empty();
  });

  $('.btn-add-tujuan, .btn-edit-tujuan').click(function() {
    var url = $(this).attr('href');
    modalFormTujuan.modal('show');
    $('.modal-body', modalFormTujuan).html('<p class="font-italic mb-0 text-center"><i class="far fa-circle-notch fa-spin"></i>&nbsp;MEMUAT...</p>');
    $('.modal-body', modalFormTujuan).load(url, function(){
      $('button[type=submit]', modalFormTujuan).unbind('click').click(function(){
        $('form', modalFormTujuan).submit();
      });
    });
    return false;
  });

  $('.btn-change-tujuan').click(function() {
    var url = $(this).attr('href');
    var prompt = $(this).data('prompt');
    swal({
      title: "APAKAH ANDA YAKIN?",
      text: prompt,
      icon: "warning",
      buttons: [
        'BATAL',
        'YAKIN'
      ],
    }).then(function(isConfirm) {
      if (isConfirm) {
        $.ajax({
          url: url,
          method: "GET",
          dataType: "json"
        }).success(function(res) {
          if(res.error) {
            swal({
              title: 'ERROR',
              text: res.error,
              icon: 'error',
              buttons:false
            });
          } else {
            location.reload();
          }
        }).fail(function() {
          swal({
            title: 'SERVER ERROR',
            text: 'Terjadi kesalahan pada server. Silakan coba kembali.',
            icon: 'error',
            buttons:false
          });
        }).done(function() {

        });

      } else {

      }
    })
    return false;
  });

  $('.btn-add-dpa, .btn-edit-dpa').click(function() {
    var url = $(this).attr('href');
    modalFormDPA.modal('show');
    $('.modal-body', modalFormDPA).html('<p class="font-italic mb-0 text-center"><i class="far fa-circle-notch fa-spin"></i>&nbsp;MEMUAT...</p>');
    $('.modal-body', modalFormDPA).load(url, function(){
      $('button[type=submit]', modalFormDPA).unbind('click').click(function(){
        $('form', modalFormDPA).submit();
      });
    });
    return false;
  });

  $('.btn-duplicate-dpa').click(function() {
    var url = $(this).attr('href');
    modalFormDuplicate.modal('show');
    $('.modal-body', modalFormDuplicate).html('<p class="font-italic mb-0 text-center"><i class="far fa-circle-notch fa-spin"></i>&nbsp;MEMUAT...</p>');
    $('.modal-body', modalFormDuplicate).load(url, function(){
      $("select", modalFormDuplicate).not('.no-select2, .custom-select').select2({ width: 'resolve', theme: 'bootstrap4' });
      $('button[type=submit]', modalFormDuplicate).unbind('click').click(function(){
        $('form', modalFormDuplicate).submit();
      });
    });
    return false;
  });

  $('.btn-change-dpa').click(function() {
    var url = $(this).attr('href');
    var prompt = $(this).data('prompt');
    swal({
      title: "APAKAH ANDA YAKIN?",
      text: prompt,
      icon: "warning",
      buttons: [
        'BATAL',
        'YAKIN'
      ],
    }).then(function(isConfirm) {
      if (isConfirm) {
        $.ajax({
          url: url,
          method: "GET",
          dataType: "json"
        }).success(function(res) {
          if(res.error) {
            swal({
              title: 'ERROR',
              text: res.error,
              icon: 'error',
              buttons:false
            });
          } else {
            location.reload();
          }
        }).fail(function() {
          swal({
            title: 'SERVER ERROR',
            text: 'Terjadi kesalahan pada server. Silakan coba kembali.',
            icon: 'error',
            buttons:false
          });
        }).done(function() {

        });

      } else {

      }
    })
    return false;
  });

  $('#btnFormIKU').click(function() {
    var url = $(this).attr('href');
    modalFormIKU.modal('show');

    $('.btn-del-iku, .btn-del-fungsi', modalFormIKU).unbind('click').click(function() {
      var row = $(this).closest('tr');
      row.remove();
    });
    $("select", modalFormIKU).not('.no-select2, .custom-select').select2({ width: 'resolve', theme: 'bootstrap4' });
    $('button[type=submit]', modalFormIKU).unbind('click').click(function(){
      $('form', modalFormIKU).submit();
    });
  });

  $('#btn-add-iku', modalFormIKU).click(function() {
    addRowIKU();
  });
  $('#btn-add-fungsi', modalFormIKU).click(function() {
    addRowFungsi();
  });

  $('#form-iku').validate({
    submitHandler: function(form) {
      var modal = $(form).closest('modal');
      if(modal) {
        var btnSubmit = $('button[type=submit]', modal);
        var txtSubmit = btnSubmit.innerHTML;
        btnSubmit.html('<i class="fad fa-circle-notch fa-spin"></i>');
        btnSubmit.attr('disabled', true);
      }

      $(form).ajaxSubmit({
        dataType: 'json',
        type : 'post',
        success: function(res) {
          if(res.error != 0) {
            toastr.error(res.error);
          } else {
            toastr.success(res.success);
            setTimeout(function(){
              location.reload();
            }, 1000);
          }
        },
        error: function() {
          toastr.error('SERVER ERROR');
        },
        complete: function() {
          btnSubmit.html(txtSubmit);
          btnSubmit.attr('disabled', false);
        }
      });
      return false;
    }
  });

  $('form', modalFormUpload).validate({
    ignore: "input[type='file']",
    submitHandler: function(form) {
      var btnSubmit = $('button[type=submit]', modalFormUpload);
      var txtSubmit = btnSubmit.innerHTML;
      btnSubmit.html('<i class="fad fa-circle-notch fa-spin"></i>');
      btnSubmit.attr('disabled', true);
      $(form).ajaxSubmit({
        dataType: 'json',
        type : 'post',
        success: function(res) {
          if(res.error != 0) {
            toastr.error(res.error);
          } else {
            toastr.success(res.success);
            setTimeout(function(){
              location.reload();
            }, 1000);
          }
        },
        error: function() {
          toastr.error('SERVER ERROR');
        },
        complete: function() {
          btnSubmit.html(txtSubmit);
          btnSubmit.attr('disabled', false);
        }
      });
    }
  });

  $('.btn-view-struktur').click(function() {
    var href = $(this).attr('href');
    $('.modal-body>img', $('#modalImgStruktur')).attr('src', href);
    $('#modalImgStruktur').modal('show');
    return false;
  });
});
</script>
