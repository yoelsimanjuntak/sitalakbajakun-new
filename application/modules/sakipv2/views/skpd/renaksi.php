<?php
$ruser = GetLoggedUser();

$rpemda = $this->db
->where(COL_PMDISAKTIF, 1)
->get(TBL_SAKIPV2_PEMDA)
->row_array();

$rOptSkpd = $this->db
->where(COL_SKPDISAKTIF, 1)
->order_by(COL_SKPDURUSAN, 'asc')
->order_by(COL_SKPDBIDANG, 'asc')
->order_by(COL_SKPDUNIT, 'asc')
->order_by(COL_SKPDSUBUNIT, 'asc')
->get(TBL_SAKIPV2_SKPD)
->result_array();

$getSkpd = '';
$getRenstra = '';
$getDPA = '';

if(!empty($_GET['idSKPD'])) $getSkpd = $_GET['idSKPD'];
else if(!empty($rOptSkpd)) $getSkpd = $rOptSkpd[0][COL_SKPDID];

if($ruser[COL_ROLEID]!=ROLEADMIN && $ruser[COL_ROLEID]!=ROLEGUEST) {
  $getSkpd=$ruser[COL_SKPDID];
}

$rOptRenstra = array();
if(!empty($getSkpd)) {
  $rOptRenstra = $this->db
  ->where(COL_IDPEMDA, $rpemda[COL_PMDID])
  ->where(COL_IDSKPD, $getSkpd)
  //->where(COL_RENSTRAISAKTIF, 1)
  ->order_by(COL_RENSTRAISAKTIF, 'desc')
  ->order_by(COL_RENSTRATAHUN, 'desc')
  ->order_by(COL_RENSTRAID, 'desc')
  ->get(TBL_SAKIPV2_SKPD_RENSTRA)
  ->result_array();
}

if(!empty($_GET['idRenstra'])) $getRenstra = $_GET['idRenstra'];
else if(!empty($rOptRenstra)) $getRenstra = $rOptRenstra[0][COL_RENSTRAID];

$rOptDpa = array();
if(!empty($getRenstra)) {
  $rOptDpa = $this->db
  ->where(COL_IDRENSTRA, $getRenstra)
  ->where(COL_DPAISAKTIF, 1)
  ->order_by(COL_DPAISAKTIF, 'desc')
  ->order_by(COL_DPATAHUN,'desc')
  ->get(TBL_SAKIPV2_SKPD_RENSTRA_DPA)
  ->result_array();
}

$getDPA = null;
if(!empty($_GET['idDPA'])) {
  $getDPA = $_GET['idDPA'];
} else if(!empty($rOptDpa) && $rOptDpa[0][COL_DPAISAKTIF]==1) {
  $getDPA = $rOptDpa[0][COL_DPAID];
}

$rSasaran = array();
if(!empty($getSkpd) && !empty($getRenstra)) {
  $rSasaran = $this->db
  ->join(TBL_SAKIPV2_SKPD_RENSTRA_TUJUAN,TBL_SAKIPV2_SKPD_RENSTRA_TUJUAN.'.'.COL_TUJUANID." = ".TBL_SAKIPV2_SKPD_RENSTRA_SASARAN.".".COL_IDTUJUAN,"left")
  ->join(TBL_SAKIPV2_SKPD_RENSTRA,TBL_SAKIPV2_SKPD_RENSTRA.'.'.COL_RENSTRAID." = ".TBL_SAKIPV2_SKPD_RENSTRA_TUJUAN.".".COL_IDRENSTRA,"left")
  ->where(COL_IDSKPD, $getSkpd)
  ->where(COL_IDRENSTRA, $getRenstra)
  ->order_by(TBL_SAKIPV2_SKPD_RENSTRA_TUJUAN.'.'.COL_TUJUANNO)
  ->order_by(TBL_SAKIPV2_SKPD_RENSTRA_SASARAN.'.'.COL_SASARANNO)
  ->get(TBL_SAKIPV2_SKPD_RENSTRA_SASARAN)
  ->result_array();
}

$getTW = null;
if(!empty($_GET['idTW'])) {
  $getTW = $_GET['idTW'];
} else {
  $getTW = 1;
}
?>
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h3 class="m-0 font-weight-light"><?=$title?></h3>
      </div>
      <?php
      if(!empty($navs)) {
        ?>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <?php
            foreach($navs as $n) {
              if(!empty($n['link'])) {
                ?>
                <li class="breadcrumb-item"><a href="<?=$n['link']?>"><?=$n['text']?></a></li>
                <?php
              } else {
                ?>
                <li class="breadcrumb-item active"><?=$n['text']?></li>
                <?php
              }
            }
            ?>
          </ol>
        </div>
        <?php
      }
      ?>
    </div>
  </div>
</div>
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-12">
        <div class="card card-outline card-secondary" id="card-monev">
          <div class="overlay" style="display: none">
            <i class="fas fa-2x fa-sync-alt fa-spin"></i>
          </div>
          <div class="card-header p-0 border-0">
            <table class="table table-bordered mb-0">
              <thead>
                <tr>
                  <td>
                    <div class="form-group row mb-0">
                      <label class="control-label col-lg-2">PERIODE PEMERINTAHAN :</label>
                      <div class="col-lg-8">
                        <p class="font-italic font-weight-bold mb-0" style="line-height: 2; text-decoration: underline">
                          <?=$rpemda[COL_PMDTAHUNMULAI].' - '.$rpemda[COL_PMDTAHUNAKHIR].' '.strtoupper($rpemda[COL_PMDPEJABAT]).' & '.strtoupper($rpemda[COL_PMDPEJABATWAKIL])?>
                        </p>
                      </div>
                    </div>
                    <div class="form-group row">
                      <label class="control-label col-lg-2">SKPD :</label>
                      <div class="col-lg-8">
                        <?php
                        if($ruser[COL_ROLEID] == ROLEADMIN || $ruser[COL_ROLEID] == ROLEGUEST) {
                          ?>
                          <select class="form-control" name="filterSkpd">
                            <?php
                            foreach($rOptSkpd as $opt) {
                              $isSelected = '';
                              if(!empty($getSkpd) && $opt[COL_SKPDID]==$getSkpd) {
                                $isSelected='selected';
                              }
                              ?>
                              <option value="<?=site_url('sakipv2/skpd/renaksi').'?idSKPD='.$opt[COL_SKPDID]?>" <?=$isSelected?>>
                                <?=$opt[COL_SKPDURUSAN].'.'.$opt[COL_SKPDBIDANG].'.'.$opt[COL_SKPDUNIT].'.'.$opt[COL_SKPDSUBUNIT].' - '.strtoupper($opt[COL_SKPDNAMA])?>
                              </option>
                              <?php
                            }
                            ?>
                          </select>
                          <?php
                        } else {
                          $ropd = $this->db
                          ->where(COL_SKPDID, $ruser[COL_SKPDID])
                          ->get(TBL_SAKIPV2_SKPD)
                          ->row_array();
                          ?>
                          <p class="font-italic font-weight-bold mb-0" style="line-height: 2; text-decoration: underline">
                            <?=strtoupper($ropd[COL_SKPDNAMA])?>
                          </p>
                          <?php
                        }
                        ?>
                      </div>
                    </div>
                    <div class="form-group row">
                      <label class="control-label col-lg-2">RENSTRA SKPD :</label>
                      <div class="col-lg-8">
                        <select class="form-control" name="filterRenstra">
                          <?php
                          foreach($rOptRenstra as $opt) {
                            $isSelected = '';
                            if(!empty($getRenstra) && $opt[COL_RENSTRAID]==$getRenstra) {
                              $isSelected='selected';
                            }
                            ?>
                            <option value="<?=site_url('sakipv2/skpd/renaksi').'?idSKPD='.$getSkpd.'&idRenstra='.$opt[COL_RENSTRAID]?>" <?=$isSelected?>>
                              <?=$opt[COL_RENSTRATAHUN].' - '.strtoupper($opt[COL_RENSTRAURAIAN])?>
                            </option>
                            <?php
                          }
                          ?>
                        </select>
                      </div>
                    </div>
                    <div class="form-group row">
                      <label class="control-label col-lg-2">DPA SKPD :</label>
                      <div class="col-lg-8">
                        <select class="form-control" name="filterDPA">
                          <?php
                          foreach($rOptDpa as $opt) {
                            ?>
                            <option value="<?=site_url('sakipv2/skpd/renaksi').'?idSKPD='.$getSkpd.'&idRenstra='.$getRenstra.'&idDPA='.$opt[COL_DPAID]?>" <?=$opt[COL_DPAID]==$getDPA?'selected':''?>>
                              <?=$opt[COL_DPATAHUN].' - '.strtoupper($opt[COL_DPAURAIAN])?>
                            </option>
                            <?php
                          }
                          ?>
                        </select>
                      </div>
                    </div>
                    <div class="form-group row">
                      <label class="control-label col-lg-2">PERIODE MONEV :</label>
                      <div class="col-lg-4">
                        <select class="form-control" name="filterTW">
                          <option value="<?=site_url('sakipv2/skpd/renaksi').'?idSKPD='.$getSkpd.'&idRenstra='.$getRenstra.'&idDPA='.$getDPA.'&idTW=1'?>" <?=$getTW==1?'selected':''?>>Triwulan I</option>
                          <option value="<?=site_url('sakipv2/skpd/renaksi').'?idSKPD='.$getSkpd.'&idRenstra='.$getRenstra.'&idDPA='.$getDPA.'&idTW=2'?>" <?=$getTW==2?'selected':''?>>Triwulan II</option>
                          <option value="<?=site_url('sakipv2/skpd/renaksi').'?idSKPD='.$getSkpd.'&idRenstra='.$getRenstra.'&idDPA='.$getDPA.'&idTW=3'?>" <?=$getTW==3?'selected':''?>>Triwulan III</option>
                          <option value="<?=site_url('sakipv2/skpd/renaksi').'?idSKPD='.$getSkpd.'&idRenstra='.$getRenstra.'&idDPA='.$getDPA.'&idTW=4'?>" <?=$getTW==4?'selected':''?>>Triwulan IV</option>
                          <option value="<?=site_url('sakipv2/skpd/renaksi').'?idSKPD='.$getSkpd.'&idRenstra='.$getRenstra.'&idDPA='.$getDPA.'&idTW=99'?>" <?=$getTW==99?'selected':''?>>Final</option>
                        </select>
                      </div>
                    </div>
                  </td>
                </tr>
              </thead>
            </table>
          </div>
          <div class="card-body p-0">

          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<div class="modal fade" id="modal-renaksi" tabindex="-1" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">Rencana Aksi</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true"><i class="fas fa-close"></i></span>
          </button>
        </div>
        <div class="modal-body">
          <form id="form-renaksi" action="" method="post">

          </form>
        </div>
        <div class="modal-footer d-block">
          <div class="row">
            <div class="col-lg-12 text-center">
              <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="far fa-times-circle"></i>&nbsp;BATAL</button>
              <button type="submit" class="btn btn-primary btn-submit"><i class="far fa-plus-circle"></i>&nbsp;SUBMIT</button>
            </div>
          </div>
        </div>
    </div>
  </div>
</div>
<script type="text/javascript">
var mainURL = '<?=site_url('sakipv2/skpd/renaksi-data/'.$getSkpd.'/'.$getRenstra.'/'.$getDPA.'/'.$getTW)?>';
var mainContainer = $('#card-monev');
var modalRenaksi = $('#modal-renaksi');

function refreshCard() {
  $('.overlay', mainContainer).show();
  $('.card-body', mainContainer).load(mainURL, function(){
    $('.overlay', mainContainer).hide();
    $('.btn-changeval').click(function(){
      var url = $(this).attr('href');
      var val = $(this).data('value');
      if(val==null) {
        val = '';
      }
      swal({
        closeOnClickOutside: true,
        buttons: ['BATAL','SUBMIT'],
        text: 'UBAH NILAI',
        content: {
          element: "input",
          attributes: {
            placeholder: 'NILAI',
            type: "text",
            value: val
          }
        },
      }).then(function(val){
        if(val) {
          $.ajax({
            url: url,
            method: "POST",
            dataType: "json",
            data: {
              value: val
            }
          }).success(function(res) {
            if(res.error) {
              swal({
                title: 'ERROR',
                text: res.error,
                icon: 'error',
                buttons:false
              });
            } else {
              refreshCard();
            }
          }).fail(function() {
            swal({
              title: 'SERVER ERROR',
              text: 'Terjadi kesalahan pada server. Silakan coba kembali.',
              icon: 'error',
              buttons:false
            });
          }).done(function() {

          });
        }
      });

      return false;
    });
  });
}

$(document).ready(function(){
  $('select[name=filterRenstra],select[name=filterSkpd],select[name=filterDPA],select[name=filterTW]').change(function(){
    var url = $(this).val();
    location.href = url;
  });

  refreshCard();
  $('#form-renaksi').validate({
    submitHandler: function(form) {
      var modal = $(form).closest('.modal');
      if(modal) {
        var btnSubmit = $('button[type=submit]', modal);
        var txtSubmit = btnSubmit[0].innerHTML;
        btnSubmit.html('<i class="fad fa-circle-notch fa-spin"></i>');
        btnSubmit.attr('disabled', true);
      }

      $(form).ajaxSubmit({
        dataType: 'json',
        type : 'post',
        success: function(res) {
          if(res.error != 0) {
            toastr.error(res.error);
          } else {
            toastr.success(res.success);
            modal.modal('hide');
            refreshCard();
          }
        },
        error: function() {
          toastr.error('SERVER ERROR');
        },
        complete: function() {
          btnSubmit.html(txtSubmit);
          btnSubmit.attr('disabled', false);
        }
      });

      return false;
    }
  });
  $('button[type=submit]', modalRenaksi).click(function(){
    $('#form-renaksi').submit();
  });
});
</script>
