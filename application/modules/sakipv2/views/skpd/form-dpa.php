<form id="form-dpa" action="<?=current_url()?>">
  <div class="form-group row">
    <label class="control-label col-lg-3">TAHUN</label>
    <div class="col-lg-2">
      <input type="number" class="form-control" name="<?=COL_DPATAHUN?>" placeholder="TAHUN" value="<?=!empty($data)?$data[COL_DPATAHUN]:''?>" required />
    </div>
  </div>
  <div class="form-group row">
    <label class="control-label col-lg-3">KETERANGAN</label>
    <div class="col-lg-8">
      <textarea class="form-control" name="<?=COL_DPAURAIAN?>" placeholder="CONTOH: DPA TH. 2020 (INDUK)" required><?=!empty($data)?$data[COL_DPAURAIAN]:''?></textarea>
    </div>
  </div>

</form>
<script type="text/javascript">
$(document).ready(function(){
  $('#form-dpa').validate({
    submitHandler: function(form) {
      var modal = $(form).closest('modal');
      if(modal) {
        var btnSubmit = $('button[type=submit]', modal);
        var txtSubmit = btnSubmit.innerHTML;
        btnSubmit.html('<i class="fad fa-circle-notch fa-spin"></i>');
        btnSubmit.attr('disabled', true);
      }

      $(form).ajaxSubmit({
        dataType: 'json',
        type : 'post',
        success: function(res) {
          if(res.error != 0) {
            toastr.error(res.error);
          } else {
            toastr.success(res.success);
            setTimeout(function(){
              location.reload();
            }, 1000);
          }
        },
        error: function() {
          toastr.error('SERVER ERROR');
        },
        complete: function() {
          btnSubmit.html(txtSubmit);
          btnSubmit.attr('disabled', false);
        }
      });

      return false;
    }
  });
});
</script>
