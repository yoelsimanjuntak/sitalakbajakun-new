<?php
if(empty($idSKPD) || empty($idRenstra) || empty($idDPA)) {
  ?>
  <p class="text-center font-italic font-weight-bold text-danger mb-0">
    DATA TIDAK DITEMUKAN
  </p>
  <?php
  exit();
}

$rpemda = $this->db
->where(COL_PMDISAKTIF, 1)
->get(TBL_SAKIPV2_PEMDA)
->row_array();
if(empty($rpemda)) {
  ?>
  <p class="text-center font-italic font-weight-bold text-danger mb-0">
    PERIODE PEMERINTAHAN AKTIF TIDAK DITEMUKAN
  </p>
  <?php
  exit();
}

$rskpd = $this->db
->where(COL_SKPDID, $idSKPD)
->get(TBL_SAKIPV2_SKPD)
->row_array();
if(empty($rskpd)) {
  ?>
  <p class="text-center font-italic font-weight-bold text-danger mb-0">
    SKPD TIDAK DITEMUKAN
  </p>
  <?php
  exit();
}

$rrenstra = $this->db
->where(COL_RENSTRAID, $idRenstra)
->get(TBL_SAKIPV2_SKPD_RENSTRA)
->row_array();
if(empty($rrenstra)) {
  ?>
  <p class="text-center font-italic font-weight-bold text-danger mb-0">
    RENSTRA TIDAK DITEMUKAN
  </p>
  <?php
  exit();
}

$rdpa = $this->db
->where(COL_DPAID, $idDPA)
->get(TBL_SAKIPV2_SKPD_RENSTRA_DPA)
->row_array();
if(empty($rskpd)) {
  ?>
  <p class="text-center font-italic font-weight-bold text-danger mb-0">
    DPA TIDAK DITEMUKAN
  </p>
  <?php
  exit();
}

$rsasaran = $this->db
->join(TBL_SAKIPV2_SKPD_RENSTRA_TUJUAN,TBL_SAKIPV2_SKPD_RENSTRA_TUJUAN.'.'.COL_TUJUANID." = ".TBL_SAKIPV2_SKPD_RENSTRA_SASARAN.".".COL_IDTUJUAN,"left")
->join(TBL_SAKIPV2_SKPD_RENSTRA,TBL_SAKIPV2_SKPD_RENSTRA.'.'.COL_RENSTRAID." = ".TBL_SAKIPV2_SKPD_RENSTRA_TUJUAN.".".COL_IDRENSTRA,"left")
->where(COL_IDPEMDA, $rpemda[COL_PMDID])
->where(COL_IDSKPD, $idSKPD)
->where(COL_IDRENSTRA, $idRenstra)
->order_by(COL_TUJUANNO)
->order_by(COL_SASARANNO)
->get(TBL_SAKIPV2_SKPD_RENSTRA_SASARAN)
->result_array();

$rsubkeg = $this->db
->join(TBL_SAKIPV2_BID_KEGIATAN,TBL_SAKIPV2_BID_KEGIATAN.'.'.COL_KEGIATANID." = ".TBL_SAKIPV2_SUBBID_SUBKEGIATAN.".".COL_IDKEGIATAN,"left")
->join(TBL_SAKIPV2_BID_PROGRAM,TBL_SAKIPV2_BID_PROGRAM.'.'.COL_PROGRAMID." = ".TBL_SAKIPV2_BID_KEGIATAN.".".COL_IDPROGRAM,"left")
->join(TBL_SAKIPV2_SKPD_RENSTRA_DPA,TBL_SAKIPV2_SKPD_RENSTRA_DPA.'.'.COL_DPAID." = ".TBL_SAKIPV2_BID_PROGRAM.".".COL_IDDPA,"left")
->join(TBL_SAKIPV2_SKPD_RENSTRA,TBL_SAKIPV2_SKPD_RENSTRA.'.'.COL_RENSTRAID." = ".TBL_SAKIPV2_SKPD_RENSTRA_DPA.".".COL_IDRENSTRA,"left")
->where(COL_IDSKPD, $idSKPD)
->where(COL_IDDPA, $idDPA)
->order_by(COL_IDBID)
->order_by(COL_SUBKEGKODE)
->get(TBL_SAKIPV2_SUBBID_SUBKEGIATAN)
->result_array();
?>

<style>
body {
  font-family: 'Source Sans Pro','Helvetica Neue',Helvetica,Arial,sans-serif;
}
th {
  background: #ffc107 !important;
}
th, td {
  font-size: 10pt !important;
  padding: 5px 10px !important;
  max-width: 200px !important;
}
table {
  width: 100%;
  border-collapse: collapse;
}
table, th, td {
  border: 1px solid black !important;
}
.d-none {
  display: none !important;
}
</style>
<h5 style="text-align: center; margin-top: 2rem;">
  <?=$title?><br />
  <?=strtoupper($rskpd[COL_SKPDNAMA])?><br />
  <!--<?=!empty($idTW)&&$idTW!=99?'TRIWULAN '.$idTW.' ':''?>-->TAHUN <?=$rdpa[COL_DPATAHUN]?>
</h5>
<div class="row mt-4s">
  <div class="col-lg-12">
    <div class="table-responsive">
      <table class="table table-bordered mt-0" autosize="1" style="width: 100% !important">
        <thead>
          <tr>
            <th style="vertical-align: middle" rowspan="2">RENCANA AKSI</th>
            <th style="vertical-align: middle" rowspan="2">INDIKATOR KINERJA</th>
            <th style="width: 10px; white-space: nowrap; vertical-align: middle" rowspan="2">SATUAN</th>
            <th style="width: 10px; white-space: nowrap; vertical-align: middle; text-align: center" colspan="5">TARGET</th>
            <!--<th style="width: 10px; white-space: nowrap; vertical-align: middle; text-align: center">CAPAIAN</th>-->
            <!--<th>CATATAN</th>-->
          </tr>
          <tr>
            <th>TW. I</th>
            <th>TW. II</th>
            <th>TW. III</th>
            <th>TW. IV</th>
            <th><?=$rdpa[COL_DPATAHUN]?></th>
          </tr>
        </thead>
        <tbody>
          <?php
          if(!empty($rsasaran)) {
            foreach($rsasaran as $s) {
              $rdet = $this->db
              ->where(COL_IDSASARAN, $s[COL_SASARANID])
              ->get(TBL_SAKIPV2_SKPD_RENSTRA_SASARANDET)
              ->result_array();

              $rprogram = $this->db
              ->join(TBL_SAKIPV2_SKPD_RENSTRA_DPA,TBL_SAKIPV2_SKPD_RENSTRA_DPA.'.'.COL_DPAID." = ".TBL_SAKIPV2_BID_PROGRAM.".".COL_IDDPA,"left")
              ->join(TBL_SAKIPV2_SKPD_RENSTRA,TBL_SAKIPV2_SKPD_RENSTRA.'.'.COL_RENSTRAID." = ".TBL_SAKIPV2_SKPD_RENSTRA_DPA.".".COL_IDRENSTRA,"left")
              ->where(COL_IDSASARANSKPD, $s[COL_SASARANID])
              ->where(COL_IDRENSTRA, $idRenstra)
              ->where(COL_IDDPA, $idDPA)
              ->order_by(COL_IDBID)
              ->order_by(COL_PROGRAMKODE)
              ->get(TBL_SAKIPV2_BID_PROGRAM)
              ->result_array();

              $rprogramInd = $this->db
              ->join(TBL_SAKIPV2_BID_PROGRAM,TBL_SAKIPV2_BID_PROGRAM.'.'.COL_PROGRAMID." = ".TBL_SAKIPV2_BID_PROGSASARAN.".".COL_IDPROGRAM,"left")
              ->join(TBL_SAKIPV2_SKPD_RENSTRA_DPA,TBL_SAKIPV2_SKPD_RENSTRA_DPA.'.'.COL_DPAID." = ".TBL_SAKIPV2_BID_PROGRAM.".".COL_IDDPA,"left")
              ->join(TBL_SAKIPV2_SKPD_RENSTRA,TBL_SAKIPV2_SKPD_RENSTRA.'.'.COL_RENSTRAID." = ".TBL_SAKIPV2_SKPD_RENSTRA_DPA.".".COL_IDRENSTRA,"left")
              ->where(COL_IDSASARANSKPD, $s[COL_SASARANID])
              ->where(COL_IDRENSTRA, $idRenstra)
              ->where(COL_IDDPA, $idDPA)
              ->count_all_results(TBL_SAKIPV2_BID_PROGSASARAN);

              $rkegiatan = $this->db
              ->join(TBL_SAKIPV2_BID_PROGRAM,TBL_SAKIPV2_BID_PROGRAM.'.'.COL_PROGRAMID." = ".TBL_SAKIPV2_BID_KEGIATAN.".".COL_IDPROGRAM,"left")
              ->join(TBL_SAKIPV2_SKPD_RENSTRA_DPA,TBL_SAKIPV2_SKPD_RENSTRA_DPA.'.'.COL_DPAID." = ".TBL_SAKIPV2_BID_PROGRAM.".".COL_IDDPA,"left")
              ->join(TBL_SAKIPV2_SKPD_RENSTRA,TBL_SAKIPV2_SKPD_RENSTRA.'.'.COL_RENSTRAID." = ".TBL_SAKIPV2_SKPD_RENSTRA_DPA.".".COL_IDRENSTRA,"left")
              ->where(COL_IDSASARANSKPD, $s[COL_SASARANID])
              ->where(COL_IDRENSTRA, $idRenstra)
              ->where(COL_IDDPA, $idDPA)
              ->count_all_results(TBL_SAKIPV2_BID_KEGIATAN);

              $rkegiatanInd = $this->db
              ->join(TBL_SAKIPV2_BID_KEGIATAN,TBL_SAKIPV2_BID_KEGIATAN.'.'.COL_KEGIATANID." = ".TBL_SAKIPV2_BID_KEGSASARAN.".".COL_IDKEGIATAN,"left")
              ->join(TBL_SAKIPV2_BID_PROGRAM,TBL_SAKIPV2_BID_PROGRAM.'.'.COL_PROGRAMID." = ".TBL_SAKIPV2_BID_KEGIATAN.".".COL_IDPROGRAM,"left")
              ->join(TBL_SAKIPV2_SKPD_RENSTRA_DPA,TBL_SAKIPV2_SKPD_RENSTRA_DPA.'.'.COL_DPAID." = ".TBL_SAKIPV2_BID_PROGRAM.".".COL_IDDPA,"left")
              ->join(TBL_SAKIPV2_SKPD_RENSTRA,TBL_SAKIPV2_SKPD_RENSTRA.'.'.COL_RENSTRAID." = ".TBL_SAKIPV2_SKPD_RENSTRA_DPA.".".COL_IDRENSTRA,"left")
              ->where(COL_IDSASARANSKPD, $s[COL_SASARANID])
              ->where(COL_IDRENSTRA, $idRenstra)
              ->where(COL_IDDPA, $idDPA)
              ->count_all_results(TBL_SAKIPV2_BID_KEGSASARAN);
              $noInd = 1;

              $rowspan = count($rdet);
              ?>
              <tr style="background: #dedede; font-style: italic">
                <td <?=$rowspan>1?'rowspan="'.$rowspan.'"':''?> style="font-style: italic; font-weight: bold; vertical-align: middle"><?=strtoupper($s[COL_SASARANURAIAN])?></td>
                <?php
                if(!empty($rdet)) {
                  $rval = $this->db
                  ->where(COL_IDSASARANINDIKATOR, $rdet[0][COL_SSRINDIKATORID])
                  ->where(COL_MONEVTAHUN, $rdpa[COL_DPATAHUN])
                  ->order_by(COL_UNIQ, 'desc')
                  ->get(TBL_SAKIPV2_SKPD_RENSTRA_SASARANMONEV)
                  ->row_array();

                  $txtTargetTW1 = !empty($rval[COL_MONEVTARGETTW1])||$rval[COL_MONEVTARGETTW1]!=null?$rval[COL_MONEVTARGETTW1]:'-';
                  $txtTargetTW2 = !empty($rval[COL_MONEVTARGETTW2])||$rval[COL_MONEVTARGETTW2]!=null?$rval[COL_MONEVTARGETTW2]:'-';
                  $txtTargetTW3 = !empty($rval[COL_MONEVTARGETTW3])||$rval[COL_MONEVTARGETTW3]!=null?$rval[COL_MONEVTARGETTW3]:'-';
                  $txtTargetTW4 = !empty($rval[COL_MONEVTARGETTW4])||$rval[COL_MONEVTARGETTW4]!=null?$rval[COL_MONEVTARGETTW4]:'-';
                  $txtRealTW1 = !empty($rval[COL_MONEVREALISASITW1])||$rval[COL_MONEVREALISASITW1]!=null?$rval[COL_MONEVREALISASITW1]:'-';
                  $txtRealTW2 = !empty($rval[COL_MONEVREALISASITW2])||$rval[COL_MONEVREALISASITW2]!=null?$rval[COL_MONEVREALISASITW2]:'-';
                  $txtRealTW3 = !empty($rval[COL_MONEVREALISASITW3])||$rval[COL_MONEVREALISASITW3]!=null?$rval[COL_MONEVREALISASITW3]:'-';
                  $txtRealTW4 = !empty($rval[COL_MONEVREALISASITW4])||$rval[COL_MONEVREALISASITW4]!=null?$rval[COL_MONEVREALISASITW4]:'-';

                  $txtTarget = !empty($rval[COL_MONEVTARGET])||$rval[COL_MONEVTARGET]!=null?$rval[COL_MONEVTARGET]:'-';
                  $txtReal = !empty($rval[COL_MONEVREALISASI])||$rval[COL_MONEVREALISASI]!=null?$rval[COL_MONEVREALISASI]:'-';
                  ?>
                  <td style="vertical-align: top;"><?=strtoupper($rdet[0][COL_SSRINDIKATORURAIAN])?></td>
                  <td style="vertical-align: middle"><?=strtoupper($rdet[0][COL_SSRINDIKATORSATUAN])?></td>
                  <td style="text-align: center; vertical-align: middle; white-space: nowrap"><?=$txtTargetTW1?></td>
                  <td style="text-align: center; vertical-align: middle; white-space: nowrap"><?=$txtTargetTW2?></td>
                  <td style="text-align: center; vertical-align: middle; white-space: nowrap"><?=$txtTargetTW3?></td>
                  <td style="text-align: center; vertical-align: middle; white-space: nowrap"><?=$txtTargetTW4?></td>
                  <td style="text-align: center; vertical-align: middle; white-space: nowrap"><?=$txtTarget?></td>
                  <?php
                  /*if($idTW==1) {
                    ?>
                    <td style="text-align: center; vertical-align: middle; white-space: nowrap"><?=$txtTargetTW1?></td>
                    <td style="text-align: center; vertical-align: middle; white-space: nowrap"><?=$txtRealTW1?></td>
                    <?php
                  } else if($idTW==2) {
                    ?>
                    <td style="text-align: center; vertical-align: middle; white-space: nowrap"><?=$txtTargetTW2?></td>
                    <td style="text-align: center; vertical-align: middle; white-space: nowrap"><?=$txtRealTW2?></td>
                    <?php
                  } else if($idTW==3) {
                    ?>
                    <td style="text-align: center; vertical-align: middle; white-space: nowrap"><?=$txtTargetTW3?></td>
                    <td style="text-align: center; vertical-align: middle; white-space: nowrap"><?=$txtRealTW3?></td>
                    <?php
                  } else if($idTW==4) {
                    ?>
                    <td style="text-align: center; vertical-align: middle; white-space: nowrap"><?=$txtTargetTW4?></td>
                    <td style="text-align: center; vertical-align: middle; white-space: nowrap"><?=$txtRealTW4?></td>
                    <?php
                  } else if($idTW==99) {
                    ?>
                    <td style="text-align: center; vertical-align: middle; white-space: nowrap"><?=$txtTarget?></td>
                    <td style="text-align: center; vertical-align: middle; white-space: nowrap"><?=$txtReal?></td>
                    <?php
                  }*/
                  ?>
                  <?php
                } else {
                  ?>
                  <td colspan="8" style="text-align: center; vertical-align: middle; white-space: nowrap">(KOSONG)</td>
                  <?php
                }
                ?>
              </tr>
              <?php
              if(!empty($rdet)&&count($rdet)>1) {
                for($i=1; $i<count($rdet); $i++) {
                  $rval = $this->db
                  ->where(COL_IDSASARANINDIKATOR, $rdet[$i][COL_SSRINDIKATORID])
                  ->where(COL_MONEVTAHUN, $rdpa[COL_DPATAHUN])
                  ->order_by(COL_UNIQ, 'desc')
                  ->get(TBL_SAKIPV2_SKPD_RENSTRA_SASARANMONEV)
                  ->row_array();

                  $txtTargetTW1 = !empty($rval[COL_MONEVTARGETTW1])||$rval[COL_MONEVTARGETTW1]!=null?$rval[COL_MONEVTARGETTW1]:'-';
                  $txtTargetTW2 = !empty($rval[COL_MONEVTARGETTW2])||$rval[COL_MONEVTARGETTW2]!=null?$rval[COL_MONEVTARGETTW2]:'-';
                  $txtTargetTW3 = !empty($rval[COL_MONEVTARGETTW3])||$rval[COL_MONEVTARGETTW3]!=null?$rval[COL_MONEVTARGETTW3]:'-';
                  $txtTargetTW4 = !empty($rval[COL_MONEVTARGETTW4])||$rval[COL_MONEVTARGETTW4]!=null?$rval[COL_MONEVTARGETTW4]:'-';

                  $txtRealTW1 = !empty($rval[COL_MONEVREALISASITW1])||$rval[COL_MONEVREALISASITW1]!=null?$rval[COL_MONEVREALISASITW1]:'-';
                  $txtRealTW2 = !empty($rval[COL_MONEVREALISASITW2])||$rval[COL_MONEVREALISASITW2]!=null?$rval[COL_MONEVREALISASITW2]:'-';
                  $txtRealTW3 = !empty($rval[COL_MONEVREALISASITW3])||$rval[COL_MONEVREALISASITW3]!=null?$rval[COL_MONEVREALISASITW3]:'-';
                  $txtRealTW4 = !empty($rval[COL_MONEVREALISASITW4])||$rval[COL_MONEVREALISASITW4]!=null?$rval[COL_MONEVREALISASITW4]:'-';

                  $txtTarget = !empty($rval[COL_MONEVTARGET])||$rval[COL_MONEVTARGET]!=null?$rval[COL_MONEVTARGET]:'-';
                  $txtReal = !empty($rval[COL_MONEVREALISASI])||$rval[COL_MONEVREALISASI]!=null?$rval[COL_MONEVREALISASI]:'-';
                  ?>
                  <tr style="background: #dedede; font-style: italic">
                    <td style="vertical-align: top"><?=strtoupper($rdet[$i][COL_SSRINDIKATORURAIAN])?></td>
                    <td style="vertical-align: middle; white-space: nowrap"><?=strtoupper($rdet[$i][COL_SSRINDIKATORSATUAN])?></td>
                    <td style="text-align: center; vertical-align: middle; white-space: nowrap"><?=$txtTargetTW1?></td>
                    <td style="text-align: center; vertical-align: middle; white-space: nowrap"><?=$txtTargetTW2?></td>
                    <td style="text-align: center; vertical-align: middle; white-space: nowrap"><?=$txtTargetTW3?></td>
                    <td style="text-align: center; vertical-align: middle; white-space: nowrap"><?=$txtTargetTW4?></td>
                    <td style="text-align: center; vertical-align: middle; white-space: nowrap"><?=$txtTarget?></td>
                    <?php
                    /*if($idTW==1) {
                      ?>
                      <td style="text-align: center; vertical-align: middle; white-space: nowrap"><?=$txtTargetTW1?></td>
                      <td style="text-align: center; vertical-align: middle; white-space: nowrap"><?=$txtRealTW1?></td>
                      <?php
                    } else if($idTW==2) {
                      ?>
                      <td style="text-align: center; vertical-align: middle; white-space: nowrap"><?=$txtTargetTW2?></td>
                      <td style="text-align: center; vertical-align: middle; white-space: nowrap"><?=$txtRealTW2?></td>
                      <?php
                    } else if($idTW==3) {
                      ?>
                      <td style="text-align: center; vertical-align: middle; white-space: nowrap"><?=$txtTargetTW3?></td>
                      <td style="text-align: center; vertical-align: middle; white-space: nowrap"><?=$txtRealTW3?></td>
                      <?php
                    } else if($idTW==4) {
                      ?>
                      <td style="text-align: center; vertical-align: middle; white-space: nowrap"><?=$txtTargetTW4?></td>
                      <td style="text-align: center; vertical-align: middle; white-space: nowrap"><?=$txtRealTW4?></td>
                      <?php
                    } else if($idTW==99) {
                      ?>
                      <td style="text-align: center; vertical-align: middle; white-space: nowrap"><?=$txtTarget?></td>
                      <td style="text-align: center; vertical-align: middle; white-space: nowrap"><?=$txtReal?></td>
                      <?php
                    }*/
                    ?>
                  </tr>
                  <?php
                }
              }

              foreach($rprogram as $prg) {
                $rPrgInd = $this->db
                ->where(COL_IDPROGRAM, $prg[COL_PROGRAMID])
                ->order_by(COL_SASARANNO)
                ->get(TBL_SAKIPV2_BID_PROGSASARAN)
                ->result_array();

                $rkegiatan = $this->db
                ->where(COL_IDPROGRAM, $prg[COL_PROGRAMID])
                ->order_by(COL_KEGIATANKODE)
                ->get(TBL_SAKIPV2_BID_KEGIATAN)
                ->result_array();
                ?>
                <!--<tr class="d-none">
                  <td <?=!empty($rPrgInd)&&count($rPrgInd)>1?'rowspan="'.(count($rPrgInd)).'"':''?> style="vertical-align: top"><?=strtoupper($prg[COL_PROGRAMURAIAN])?></td>
                  <?php
                  if(!empty($rPrgInd)) {
                    $txtTargetTW1 = !empty($rPrgInd[0][COL_SASARANTARGETTW1])||$rPrgInd[0][COL_SASARANTARGETTW1]!=null?$rPrgInd[0][COL_SASARANTARGETTW1]:'-';
                    $txtTargetTW2 = !empty($rPrgInd[0][COL_SASARANTARGETTW2])||$rPrgInd[0][COL_SASARANTARGETTW2]!=null?$rPrgInd[0][COL_SASARANTARGETTW2]:'-';
                    $txtTargetTW3 = !empty($rPrgInd[0][COL_SASARANTARGETTW3])||$rPrgInd[0][COL_SASARANTARGETTW3]!=null?$rPrgInd[0][COL_SASARANTARGETTW3]:'-';
                    $txtTargetTW4 = !empty($rPrgInd[0][COL_SASARANTARGETTW4])||$rPrgInd[0][COL_SASARANTARGETTW4]!=null?$rPrgInd[0][COL_SASARANTARGETTW4]:'-';

                    $txtRealTW1 = !empty($rPrgInd[0][COL_SASARANREALISASITW1])||$rPrgInd[0][COL_SASARANREALISASITW1]!=null?$rPrgInd[0][COL_SASARANREALISASITW1]:'-';
                    $txtRealTW2 = !empty($rPrgInd[0][COL_SASARANREALISASITW2])||$rPrgInd[0][COL_SASARANREALISASITW2]!=null?$rPrgInd[0][COL_SASARANREALISASITW2]:'-';
                    $txtRealTW3 = !empty($rPrgInd[0][COL_SASARANREALISASITW3])||$rPrgInd[0][COL_SASARANREALISASITW3]!=null?$rPrgInd[0][COL_SASARANREALISASITW3]:'-';
                    $txtRealTW4 = !empty($rPrgInd[0][COL_SASARANREALISASITW4])||$rPrgInd[0][COL_SASARANREALISASITW4]!=null?$rPrgInd[0][COL_SASARANREALISASITW4]:'-';

                    $txtReal = !empty($rPrgInd[0][COL_SASARANREALISASI])||$rPrgInd[0][COL_SASARANREALISASI]!=null?$rPrgInd[0][COL_SASARANREALISASI]:'-';
                    ?>
                    <td style="vertical-align: top"><?=strtoupper($rPrgInd[0][COL_SASARANINDIKATOR])?></td>
                    <td style="vertical-align: middle; white-space: nowrap"><?=!empty($rPrgInd)?strtoupper($rPrgInd[0][COL_SASARANSATUAN]):'-'?></td>

                    <?php
                    if($idTW==1) {
                      ?>
                      <td style="text-align: center; vertical-align: middle; white-space: nowrap"><?=$txtTargetTW1?></td>
                      <td style="text-align: center; vertical-align: middle; white-space: nowrap"><?=$txtRealTW1?></td>
                      <td style="text-align: center; vertical-align: middle; white-space: nowrap"></td>
                      <?php
                    } else if($idTW==2) {
                      ?>
                      <td style="text-align: center; vertical-align: middle; white-space: nowrap"><?=$txtTargetTW2?></td>
                      <td style="text-align: center; vertical-align: middle; white-space: nowrap"><?=$txtRealTW2?></td>
                      <td style="text-align: center; vertical-align: middle; white-space: nowrap"></td>
                      <?php
                    } else if($idTW==3) {
                      ?>
                      <td style="text-align: center; vertical-align: middle; white-space: nowrap"><?=$txtTargetTW3?></td>
                      <td style="text-align: center; vertical-align: middle; white-space: nowrap"><?=$txtRealTW3?></td>
                      <td style="text-align: center; vertical-align: middle; white-space: nowrap"></td>
                      <?php
                    } else if($idTW==4) {
                      ?>
                      <td style="text-align: center; vertical-align: middle; white-space: nowrap"><?=$txtTargetTW4?></td>
                      <td style="text-align: center; vertical-align: middle; white-space: nowrap"><?=$txtRealTW4?></td>
                      <td style="text-align: center; vertical-align: middle; white-space: nowrap"></td>
                      <?php
                    } else if($idTW==99) {
                      ?>
                      <td style="text-align: center; vertical-align: middle; white-space: nowrap"><?=!empty($rPrgInd)?strtoupper($rPrgInd[0][COL_SASARANTARGET]):'-'?></td>
                      <td style="text-align: center; vertical-align: middle; white-space: nowrap"><?=$txtReal?></td>
                      <td style="text-align: center; vertical-align: middle; white-space: nowrap"></td>
                      <?php
                    }
                    ?>

                    <?php
                  } else {
                    ?>
                    <td colspan="5" style="text-align: center; vertical-align: middle; white-space: nowrap">(KOSONG)</td>
                    <?php
                  }
                  ?>
                </tr>-->
                <?php
                if(!empty($rPrgInd) && count($rPrgInd)>1) {
                  for($i=1; $i<count($rPrgInd); $i++) {
                    $txtTargetTW1 = !empty($rPrgInd[$i][COL_SASARANTARGETTW1])||$rPrgInd[$i][COL_SASARANTARGETTW1]!=null?$rPrgInd[$i][COL_SASARANTARGETTW1]:'-';
                    $txtTargetTW2 = !empty($rPrgInd[$i][COL_SASARANTARGETTW2])||$rPrgInd[$i][COL_SASARANTARGETTW2]!=null?$rPrgInd[$i][COL_SASARANTARGETTW2]:'-';
                    $txtTargetTW3 = !empty($rPrgInd[$i][COL_SASARANTARGETTW3])||$rPrgInd[$i][COL_SASARANTARGETTW3]!=null?$rPrgInd[$i][COL_SASARANTARGETTW3]:'-';
                    $txtTargetTW4 = !empty($rPrgInd[$i][COL_SASARANTARGETTW4])||$rPrgInd[$i][COL_SASARANTARGETTW4]!=null?$rPrgInd[$i][COL_SASARANTARGETTW4]:'-';

                    $txtRealTW1 = !empty($rPrgInd[$i][COL_SASARANREALISASITW1])||$rPrgInd[$i][COL_SASARANREALISASITW1]!=null?$rPrgInd[$i][COL_SASARANREALISASITW1]:'-';
                    $txtRealTW2 = !empty($rPrgInd[$i][COL_SASARANREALISASITW2])||$rPrgInd[$i][COL_SASARANREALISASITW2]!=null?$rPrgInd[$i][COL_SASARANREALISASITW2]:'-';
                    $txtRealTW3 = !empty($rPrgInd[$i][COL_SASARANREALISASITW3])||$rPrgInd[$i][COL_SASARANREALISASITW3]!=null?$rPrgInd[$i][COL_SASARANREALISASITW3]:'-';
                    $txtRealTW4 = !empty($rPrgInd[$i][COL_SASARANREALISASITW4])||$rPrgInd[$i][COL_SASARANREALISASITW4]!=null?$rPrgInd[$i][COL_SASARANREALISASITW4]:'-';

                    $txtReal = !empty($rPrgInd[$i][COL_SASARANREALISASI])||$rPrgInd[$i][COL_SASARANREALISASI]!=null?$rPrgInd[$i][COL_SASARANREALISASI]:'-';
                    ?>
                    <!--<tr class="d-none">
                      <td style="vertical-align: top"><?=strtoupper($rPrgInd[$i][COL_SASARANINDIKATOR])?></td>
                      <td style="vertical-align: middle; white-space: nowrap"><?=strtoupper($rPrgInd[$i][COL_SASARANSATUAN])?></td>
                      <?php
                      if($idTW==1) {
                        ?>
                        <td style="text-align: center; vertical-align: middle; white-space: nowrap"><?=$txtTargetTW1?></td>
                        <td style="text-align: center; vertical-align: middle; white-space: nowrap"><?=$txtRealTW1?></td>
                        <td style="text-align: center; vertical-align: middle; white-space: nowrap"></td>
                        <?php
                      } else if($idTW==2) {
                        ?>
                        <td style="text-align: center; vertical-align: middle; white-space: nowrap"><?=$txtTargetTW2?></td>
                        <td style="text-align: center; vertical-align: middle; white-space: nowrap"><?=$txtRealTW2?></td>
                        <td style="text-align: center; vertical-align: middle; white-space: nowrap"></td>
                        <?php
                      } else if($idTW==3) {
                        ?>
                        <td style="text-align: center; vertical-align: middle; white-space: nowrap"><?=$txtTargetTW3?></td>
                        <td style="text-align: center; vertical-align: middle; white-space: nowrap"><?=$txtRealTW3?></td>
                        <td style="text-align: center; vertical-align: middle; white-space: nowrap"></td>
                        <?php
                      } else if($idTW==4) {
                        ?>
                        <td style="text-align: center; vertical-align: middle; white-space: nowrap"><?=$txtTargetTW4?></td>
                        <td style="text-align: center; vertical-align: middle; white-space: nowrap"><?=$txtRealTW4?></td>
                        <td style="text-align: center; vertical-align: middle; white-space: nowrap"></td>
                        <?php
                      } else if($idTW==99) {
                        ?>
                        <td style="text-align: center; vertical-align: middle; white-space: nowrap"><?=strtoupper($rPrgInd[$i][COL_SASARANTARGET])?></td>
                        <td style="text-align: center; vertical-align: middle; white-space: nowrap"><?=$txtReal?></td>
                        <td style="text-align: center; vertical-align: middle; white-space: nowrap"></td>
                        <?php
                      }
                      ?>
                    </tr>-->
                    <?php
                  }
                }

                foreach($rkegiatan as $keg) {
                  $rKegInd = $this->db
                  ->where(COL_IDKEGIATAN, $keg[COL_KEGIATANID])
                  ->order_by(COL_SASARANNO)
                  ->get(TBL_SAKIPV2_BID_KEGSASARAN)
                  ->result_array();

                  $rsubkeg = $this->db
                  ->where(COL_IDKEGIATAN, $keg[COL_KEGIATANID])
                  ->order_by(COL_SUBKEGKODE)
                  ->get(TBL_SAKIPV2_SUBBID_SUBKEGIATAN)
                  ->result_array();
                  ?>
                  <!--<tr class="d-none">
                    <td <?=!empty($rKegInd)&&count($rKegInd)>1?'rowspan="'.(count($rKegInd)).'"':''?> style="vertical-align: top; padding-left: 1.5rem!important"><?=strtoupper($keg[COL_KEGIATANURAIAN])?></td>
                    <?php
                    if(!empty($rKegInd)) {
                      $txtTargetTW1 = !empty($rKegInd[0][COL_SASARANTARGETTW1])||$rKegInd[0][COL_SASARANTARGETTW1]!=null?$rKegInd[0][COL_SASARANTARGETTW1]:'-';
                      $txtTargetTW2 = !empty($rKegInd[0][COL_SASARANTARGETTW2])||$rKegInd[0][COL_SASARANTARGETTW2]!=null?$rKegInd[0][COL_SASARANTARGETTW2]:'-';
                      $txtTargetTW3 = !empty($rKegInd[0][COL_SASARANTARGETTW3])||$rKegInd[0][COL_SASARANTARGETTW3]!=null?$rKegInd[0][COL_SASARANTARGETTW3]:'-';
                      $txtTargetTW4 = !empty($rKegInd[0][COL_SASARANTARGETTW4])||$rKegInd[0][COL_SASARANTARGETTW4]!=null?$rKegInd[0][COL_SASARANTARGETTW4]:'-';

                      $txtRealTW1 = !empty($rKegInd[0][COL_SASARANREALISASITW1])||$rKegInd[0][COL_SASARANREALISASITW1]!=null?$rKegInd[0][COL_SASARANREALISASITW1]:'-';
                      $txtRealTW2 = !empty($rKegInd[0][COL_SASARANREALISASITW2])||$rKegInd[0][COL_SASARANREALISASITW2]!=null?$rKegInd[0][COL_SASARANREALISASITW2]:'-';
                      $txtRealTW3 = !empty($rKegInd[0][COL_SASARANREALISASITW3])||$rKegInd[0][COL_SASARANREALISASITW3]!=null?$rKegInd[0][COL_SASARANREALISASITW3]:'-';
                      $txtRealTW4 = !empty($rKegInd[0][COL_SASARANREALISASITW4])||$rKegInd[0][COL_SASARANREALISASITW4]!=null?$rKegInd[0][COL_SASARANREALISASITW4]:'-';

                      $txtReal = !empty($rKegInd[0][COL_SASARANREALISASI])||$rKegInd[0][COL_SASARANREALISASI]!=null?$rKegInd[0][COL_SASARANREALISASI]:'-';
                      ?>
                      <td style="vertical-align: top"><?=strtoupper($rKegInd[0][COL_SASARANINDIKATOR])?></td>
                      <td style="vertical-align: middle; white-space: nowrap"><?=!empty($rKegInd)?strtoupper($rKegInd[0][COL_SASARANSATUAN]):'-'?></td>
                      <?php
                      if($idTW==1) {
                        ?>
                        <td style="text-align: center; vertical-align: middle; white-space: nowrap"><?=$txtTargetTW1?></td>
                        <td style="text-align: center; vertical-align: middle; white-space: nowrap"><?=$txtRealTW1?></td>
                        <td style="text-align: center; vertical-align: middle; white-space: nowrap"></td>
                        <?php
                      } else if($idTW==2) {
                        ?>
                        <td style="text-align: center; vertical-align: middle; white-space: nowrap"><?=$txtTargetTW2?></td>
                        <td style="text-align: center; vertical-align: middle; white-space: nowrap"><?=$txtRealTW2?></td>
                        <td style="text-align: center; vertical-align: middle; white-space: nowrap"></td>
                        <?php
                      } else if($idTW==3) {
                        ?>
                        <td style="text-align: center; vertical-align: middle; white-space: nowrap"><?=$txtTargetTW3?></td>
                        <td style="text-align: center; vertical-align: middle; white-space: nowrap"><?=$txtRealTW3?></td>
                        <td style="text-align: center; vertical-align: middle; white-space: nowrap"></td>
                        <?php
                      } else if($idTW==4) {
                        ?>
                        <td style="text-align: center; vertical-align: middle; white-space: nowrap"><?=$txtTargetTW4?></td>
                        <td style="text-align: center; vertical-align: middle; white-space: nowrap"><?=$txtRealTW4?></td>
                        <td style="text-align: center; vertical-align: middle; white-space: nowrap"></td>
                        <?php
                      } else if($idTW==99) {
                        ?>
                        <td style="text-align: center; vertical-align: middle; white-space: nowrap"><?=!empty($rKegInd)?strtoupper($rKegInd[0][COL_SASARANTARGET]):'-'?><</td>
                        <td style="text-align: center; vertical-align: middle; white-space: nowrap"><?=$txtReal?></td>
                        <td style="text-align: center; vertical-align: middle; white-space: nowrap"></td>
                        <?php
                      }
                      ?>

                      <?php
                    } else {
                      ?>
                      <td colspan="5" style="text-align: center; vertical-align: middle; white-space: nowrap">(KOSONG)</td>
                      <?php
                    }
                    ?>
                  </tr>-->
                  <?php
                  if(!empty($rKegInd) && count($rKegInd)>1) {
                    for($i=1; $i<count($rKegInd); $i++) {
                      $txtTargetTW1 = !empty($rKegInd[$i][COL_SASARANTARGETTW1])||$rKegInd[$i][COL_SASARANTARGETTW1]!=null?$rKegInd[$i][COL_SASARANTARGETTW1]:'-';
                      $txtTargetTW2 = !empty($rKegInd[$i][COL_SASARANTARGETTW2])||$rKegInd[$i][COL_SASARANTARGETTW2]!=null?$rKegInd[$i][COL_SASARANTARGETTW2]:'-';
                      $txtTargetTW3 = !empty($rKegInd[$i][COL_SASARANTARGETTW3])||$rKegInd[$i][COL_SASARANTARGETTW3]!=null?$rKegInd[$i][COL_SASARANTARGETTW3]:'-';
                      $txtTargetTW4 = !empty($rKegInd[$i][COL_SASARANTARGETTW4])||$rKegInd[$i][COL_SASARANTARGETTW4]!=null?$rKegInd[$i][COL_SASARANTARGETTW4]:'-';

                      $txtRealTW1 = !empty($rKegInd[$i][COL_SASARANREALISASITW1])||$rKegInd[$i][COL_SASARANREALISASITW1]!=null?$rKegInd[$i][COL_SASARANREALISASITW1]:'-';
                      $txtRealTW2 = !empty($rKegInd[$i][COL_SASARANREALISASITW2])||$rKegInd[$i][COL_SASARANREALISASITW2]!=null?$rKegInd[$i][COL_SASARANREALISASITW2]:'-';
                      $txtRealTW3 = !empty($rKegInd[$i][COL_SASARANREALISASITW3])||$rKegInd[$i][COL_SASARANREALISASITW3]!=null?$rKegInd[$i][COL_SASARANREALISASITW3]:'-';
                      $txtRealTW4 = !empty($rKegInd[$i][COL_SASARANREALISASITW4])||$rKegInd[$i][COL_SASARANREALISASITW4]!=null?$rKegInd[$i][COL_SASARANREALISASITW4]:'-';

                      $txtReal = !empty($rKegInd[$i][COL_SASARANREALISASI])||$rKegInd[$i][COL_SASARANREALISASI]!=null?$rKegInd[$i][COL_SASARANREALISASI]:'-';
                      ?>
                      <!--<tr class="d-none">
                        <td style="vertical-align: top"><?=strtoupper($rKegInd[$i][COL_SASARANINDIKATOR])?></td>
                        <td style="vertical-align: middle; white-space: nowrap"><?=strtoupper($rKegInd[$i][COL_SASARANSATUAN])?></td>
                        <?php
                        if($idTW==1) {
                          ?>
                          <td style="text-align: center; vertical-align: middle; white-space: nowrap"><?=$txtTargetTW1?></td>
                          <td style="text-align: center; vertical-align: middle; white-space: nowrap"><?=$txtRealTW1?></td>
                          <td style="text-align: center; vertical-align: middle; white-space: nowrap"></td>
                          <?php
                        } else if($idTW==2) {
                          ?>
                          <td style="text-align: center; vertical-align: middle; white-space: nowrap"><?=$txtTargetTW2?></td>
                          <td style="text-align: center; vertical-align: middle; white-space: nowrap"><?=$txtRealTW2?></td>
                          <td style="text-align: center; vertical-align: middle; white-space: nowrap"></td>
                          <?php
                        } else if($idTW==3) {
                          ?>
                          <td style="text-align: center; vertical-align: middle; white-space: nowrap"><?=$txtTargetTW3?></td>
                          <td style="text-align: center; vertical-align: middle; white-space: nowrap"><?=$txtRealTW3?></td>
                          <td style="text-align: center; vertical-align: middle; white-space: nowrap"></td>
                          <?php
                        } else if($idTW==4) {
                          ?>
                          <td style="text-align: center; vertical-align: middle; white-space: nowrap"><?=$txtTargetTW4?></td>
                          <td style="text-align: center; vertical-align: middle; white-space: nowrap"><?=$txtRealTW4?></td>
                          <td style="text-align: center; vertical-align: middle; white-space: nowrap"></td>
                          <?php
                        } else if($idTW==99) {
                          ?>
                          <td style="text-align: center; vertical-align: middle; white-space: nowrap"><?=strtoupper($rKegInd[$i][COL_SASARANTARGET])?></td>
                          <td style="text-align: center; vertical-align: middle; white-space: nowrap"><?=$txtReal?></td>
                          <td style="text-align: center; vertical-align: middle; white-space: nowrap"></td>
                          <?php
                        }
                        ?>
                      </tr>-->
                      <?php
                    }
                  }

                  foreach($rsubkeg as $sub) {
                    $rSubKegInd = $this->db
                    ->where(COL_IDSUBKEG, $sub[COL_SUBKEGID])
                    ->order_by(COL_SASARANNO)
                    ->get(TBL_SAKIPV2_SUBBID_SUBKEGSASARAN)
                    ->result_array();

                    $rrenaksi = $this->db
                    ->where(COL_IDSUBKEG, $sub[COL_SUBKEGID])
                    ->get(TBL_SAKIPV2_SKPD_RENAKSI)
                    ->result_array();
                    ?>
                    <tr>
                      <td <?=!empty($rSubKegInd)&&count($rSubKegInd)>1?'rowspan="'.(count($rSubKegInd)).'"':''?> style="vertical-align: middle;"><?=strtoupper($sub[COL_SUBKEGURAIAN])?></td>
                      <?php
                      if(!empty($rSubKegInd)) {
                        $txtTargetTW1 = !empty($rSubKegInd[0][COL_SASARANTARGETTW1])||$rSubKegInd[0][COL_SASARANTARGETTW1]!=null?$rSubKegInd[0][COL_SASARANTARGETTW1]:'-';
                        $txtTargetTW2 = !empty($rSubKegInd[0][COL_SASARANTARGETTW2])||$rSubKegInd[0][COL_SASARANTARGETTW2]!=null?$rSubKegInd[0][COL_SASARANTARGETTW2]:'-';
                        $txtTargetTW3 = !empty($rSubKegInd[0][COL_SASARANTARGETTW3])||$rSubKegInd[0][COL_SASARANTARGETTW3]!=null?$rSubKegInd[0][COL_SASARANTARGETTW3]:'-';
                        $txtTargetTW4 = !empty($rSubKegInd[0][COL_SASARANTARGETTW4])||$rSubKegInd[0][COL_SASARANTARGETTW4]!=null?$rSubKegInd[0][COL_SASARANTARGETTW4]:'-';

                        $txtRealTW1 = !empty($rSubKegInd[0][COL_SASARANREALISASITW1])||$rSubKegInd[0][COL_SASARANREALISASITW1]!=null?$rSubKegInd[0][COL_SASARANREALISASITW1]:'-';
                        $txtRealTW2 = !empty($rSubKegInd[0][COL_SASARANREALISASITW2])||$rSubKegInd[0][COL_SASARANREALISASITW2]!=null?$rSubKegInd[0][COL_SASARANREALISASITW2]:'-';
                        $txtRealTW3 = !empty($rSubKegInd[0][COL_SASARANREALISASITW3])||$rSubKegInd[0][COL_SASARANREALISASITW3]!=null?$rSubKegInd[0][COL_SASARANREALISASITW3]:'-';
                        $txtRealTW4 = !empty($rSubKegInd[0][COL_SASARANREALISASITW4])||$rSubKegInd[0][COL_SASARANREALISASITW4]!=null?$rSubKegInd[0][COL_SASARANREALISASITW4]:'-';

                        $txtReal = !empty($rSubKegInd[0][COL_SASARANREALISASI])||$rSubKegInd[0][COL_SASARANREALISASI]!=null?$rSubKegInd[0][COL_SASARANREALISASI]:'-';
                        ?>
                        <td style="vertical-align: top"><?=strtoupper($rSubKegInd[0][COL_SASARANINDIKATOR])?></td>
                        <td style="vertical-align: middle; white-space: nowrap"><?=!empty($rSubKegInd)?strtoupper($rSubKegInd[0][COL_SASARANSATUAN]):'-'?></td>
                        <td style="text-align: center; vertical-align: middle; white-space: nowrap"><?=$txtTargetTW1?></td>
                        <td style="text-align: center; vertical-align: middle; white-space: nowrap"><?=$txtTargetTW2?></td>
                        <td style="text-align: center; vertical-align: middle; white-space: nowrap"><?=$txtTargetTW3?></td>
                        <td style="text-align: center; vertical-align: middle; white-space: nowrap"><?=$txtTargetTW4?></td>
                        <td style="text-align: center; vertical-align: middle; white-space: nowrap"><?=!empty($rSubKegInd)?strtoupper($rSubKegInd[0][COL_SASARANTARGET]):'-'?></td>
                        <?php
                        /*if($idTW==1) {
                          ?>
                          <td style="text-align: center; vertical-align: middle; white-space: nowrap"><?=$txtTargetTW1?></td>
                          <td style="text-align: center; vertical-align: middle; white-space: nowrap"><?=$txtRealTW1?></td>
                          <?php
                        } else if($idTW==2) {
                          ?>
                          <td style="text-align: center; vertical-align: middle; white-space: nowrap"><?=$txtTargetTW2?></td>
                          <td style="text-align: center; vertical-align: middle; white-space: nowrap"><?=$txtRealTW2?></td>
                          <?php
                        } else if($idTW==3) {
                          ?>
                          <td style="text-align: center; vertical-align: middle; white-space: nowrap"><?=$txtTargetTW3?></td>
                          <td style="text-align: center; vertical-align: middle; white-space: nowrap"><?=$txtRealTW3?></td>
                          <?php
                        } else if($idTW==4) {
                          ?>
                          <td style="text-align: center; vertical-align: middle; white-space: nowrap"><?=$txtTargetTW4?></td>
                          <td style="text-align: center; vertical-align: middle; white-space: nowrap"><?=$txtRealTW4?></td>
                          <?php
                        } else if($idTW==99) {
                          ?>
                          <td style="text-align: center; vertical-align: middle; white-space: nowrap"><?=!empty($rSubKegInd)?strtoupper($rSubKegInd[0][COL_SASARANTARGET]):'-'?></td>
                          <td style="text-align: center; vertical-align: middle; white-space: nowrap"><?=$txtReal?></td>
                          <?php
                        }*/
                        ?>
                        <?php
                      } else {
                        ?>
                        <td colspan="8" style="text-align: center; vertical-align: middle; white-space: nowrap">(KOSONG)</td>
                        <?php
                      }
                      ?>
                    </tr>
                    <?php
                    if(!empty($rSubKegInd) && count($rSubKegInd)>1) {
                      for($i=1; $i<count($rSubKegInd); $i++) {
                        $txtTargetTW1 = !empty($rSubKegInd[$i][COL_SASARANTARGETTW1])||$rSubKegInd[$i][COL_SASARANTARGETTW1]!=null?$rSubKegInd[$i][COL_SASARANTARGETTW1]:'-';
                        $txtTargetTW2 = !empty($rSubKegInd[$i][COL_SASARANTARGETTW2])||$rSubKegInd[$i][COL_SASARANTARGETTW2]!=null?$rSubKegInd[$i][COL_SASARANTARGETTW2]:'-';
                        $txtTargetTW3 = !empty($rSubKegInd[$i][COL_SASARANTARGETTW3])||$rSubKegInd[$i][COL_SASARANTARGETTW3]!=null?$rSubKegInd[$i][COL_SASARANTARGETTW3]:'-';
                        $txtTargetTW4 = !empty($rSubKegInd[$i][COL_SASARANTARGETTW4])||$rSubKegInd[$i][COL_SASARANTARGETTW4]!=null?$rSubKegInd[$i][COL_SASARANTARGETTW4]:'-';

                        $txtRealTW1 = !empty($rSubKegInd[$i][COL_SASARANREALISASITW1])||$rSubKegInd[$i][COL_SASARANREALISASITW1]!=null?$rSubKegInd[$i][COL_SASARANREALISASITW1]:'-';
                        $txtRealTW2 = !empty($rSubKegInd[$i][COL_SASARANREALISASITW2])||$rSubKegInd[$i][COL_SASARANREALISASITW2]!=null?$rSubKegInd[$i][COL_SASARANREALISASITW2]:'-';
                        $txtRealTW3 = !empty($rSubKegInd[$i][COL_SASARANREALISASITW3])||$rSubKegInd[$i][COL_SASARANREALISASITW3]!=null?$rSubKegInd[$i][COL_SASARANREALISASITW3]:'-';
                        $txtRealTW4 = !empty($rSubKegInd[$i][COL_SASARANREALISASITW4])||$rSubKegInd[$i][COL_SASARANREALISASITW4]!=null?$rSubKegInd[$i][COL_SASARANREALISASITW4]:'-';

                        $valReal = !empty($rSubKegInd[$i][COL_SASARANREALISASI])||$rSubKegInd[$i][COL_SASARANREALISASI]!=null?$rSubKegInd[$i][COL_SASARANREALISASI]:'';
                        ?>
                        <tr>
                          <td style="vertical-align: top"><?=strtoupper($rSubKegInd[$i][COL_SASARANINDIKATOR])?></td>
                          <td style="vertical-align: middle; white-space: nowrap"><?=$rSubKegInd[$i][COL_SASARANSATUAN]?></td>
                          <td style="text-align: center; vertical-align: middle; white-space: nowrap"><?=$txtTargetTW1?></td>
                          <td style="text-align: center; vertical-align: middle; white-space: nowrap"><?=$txtTargetTW2?></td>
                          <td style="text-align: center; vertical-align: middle; white-space: nowrap"><?=$txtTargetTW3?></td>
                          <td style="text-align: center; vertical-align: middle; white-space: nowrap"><?=$txtTargetTW4?></td>
                          <td style="text-align: center; vertical-align: middle; white-space: nowrap"><?=$rSubKegInd[$i][COL_SASARANTARGET]?></td>
                          <?php
                          /*if($idTW==1) {
                            ?>
                            <td style="text-align: center; vertical-align: middle; white-space: nowrap"><?=$txtTargetTW1?></td>
                            <td style="text-align: center; vertical-align: middle; white-space: nowrap"><?=$txtRealTW1?></td>
                            <?php
                          } else if($idTW==2) {
                            ?>
                            <td style="text-align: center; vertical-align: middle; white-space: nowrap"><?=$txtTargetTW2?></td>
                            <td style="text-align: center; vertical-align: middle; white-space: nowrap"><?=$txtRealTW2?></td>
                            <?php
                          } else if($idTW==3) {
                            ?>
                            <td style="text-align: center; vertical-align: middle; white-space: nowrap"><?=$txtTargetTW3?></td>
                            <td style="text-align: center; vertical-align: middle; white-space: nowrap"><?=$txtRealTW3?></td>
                            <?php
                          } else if($idTW==4) {
                            ?>
                            <td style="text-align: center; vertical-align: middle; white-space: nowrap"><?=$txtTargetTW4?></td>
                            <td style="text-align: center; vertical-align: middle; white-space: nowrap"><?=$txtRealTW4?></td>
                            <?php
                          } else if($idTW==99) {
                            ?>
                            <td style="text-align: center; vertical-align: middle; white-space: nowrap"><?=$rSubKegInd[$i][COL_SASARANTARGET]?></td>
                            <td style="text-align: center; vertical-align: middle; white-space: nowrap"><?=$txtReal?></td>
                            <?php
                          }*/
                          ?>
                        </tr>
                        <?php
                      }
                    }
                    ?>

                    <?php
                    if(!empty($rrenaksi)) {
                      $noren = 1;
                      foreach($rrenaksi as $ren) {
                        $renTargetTW1 = $ren['RenaksiTargetTW1'];
                        $renTargetTW2 = $ren['RenaksiTargetTW2'];
                        $renTargetTW3 = $ren['RenaksiTargetTW3'];
                        $renTargetTW4 = $ren['RenaksiTargetTW4'];
                        $renTarget = $ren['RenaksiTarget'];
                        ?>
                        <tr>
                          <td style="vertical-align: middle;"><?=$ren[COL_RENAKSIURAIAN]?></td>
                          <td style="vertical-align: middle"><?=$ren[COL_RENAKSIINDIKATOR]?></td>
                          <td style="vertical-align: middle; white-space: nowrap"><?=$ren[COL_RENAKSISATUAN]?></td>
                          <td style="text-align: center; vertical-align: middle; white-space: nowrap"><?=$renTargetTW1?></td>
                          <td style="text-align: center; vertical-align: middle; white-space: nowrap"><?=$renTargetTW2?></td>
                          <td style="text-align: center; vertical-align: middle; white-space: nowrap"><?=$renTargetTW3?></td>
                          <td style="text-align: center; vertical-align: middle; white-space: nowrap"><?=$renTargetTW4?></td>
                          <td style="text-align: center; vertical-align: middle; white-space: nowrap"><?=$renTarget?></td>
                          <!--<td style="text-align: center; vertical-align: middle; white-space: nowrap"><?=$renCapaian?></td>-->
                          <!--<td style="vertical-align: middle; width: 40px !important; overflow: hidden;">
                            <?=filter_var($renRemarks, FILTER_VALIDATE_URL) !== false?'<a href="'.$renRemarks.'" target="_blank" style="text-align: center">Link</a>':$renRemarks?>
                          </td>-->
                        </tr>
                        <?php
                        $noren++;
                      }
                    } else {
                      ?>
                      <tr>
                        <td colspan="5" style="text-align: center; font-style: italic">belum ada data tersedia.</td>
                      </tr>
                      <?php
                    }
                    ?>


                    <?php
                  }
                }
              }
            }
          } else {
            ?>
            <tr>
              <td colspan="6">
                <p style="text-align: center; font-style: italic; margin-bottom: 0 !important">
                  (KOSONG)
                </p>
              </td>
            </tr>
            <?php
          }
          ?>
        </tbody>
      </table>
    </div>

  </div>
</div>
