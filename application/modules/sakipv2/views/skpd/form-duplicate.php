<?php
$rOptDpa = $this->db
->where(COL_IDRENSTRA, $data[COL_IDRENSTRA])
->where(COL_DPAID." != ",$data[COL_DPAID])
->where(COL_DPAISAKTIF, 1)
->order_by(COL_DPATAHUN)
->get(TBL_SAKIPV2_SKPD_RENSTRA_DPA)
->result_array();
?>
<form id="form-dpa" method="post" action="<?=site_url('sakipv2/skpd/ajax-duplicate-dpa/'.$data[COL_DPAID])?>">
  <div class="row">
    <div class="col-sm-12">
      <div class="form-group">
        <label>DPA SUMBER</label>
        <input type="text" class="form-control" style="padding: .375rem 8px !important" value="<?=$data[COL_DPATAHUN].' - '.$data[COL_DPAURAIAN]?>" readonly />
      </div>
      <div class="form-group">
        <label>DPA TUJUAN</label>
        <select class="form-control" name="<?=COL_DPAID?>" style="width: 100%">
          <?php
          foreach($rOptDpa as $opt) {
            ?>
            <option value="<?=$opt[COL_DPAID]?>"><?=$opt[COL_DPATAHUN].' - ',$opt[COL_DPAURAIAN]?></option>
            <?php
          }
          ?>
        </select>
      </div>
      <p class="text-muted font-italic mb-0" style="text-align: justify">
        <strong>PERINGATAN:</strong><br />
        Menduplikasi data DPA SKPD akan otomatis <span class="text-danger">mengosongkan</span> data DPA TUJUAN lalu diisi dengan salinan data yang ada pada DPA SUMBER. Harap memeriksa terlebih dahulu sebelum melanjutkan duplikasi.
      </p>
    </div>
  </div>
</form>
<script type="text/javascript">
$(document).ready(function(){
  $('#form-dpa').validate({
    submitHandler: function(form) {
      if(confirm("Yakin ingin melanjutkan duplikasi?")) {
        var modal = $(form).closest('modal');
        if(modal) {
          var btnSubmit = $('button[type=submit]', modal);
          var txtSubmit = btnSubmit.innerHTML;
          btnSubmit.html('<i class="fad fa-circle-notch fa-spin"></i>');
          btnSubmit.attr('disabled', true);
        }

        $(form).ajaxSubmit({
          dataType: 'json',
          type : 'post',
          success: function(res) {
            if(res.error != 0) {
              toastr.error(res.error);
            } else {
              toastr.success(res.success);
              setTimeout(function(){
                location.reload();
              }, 1000);
            }
          },
          error: function() {
            toastr.error('SERVER ERROR');
          },
          complete: function() {
            btnSubmit.html(txtSubmit);
            btnSubmit.attr('disabled', false);
          }
        });

        return false;
      }
    }
  });
});
</script>
