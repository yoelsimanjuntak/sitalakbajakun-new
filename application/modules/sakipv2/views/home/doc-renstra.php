<?php
$rrpjmd = $this->db
->where(COL_PMDISAKTIF, 1)
->order_by(COL_PMDTAHUNMULAI, 'desc')
->get(TBL_SAKIPV2_PEMDA)
->row_array();
if(empty($rrpjmd)) {
  $this->load->view('sakipv2/home/404');
  exit();
}

$rskpd = $this->db
->where(COL_SKPDISAKTIF,1)
->order_by(COL_SKPDNAMA)
->get(TBL_SAKIPV2_SKPD)
->result_array();
?>
<div class="row">
  <div class="col-lg-12">
    <div class="ibox ">
      <div class="ibox-title">
        <h5><?=$title?></h5>
        <div class="ibox-tools">
          <a href="<?=site_url('sakipv2/home')?>"><i class="fa fa-arrow-left"></i> KEMBALI</a>
        </div>
      </div>
      <div class="ibox-content">
        <div class="table-responsive">
          <table class="table table-striped" style="border: 1px solid #dedede">
            <thead>
              <tr>
                <th style="width: 10px; white-space: nowrap;">No.</th>
                <th>OPD</th>
                <th>Aksi</th>
              </tr>
            </thead>
            <tbody>
              <?php
              if(!empty($rskpd)) {
                $no=1;
                foreach($rskpd as $r) {
                  $rrenstra = $this->db
                  ->where(COL_RENSTRAISAKTIF, 1)
                  ->where(COL_IDSKPD, $r[COL_SKPDID])
                  ->where(COL_IDPEMDA, $rrpjmd[COL_PMDID])
                  ->get(TBL_SAKIPV2_SKPD_RENSTRA)
                  ->row_array();
                  if(empty($rrenstra)) continue;
                  ?>
                  <tr>
                    <td class="text-right" style="width: 10px; white-space: nowrap;"><?=$no?></td>
                    <td><?=strtoupper($r[COL_SKPDNAMA])?><br /><small class="font-italic">Tahun Berlaku: <strong><?=$rrenstra[COL_RENSTRATAHUN]?></strong></small></td>
                    <td class="text-center">
                      <a href="<?=site_url('sakipv2/home/renstra/'.$rrenstra[COL_RENSTRAID])?>" class="btn btn-primary btn-sm"><i class="fa fa-search"></i> Lihat </a>
                    </td>
                  </tr>
                  <?php
                  $no++;
                }
              } else {
                echo '<tr><td colspan="3" class="text-center font-italic">Belum ada data tersedia.</td></tr>';
              }
              ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
