<?php
$rrpjmd = $this->db
->where(COL_PMDISAKTIF, 1)
->order_by(COL_PMDTAHUNMULAI, 'desc')
->get(TBL_SAKIPV2_PEMDA)
->row_array();
if(empty($rrpjmd)) {
  $this->load->view('sakipv2/home/404');
  exit();
}

$rskpd = $this->db
->where(COL_SKPDISAKTIF,1)
->order_by(COL_SKPDNAMA)
->get(TBL_SAKIPV2_SKPD)
->result_array();
?>
<div class="row">
  <div class="col-lg-12">
    <div class="ibox ">
      <div class="ibox-title">
        <h5><?=$title?></h5>
        <div class="ibox-tools">
          <a href="<?=site_url('sakipv2/home')?>"><i class="fa fa-arrow-left"></i> KEMBALI</a>
        </div>
      </div>
      <div class="ibox-content">
        <div class="table-responsive">
          <table class="table table-striped" style="border: 1px solid #dedede">
            <thead>
              <tr>
                <th style="width: 10px; white-space: nowrap;">No.</th>
                <th>OPD</th>
                <th>Program</th>
                <th>Kegiatan</th>
                <th>Sub Kegiatan</th>
              </tr>
            </thead>
            <tbody>
              <?php
              if(!empty($rskpd)) {
                $no=1;
                foreach($rskpd as $r) {
                  $rrenstra = $this->db
                  ->where(COL_RENSTRAISAKTIF, 1)
                  ->where(COL_IDSKPD, $r[COL_SKPDID])
                  ->where(COL_IDPEMDA, $rrpjmd[COL_PMDID])
                  ->get(TBL_SAKIPV2_SKPD_RENSTRA)
                  ->row_array();
                  if(empty($rrenstra)) continue;

                  $rrenja = $this->db
                  ->where(COL_IDRENSTRA, $rrenstra[COL_RENSTRAID])
                  ->where(COL_DPAISAKTIF, 1)
                  ->where(COL_DPATAHUN, date('Y'))
                  //->order_by(COL_DPATAHUN, 'desc')
                  ->get(TBL_SAKIPV2_SKPD_RENSTRA_DPA)
                  ->row_array();

                  $rprogram = array();
                  $rkegiatan = array();
                  $rsubkeg = array();

                  if(!empty($rrenja)) {
                    $rprogram = $this->db
                    ->join(TBL_SAKIPV2_BID,TBL_SAKIPV2_BID.'.'.COL_BIDID." = ".TBL_SAKIPV2_BID_PROGRAM.".".COL_IDBID,"left")
                    ->where(TBL_SAKIPV2_BID_PROGRAM.'.'.COL_IDDPA, $rrenja[COL_DPAID])
                    ->where(TBL_SAKIPV2_BID.'.'.COL_BIDISAKTIF, 1)
                    ->group_by(TBL_SAKIPV2_BID_PROGRAM.'.'.COL_PROGRAMKODE)
                    ->get(TBL_SAKIPV2_BID_PROGRAM)
                    ->result_array();

                    $rkegiatan = $this->db
                    ->join(TBL_SAKIPV2_BID_PROGRAM,TBL_SAKIPV2_BID_PROGRAM.'.'.COL_PROGRAMID." = ".TBL_SAKIPV2_BID_KEGIATAN.".".COL_IDPROGRAM,"left")
                    ->join(TBL_SAKIPV2_BID,TBL_SAKIPV2_BID.'.'.COL_BIDID." = ".TBL_SAKIPV2_BID_PROGRAM.".".COL_IDBID,"left")
                    ->where(TBL_SAKIPV2_BID_PROGRAM.'.'.COL_IDDPA, $rrenja[COL_DPAID])
                    ->where(TBL_SAKIPV2_BID.'.'.COL_BIDISAKTIF, 1)
                    ->group_by(array(TBL_SAKIPV2_BID_PROGRAM.'.'.COL_PROGRAMKODE,TBL_SAKIPV2_BID_KEGIATAN.'.'.COL_KEGIATANKODE))
                    ->get(TBL_SAKIPV2_BID_KEGIATAN)
                    ->result_array();

                    $rsubkeg = $this->db
                    ->join(TBL_SAKIPV2_BID_KEGIATAN,TBL_SAKIPV2_BID_KEGIATAN.'.'.COL_KEGIATANID." = ".TBL_SAKIPV2_SUBBID_SUBKEGIATAN.".".COL_IDKEGIATAN,"left")
                    ->join(TBL_SAKIPV2_BID_PROGRAM,TBL_SAKIPV2_BID_PROGRAM.'.'.COL_PROGRAMID." = ".TBL_SAKIPV2_BID_KEGIATAN.".".COL_IDPROGRAM,"left")
                    ->join(TBL_SAKIPV2_BID,TBL_SAKIPV2_BID.'.'.COL_BIDID." = ".TBL_SAKIPV2_BID_PROGRAM.".".COL_IDBID,"left")
                    ->join(TBL_SAKIPV2_SUBBID,TBL_SAKIPV2_SUBBID.'.'.COL_SUBBIDID." = ".TBL_SAKIPV2_SUBBID_SUBKEGIATAN.".".COL_IDSUBBID,"left")
                    ->where(TBL_SAKIPV2_BID_PROGRAM.'.'.COL_IDDPA, $rrenja[COL_DPAID])
                    ->where(TBL_SAKIPV2_BID.'.'.COL_BIDISAKTIF, 1)
                    ->where("(sakipv2_subbid.SubbidId is null or sakipv2_subbid.SubbidIsAktif=1)")
                    ->group_by(array(TBL_SAKIPV2_BID_PROGRAM.'.'.COL_PROGRAMKODE,TBL_SAKIPV2_BID_KEGIATAN.'.'.COL_KEGIATANKODE,TBL_SAKIPV2_SUBBID_SUBKEGIATAN.'.'.COL_SUBKEGKODE))
                    ->get(TBL_SAKIPV2_SUBBID_SUBKEGIATAN)
                    ->result_array();
                  }
                  ?>
                  <tr>
                    <td class="text-right" style="width: 10px; white-space: nowrap;"><?=$no?></td>
                    <td><?=strtoupper($r[COL_SKPDNAMA])?><br /><small class="font-italic">Diinput pada: <strong><?=!empty($rrenja)?date('Y-m-d H:i', strtotime($rrenja[COL_CREATEDON])):'-'?></strong></small></td>
                    <td class="text-center" style="vertical-align: middle"><a href="<?=site_url('sakipv2/home/renja/'.$rrenja[COL_DPAID].'/program')?>" class="btn-popup font-weight-bold" data-title="Daftar Program"><?=number_format(count($rprogram))?></a></td>
                    <td class="text-center" style="vertical-align: middle"><a href="<?=site_url('sakipv2/home/renja/'.$rrenja[COL_DPAID].'/kegiatan')?>" class="btn-popup font-weight-bold" data-title="Daftar Kegiatan"><?=number_format(count($rkegiatan))?></a></td>
                    <td class="text-center" style="vertical-align: middle"><a href="<?=site_url('sakipv2/home/renja/'.$rrenja[COL_DPAID].'/subkegiatan')?>" class="btn-popup font-weight-bold" data-title="Daftar Sub Kegiatan"><?=number_format(count($rsubkeg))?></a></td>
                  </tr>
                  <?php
                  $no++;
                }
              } else {
                echo '<tr><td colspan="3" class="text-center font-italic">Belum ada data tersedia.</td></tr>';
              }
              ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="modal-popup" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">Dokumen Renja</h5>
        </div>
        <div class="modal-body">
        </div>
    </div>
  </div>
</div>
<script type="text/javascript">
$(document).ready(function(){
  $('.btn-popup').click(function(){
    var href = $(this).attr('href');

    $('#modal-popup').modal('show');
    $('.modal-body', $('#modal-popup')).html('<p>Memuat...</p>');
    $('.modal-body', $('#modal-popup')).load(href, function(){

    });

    return false;
  });
});
</script>
