<?php
$rrpjmd = $this->db
->where(COL_PMDISAKTIF,1)
->order_by(COL_PMDTAHUNMULAI, 'desc')
->get(TBL_SAKIPV2_PEMDA)
->row_array();
?>
<div class="carousel slide" data-ride="carousel">
  <div class="carousel-inner" role="listbox">
    <div class="carousel-item active">
        <div class="container">
          <div class="carousel-caption">
            <h2 class="font-weight-light">SELAMAT DATANG</h2>
            <h3 class="mb-0">PORTAL INFORMASI AKUNTABILITAS KINERJA INSTANSI PEMERINTAH</h3>
            <h3 class="mb-3"><?=$this->setting_org_name?></h3>
            <p>
              <a class="btn btn-lg btn-primary" href="<?=site_url('sakipv2/user/login')?>" role="button">Login <i class="fa fa-sign-in"></i></a>
            </p>
          </div>
        </div>
        <div class="header-back" style="background: url('<?=MY_IMAGEURL.'bg-overlay.png'?>') 0 0 no-repeat"></div>
    </div>
  </div>
</div>
<section class="container features">
    <div class="row">
      <div class="col-lg-12 text-center">
        <div class="navy-line"></div>
        <h1>Informasi Publik</h1>
        <p class="font-weight-bold">DOKUMEN KINERJA INSTANSI <?=$this->setting_org_name?></p>
      </div>
    </div>
    <div class="row features-block">
      <div class="col-lg-12 features-text wow fadeInLeft animated" style="visibility: visible; animation-name: fadeInLeft;">
        <div class="tabs-container">
          <ul class="nav nav-tabs" role="tablist">
            <li><a class="nav-link active show" data-toggle="tab" href="#tab-1">Perencanaan</a></li>
            <li><a class="nav-link" data-toggle="tab" href="#tab-2">Pengukuran</a></li>
            <li><a class="nav-link" data-toggle="tab" href="#tab-3">Pelaporan</a></li>
            <li><a class="nav-link" data-toggle="tab" href="#tab-4">Evaluasi</a></li>
          </ul>
          <div class="tab-content">
            <div role="tabpanel" id="tab-1" class="tab-pane active show">
              <div class="panel-body">
                <div class="project-list">
                  <table class="table table-hover">
                    <tbody>
                      <tr>
                        <td class="project-status">
                          <span class="label label-primary">1</span>
                        </td>
                        <td class="project-title">
                          <a href="<?=!empty($rrpjmd)?site_url('sakipv2/home/doc/rpjmd/'.$rrpjmd[COL_PMDID]):'#'?>">RPJMD</a><br />
                          <small>Rencana Pembangunan Jangka Menengah Daerah</small>
                        </td>
                        <td class="text-right">
                          <small class="text-dark"><?=!empty($rrpjmd)?$rrpjmd[COL_PMDTAHUNMULAI].' - '.$rrpjmd[COL_PMDTAHUNAKHIR]:'-'?></small>
                        </td>
                        <td class="project-actions">
                          <a href="<?=!empty($rrpjmd)?site_url('sakipv2/home/doc/rpjmd/'.$rrpjmd[COL_PMDID]):'#'?>" class="btn btn-white btn-sm <?=!empty($rrpjmd)?'':'disabled'?>"><i class="fa fa-search"></i> Lihat </a>
                        </td>
                      </tr>
                      <tr>
                        <td class="project-status">
                          <span class="label label-primary">2</span>
                        </td>
                        <td class="project-title">
                          <a href="<?=site_url('sakipv2/home/doc/renstra')?>">RENSTRA</a><br />
                          <small>Rencana Strategis Perangkat Daerah</small>
                        </td>
                        <td class="text-right">
                          <small class="text-dark"></small>
                        </td>
                        <td class="project-actions">
                          <a href="<?=site_url('sakipv2/home/doc/renstra')?>" class="btn btn-white btn-sm"><i class="fa fa-search"></i> Lihat </a>
                        </td>
                      </tr>
                      <tr>
                        <td class="project-status">
                          <span class="label label-primary">3</span>
                        </td>
                        <td class="project-title">
                          <a href="<?=site_url('sakipv2/home/doc/cascading')?>">Cascading</a><br />
                          <small>Penjenjangan Kinerja Perangkat Daerah</small>
                        </td>
                        <td class="text-right">
                          <small class="text-dark"></small>
                        </td>
                        <td class="project-actions">
                          <a href="<?=site_url('sakipv2/home/doc/cascading')?>" class="btn btn-white btn-sm"><i class="fa fa-search"></i> Lihat </a>
                        </td>
                      </tr>
                      <tr>
                        <td class="project-status">
                          <span class="label label-primary">4</span>
                        </td>
                        <td class="project-title">
                          <a href="<?=site_url('sakipv2/home/doc/probis')?>">Probis</a><br />
                          <small>Peta Proses Bisnis Perangkat Daerah</small>
                        </td>
                        <td class="text-right">
                          <small class="text-dark"></small>
                        </td>
                        <td class="project-actions">
                          <a href="<?=site_url('sakipv2/home/doc/probis')?>" class="btn btn-white btn-sm"><i class="fa fa-search"></i> Lihat </a>
                        </td>
                      </tr>

                      <tr>
                        <td class="project-status">
                          <span class="label label-primary">5</span>
                        </td>
                        <td class="project-title">
                          <a href="<?=site_url('sakipv2/home/doc/renja')?>">Renja</a><br />
                          <small>Rencana Kerja Perangkat Daerah</small>
                        </td>
                        <td class="text-right">
                          <small class="text-dark"></small>
                        </td>
                        <td class="project-actions">
                          <a href="<?=site_url('sakipv2/home/doc/renja')?>" class="btn btn-white btn-sm"><i class="fa fa-search"></i> Lihat </a>
                        </td>
                      </tr>
                      <tr>
                        <td class="project-status">
                          <span class="label label-primary">6</span>
                        </td>
                        <td class="project-title">
                          <a href="#">Renaksi</a><br />
                          <small>Rencana Aksi Perangkat Daerah</small>
                        </td>
                        <td class="text-right">
                          <small class="text-dark"></small>
                        </td>
                        <td class="project-actions">
                          <a href="#" class="btn btn-white btn-sm"><i class="fa fa-search"></i> Lihat </a>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
            <div role="tabpanel" id="tab-2" class="tab-pane">
                <div class="panel-body">
                  <div class="project-list">
                    <table class="table table-hover">
                      <tbody>
                        <tr>
                          <td class="project-status">
                            <span class="label label-primary">1</span>
                          </td>
                          <td class="project-title">
                            <a href="#">PEMDA</a><br />
                            <small>Monitoring dan Evaluasi Kinerja Pemerintah Daerah</small>
                          </td>
                          <td class="text-right">
                            <small class="text-dark">1 Dokumen</small>
                          </td>
                          <td class="project-actions">
                            <a href="#" class="btn btn-white btn-sm"><i class="fa fa-search"></i> Lihat </a>
                          </td>
                        </tr>
                        <tr>
                          <td class="project-status">
                            <span class="label label-primary">2</span>
                          </td>
                          <td class="project-title">
                            <a href="#">OPD</a><br />
                            <small>Monitoring dan Evaluasi Kinerja Perangkat Daerah</small>
                          </td>
                          <td class="text-right">
                            <small class="text-dark"></small>
                          </td>
                          <td class="project-actions">
                            <a href="#" class="btn btn-white btn-sm"><i class="fa fa-search"></i> Lihat </a>
                          </td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div>
            </div>
            <div role="tabpanel" id="tab-3" class="tab-pane">
              <div class="panel-body">
                <div class="project-list">
                  <table class="table table-hover">
                    <tbody>
                      <tr>
                        <td class="project-status">
                          <span class="label label-primary">1</span>
                        </td>
                        <td class="project-title">
                          <a href="#">PEMDA</a><br />
                          <small>Laporan Kinerja Instansi Pemerintah</small>
                        </td>
                        <td class="text-right">
                          <small class="text-dark">1 Dokumen</small>
                        </td>
                        <td class="project-actions">
                          <a href="#" class="btn btn-white btn-sm"><i class="fa fa-search"></i> Lihat </a>
                        </td>
                      </tr>
                      <tr>
                        <td class="project-status">
                          <span class="label label-primary">2</span>
                        </td>
                        <td class="project-title">
                          <a href="#">OPD</a><br />
                          <small>Laporan Kinerja Perangkat Daerah</small>
                        </td>
                        <td class="text-right">
                          <small class="text-dark"></small>
                        </td>
                        <td class="project-actions">
                          <a href="#" class="btn btn-white btn-sm"><i class="fa fa-search"></i> Lihat </a>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
            <div role="tabpanel" id="tab-4" class="tab-pane">
              <div class="project-list">
                <table class="table table-hover">
                  <tbody>
                    <tr>
                      <td class="project-status">
                        <span class="label label-primary">1</span>
                      </td>
                      <td class="project-title">
                        <a href="#">LKE AKIP</a><br />
                        <small>Lembar Kerja Evaluasi AKIP</small>
                      </td>
                      <td class="text-right">
                        <small class="text-dark">33 Dokumen</small>
                      </td>
                      <td class="project-actions">
                        <a href="#" class="btn btn-white btn-sm"><i class="fa fa-search"></i> Lihat </a>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
</section>
