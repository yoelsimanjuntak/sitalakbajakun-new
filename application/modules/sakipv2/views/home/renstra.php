<?php
$rskpd = $this->db
->where(COL_SKPDID, $rrenstra[COL_IDSKPD])
->get(TBL_SAKIPV2_SKPD)
->row_array();
if(empty($rskpd)) {
  $this->load->view('sakipv2/home/404');
  exit();
}

$arrIKU = array();
if(!empty($rrenstra[COL_RENSTRAIKU])) {
  $arrIKU = json_decode($rrenstra[COL_RENSTRAIKU]);
}

$rtujuan = $this->db
->where(COL_IDRENSTRA, $rrenstra[COL_RENSTRAID])
->order_by(COL_TUJUANNO)
->get(TBL_SAKIPV2_SKPD_RENSTRA_TUJUAN)
->result_array();
?>
<div class="row">
  <div class="col-lg-12">
    <div class="tabs-container">
      <ul class="nav nav-tabs" role="tablist">
        <li><a class="nav-link active show" data-toggle="tab" href="#tab-1">Rincian</a></li>
        <li><a class="nav-link" data-toggle="tab" href="#tab-2">IKU</a></li>
        <li><a class="nav-link" data-toggle="tab" href="#tab-3">Tujuan & Sasaran</a></li>
      </ul>
      <div class="tab-content">
        <div role="tabpanel" id="tab-1" class="tab-pane active show">
          <div class="panel-body">
            <h3>Rencana Strategis</h3>
            <div class="table-responsive">
              <table class="table table-striped" style="border: 1px solid #dedede">
                <tr>
                  <td>OPD</td>
                  <td class="float-right font-weight-bold"><?=$rskpd[COL_SKPDNAMA]?></td>
                </tr>
                <tr>
                  <td>Tahun Berlaku</td>
                  <td class="float-right font-weight-bold"><?=$rrenstra[COL_RENSTRATAHUN]?></td>
                </tr>
                <tr>
                  <td>Pimpinan</td>
                  <td class="float-right font-weight-bold"><?=$rskpd[COL_SKPDNAMAPIMPINAN]?></td>
                </tr>
              </table>
            </div>
            <h3>Tugas Pokok</h3>
            <p class="font-italic"><?=!empty($rrenstra[COL_RENSTRATUGASPOKOK])?$rrenstra[COL_RENSTRATUGASPOKOK]:'-'?></p>
            <h3>Fungsi</h3>
            <?php
            $arrFungsi = array();
            if(!empty($rrenstra[COL_RENSTRAFUNGSI])) {
              $arrFungsi = json_decode($rrenstra[COL_RENSTRAFUNGSI]);
            }
            if(!empty($arrFungsi)) {
              ?>
              <ul class="font-italic">
                <?php
                foreach($arrFungsi as $f) {
                  ?>
                  <li><?=$f?></li>
                  <?php
                }
                ?>
              </ul>
              <?php
            } else {
              echo '<p>-</p>';
            }
            ?>
            <a href="<?=site_url('sakipv2/home/doc/renstra')?>" class="btn btn-xs btn-primary"><i class="fa fa-arrow-circle-left"></i> KEMBALI</a>
          </div>
        </div>
        <div role="tabpanel" id="tab-2" class="tab-pane">
          <div class="panel-body">
            <h3>Indikator Kinerja Utama</h3>
            <div class="table-responsive">
              <table class="table table-striped" style="border: 1px solid #dedede">
                <thead>
                  <tr>
                    <th style="width: 10px; white-space: nowrap;">No.</th>
                    <th>Uraian</th>
                    <th>Target</th>
                    <th>Satuan</th>
                  </tr>
                </thead>
                <tbody>
                  <?php
                  if(!empty($arrIKU)) {
                    $no=1;
                    foreach($arrIKU as $r) {
                      ?>
                      <tr>
                        <td class="text-right" style="width: 10px; white-space: nowrap;"><?=$no?></td>
                        <td><?=strtoupper($r->Uraian)?></td>
                        <td class="text-right"><?=$r->Target?></td>
                        <td><?=$r->Satuan?></td>
                      </tr>
                      <?php
                      $no++;
                    }
                  } else {
                    echo '<tr><td colspan="4" class="text-center font-italic">Belum ada data tersedia.</td></tr>';
                  }
                  ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
        <div role="tabpanel" id="tab-3" class="tab-pane">
          <div class="panel-body">
            <div class="table-responsive">
              <table class="table table-striped" style="border: 1px solid #dedede">
                <thead>
                  <tr>
                    <th style="width: 10px; white-space: nowrap;">No.</th>
                    <th>Uraian</th>
                    <th>Target</th>
                    <th>Satuan</th>
                  </tr>
                </thead>
                <tbody>
                  <?php
                  if(!empty($rtujuan)) {
                    $no=1;
                    foreach($rtujuan as $r) {
                      $rsasaran = $this->db
                      ->where(COL_IDTUJUAN, $r[COL_TUJUANID])
                      ->get(TBL_SAKIPV2_SKPD_RENSTRA_SASARAN)
                      ->result_array();
                      ?>
                      <tr>
                        <td colspan="4" class="font-weight-bold"><?=strtoupper($r[COL_TUJUANURAIAN])?></td>
                      </tr>
                      <?php
                      if(!empty($rsasaran)) {
                        foreach($rsasaran as $s) {
                          $rsasarandet = $this->db
                          ->where(COL_IDSASARAN, $s[COL_SASARANID])
                          ->get(TBL_SAKIPV2_SKPD_RENSTRA_SASARANDET)
                          ->result_array();
                          foreach($rsasarandet as $sd) {
                            ?>
                            <tr>
                              <td class="font-italic text-right" style="width: 10px; white-space: nowrap; vertical-align: middle"><?=$no?></td>
                              <td class="font-italic"><?=$sd[COL_SSRINDIKATORURAIAN]?></td>
                              <td class="font-italic text-right" style="vertical-align: middle; white-space: nowrap"><?=$sd[COL_SSRINDIKATORTARGET]?></td>
                              <td class="font-italic" style="vertical-align: middle"><?=$sd[COL_SSRINDIKATORSATUAN]?></td>

                            </tr>
                            <?php
                            $no++;
                          }
                        }
                      }

                    }
                  } else {
                    echo '<tr><td colspan="4" class="text-center font-italic">Belum ada data tersedia.</td></tr>';
                  }
                  ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
