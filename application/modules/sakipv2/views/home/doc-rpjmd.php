<?php
$rrpjmd = $this->db
->where(COL_PMDID, $param)
->get(TBL_SAKIPV2_PEMDA)
->row_array();
if(empty($rrpjmd)) {
  $this->load->view('sakipv2/home/404');
  exit();
}

$rmisi = $this->db
->where(COL_IDPMD, $param)
->order_by(COL_MISINO)
->get(TBL_SAKIPV2_PEMDA_MISI)
->result_array();

$arrIKU = array();
foreach($rmisi as $r) {
  $iku = array();
  if(!empty($r[COL_MISIIKU])) {
    $iku = json_decode($r[COL_MISIIKU]);
  }
  if(!empty($iku)) {
    $arrIKU = array_merge($arrIKU,$iku);
  }
}

$rtujuan = $this->db
->where("IdMisi in (select MisiId from sakipv2_pemda_misi where IdPmd=$param)")
->order_by(COL_TUJUANNO)
->get(TBL_SAKIPV2_PEMDA_TUJUAN)
->result_array();
?>
<div class="row">
  <div class="col-lg-12">
    <div class="tabs-container">
      <ul class="nav nav-tabs" role="tablist">
        <li><a class="nav-link active show" data-toggle="tab" href="#tab-1">Rincian</a></li>
        <li><a class="nav-link" data-toggle="tab" href="#tab-2">IKU</a></li>
        <li><a class="nav-link" data-toggle="tab" href="#tab-3">Tujuan & Sasaran</a></li>
      </ul>
      <div class="tab-content">
        <div role="tabpanel" id="tab-1" class="tab-pane active show">
          <div class="panel-body">
            <h3>Periode Pemerintahan</h3>
            <div class="table-responsive">
              <table class="table table-striped" style="border: 1px solid #dedede">
                <tr>
                  <td>Status</td>
                  <td class="float-right"><?=$rrpjmd[COL_PMDISAKTIF]==1?'<span class="label label-primary">AKTIF</span></span>':'<span class="label label-danger">INAKTIF</span></span>'?></td>
                </tr>
                <tr>
                  <td>Kepala Daerah</td>
                  <td class="font-weight-bold float-right"><?=$rrpjmd[COL_PMDPEJABAT]?></td>
                </tr>
                <tr>
                  <td>Wakil Kepala Daerah</td>
                  <td class="font-weight-bold float-right"><?=$rrpjmd[COL_PMDPEJABATWAKIL]?></td>
                </tr>
                <tr>
                  <td>Periode</td>
                  <td class="font-weight-bold float-right"><?=$rrpjmd[COL_PMDTAHUNMULAI]?> - <?=$rrpjmd[COL_PMDTAHUNAKHIR]?></td>
                </tr>
                <tr>
                  <td>Visi</td>
                  <td class="font-weight-bold float-right"><?=$rrpjmd[COL_PMDVISI]?></td>
                </tr>
                <tr>
                  <td <?=!empty($rmisi)&&count($rmisi)>1?'rowspan="2"':''?>>Misi</td>
                  <?php
                  if(empty($rmisi)||count($rmisi)<=1) {
                    ?>
                    <td class="font-weight-bold float-right"><?=!empty($rmisi)?$rmisi[0][COL_MISIURAIAN]:'-'?></td>
                    <?php
                  }
                  ?>
                </tr>
                <?php
                if(!empty($rmisi)&&count($rmisi)>1) {
                  for($i=1;$i=count($rmisi);$i++) {
                    ?>
                    <tr>
                      <td class="font-weight-bold float-right"><?=$r[$i][COL_MISIURAIAN]?></td>
                    </tr>
                    <?php
                  }
                }
                ?>
              </table>
            </div>
            <a href="<?=site_url('sakipv2/home')?>" class="btn btn-xs btn-primary"><i class="fa fa-arrow-circle-left"></i> KEMBALI</a>
          </div>
        </div>
        <div role="tabpanel" id="tab-2" class="tab-pane">
          <div class="panel-body">
            <h3>Indikator Kinerja Utama</h3>
            <div class="table-responsive">
              <table class="table table-striped" style="border: 1px solid #dedede">
                <thead>
                  <tr>
                    <th style="width: 10px; white-space: nowrap;">No.</th>
                    <th>Uraian</th>
                    <th>Target</th>
                    <th>Satuan</th>
                  </tr>
                </thead>
                <tbody>
                  <?php
                  if(!empty($arrIKU)) {
                    $no=1;
                    foreach($arrIKU as $r) {
                      ?>
                      <tr>
                        <td class="text-right" style="width: 10px; white-space: nowrap;"><?=$no?></td>
                        <td><?=strtoupper($r->IKUUraian)?></td>
                        <td class="text-right"><?=$r->IKUTarget?></td>
                        <td><?=$r->IKUSatuan?></td>
                      </tr>
                      <?php
                      $no++;
                    }
                  } else {
                    echo '<tr><td colspan="4" class="text-center font-italic">Belum ada data tersedia.</td></tr>';
                  }
                  ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
        <div role="tabpanel" id="tab-3" class="tab-pane">
          <div class="panel-body">
            <div class="table-responsive">
              <table class="table table-striped" style="border: 1px solid #dedede">
                <thead>
                  <tr>
                    <th style="width: 10px; white-space: nowrap;">No.</th>
                    <th>Uraian</th>
                    <th>Target</th>
                    <th>Satuan</th>
                  </tr>
                </thead>
                <tbody>
                  <?php
                  if(!empty($rtujuan)) {
                    $no=1;
                    foreach($rtujuan as $r) {
                      $rsasaran = $this->db
                      ->where(COL_IDTUJUAN, $r[COL_TUJUANID])
                      ->get(TBL_SAKIPV2_PEMDA_SASARAN)
                      ->result_array();
                      ?>
                      <tr>
                        <td colspan="4" class="font-weight-bold"><?=strtoupper($r[COL_TUJUANURAIAN])?></td>
                      </tr>
                      <?php
                      if(!empty($rsasaran)) {
                        foreach($rsasaran as $s) {
                          $rsasarandet = $this->db
                          ->where(COL_IDSASARAN, $s[COL_SASARANID])
                          ->get(TBL_SAKIPV2_PEMDA_SASARANDET)
                          ->result_array();
                          foreach($rsasarandet as $sd) {
                            ?>
                            <tr>
                              <td class="font-italic text-right" style="width: 10px; white-space: nowrap; vertical-align: middle"><?=$no?></td>
                              <td class="font-italic"><?=$sd[COL_SSRINDIKATORURAIAN]?></td>
                              <td class="font-italic text-right" style="vertical-align: middle; white-space: nowrap"><?=$sd[COL_SSRINDIKATORTARGET]?></td>
                              <td class="font-italic" style="vertical-align: middle"><?=$sd[COL_SSRINDIKATORSATUAN]?></td>

                            </tr>
                            <?php
                            $no++;
                          }
                        }
                      }

                    }
                  } else {
                    echo '<tr><td colspan="4" class="text-center font-italic">Belum ada data tersedia.</td></tr>';
                  }
                  ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
