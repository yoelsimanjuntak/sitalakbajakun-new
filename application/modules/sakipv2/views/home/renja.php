<div class="table-responsive">
  <table class="table table-striped" style="border: 1px solid #dedede">
    <thead>
      <tr>
        <th style="width: 10px; white-space: nowrap;">Kode</th>
        <th>Uraian</th>
        <th>Target</th>
        <th>Satuan</th>
      </tr>
    </thead>
    <tbody>
      <?php
      if(!empty($rdata)) {
        $no=1;
        foreach($rdata as $r) {
          ?>
          <tr>
            <td class="text-right font-weight-bold" style="width: 10px; white-space: nowrap;"><?=$r['Kode']?></td>
            <td><strong><?=strtoupper($r['Uraian'])?></strong><br /><small class="font-italic"><?=!empty($r['Indikator'])?strtoupper($r['Indikator']):''?></small></td>
            <td class="text-right"><?=!empty($r['Target'])?strtoupper(is_numeric($r['Target'])?number_format($r['Target']):$r['Target']):''?></td>
            <td><?=!empty($r['Satuan'])?strtoupper($r['Satuan']):''?></td>
          </tr>
          <?php
          $no++;
        }
      } else {
        echo '<tr><td colspan="4" class="text-center font-italic">Belum ada data tersedia.</td></tr>';
      }
      ?>
    </tbody>
  </table>
</div>
