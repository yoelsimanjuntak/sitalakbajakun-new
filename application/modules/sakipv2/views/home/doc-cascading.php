<?php
$rrpjmd = $this->db
->where(COL_PMDISAKTIF, 1)
->order_by(COL_PMDTAHUNMULAI, 'desc')
->get(TBL_SAKIPV2_PEMDA)
->row_array();
if(empty($rrpjmd)) {
  $this->load->view('sakipv2/home/404');
  exit();
}

$rmisi = $this->db
->where(COL_IDPMD, $rrpjmd[COL_PMDID])
->get(TBL_SAKIPV2_PEMDA_MISI)
->result_array();

$rpemda = $this->db
->where(COL_PMDID, $rrpjmd[COL_PMDID])
->get(TBL_SAKIPV2_PEMDA)
->row_array();

$arrMisi = array();
foreach($rmisi as $m) {
    $arrKabTujuan = array();
    $this->db->join(TBL_SAKIPV2_PEMDA_MISI,TBL_SAKIPV2_PEMDA_MISI.'.'.COL_MISIID." = ".TBL_SAKIPV2_PEMDA_TUJUAN.".".COL_IDMISI,"inner");
    $this->db->where(TBL_SAKIPV2_PEMDA_TUJUAN.'.'.COL_IDMISI, $m[COL_MISIID]);
    $this->db->order_by(TBL_SAKIPV2_PEMDA_TUJUAN.".".COL_TUJUANNO, 'asc');
    $rkab_tujuan = $this->db->get(TBL_SAKIPV2_PEMDA_TUJUAN)->result_array();

    foreach($rkab_tujuan as $tujuankab) {
        $arrKabIkTujuan = array();
        $kab_iktujuan = $this->db
            ->where(COL_IDTUJUAN, $tujuankab[COL_TUJUANID])
            ->get(TBL_SAKIPV2_PEMDA_TUJUANDET)
            ->result_array();

        $arrKabSasaran = array();
        $kab_sasaran = $this->db
            ->join(TBL_SAKIPV2_PEMDA_TUJUAN,TBL_SAKIPV2_PEMDA_TUJUAN.'.'.COL_TUJUANID." = ".TBL_SAKIPV2_PEMDA_SASARAN.".".COL_IDTUJUAN,"inner")
            ->join(TBL_SAKIPV2_PEMDA_MISI,TBL_SAKIPV2_PEMDA_MISI.'.'.COL_MISIID." = ".TBL_SAKIPV2_PEMDA_TUJUAN.".".COL_IDMISI,"inner")
            ->where(TBL_SAKIPV2_PEMDA_SASARAN.'.'.COL_IDTUJUAN, $tujuankab[COL_TUJUANID])
            ->order_by(TBL_SAKIPV2_PEMDA_SASARAN.".".COL_SASARANNO, 'asc')
            ->get(TBL_SAKIPV2_PEMDA_SASARAN)
            ->result_array();

        foreach($kab_sasaran as $skab) {
            $arrKabIkSasaran = array();
            $kab_iksasaran = $this->db
                ->where(COL_IDSASARAN, $skab[COL_SASARANID])
                ->get(TBL_SAKIPV2_PEMDA_SASARANDET)
                ->result_array();

            $arrSasaranOPD = array();
            $sasaranOPD = $this->db
            ->join(TBL_SAKIPV2_PEMDA_SASARAN,TBL_SAKIPV2_PEMDA_SASARAN.'.'.COL_SASARANID." = ".TBL_SAKIPV2_SKPD_RENSTRA_SASARAN.".".COL_IDSASARANPMD,"inner")
            ->join(TBL_SAKIPV2_SKPD_RENSTRA_TUJUAN,TBL_SAKIPV2_SKPD_RENSTRA_TUJUAN.'.'.COL_TUJUANID." = ".TBL_SAKIPV2_SKPD_RENSTRA_SASARAN.".".COL_IDTUJUAN,"inner")
            ->join(TBL_SAKIPV2_SKPD_RENSTRA,TBL_SAKIPV2_SKPD_RENSTRA.'.'.COL_RENSTRAID." = ".TBL_SAKIPV2_SKPD_RENSTRA_TUJUAN.".".COL_IDRENSTRA,"inner")
            ->join(TBL_SAKIPV2_SKPD,TBL_SAKIPV2_SKPD.'.'.COL_SKPDID." = ".TBL_SAKIPV2_SKPD_RENSTRA.".".COL_IDSKPD,"inner")
            ->where(TBL_SAKIPV2_SKPD_RENSTRA_TUJUAN.'.'.COL_IDTUJUANPMD, $tujuankab[COL_TUJUANID])
            ->where(TBL_SAKIPV2_SKPD_RENSTRA_SASARAN.'.'.COL_IDSASARANPMD, $skab[COL_SASARANID])
            ->where(TBL_SAKIPV2_SKPD_RENSTRA.'.'.COL_RENSTRAISAKTIF, 1)
            ->group_by(TBL_SAKIPV2_SKPD.'.'.COL_SKPDID)
            ->get(TBL_SAKIPV2_SKPD_RENSTRA_SASARAN)
            ->result_array();

            foreach($sasaranOPD as $i_) {
              $arrSasaranOPD[] = array(
                "text" => $i_[COL_SKPDNAMA],
                "icon" => "far fa-building",
              );
            }

            $arrKabSasaran[] = array(
              "text"=>"SASARAN ".$skab[COL_MISINO].".".$skab[COL_TUJUANNO].".".$skab[COL_SASARANNO].": ".strtoupper($skab[COL_SASARANURAIAN]),
              "icon" => "far fa-check-circle",
              "children" => $arrSasaranOPD,
            );
        }

        $arrKabTujuan[] = array(
          "text"=>"TUJUAN ".$tujuankab[COL_MISINO].".".$tujuankab[COL_TUJUANNO].": ".strtoupper($tujuankab[COL_TUJUANURAIAN]),
          "icon" => "far fa-check-circle",
          "children" => $arrKabSasaran
        );
    }

    $arrMisi[] = array(
      "text" => "MISI ".$m[COL_MISINO].": ".strtoupper($m[COL_MISIURAIAN]),
      "icon" => "far fa-check-circle",
      "children" => $arrKabTujuan
    );
}
$nodes = array(
  "text" => "VISI: ".$rpemda[COL_PMDVISI],
  "icon" => "far fa-check-circle",
  "children" => $arrMisi,
);
?>
<link href="<?=base_url()?>assets/themes/inspinia/css/plugins/jsTree/style.min.css" rel="stylesheet">
<div class="row">
  <div class="col-lg-12">
    <div class="ibox">
      <div class="ibox-title">
        <h5><?=$title?></h5>
        <div class="ibox-tools">
          <a href="<?=site_url('sakipv2/home/')?>"><i class="fa fa-arrow-left"></i> KEMBALI</a>
        </div>
      </div>
      <div class="ibox-content">
        <table class="table table-striped" style="border: 1px solid #dedede">
          <tr>
            <td>Periode</td><td style="width: 10px; white-space">:</td>
            <td class="font-weight-bold"><?=$rrpjmd[COL_PMDTAHUNMULAI].' s.d '.$rrpjmd[COL_PMDTAHUNAKHIR]?></td>
          </tr>
          <tr>
            <td>Kepala Daerah</td><td style="width: 10px; white-space">:</td>
            <td class="font-weight-bold"><?=$rrpjmd[COL_PMDPEJABAT].(!empty($rrpjmd[COL_PMDPEJABATWAKIL])?' & '.$rrpjmd[COL_PMDPEJABATWAKIL]:'')?></td>
          </tr>
        </table>
        <div id="cascading"></div>
      </div>
    </div>
  </div>
</div>
<script src="<?=base_url()?>assets/themes/inspinia/js/plugins/jsTree/jstree.min.js"></script>
<script type="text/javascript">
$('#cascading').jstree({
  'core' : {
    'plugins' : [ 'types', 'dnd' ],
    'data' : <?=json_encode($nodes)?>
  }});
</script>
