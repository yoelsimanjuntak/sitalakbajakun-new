<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>E-SAKIP - 404 Halaman Tidak Ditemukan</title>
  <link href="<?=base_url()?>assets/themes/inspinia/css/bootstrap.min.css" rel="stylesheet">
  <link href="<?=base_url()?>assets/themes/inspinia/font-awesome/css/font-awesome.css" rel="stylesheet">
  <link href="<?=base_url()?>assets/themes/inspinia/css/animate.css" rel="stylesheet">
  <link href="<?=base_url()?>assets/themes/inspinia/css/style.css" rel="stylesheet">
</head>

<body class="gray-bg">
  <div class="middle-box text-center animated fadeInDown">
    <h1>404</h1>
    <h3 class="font-bold">Halaman Tidak Ditemukan</h3>
    <div class="error-desc">
      Maaf, halaman yang anda tuju tidak ditemukan / belum tersedia. Silakan kontak Administrator untuk informasi lebih lanjut.
      <p class="mt-4">
        <a href="<?=site_url('sakipv2/home')?>">Kembali ke Beranda</a>
      </p>
    </div>
  </div>
  <script src="<?=base_url()?>assets/themes/inspinia/js/jquery-3.1.1.min.js"></script>
  <script src="<?=base_url()?>assets/themes/inspinia/js/popper.min.js"></script>
  <script src="<?=base_url()?>assets/themes/inspinia/js/bootstrap.js"></script>
</body>

</html>
