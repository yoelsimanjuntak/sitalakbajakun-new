
<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="">
  <meta name="author" content="">
  <title><?=!empty($title) ? 'E-SAKIP - '.$title : 'E-SAKIP'?></title>

  <link rel="icon" type="image/png" href=<?=MY_IMAGEURL.$this->setting_web_logo?>>
  <link href="<?=base_url()?>assets/themes/inspinia/css/bootstrap.min.css" rel="stylesheet">
  <link href="<?=base_url()?>assets/themes/inspinia/css/animate.css" rel="stylesheet">
  <link rel="stylesheet" href="<?=base_url()?>assets/tbs/css/font-awesome.min.css" />
  <link rel="stylesheet" href="<?=base_url()?>assets/tbs/fontawesome-pro/web/css/all.min.css" />
  <link href="<?=base_url()?>assets/themes/inspinia/css/plugins/dataTables/datatables.min.css" rel="stylesheet">
  <link href="<?=base_url()?>assets/themes/inspinia/css/style.css" rel="stylesheet">

  <script src="<?=base_url()?>assets/themes/inspinia/js/jquery-3.1.1.min.js"></script>
</head>

<body class="top-navigation">
  <div id="wrapper">
    <div id="page-wrapper" class="gray-bg">
      <div class="row border-bottom white-bg">
        <nav class="navbar navbar-expand-lg navbar-static-top" role="navigation">
          <a href="<?=site_url('sakipv2/home/index')?>" class="navbar-brand">E-SAKIP</a>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-label="Toggle navigation">
              <i class="fa fa-reorder"></i>
          </button>
          <div class="navbar-collapse collapse" id="navbar">
            <ul class="nav navbar-top-links navbar-right ml-auto">
              <li>
                <a href="<?=site_url('sakipv2/home/')?>">
                  <i class="fa fa-home"></i> Beranda
                </a>
              </li>
            </ul>
          </div>
        </nav>
      </div>
    <div class="wrapper wrapper-content">
      <div class="container pb-2">
        <?=$content?>
      </div>
    </div>
    <div class="footer">
      <div>
        Copyright &copy; <?=date("Y")?> <strong><?=$this->setting_web_name?> <?=$this->setting_web_version?></strong>
      </div>
    </div>
    </div>
  </div>

  <script src="<?=base_url()?>assets/themes/inspinia/js/popper.min.js"></script>
  <script src="<?=base_url()?>assets/themes/inspinia/js/bootstrap.js"></script>
  <script src="<?=base_url()?>assets/themes/inspinia/js/plugins/metisMenu/jquery.metisMenu.js"></script>
  <script src="<?=base_url()?>assets/themes/inspinia/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
  <script src="<?=base_url()?>assets/themes/inspinia/js/inspinia.js"></script>
  <script src="<?=base_url()?>assets/themes/inspinia/js/plugins/pace/pace.min.js"></script>
  <script src="<?=base_url()?>assets/themes/inspinia/js/plugins/wow/wow.min.js"></script>
  <script src="<?=base_url()?>assets/themes/inspinia/js/plugins/dataTables/datatables.min.js"></script>
  <script src="<?=base_url()?>assets/themes/inspinia/js/plugins/dataTables/dataTables.bootstrap4.min.js"></script>
</body>
</html>
