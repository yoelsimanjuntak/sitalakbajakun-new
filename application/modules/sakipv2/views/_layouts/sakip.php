<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="">
  <meta name="author" content="">

  <title><?=!empty($title) ? 'E-SAKIP - '.$title : 'E-SAKIP'?></title>

  <link rel="icon" type="image/png" href=<?=MY_IMAGEURL.$this->setting_web_logo?>>
  <link href="<?=base_url()?>assets/themes/inspinia/css/bootstrap.min.css" rel="stylesheet">
  <link href="<?=base_url()?>assets/themes/inspinia/css/animate.css" rel="stylesheet">
  <link rel="stylesheet" href="<?=base_url()?>assets/tbs/css/font-awesome.min.css" />
  <link rel="stylesheet" href="<?=base_url()?>assets/tbs/fontawesome-pro/web/css/all.min.css" />

  <link href="<?=base_url()?>assets/themes/inspinia/css/plugins/dataTables/datatables.min.css" rel="stylesheet">

  <link href="<?=base_url()?>assets/themes/inspinia/css/style.css" rel="stylesheet">

  <script src="<?=base_url()?>assets/themes/inspinia/js/jquery-3.1.1.min.js"></script>
  <style>
  .se-pre-con {
      position: fixed;
      left: 0px;
      top: 0px;
      width: 100%;
      height: 100%;
      z-index: 9999;
      background: url(<?=base_url()?>assets/preloader/images/<?=$this->setting_web_preloader?>) center no-repeat #fff;
  }
  </style>
  <script>
  window.onload = function() {
    $(".se-pre-con").fadeOut("slow");
  };
  </script>
</head>
<body id="page-top" class="landing-page no-skin-config">
  <div class="se-pre-con"></div>
  <div class="navbar-wrapper">
    <nav class="navbar navbar-default navbar-fixed-top navbar-expand-md" role="navigation">
      <div class="container">
        <a class="navbar-brand" href="<?=site_url('site/home/index')?>">E-SAKIP</a>
        <div class="navbar-header page-scroll">
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar">
            <i class="fa fa-bars"></i>
          </button>
        </div>
        <div class="collapse navbar-collapse justify-content-end" id="navbar">
          <ul class="nav navbar-nav navbar-right">
            <li><a class="nav-link page-scroll" href="#page-top">Beranda</a></li>
            <li><a class="nav-link page-scroll" href="#guide">Panduan</a></li>
            <li><a class="nav-link" href="<?=site_url('sakipv2/user/login')?>">Login <i class="fa fa-sign-in"></i></a></li>
          </ul>
        </div>
      </div>
    </nav>
  </div>
  <?=$content?>
  <section id="contact" class="gray-section contact">
    <div class="container">
      <div class="row m-b-lg">
        <div class="col-lg-12 text-center">
          <div class="navy-line"></div>
          <h1>Kontak</h1>
        </div>
      </div>
      <div class="row m-b-lg justify-content-center">
        <div class="col-lg-4 text-right">
          <img src="<?=MY_IMAGEURL.'logo.png'?>" width="80" style="margin-bottom: 10px">
        </div>
        <div class="col-lg-4">
          <address>
            <strong><span class="navy">Bagian Organisasi<br />Sekretariat Daerah Kota Tebing Tinggi</span></strong><br>
            <?=$this->setting_org_address?>
          </address>
        </div>
        </div>
    </div>
</section>
<script src="<?=base_url()?>assets/themes/inspinia/js/popper.min.js"></script>
<script src="<?=base_url()?>assets/themes/inspinia/js/bootstrap.js"></script>
<script src="<?=base_url()?>assets/themes/inspinia/js/plugins/metisMenu/jquery.metisMenu.js"></script>
<script src="<?=base_url()?>assets/themes/inspinia/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
<script src="<?=base_url()?>assets/themes/inspinia/js/inspinia.js"></script>
<script src="<?=base_url()?>assets/themes/inspinia/js/plugins/pace/pace.min.js"></script>
<script src="<?=base_url()?>assets/themes/inspinia/js/plugins/wow/wow.min.js"></script>
<script src="<?=base_url()?>assets/themes/inspinia/js/plugins/dataTables/datatables.min.js"></script>
<script src="<?=base_url()?>assets/themes/inspinia/js/plugins/dataTables/dataTables.bootstrap4.min.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
      $('body').scrollspy({
        target: '#navbar',
        offset: 80
      });

      // Page scrolling feature
      $('a.page-scroll').bind('click', function(event) {
        var link = $(this);
        $('html, body').stop().animate({
            scrollTop: $(link.attr('href')).offset().top - 50
        }, 500);
        event.preventDefault();
        $("#navbar").collapse('hide');
      });
    });

    var cbpAnimatedHeader = (function() {
      var docElem = document.documentElement, header = document.querySelector( '.navbar-default' ), didScroll = false, changeHeaderOn = 200;
      function init() {
        window.addEventListener( 'scroll', function( event ) {
          if( !didScroll ) {
            didScroll = true;
            setTimeout( scrollPage, 250 );
          }
        }, false);
      }
      function scrollPage() {
        var sy = scrollY();
        if (sy >= changeHeaderOn) {
          $(header).addClass('navbar-scroll')
        }
        else {
          $(header).removeClass('navbar-scroll')
        }
        didScroll = false;
      }
      function scrollY() {
        return window.pageYOffset || docElem.scrollTop;
      }
      init();
    })();
    new WOW().init();
</script>
</body>
