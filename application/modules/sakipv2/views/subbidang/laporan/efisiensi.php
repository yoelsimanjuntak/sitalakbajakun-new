<?php
if(isset($cetak)) {
  header("Content-type: application/vnd-ms-excel");
  header("Content-Disposition: attachment; filename=Tabel Efisiensi ".date('YmdHi').".xls");
}
?>
<style>
td.num {
  mso-number-format:"0.0000";
}
</style>
<table class="table table-bordered">
  <thead>
    <tr>
      <th>SASARAN</th>
      <th>INDIKATOR</th>
      <th>SATUAN</th>
      <th>TARGET</th>
      <th>REALISASI</th>
      <th>PAGU</th>
      <th>REALISASI</th>
      <th>TARGET<br />GABUNGAN</th>
      <th>REALISASI<br />GABUNGAN</th>
      <th>CKK</th>
      <th>SEHARUSNYA</th>
      <th>GAP</th>
      <th>EFF</th>
    </tr>
  </thead>
  <tbody>
    <?php
    foreach($rSasaran as $s) {
      $arrSasaranTarget = array();
      $arrSasaranRealisasi = array();

      $arrSubkegCapaian = array();
      $errNumeric = '';

      $rmonev = $this->db
      ->where('IdSasaranIndikator in (select SsrIndikatorId from sakipv2_skpd_renstra_sasarandet where IdSasaran='.$s[COL_SASARANID].')')
      ->where(COL_MONEVTAHUN, $rdpa[COL_DPATAHUN])
      ->order_by(COL_UNIQ, 'desc')
      ->get(TBL_SAKIPV2_SKPD_RENSTRA_SASARANMONEV)
      ->result_array();

      $rprogram = $this->db
      ->where(COL_IDSASARANSKPD, $s[COL_SASARANID])
      ->where(COL_IDDPA, $getDPA)
      ->get(TBL_SAKIPV2_BID_PROGRAM)
      ->result_array();

      foreach($rprogram as $p) {
        $rkegiatan = $this->db
        ->where(COL_IDPROGRAM, $p[COL_PROGRAMID])
        ->get(TBL_SAKIPV2_BID_KEGIATAN)
        ->result_array();
        foreach($rkegiatan as $k) {
          $rkegiatansub = $this->db
          ->where(COL_IDKEGIATAN, $k[COL_KEGIATANID])
          ->get(TBL_SAKIPV2_SUBBID_SUBKEGIATAN)
          ->result_array();

          foreach($rkegiatansub as $sk) {
            $rsubkegsasaran = $this->db
            ->where(COL_IDSUBKEG, $sk[COL_SUBKEGID])
            ->get(TBL_SAKIPV2_SUBBID_SUBKEGSASARAN)
            ->result_array();
            foreach($rsubkegsasaran as $sks) {
              if(!is_numeric($sks[COL_SASARANTARGET]) || !is_numeric($sks[COL_SASARANREALISASI])) {
                $arrSubkegCapaian=array();
                $errNumeric=$sks[COL_SASARANTARGET].'/'.$sks[COL_SASARANREALISASI];
                break;
              }

              if(floatval($sks[COL_SASARANTARGET])>0 && floatval($sks[COL_SASARANREALISASI])>0) {
                $arrSubkegCapaian[] = floatval($sks[COL_SASARANREALISASI])/floatval($sks[COL_SASARANTARGET])*100;
              }
            }
          }
        }
      }
      foreach($rmonev as $r) {
        if(!is_numeric($r[COL_MONEVTARGET]) || !is_numeric($r[COL_MONEVREALISASI])) {
          $arrSasaranTarget=array();
          $arrSasaranRealisasi = array();
          break;
        }

        if(floatval($r[COL_MONEVTARGET])>0 && floatval($r[COL_MONEVREALISASI])>0) {
          $arrSasaranTarget[] = floatval($r[COL_MONEVTARGET]);
          $arrSasaranRealisasi[] = floatval($r[COL_MONEVREALISASI]);
        }

      }

      $rdet = $this->db
      ->where(COL_IDSASARAN, $s[COL_SASARANID])
      ->get(TBL_SAKIPV2_SKPD_RENSTRA_SASARANDET)
      ->result_array();
      $rowspan = count($rdet);

      $ckk = '-';
      if (empty($arrSasaranTarget) || empty($arrSasaranRealisasi)) {
        $ckk = 'ERROR : TARGET / REALISASI SASARAN';
      } else if (empty($arrSubkegCapaian)) {
        $ckk = 'ERROR : CAPAIAN SUB. KEGIATAN'.(!empty($errNumeric)?' (NUMERIC: '.$errNumeric.')':'');
      } else {
        $ckk = geomean($arrSasaranRealisasi)/geomean($arrSasaranTarget)*geomean($arrSubkegCapaian);
      }

      $ideal = '-';
      $gap = '-';
      $eff = '-';
      if(is_numeric($ckk)) {
        $ideal = $s['SasaranPagu']*$ckk/100;
        $gap = $ideal-$s['SasaranPaguRealisasi'];
        if($ideal>0) {
          $eff = $gap/$ideal*100;
        }

      }
      ?>
      <tr>
        <td <?=$rowspan>1?'rowspan="'.$rowspan.'"':''?> style="vertical-align: middle"><?=strtoupper($s[COL_SASARANURAIAN])?></td>
        <?php
        if(!empty($rdet)) {
          $rmonev = $this->db
          ->where(COL_IDSASARANINDIKATOR, $rdet[0][COL_SSRINDIKATORID])
          ->where(COL_MONEVTAHUN, $rdpa[COL_DPATAHUN])
          ->order_by(COL_UNIQ, 'desc')
          ->get(TBL_SAKIPV2_SKPD_RENSTRA_SASARANMONEV)
          ->row_array();
          ?>
          <td style="vertical-align: top;"><?=strtoupper($rdet[0][COL_SSRINDIKATORURAIAN])?></td>
          <td style="vertical-align: middle; white-space: nowrap"><?=strtoupper($rdet[0][COL_SSRINDIKATORSATUAN])?></td>
          <td class="num" style="text-align: center; vertical-align: middle"><?=isset($cetak)?'\'':''?><?=!empty($rmonev)?strtoupper($rmonev[COL_MONEVTARGET]):'-'?></td>
          <td class="num" style="text-align: center; vertical-align: middle"><?=isset($cetak)?'\'':''?><?=!empty($rmonev)?strtoupper($rmonev[COL_MONEVREALISASI]):'-'?></td>
          <?php
        } else {

        }
        ?>
        <td <?=$rowspan>1?'rowspan="'.$rowspan.'"':''?> style="vertical-align: top; text-align: right; white-space: nowrap">Rp. <?=number_format($s['SasaranPagu'])?></td>
        <td <?=$rowspan>1?'rowspan="'.$rowspan.'"':''?> style="vertical-align: top; text-align: right; white-space: nowrap">Rp. <?=number_format($s['SasaranPaguRealisasi'])?></td>
        <td <?=$rowspan>1?'rowspan="'.$rowspan.'"':''?> class="num" style="text-align: right; vertical-align: top"><?=isset($cetak)?'\'':''?><?=!empty($arrSasaranTarget)?number_format(geomean($arrSasaranTarget),2):'??'?></td>
        <td <?=$rowspan>1?'rowspan="'.$rowspan.'"':''?> class="num" style="text-align: right; vertical-align: top"><?=isset($cetak)?'\'':''?><?=!empty($arrSasaranRealisasi)?number_format(geomean($arrSasaranRealisasi),2):'??'?></td>
        <td <?=$rowspan>1?'rowspan="'.$rowspan.'"':''?> class="num" style="text-align: right; vertical-align: top"><?=isset($cetak)?'\'':''?><?=is_numeric($ckk)?number_format($ckk,4):$ckk?></td>
        <td <?=$rowspan>1?'rowspan="'.$rowspan.'"':''?> class="num" style="text-align: right; vertical-align: top"><?=isset($cetak)?'\'':''?><?=is_numeric($ideal)?number_format($ideal,0):$ideal?></td>
        <td <?=$rowspan>1?'rowspan="'.$rowspan.'"':''?> class="num" style="text-align: right; vertical-align: top"><?=isset($cetak)?'\'':''?><?=is_numeric($gap)?number_format($gap,0):$gap?></td>
        <td <?=$rowspan>1?'rowspan="'.$rowspan.'"':''?> class="num" style="text-align: right; vertical-align: top"><?=isset($cetak)?'\'':''?><?=is_numeric($eff)?number_format($eff,4):$eff?></td>
      </tr>
      <?php
      if(!empty($rdet)&&count($rdet)>1) {
        for($i=1; $i<count($rdet); $i++) {
          $rmonev = $this->db
          ->where(COL_IDSASARANINDIKATOR, $rdet[$i][COL_SSRINDIKATORID])
          ->where(COL_MONEVTAHUN, $rdpa[COL_DPATAHUN])
          ->order_by(COL_UNIQ, 'desc')
          ->get(TBL_SAKIPV2_SKPD_RENSTRA_SASARANMONEV)
          ->row_array();
          ?>
          <tr>
            <td style="vertical-align: top"><?=strtoupper($rdet[$i][COL_SSRINDIKATORURAIAN])?></td>
            <td style="vertical-align: middle; white-space: nowrap"><?=strtoupper($rdet[$i][COL_SSRINDIKATORSATUAN])?></td>
            <td class="num" style="text-align: center; vertical-align: middle"><?=isset($cetak)?'\'':''?><?=!empty($rmonev)?strtoupper($rmonev[COL_MONEVTARGET]):'-'?></td>
            <td class="num" style="text-align: center; vertical-align: middle"><?=isset($cetak)?'\'':''?><?=!empty($rmonev)?strtoupper($rmonev[COL_MONEVREALISASI]):'-'?></td>
          </tr>

          <?php
        }
      }
    }
    ?>
  </tbody>
</table>
