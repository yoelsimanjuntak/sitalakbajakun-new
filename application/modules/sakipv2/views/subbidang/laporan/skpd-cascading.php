<?php
$arrMisi = array();
$arrTujuan = array();
$rskpd = $this->db
->where(COL_SKPDID, $idSKPD)
->get(TBL_SAKIPV2_SKPD)
->row_array();

$rtujuan = $this->db
->join(TBL_SAKIPV2_SKPD_RENSTRA,TBL_SAKIPV2_SKPD_RENSTRA.'.'.COL_RENSTRAID." = ".TBL_SAKIPV2_SKPD_RENSTRA_TUJUAN.".".COL_IDRENSTRA,"inner")
->where(COL_RENSTRAID, $idRenstra)
->order_by(COL_TUJUANNO)
->get(TBL_SAKIPV2_SKPD_RENSTRA_TUJUAN)
->result_array();
foreach($rtujuan as $t) {
    $arrSasaran = array();
    $indikator = $this->db
    ->where(COL_IDTUJUAN, $t[COL_TUJUANID])
    ->get(TBL_SAKIPV2_SKPD_RENSTRA_TUJUANDET)
    ->result_array();

    $sasaran = $this->db
    ->where(COL_IDTUJUAN, $t[COL_TUJUANID])
    ->order_by(COL_SASARANNO)
    ->get(TBL_SAKIPV2_SKPD_RENSTRA_SASARAN)
    ->result_array();

    foreach($sasaran as $s) {
        $arrProgramOPD = array();

        $iksasaran = $this->db
        ->where(COL_IDSASARAN, $s[COL_SASARANID])
        ->get(TBL_SAKIPV2_SKPD_RENSTRA_SASARANDET)
        ->result_array();

        $arrProgram = array();
        $program = $this->db
        ->where(COL_IDSASARANSKPD, $s[COL_SASARANID])
        ->where(COL_IDDPA, $idDPA)
        ->order_by(COL_PROGRAMKODE)
        ->get(TBL_SAKIPV2_BID_PROGRAM)
        ->result_array();

        foreach($program as $p) {
          $arrKegiatanOPD = array();
          $indProgramOPD = $this->db
          ->where(COL_IDPROGRAM, $p[COL_PROGRAMID])
          ->order_by(COL_SASARANNO)
          ->get(TBL_SAKIPV2_BID_PROGSASARAN)
          ->result_array();

          $kegiatan = $this->db
          ->where(COL_IDPROGRAM, $p[COL_PROGRAMID])
          ->order_by(COL_KEGIATANKODE)
          ->get(TBL_SAKIPV2_BID_KEGIATAN)
          ->result_array();

          foreach($kegiatan as $keg) {
            $arrSubKegiatanOPD = array();
            $indKegiatanOPD = $this->db
            ->where(COL_IDKEGIATAN, $keg[COL_KEGIATANID])
            ->order_by(COL_SASARANNO)
            ->get(TBL_SAKIPV2_BID_KEGSASARAN)
            ->result_array();

            $subkegiatan = $this->db
            ->where(COL_IDKEGIATAN, $keg[COL_KEGIATANID])
            ->order_by(COL_SUBKEGKODE)
            ->get(TBL_SAKIPV2_SUBBID_SUBKEGIATAN)
            ->result_array();

            $htmlKegiatanOPD = "<p class='node-name'>".strtoupper($keg[COL_KEGIATANURAIAN])."</p>";
            $ikkegiatan_ = "";
            if(count($indKegiatanOPD) > 0) {
                $ikkegiatan_ .= "<p class='node-title' style='margin-bottom: 0px;'>INDIKATOR :</p><ul style='padding-left: 2rem'>";
                foreach($indKegiatanOPD as $ikp) {
                  $ikkegiatan_ .= "<li>".strtoupper($ikp[COL_SASARANINDIKATOR])."</li>";
                }
                $ikkegiatan_ .= "</ul>";
            } else {
              $htmlKegiatanOPD .= '<p class="node-title" style="font-style: italic; margin-bottom: 0; text-align: center">(KOSONG)</p>';
            }
            $htmlKegiatanOPD .= $ikkegiatan_;

            foreach($subkegiatan as $subkeg) {
              $indSubKegiatanOPD = $this->db
              ->where(COL_IDSUBKEG, $subkeg[COL_SUBKEGID])
              ->order_by(COL_SASARANNO)
              ->get(TBL_SAKIPV2_SUBBID_SUBKEGSASARAN)
              ->result_array();

              $htmlSubKegiatanOPD = "<p class='node-name'>".strtoupper($subkeg[COL_SUBKEGURAIAN])."</p>";
              $iksubkegiatan_ = "";
              if(count($indSubKegiatanOPD) > 0) {
                  $iksubkegiatan_ .= "<p class='node-title' style='margin-bottom: 0px;'>INDIKATOR :</p><ul style='padding-left: 2rem'>";
                  foreach($indSubKegiatanOPD as $ikp) {
                    $iksubkegiatan_ .= "<li>".strtoupper($ikp[COL_SASARANINDIKATOR])."</li>";
                  }
                  $ikkegiatan_ .= "</ul>";
              } else {
                $htmlSubKegiatanOPD .= '<p class="node-title" style="font-style: italic; margin-bottom: 0; text-align: center">(KOSONG)</p>';
              }
              $htmlSubKegiatanOPD .= $iksubkegiatan_;

              $arrSubKegiatanOPD[] = array(
                "innerHTML" => $htmlSubKegiatanOPD,
                "connectors" => array("style" => array("stroke" => "#000", "arrow-end" => "block-wide-long")),
              );

            }

            /*if(!empty($keg[COL_NM_ARRSUBKEGIATAN])) {
              $arrSubKeg_ = json_decode($keg[COL_NM_ARRSUBKEGIATAN]);
              foreach ($arrSubKeg_ as $subkeg) {
                $htmlSubKegiatanOPD = "<p class='node-name'>".(!empty($subkeg->NmSubKegiatan)?$subkeg->NmSubKegiatan:'-')."</p>";
                if(!empty($subkeg->NmIndikatorSubKegiatan)) {
                  $htmlSubKegiatanOPD .= "<p class='node-title' style='margin-bottom: 0px;'>INDIKATOR: <ul style='padding-left: 2rem'><li>".$subkeg->NmIndikatorSubKegiatan."</li></ul></p>";
                }
                $arrSubKegiatanOPD[] = array(
                  "innerHTML" => $htmlSubKegiatanOPD,
                  "connectors" => array("style" => array("stroke" => "#000", "arrow-end" => "block-wide-long")),
                );
              }
            }*/

            $arrKegiatanOPD[] = array(
              "innerHTML" => $htmlKegiatanOPD,
              "connectors" => array("style" => array("stroke" => "#000", "arrow-end" => "block-wide-long")),
              "children" => $arrSubKegiatanOPD,
              "HTMLclass" => "bg-fuchsia"
            );
          }

          $htmlProgramOPD = "<p class='node-name'>".strtoupper($p[COL_PROGRAMURAIAN])."</p>";
          if(count($indProgramOPD) > 0) {
            $htmlProgramOPD .= "<p class='node-title' style='margin-bottom: 0px;'>INDIKATOR :</p><ul style='padding-left: 2rem'>";
            foreach($indProgramOPD as $i_) {
              $htmlProgramOPD .= "<li>".strtoupper($i_[COL_SASARANINDIKATOR])."</li>";
            }
            $htmlProgramOPD .= "</ul>";
          } else {
            $htmlProgramOPD .= '<p class="node-title" style="font-style: italic; margin-bottom: 0; text-align: center">(KOSONG)</p>';
          }
          $arrProgramOPD[] = array(
            "innerHTML" => $htmlProgramOPD,
            "connectors" => array("style" => array("stroke" => "#000", "arrow-end" => "block-wide-long")),
            "children" => $arrKegiatanOPD,
            "HTMLclass" => "bg-orange"
          );
        }

        $htmlSasaran = "<p class='node-name'>".strtoupper($s[COL_SASARANURAIAN])."</p>";
        if(count($iksasaran) > 0) {
          $htmlSasaran .= "<p class='node-title' style='margin-bottom: 0px;'>INDIKATOR :</p><ul style='padding-left: 2rem'>";
          foreach($iksasaran as $i_) {
            $htmlSasaran .= "<li>".strtoupper($i_[COL_SSRINDIKATORURAIAN])."</li>";
          }
          $htmlSasaran .= "</ul>";
        } else {
          $htmlSasaran .= '<p class="node-title" style="font-style: italic; margin-bottom: 0; text-align: center">(KOSONG)</p>';
        }
        $arrSasaran[] = array(
            //"text" => array("name"=> $s[COL_KD_TUJUANOPD].".".$s[COL_KD_SASARANOPD].". Sasaran OPD (Eselon 2)", "title"=> $s[COL_NM_SASARANOPD]),
            "innerHTML" => $htmlSasaran,
            "connectors" => array("style" => array("stroke" => "#000", "arrow-end" => "block-wide-long")),
            "children" => $arrProgramOPD,
            "HTMLclass" => "bg-lime"
        );
    }

    $htmlTujuan = "<p class='node-name'>".strtoupper($t[COL_TUJUANURAIAN])."</p>";
    if(count($indikator) > 0) {
      $htmlTujuan .= "<p class='node-title' style='margin-bottom: 0px;'>INDIKATOR :</p><ul style='padding-left: 2rem'>";
      foreach($indikator as $i_) {
        $htmlTujuan .= "<li>".strtoupper($i_[COL_TUJINDIKATORURAIAN])."</li>";
      }
      $htmlTujuan .= "</ul>";
    } else {
      $htmlTujuan .= '<p class="node-title" style="font-style: italic; margin-bottom: 0; text-align: center">(KOSONG)</p>';
    }
    $arrTujuan[] = array(
        //"text" => array("name"=> "Tujuan OPD", "title"=> $t[COL_KD_TUJUANOPD].'. '.$t[COL_NM_TUJUANOPD]),
        "innerHTML" => $htmlTujuan,
        "connectors" => array("style" => array("stroke" => "#000", "arrow-end" => "block-wide-long")),
        "children" => $arrSasaran,
        "HTMLclass" => "bg-teal"
    );
}
$nodes = array(
    "text" => array("name"=> "OPD", "title"=> strtoupper(!empty($rskpd)?$rskpd[COL_SKPDNAMA]:'-')),
    "connectors" => array("style" => array("stroke" => "#000", "arrow-end" => "block-wide-long")),
    "children" => $arrTujuan,
    "HTMLclass" => "bg-aqua"
);
?>
<script type="text/javascript" src="<?=base_url()?>assets/treant/vendor/raphael.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/treant/Treant.js"></script>
<link href="<?=base_url()?>assets/treant/Treant.css" rel="stylesheet" type="text/css" />

<link rel="stylesheet" href="<?=base_url()?>assets/treant/vendor/perfect-scrollbar/perfect-scrollbar.css">
<script type="text/javascript" src="<?=base_url()?>assets/treant/vendor/jquery.mousewheel.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/treant/vendor/perfect-scrollbar/perfect-scrollbar.js"></script>
<style>
  .nodeExample1 {
      border: 1px solid #000;
      padding : 0px !important;
      width : 25vh !important;
      font-size: 8pt;
      color: #000 !important;
  }
  .node-wide {
      width : 32vh !important;
  }
  .nodeExample1 .node-name {
      font-weight: bold;
      margin: 0 0 5px !important;
      border-bottom: 1px solid #000;
      padding: 2px;
  }
  .nodeExample1 .node-title {
      #text-align: justify;
      padding: 2px;
  }
  .chart {
      overflow: auto;
  }
  .bg-greenlight {
      background-color: #c4dd39 !important;
  }
  .node-title {
      padding: 5px !important;
  }
  .nodeExample1 ul {
      margin-left: 5px !important;
  }
  .bg-lime {
    background-color: #01ff7054 !important;
  }
  .bg-teal {
    background-color: #39cccc66 !important;
  }
  .bg-aqua {
    background-color: #00c0ef61 !important;
  }
  .bg-orange {
    background-color: #ff851b73 !important;
  }
  .bg-fuchsia {
    background-color: #f012be73 !important;
  }
</style>

<div style="padding: 1rem; text-align: right">
<a id="btn-download" style="display: none" download="SITALAKBAJAKUN - CASCADING PEMERINTAH DAERAH PERIODE <?=strtoupper(!empty($rskpd)?$rskpd[COL_SKPDNAMA]:'-')?>.jpg" class="btn btn-primary btn-sm" href="">DOWNLOAD</a>
</div>
<div id="chart">
<h4 style="text-align: center; font-size: 14pt !important; margin-top: 10px !important">
  CASCADING KINERJA<br />
  <strong><?=strtoupper(!empty($rskpd)?$rskpd[COL_SKPDNAMA]:'-')?></strong><br />
  <strong>KOTA TEBING TINGGI</strong><br />
  <small style="font-style: italic">Dicetak melalui aplikasi <strong><?=$this->setting_web_name.' - '.$this->setting_web_desc?></strong></small>
</h4><hr />
  <div class="chart" id="basic-example">

  </div>
</div>
<div id="canvas" style="display: none">

</div>
<script src="<?=base_url()?>assets/js/html2canvas.min.js"></script>
<script>
  var chart_config = {
      chart: {
          container: "#basic-example",
          scrollbar: "fancy",
          //animateOnInit: true,
          rootOrientation:  'WEST', // NORTH || EAST || WEST || SOUTH
          connectors: {
              type: "step",
              style: {
                  "stroke-width": 1
              }
          },
          node: {
              HTMLclass: 'nodeExample1'
          },
          nodeAlign: 'TOP',
          hideRootNode: false
          /*animation: {
           nodeAnimation: "easeOutBounce",
           nodeSpeed: 700,
           connectorsAnimation: "bounce",
           connectorsSpeed: 700
           }*/
      },
      nodeStructure: <?=json_encode($nodes)?>
  };
  new Treant( chart_config );
  html2canvas(document.querySelector("#chart"), {scale: 1.75}).then(canvas => {
      document.getElementById("canvas").appendChild(canvas);
      var img = canvas.toDataURL("image/jpg");
      $("#btn-download").attr("href", img).show();
  });
</script>
