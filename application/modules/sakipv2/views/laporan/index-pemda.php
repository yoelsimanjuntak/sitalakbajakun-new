<?php
$rOptPmd = $this->db
->order_by(COL_PMDISAKTIF,'desc')
->order_by(COL_PMDTAHUNMULAI,'desc')
->get(TBL_SAKIPV2_PEMDA)
->result_array();
$getPmd = null;
if(!empty($_GET['idPmd'])) $getPmd = $_GET['idPmd'];
else if(!empty($rOptPmd)) $getPmd = $rOptPmd[0][COL_PMDID];

$getTahun = !empty($_GET['Tahun'])?$_GET['Tahun']:date('Y');

$rpmd = $this->db
->where(COL_PMDID, $getPmd)
->get(TBL_SAKIPV2_PEMDA)
->row_array();

$isDPAPerubahan = '';
if(!empty($_GET['isDPAPerubahan'])) {
  $isDPAPerubahan = $_GET['isDPAPerubahan'];
}
?>
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h3 class="m-0 font-weight-light"><?=$title?></h3>
      </div>
      <?php
      if(!empty($navs)) {
        ?>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <?php
            foreach($navs as $n) {
              if(!empty($n['link'])) {
                ?>
                <li class="breadcrumb-item"><a href="<?=$n['link']?>"><?=$n['text']?></a></li>
                <?php
              } else {
                ?>
                <li class="breadcrumb-item active"><?=$n['text']?></li>
                <?php
              }
            }
            ?>
          </ol>
        </div>
        <?php
      }
      ?>
    </div>
  </div>
</div>
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-12">
        <div class="card card-outline card-secondary">
          <div class="card-header p-0 border-0">
            <table class="table table-bordered mb-0">
              <thead>
                <tr>
                  <td>
                    <div class="form-group row">
                      <label class="control-label col-lg-2">PERIODE PEMERINTAHAN :</label>
                      <div class="col-lg-10">
                        <select class="form-control" name="idPmd">
                          <?php
                          foreach($rOptPmd as $opt) {
                            ?>
                            <option value="<?=site_url('sakipv2/laporan/index/'.$page).'?idPmd='.$opt[COL_PMDID].'&Tahun='.$getTahun?>" <?=$opt[COL_PMDID]==$getPmd?'selected':''?>>
                              <?=$opt[COL_PMDTAHUNMULAI].' s.d '.$opt[COL_PMDTAHUNAKHIR].' - '.strtoupper($opt[COL_PMDPEJABAT]).($opt['PmdIsPenjabat']==1?' (Pj.)':'')?>
                            </option>
                            <?php
                          }
                          ?>
                        </select>
                      </div>
                    </div>
                    <div class="form-group row">
                      <label class="control-label col-lg-2">TAHUN :</label>
                      <div class="col-lg-2">
                        <select class="form-control" name="Tahun">
                          <?php
                          if(!empty($rpmd)) {
                            for($i=$rpmd[COL_PMDTAHUNMULAI]; $i<=$rpmd[COL_PMDTAHUNAKHIR]; $i++) {
                              ?>
                              <option value="<?=site_url('sakipv2/laporan/index/'.$page).'?idPmd='.$opt[COL_PMDID].'&Tahun='.$getTahun?>" <?=$i==$getTahun?'selected':''?>>
                                <?=$i?>
                              </option>
                              <?php
                            }
                          }

                          ?>
                        </select>
                      </div>
                    </div>
                    <?php
                    if($page=='pemda-pk') {
                      ?>
                      <div class="form-group row">
                        <div class="offset-lg-2 pl-2">
                          <div class="custom-control custom-checkbox">
                            <input class="custom-control-input" type="checkbox" id="isDPAPerubahan" <?=$isDPAPerubahan?'checked="true"':''?>>
                            <input type="hidden" name="isDPAPerubahan" value="<?=$isDPAPerubahan?>" />
                            <label for="isDPAPerubahan" class="custom-control-label">Tampilkan sebagai PK Perubahan</label>
                          </div>
                        </div>
                      </div>
                      <?php
                    }
                    ?>
                  </td>
                </tr>
              </thead>
            </table>
          </div>
          <div class="card-body p-0">
            <?=$this->load->view('sakipv2/laporan/'.$page, array('idPmd'=>$getPmd,'isDPAPerubahan'=>$isDPAPerubahan))?>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<script type="text/javascript">
$(document).ready(function(){
  $('select[name=idPmd],input[name=Tahun],input[name=isDPAPerubahan]').change(function(){
    var url = $(this).val();
    location.href = url;
  });

  $('#isDPAPerubahan').change(function() {
    if($(this).is(':checked')) {
      $('[name=isDPAPerubahan]').val("<?=site_url('sakipv2/laporan/index/'.$page).'?idPmd='.$getPmd.'&Tahun='.$getTahun.'&isDPAPerubahan=1'?>").trigger('change');
    } else {
      $('[name=isDPAPerubahan]').val("<?=site_url('sakipv2/laporan/index/'.$page).'?idPmd='.$getPmd.'&Tahun='.$getTahun?>").trigger('change');
    }
  });
});
</script>
