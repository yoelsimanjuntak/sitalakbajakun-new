<?php
$rpmd = $this->db
->where(COL_PMDID, $idPmd)
->get(TBL_SAKIPV2_PEMDA)
->row_array();

$rskpd = $this->db
->where(COL_SKPDID, $idSKPD)
->get(TBL_SAKIPV2_SKPD)
->row_array();

$rrenstra = $this->db
->where(COL_RENSTRAID, $idRenstra)
->get(TBL_SAKIPV2_SKPD_RENSTRA)
->row_array();

$rdpa = $this->db
->where(COL_DPAID, $idDPA)
->get(TBL_SAKIPV2_SKPD_RENSTRA_DPA)
->row_array();

$arrFungsi = array();
$arrIKU = array();
if(!empty($rrenstra)) {
  $arrFungsi = json_decode($rrenstra[COL_RENSTRAFUNGSI]);
  $arrIKU = json_decode($rrenstra[COL_RENSTRAIKU]);
}
?>
<?php
if(!empty($isCetak) && $isCetak==1) {

} else {
  ?>
  <div class="row p-3">
    <div class="col-lg-12 text-center">
      <a href="<?=site_url('sakipv2/laporan/index/skpd-pk-cetak').'?idSKPD='.$idSKPD.'&idRenstra='.$idRenstra.'&idDPA='.$idDPA.(isset($isDPAPerubahan)?'&isDPAPerubahan='.$isDPAPerubahan:'')?>" class="btn btn-outline-primary btn-sm" target="_blank">
        <i class="far fa-print"></i>&nbsp;&nbsp;CETAK
      </a>
    </div>
  </div>
  <?php
}
?>
<!-- ES. II -->
<?php
if(!empty($isCetak) && $isCetak==1) {
  ?>
  <html>
  <head>
    <title>Informasi Jabatan - <?=$data['NM_JAB']?></title>
    <style>
    body {
      font-family: 'Source Sans Pro','Helvetica Neue',Helvetica,Arial,sans-serif;
    }
    th, td {
      padding: 5px;
    }
    table {
      width: 100%;
      border-collapse: collapse;
    }
    table, th, td {
      /*border: 1px solid black !important;*/
    }
    </style>
  </head>
  <body>
  <?php
  if(!empty($rskpd[COL_SKPDKOP])) {
    ?>
    <table width="100%" style="border-bottom: 1px solid #000000">
      <tr>
        <td rowspan="2" style="text-align: center; width: 60px; height: 100px">
          <img class="user-image" src="<?=MY_IMAGEPATH?>logo.png" style="width: 60px" alt="Logo">
        </td>
        <td style="text-align: center; padding-bottom: 0px"><h3 style="margin-bottom: 0px">PEMERINTAH KOTA TEBING TINGGI</h3></td>
      </tr>
      <tr>
        <td style="text-align: center; padding-top: 0px">
          <h2 style="margin-bottom: 0px"><?=strtoupper($rskpd[COL_SKPDNAMA])?></h2>
          <p style="margin-top: 0px"><?=$rskpd[COL_SKPDKOP]?></p>
        </td>
      </tr>
    </table>
    <?php
  }
  ?>
  <table width="100%">
    <tr>
      <td colspan="2" style="text-align: center; vertical-align: top">
        <br />
        <h4>PERNYATAAN PERJANJIAN KINERJA <?=isset($isDPAPerubahan)&&$isDPAPerubahan?'PERUBAHAN':''?> <br  />TAHUN <?=$rdpa[COL_DPATAHUN]?></h4>
      </td>
    </tr>
    <tr>
      <td colspan="2" style="text-align: justify; vertical-align: top">
          <p>Dalam rangka mewujudkan manajemen pemerintahan yang efektif, transparan, dan akuntabel serta berorientasi pada hasil, yang bertanda tangan dibawah ini:</p><br />
          <table>
              <tr>
                  <td style="padding-left: 25px; width: 10px; white-space: nowrap">Nama</td>
                  <td style="width:60px; text-align: right">:</td>
                  <td><?=$rskpd[COL_SKPDNAMAPIMPINAN]?></td>
              </tr>
              <tr>
                  <td style="padding-left: 25px; width: 10px; white-space: nowrap">Jabatan</td>
                  <td style="width: 60px; text-align: right">:</td>
                  <td style="text-align: left"><?=!empty($rskpd[COL_SKPDNAMAJABATAN])?strtoupper($rskpd[COL_SKPDNAMAJABATAN]):'KEPALA '.strtoupper($rskpd[COL_SKPDNAMA])?></td>
              </tr>
              <tr>
                <td colspan="3">
                  <p>Selanjutnya disebut Pihak Pertama</p>
                </td>
              </tr>
              <tr>
                  <td style="padding-left: 25px; width: 10px; white-space: nowrap">Nama</td>
                  <td style="width: 60px; text-align: right">:</td>
                  <td><?=$rpmd[COL_PMDPEJABAT]?></td>
              </tr>
              <tr>
                  <td style="padding-left: 25px; width: 10px; white-space: nowrap">Jabatan</td>
                  <td style="width: 60px; text-align: right">:</td>
                  <td><?=$rpmd['PmdIsPenjabat']==1?'PJ. ':''?>WALI KOTA TEBING TINGGI</td>
              </tr>
              <tr>
                <td colspan="3">
                  <p>Selaku atasan Pihak Pertama, selanjutnya disebut Pihak Kedua</p>
                </td>
              </tr>
              <tr>
                <td colspan="3">
                  <p>
                      Pihak Pertama berjanji akan mewujudkan target kinerja yang seharusnya sesuai lampiran perjanjian ini,
                      dalam rangka mencapai target kinerja jangka menengah seperti yang telah ditetapkan dalam dokumen perencanaan. Keberhasilan dan kegagalan pencapaian target kinerja tersebut menjadi tanggung jawab kami.
                  </p><br />
                  <p>
                      Pihak Kedua akan melakukan supervisi yang diperlukan serta akan melakukan evaluasi terhadap capaian kinerja dari perjanjian ini dan
                      mengambil tindakan yang diperlukan dalam rangka pemberian penghargaan dan sanksi.
                  </p>
                </td>
              </tr>
          </table>
      </td>
    </tr>
  </table>
  <br />
  <table width="100%">
    <tr>
      <td style="width: 80%; text-align: right">
        Tebing Tinggi,
      </td>
      <td style="white-space: nowrap; padding-left: 100px">
        <?=date("Y")?>
      </td>
    </tr>
  </table>
  <table width="100%">
    <tr>
        <td style="text-align: center">Pihak Kedua,</td>
        <td style="text-align: center">Pihak Pertama,</td>
    </tr>
    <tr>
        <td colspan="2">
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
        </td>
    </tr>
    <tr>
        <td style="text-align: center"><strong><?=$rpmd[COL_PMDPEJABAT]?></strong></td>
        <td style="text-align: center"><strong><?=$rskpd[COL_SKPDNAMAPIMPINAN]?></strong></td>
    </tr>
  </table>
  <pagebreak></pagebreak>
  <table width="100%">
    <tr>
        <td colspan="2" style="text-align: center; vertical-align: top">
            <h4>
                PERJANJIAN KINERJA <?=isset($isDPAPerubahan)&&$isDPAPerubahan?'PERUBAHAN':''?> TAHUN <?=$rdpa[COL_DPATAHUN]?>
                <br />
                <?=strtoupper($rskpd[COL_SKPDNAMA])?>
                <br />
                KOTA TEBING TINGGI
            </h4>
        </td>
    </tr>
  </table>
  <br />
  <table style="border: 1px solid #000; border-spacing: 0" border="1" width="100%">
    <thead>
      <tr>
          <th>No.</th>
          <th>Sasaran Strategis</th>
          <th>Indikator Kinerja</th>
          <th>Target</th>
      </tr>
      <tr>
          <th>(1)</th>
          <th>(2)</th>
          <th>(3)</th>
          <th>(4)</th>
      </tr>
    </thead>
    <tbody>
      <?php
      $rsasaran = $this->db
      ->join(TBL_SAKIPV2_SKPD_RENSTRA_TUJUAN,TBL_SAKIPV2_SKPD_RENSTRA_TUJUAN.'.'.COL_TUJUANID." = ".TBL_SAKIPV2_SKPD_RENSTRA_SASARAN.".".COL_IDTUJUAN,"left")
      ->join(TBL_SAKIPV2_SKPD_RENSTRA,TBL_SAKIPV2_SKPD_RENSTRA.'.'.COL_RENSTRAID." = ".TBL_SAKIPV2_SKPD_RENSTRA_TUJUAN.".".COL_IDRENSTRA,"left")
      ->where(COL_IDSKPD, $idSKPD)
      ->where(COL_IDRENSTRA, $idRenstra)
      ->order_by(TBL_SAKIPV2_SKPD_RENSTRA_TUJUAN.'.'.COL_TUJUANNO)
      ->order_by(TBL_SAKIPV2_SKPD_RENSTRA_SASARAN.'.'.COL_SASARANNO)
      ->get(TBL_SAKIPV2_SKPD_RENSTRA_SASARAN)
      ->result_array();
      $n=1;
      foreach($rsasaran as $s) {
        $rIndikatorSasaran = $this->db
        ->where(COL_IDSASARAN, $s[COL_SASARANID])
        ->get(TBL_SAKIPV2_SKPD_RENSTRA_SASARANDET)
        ->result_array();

        ?>
        <tr>
          <td <?=count($rIndikatorSasaran) > 1 ? 'rowspan='.count($rIndikatorSasaran) : ''?> style="text-align: right"><?=$n?>.</td>
          <td <?=count($rIndikatorSasaran) > 1 ? 'rowspan='.count($rIndikatorSasaran) : ''?>><?=strtoupper($s[COL_SASARANURAIAN])?></td>
          <?php
          if(count($rIndikatorSasaran) > 0) {
            $rIndikatorSasaranmonev = $this->db
            ->where(COL_IDSASARANINDIKATOR, $rIndikatorSasaran[0][COL_SSRINDIKATORID])
            ->where(COL_MONEVTAHUN, $rdpa[COL_DPATAHUN])
            ->get(TBL_SAKIPV2_SKPD_RENSTRA_SASARANMONEV)
            ->row_array();
            ?>
            <td><?=strtoupper($rIndikatorSasaran[0][COL_SSRINDIKATORURAIAN])?></td>
            <td><?=!empty($rIndikatorSasaranmonev)?$rIndikatorSasaranmonev[COL_MONEVTARGET].' ('.$rIndikatorSasaran[0][COL_SSRINDIKATORSATUAN].')':'-'?></td>
            <?php
          } else {

          }
          ?>
        </tr>
        <?php
        if(count($rIndikatorSasaran) > 1) {
          for($i=1; $i<count($rIndikatorSasaran); $i++) {
            $rIndikatorSasaranmonev = $this->db
            ->where(COL_IDSASARANINDIKATOR, $rIndikatorSasaran[$i][COL_SSRINDIKATORID])
            ->where(COL_MONEVTAHUN, $rdpa[COL_DPATAHUN])
            ->get(TBL_SAKIPV2_SKPD_RENSTRA_SASARANMONEV)
            ->row_array();
            ?>
            <tr>
              <td><?=strtoupper($rIndikatorSasaran[$i][COL_SSRINDIKATORURAIAN])?></td>
              <td><?=!empty($rIndikatorSasaranmonev)?$rIndikatorSasaranmonev[COL_MONEVTARGET].' ('.$rIndikatorSasaran[$i][COL_SSRINDIKATORSATUAN].')':'-'?></td>
            </tr>
            <?php
          }
        }
        ?>
        <?php
        $n++;
      }
      ?>
    </tbody>
  </table>
  <br />
  <?php
  $rprogram = $this->db
  ->join(TBL_SAKIPV2_BID,TBL_SAKIPV2_BID.'.'.COL_BIDID." = ".TBL_SAKIPV2_BID_PROGRAM.".".COL_IDBID." and ".TBL_SAKIPV2_BID.'.'.COL_BIDISAKTIF." = 1","left")
  ->join(TBL_SAKIPV2_SKPD_RENSTRA,TBL_SAKIPV2_SKPD_RENSTRA.'.'.COL_RENSTRAID." = ".TBL_SAKIPV2_BID.".".COL_IDRENSTRA,"left")
  ->where(COL_IDDPA, $idDPA)
  ->where(COL_IDRENSTRA, $idRenstra)
  ->order_by(TBL_SAKIPV2_BID_PROGRAM.'.'.COL_PROGRAMKODE)
  ->group_by(TBL_SAKIPV2_BID_PROGRAM.'.'.COL_PROGRAMKODE)
  ->get(TBL_SAKIPV2_BID_PROGRAM)
  ->result_array();
  $q__ = $this->db->last_query();
  ?>
  <table style="border: 1px solid #000; border-spacing: 0; page-break-inside:avoid;" border="1" width="100%">
    <tbody>
      <tr>
          <th>No.</th>
          <th>Program</th>
          <th>Anggaran</th>
      </tr>
      <tr>
          <th>(1)</th>
          <th>(2)</th>
          <th>(3)</th>
      </tr>
      <?php
      $n=1;
      $sumProg = 0;
      foreach($rprogram as $p) {
        $rsumkeg = $this->db
        ->select_sum(COL_SUBKEGPAGU)
        ->join(TBL_SAKIPV2_BID_KEGIATAN,TBL_SAKIPV2_BID_KEGIATAN.'.'.COL_KEGIATANID." = ".TBL_SAKIPV2_SUBBID_SUBKEGIATAN.".".COL_IDKEGIATAN,"left")
        ->join(TBL_SAKIPV2_BID_PROGRAM,TBL_SAKIPV2_BID_PROGRAM.'.'.COL_PROGRAMID." = ".TBL_SAKIPV2_BID_KEGIATAN.".".COL_IDPROGRAM,"left")
        //->join(TBL_SAKIPV2_BID,TBL_SAKIPV2_BID.'.'.COL_BIDID." = ".TBL_SAKIPV2_BID_PROGRAM.".".COL_IDBID,"left")
        ->join(TBL_SAKIPV2_SUBBID,TBL_SAKIPV2_SUBBID.'.'.COL_SUBBIDID." = ".TBL_SAKIPV2_SUBBID_SUBKEGIATAN.".".COL_IDSUBBID." and (".TBL_SAKIPV2_SUBBID.'.'.COL_SUBBIDISAKTIF." = 1 or ".TBL_SAKIPV2_SUBBID.'.'.COL_SUBBIDISAKTIF." is null)","left")
        ->join(TBL_SAKIPV2_BID,TBL_SAKIPV2_BID.'.'.COL_BIDID." = ".TBL_SAKIPV2_BID_PROGRAM.".".COL_IDBID." and (".TBL_SAKIPV2_BID.'.'.COL_BIDISAKTIF." = 1 or ".TBL_SAKIPV2_BID.'.'.COL_BIDISAKTIF." is null) ","left")
        ->join(TBL_SAKIPV2_SKPD_RENSTRA,TBL_SAKIPV2_SKPD_RENSTRA.'.'.COL_RENSTRAID." = ".TBL_SAKIPV2_BID.".".COL_IDRENSTRA,"left")
        ->where(COL_IDDPA, $idDPA)
        ->where(TBL_SAKIPV2_SKPD_RENSTRA.'.'.COL_RENSTRAID, $idRenstra)
        //->where(COL_IDPROGRAM, $p[COL_PROGRAMID])
        ->where(TBL_SAKIPV2_BID_PROGRAM.'.'.COL_PROGRAMKODE, $p[COL_PROGRAMKODE])
        ->where('(sakipv2_subbid.SubbidId is not null or sakipv2_subbid_subkegiatan.IdSubbid is null or sakipv2_subbid_subkegiatan.IdSubbid = 0)')
        ->get(TBL_SAKIPV2_SUBBID_SUBKEGIATAN)
        ->row_array();
        $q = $this->db->last_query();
        $sumProg += $rsumkeg[COL_SUBKEGPAGU];
        ?>
        <tr>
            <td style="text-align: right"><?=$n?>.</td>
            <td><?=$p[COL_PROGRAMURAIAN]?></td>
            <td style="text-align: right">
              <?='Rp. '.number_format($rsumkeg[COL_SUBKEGPAGU])?>
            </td>
        </tr>
        <?php
        $n++;
      }
      ?>
      <tr>
        <td style="text-align: right; font-weight: bold" colspan="2">TOTAL</td>
        <td style="text-align: right; font-weight: bold">
          <?='Rp. '.number_format($sumProg)?>
        </td>
      </tr>
      <tr style="border: none">
        <td colspan="3">
          <table width="100%" style="margin-top: 50px !important">
            <tr>
                <td style="text-align: center">Pihak Kedua,</td>
                <td style="text-align: center">Pihak Pertama,</td>
            </tr>
            <tr>
                <td colspan="2">
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                </td>
            </tr>
            <tr>
              <td style="text-align: center"><strong><?=$rpmd[COL_PMDPEJABAT]?></strong></td>
              <td style="text-align: center"><strong><?=$rskpd[COL_SKPDNAMAPIMPINAN]?></strong></td>
            </tr>
          </table>
        </td>
      </tr>
    </tbody>
  </table>
<!-- ES. II -->

<!-- ES. III -->
<?php
  $rbidang = array();
  if(!empty($rrenstra)) {
    $rbidang = $this->db
    ->where(COL_IDRENSTRA, $idRenstra)
    ->get(TBL_SAKIPV2_BID)
    ->result_array();
  }
  foreach($rbidang as $bid) {
    ?>
    <pagebreak></pagebreak>
    <?php
    if(!empty($rskpd[COL_SKPDKOP]) && false) {
      ?>
      <table width="100%" style="border-bottom: 1px solid #000000">
        <tr>
          <td rowspan="2" style="text-align: center; width: 60px; height: 100px">
            <!--<img class="user-image" src="<?=MY_IMAGEURL?>logo.png" style="width: 60px" alt="Logo">-->
          </td>
          <td style="text-align: center; padding-bottom: 0px"><h3 style="margin-bottom: 0px">PEMERINTAH KOTA TEBING TINGGI</h3></td>
        </tr>
        <tr>
          <td style="text-align: center; padding-top: 0px; white-space: nowrap">
            <h2 style="margin-bottom: 0px"><?=strtoupper($rskpd[COL_SKPDNAMA])?></h2>
            <p style="margin-top: 0px"><?=$rskpd[COL_SKPDKOP]?></p>
          </td>
        </tr>
      </table>
      <?php
    }
    ?>
    <table width="100%">
      <tr>
        <td colspan="2" style="text-align: center; vertical-align: top">
          <br />
          <h4>PERNYATAAN PERJANJIAN KINERJA <?=isset($isDPAPerubahan)&&$isDPAPerubahan?'PERUBAHAN':''?> <br  />TAHUN <?=$rdpa[COL_DPATAHUN]?></h4>
        </td>
      </tr>
      <tr>
        <td colspan="2" style="text-align: justify; vertical-align: top">
            <p>Dalam rangka mewujudkan manajemen pemerintahan yang efektif, transparan, dan akuntabel serta berorientasi pada hasil, yang bertanda tangan dibawah ini:</p><br />
            <table>
                <tr>
                    <td style="padding-left: 25px; width: 10px; white-space: nowrap">Nama</td>
                    <td style="width:60px; text-align: right">:</td>
                    <td><?=$bid[COL_BIDNAMAPIMPINAN]?></td>
                </tr>
                <tr>
                    <td style="padding-left: 25px; width: 10px; white-space: nowrap">Jabatan</td>
                    <td style="width: 60px; text-align: right">:</td>
                    <td style="text-align: left"><?=strtoupper(!empty($bid[COL_BIDNAMAJABATAN])?$bid[COL_BIDNAMAJABATAN]:'KEPALA '.$bid[COL_BIDNAMA])?></td>
                </tr>
                <tr>
                  <td colspan="3">
                    <p>Selanjutnya disebut Pihak Pertama</p>
                  </td>
                </tr>
                <tr>
                    <td style="padding-left: 25px; width: 10px; white-space: nowrap">Nama</td>
                    <td style="width: 60px; text-align: right">:</td>
                    <td><?=$rskpd[COL_SKPDNAMAPIMPINAN]?></td>
                </tr>
                <tr>
                    <td style="padding-left: 25px; width: 10px; white-space: nowrap">Jabatan</td>
                    <td style="width: 60px; text-align: right">:</td>
                    <td><?=!empty($rskpd[COL_SKPDNAMAJABATAN])?strtoupper($rskpd[COL_SKPDNAMAJABATAN]):'KEPALA '.strtoupper($rskpd[COL_SKPDNAMA])?></td>
                </tr>
                <tr>
                  <td colspan="3">
                    <p>Selaku atasan Pihak Pertama, selanjutnya disebut Pihak Kedua</p>
                  </td>
                </tr>
                <tr>
                  <td colspan="3">
                    <p>
                        Pihak Pertama berjanji akan mewujudkan target kinerja yang seharusnya sesuai lampiran perjanjian ini,
                        dalam rangka mencapai target kinerja jangka menengah seperti yang telah ditetapkan dalam dokumen perencanaan. Keberhasilan dan kegagalan pencapaian target kinerja tersebut menjadi tanggung jawab kami.
                    </p><br />
                    <p>
                        Pihak Kedua akan melakukan supervisi yang diperlukan serta akan melakukan evaluasi terhadap capaian kinerja dari perjanjian ini dan
                        mengambil tindakan yang diperlukan dalam rangka pemberian penghargaan dan sanksi.
                    </p>
                  </td>
                </tr>
            </table>
        </td>
      </tr>
    </table>
    <table width="100%">
      <tr>
          <td style="text-align: center">Pihak Kedua,</td>
          <td style="text-align: center">Pihak Pertama,</td>
      </tr>
      <tr>
          <td colspan="2">
              <br />
              <br />
              <br />
              <br />
              <br />
              <br />
          </td>
      </tr>
      <tr>
          <td style="text-align: center"><strong><?=$rskpd[COL_SKPDNAMAPIMPINAN]?></strong></td>
          <td style="text-align: center"><strong><?=$bid[COL_BIDNAMAPIMPINAN]?></strong></td>
      </tr>
    </table>
    <pagebreak></pagebreak>
    <table width="100%">
      <tr>
          <td colspan="2" style="text-align: center; vertical-align: top">
              <h4>
                  PERJANJIAN KINERJA <?=isset($isDPAPerubahan)&&$isDPAPerubahan?'PERUBAHAN':''?> TAHUN <?=$rdpa[COL_DPATAHUN]?>
                  <br />
                  <?=strtoupper($rskpd[COL_SKPDNAMA])?>
                  <br />
                  KOTA TEBING TINGGI
              </h4>
          </td>
      </tr>
    </table>
    <br />
    <table style="border: 1px solid #000; border-spacing: 0" border="1" width="100%">
      <thead>
        <tr>
            <th>No.</th>
            <th>Sasaran Strategis</th>
            <th>Indikator Kinerja</th>
            <th>Target</th>
        </tr>
        <tr>
            <th>(1)</th>
            <th>(2)</th>
            <th>(3)</th>
            <th>(4)</th>
        </tr>
      </thead>
      <tbody>
        <?php
        $rsasaran = $this->db
        ->select('*,
        (select count(*) from sakipv2_bid_progsasaran sas
          left join sakipv2_bid_program prg on prg.ProgramId=sas.IdProgram where
          sas.IdProgram=sakipv2_bid_progsasaran.IdProgram and
          /*prg.IdDPA=sakipv2_bid_program.IdDPA and
          prg.IdBid=sakipv2_bid_program.IdBid and*/
          sas.SasaranNo=sakipv2_bid_progsasaran.SasaranNo and
          sas.SasaranUraian=sakipv2_bid_progsasaran.SasaranUraian
        ) as grpSasaran')
        ->join(TBL_SAKIPV2_BID_PROGRAM,TBL_SAKIPV2_BID_PROGRAM.'.'.COL_PROGRAMID." = ".TBL_SAKIPV2_BID_PROGSASARAN.".".COL_IDPROGRAM,"left")
        ->join(TBL_SAKIPV2_BID,TBL_SAKIPV2_BID.'.'.COL_BIDID." = ".TBL_SAKIPV2_BID_PROGRAM.".".COL_IDBID,"left")
        ->join(TBL_SAKIPV2_SKPD_RENSTRA,TBL_SAKIPV2_SKPD_RENSTRA.'.'.COL_RENSTRAID." = ".TBL_SAKIPV2_BID.".".COL_IDRENSTRA,"left")
        ->where(COL_IDDPA, $idDPA)
        ->where(COL_IDRENSTRA, $idRenstra)
        ->where(COL_IDBID, $bid[COL_BIDID])
        ->order_by(TBL_SAKIPV2_BID_PROGRAM.'.'.COL_PROGRAMKODE)
        ->order_by(TBL_SAKIPV2_BID_PROGSASARAN.'.'.COL_SASARANNO)
        //->group_by(TBL_SAKIPV2_BID_PROGSASARAN.'.'.COL_SASARANURAIAN)
        /*->group_by(
          array(TBL_SAKIPV2_BID_PROGSASARAN.'.'.COL_SASARANNO,
                TBL_SAKIPV2_BID_PROGSASARAN.'.'.COL_SASARANURAIAN,
                TBL_SAKIPV2_BID_PROGSASARAN.'.'.COL_SASARANSATUAN,
                TBL_SAKIPV2_BID_PROGSASARAN.'.'.COL_SASARANTARGET
          ))*/
        ->get(TBL_SAKIPV2_BID_PROGSASARAN)
        ->result_array();

        $n=1;
        $lastprg="";
        $lastno="";
        $lastdesc="";
        $lastsatuan="";
        $lasttarget="";
        foreach($rsasaran as $s) {
          $rowspan = $s['grpSasaran'];
          ?>
          <tr>
            <?php
          if( $lastprg!=$s[COL_IDPROGRAM] || $lastno!=$s[COL_SASARANNO] || strtolower($lastdesc)!=strtolower($s[COL_SASARANURAIAN])/* || strtolower($lastsatuan)!=strtolower($s[COL_SASARANSATUAN]) || strtolower($lasttarget)!=strtolower($s[COL_SASARANTARGET])*/) {
              ?>
              <td <?=$rowspan>1?"rowspan=$rowspan":""?> style="text-align: right; width: 10px"><?=$n?>.</td>
              <td <?=$rowspan>1?"rowspan=$rowspan":""?>><?=strtoupper($s[COL_SASARANURAIAN])?></td>
              <?php
              $n++;
            }
            ?>
            <td><?=strtoupper($s[COL_SASARANINDIKATOR])?></td>
            <td style="text-align: right"><?=strtoupper($s[COL_SASARANTARGET]).' ('.strtoupper($s[COL_SASARANSATUAN]).')'?></td>
          </tr>
          <?php
          $lastprg=$s[COL_IDPROGRAM];
          $lastno=$s[COL_SASARANNO];
          $lastdesc=$s[COL_SASARANURAIAN];
          $lastsatuan=$s[COL_SASARANSATUAN];
          $lasttarget=$s[COL_SASARANTARGET];
        }
        ?>
      </tbody>
    </table>
    <br />
    <?php
    $rprogram = $this->db
    ->select('*, sum(sakipv2_subbid_subkegiatan.SubkegPagu) as SumPagu')
    /*->join(TBL_SAKIPV2_SUBBID_SUBKEGIATAN,TBL_SAKIPV2_SUBBID_SUBKEGIATAN.'.'.COL_IDKEGIATAN." = ".TBL_SAKIPV2_BID_KEGIATAN.".".COL_KEGIATANID,"left")
    ->join(TBL_SAKIPV2_BID_PROGRAM,TBL_SAKIPV2_BID_PROGRAM.'.'.COL_PROGRAMID." = ".TBL_SAKIPV2_BID_KEGIATAN.".".COL_IDPROGRAM,"left")
    ->join(TBL_SAKIPV2_BID,TBL_SAKIPV2_BID.'.'.COL_BIDID." = ".TBL_SAKIPV2_BID_PROGRAM.".".COL_IDBID,"left")
    ->join(TBL_SAKIPV2_SKPD_RENSTRA,TBL_SAKIPV2_SKPD_RENSTRA.'.'.COL_RENSTRAID." = ".TBL_SAKIPV2_BID.".".COL_IDRENSTRA,"left")*/
    ->join(TBL_SAKIPV2_BID_PROGRAM,TBL_SAKIPV2_BID_PROGRAM.'.'.COL_PROGRAMID." = ".TBL_SAKIPV2_BID_KEGIATAN.".".COL_IDPROGRAM,"left")
    ->join(TBL_SAKIPV2_BID,TBL_SAKIPV2_BID.'.'.COL_BIDID." = ".TBL_SAKIPV2_BID_PROGRAM.".".COL_IDBID." and ".TBL_SAKIPV2_BID.'.'.COL_BIDISAKTIF." = 1","left")
    ->join(TBL_SAKIPV2_SUBBID_SUBKEGIATAN,TBL_SAKIPV2_SUBBID_SUBKEGIATAN.'.'.COL_IDKEGIATAN." = ".TBL_SAKIPV2_BID_KEGIATAN.".".COL_KEGIATANID,"left")
    ->join(TBL_SAKIPV2_SUBBID,TBL_SAKIPV2_SUBBID.'.'.COL_SUBBIDID." = ".TBL_SAKIPV2_SUBBID_SUBKEGIATAN.".".COL_IDSUBBID." and (".TBL_SAKIPV2_SUBBID.'.'.COL_SUBBIDISAKTIF." = 1 or ".TBL_SAKIPV2_SUBBID.'.'.COL_SUBBIDISAKTIF." is null)","left")
    ->join(TBL_SAKIPV2_SKPD_RENSTRA,TBL_SAKIPV2_SKPD_RENSTRA.'.'.COL_RENSTRAID." = ".TBL_SAKIPV2_BID.".".COL_IDRENSTRA,"left")
    ->where(TBL_SAKIPV2_BID_PROGRAM.'.'.COL_IDDPA, $idDPA)
    ->where(TBL_SAKIPV2_BID.'.'.COL_IDRENSTRA, $idRenstra)
    ->where(TBL_SAKIPV2_BID_PROGRAM.'.'.COL_IDBID, $bid[COL_BIDID])
    ->order_by(TBL_SAKIPV2_BID_KEGIATAN.'.'.COL_KEGIATANKODE)
    ->group_by(array(TBL_SAKIPV2_BID_KEGIATAN.'.'.COL_KEGIATANKODE))
    ->get(TBL_SAKIPV2_BID_KEGIATAN)
    ->result_array();
    ?>
      <table style="border: 1px solid #000; border-spacing: 0; page-break-inside:avoid;" border="1" width="100%">
        <tbody>
          <tr>
              <th>No.</th>
              <th>Kegiatan</th>
              <th>Anggaran</th>
          </tr>
          <tr>
              <th>(1)</th>
              <th>(2)</th>
              <th>(3)</th>
          </tr>
          <?php
          $n=1;
          $sumProg = 0;
          foreach($rprogram as $p) {
            /*$rsumkeg = $this->db
            ->select_sum(COL_SUBKEGPAGU)
            ->where(COL_IDKEGIATAN, $p[COL_KEGIATANID])
            ->get(TBL_SAKIPV2_SUBBID_SUBKEGIATAN)
            ->row_array();
            $sumProg += $rsumkeg[COL_SUBKEGPAGU];*/
            $sumProg += $p['SumPagu'];
            ?>
            <tr>
                <td style="text-align: right; width: 10px"><?=$n?>.</td>
                <td><?=$p[COL_KEGIATANURAIAN]?></td>
                <td style="text-align: right">
                  <?='Rp. '.number_format($p['SumPagu'])?>
                </td>
            </tr>
            <?php
            $n++;
          }
          ?>
          <tr>
            <td style="text-align: right; font-weight: bold" colspan="2">TOTAL</td>
            <td style="text-align: right; font-weight: bold">
              <?='Rp. '.number_format($sumProg)?>
            </td>
          </tr>
          <tr style="border: none">
            <td colspan="3">
              <table width="100%" style="margin-top: 50px !important">
                <tr>
                    <td style="text-align: center">Pihak Kedua,</td>
                    <td style="text-align: center">Pihak Pertama,</td>
                </tr>
                <tr>
                    <td colspan="2">
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                    </td>
                </tr>
                <tr>
                    <td style="text-align: center"><strong><?=$rskpd[COL_SKPDNAMAPIMPINAN]?></strong></td>
                    <td style="text-align: center"><strong><?=$bid[COL_BIDNAMAPIMPINAN]?></strong></td>
                </tr>
              </table>
            </td>
          </tr>
        </tbody>
      </table>
    <?php
  }
?>
<!-- ES. III -->

<!-- ES. IV -->
<?php
  $rsubbidang = $this->db
  ->join(TBL_SAKIPV2_BID,TBL_SAKIPV2_BID.'.'.COL_BIDID." = ".TBL_SAKIPV2_SUBBID.".".COL_IDBID,"left")
  ->where(COL_IDRENSTRA, $idRenstra)
  ->get(TBL_SAKIPV2_SUBBID)
  ->result_array();

  foreach($rsubbidang as $sub) {
    ?>
    <pagebreak></pagebreak>
    <?php
    if(!empty($rskpd[COL_SKPDKOP]) && false) {
      ?>
      <table width="100%" style="border-bottom: 1px solid #000000">
        <tr>
          <td rowspan="2" style="text-align: center; width: 60px; height: 100px">
            <!--<img class="user-image" src="<?=MY_IMAGEURL?>logo.png" style="width: 60px" alt="Logo">-->
          </td>
          <td style="text-align: center; padding-bottom: 0px"><h3 style="margin-bottom: 0px">PEMERINTAH KOTA TEBING TINGGI</h3></td>
        </tr>
        <tr>
          <td style="text-align: center; padding-top: 0px; white-space: nowrap">
            <h2 style="margin-bottom: 0px"><?=strtoupper($rskpd[COL_SKPDNAMA])?></h2>
            <p style="margin-top: 0px"><?=$rskpd[COL_SKPDKOP]?></p>
          </td>
        </tr>
      </table>
      <?php
    }
    ?>
    <table width="100%">
      <tr>
        <td colspan="2" style="text-align: center; vertical-align: top">
          <br />
          <h4>PERNYATAAN PERJANJIAN KINERJA <?=isset($isDPAPerubahan)&&$isDPAPerubahan?'PERUBAHAN':''?> <br  />TAHUN <?=$rdpa[COL_DPATAHUN]?></h4>
        </td>
      </tr>
      <tr>
        <td colspan="2" style="text-align: justify; vertical-align: top">
            <p>Dalam rangka mewujudkan manajemen pemerintahan yang efektif, transparan, dan akuntabel serta berorientasi pada hasil, yang bertanda tangan dibawah ini:</p><br />
            <table>
                <tr>
                    <td style="padding-left: 25px; width: 10px; white-space: nowrap">Nama</td>
                    <td style="width:60px; text-align: right">:</td>
                    <td><?=$sub[COL_SUBBIDNAMAPIMPINAN]?></td>
                </tr>
                <tr>
                    <td style="padding-left: 25px; width: 10px; white-space: nowrap">Jabatan</td>
                    <td style="width: 60px; text-align: right">:</td>
                    <td style="text-align: left"><?=strtoupper(!empty($sub[COL_SUBBIDNAMAJABATAN])?$sub[COL_SUBBIDNAMAJABATAN]:'KEPALA '.$sub[COL_SUBBIDNAMA])?></td>
                </tr>
                <tr>
                  <td colspan="3">
                    <p>Selanjutnya disebut Pihak Pertama</p>
                  </td>
                </tr>
                <tr>
                    <td style="padding-left: 25px; width: 10px; white-space: nowrap">Nama</td>
                    <td style="width: 60px; text-align: right">:</td>
                    <td><?=$sub[COL_BIDNAMAPIMPINAN]?></td>
                </tr>
                <tr>
                    <td style="padding-left: 25px; width: 10px; white-space: nowrap">Jabatan</td>
                    <td style="width: 60px; text-align: right">:</td>
                    <td><?=strtoupper(!empty($sub[COL_BIDNAMAJABATAN])?$sub[COL_BIDNAMAJABATAN]:'KEPALA '.$sub[COL_BIDNAMA])?></td>
                </tr>
                <tr>
                  <td colspan="3">
                    <p>Selaku atasan Pihak Pertama, selanjutnya disebut Pihak Kedua</p>
                  </td>
                </tr>
                <tr>
                  <td colspan="3">
                    <p>
                        Pihak Pertama berjanji akan mewujudkan target kinerja yang seharusnya sesuai lampiran perjanjian ini,
                        dalam rangka mencapai target kinerja jangka menengah seperti yang telah ditetapkan dalam dokumen perencanaan. Keberhasilan dan kegagalan pencapaian target kinerja tersebut menjadi tanggung jawab kami.
                    </p><br />
                    <p>
                        Pihak Kedua akan melakukan supervisi yang diperlukan serta akan melakukan evaluasi terhadap capaian kinerja dari perjanjian ini dan
                        mengambil tindakan yang diperlukan dalam rangka pemberian penghargaan dan sanksi.
                    </p>
                  </td>
                </tr>
            </table>
        </td>
      </tr>
    </table>
    <table width="100%">
      <tr>
          <td style="text-align: center">Pihak Kedua,</td>
          <td style="text-align: center">Pihak Pertama,</td>
      </tr>
      <tr>
          <td colspan="2">
              <br />
              <br />
              <br />
              <br />
              <br />
              <br />
          </td>
      </tr>
      <tr>
          <td style="text-align: center"><strong><?=$sub[COL_BIDNAMAPIMPINAN]?></strong></td>
          <td style="text-align: center"><strong><?=$sub[COL_SUBBIDNAMAPIMPINAN]?></strong></td>
      </tr>
    </table>
    <pagebreak></pagebreak>
    <table width="100%">
      <tr>
          <td colspan="2" style="text-align: center; vertical-align: top">
              <h4>
                  PERJANJIAN KINERJA <?=isset($isDPAPerubahan)&&$isDPAPerubahan?'PERUBAHAN':''?> TAHUN <?=$rdpa[COL_DPATAHUN]?>
                  <br />
                  <?=strtoupper($rskpd[COL_SKPDNAMA])?>
                  <br />
                  KOTA TEBING TINGGI
              </h4>
          </td>
      </tr>
    </table>
    <br />
    <table style="border: 1px solid #000; border-spacing: 0" border="1" width="100%">
      <thead>
        <tr>
            <th>No.</th>
            <th>Sasaran Strategis</th>
            <th>Indikator Kinerja</th>
            <th>Target</th>
        </tr>
        <tr>
            <th>(1)</th>
            <th>(2)</th>
            <th>(3)</th>
            <th>(4)</th>
        </tr>
      </thead>
      <tbody>
        <?php
        $rsasaran = $this->db
        ->select('*, (select count(*) from sakipv2_subbid_subkegsasaran sas where sas.IdSubkeg=sakipv2_subbid_subkegsasaran.IdSubkeg and sas.SasaranNo=sakipv2_subbid_subkegsasaran.SasaranNo and sas.SasaranUraian=sakipv2_subbid_subkegsasaran.SasaranUraian) as grpSasaran')
        ->join(TBL_SAKIPV2_SUBBID_SUBKEGIATAN,TBL_SAKIPV2_SUBBID_SUBKEGIATAN.'.'.COL_SUBKEGID." = ".TBL_SAKIPV2_SUBBID_SUBKEGSASARAN.".".COL_IDSUBKEG,"left")
        ->join(TBL_SAKIPV2_BID_KEGIATAN,TBL_SAKIPV2_BID_KEGIATAN.'.'.COL_KEGIATANID." = ".TBL_SAKIPV2_SUBBID_SUBKEGIATAN.".".COL_IDKEGIATAN,"left")
        ->join(TBL_SAKIPV2_BID_PROGRAM,TBL_SAKIPV2_BID_PROGRAM.'.'.COL_PROGRAMID." = ".TBL_SAKIPV2_BID_KEGIATAN.".".COL_IDPROGRAM,"left")
        ->join(TBL_SAKIPV2_BID,TBL_SAKIPV2_BID.'.'.COL_BIDID." = ".TBL_SAKIPV2_BID_PROGRAM.".".COL_IDBID,"left")
        ->join(TBL_SAKIPV2_SKPD_RENSTRA,TBL_SAKIPV2_SKPD_RENSTRA.'.'.COL_RENSTRAID." = ".TBL_SAKIPV2_BID.".".COL_IDRENSTRA,"left")
        ->where(COL_IDDPA, $idDPA)
        ->where(COL_IDRENSTRA, $idRenstra)
        ->where(COL_IDSUBBID, $sub[COL_SUBBIDID])
        ->order_by(TBL_SAKIPV2_SUBBID_SUBKEGIATAN.'.'.COL_SUBKEGKODE)
        ->order_by(TBL_SAKIPV2_SUBBID_SUBKEGSASARAN.'.'.COL_SASARANNO)
        ->get(TBL_SAKIPV2_SUBBID_SUBKEGSASARAN)
        ->result_array();
        $n=1;
        $lastid="";
        $lastno="";
        $lastdesc="";
        foreach($rsasaran as $s) {
          $rowspan = $s['grpSasaran'];
          ?>
          <tr>
            <?php
            if($lastno!=$s[COL_SASARANNO] || $lastdesc!=$s[COL_SASARANURAIAN] || $lastid!=$s[COL_SUBKEGID]) {
              ?>
              <td <?=$rowspan>1?"rowspan=$rowspan":""?> style="text-align: right"><?=$n?>.</td>
              <td <?=$rowspan>1?"rowspan=$rowspan":""?>><?=strtoupper($s[COL_SASARANURAIAN])?></td>
              <?php
              $n++;
            }
            ?>
            <td><?=strtoupper($s[COL_SASARANINDIKATOR])?></td>
            <td style="text-align: right"><?=strtoupper($s[COL_SASARANTARGET]).' ('.strtoupper($s[COL_SASARANSATUAN]).')'?></td>
          </tr>
          <?php
          $lastid=$s[COL_SUBKEGID];
          $lastno=$s[COL_SASARANNO];
          $lastdesc=$s[COL_SASARANURAIAN];
        }
        ?>
      </tbody>
    </table>
    <br />
    <?php
    $rsubkeg = $this->db
    ->join(TBL_SAKIPV2_BID_KEGIATAN,TBL_SAKIPV2_BID_KEGIATAN.'.'.COL_KEGIATANID." = ".TBL_SAKIPV2_SUBBID_SUBKEGIATAN.".".COL_IDKEGIATAN,"left")
    ->join(TBL_SAKIPV2_BID_PROGRAM,TBL_SAKIPV2_BID_PROGRAM.'.'.COL_PROGRAMID." = ".TBL_SAKIPV2_BID_KEGIATAN.".".COL_IDPROGRAM,"left")
    ->join(TBL_SAKIPV2_BID,TBL_SAKIPV2_BID.'.'.COL_BIDID." = ".TBL_SAKIPV2_BID_PROGRAM.".".COL_IDBID,"left")
    ->join(TBL_SAKIPV2_SKPD_RENSTRA,TBL_SAKIPV2_SKPD_RENSTRA.'.'.COL_RENSTRAID." = ".TBL_SAKIPV2_BID.".".COL_IDRENSTRA,"left")
    ->where(COL_IDDPA, $idDPA)
    ->where(COL_IDRENSTRA, $idRenstra)
    ->where(COL_IDSUBBID, $sub[COL_SUBBIDID])
    ->get(TBL_SAKIPV2_SUBBID_SUBKEGIATAN)
    ->result_array();
    ?>
    <table style="border: 1px solid #000; border-spacing: 0; page-break-inside:avoid;" border="1" width="100%">
      <tbody>
        <tr>
            <th>No.</th>
            <th>Sub Kegiatan</th>
            <th>Anggaran</th>
        </tr>
        <tr>
            <th>(1)</th>
            <th>(2)</th>
            <th>(3)</th>
        </tr>
        <?php
        $n=1;
        $sumProg = 0;
        foreach($rsubkeg as $p) {
          $sumProg += $p[COL_SUBKEGPAGU]
          ?>
          <tr>
              <td style="text-align: right"><?=$n?>.</td>
              <td><?=$p[COL_SUBKEGURAIAN]?></td>
              <td style="text-align: right">
                <?='Rp. '.number_format($p[COL_SUBKEGPAGU])?>
              </td>
          </tr>
          <?php
          $n++;
        }
        ?>
        <tr>
          <td style="text-align: right; font-weight: bold" colspan="2">TOTAL</td>
          <td style="text-align: right; font-weight: bold">
            <?='Rp. '.number_format($sumProg)?>
          </td>
        </tr>
        <tr style="border: none">
          <td colspan="3">
            <table width="100%" style="margin-top: 50px !important">
              <tr>
                  <td style="text-align: center">Pihak Kedua,</td>
                  <td style="text-align: center">Pihak Pertama,</td>
              </tr>
              <tr>
                  <td colspan="2">
                      <br />
                      <br />
                      <br />
                      <br />
                      <br />
                      <br />
                  </td>
              </tr>
              <tr>
                <td style="text-align: center"><strong><?=$sub[COL_BIDNAMAPIMPINAN]?></strong></td>
                <td style="text-align: center"><strong><?=$sub[COL_SUBBIDNAMAPIMPINAN]?></strong></td>
              </tr>
            </table>
          </td>
        </tr>
      </tbody>
    </table>
    <?php
  }
?>
<!-- ES. IV -->

<!-- INDIVIDU -->
<?php
$rpelaksana = array();
$rrenstra = $this->db
->where(COL_RENSTRAID, $idRenstra)
->get(TBL_SAKIPV2_SKPD_RENSTRA)
->row_array();

if(!empty($rrenstra)) {
  $rpelaksana = $this->db
  ->select('*, COALESCE(sakipv2_subbid.SubbidNama, sakipv2_bid.BidNama, skpd_.SkpdNama) as UnitNama, COALESCE(sakipv2_subbid.SubbidNamaPimpinan, sakipv2_bid.BidNamaPimpinan, skpd_.SkpdNamaPimpinan) as UnitNamaPimpinan, COALESCE(sakipv2_subbid.SubbidNamaJabatan, sakipv2_bid.BidNamaJabatan, skpd_.SkpdNamaJabatan) as UnitNamaJabatan')
  ->join(TBL_SAKIPV2_SUBBID,TBL_SAKIPV2_SUBBID.'.'.COL_SUBBIDID." = ".TBL_SAKIPV2_SUBBID_PELAKSANA.".".COL_IDSUBBID,"left")
  ->join(TBL_SAKIPV2_BID.' bid_','bid_.'.COL_BIDID." = (sakipv2_subbid_pelaksana.IdSubbid*-1-1000)","left")
  ->join(TBL_SAKIPV2_BID,TBL_SAKIPV2_BID.'.'.COL_BIDID." = COALESCE(sakipv2_subbid.IdBid, bid_.BidId)","left")
  ->join(TBL_SAKIPV2_SKPD.' skpd_','skpd_.'.COL_SKPDID." = (sakipv2_subbid_pelaksana.IdSubbid*-1-100)","left")

  ->where(TBL_SAKIPV2_BID.'.'.COL_IDRENSTRA, $idRenstra)
  ->or_where(TBL_SAKIPV2_SUBBID_PELAKSANA.'.'.COL_IDSUBBID, ($rrenstra[COL_IDSKPD]+100)*-1)
  ->get(TBL_SAKIPV2_SUBBID_PELAKSANA)
  ->result_array();
  /*->join(TBL_SAKIPV2_SUBBID,TBL_SAKIPV2_SUBBID.'.'.COL_SUBBIDID." = ".TBL_SAKIPV2_SUBBID_PELAKSANA.".".COL_IDSUBBID,"left")
  ->join(TBL_SAKIPV2_BID,TBL_SAKIPV2_BID.'.'.COL_BIDID." = ".TBL_SAKIPV2_SUBBID.".".COL_IDBID,"left")
  ->where(COL_IDRENSTRA, $idRenstra)
  ->get(TBL_SAKIPV2_SUBBID_PELAKSANA)
  ->result_array();*/
}

foreach($rpelaksana as $pel) {
  $rsasaran = array();
  $rfungsi = array();
  if(!empty($pel[COL_PLSIKU])) {
    $rsasaran = json_decode($pel[COL_PLSIKU]);
  }
  if(!empty($pel[COL_PLSFUNGSI])) {
    $rfungsi = json_decode($pel[COL_PLSFUNGSI]);
  }
  ?>
  <pagebreak></pagebreak>
  <table width="100%">
    <tr>
      <td colspan="2" style="text-align: center; vertical-align: top">
        <br />
        <h4>PERNYATAAN PERJANJIAN KINERJA <?=isset($isDPAPerubahan)&&$isDPAPerubahan?'PERUBAHAN':''?> <br  />TAHUN <?=$rdpa[COL_DPATAHUN]?></h4>
      </td>
    </tr>
    <tr>
      <td colspan="2" style="text-align: justify; vertical-align: top">
          <p>Dalam rangka mewujudkan manajemen pemerintahan yang efektif, transparan, dan akuntabel serta berorientasi pada hasil, yang bertanda tangan dibawah ini:</p><br />
          <table>
              <tr>
                  <td style="padding-left: 25px; width: 10px; white-space: nowrap">Nama</td>
                  <td style="width:60px; text-align: right">:</td>
                  <td><?=$pel[COL_PLSNAMAPEGAWAI]?></td>
              </tr>
              <tr>
                  <td style="padding-left: 25px; width: 10px; white-space: nowrap">Jabatan</td>
                  <td style="width: 60px; text-align: right">:</td>
                  <td style="text-align: left"><?=strtoupper($pel[COL_PLSNAMA])?></td>
              </tr>
              <tr>
                <td colspan="3">
                  <p>Selanjutnya disebut Pihak Pertama</p>
                </td>
              </tr>
              <tr>
                  <td style="padding-left: 25px; width: 10px; white-space: nowrap">Nama</td>
                  <td style="width: 60px; text-align: right">:</td>
                  <td><?=$pel['UnitNamaPimpinan']?></td>
              </tr>
              <tr>
                  <td style="padding-left: 25px; width: 10px; white-space: nowrap">Jabatan</td>
                  <td style="width: 60px; text-align: right">:</td>
                  <td><?=strtoupper(!empty($pel['UnitNamaJabatan'])?$pel['UnitNamaJabatan']:'KEPALA '.$pel['UnitNama'])?></td>
              </tr>
              <tr>
                <td colspan="3">
                  <p>Selaku atasan Pihak Pertama, selanjutnya disebut Pihak Kedua</p>
                </td>
              </tr>
              <tr>
                <td colspan="3">
                  <p>
                      Pihak Pertama berjanji akan mewujudkan target kinerja yang seharusnya sesuai lampiran perjanjian ini,
                      dalam rangka mencapai target kinerja jangka menengah seperti yang telah ditetapkan dalam dokumen perencanaan. Keberhasilan dan kegagalan pencapaian target kinerja tersebut menjadi tanggung jawab kami.
                  </p><br />
                  <p>
                      Pihak Kedua akan melakukan supervisi yang diperlukan serta akan melakukan evaluasi terhadap capaian kinerja dari perjanjian ini dan
                      mengambil tindakan yang diperlukan dalam rangka pemberian penghargaan dan sanksi.
                  </p>
                </td>
              </tr>
          </table>
      </td>
    </tr>
  </table>
  <br />
  <table width="100%">
    <tr>
        <td style="text-align: center">Pihak Kedua,</td>
        <td style="text-align: center">Pihak Pertama,</td>
    </tr>
    <tr>
        <td colspan="2">
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
        </td>
    </tr>
    <tr>
        <td style="text-align: center"><strong><?=$pel['UnitNamaPimpinan']?></strong></td>
        <td style="text-align: center"><strong><?=$pel[COL_PLSNAMAPEGAWAI]?></strong></td>
    </tr>
  </table>
  <pagebreak></pagebreak>
  <table width="100%">
    <tr>
        <td colspan="2" style="text-align: center; vertical-align: top">
            <h4>
                PERJANJIAN KINERJA <?=isset($isDPAPerubahan)&&$isDPAPerubahan?'PERUBAHAN':''?> TAHUN <?=$rdpa[COL_DPATAHUN]?>
                <br />
                <?=strtoupper($rskpd[COL_SKPDNAMA])?>
                <br />
                KOTA TEBING TINGGI
            </h4>
        </td>
    </tr>
  </table>
  <br />
  <table style="border: 1px solid #000; border-spacing: 0" border="1" width="100%">
    <thead>
      <tr>
          <th>No.</th>
          <th>Sasaran</th>
      </tr>
      <tr>
          <th>(1)</th>
          <th>(2)</th>
      </tr>
    </thead>
    <tbody>
      <?php
      $n=1;
      foreach($rfungsi as $s) {
        ?>
        <tr>
          <td style="text-align: right"><?=$n?>.</td>
          <td><?=strtoupper($s)?></td>
        </tr>
        <?php
        $n++;
      }
      ?>
    </tbody>
  </table>
  <br />
  <table style="border: 1px solid #000; border-spacing: 0" border="1" width="100%">
    <thead>
      <tr>
          <th>No.</th>
          <th>Indikator Kinerja</th>
          <!--<th>Formulasi</th>-->
          <th>Target</th>
      </tr>
      <tr>
          <th>(1)</th>
          <th>(2)</th>
          <!--<th>(3)</th>-->
          <th>(4)</th>
      </tr>
    </thead>
    <tbody>
      <?php
      $n=1;
      foreach($rsasaran as $s) {
        ?>
        <tr>
          <td style="text-align: right"><?=$n?>.</td>
          <td><?=strtoupper($s->Uraian)?></td>
          <!--<td><?=$s->Formulasi?></td>-->
          <td style="text-align: right"><?=$s->Target.' ('.$s->Satuan.')'?></td>
        </tr>
        <?php
        $n++;
      }
      ?>
      <tr style="border: none">
        <td colspan="4">
          <table width="100%" style="margin-top: 50px !important">
            <tr>
                <td style="text-align: center">Pihak Kedua,</td>
                <td style="text-align: center">Pihak Pertama,</td>
            </tr>
            <tr>
                <td colspan="2">
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                </td>
            </tr>
            <tr>
              <td style="text-align: center"><strong><?=$pel['UnitNamaPimpinan']?></strong></td>
              <td style="text-align: center"><strong><?=$pel[COL_PLSNAMAPEGAWAI]?></strong></td>
            </tr>
          </table>
        </td>
      </tr>
    </tbody>
  </table>
  <?php
}
?>
<!-- INDIVIDU -->
</body>
<?php
}
?>
