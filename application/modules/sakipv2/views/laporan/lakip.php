<?php
$ruser = GetLoggedUser();

$rOptSkpd = $this->db
->where(COL_SKPDISAKTIF, 1)
->order_by(COL_SKPDURUSAN, 'asc')
->order_by(COL_SKPDBIDANG, 'asc')
->order_by(COL_SKPDUNIT, 'asc')
->order_by(COL_SKPDSUBUNIT, 'asc')
->get(TBL_SAKIPV2_SKPD)
->result_array();

$getIdSasaranPmd = '';
$getSkpd = '';
$getRenstra = '';
$getDPA = '';

if(!empty($_GET['idSasaranPmd'])) $getIdSasaranPmd = $_GET['idSasaranPmd'];

if(!empty($_GET['idSKPD'])) $getSkpd = $_GET['idSKPD'];
else if(!empty($rOptSkpd)) $getSkpd = $rOptSkpd[0][COL_SKPDID];

if($ruser[COL_ROLEID]!=ROLEADMIN && $ruser[COL_ROLEID]!=ROLEGUEST) {
  $getSkpd=$ruser[COL_SKPDID];
}

$rOptRenstra = array();
if(!empty($getSkpd)) {
  $rOptRenstra = $this->db
  ->where(COL_IDPEMDA, $rpemda[COL_PMDID])
  ->where(COL_IDSKPD, $getSkpd)
  //->where(COL_RENSTRAISAKTIF, 1)
  ->order_by(COL_RENSTRAISAKTIF, 'desc')
  ->order_by(COL_RENSTRATAHUN, 'desc')
  ->order_by(COL_RENSTRAID, 'desc')
  ->get(TBL_SAKIPV2_SKPD_RENSTRA)
  ->result_array();
}

if(!empty($_GET['idRenstra'])) $getRenstra = $_GET['idRenstra'];
else if(!empty($rOptRenstra)) $getRenstra = $rOptRenstra[0][COL_RENSTRAID];

$rOptDpa = array();
if(!empty($getRenstra)) {
  $rOptDpa = $this->db
  ->where(COL_IDRENSTRA, $getRenstra)
  ->where(COL_DPAISAKTIF, 1)
  ->order_by(COL_DPAISAKTIF, 'desc')
  ->order_by(COL_DPATAHUN,'desc')
  ->get(TBL_SAKIPV2_SKPD_RENSTRA_DPA)
  ->result_array();
}

$getDPA = null;
if(!empty($_GET['idDPA'])) {
  $getDPA = $_GET['idDPA'];
} else if(!empty($rOptDpa) && $rOptDpa[0][COL_DPAISAKTIF]==1) {
  $getDPA = $rOptDpa[0][COL_DPAID];
}

$rSasaran = array();
if(!empty($getSkpd) && !empty($getRenstra)) {
  if(!empty($getIdSasaranPmd)) {
    $this->db->where(TBL_SAKIPV2_SKPD_RENSTRA_SASARAN.'.'.COL_IDSASARANPMD, $getIdSasaranPmd);
  }

  $rSasaran = $this->db
  ->select("*,
    (select sum(sakipv2_subbid_subkegiatan.SubkegPagu)
      from sakipv2_subbid_subkegiatan
      left join sakipv2_bid_kegiatan keg on keg.KegiatanId = sakipv2_subbid_subkegiatan.IdKegiatan
      left join sakipv2_bid_program prg on prg.ProgramId = keg.IdProgram
      where
        prg.IdSasaranSkpd = sakipv2_skpd_renstra_sasaran.SasaranId
        and prg.IdDPA = $getDPA
      ) as SasaranPagu,
    (select sum(sakipv2_subbid_subkegiatan.SubkegRealisasi)
      from sakipv2_subbid_subkegiatan
      left join sakipv2_bid_kegiatan keg on keg.KegiatanId = sakipv2_subbid_subkegiatan.IdKegiatan
      left join sakipv2_bid_program prg on prg.ProgramId = keg.IdProgram
      where
        prg.IdSasaranSkpd = sakipv2_skpd_renstra_sasaran.SasaranId
        and prg.IdDPA = $getDPA
      ) as SasaranPaguRealisasi")
  ->join(TBL_SAKIPV2_SKPD_RENSTRA_TUJUAN,TBL_SAKIPV2_SKPD_RENSTRA_TUJUAN.'.'.COL_TUJUANID." = ".TBL_SAKIPV2_SKPD_RENSTRA_SASARAN.".".COL_IDTUJUAN,"left")
  ->join(TBL_SAKIPV2_SKPD_RENSTRA,TBL_SAKIPV2_SKPD_RENSTRA.'.'.COL_RENSTRAID." = ".TBL_SAKIPV2_SKPD_RENSTRA_TUJUAN.".".COL_IDRENSTRA,"left")
  ->where(COL_IDSKPD, $getSkpd)
  ->where(COL_IDRENSTRA, $getRenstra)
  ->order_by(TBL_SAKIPV2_SKPD_RENSTRA_TUJUAN.'.'.COL_TUJUANNO)
  ->order_by(TBL_SAKIPV2_SKPD_RENSTRA_SASARAN.'.'.COL_SASARANNO)
  ->get(TBL_SAKIPV2_SKPD_RENSTRA_SASARAN)
  ->result_array();
}

$rdpa = array();
if(!empty($getDPA)) {
  $rdpa = $this->db
  ->where(COL_DPAID, $getDPA)
  ->get(TBL_SAKIPV2_SKPD_RENSTRA_DPA)
  ->row_array();
}
?>
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h3 class="m-0 font-weight-light"><?=$title?></h3>
      </div>
      <?php
      if(!empty($navs)) {
        ?>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <?php
            foreach($navs as $n) {
              if(!empty($n['link'])) {
                ?>
                <li class="breadcrumb-item"><a href="<?=$n['link']?>"><?=$n['text']?></a></li>
                <?php
              } else {
                ?>
                <li class="breadcrumb-item active"><?=$n['text']?></li>
                <?php
              }
            }
            ?>
          </ol>
        </div>
        <?php
      }
      ?>
    </div>
  </div>
</div>
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-12">
        <div class="card card-outline card-secondary">
          <div class="card-body p-0">
            <table class="table table-bordered">
              <thead>
                <tr>
                  <td>
                    <div class="form-group row mb-0">
                      <label class="control-label col-lg-2">PERIODE PEMERINTAHAN :</label>
                      <div class="col-lg-10">
                        <p class="font-italic font-weight-bold mb-0" style="line-height: 2; text-decoration: underline">
                          <?=$rpemda[COL_PMDTAHUNMULAI].' - '.$rpemda[COL_PMDTAHUNAKHIR].' '.strtoupper($rpemda[COL_PMDPEJABAT])?>
                        </p>
                      </div>
                    </div>
                    <div class="form-group row">
                      <label class="control-label col-lg-2">SKPD :</label>
                      <div class="col-lg-10">
                        <?php
                        if($ruser[COL_ROLEID] == ROLEADMIN || $ruser[COL_ROLEID] == ROLEGUEST) {
                          ?>
                          <select class="form-control" name="filterSkpd">
                            <?php
                            foreach($rOptSkpd as $opt) {
                              $isSelected = '';
                              if(!empty($getSkpd) && $opt[COL_SKPDID]==$getSkpd) {
                                $isSelected='selected';
                              }
                              ?>
                              <option value="<?=site_url('sakipv2/laporan/lakip').'?idSKPD='.$opt[COL_SKPDID]?>" <?=$isSelected?>>
                                <?=$opt[COL_SKPDURUSAN].'.'.$opt[COL_SKPDBIDANG].'.'.$opt[COL_SKPDUNIT].'.'.$opt[COL_SKPDSUBUNIT].' - '.strtoupper($opt[COL_SKPDNAMA])?>
                              </option>
                              <?php
                            }
                            ?>
                          </select>
                          <?php
                        } else {
                          $ropd = $this->db
                          ->where(COL_SKPDID, $ruser[COL_SKPDID])
                          ->get(TBL_SAKIPV2_SKPD)
                          ->row_array();
                          ?>
                          <p class="font-italic font-weight-bold mb-0" style="line-height: 2; text-decoration: underline">
                            <?=strtoupper($ropd[COL_SKPDNAMA])?>
                          </p>
                          <?php
                        }
                        ?>
                      </div>
                    </div>
                    <div class="form-group row">
                      <label class="control-label col-lg-2">RENSTRA SKPD :</label>
                      <div class="col-lg-10">
                        <select class="form-control" name="filterRenstra">
                          <?php
                          foreach($rOptRenstra as $opt) {
                            $isSelected = '';
                            if(!empty($getRenstra) && $opt[COL_RENSTRAID]==$getRenstra) {
                              $isSelected='selected';
                            }
                            ?>
                            <option value="<?=site_url('sakipv2/laporan/lakip').'?idSKPD='.$getSkpd.'&idRenstra='.$opt[COL_RENSTRAID]?>" <?=$isSelected?>>
                              <?=$opt[COL_RENSTRATAHUN].' - '.strtoupper($opt[COL_RENSTRAURAIAN])?>
                            </option>
                            <?php
                          }
                          ?>
                        </select>
                      </div>
                    </div>
                    <div class="form-group row">
                      <label class="control-label col-lg-2">DPA SKPD :</label>
                      <div class="col-lg-10">
                        <select class="form-control" name="filterDPA">
                          <?php
                          foreach($rOptDpa as $opt) {
                            ?>
                            <option value="<?=site_url('sakipv2/laporan/lakip').'?idSKPD='.$getSkpd.'&idRenstra='.$getRenstra.'&idDPA='.$opt[COL_DPAID]?>" <?=$opt[COL_DPAID]==$getDPA?'selected':''?>>
                              <?=$opt[COL_DPATAHUN].' - '.strtoupper($opt[COL_DPAURAIAN])?>
                            </option>
                            <?php
                          }
                          ?>
                        </select>
                      </div>
                    </div>
                  </td>
                </tr>
              </thead>
            </table>
          </div>
        </div>
        <div class="card card-outline card-secondary">
          <div class="card-body">
            <form action="<?=current_url()?>" method="POST">
              <input type="hidden" name="idSKPD" value="<?=$getSkpd?>" />
              <input type="hidden" name="idRenstra" value="<?=$getRenstra?>" />
              <input type="hidden" name="idDPA" value="<?=$getDPA?>" />
              <div class="row">
                <div class="col-sm-12 text-right">
                  <button type="submit" class="btn btn-sm btn-success"><i class="far fa-save"></i>&nbsp;SIMPAN & CETAK</button>
                </div>
              </div>
              <ul class="nav nav-tabs" id="tab-main" role="tablist">
                <li class="nav-item">
                  <a class="nav-link font-weight-bold active" id="tab-main-1" data-toggle="pill" href="#tab-content-1" role="tab">BAB. I PENDAHULUAN</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link font-weight-bold" id="tab-main-2" data-toggle="pill" href="#tab-content-2" role="tab">BAB. II PERENCANAAN KINERJA</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link font-weight-bold" id="tab-main-3" data-toggle="pill" href="#tab-content-3" role="tab">BAB. III AKUNTABILITAS KINERJA</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link font-weight-bold" id="tab-main-4" data-toggle="pill" href="#tab-content-4" role="tab">BAB. IV PENUTUP</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link font-weight-bold" id="tab-efisiensi" data-toggle="pill" href="#tab-content-5" role="tab">TABEL EFISIENSI</a>
                </li>
              </ul>
              <div class="tab-content" id="tab-main">
                <div class="tab-pane fade show active" id="tab-content-1" role="tabpanel">
                  <h6 class="mt-2 font-weight-bold">1.1 Latar Belakang</h6>
                  <textarea class="form-control" id="<?=COL_LK11?>" name="<?=COL_LK11?>" rows="5"><?=!empty($rdpa)?$rdpa[COL_LK11]:''?></textarea><br />
                  <h6 class="mt-2 font-weight-bold">1.2 Struktur Organisasi dan Tata Kerja</h6>
                  <textarea class="form-control" id="<?=COL_LK12?>" name="<?=COL_LK12?>" rows="5"><?=!empty($rdpa)?$rdpa[COL_LK12]:''?></textarea><br />
                  <h6 class="mt-2 font-weight-bold">1.3 Aspek Strategis dan Permasalahan Utama</h6>
                  <textarea class="form-control" id="<?=COL_LK13?>" name="<?=COL_LK13?>" rows="5"><?=!empty($rdpa)?$rdpa[COL_LK13]:''?></textarea><br />
                  <h6 class="mt-2 font-weight-bold">1.4 Landasan Hukum</h6>
                  <textarea class="form-control" id="<?=COL_LK14?>" name="<?=COL_LK14?>" rows="5"><?=!empty($rdpa)?$rdpa[COL_LK14]:''?></textarea><br />
                  <h6 class="mt-2 font-weight-bold">1.5 Sistematika</h6>
                  <p class="font-italic">-OTOMATIS DIGENERATE-</p>
                </div>
                <div class="tab-pane fade" id="tab-content-2" role="tabpanel">
                  <h6 class="mt-2 font-weight-bold">2.1 Visi</h6>
                  <p><?=$rpemda[COL_PMDVISI]?></p>
                  <h6 class="mt-2 font-weight-bold">2.1 Misi</h6>
                  <?php
                  if(!empty($rmisi)) {
                    ?>
                    <ul class="mt-0">
                      <?php
                      foreach($rmisi as $m) {
                        ?>
                        <li><?=$m[COL_MISIURAIAN]?></li>
                        <?php
                      }
                      ?>
                    </ul>
                    <?php
                  } else {
                    echo '<p>(KOSONG)</p>';
                  }
                  ?>
                  <h6 class="mt-2 font-weight-bold">2.3 Tujuan dan Sasaran</h6>
                  <textarea class="form-control" id="<?=COL_LK23?>" name="<?=COL_LK23?>" rows="5"><?=!empty($rdpa)?$rdpa[COL_LK23]:''?></textarea><br />
                  <h6 class="mt-2 font-weight-bold">2.4 Rencana Kinerja</h6>
                  <textarea class="form-control" id="<?=COL_LK24?>" name="<?=COL_LK24?>" rows="5"><?=!empty($rdpa)?$rdpa[COL_LK24]:''?></textarea><br />
                </div>
                <div class="tab-pane fade" id="tab-content-3" role="tabpanel">
                  <h6 class="mt-2 font-weight-bold">3.1 Capaian Kinerja Organisasi</h6>
                  <textarea class="form-control" id="<?=COL_LK31?>" name="<?=COL_LK31?>" rows="5"><?=!empty($rdpa)?$rdpa[COL_LK31]:''?></textarea><br />
                  <h6 class="mt-2 font-weight-bold">3.2 Realisasi Anggaran</h6>
                  <textarea class="form-control" id="<?=COL_LK32?>" name="<?=COL_LK32?>" rows="5"><?=!empty($rdpa)?$rdpa[COL_LK32]:''?></textarea><br />
                </div>
                <div class="tab-pane fade" id="tab-content-4" role="tabpanel">
                  <textarea class="form-control" id="<?=COL_LK4?>" name="<?=COL_LK4?>" rows="5"><?=!empty($rdpa)?$rdpa[COL_LK4]:''?></textarea>
                </div>
                <div class="tab-pane fade" id="tab-content-5" role="tabpanel">
                  <a class="btn btn-primary btn-sm mt-3" href="<?=site_url('sakipv2/laporan/efisiensi-partial').'?idSKPD='.$getSkpd.'&idRenstra='.$getRenstra.'&idDPA='.$getDPA?>" target="_blank"><i class="far fa-file-excel"></i>&nbsp;&nbsp;EXCEL</a>
                  <div class="table-responsive mt-3">
                    <?=$this->load->view('sakipv2/laporan/efisiensi', array('rdpa'=>$rdpa,'getDPA'=>$getDPA,'rSasaran'=>$rSasaran))?>
                  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<script type="text/javascript">
$(document).ready(function(){
  /*CKEDITOR.config.toolbarGroups = [
		{ name: 'clipboard', groups: [ 'clipboard', 'undo' ] },
		{ name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
		//{ name: 'paragraph', groups: [ 'list', 'indent', 'align', 'bidi', 'blocks', 'paragraph' ] },
		{ name: 'insert', groups: [ 'insert' ] },
		{ name: 'styles', groups: [ 'styles' ] },
		{ name: 'document', groups: [ 'mode', 'document', 'doctools' ] },
		{ name: 'editing', groups: [ 'find', 'selection', 'spellchecker', 'editing' ] },
		{ name: 'forms', groups: [ 'forms' ] },
		{ name: 'links', groups: [ 'links' ] },
		//'/',
		{ name: 'colors', groups: [ 'colors' ] },
		{ name: 'tools', groups: [ 'tools' ] },
		{ name: 'others', groups: [ 'others' ] },
		{ name: 'about', groups: [ 'about' ] }
	];
  CKEDITOR.config.removeButtons = 'Source,Save,Templates,NewPage,ExportPdf,Preview,Print,Find,Replace,SelectAll,Scayt,CopyFormatting,RemoveFormat,Form,Checkbox,Radio,TextField,Textarea,Select,Button,ImageButton,HiddenField,Blockquote,CreateDiv,BidiLtr,BidiRtl,Language,Link,Unlink,Anchor,Image,Smiley,PageBreak,Iframe,TextColor,BGColor,Maximize,ShowBlocks,About';
  CKEDITOR.config.height = 150;
  CKEDITOR.replace('<?=COL_LK11?>');
  CKEDITOR.replace('<?=COL_LK12?>');
  CKEDITOR.replace('<?=COL_LK13?>');
  CKEDITOR.replace('<?=COL_LK14?>');
  CKEDITOR.replace('<?=COL_LK23?>');
  CKEDITOR.replace('<?=COL_LK24?>');
  CKEDITOR.replace('<?=COL_LK31?>');
  CKEDITOR.replace('<?=COL_LK32?>');
  CKEDITOR.replace('<?=COL_LK4?>');*/
  $('select[name=filterRenstra],select[name=filterSkpd],select[name=filterDPA]').change(function(){
    var url = $(this).val();
    location.href = url;
  });
});
</script>
