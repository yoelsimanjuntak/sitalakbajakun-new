<?php
if(empty($idPmd)) {
  ?>
  <p class="text-center font-italic font-weight-bold text-danger mb-0">
    DATA TIDAK DITEMUKAN
  </p>
  <?php
  exit();
}

$rpemda = $this->db
->where(COL_PMDID, $idPmd)
->get(TBL_SAKIPV2_PEMDA)
->row_array();
if(empty($rpemda)) {
  ?>
  <p class="text-center font-italic font-weight-bold text-danger mb-0">
    PERIODE PEMERINTAHAN TIDAK DITEMUKAN
  </p>
  <?php
}

$rsasaran = $this->db
->join(TBL_SAKIPV2_PEMDA_TUJUAN,TBL_SAKIPV2_PEMDA_TUJUAN.'.'.COL_TUJUANID." = ".TBL_SAKIPV2_PEMDA_SASARAN.".".COL_IDTUJUAN,"left")
->join(TBL_SAKIPV2_PEMDA_MISI,TBL_SAKIPV2_PEMDA_MISI.'.'.COL_MISIID." = ".TBL_SAKIPV2_PEMDA_TUJUAN.".".COL_IDMISI,"left")
->where(COL_IDPMD, $rpemda[COL_PMDID])
->order_by(COL_MISINO)
->order_by(COL_TUJUANNO)
->order_by(COL_SASARANNO)
->get(TBL_SAKIPV2_PEMDA_SASARAN)
->result_array();
?>
<?php
if(!empty($modCetak)) {
  ?>
  <style>
  body {
    font-family: 'Source Sans Pro','Helvetica Neue',Helvetica,Arial,sans-serif;
  }
  th {
    background: #dedede !important;
  }
  th, td {
    font-size: 10pt !important;
    padding: 5px 10px;
  }
  table {
    width: 100%;
    border-collapse: collapse;
  }
  table, th, td {
    border: 1px solid black !important;
  }
  </style>
  <?php

} else {
  ?>
  <style>
  td, th {
    font-size: 10pt !important;
    padding: 5px 10px !important;
  }
  td a {
    text-decoration-line: underline !important;
    text-decoration-style: dotted !important;
  }
  .card-body.p-0 .table tbody>tr>td:first-of-type, .card-body.p-0 .table tbody>tr>th:first-of-type, .card-body.p-0 .table thead>tr>td:first-of-type, .card-body.p-0 .table thead>tr>th:first-of-type, .pl-4, .px-4 {
    padding-left: 10px !important;
  }
  </style>
  <?php
}
?>
<h5 style="text-align: center; margin-top: 2rem; margin-bottom: 0 !important; font-weight: bold; font-size: 12pt">
  TARGET & CAPAIAN<br />
  KINERJA PEMERINTAH DAERAH<br />
  TAHUN <?=$getTahun?>
</h5>
<hr />

<?php
if(empty($modCetak)) {
  ?>
  <p class="font-weight-bold font-italic pl-3 pr-3 mb-0">
    PETUNJUK: <span class="text-danger">Klik pada nilai / angka di kolom RENCANA AKSI dan REALISASI untuk mengubah nilai / angka.</span>
  </p>
  <?php
}
?>

<div class="row" style="margin-top: 1rem">
  <div class="col-lg-12">
    <div class="table-responsive">
      <table class="table table-bordered" autosize="1" style="width: 100% !important">
        <thead>
          <tr>
            <th rowspan="<?=!empty($modCetak)?'2':'3'?>" style="width: 10px; white-space: nowrap; vertical-align: middle">NO.</th>
            <th rowspan="<?=!empty($modCetak)?'2':'3'?>" style="vertical-align: middle">SASARAN</th>
            <th rowspan="<?=!empty($modCetak)?'2':'3'?>" style="vertical-align: middle">INDIKATOR</th>
            <th rowspan="<?=!empty($modCetak)?'2':'3'?>" style="width: 10px; white-space: nowrap; vertical-align: middle">SATUAN</th>
            <th rowspan="<?=!empty($modCetak)?'2':'3'?>" style="width: 10px; white-space: nowrap; vertical-align: middle; text-align: center">TARGET<br /><?=$rpemda[COL_PMDTAHUNAKHIR]?></th>
            <th colspan="<?=!empty($modCetak)?'6':'10'?>" style="vertical-align: middle; text-align: center">
              <?=!empty($modCetak)?($modCetak=='RENCANA'?'RENCANA AKSI':'REALISASI'):'RENCANA AKSI & REALISASI'?>
            </th>
          </tr>
          <tr>
            <th <?=!empty($modCetak)?'':'colspan="2"'?> style="text-align: center">TW-I</th>
            <th <?=!empty($modCetak)?'':'colspan="2"'?> style="text-align: center">TW-II</th>
            <th <?=!empty($modCetak)?'':'colspan="2"'?> style="text-align: center">TW-III</th>
            <th <?=!empty($modCetak)?'':'colspan="2"'?> style="text-align: center">TW-IV</th>
            <th <?=!empty($modCetak)?'':'colspan="2"'?> style="text-align: center"><?=$getTahun?></th>
          </tr>
          <?php
          if(empty($modCetak)) {
            ?>
            <tr>
              <th class="bg-primary disabled" style="text-align: center">TARGET</th>
              <th class="bg-success disabled" style="text-align: center">REALISASI</th>
              <th class="bg-primary disabled" style="text-align: center">TARGET</th>
              <th class="bg-success disabled" style="text-align: center">REALISASI</th>
              <th class="bg-primary disabled" style="text-align: center">TARGET</th>
              <th class="bg-success disabled" style="text-align: center">REALISASI</th>
              <th class="bg-primary disabled" style="text-align: center">TARGET</th>
              <th class="bg-success disabled" style="text-align: center">REALISASI</th>
              <th class="bg-primary disabled" style="text-align: center">TARGET</th>
              <th class="bg-success disabled" style="text-align: center">REALISASI</th>
            </tr>
            <?php
          }
          ?>
        </thead>
        <tbody>
          <?php
          $no=1;
          if(!empty($rsasaran)) {
            foreach($rsasaran as $s) {
              $rdet = $this->db
              ->where(COL_IDSASARAN, $s[COL_SASARANID])
              ->get(TBL_SAKIPV2_PEMDA_SASARANDET)
              ->result_array();

              $noInd = 1;
              $rowspan = count($rdet);
              ?>
              <tr>
                <td <?=$rowspan>1?'rowspan="'.$rowspan.'"':''?> style="font-weight: bold; vertical-align: top; text-align: right"><?=$no?>.</td>
                <td <?=$rowspan>1?'rowspan="'.$rowspan.'"':''?> style="font-weight: bold; vertical-align: top;"><?=strtoupper($s[COL_SASARANURAIAN])?></td>
                <?php
                if(!empty($rdet)) {
                  $rval = $this->db
                  ->where(COL_IDSASARANINDIKATOR, $rdet[0][COL_SSRINDIKATORID])
                  ->where(COL_MONEVTAHUN, $getTahun)
                  ->order_by(COL_UNIQ, 'desc')
                  ->get(TBL_SAKIPV2_PEMDA_SASARANMONEV)
                  ->row_array();

                  $valTargetTW1 = !empty($rval[COL_MONEVTARGETTW1])||$rval[COL_MONEVTARGETTW1]!=null?$rval[COL_MONEVTARGETTW1]:'';
                  $txtTargetTW1 = !empty($rval[COL_MONEVTARGETTW1])||$rval[COL_MONEVTARGETTW1]!=null?$rval[COL_MONEVTARGETTW1]:'N/A';
                  $hrefTargetTW1 = '<a class="btn-changeval" href="'.site_url('sakipv2/pemda/monev-ajax-change-sasaran/'.COL_MONEVTARGETTW1.'/'.$getTahun.'/'.$rdet[0][COL_SSRINDIKATORID]).'" data-value="'.$valTargetTW1.'">'.$txtTargetTW1.'</a>';

                  $valTargetTW2 = !empty($rval[COL_MONEVTARGETTW2])||$rval[COL_MONEVTARGETTW2]!=null?$rval[COL_MONEVTARGETTW2]:'';
                  $txtTargetTW2 = !empty($rval[COL_MONEVTARGETTW2])||$rval[COL_MONEVTARGETTW2]!=null?$rval[COL_MONEVTARGETTW2]:'N/A';
                  $hrefTargetTW2 = '<a class="btn-changeval" href="'.site_url('sakipv2/pemda/monev-ajax-change-sasaran/'.COL_MONEVTARGETTW2.'/'.$getTahun.'/'.$rdet[0][COL_SSRINDIKATORID]).'" data-value="'.$valTargetTW2.'">'.$txtTargetTW2.'</a>';

                  $valTargetTW3 = !empty($rval[COL_MONEVTARGETTW3])||$rval[COL_MONEVTARGETTW3]!=null?$rval[COL_MONEVTARGETTW3]:'';
                  $txtTargetTW3 = !empty($rval[COL_MONEVTARGETTW3])||$rval[COL_MONEVTARGETTW3]!=null?$rval[COL_MONEVTARGETTW3]:'N/A';
                  $hrefTargetTW3 = '<a class="btn-changeval" href="'.site_url('sakipv2/pemda/monev-ajax-change-sasaran/'.COL_MONEVTARGETTW3.'/'.$getTahun.'/'.$rdet[0][COL_SSRINDIKATORID]).'" data-value="'.$valTargetTW3.'">'.$txtTargetTW3.'</a>';

                  $valTargetTW4 = !empty($rval[COL_MONEVTARGETTW4])||$rval[COL_MONEVTARGETTW4]!=null?$rval[COL_MONEVTARGETTW4]:'';
                  $txtTargetTW4 = !empty($rval[COL_MONEVTARGETTW4])||$rval[COL_MONEVTARGETTW4]!=null?$rval[COL_MONEVTARGETTW4]:'N/A';
                  $hrefTargetTW4 = '<a class="btn-changeval" href="'.site_url('sakipv2/pemda/monev-ajax-change-sasaran/'.COL_MONEVTARGETTW4.'/'.$getTahun.'/'.$rdet[0][COL_SSRINDIKATORID]).'" data-value="'.$valTargetTW4.'">'.$txtTargetTW4.'</a>';

                  $valRealTW1 = !empty($rval[COL_MONEVREALISASITW1])||$rval[COL_MONEVREALISASITW1]!=null?$rval[COL_MONEVREALISASITW1]:'';
                  $txtRealTW1 = !empty($rval[COL_MONEVREALISASITW1])||$rval[COL_MONEVREALISASITW1]!=null?$rval[COL_MONEVREALISASITW1]:'N/A';
                  $hrefRealTW1 = '<a class="btn-changeval" href="'.site_url('sakipv2/pemda/monev-ajax-change-sasaran/'.COL_MONEVREALISASITW1.'/'.$getTahun.'/'.$rdet[0][COL_SSRINDIKATORID]).'" data-value="'.$valRealTW1.'">'.$txtRealTW1.'</a>';

                  $valRealTW2 = !empty($rval[COL_MONEVREALISASITW2])||$rval[COL_MONEVREALISASITW2]!=null?$rval[COL_MONEVREALISASITW2]:'';
                  $txtRealTW2 = !empty($rval[COL_MONEVREALISASITW2])||$rval[COL_MONEVREALISASITW2]!=null?$rval[COL_MONEVREALISASITW2]:'N/A';
                  $hrefRealTW2 = '<a class="btn-changeval" href="'.site_url('sakipv2/pemda/monev-ajax-change-sasaran/'.COL_MONEVREALISASITW2.'/'.$getTahun.'/'.$rdet[0][COL_SSRINDIKATORID]).'" data-value="'.$valRealTW2.'">'.$txtRealTW2.'</a>';

                  $valRealTW3 = !empty($rval[COL_MONEVREALISASITW3])||$rval[COL_MONEVREALISASITW3]!=null?$rval[COL_MONEVREALISASITW3]:'';
                  $txtRealTW3 = !empty($rval[COL_MONEVREALISASITW3])||$rval[COL_MONEVREALISASITW3]!=null?$rval[COL_MONEVREALISASITW3]:'N/A';
                  $hrefRealTW3 = '<a class="btn-changeval" href="'.site_url('sakipv2/pemda/monev-ajax-change-sasaran/'.COL_MONEVREALISASITW3.'/'.$getTahun.'/'.$rdet[0][COL_SSRINDIKATORID]).'" data-value="'.$valRealTW3.'">'.$txtRealTW3.'</a>';

                  $valRealTW4 = !empty($rval[COL_MONEVREALISASITW4])||$rval[COL_MONEVREALISASITW4]!=null?$rval[COL_MONEVREALISASITW4]:'';
                  $txtRealTW4 = !empty($rval[COL_MONEVREALISASITW4])||$rval[COL_MONEVREALISASITW4]!=null?$rval[COL_MONEVREALISASITW4]:'N/A';
                  $hrefRealTW4 = '<a class="btn-changeval" href="'.site_url('sakipv2/pemda/monev-ajax-change-sasaran/'.COL_MONEVREALISASITW4.'/'.$getTahun.'/'.$rdet[0][COL_SSRINDIKATORID]).'" data-value="'.$valRealTW4.'">'.$txtRealTW4.'</a>';

                  $valTarget = !empty($rval[COL_MONEVTARGET])||$rval[COL_MONEVTARGET]!=null?$rval[COL_MONEVTARGET]:'';
                  $txtTarget = !empty($rval[COL_MONEVTARGET])||$rval[COL_MONEVTARGET]!=null?$rval[COL_MONEVTARGET]:'N/A';
                  $hrefTarget = '<a class="btn-changeval" href="'.site_url('sakipv2/pemda/monev-ajax-change-sasaran/'.COL_MONEVTARGET.'/'.$getTahun.'/'.$rdet[0][COL_SSRINDIKATORID]).'" data-value="'.$valTarget.'">'.$txtTarget.'</a>';

                  $valReal = !empty($rval[COL_MONEVREALISASI])||$rval[COL_MONEVREALISASI]!=null?$rval[COL_MONEVREALISASI]:'';
                  $txtReal = !empty($rval[COL_MONEVREALISASI])||$rval[COL_MONEVREALISASI]!=null?$rval[COL_MONEVREALISASI]:'N/A';
                  $hrefReal = '<a class="btn-changeval" href="'.site_url('sakipv2/pemda/monev-ajax-change-sasaran/'.COL_MONEVREALISASI.'/'.$getTahun.'/'.$rdet[0][COL_SSRINDIKATORID]).'" data-value="'.$valReal.'">'.$txtReal.'</a>';
                  ?>
                  <td style="vertical-align: top;"><?=strtoupper($rdet[0][COL_SSRINDIKATORURAIAN])?></td>
                  <td style="vertical-align: middle; white-space: nowrap"><?=strtoupper($rdet[0][COL_SSRINDIKATORSATUAN])?></td>
                  <td style="text-align: center; vertical-align: middle; white-space: nowrap"><?=strtoupper($rdet[0][COL_SSRINDIKATORTARGET])?></td>
                  <?php
                  if(!empty($modCetak) && $modCetak=='RENCANA') {
                    ?>
                    <td style="text-align: center; vertical-align: middle; white-space: nowrap"><?=empty($modCetak)?$hrefTargetTW1:$txtTargetTW1?></td>
                    <td style="text-align: center; vertical-align: middle; white-space: nowrap"><?=empty($modCetak)?$hrefTargetTW2:$txtTargetTW2?></td>
                    <td style="text-align: center; vertical-align: middle; white-space: nowrap"><?=empty($modCetak)?$hrefTargetTW3:$txtTargetTW3?></td>
                    <td style="text-align: center; vertical-align: middle; white-space: nowrap"><?=empty($modCetak)?$hrefTargetTW4:$txtTargetTW4?></td>
                    <td style="text-align: center; vertical-align: middle; white-space: nowrap"><?=empty($modCetak)?$hrefTarget:$txtTarget?></td>
                    <?php
                  } else if(!empty($modCetak) && $modCetak=='REALISASI') {
                    ?>
                    <td style="text-align: center; vertical-align: middle; white-space: nowrap"><?=empty($modCetak)?$hrefRealTW1:$txtRealTW1?></td>
                    <td style="text-align: center; vertical-align: middle; white-space: nowrap"><?=empty($modCetak)?$hrefRealTW2:$txtRealTW2?></td>
                    <td style="text-align: center; vertical-align: middle; white-space: nowrap"><?=empty($modCetak)?$hrefRealTW3:$txtRealTW3?></td>
                    <td style="text-align: center; vertical-align: middle; white-space: nowrap"><?=empty($modCetak)?$hrefRealTW4:$txtRealTW4?></td>
                    <td style="text-align: center; vertical-align: middle; white-space: nowrap"><?=empty($modCetak)?$hrefReal:$txtReal?></td>
                    <?php
                  } else {
                    ?>
                    <td class="bg-primary disabled" style="text-align: center; vertical-align: middle; white-space: nowrap"><?=empty($modCetak)?$hrefTargetTW1:$txtTargetTW1?></td>
                    <td class="bg-success disabled" style="text-align: center; vertical-align: middle; white-space: nowrap"><?=empty($modCetak)?$hrefRealTW1:$txtRealTW1?></td>
                    <td class="bg-primary disabled" style="text-align: center; vertical-align: middle; white-space: nowrap"><?=empty($modCetak)?$hrefTargetTW2:$txtTargetTW2?></td>
                    <td class="bg-success disabled" style="text-align: center; vertical-align: middle; white-space: nowrap"><?=empty($modCetak)?$hrefRealTW2:$txtRealTW2?></td>
                    <td class="bg-primary disabled" style="text-align: center; vertical-align: middle; white-space: nowrap"><?=empty($modCetak)?$hrefTargetTW3:$txtTargetTW3?></td>
                    <td class="bg-success disabled" style="text-align: center; vertical-align: middle; white-space: nowrap"><?=empty($modCetak)?$hrefRealTW3:$txtRealTW3?></td>
                    <td class="bg-primary disabled" style="text-align: center; vertical-align: middle; white-space: nowrap"><?=empty($modCetak)?$hrefTargetTW4:$txtTargetTW4?></td>
                    <td class="bg-success disabled" style="text-align: center; vertical-align: middle; white-space: nowrap"><?=empty($modCetak)?$hrefRealTW4:$txtRealTW4?></td>
                    <td class="bg-primary disabled" style="text-align: center; vertical-align: middle; white-space: nowrap"><?=empty($modCetak)?$hrefTarget:$txtTarget?></td>
                    <td class="bg-success disabled" style="text-align: center; vertical-align: middle; white-space: nowrap"><?=empty($modCetak)?$hrefReal:$txtReal?></td>
                    <?php
                  }
                  ?>
                  <?php
                } else {
                  ?>
                  <td colspan="13" style="text-align: center; vertical-align: middle; white-space: nowrap">(KOSONG)</td>
                  <?php
                }
                ?>
              </tr>
              <?php
              if(!empty($rdet)&&count($rdet)>1) {
                for($i=1; $i<count($rdet); $i++) {
                  $rval = $this->db
                  ->where(COL_IDSASARANINDIKATOR, $rdet[$i][COL_SSRINDIKATORID])
                  ->where(COL_MONEVTAHUN, $getTahun)
                  ->order_by(COL_UNIQ, 'desc')
                  ->get(TBL_SAKIPV2_PEMDA_SASARANMONEV)
                  ->row_array();
                  $q = $this->db->last_query();

                  $valTargetTW1 = !empty($rval[COL_MONEVTARGETTW1])||$rval[COL_MONEVTARGETTW1]!=null?$rval[COL_MONEVTARGETTW1]:'';
                  $txtTargetTW1 = !empty($rval[COL_MONEVTARGETTW1])||$rval[COL_MONEVTARGETTW1]!=null?$rval[COL_MONEVTARGETTW1]:'N/A';
                  $hrefTargetTW1 = '<a class="btn-changeval" href="'.site_url('sakipv2/pemda/monev-ajax-change-sasaran/'.COL_MONEVTARGETTW1.'/'.$getTahun.'/'.$rdet[$i][COL_SSRINDIKATORID]).'" data-value="'.$q.'">'.$txtTargetTW1.'</a>';

                  $valTargetTW2 = !empty($rval[COL_MONEVTARGETTW2])||$rval[COL_MONEVTARGETTW2]!=null?$rval[COL_MONEVTARGETTW2]:'';
                  $txtTargetTW2 = !empty($rval[COL_MONEVTARGETTW2])||$rval[COL_MONEVTARGETTW2]!=null?$rval[COL_MONEVTARGETTW2]:'N/A';
                  $hrefTargetTW2 = '<a class="btn-changeval" href="'.site_url('sakipv2/pemda/monev-ajax-change-sasaran/'.COL_MONEVTARGETTW2.'/'.$getTahun.'/'.$rdet[$i][COL_SSRINDIKATORID]).'" data-value="'.$valTargetTW2.'">'.$txtTargetTW2.'</a>';

                  $valTargetTW3 = !empty($rval[COL_MONEVTARGETTW3])||$rval[COL_MONEVTARGETTW3]!=null?$rval[COL_MONEVTARGETTW3]:'';
                  $txtTargetTW3 = !empty($rval[COL_MONEVTARGETTW3])||$rval[COL_MONEVTARGETTW3]!=null?$rval[COL_MONEVTARGETTW3]:'N/A';
                  $hrefTargetTW3 = '<a class="btn-changeval" href="'.site_url('sakipv2/pemda/monev-ajax-change-sasaran/'.COL_MONEVTARGETTW3.'/'.$getTahun.'/'.$rdet[$i][COL_SSRINDIKATORID]).'" data-value="'.$valTargetTW3.'">'.$txtTargetTW3.'</a>';

                  $valTargetTW4 = !empty($rval[COL_MONEVTARGETTW4])||$rval[COL_MONEVTARGETTW4]!=null?$rval[COL_MONEVTARGETTW4]:'';
                  $txtTargetTW4 = !empty($rval[COL_MONEVTARGETTW4])||$rval[COL_MONEVTARGETTW4]!=null?$rval[COL_MONEVTARGETTW4]:'N/A';
                  $hrefTargetTW4 = '<a class="btn-changeval" href="'.site_url('sakipv2/pemda/monev-ajax-change-sasaran/'.COL_MONEVTARGETTW4.'/'.$getTahun.'/'.$rdet[$i][COL_SSRINDIKATORID]).'" data-value="'.$valTargetTW4.'">'.$txtTargetTW4.'</a>';

                  $valRealTW1 = !empty($rval[COL_MONEVREALISASITW1])||$rval[COL_MONEVREALISASITW1]!=null?$rval[COL_MONEVREALISASITW1]:'';
                  $txtRealTW1 = !empty($rval[COL_MONEVREALISASITW1])||$rval[COL_MONEVREALISASITW1]!=null?$rval[COL_MONEVREALISASITW1]:'N/A';
                  $hrefRealTW1 = '<a class="btn-changeval" href="'.site_url('sakipv2/pemda/monev-ajax-change-sasaran/'.COL_MONEVREALISASITW1.'/'.$getTahun.'/'.$rdet[$i][COL_SSRINDIKATORID]).'" data-value="'.$valRealTW1.'">'.$txtRealTW1.'</a>';

                  $valRealTW2 = !empty($rval[COL_MONEVREALISASITW2])||$rval[COL_MONEVREALISASITW2]!=null?$rval[COL_MONEVREALISASITW2]:'';
                  $txtRealTW2 = !empty($rval[COL_MONEVREALISASITW2])||$rval[COL_MONEVREALISASITW2]!=null?$rval[COL_MONEVREALISASITW2]:'N/A';
                  $hrefRealTW2 = '<a class="btn-changeval" href="'.site_url('sakipv2/pemda/monev-ajax-change-sasaran/'.COL_MONEVREALISASITW2.'/'.$getTahun.'/'.$rdet[$i][COL_SSRINDIKATORID]).'" data-value="'.$valRealTW2.'">'.$txtRealTW2.'</a>';

                  $valRealTW3 = !empty($rval[COL_MONEVREALISASITW3])||$rval[COL_MONEVREALISASITW3]!=null?$rval[COL_MONEVREALISASITW3]:'';
                  $txtRealTW3 = !empty($rval[COL_MONEVREALISASITW3])||$rval[COL_MONEVREALISASITW3]!=null?$rval[COL_MONEVREALISASITW3]:'N/A';
                  $hrefRealTW3 = '<a class="btn-changeval" href="'.site_url('sakipv2/pemda/monev-ajax-change-sasaran/'.COL_MONEVREALISASITW3.'/'.$getTahun.'/'.$rdet[$i][COL_SSRINDIKATORID]).'" data-value="'.$valRealTW3.'">'.$txtRealTW3.'</a>';

                  $valRealTW4 = !empty($rval[COL_MONEVREALISASITW4])||$rval[COL_MONEVREALISASITW4]!=null?$rval[COL_MONEVREALISASITW4]:'';
                  $txtRealTW4 = !empty($rval[COL_MONEVREALISASITW4])||$rval[COL_MONEVREALISASITW4]!=null?$rval[COL_MONEVREALISASITW4]:'N/A';
                  $hrefRealTW4 = '<a class="btn-changeval" href="'.site_url('sakipv2/pemda/monev-ajax-change-sasaran/'.COL_MONEVREALISASITW4.'/'.$getTahun.'/'.$rdet[$i][COL_SSRINDIKATORID]).'" data-value="'.$valRealTW4.'">'.$txtRealTW4.'</a>';

                  $valTarget = !empty($rval[COL_MONEVTARGET])||$rval[COL_MONEVTARGET]!=null?$rval[COL_MONEVTARGET]:'';
                  $txtTarget = !empty($rval[COL_MONEVTARGET])||$rval[COL_MONEVTARGET]!=null?$rval[COL_MONEVTARGET]:'N/A';
                  $hrefTarget = '<a class="btn-changeval" href="'.site_url('sakipv2/pemda/monev-ajax-change-sasaran/'.COL_MONEVTARGET.'/'.$getTahun.'/'.$rdet[$i][COL_SSRINDIKATORID]).'" data-value="'.$valTarget.'">'.$txtTarget.'</a>';

                  $valReal = !empty($rval[COL_MONEVREALISASI])||$rval[COL_MONEVREALISASI]!=null?$rval[COL_MONEVREALISASI]:'';
                  $txtReal = !empty($rval[COL_MONEVREALISASI])||$rval[COL_MONEVREALISASI]!=null?$rval[COL_MONEVREALISASI]:'N/A';
                  $hrefReal = '<a class="btn-changeval" href="'.site_url('sakipv2/pemda/monev-ajax-change-sasaran/'.COL_MONEVREALISASI.'/'.$getTahun.'/'.$rdet[$i][COL_SSRINDIKATORID]).'" data-value="'.$valReal.'">'.$txtReal.'</a>';
                  ?>
                  <tr>
                    <td style="vertical-align: top"><?=strtoupper($rdet[$i][COL_SSRINDIKATORURAIAN])?></td>
                    <td style="vertical-align: middle; white-space: nowrap"><?=strtoupper($rdet[$i][COL_SSRINDIKATORSATUAN])?></td>
                    <td style="text-align: center; vertical-align: middle; white-space: nowrap"><?=strtoupper($rdet[$i][COL_SSRINDIKATORTARGET])?></td>
                    <?php
                    if(!empty($modCetak) && $modCetak=='RENCANA') {
                      ?>
                      <td style="text-align: center; vertical-align: middle; white-space: nowrap"><?=empty($modCetak)?$hrefTargetTW1:$txtTargetTW1?></td>
                      <td style="text-align: center; vertical-align: middle; white-space: nowrap"><?=empty($modCetak)?$hrefTargetTW2:$txtTargetTW2?></td>
                      <td style="text-align: center; vertical-align: middle; white-space: nowrap"><?=empty($modCetak)?$hrefTargetTW3:$txtTargetTW3?></td>
                      <td style="text-align: center; vertical-align: middle; white-space: nowrap"><?=empty($modCetak)?$hrefTargetTW4:$txtTargetTW4?></td>
                      <td style="text-align: center; vertical-align: middle; white-space: nowrap"><?=empty($modCetak)?$hrefTarget:$txtTarget?></td>
                      <?php
                    } else if(!empty($modCetak) && $modCetak=='REALISASI') {
                      ?>
                      <td style="text-align: center; vertical-align: middle; white-space: nowrap"><?=empty($modCetak)?$hrefRealTW1:$txtRealTW1?></td>
                      <td style="text-align: center; vertical-align: middle; white-space: nowrap"><?=empty($modCetak)?$hrefRealTW2:$txtRealTW2?></td>
                      <td style="text-align: center; vertical-align: middle; white-space: nowrap"><?=empty($modCetak)?$hrefRealTW3:$txtRealTW3?></td>
                      <td style="text-align: center; vertical-align: middle; white-space: nowrap"><?=empty($modCetak)?$hrefRealTW4:$txtRealTW4?></td>
                      <td style="text-align: center; vertical-align: middle; white-space: nowrap"><?=empty($modCetak)?$hrefReal:$txtReal?></td>
                      <?php
                    } else {
                      ?>
                      <td class="bg-primary disabled" style="text-align: center; vertical-align: middle; white-space: nowrap"><?=empty($modCetak)?$hrefTargetTW1:$txtTargetTW1?></td>
                      <td class="bg-success disabled" style="text-align: center; vertical-align: middle; white-space: nowrap"><?=empty($modCetak)?$hrefRealTW1:$txtRealTW1?></td>
                      <td class="bg-primary disabled" style="text-align: center; vertical-align: middle; white-space: nowrap"><?=empty($modCetak)?$hrefTargetTW2:$txtTargetTW2?></td>
                      <td class="bg-success disabled" style="text-align: center; vertical-align: middle; white-space: nowrap"><?=empty($modCetak)?$hrefRealTW2:$txtRealTW2?></td>
                      <td class="bg-primary disabled" style="text-align: center; vertical-align: middle; white-space: nowrap"><?=empty($modCetak)?$hrefTargetTW3:$txtTargetTW3?></td>
                      <td class="bg-success disabled" style="text-align: center; vertical-align: middle; white-space: nowrap"><?=empty($modCetak)?$hrefRealTW3:$txtRealTW3?></td>
                      <td class="bg-primary disabled" style="text-align: center; vertical-align: middle; white-space: nowrap"><?=empty($modCetak)?$hrefTargetTW4:$txtTargetTW4?></td>
                      <td class="bg-success disabled" style="text-align: center; vertical-align: middle; white-space: nowrap"><?=empty($modCetak)?$hrefRealTW4:$txtRealTW4?></td>
                      <td class="bg-primary disabled" style="text-align: center; vertical-align: middle; white-space: nowrap"><?=empty($modCetak)?$hrefTarget:$txtTarget?></td>
                      <td class="bg-success disabled" style="text-align: center; vertical-align: middle; white-space: nowrap"><?=empty($modCetak)?$hrefReal:$txtReal?></td>
                      <?php
                    }
                    ?>
                  </tr>
                  <?php
                }
              }
              $no++;
            }
          } else {
            ?>
            <tr>
              <td colspan="15">
                <p style="text-align: center; font-style: italic; margin-bottom: 0 !important">
                  (KOSONG)
                </p>
              </td>
            </tr>
            <?php
          }
          ?>
        </tbody>
      </table>
    </div>

  </div>
</div>
