<?php
$rmisi = $this->db
->where(COL_IDPMD, $rperiod[COL_PMDID])
->order_by(COL_MISINO)
->get(TBL_SAKIPV2_PEMDA_MISI)
->result_array();
?>
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h3 class="m-0 font-weight-light"><?=$title?></h3>
      </div>
      <?php
      if(!empty($navs)) {
        ?>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <?php
            foreach($navs as $n) {
              if(!empty($n['link'])) {
                ?>
                <li class="breadcrumb-item"><a href="<?=$n['link']?>"><?=$n['text']?></a></li>
                <?php
              } else {
                ?>
                <li class="breadcrumb-item active"><?=$n['text']?></li>
                <?php
              }
            }
            ?>
          </ol>
        </div>
        <?php
      }
      ?>
    </div>
  </div>
</div>
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-12">
        <div class="card card-outline card-secondary">
          <?php
          if(!empty($subtitle)) {
            ?>
            <div class="card-header">
              <h4 class="card-title"><?=$subtitle?></h4>
            </div>
            <?php
          }
          ?>
          <div class="card-body p-0">
            <table class="table table-striped">
              <tbody>
                <tr>
                  <td class="text-right"  style="width: 200px; white-space: nowrap">PERIODE</td>
                  <td style="width: 10px; white-space: nowrap">:</td>
                  <td><strong><?=$rperiod[COL_PMDTAHUNMULAI]?></strong> - <strong><?=$rperiod[COL_PMDTAHUNAKHIR]?></strong></td>
                </tr>
                <tr>
                  <td class="text-right" style="width: 200px; white-space: nowrap">KEPALA DAERAH</td>
                  <td style="width: 10px; white-space: nowrap">:</td>
                  <td><strong><?=$rperiod[COL_PMDPEJABAT]?></strong></td>
                </tr>
                <tr>
                  <td class="text-right" style="width: 200px; white-space: nowrap">WAKIL KEPALA DAERAH</td>
                  <td style="width: 10px; white-space: nowrap">:</td>
                  <td><strong><?=$rperiod[COL_PMDPEJABATWAKIL]?></strong></td>
                </tr>
                <tr>
                  <td class="text-right" style="width: 200px; white-space: nowrap">VISI</td>
                  <td style="width: 10px; white-space: nowrap">:</td>
                  <td><strong><?=$rperiod[COL_PMDVISI]?></strong></td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>

        <div class="card card-outline card-secondary">
          <div class="card-header">
            <h4 class="card-title">DAFTAR MISI</h4>
          </div>
          <div class="card-body p-0">
            <table class="table table-bordered">
              <thead>
                <tr>
                  <th style="width: 10px; white-space: nowrap">NO</th>
                  <th>URAIAN MISI</th>
                  <th style="width: 100px; white-space: nowrap">AKSI</th>
                </tr>
              </thead>
              <tbody>
                <?php
                if(!empty($rmisi)) {
                  foreach($rmisi as $r) {
                    ?>
                    <tr>
                      <td style="width: 10px; white-space: nowrap"><?=$r[COL_MISINO]?></td>
                      <td><?=mb_strtoupper($r[COL_MISIURAIAN])?></td>
                      <td class="text-center" style="white-space: nowrap">
                        <a href="<?=site_url('sakipv2/pemda/ajax-form-misi/edit/'.$r[COL_IDPMD].'/'.$r[COL_MISIID])?>" data-toggle="tooltip" data-placement="bottom" title="UBAH" class="btn btn-primary btn-sm btn-edit-misi"><i class="far fa-edit"></i></a>
                        <a href="<?=site_url('sakipv2/pemda/ajax-change-misi/delete/'.$r[COL_MISIID])?>" data-toggle="tooltip" data-placement="bottom" title="HAPUS" data-prompt="Menghapus data misi akan otomatis menghapus data TUJUAN dan SASARAN terkait." class="btn btn-danger btn-sm btn-change-misi"><i class="far fa-times-circle"></i></a>
                        <a href="<?=site_url('sakipv2/pemda/index').'?opr=detail-misi&id='.$r[COL_MISIID]?>" data-toggle="tooltip" data-placement="bottom" title="TELUSURI" class="btn btn-info btn-sm"><i class="far fa-search"></i></a>
                      </td>
                    </tr>
                    <?php
                  }
                } else {
                  ?>
                  <tr>
                    <td colspan="3">
                      <p class="text-center font-italic mb-0">
                        BELUM ADA DATA
                      </p>
                    </td>
                  </tr>
                  <?php
                }
                ?>
              </tbody>
            </table>
          </div>
          <div class="card-footer">
            <a href="<?=site_url('sakipv2/pemda/ajax-form-misi/add/'.$rperiod[COL_PMDID])?>" class="btn btn-primary btn-add-misi font-weight-bold"><i class="far fa-plus-circle"></i>&nbsp;TAMBAH MISI</a>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<div class="modal fade" id="modalFormMisi" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">FORM MISI PEMERINTAH DAERAH</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true"><i class="fas fa-close"></i></span>
          </button>
        </div>
        <div class="modal-body">
        </div>
        <div class="modal-footer d-block">
          <div class="row">
            <div class="col-lg-12 text-center">
              <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="far fa-times-circle"></i>&nbsp;BATAL</button>
              <button type="submit" class="btn btn-primary btn-submit"><i class="far fa-plus-circle"></i>&nbsp;SUBMIT</button>
            </div>
          </div>
        </div>
    </div>
  </div>
</div>
<script type="text/javascript">
var modalFormMisi = $('#modalFormMisi');
$(document).ready(function() {
  modalFormMisi.on('hidden.bs.modal', function (e) {
    $('.modal-body', modalFormMisi).empty();
  });

  $('.btn-add-misi, .btn-edit-misi').click(function() {
    var url = $(this).attr('href');
    modalFormMisi.modal('show');
    $('.modal-body', modalFormMisi).html('<p class="font-italic mb-0 text-center"><i class="far fa-circle-notch fa-spin"></i>&nbsp;MEMUAT...</p>');
    $('.modal-body', modalFormMisi).load(url, function(){
      $('button[type=submit]', modalFormMisi).unbind('click').click(function(){
        $('form', modalFormMisi).submit();
      });
    });
    return false;
  });

  $('.btn-change-misi').click(function() {
    var url = $(this).attr('href');
    var prompt = $(this).data('prompt');
    swal({
      title: "APAKAH ANDA YAKIN?",
      text: prompt,
      icon: "warning",
      buttons: [
        'BATAL',
        'YAKIN'
      ],
    }).then(function(isConfirm) {
      if (isConfirm) {
        $.ajax({
          url: url,
          method: "GET",
          dataType: "json"
        }).success(function(res) {
          if(res.error) {
            swal({
              title: 'ERROR',
              text: res.error,
              icon: 'error',
              buttons:false
            });
          } else {
            location.reload();
          }
        }).fail(function() {
          swal({
            title: 'SERVER ERROR',
            text: 'Terjadi kesalahan pada server. Silakan coba kembali.',
            icon: 'error',
            buttons:false
          });
        }).done(function() {

        });

      } else {

      }
    })
    return false;
  });
});
</script>
