<?php
class Home extends MY_Controller {
  function __construct() {
    parent::__construct();
  }

  public function _404() {
    $this->load->view('sakipv2/home/404');
  }

  public function index() {
    $data['title'] = 'Beranda';
    $this->template->load('sakip', 'sakipv2/home/index', $data);
  }

  public function doc($type, $param=null) {
    $data['title'] = 'Dokumen SAKIP';
    $rdoc = $this->db->where(COL_DOCLABEL, $type)->get(TBL_SAKIPV2_PUBLICDOCS)->row_array();
    if(empty($rdoc)) {
      show_error('Parameter tidak valid!');
      exit();
    }

    $data['title'] = $rdoc[COL_DOCNAME];
    $data['param'] = $param;
    $this->template->load('sakip-doc', 'sakipv2/home/doc-'.$type, $data);
  }

  public function renstra($id) {
    $rrenstra = $this->db
    ->where(COL_RENSTRAID, $id)
    ->get(TBL_SAKIPV2_SKPD_RENSTRA)
    ->row_array();
    if(empty($rrenstra)) {
      show_error('Parameter tidak valid!');
      exit();
    }

    $data['title'] = 'Dokumen Renstra';
    $data['rrenstra'] = $rrenstra;
    $this->template->load('sakip-doc', 'sakipv2/home/renstra', $data);
  }

  public function renja($id, $param) {
    $rrenja = $this->db
    ->where(COL_DPAID, $id)
    ->where(COL_DPAISAKTIF, 1)
    ->get(TBL_SAKIPV2_SKPD_RENSTRA_DPA)
    ->row_array();
    if(empty($rrenja)) {
      echo 'Parameter tidak valid!';
      exit();
    }

    $rdata = array();
    if($param=='program') {
      $rdata = $this->db
      ->select('ProgramKode as Kode, ProgramUraian as Uraian, SasaranUraian as Sasaran, SasaranIndikator as Indikator, SasaranTarget as Target, SasaranSatuan as Satuan')
      ->join(TBL_SAKIPV2_BID_PROGRAM,TBL_SAKIPV2_BID_PROGRAM.'.'.COL_PROGRAMID." = ".TBL_SAKIPV2_BID_PROGSASARAN.".".COL_IDPROGRAM,"left")
      ->join(TBL_SAKIPV2_BID,TBL_SAKIPV2_BID.'.'.COL_BIDID." = ".TBL_SAKIPV2_BID_PROGRAM.".".COL_IDBID,"left")
      ->where(TBL_SAKIPV2_BID_PROGRAM.'.'.COL_IDDPA, $rrenja[COL_DPAID])
      ->where(TBL_SAKIPV2_BID.'.'.COL_BIDISAKTIF, 1)
      ->order_by(TBL_SAKIPV2_BID_PROGRAM.'.'.COL_PROGRAMKODE)
      ->group_by(TBL_SAKIPV2_BID_PROGRAM.'.'.COL_PROGRAMKODE)
      ->get(TBL_SAKIPV2_BID_PROGSASARAN)
      ->result_array();
    } else if($param=='kegiatan') {
      $rdata = $this->db
      ->select('KegiatanKode as Kode, KegiatanUraian as Uraian, SasaranUraian as Sasaran, SasaranIndikator as Indikator, SasaranTarget as Target, SasaranSatuan as Satuan')
      ->join(TBL_SAKIPV2_BID_KEGIATAN,TBL_SAKIPV2_BID_KEGIATAN.'.'.COL_KEGIATANID." = ".TBL_SAKIPV2_BID_KEGSASARAN.".".COL_IDKEGIATAN,"left")
      ->join(TBL_SAKIPV2_BID_PROGRAM,TBL_SAKIPV2_BID_PROGRAM.'.'.COL_PROGRAMID." = ".TBL_SAKIPV2_BID_KEGIATAN.".".COL_IDPROGRAM,"left")
      ->join(TBL_SAKIPV2_BID,TBL_SAKIPV2_BID.'.'.COL_BIDID." = ".TBL_SAKIPV2_BID_PROGRAM.".".COL_IDBID,"left")
      ->where(TBL_SAKIPV2_BID_PROGRAM.'.'.COL_IDDPA, $rrenja[COL_DPAID])
      ->where(TBL_SAKIPV2_BID.'.'.COL_BIDISAKTIF, 1)
      ->group_by(array(TBL_SAKIPV2_BID_PROGRAM.'.'.COL_PROGRAMKODE,TBL_SAKIPV2_BID_KEGIATAN.'.'.COL_KEGIATANKODE))
      ->get(TBL_SAKIPV2_BID_KEGSASARAN)
      ->result_array();
    } else if($param=='subkegiatan') {
      $rdata = $this->db
      ->select('SubkegKode as Kode, SubkegUraian as Uraian, SasaranUraian as Sasaran, SasaranIndikator as Indikator, SasaranTarget as Target, SasaranSatuan as Satuan')
      ->join(TBL_SAKIPV2_SUBBID_SUBKEGIATAN,TBL_SAKIPV2_SUBBID_SUBKEGIATAN.'.'.COL_SUBKEGID." = ".TBL_SAKIPV2_SUBBID_SUBKEGSASARAN.".".COL_IDSUBKEG,"left")
      ->join(TBL_SAKIPV2_BID_KEGIATAN,TBL_SAKIPV2_BID_KEGIATAN.'.'.COL_KEGIATANID." = ".TBL_SAKIPV2_SUBBID_SUBKEGIATAN.".".COL_IDKEGIATAN,"left")
      ->join(TBL_SAKIPV2_BID_PROGRAM,TBL_SAKIPV2_BID_PROGRAM.'.'.COL_PROGRAMID." = ".TBL_SAKIPV2_BID_KEGIATAN.".".COL_IDPROGRAM,"left")
      ->join(TBL_SAKIPV2_BID,TBL_SAKIPV2_BID.'.'.COL_BIDID." = ".TBL_SAKIPV2_BID_PROGRAM.".".COL_IDBID,"left")
      ->join(TBL_SAKIPV2_SUBBID,TBL_SAKIPV2_SUBBID.'.'.COL_SUBBIDID." = ".TBL_SAKIPV2_SUBBID_SUBKEGIATAN.".".COL_IDSUBBID,"left")
      ->where(TBL_SAKIPV2_BID_PROGRAM.'.'.COL_IDDPA, $rrenja[COL_DPAID])
      ->where(TBL_SAKIPV2_BID.'.'.COL_BIDISAKTIF, 1)
      ->where("(sakipv2_subbid.SubbidId is null or sakipv2_subbid.SubbidIsAktif=1)")
      ->group_by(array(TBL_SAKIPV2_BID_PROGRAM.'.'.COL_PROGRAMKODE,TBL_SAKIPV2_BID_KEGIATAN.'.'.COL_KEGIATANKODE,TBL_SAKIPV2_SUBBID_SUBKEGIATAN.'.'.COL_SUBKEGKODE))
      ->get(TBL_SAKIPV2_SUBBID_SUBKEGSASARAN)
      ->result_array();
    }

    $this->load->view('sakipv2/home/renja', array('rdata'=>$rdata));
  }
}
?>
