<?php
include_once(APPPATH."third_party/PhpOffice/PhpWord/Autoloader.php");

use PhpOffice\PhpWord\Autoloader;
Autoloader::register();
class Laporan extends MY_Controller {
  function __construct() {
      parent::__construct();
      if(!IsLogin()) {
        redirect(site_url());
      }
  }

  public function index($page) {
    $ruser = GetLoggedUser();
    $data['page'] = $page;
    switch ($page) {
      case 'pemda-cascading':
        $data['title'] = 'Cascading Pemerintah Daerah';
        $this->template->load('main', 'sakipv2/laporan/index-pemda', $data);
      break;

      case 'pemda-iku':
        $data['title'] = 'Indikator Kinerja Utama (IKU) Pemerintah Daerah';
        $this->template->load('main', 'sakipv2/laporan/index-pemda', $data);
      break;

      case 'pemda-iku-cetak':
        $rOptPmd = $this->db
        ->order_by(COL_PMDISAKTIF,'desc')
        ->order_by(COL_PMDTAHUNMULAI,'desc')
        ->get(TBL_SAKIPV2_PEMDA)
        ->result_array();

        $getPmd = null;
        if(!empty($_GET['idPmd'])) $getPmd = $_GET['idPmd'];
        else if(!empty($rOptPmd)) $getPmd = $rOptPmd[0][COL_PMDID];

        if(empty($getPmd)) {
          show_error('PARAMETER TIDAK VALID');
          exit();
        }

        $rpemda = $this->db
        ->where(COL_PMDID, $getPmd)
        ->get(TBL_SAKIPV2_PEMDA)
        ->row_array();

        $data['idPmd'] = $getPmd;
        $data['isCetak'] = 1;

        $this->load->library('Mypdf');
        $mpdf = new Mypdf('utf-8', 'A4-L', 0,'',15,15,30,16);

        $html = $this->load->view('sakipv2/laporan/pemda-iku', $data, TRUE);
        $htmlLogo = MY_IMAGEPATH.'logo.png';
        $htmlTitle = 'INDIKATOR KINERJA';
        $htmlHeader = @"
        <table style=\"border: none !important\">
          <tr>
            <td rowspan=\"2\" style=\"border: none !important; padding: 0 !important; width: 30px; white-space: nowrap; vertical-align: top\"><img src=\"$htmlLogo\" style=\"width: 25px\" /></td>
            <td style=\"border: none !important; padding: 0 !important; font-size: 10pt; font-weight: bold\">$htmlTitle</td>
          </tr>
          <tr>
            <td style=\"border: none !important; padding: 0 !important; font-size: 10pt; font-weight: bold\">PEMERINTAH KOTA TEBING TINGGI</td>
          </tr>
        </table>
        <hr />
        ";
        //echo $html;
        //return;
        $mpdf->pdf->SetTitle('Indikator Kinerja Utama (IKU) Pemerintah Daerah');
        $mpdf->pdf->SetHTMLHeader($htmlHeader);
        $mpdf->pdf->SetFooter('Dicetak melalui aplikasi '.$this->setting_web_name.' pada '.date('d-m-Y H:i'));
        $mpdf->pdf->WriteHTML($html);
        $mpdf->pdf->Output('SITALAKBAJAKUN - IKU PEMERINTAH DAERAH PERIODE '.$rpemda[COL_PMDTAHUNMULAI].' s.d '.$rpemda[COL_PMDTAHUNAKHIR].' ('.strtoupper($rpemda[COL_PMDPEJABAT]).')'.'.pdf', 'I');
      break;

      case 'pemda-pk':
        $data['title'] = 'Perjanjian Kinerja Pemerintah Daerah';
        $this->template->load('main', 'sakipv2/laporan/index-pemda', $data);
      break;

      case 'pemda-pk-cetak':
        $rOptPmd = $this->db
        ->order_by(COL_PMDISAKTIF,'desc')
        ->order_by(COL_PMDTAHUNMULAI,'desc')
        ->get(TBL_SAKIPV2_PEMDA)
        ->result_array();

        $getPmd = null;
        if(!empty($_GET['idPmd'])) $getPmd = $_GET['idPmd'];
        else if(!empty($rOptPmd)) $getPmd = $rOptPmd[0][COL_PMDID];

        if(empty($getPmd)) {
          show_error('PARAMETER TIDAK VALID');
          exit();
        }

        $rpemda = $this->db
        ->where(COL_PMDID, $getPmd)
        ->get(TBL_SAKIPV2_PEMDA)
        ->row_array();

        $data['idPmd'] = $getPmd;
        $data['isCetak'] = 1;
        $data['rpemda'] = $rpemda;
        $data['isDPAPerubahan'] = isset($_GET['isDPAPerubahan'])&&$_GET['isDPAPerubahan']?1:0;

        $this->load->library('Mypdf');
        $mpdf = new Mypdf('','A4',0,'',15,15,30,16);

        $html = $this->load->view('sakipv2/laporan/pemda-pk', $data, TRUE);
        $htmlLogo = MY_IMAGEPATH.'logo.png';
        $htmlYear = date('Y');
        $htmlHeader = @"
        <table>
          <tr>
            <td rowspan=\"2\" style=\"padding: 0 !important; width: 30px; white-space: nowrap; vertical-align: top\"><img src=\"$htmlLogo\" style=\"width: 25px\" /></td>
            <td style=\"padding: 0 !important; font-size: 10pt; font-weight: bold\">PERJANJIAN KINERJA PEMERINTAH DAERAH</td>
          </tr>
          <tr>
            <td style=\"padding: 0 !important; font-size: 10pt; font-weight: bold\">KOTA TEBING TINGGI TAHUN $htmlYear</td>
          </tr>
        </table>
        <hr />
        ";
        //echo $html;
        //exit();
        //return;
        //$mpdf->pdf->SetHeader(' PERJANJIAN KINERJA PEMERINTAH DAERAH<br />KOTA TEBING TINGGI TAHUN '.date('Y'));
        $mpdf->pdf->use_kwt = true;
        $mpdf->pdf->SetHTMLHeader($htmlHeader);
        $mpdf->pdf->SetFooter('Dicetak melalui aplikasi '.$this->setting_web_name);
        $mpdf->pdf->WriteHTML($html);
        $mpdf->pdf->SetTitle('Perjanjian Kinerja Pemerintah Daerah');
        $mpdf->pdf->Output('SITALAKBAJAKUN - IKU PEMERINTAH DAERAH PERIODE '.$rpemda[COL_PMDTAHUNMULAI].' s.d '.$rpemda[COL_PMDTAHUNAKHIR].' ('.strtoupper($rpemda[COL_PMDPEJABAT]).')'.'.pdf', 'I');
      break;

      case 'skpd-cascading':
        $data['title'] = 'Cascading Kinerja SKPD';
        $data['isFilterDPA'] = 1;
        $this->template->load('main', 'sakipv2/laporan/index-skpd', $data);
      break;

      case 'skpd-iku':
        $data['title'] = 'Indikator Kinerja Utama (IKU) SKPD';
        $this->template->load('main', 'sakipv2/laporan/index-skpd', $data);
      break;

      case 'skpd-iku-cetak':
        $rpemda = $this->db
        ->where(COL_PMDISAKTIF, 1)
        ->get(TBL_SAKIPV2_PEMDA)
        ->row_array();

        $rOptSkpd = $this->db
        ->where(COL_SKPDISAKTIF, 1)
        ->order_by(COL_SKPDURUSAN, 'asc')
        ->order_by(COL_SKPDBIDANG, 'asc')
        ->order_by(COL_SKPDUNIT, 'asc')
        ->order_by(COL_SKPDSUBUNIT, 'asc')
        ->get(TBL_SAKIPV2_SKPD)
        ->result_array();

        $getSkpd = '';
        $getRenstra = '';
        $getDPA = '';

        if(!empty($_GET['idSKPD'])) $getSkpd = $_GET['idSKPD'];
        else if(!empty($rOptSkpd)) $getSkpd = $rOptSkpd[0][COL_SKPDID];

        if($ruser[COL_ROLEID]!=ROLEADMIN&&$ruser[COL_ROLEID]!=ROLEGUEST) {
          $getSkpd=$ruser[COL_SKPDID];
        }

        $rOptRenstra = array();
        if(!empty($getSkpd)) {
          $rOptRenstra = $this->db
          ->where(COL_IDPEMDA, $rpemda[COL_PMDID])
          ->where(COL_IDSKPD, $getSkpd)
          //->where(COL_RENSTRAISAKTIF, 1)
          ->order_by(COL_RENSTRAISAKTIF, 'desc')
          ->order_by(COL_RENSTRATAHUN, 'desc')
          ->order_by(COL_RENSTRAID, 'desc')
          ->get(TBL_SAKIPV2_SKPD_RENSTRA)
          ->result_array();
        }

        if(!empty($_GET['idRenstra'])) $getRenstra = $_GET['idRenstra'];
        else if(!empty($rOptRenstra)) $getRenstra = $rOptRenstra[0][COL_RENSTRAID];

        $data['idSKPD'] = $getSkpd;
        $data['idRenstra'] = $getRenstra;
        $data['isCetak'] = 1;

        $rskpd = $this->db
        ->where(COL_SKPDID, $getSkpd)
        ->get(TBL_SAKIPV2_SKPD)
        ->row_array();

        $this->load->library('Mypdf');
        $mpdf = new Mypdf('utf-8', 'A4-L', 0,'',15,15,30,16);

        $html = $this->load->view('sakipv2/laporan/skpd-iku', $data, TRUE);
        $htmlLogo = MY_IMAGEPATH.'logo.png';
        $htmlTitle = 'INDIKATOR KINERJA'.(!empty($rskpd)?' '.strtoupper($rskpd[COL_SKPDNAMA]):'');
        $htmlHeader = @"
        <table style=\"border: none !important\">
          <tr>
            <td rowspan=\"2\" style=\"border: none !important; padding: 0 !important; width: 30px; white-space: nowrap; vertical-align: top\"><img src=\"$htmlLogo\" style=\"width: 25px\" /></td>
            <td style=\"border: none !important; padding: 0 !important; font-size: 10pt; font-weight: bold\">$htmlTitle</td>
          </tr>
          <tr>
            <td style=\"border: none !important; padding: 0 !important; font-size: 10pt; font-weight: bold\">PEMERINTAH KOTA TEBING TINGGI</td>
          </tr>
        </table>
        <hr />
        ";
        $mpdf->pdf->SetTitle('Indikator Kinerja Utama (IKU) '.(!empty($rskpd)?$rskpd[COL_SKPDNAMA]:''));
        $mpdf->pdf->SetHTMLHeader($htmlHeader);
        $mpdf->pdf->SetFooter('Dicetak melalui aplikasi '.$this->setting_web_name);
        //echo $html;
        //return;
        $mpdf->pdf->WriteHTML($html);
        //$mpdf->pdf->use_kwt = true;
        $mpdf->pdf->Output('SITALAKBAJAKUN - IKU PERANGKAT DAERAH.pdf', 'I');
      break;

      case 'skpd-pk':
        $data['title'] = 'Perjanjian Kinerja';
        $data['isFilterDPA'] = 1;
        $this->template->load('main', 'sakipv2/laporan/index-skpd', $data);
      break;

      case 'skpd-pk-cetak':
        $rpemda = $this->db
        ->where(COL_PMDISAKTIF, 1)
        ->get(TBL_SAKIPV2_PEMDA)
        ->row_array();

        $rOptSkpd = $this->db
        ->where(COL_SKPDISAKTIF, 1)
        ->order_by(COL_SKPDURUSAN, 'asc')
        ->order_by(COL_SKPDBIDANG, 'asc')
        ->order_by(COL_SKPDUNIT, 'asc')
        ->order_by(COL_SKPDSUBUNIT, 'asc')
        ->get(TBL_SAKIPV2_SKPD)
        ->result_array();

        $getSkpd = '';
        $getRenstra = '';
        $getDPA = '';

        if(!empty($_GET['idSKPD'])) $getSkpd = $_GET['idSKPD'];
        else if(!empty($rOptSkpd)) $getSkpd = $rOptSkpd[0][COL_SKPDID];

        if($ruser[COL_ROLEID]!=ROLEADMIN&&$ruser[COL_ROLEID]!=ROLEGUEST) {
          $getSkpd=$ruser[COL_SKPDID];
        }

        $rOptRenstra = array();
        if(!empty($getSkpd)) {
          $rOptRenstra = $this->db
          ->where(COL_IDPEMDA, $rpemda[COL_PMDID])
          ->where(COL_IDSKPD, $getSkpd)
          //->where(COL_RENSTRAISAKTIF, 1)
          ->order_by(COL_RENSTRAISAKTIF, 'desc')
          ->order_by(COL_RENSTRATAHUN, 'desc')
          ->order_by(COL_RENSTRAID, 'desc')
          ->get(TBL_SAKIPV2_SKPD_RENSTRA)
          ->result_array();
        }

        if(!empty($_GET['idRenstra'])) $getRenstra = $_GET['idRenstra'];
        else if(!empty($rOptRenstra)) $getRenstra = $rOptRenstra[0][COL_RENSTRAID];

        $rOptDpa = array();
        if(!empty($getRenstra)) {
          $rOptDpa = $this->db
          ->where(COL_IDRENSTRA, $getRenstra)
          ->where(COL_DPAISAKTIF, 1)
          ->order_by(COL_DPAISAKTIF, 'desc')
          ->order_by(COL_DPATAHUN,'desc')
          ->get(TBL_SAKIPV2_SKPD_RENSTRA_DPA)
          ->result_array();
        }

        $getDPA = null;
        if(!empty($_GET['idDPA'])) {
          $getDPA = $_GET['idDPA'];
        } else if(!empty($rOptDpa) && $rOptDpa[0][COL_DPAISAKTIF]==1) {
          $getDPA = $rOptDpa[0][COL_DPAID];
        }

        $data['idPmd'] = $rpemda[COL_PMDID];
        $data['idSKPD'] = $getSkpd;
        $data['idRenstra'] = $getRenstra;
        $data['idDPA'] = $getDPA;
        $data['isCetak'] = 1;
        $data['isDPAPerubahan'] = isset($_GET['isDPAPerubahan'])&&$_GET['isDPAPerubahan']?1:0;
        $rskpd = $this->db
        ->where(COL_SKPDID, $getSkpd)
        ->get(TBL_SAKIPV2_SKPD)
        ->row_array();

        $this->load->library('Mypdf');
        $mpdf = new Mypdf();

        $html = $this->load->view('sakipv2/laporan/skpd-pk', $data, TRUE);
        //echo $html;
        //return;

        $mpdf->pdf->use_kwt = true;
        $mpdf->pdf->showWatermarkImage = true;
        $mpdf->pdf->SetWatermarkImage(MY_IMAGEPATH.$this->setting_web_logo, 0.1, array(100,100));
        $mpdf->pdf->WriteHTML($html);
        $mpdf->pdf->SetTitle('Perjanjian Kinerja - '.(!empty($rskpd)?$rskpd[COL_SKPDNAMA]:''));
        $mpdf->pdf->SetFooter('Dicetak melalui aplikasi '.$this->setting_web_name);
        $mpdf->pdf->Output('SITALAKBAJAKUN - PERJANJIAN KINERJA '.(!empty($rskpd)?$rskpd[COL_SKPDNAMA]:'').'.pdf', 'I');
      break;
    }

  }

  public function dpa() {
    $data['title'] = 'Laporan - DPA';

    if(!empty($_GET['idSasaranPmd'])) {
      $data['rpemda'] = $this->db
      ->select('sakipv2_pemda.*')
      ->join(TBL_SAKIPV2_PEMDA_TUJUAN,TBL_SAKIPV2_PEMDA_TUJUAN.'.'.COL_TUJUANID." = ".TBL_SAKIPV2_PEMDA_SASARAN.".".COL_IDTUJUAN,"inner")
      ->join(TBL_SAKIPV2_PEMDA_MISI,TBL_SAKIPV2_PEMDA_MISI.'.'.COL_MISIID." = ".TBL_SAKIPV2_PEMDA_TUJUAN.".".COL_IDMISI,"inner")
      ->join(TBL_SAKIPV2_PEMDA,TBL_SAKIPV2_PEMDA.'.'.COL_PMDID." = ".TBL_SAKIPV2_PEMDA_MISI.".".COL_IDPMD,"inner")
      ->where(COL_SASARANID, $_GET['idSasaranPmd'])
      ->get(TBL_SAKIPV2_PEMDA_SASARAN)
      ->row_array();
    } else {
      $data['rpemda'] = $this->db
      ->where(COL_PMDISAKTIF, 1)
      ->get(TBL_SAKIPV2_PEMDA)
      ->row_array();
    }
    $this->template->load('main', 'sakipv2/laporan/dpa', $data);
  }

  public function lakip() {
    $data['title'] = 'Laporan - LAKIP SKPD';

    if(!empty($_GET['idSasaranPmd'])) {
      $rpemda = $data['rpemda'] = $this->db
      ->select('sakipv2_pemda.*')
      ->join(TBL_SAKIPV2_PEMDA_TUJUAN,TBL_SAKIPV2_PEMDA_TUJUAN.'.'.COL_TUJUANID." = ".TBL_SAKIPV2_PEMDA_SASARAN.".".COL_IDTUJUAN,"inner")
      ->join(TBL_SAKIPV2_PEMDA_MISI,TBL_SAKIPV2_PEMDA_MISI.'.'.COL_MISIID." = ".TBL_SAKIPV2_PEMDA_TUJUAN.".".COL_IDMISI,"inner")
      ->join(TBL_SAKIPV2_PEMDA,TBL_SAKIPV2_PEMDA.'.'.COL_PMDID." = ".TBL_SAKIPV2_PEMDA_MISI.".".COL_IDPMD,"inner")
      ->where(COL_SASARANID, $_GET['idSasaranPmd'])
      ->get(TBL_SAKIPV2_PEMDA_SASARAN)
      ->row_array();
    } else {
      $rpemda = $data['rpemda'] = $this->db
      ->where(COL_PMDISAKTIF, 1)
      ->get(TBL_SAKIPV2_PEMDA)
      ->row_array();
    }

    $rmisi = $data['rmisi'] = $this->db
    ->where(COL_IDPMD, $rpemda[COL_PMDID])
    ->order_by(COL_MISINO)
    ->get(TBL_SAKIPV2_PEMDA_MISI)
    ->result_array();

    if(!empty($_POST)) {
      $getSkpd = $_POST['idSKPD'];
      $getRenstra = $_POST['idRenstra'];
      $getDPA = $_POST['idDPA'];

      $rrenstra = $this->db
      ->where(COL_RENSTRAID, $getRenstra)
      ->get(TBL_SAKIPV2_SKPD_RENSTRA)
      ->row_array();

      $rdpa = $this->db
      ->select('sakipv2_skpd_renstra_dpa.*, sakipv2_skpd.SkpdNama')
      ->where(COL_DPAID, $getDPA)
      ->join(TBL_SAKIPV2_SKPD_RENSTRA,TBL_SAKIPV2_SKPD_RENSTRA.'.'.COL_RENSTRAID." = ".TBL_SAKIPV2_SKPD_RENSTRA_DPA.".".COL_IDRENSTRA,"inner")
      ->join(TBL_SAKIPV2_SKPD,TBL_SAKIPV2_SKPD.'.'.COL_SKPDID." = ".TBL_SAKIPV2_SKPD_RENSTRA.".".COL_IDSKPD,"inner")
      ->get(TBL_SAKIPV2_SKPD_RENSTRA_DPA)
      ->row_array();

      if(!empty($rdpa)) {
        $res = $this->db
        ->where(COL_DPAID, $getDPA)
        ->update(TBL_SAKIPV2_SKPD_RENSTRA_DPA, array(
          COL_LK11=>$this->input->post(COL_LK11),
          COL_LK12=>$this->input->post(COL_LK12),
          COL_LK13=>$this->input->post(COL_LK13),
          COL_LK14=>$this->input->post(COL_LK14),
          COL_LK23=>$this->input->post(COL_LK23),
          COL_LK24=>$this->input->post(COL_LK24),
          COL_LK31=>$this->input->post(COL_LK31),
          COL_LK32=>$this->input->post(COL_LK32),
          COL_LK4=>$this->input->post(COL_LK4),
        ));

        $rrenssr = $this->db
        ->where('IdTujuan in (select TujuanId from sakipv2_skpd_renstra_tujuan t where t.IdRenstra='.$rrenstra[COL_RENSTRAID].')')
        ->join(TBL_SAKIPV2_SKPD_RENSTRA_SASARANDET,TBL_SAKIPV2_SKPD_RENSTRA_SASARANDET.'.'.COL_IDSASARAN." = ".TBL_SAKIPV2_SKPD_RENSTRA_SASARAN.".".COL_SASARANID,"left")
        ->get(TBL_SAKIPV2_SKPD_RENSTRA_SASARAN)
        ->result_array();

        $rprogram = $this->db
        ->select('*,
        (select sum(sakipv2_subbid_subkegiatan.SubkegPagu) from sakipv2_subbid_subkegiatan left join sakipv2_bid_kegiatan keg on keg.KegiatanId = sakipv2_subbid_subkegiatan.IdKegiatan where keg.IdProgram = sakipv2_bid_program.ProgramId) as ProgPagu,
        (select sum(sakipv2_subbid_subkegiatan.SubkegRealisasi) from sakipv2_subbid_subkegiatan left join sakipv2_bid_kegiatan keg on keg.KegiatanId = sakipv2_subbid_subkegiatan.IdKegiatan where keg.IdProgram = sakipv2_bid_program.ProgramId) as ProgRealisasi')
        ->where(COL_IDDPA, $getDPA)
        ->get(TBL_SAKIPV2_BID_PROGRAM)
        ->result_array();

        $phpWord = new \PhpOffice\PhpWord\PhpWord();
        \PhpOffice\PhpWord\Settings::setTempDir(FCPATH.'assets/phpword/tempdir');

        $templateProcessor = new \PhpOffice\PhpWord\TemplateProcessor(FCPATH.'assets/phpword/tempdir/LAKIP-FINAL.docx');

        if(!empty($rrenstra) && !empty($rrenstra[COL_RENSTRAORG])) {
          $templateProcessor->setImageValue('1-2-struktur-org', MY_UPLOADPATH.$rrenstra[COL_RENSTRAORG]);
        }

        $templateProcessor->setValue('lakip-tahun', $rdpa[COL_DPATAHUN]);
        $templateProcessor->setValue('nama-opd', $rdpa[COL_SKPDNAMA]);

        $templateProcessor->setValue('rpjmd-org', ucwords( strtolower($this->setting_org_name)));
        $templateProcessor->setValue('rpjmd-visi', ucwords(strtolower($rpemda[COL_PMDVISI])));
        $templateProcessor->setValue('rpjmd-from', $rpemda[COL_PMDTAHUNMULAI]);
        $templateProcessor->setValue('rpjmd-to', $rpemda[COL_PMDTAHUNAKHIR]);

        if(!empty($rmisi)) {
          $arrmisi = array();
          foreach($rmisi as $m) {
            $arrmisi[] = array('rpjmd-misi-item' => ucwords( strtolower($m[COL_MISIURAIAN])));
          }
          $templateProcessor->cloneBlock('rpjmd-misi', 0, true, false, $arrmisi);
        } else {
          $templateProcessor->deleteBlock('rpjmd-misi');
        }

        $templateProcessor->setValue('1-1', $this->input->post(COL_LK11));
        $templateProcessor->setValue('1-2', $this->input->post(COL_LK12));
        $templateProcessor->setValue('1-3', $this->input->post(COL_LK13));
        $templateProcessor->setValue('1-4', $this->input->post(COL_LK14));
        $templateProcessor->setValue('2-3', $this->input->post(COL_LK23));
        $templateProcessor->setValue('2-4', $this->input->post(COL_LK24));
        $templateProcessor->setValue('3-1', $this->input->post(COL_LK31));
        $templateProcessor->setValue('3-2', $this->input->post(COL_LK32));
        $templateProcessor->setValue('4', $this->input->post(COL_LK4));

        $arr1 = array();
        $arr3 = array();
        $no = 1;
        foreach($rrenssr as $s) {
          $rmonev = $this->db
          ->where(COL_IDSASARANINDIKATOR, $s[COL_SSRINDIKATORID])
          ->where(COL_MONEVTAHUN, $rdpa[COL_DPATAHUN])
          ->get(TBL_SAKIPV2_SKPD_RENSTRA_SASARANMONEV)
          ->row_array();

          $arr1[] = array(
            '11'=>$no,
            'TR12'=>$s[COL_SASARANURAIAN],
            'TR13'=>$s[COL_SSRINDIKATORURAIAN],
            'TR14'=>$s[COL_SSRINDIKATORSATUAN],
            'TR15'=>$s[COL_SSRINDIKATORTARGET],
          );

          $arr3[] = array(
            '31'=>$no,
            'TR32'=>$s[COL_SASARANURAIAN],
            'TR33'=>$s[COL_SSRINDIKATORURAIAN],
            'TR34'=>(!empty($rmonev)&&!empty($rmonev[COL_MONEVTARGET])?$rmonev[COL_MONEVTARGET]:'-'),
            'TR35'=>(!empty($rmonev)&&!empty($rmonev[COL_MONEVREALISASI])?$rmonev[COL_MONEVREALISASI]:'-'),
            'TR36'=>(!empty($rmonev)&&!empty($rmonev[COL_MONEVTARGET])&&!empty($rmonev[COL_MONEVREALISASI])&&is_numeric($rmonev[COL_MONEVTARGET])&&is_numeric($rmonev[COL_MONEVREALISASI])?(floatval($rmonev[COL_MONEVTARGET])>0?number_format(floatval($rmonev[COL_MONEVREALISASI])/floatval($rmonev[COL_MONEVTARGET])*100,2):''):'-')
          );
          $no++;
        }
        if(!empty($rrenssr)) {
          $templateProcessor->cloneRowAndSetValues('11', $arr1);
          $templateProcessor->cloneRowAndSetValues('31', $arr3);
        }

        $arr2 = array();
        $arr4 = array();
        $no_prg = 1;
        foreach($rprogram as $p) {
          $rPrgIndikator = $this->db
          ->where(COL_IDPROGRAM, $p[COL_PROGRAMID])
          ->get(TBL_SAKIPV2_BID_PROGSASARAN)
          ->result_array();
          $txtIndikator = '';
          $txtTarget = '';
          $numIndikator = 1;
          foreach ($rPrgIndikator as $i) {
            $txtIndikator .= strtoupper($i[COL_SASARANINDIKATOR]).($numIndikator==count($rPrgIndikator)?'':', ');
            $txtTarget .= strtoupper($i[COL_SASARANTARGET]).' ('.strtoupper($i[COL_SASARANSATUAN]).')'.($numIndikator==count($rPrgIndikator)?'':', ');
            $numIndikator++;
          }

          $rkegiatan = $this->db
          ->select('*,
          (select sum(sakipv2_subbid_subkegiatan.SubkegPagu) from sakipv2_subbid_subkegiatan where sakipv2_subbid_subkegiatan.IdKegiatan = sakipv2_bid_kegiatan.KegiatanId) as KegPagu,
          (select sum(sakipv2_subbid_subkegiatan.SubkegRealisasi) from sakipv2_subbid_subkegiatan where sakipv2_subbid_subkegiatan.IdKegiatan = sakipv2_bid_kegiatan.KegiatanId) as KegRealisasi')
          ->where(COL_IDPROGRAM, $p[COL_PROGRAMID])
          ->get(TBL_SAKIPV2_BID_KEGIATAN)
          ->result_array();

          $arr2[] = array(
            '21'=>$no_prg,
            'TR22'=>$p[COL_PROGRAMURAIAN],
            'TR23'=>$txtIndikator,
            'TR24'=>$txtTarget,
            'TR25'=>'Rp. '.number_format($p['ProgPagu']),
          );

          $arr4[] = array(
            '41'=>$no_prg,
            'TR42'=>$p[COL_PROGRAMURAIAN],
            'TR43'=>'Rp. '.number_format($p['ProgPagu']),
            'TR44'=>'Rp. '.number_format($p['ProgRealisasi']),
            'TR45'=>!empty($p['ProgPagu'])&&!empty($p['ProgRealisasi'])&&$p['ProgPagu']>0?number_format($p['ProgRealisasi']/$p['ProgPagu']*100,2):'-',
          );

          $no_keg = 1;
          foreach($rkegiatan as $k) {
            $rkegiatansub = $this->db
            ->where(COL_IDKEGIATAN, $k[COL_KEGIATANID])
            ->get(TBL_SAKIPV2_SUBBID_SUBKEGIATAN)
            ->result_array();

            $rKegIndikator = $this->db
            ->where(COL_IDKEGIATAN, $k[COL_KEGIATANID])
            ->get(TBL_SAKIPV2_BID_KEGSASARAN)
            ->result_array();
            $txtIndikator = '';
            $txtTarget = '';
            $numIndikator = 1;
            foreach ($rKegIndikator as $i) {
              $txtIndikator .= strtoupper($i[COL_SASARANINDIKATOR]).($numIndikator==count($rKegIndikator)?'':', ');
              $txtTarget .= strtoupper($i[COL_SASARANTARGET]).' ('.strtoupper($i[COL_SASARANSATUAN]).')'.($numIndikator==count($rKegIndikator)?'':', ');
              $numIndikator++;
            }

            $arr2[] = array(
              '21'=>$no_prg.'.'.$no_keg,
              'TR22'=>$k[COL_KEGIATANURAIAN],
              'TR23'=>$txtIndikator,
              'TR24'=>$txtTarget,
              'TR25'=>'Rp. '.number_format($k['KegPagu']),
            );

            $arr4[] = array(
              '41'=>$no_prg.'.'.$no_keg,
              'TR42'=>$k[COL_KEGIATANURAIAN],
              'TR43'=>'Rp. '.number_format($k['KegPagu']),
              'TR44'=>'Rp. '.number_format($k['KegRealisasi']),
              'TR45'=>!empty($k['KegPagu'])&&!empty($k['KegRealisasi'])&&$k['KegPagu']>0?number_format($k['KegRealisasi']/$k['KegPagu']*100,2):'-',
            );

            $no_sub = 1;
            foreach($rkegiatansub as $sk) {
              $rSubkegIndikator = $this->db
              ->where(COL_IDSUBKEG, $sk[COL_SUBKEGID])
              ->get(TBL_SAKIPV2_SUBBID_SUBKEGSASARAN)
              ->result_array();
              $txtIndikator = '';
              $txtTarget = '';
              $numIndikator = 1;
              foreach ($rSubkegIndikator as $i) {
                $txtIndikator .= strtoupper($i[COL_SASARANINDIKATOR]).($numIndikator==count($rSubkegIndikator)?'':', ');
                $txtTarget .= strtoupper($i[COL_SASARANTARGET]).' ('.strtoupper($i[COL_SASARANSATUAN]).')'.($numIndikator==count($rSubkegIndikator)?'':', ');
                $numIndikator++;
              }

              $arr2[] = array(
                '21'=>$no_prg.'.'.$no_keg.'.'.$no_sub,
                'TR22'=>$sk[COL_SUBKEGURAIAN],
                'TR23'=>$txtIndikator,
                'TR24'=>$txtTarget,
                'TR25'=>'Rp. '.number_format($sk['SubkegPagu']),
              );

              $arr4[] = array(
                '41'=>$no_prg.'.'.$no_keg.'.'.$no_sub,
                'TR42'=>$sk[COL_SUBKEGURAIAN],
                'TR43'=>'Rp. '.number_format($sk['SubkegPagu']),
                'TR44'=>'Rp. '.number_format($sk['SubkegRealisasi']),
                'TR45'=>!empty($sk['SubkegPagu'])&&!empty($sk['SubkegRealisasi'])&&$sk['SubkegPagu']>0?number_format($sk['SubkegRealisasi']/$sk['SubkegPagu']*100,2):'-',
              );

              $no_sub++;
            }

            $no_keg++;
          }
          $no_prg++;
        }
        if(!empty($rprogram)) {
          $templateProcessor->cloneRowAndSetValues('21', $arr2);
          $templateProcessor->cloneRowAndSetValues('41', $arr4);
        }

        $filename = 'LAKIP-'.date('Y-m-d-Hi') . '.docx';
        header("Content-Description: File Transfer");
        header('Content-Type: application/vnd.openxmlformats-officedocument.wordprocessingml.document'); //mime type
        header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
        header('Cache-Control: max-age=0'); //no cache
        header('Content-Transfer-Encoding: binary');
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Expires: 0');
        //$objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');
        //$objWriter->save('php://output');

        $templateProcessor->saveAs('php://output');
      } else {
        redirect(current_url().'?idSKPD='.$getSkpd.'&idRenstra='.$getRenstra.'&idDPA='.$getDPA);
      }
    }

    $this->template->load('main', 'sakipv2/laporan/lakip', $data);
  }

  public function efisiensi_partial() {
    $ruser = GetLoggedUser();
    if(!empty($_GET['idSasaranPmd'])) {
      $rpemda = $data['rpemda'] = $this->db
      ->select('sakipv2_pemda.*')
      ->join(TBL_SAKIPV2_PEMDA_TUJUAN,TBL_SAKIPV2_PEMDA_TUJUAN.'.'.COL_TUJUANID." = ".TBL_SAKIPV2_PEMDA_SASARAN.".".COL_IDTUJUAN,"inner")
      ->join(TBL_SAKIPV2_PEMDA_MISI,TBL_SAKIPV2_PEMDA_MISI.'.'.COL_MISIID." = ".TBL_SAKIPV2_PEMDA_TUJUAN.".".COL_IDMISI,"inner")
      ->join(TBL_SAKIPV2_PEMDA,TBL_SAKIPV2_PEMDA.'.'.COL_PMDID." = ".TBL_SAKIPV2_PEMDA_MISI.".".COL_IDPMD,"inner")
      ->where(COL_SASARANID, $_GET['idSasaranPmd'])
      ->get(TBL_SAKIPV2_PEMDA_SASARAN)
      ->row_array();
    } else {
      $rpemda = $data['rpemda'] = $this->db
      ->where(COL_PMDISAKTIF, 1)
      ->get(TBL_SAKIPV2_PEMDA)
      ->row_array();
    }

    $rOptSkpd = $this->db
    ->where(COL_SKPDISAKTIF, 1)
    ->order_by(COL_SKPDURUSAN, 'asc')
    ->order_by(COL_SKPDBIDANG, 'asc')
    ->order_by(COL_SKPDUNIT, 'asc')
    ->order_by(COL_SKPDSUBUNIT, 'asc')
    ->get(TBL_SAKIPV2_SKPD)
    ->result_array();

    $getIdSasaranPmd = '';
    $getSkpd = '';
    $getRenstra = '';
    $getDPA = '';

    if(!empty($_GET['idSasaranPmd'])) $getIdSasaranPmd = $_GET['idSasaranPmd'];

    if(!empty($_GET['idSKPD'])) $getSkpd = $_GET['idSKPD'];
    else if(!empty($rOptSkpd)) $getSkpd = $rOptSkpd[0][COL_SKPDID];

    if($ruser[COL_ROLEID]!=ROLEADMIN && $ruser[COL_ROLEID]!=ROLEGUEST) {
      $getSkpd=$ruser[COL_SKPDID];
    }

    $rOptRenstra = array();
    if(!empty($getSkpd)) {
      $rOptRenstra = $this->db
      ->where(COL_IDPEMDA, $rpemda[COL_PMDID])
      ->where(COL_IDSKPD, $getSkpd)
      //->where(COL_RENSTRAISAKTIF, 1)
      ->order_by(COL_RENSTRAISAKTIF, 'desc')
      ->order_by(COL_RENSTRATAHUN, 'desc')
      ->order_by(COL_RENSTRAID, 'desc')
      ->get(TBL_SAKIPV2_SKPD_RENSTRA)
      ->result_array();
    }

    if(!empty($_GET['idRenstra'])) $getRenstra = $_GET['idRenstra'];
    else if(!empty($rOptRenstra)) $getRenstra = $rOptRenstra[0][COL_RENSTRAID];

    $rOptDpa = array();
    if(!empty($getRenstra)) {
      $rOptDpa = $this->db
      ->where(COL_IDRENSTRA, $getRenstra)
      ->where(COL_DPAISAKTIF, 1)
      ->order_by(COL_DPAISAKTIF, 'desc')
      ->order_by(COL_DPATAHUN,'desc')
      ->get(TBL_SAKIPV2_SKPD_RENSTRA_DPA)
      ->result_array();
    }

    $getDPA = null;
    if(!empty($_GET['idDPA'])) {
      $getDPA = $_GET['idDPA'];
    } else if(!empty($rOptDpa) && $rOptDpa[0][COL_DPAISAKTIF]==1) {
      $getDPA = $rOptDpa[0][COL_DPAID];
    }

    $rSasaran = array();
    if(!empty($getSkpd) && !empty($getRenstra)) {
      if(!empty($getIdSasaranPmd)) {
        $this->db->where(TBL_SAKIPV2_SKPD_RENSTRA_SASARAN.'.'.COL_IDSASARANPMD, $getIdSasaranPmd);
      }

      $rSasaran = $this->db
      ->select("*,
        (select sum(sakipv2_subbid_subkegiatan.SubkegPagu)
          from sakipv2_subbid_subkegiatan
          left join sakipv2_bid_kegiatan keg on keg.KegiatanId = sakipv2_subbid_subkegiatan.IdKegiatan
          left join sakipv2_bid_program prg on prg.ProgramId = keg.IdProgram
          where
            prg.IdSasaranSkpd = sakipv2_skpd_renstra_sasaran.SasaranId
            and prg.IdDPA = $getDPA
          ) as SasaranPagu,
        (select sum(sakipv2_subbid_subkegiatan.SubkegRealisasi)
          from sakipv2_subbid_subkegiatan
          left join sakipv2_bid_kegiatan keg on keg.KegiatanId = sakipv2_subbid_subkegiatan.IdKegiatan
          left join sakipv2_bid_program prg on prg.ProgramId = keg.IdProgram
          where
            prg.IdSasaranSkpd = sakipv2_skpd_renstra_sasaran.SasaranId
            and prg.IdDPA = $getDPA
          ) as SasaranPaguRealisasi")
      ->join(TBL_SAKIPV2_SKPD_RENSTRA_TUJUAN,TBL_SAKIPV2_SKPD_RENSTRA_TUJUAN.'.'.COL_TUJUANID." = ".TBL_SAKIPV2_SKPD_RENSTRA_SASARAN.".".COL_IDTUJUAN,"left")
      ->join(TBL_SAKIPV2_SKPD_RENSTRA,TBL_SAKIPV2_SKPD_RENSTRA.'.'.COL_RENSTRAID." = ".TBL_SAKIPV2_SKPD_RENSTRA_TUJUAN.".".COL_IDRENSTRA,"left")
      ->where(COL_IDSKPD, $getSkpd)
      ->where(COL_IDRENSTRA, $getRenstra)
      ->order_by(TBL_SAKIPV2_SKPD_RENSTRA_TUJUAN.'.'.COL_TUJUANNO)
      ->order_by(TBL_SAKIPV2_SKPD_RENSTRA_SASARAN.'.'.COL_SASARANNO)
      ->get(TBL_SAKIPV2_SKPD_RENSTRA_SASARAN)
      ->result_array();
    }

    $rdpa = array();
    if(!empty($getDPA)) {
      $rdpa = $this->db
      ->where(COL_DPAID, $getDPA)
      ->get(TBL_SAKIPV2_SKPD_RENSTRA_DPA)
      ->row_array();
    }
    $this->load->view('sakipv2/laporan/efisiensi', array('rdpa'=>$rdpa,'getDPA'=>$getDPA,'rSasaran'=>$rSasaran,'cetak'=>1));
  }
}
?>
