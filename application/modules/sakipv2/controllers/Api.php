<?php
header('Access-Control-Allow-Origin: *');
class Api extends MY_Controller {
  function __construct() {
      parent::__construct();
  }

  public function sakip_summary_skpd() {
    $token = $this->input->post('AuthKey');
    $tahun = $this->input->post('Tahun');

    $ruser = $this->db
    ->join(TBL__USERINFORMATION,TBL__USERINFORMATION.'.'.COL_USERNAME." = ".TBL__USERS.".".COL_USERNAME,"inner")
    ->join(TBL__ROLES,TBL__ROLES.'.'.COL_ROLEID." = ".TBL__USERS.".".COL_ROLEID,"inner")
    ->where("(MD5(".TBL__USERS.".".COL_USERNAME.") = '".$token."' OR MD5(".TBL__USERINFORMATION.".".COL_EMAIL.") = '".$token."')")
    ->get(TBL__USERS)
    ->row_array();

    if(empty($ruser)) {
      ShowJsonError('Parameter tidak valid!');
      exit();
    }

    if($ruser[COL_ROLEID] != ROLEADMIN) {
      $arrUnit = explode('.', $ruser[COL_COMPANYID]);

      $rskpd = $this->db
      ->select('sakipv2_skpd.SkpdId, UPPER(sakipv2_skpd.SkpdNama) as Nama, sakipv2_skpd.SkpdNamaPimpinan as Pimpinan')
      ->where(array(COL_SKPDURUSAN=>$arrUnit[0], COL_SKPDBIDANG=>$arrUnit[1], COL_SKPDUNIT=>$arrUnit[2], COL_SKPDSUBUNIT=>$arrUnit[3]))
      ->where(COL_SKPDISAKTIF, 1)
      ->get(TBL_SAKIPV2_SKPD)
      ->row_array();
      if(empty($rskpd)) {
        ShowJsonError('Unit Kerja tidak ditemukan.');
        exit();
      }

      $rpmd = $this->db
      ->select('sakipv2_pemda.PmdId, sakipv2_pemda.PmdTahunMulai as PeriodeMulai, sakipv2_pemda.PmdTahunAkhir as PeriodeAkhir, sakipv2_pemda.PmdPejabat as KepalaDaerah1, sakipv2_pemda.PmdPejabatWakil as KepalaDaerah2, sakipv2_pemda.PmdVisi as Visi')
      ->where(COL_PMDISAKTIF,1)
      ->where(COL_PMDTAHUNMULAI.' <= ', $tahun)
      ->where(COL_PMDTAHUNAKHIR.' >= ', $tahun)
      ->get(TBL_SAKIPV2_PEMDA)
      ->row_array();
      if(empty($rskpd)) {
        ShowJsonError('RPJMD tidak ditemukan.');
        exit();
      }

      $rmisi = $this->db
      ->select('sakipv2_pemda_misi.MisiNo as No, sakipv2_pemda_misi.MisiUraian as Uraian, sakipv2_pemda_misi.MisiIKU as IKU')
      ->where(COL_IDPMD,$rpmd[COL_PMDID])
      ->order_by(COL_MISINO)
      ->get(TBL_SAKIPV2_PEMDA_MISI)
      ->result_array();

      $rmisi_ = array();
      foreach($rmisi as $m) {
        $arrIKU = !empty($m['IKU'])?json_decode($m['IKU']):array();
        $arrIKU_ = array();
        foreach($arrIKU as $i) {
          $arrIKU_[] = array(
            'Uraian'=>$i->IKUUraian,
            'Formula'=>$i->IKUFormulasi,
            'Target'=>$i->IKUTarget,
            'Satuan'=>$i->IKUTarget
          );
        }
        $misi_[] = array(
          'No'=>$m['No'],
          'Uraian'=>$m['Uraian'],
          'IKU'=>$arrIKU_
        );
      }
      $rpmd['Misi'] = $misi_;

      $rrenstra = $this->db
      ->select('sakipv2_skpd_renstra.RenstraId as RenstraId, sakipv2_skpd_renstra.RenstraUraian as Uraian, sakipv2_skpd_renstra.RenstraTahun as Tahun')
      ->where(COL_RENSTRAISAKTIF,1)
      ->where(COL_IDSKPD, $rskpd[COL_SKPDID])
      ->where(COL_IDPEMDA, $rpmd[COL_PMDID])
      ->order_by(COL_CREATEDON, 'desc')
      ->get(TBL_SAKIPV2_SKPD_RENSTRA)
      ->row_array();
      if(empty($rrenstra)) {
        ShowJsonError('Dokumen Rencana Strategis tidak ditemukan.');
        exit();
      }

      $rdpa = $this->db
      ->select('sakipv2_skpd_renstra_dpa.DPAId, sakipv2_skpd_renstra_dpa.DPAUraian as Uraian, sakipv2_skpd_renstra_dpa.DPATahun as Tahun')
      ->where(COL_DPAISAKTIF,1)
      ->where(COL_IDRENSTRA, $rrenstra[COL_RENSTRAID])
      ->where(COL_DPATAHUN, $tahun)
      ->order_by(COL_CREATEDON, 'desc')
      ->get(TBL_SAKIPV2_SKPD_RENSTRA_DPA)
      ->row_array();

      $arrTujuan = array();
      $arrSasaran = array();
      $arrProgram = array();
      $arrKegiatan = array();
      $arrSubKegiatan = array();

      $rtujuan = $this->db
      ->select('sakipv2_skpd_renstra_tujuan.*, sakipv2_pemda_tujuan.TujuanUraian as TujuanUraianPmd')
      ->join(TBL_SAKIPV2_PEMDA_TUJUAN,TBL_SAKIPV2_PEMDA_TUJUAN.'.'.COL_TUJUANID." = ".TBL_SAKIPV2_SKPD_RENSTRA_TUJUAN.".".COL_IDTUJUANPMD,"left")
      ->where(COL_IDRENSTRA, $rrenstra[COL_RENSTRAID])
      ->order_by(COL_TUJUANNO)
      ->get(TBL_SAKIPV2_SKPD_RENSTRA_TUJUAN)
      ->result_array();
      foreach($rtujuan as $t) {
        $arrTujuanIndikator = $this->db
        ->select('TujIndikatorUraian as Uraian')
        ->where(COL_IDTUJUAN, $t[COL_TUJUANID])
        ->get(TBL_SAKIPV2_SKPD_RENSTRA_TUJUANDET)
        ->result_array();
        $arrTujuan[] = array(
          'No'=>$t[COL_TUJUANNO],
          'Uraian'=>strtoupper($t[COL_TUJUANURAIAN]),
          'UraianRPJMD'=>strtoupper($t['TujuanUraianPmd']),
          'Indikator'=>$arrTujuanIndikator
        );
      }

      $rsasaran = $this->db
      ->select('sakipv2_skpd_renstra_sasaran.*, sakipv2_skpd_renstra_tujuan.TujuanNo, sakipv2_pemda_sasaran.SasaranUraian as SasaranUraianPmd')
      ->join(TBL_SAKIPV2_PEMDA_SASARAN,TBL_SAKIPV2_PEMDA_SASARAN.'.'.COL_SASARANID." = ".TBL_SAKIPV2_SKPD_RENSTRA_SASARAN.".".COL_IDSASARANPMD,"left")
      ->join(TBL_SAKIPV2_SKPD_RENSTRA_TUJUAN,TBL_SAKIPV2_SKPD_RENSTRA_TUJUAN.'.'.COL_TUJUANID." = ".TBL_SAKIPV2_SKPD_RENSTRA_SASARAN.".".COL_IDTUJUAN,"left")
      ->where(TBL_SAKIPV2_SKPD_RENSTRA_TUJUAN.'.'.COL_IDRENSTRA, $rrenstra[COL_RENSTRAID])
      ->order_by(TBL_SAKIPV2_SKPD_RENSTRA_TUJUAN.'.'.COL_TUJUANNO)
      ->order_by(TBL_SAKIPV2_SKPD_RENSTRA_SASARAN.'.'.COL_SASARANNO)
      ->get(TBL_SAKIPV2_SKPD_RENSTRA_SASARAN)
      ->result_array();
      foreach($rsasaran as $s) {
        $arrSasaranIndikator = $this->db
        ->select('SsrIndikatorUraian as Uraian, SsrIndikatorSumberData as SumberData, SsrIndikatorFormulasi as Formulasi, SsrIndikatorSatuan as Satuan, SsrIndikatorTarget as Target')
        ->where(COL_IDSASARAN, $s[COL_SASARANID])
        ->get(TBL_SAKIPV2_SKPD_RENSTRA_SASARANDET)
        ->result_array();
        $arrSasaran[] = array(
          'No'=>$s[COL_TUJUANNO].'.'.$s[COL_SASARANNO],
          'Uraian'=>strtoupper($s[COL_SASARANURAIAN]),
          'UraianRPJMD'=>strtoupper($s['SasaranUraianPmd']),
          'Indikator'=>$arrSasaranIndikator
        );
      }

      $rprogram = $this->db
      ->where(COL_IDDPA, $rdpa[COL_DPAID])
      ->order_by(COL_PROGRAMKODE)
      ->group_by(COL_PROGRAMKODE)
      ->get(TBL_SAKIPV2_BID_PROGRAM)
      ->result_array();
      foreach($rprogram as $prg) {
        $arrProgram[] = array(
          'Kode'=>$prg[COL_PROGRAMKODE],
          'Uraian'=>$prg[COL_PROGRAMURAIAN]
        );
      }

      $json = array(
        'Rpjmd'=>$rpmd,
        'Skpd'=>$rskpd,
        'Renstra'=>$rrenstra,
        'DPA'=>$rdpa,
        'Tujuan'=>$arrTujuan,
        'Sasaran'=>$arrSasaran,
        'Program'=>$arrProgram
      );
      echo json_encode($json);
    }
  }
}
?>
