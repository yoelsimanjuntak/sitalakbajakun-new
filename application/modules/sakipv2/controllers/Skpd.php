<?php
class Skpd extends MY_Controller {
  function __construct() {
    ini_set('memory_limit', '-1');
    parent::__construct();
    if(!IsLogin()) {
      redirect(site_url());
    }
  }

  public function showErrUnathorized() {
    $act = $this->router->fetch_method();
    if($this->input->is_ajax_request() && strpos($act, "_form")===false) {
      ShowJsonError('MAAF, ANDA TIDAK MEMILIKI HAK AKSES.');
      exit();
    }else{
      echo 'MAAF, ANDA TIDAK MEMILIKI HAK AKSES.';
      exit();
    }
  }

  public function index() {
    $ruser = GetLoggedUser();
    $data['title'] = 'Kinerja SKPD';
    $opr = !empty($_GET['opr'])?$_GET['opr']:'';
    $id = !empty($_GET['id'])?$_GET['id']:'';

    switch ($opr) {
      case 'detail-skpd':
        $data['subtitle'] = 'DETIL SKPD';
        $data['ropd'] = $ropd = $this->db
        ->where(COL_SKPDID, $id)
        ->get(TBL_SAKIPV2_SKPD)
        ->row_array();
        if(empty($ropd)) {
          show_error('PARAMETER TIDAK VALID!');
          exit();
        }
        $data['navs'] = array(
          array('text'=>strtoupper($ropd[COL_SKPDNAMA]))
        );
        $this->template->load('main', 'sakipv2/skpd/view-skpd', $data);
      break;

      case 'detail-renstra':
        $data['subtitle'] = 'DETIL RENSTRA';
        $data['rrenstra'] = $rrenstra = $this->db
        ->select('sakipv2_skpd_renstra.*, sakipv2_skpd.SkpdId, sakipv2_skpd.SkpdNama, sakipv2_skpd.SkpdUrusan, sakipv2_skpd.SkpdBidang, sakipv2_skpd.SkpdUnit, sakipv2_skpd.SkpdSubUnit')
        ->join(TBL_SAKIPV2_SKPD,TBL_SAKIPV2_SKPD.'.'.COL_SKPDID." = ".TBL_SAKIPV2_SKPD_RENSTRA.".".COL_IDSKPD,"left")
        ->where(COL_RENSTRAID, $id)
        ->get(TBL_SAKIPV2_SKPD_RENSTRA)
        ->row_array();
        if(empty($rrenstra)) {
          show_error('PARAMETER TIDAK VALID!');
          exit();
        }
        $data['navs'] = array(
          array('text'=>strtoupper($rrenstra[COL_SKPDNAMA]), 'link'=>site_url('sakipv2/skpd/index').'?opr=detail-skpd&id='.$rrenstra[COL_SKPDID]),
          array('text'=>'RENSTRA TH. '.$rrenstra[COL_RENSTRATAHUN])
        );
        $this->template->load('main', 'sakipv2/skpd/view-renstra', $data);
      break;

      case 'detail-tujuan':
        $data['subtitle'] = 'DETIL TUJUAN SKPD';
        $data['rtujuan'] = $rtujuan = $this->db
        ->select('sakipv2_skpd_renstra_tujuan.*, sakipv2_skpd_renstra.*, sakipv2_pemda_tujuan.TujuanNo as PmdTujuanNo, sakipv2_pemda_tujuan.TujuanUraian as PmdTujuanUraian, sakipv2_pemda_misi.*, sakipv2_skpd.SkpdId, sakipv2_skpd.SkpdNama, sakipv2_skpd.SkpdUrusan, sakipv2_skpd.SkpdBidang, sakipv2_skpd.SkpdUnit, sakipv2_skpd.SkpdSubUnit')
        ->join(TBL_SAKIPV2_SKPD_RENSTRA,TBL_SAKIPV2_SKPD_RENSTRA.'.'.COL_RENSTRAID." = ".TBL_SAKIPV2_SKPD_RENSTRA_TUJUAN.".".COL_IDRENSTRA,"left")
        ->join(TBL_SAKIPV2_SKPD,TBL_SAKIPV2_SKPD.'.'.COL_SKPDID." = ".TBL_SAKIPV2_SKPD_RENSTRA.".".COL_IDSKPD,"left")
        ->join(TBL_SAKIPV2_PEMDA_TUJUAN,TBL_SAKIPV2_PEMDA_TUJUAN.'.'.COL_TUJUANID." = ".TBL_SAKIPV2_SKPD_RENSTRA_TUJUAN.".".COL_IDTUJUANPMD,"left")
        ->join(TBL_SAKIPV2_PEMDA_MISI,TBL_SAKIPV2_PEMDA_MISI.'.'.COL_MISIID." = ".TBL_SAKIPV2_PEMDA_TUJUAN.".".COL_IDMISI,"left")
        ->where(TBL_SAKIPV2_SKPD_RENSTRA_TUJUAN.'.'.COL_TUJUANID, $id)
        ->get(TBL_SAKIPV2_SKPD_RENSTRA_TUJUAN)
        ->row_array();
        if(empty($rtujuan)) {
          show_error('PARAMETER TIDAK VALID!');
          exit();
        }
        $data['navs'] = array(
          array('text'=>strtoupper($rtujuan[COL_SKPDNAMA]), 'link'=>site_url('sakipv2/skpd/index').'?opr=detail-skpd&id='.$rtujuan[COL_SKPDID]),
          array('text'=>'RENSTRA TH. '.$rtujuan[COL_RENSTRATAHUN], 'link'=>site_url('sakipv2/skpd/index').'?opr=detail-renstra&id='.$rtujuan[COL_RENSTRAID]),
          array('text'=>'TUJUAN '.$rtujuan[COL_TUJUANNO])
        );
        $this->template->load('main', 'sakipv2/skpd/view-tujuan', $data);
      break;

      case 'detail-sasaran':
        $data['subtitle'] = 'DETIL SASARAN SKPD';
        $data['rsasaran'] = $rsasaran = $this->db
        ->select('sakipv2_skpd_renstra_sasaran.*,
                  sakipv2_skpd_renstra_tujuan.TujuanId,
                  sakipv2_skpd_renstra_tujuan.TujuanNo,
                  sakipv2_skpd_renstra_tujuan.TujuanUraian,
                  sakipv2_skpd_renstra.*,
                  sakipv2_pemda_tujuan.TujuanNo as PmdTujuanNo,
                  sakipv2_pemda_tujuan.TujuanUraian as PmdTujuanUraian,
                  sakipv2_pemda_sasaran.SasaranNo as PmdSasaranNo,
                  sakipv2_pemda_sasaran.SasaranUraian as PmdSasaranUraian,
                  sakipv2_pemda_misi.*,
                  sakipv2_skpd.SkpdId,
                  sakipv2_skpd.SkpdNama,
                  sakipv2_skpd.SkpdUrusan,
                  sakipv2_skpd.SkpdBidang,
                  sakipv2_skpd.SkpdUnit,
                  sakipv2_skpd.SkpdSubUnit')
        ->join(TBL_SAKIPV2_SKPD_RENSTRA_TUJUAN,TBL_SAKIPV2_SKPD_RENSTRA_TUJUAN.'.'.COL_TUJUANID." = ".TBL_SAKIPV2_SKPD_RENSTRA_SASARAN.".".COL_IDTUJUAN,"left")
        ->join(TBL_SAKIPV2_SKPD_RENSTRA,TBL_SAKIPV2_SKPD_RENSTRA.'.'.COL_RENSTRAID." = ".TBL_SAKIPV2_SKPD_RENSTRA_TUJUAN.".".COL_IDRENSTRA,"left")
        ->join(TBL_SAKIPV2_SKPD,TBL_SAKIPV2_SKPD.'.'.COL_SKPDID." = ".TBL_SAKIPV2_SKPD_RENSTRA.".".COL_IDSKPD,"left")
        ->join(TBL_SAKIPV2_PEMDA_SASARAN,TBL_SAKIPV2_PEMDA_SASARAN.'.'.COL_SASARANID." = ".TBL_SAKIPV2_SKPD_RENSTRA_SASARAN.".".COL_IDSASARANPMD,"left")
        ->join(TBL_SAKIPV2_PEMDA_TUJUAN,TBL_SAKIPV2_PEMDA_TUJUAN.'.'.COL_TUJUANID." = ".TBL_SAKIPV2_PEMDA_SASARAN.".".COL_IDTUJUAN,"left")
        ->join(TBL_SAKIPV2_PEMDA_MISI,TBL_SAKIPV2_PEMDA_MISI.'.'.COL_MISIID." = ".TBL_SAKIPV2_PEMDA_TUJUAN.".".COL_IDMISI,"left")
        ->where(TBL_SAKIPV2_SKPD_RENSTRA_SASARAN.'.'.COL_SASARANID, $id)
        ->get(TBL_SAKIPV2_SKPD_RENSTRA_SASARAN)
        ->row_array();
        if(empty($rsasaran)) {
          show_error('PARAMETER TIDAK VALID!');
          exit();
        }
        $data['navs'] = array(
          array('text'=>strtoupper($rsasaran[COL_SKPDNAMA]), 'link'=>site_url('sakipv2/skpd/index').'?opr=detail-skpd&id='.$rsasaran[COL_SKPDID]),
          array('text'=>'RENSTRA TH. '.$rsasaran[COL_RENSTRATAHUN], 'link'=>site_url('sakipv2/skpd/index').'?opr=detail-renstra&id='.$rsasaran[COL_RENSTRAID]),
          array('text'=>'TUJUAN '.$rsasaran[COL_TUJUANNO], 'link'=>site_url('sakipv2/skpd/index').'?opr=detail-tujuan&id='.$rsasaran[COL_TUJUANID]),
          array('text'=>'SASARAN '.$rsasaran[COL_SASARANNO])
        );
        $this->template->load('main', 'sakipv2/skpd/view-sasaran', $data);
      break;

      default:
      if($ruser[COL_ROLEID]==ROLEADMIN || $ruser[COL_ROLEID]==ROLEGUEST) {
        $data['subtitle'] = 'DAFTAR SKPD';
        $this->template->load('main', 'sakipv2/skpd/index-skpd', $data);
      } else {
        redirect(current_url().'?opr=detail-skpd&id='.$ruser[COL_SKPDID]);
      }
      break;
    }
  }

  public function ajax_form_skpd($mode, $id=null) {
    $data['mode'] = $mode;
    $ruser = GetLoggedUser();
    if($ruser[COL_ROLEID]!=ROLEADMIN && $ruser[COL_ROLEID]!=ROLEKADIS) {
      $this->showErrUnathorized();
    }

    if(!empty($_POST)) {
      if($mode=='edit') {
        $this->db->trans_begin();
        try {
          if(empty($id)) {
            throw new Exception('PARAMETER TIDAK VALID');
          }

          $rec = array(
            COL_SKPDNAMAPIMPINAN=>$this->input->post(COL_SKPDNAMAPIMPINAN),
            COL_SKPDNAMAJABATAN=>$this->input->post(COL_SKPDNAMAJABATAN),
            COL_SKPDKOP=>$this->input->post(COL_SKPDKOP),

            COL_UPDATEDBY=>$ruser[COL_USERNAME],
            COL_UPDATEDON=>date('Y-m-d H:i:s')
          );

          $res = $this->db->where(COL_SKPDID, $id)->update(TBL_SAKIPV2_SKPD, $rec);
          if(!$res) {
            $err = $this->db->error();
            throw new Exception($err['message']);
          }

          $this->db->trans_commit();
          ShowJsonSuccess('INPUT DATA BERHASIL');
          exit();
        } catch(Exception $ex) {
          $this->db->trans_rollback();
          ShowJsonError($ex->getMessage());
          exit();
        }
      }
    } else {
      if($mode=='edit') {
        $rdata = $this->db->where(COL_SKPDID, $id)->get(TBL_SAKIPV2_SKPD)->row_array();
        if(empty($rdata)) {
          echo '<p class="text-center text-danger font-weight-bold">PARAMETER TIDAK VALID!</p>';
          exit();
        }
        $data['data'] = $rdata;
      }
      $this->load->view('sakipv2/skpd/form-skpd', $data);
    }
  }

  public function ajax_form_renstra($mode, $id=null, $idSkpd=null) {
    $data['mode'] = $mode;
    $ruser = GetLoggedUser();
    if($ruser[COL_ROLEID]!=ROLEADMIN && $ruser[COL_ROLEID]!=ROLEKADIS) {
      $this->showErrUnathorized();
    }

    if(!empty($_POST)) {
      if($mode=='add') {
        if(empty($id) || empty($idSkpd)) {
          ShowJsonError('PARAMETER TIDAK VALID!');
          exit();
        }

        $this->db->trans_begin();
        try {
          $rec = array(
            COL_IDSKPD=>$idSkpd,
            COL_IDPEMDA=>$id,
            COL_RENSTRATAHUN=>$this->input->post(COL_RENSTRATAHUN),
            COL_RENSTRAURAIAN=>$this->input->post(COL_RENSTRAURAIAN),

            COL_CREATEDBY=>$ruser[COL_USERNAME],
            COL_CREATEDON=>date('Y-m-d H:i:s')
          );

          $res = $this->db
          ->where(COL_IDPEMDA, $rec[COL_IDPEMDA])
          ->where(COL_IDSKPD, $idSkpd)
          ->update(TBL_SAKIPV2_SKPD_RENSTRA, array(COL_RENSTRAISAKTIF=>0));
          if(!$res) {
            $err = $this->db->error();
            throw new Exception($err['message']);
          }

          $res = $this->db->insert(TBL_SAKIPV2_SKPD_RENSTRA, $rec);
          if(!$res) {
            $err = $this->db->error();
            throw new Exception($err['message']);
          }

          $this->db->trans_commit();
          ShowJsonSuccess('INPUT DATA BERHASIL');
          exit();
        } catch(Exception $ex) {
          $this->db->trans_rollback();
          ShowJsonError($ex->getMessage());
          exit();
        }
      } else if($mode=='edit') {
        $this->db->trans_begin();
        try {
          if(empty($id)) {
            throw new Exception('PARAMETER TIDAK VALID');
          }

          $rec = array(
            COL_RENSTRATAHUN=>$this->input->post(COL_RENSTRATAHUN),
            COL_RENSTRAURAIAN=>$this->input->post(COL_RENSTRAURAIAN),

            COL_UPDATEDBY=>$ruser[COL_USERNAME],
            COL_UPDATEDON=>date('Y-m-d H:i:s')
          );

          if(!empty($this->input->post(COL_RENSTRATUGASPOKOK))) {
            $rec[COL_RENSTRATUGASPOKOK] = $this->input->post(COL_RENSTRATUGASPOKOK);
          }

          $arrFungsi = array();
          $arrIKU = array();
          $fungsiUraian = $this->input->post('FungsiUraian');
          $IKUUraian = $this->input->post('IKUUraian');
          $IKUSumberData = $this->input->post('IKUSumberData');
          $IKUFormulasi = $this->input->post('IKUFormulasi');
          $IKUSatuan = $this->input->post('IKUSatuan');
          $IKUTarget = $this->input->post('IKUTarget');

          for($i = 0; $i<count($fungsiUraian); $i++) {
            $arrFungsi[] = $fungsiUraian[$i];
          }
          for($i = 0; $i<count($IKUUraian); $i++) {
            $arrIKU[] = array(
              'Uraian'=>$IKUUraian[$i],
              'SumberData'=>$IKUSumberData[$i],
              'Formulasi'=>$IKUFormulasi[$i],
              'Satuan'=>$IKUSatuan[$i],
              'Target'=>$IKUTarget[$i]
            );
          }

          if(!empty($arrFungsi)) {
            $rec[COL_RENSTRAFUNGSI]=json_encode($arrFungsi);
          }
          if(!empty($arrIKU)) {
            $rec[COL_RENSTRAIKU]=json_encode($arrIKU);
          }

          $res = $this->db->where(COL_RENSTRAID, $id)->update(TBL_SAKIPV2_SKPD_RENSTRA, $rec);
          if(!$res) {
            $err = $this->db->error();
            throw new Exception($err['message']);
          }

          $this->db->trans_commit();
          ShowJsonSuccess('INPUT DATA BERHASIL');
          exit();
        } catch(Exception $ex) {
          $this->db->trans_rollback();
          ShowJsonError($ex->getMessage());
          exit();
        }
      }
    } else {
      if($mode=='edit') {
        $rdata = $this->db->where(COL_RENSTRAID, $id)->get(TBL_SAKIPV2_SKPD_RENSTRA)->row_array();
        if(empty($rdata)) {
          echo '<p class="text-center text-danger font-weight-bold">PARAMETER TIDAK VALID!</p>';
          exit();
        }
        $data['data'] = $rdata;
      }
      $this->load->view('sakipv2/skpd/form-renstra', $data);
    }
  }

  public function ajax_change_renstra($mode, $id)  {
    $ruser = GetLoggedUser();
    if($ruser[COL_ROLEID]!=ROLEADMIN && $ruser[COL_ROLEID]!=ROLEKADIS) {
      $this->showErrUnathorized();
    }

    $rdata = $this->db->where(COL_RENSTRAID, $id)->get(TBL_SAKIPV2_SKPD_RENSTRA)->row_array();
    if($mode=='delete') {
      if(empty($rdata)) {
        howJsonError('PARAMETER TIDAK VALID!');
        exit();
      }

      $res = $this->db->where(COL_RENSTRAID, $id)->delete(TBL_SAKIPV2_SKPD_RENSTRA);
      if(!$res) {
        $err = $this->db->error();
        ShowJsonError($err['message']);
        exit();
      }

      $rexist = $this->db
      ->where(COL_IDPEMDA, $rdata[COL_IDPEMDA])
      ->order_by(COL_RENSTRAISAKTIF,'asc')
      ->order_by(COL_RENSTRATAHUN,'desc')
      ->get(TBL_SAKIPV2_SKPD_RENSTRA)
      ->row_array();

      if(!empty($rexist) && $rexist[COL_RENSTRAISAKTIF]==0) {
        $res = $this->db->where(COL_RENSTRAID, $rexist[COL_RENSTRAID])->update(TBL_SAKIPV2_SKPD_RENSTRA, array(COL_RENSTRAISAKTIF=>1));
        if(!$res) {
          $this->db->trans_rollback();
          $err = $this->db->error();
          ShowJsonError($err['message']);
          exit();
        }
      }
    } else  if($mode=='activate') {
      $this->db->trans_begin();
      $res = $this->db
      ->where(COL_IDPEMDA, $rdata[COL_IDPEMDA])
      ->where(COL_IDSKPD, $rdata[COL_IDSKPD])
      ->where(COL_RENSTRAID.'!=', $id)
      ->update(TBL_SAKIPV2_SKPD_RENSTRA, array(COL_RENSTRAISAKTIF=>0));
      if(!$res) {
        $this->db->trans_rollback();
        $err = $this->db->error();
        ShowJsonError($err['message']);
        exit();
      }
      $res = $this->db->where(COL_RENSTRAID, $id)->update(TBL_SAKIPV2_SKPD_RENSTRA, array(COL_RENSTRAISAKTIF=>1));
      if(!$res) {
        $this->db->trans_rollback();
        $err = $this->db->error();
        ShowJsonError($err['message']);
        exit();
      }
      $this->db->trans_commit();
    } else {
      ShowJsonError('PARAMETER TIDAK VALID');
      exit();
    }

    ShowJsonSuccess('BERHASIL MENGUBAH DATA');
    exit();
  }

  public function ajax_form_iku($mode, $id, $idx=null)  {
    $ruser = GetLoggedUser();
    if($ruser[COL_ROLEID]!=ROLEADMIN && $ruser[COL_ROLEID]!=ROLEKADIS) {
      $this->showErrUnathorized();
    }
    $uraianIKU = $this->input->post('Uraian');

    if($mode == 'add-tupoksi' || $mode == 'add-iku') {
      if(empty($uraianIKU)) {
        ShowJsonError('PARAMETER TIDAK VALID!');
        exit();
      }

      $rrenstra = $this->db
      ->where(COL_RENSTRAID, $id)
      ->get(TBL_SAKIPV2_SKPD_RENSTRA)
      ->row_array();
      if(empty($rrenstra)) {
        ShowJsonError('PARAMETER TIDAK VALID!');
        exit();
      }

      $arrCurrJSON = array();
      if($mode == 'add-tupoksi') {
        if(!empty($rrenstra[COL_RENSTRATUPOKSI])) {
          $arrCurrJSON = json_decode($rrenstra[COL_RENSTRATUPOKSI]);
        }
      } else if($mode == 'add-iku') {
        if(!empty($rrenstra[COL_RENSTRAIKU])) {
          $arrCurrJSON = json_decode($rrenstra[COL_RENSTRAIKU]);
        }
      }

      $arrCurrJSON[] = $uraianIKU;

      $rec = array();
      if($mode == 'add-tupoksi') {
        $rec = array(COL_RENSTRATUPOKSI=>json_encode($arrCurrJSON));
      } else if($mode == 'add-iku') {
        $rec = array(COL_RENSTRAIKU=>json_encode($arrCurrJSON));
      }

      $res = $this->db->where(COL_RENSTRAID, $id)->update(TBL_SAKIPV2_SKPD_RENSTRA, $rec);
      if(!$res) {
        $err = $this->db->error();
        ShowJsonError($err['message']);
        exit();
      }

      ShowJsonSuccess('INPUT DATA BERHASIL');
      exit();

    } else if($mode == 'edit-tupoksi' || $mode == 'edit-iku') {
      if(empty($uraianIKU)) {
        ShowJsonError('PARAMETER TIDAK VALID!');
        exit();
      }
      if($idx==null) {
        ShowJsonError('PARAMETER TIDAK VALID!');
        exit();
      }

      $rrenstra = $this->db
      ->where(COL_RENSTRAID, $id)
      ->get(TBL_SAKIPV2_SKPD_RENSTRA)
      ->row_array();
      if(empty($rrenstra)) {
        ShowJsonError('PARAMETER TIDAK VALID!');
        exit();
      }

      $arrCurrJSON = array();
      if($mode == 'edit-tupoksi') {
        if(!empty($rrenstra[COL_RENSTRATUPOKSI])) {
          $arrCurrJSON = json_decode($rrenstra[COL_RENSTRATUPOKSI]);
        }
      } else if($mode == 'edit-iku') {
        if(!empty($rrenstra[COL_RENSTRAIKU])) {
          $arrCurrJSON = json_decode($rrenstra[COL_RENSTRAIKU]);
        }
      }

      if(isset($arrCurrJSON[$idx])) {
        $arrCurrJSON[$idx] = $uraianIKU;
      }

      $rec = array();
      if($mode == 'edit-tupoksi') {
        $rec = array(COL_RENSTRATUPOKSI=>json_encode($arrCurrJSON));
      } else if($mode == 'edit-iku') {
        $rec = array(COL_RENSTRAIKU=>json_encode($arrCurrJSON));
      }

      $res = $this->db->where(COL_RENSTRAID, $id)->update(TBL_SAKIPV2_SKPD_RENSTRA, $rec);
      if(!$res) {
        $err = $this->db->error();
        ShowJsonError($err['message']);
        exit();
      }

      ShowJsonSuccess('INPUT DATA BERHASIL');
      exit();

    } else if($mode == 'delete-tupoksi' || $mode == 'delete-iku') {
      if($idx==null) {
        ShowJsonError('PARAMETER TIDAK VALID!');
        exit();
      }

      $rrenstra = $this->db
      ->where(COL_RENSTRAID, $id)
      ->get(TBL_SAKIPV2_SKPD_RENSTRA)
      ->row_array();
      if(empty($rrenstra)) {
        ShowJsonError('PARAMETER TIDAK VALID!');
        exit();
      }

      $arrCurrJSON = array();
      if($mode == 'delete-tupoksi') {
        if(!empty($rrenstra[COL_RENSTRATUPOKSI])) {
          $arrCurrJSON = json_decode($rrenstra[COL_RENSTRATUPOKSI]);
        }
      } else if($mode == 'delete-iku') {
        if(!empty($rrenstra[COL_RENSTRAIKU])) {
          $arrCurrJSON = json_decode($rrenstra[COL_RENSTRAIKU]);
        }
      }

      if(isset($arrCurrJSON[$idx])) {
        array_splice($arrCurrJSON,$idx,1);
      }

      $rec = array();
      if($mode == 'delete-tupoksi') {
        $rec = array(COL_RENSTRATUPOKSI=>json_encode($arrCurrJSON));
      } else if($mode == 'delete-iku') {
        $rec = array(COL_RENSTRAIKU=>json_encode($arrCurrJSON));
      }

      $res = $this->db->where(COL_RENSTRAID, $id)->update(TBL_SAKIPV2_SKPD_RENSTRA, $rec);
      if(!$res) {
        $err = $this->db->error();
        ShowJsonError($err['message']);
        exit();
      }

      ShowJsonSuccess('PERUBAHAN DATA BERHASIL');
      exit();
    }
  }

  public function ajax_form_tujuan($mode, $idRenstra, $id=null) {
    $ruser = GetLoggedUser();
    if($ruser[COL_ROLEID]!=ROLEADMIN && $ruser[COL_ROLEID]!=ROLEKADIS) {
      $this->showErrUnathorized();
    }

    $data['mode'] = $mode;
    $data['idRenstra'] = $idRenstra;

    if(!empty($_POST)) {
      if($mode=='add') {
        $this->db->trans_begin();
        try {
          $rec = array(
            COL_IDRENSTRA=>$idRenstra,
            COL_IDTUJUANPMD=>$this->input->post(COL_IDTUJUANPMD),
            COL_TUJUANNO=>$this->input->post(COL_TUJUANNO),
            COL_TUJUANURAIAN=>$this->input->post(COL_TUJUANURAIAN),

            COL_CREATEDBY=>$ruser[COL_USERNAME],
            COL_CREATEDON=>date('Y-m-d H:i:s')
          );

          $res = $this->db->insert(TBL_SAKIPV2_SKPD_RENSTRA_TUJUAN, $rec);
          if(!$res) {
            $err = $this->db->error();
            throw new Exception($err['message']);
          }

          $this->db->trans_commit();
          ShowJsonSuccess('INPUT DATA BERHASIL');
          exit();
        } catch(Exception $ex) {
          $this->db->trans_rollback();
          ShowJsonError($ex->getMessage());
          exit();
        }
      } else if($mode=='edit') {
        $this->db->trans_begin();
        try {
          if(empty($id)) {
            throw new Exception('PARAMETER TIDAK VALID');
          }

          $rec = array(
            COL_IDTUJUANPMD=>$this->input->post(COL_IDTUJUANPMD),
            COL_TUJUANNO=>$this->input->post(COL_TUJUANNO),
            COL_TUJUANURAIAN=>$this->input->post(COL_TUJUANURAIAN),

            COL_UPDATEDBY=>$ruser[COL_USERNAME],
            COL_UPDATEDON=>date('Y-m-d H:i:s')
          );

          $res = $this->db->where(COL_TUJUANID, $id)->update(TBL_SAKIPV2_SKPD_RENSTRA_TUJUAN, $rec);
          if(!$res) {
            $err = $this->db->error();
            throw new Exception($err['message']);
          }

          $this->db->trans_commit();
          ShowJsonSuccess('INPUT DATA BERHASIL');
          exit();
        } catch(Exception $ex) {
          $this->db->trans_rollback();
          ShowJsonError($ex->getMessage());
          exit();
        }
      }
    } else {
      if($mode=='edit') {
        $rdata = $this->db->where(COL_TUJUANID, $id)->get(TBL_SAKIPV2_SKPD_RENSTRA_TUJUAN)->row_array();
        if(empty($rdata)) {
          echo '<p class="text-center text-danger font-weight-bold">PARAMETER TIDAK VALID!</p>';
          exit();
        }
        $data['data'] = $rdata;
      }
      $this->load->view('sakipv2/skpd/form-tujuan', $data);
    }
  }

  public function ajax_change_tujuan($mode, $id)  {
    $ruser = GetLoggedUser();
    if($ruser[COL_ROLEID]!=ROLEADMIN && $ruser[COL_ROLEID]!=ROLEKADIS) {
      $this->showErrUnathorized();
    }

    if($mode=='delete') {
      $res = $this->db->where(COL_TUJUANID, $id)->delete(TBL_SAKIPV2_SKPD_RENSTRA_TUJUAN);
      if(!$res) {
        $err = $this->db->error();
        ShowJsonError($err['message']);
        exit();
      }
    }

    ShowJsonSuccess('BERHASIL MENGUBAH DATA');
    exit();
  }

  public function ajax_form_indikatortujuan($mode, $id)  {
    $ruser = GetLoggedUser();
    if($ruser[COL_ROLEID]!=ROLEADMIN && $ruser[COL_ROLEID]!=ROLEKADIS) {
      $this->showErrUnathorized();
    }
    $uraianIndikator = $this->input->post(COL_TUJINDIKATORURAIAN);

    if($mode == 'add') {
      if(empty($uraianIndikator)) {
        ShowJsonError('PARAMETER TIDAK VALID!');
        exit();
      }

      $rec = array(
        COL_IDTUJUAN=>$id,
        COL_TUJINDIKATORURAIAN=> $uraianIndikator,
        COL_CREATEDBY=>$ruser[COL_USERNAME],
        COL_CREATEDON=>date('Y-m-d H:i:s')
      );
      $res = $this->db->insert(TBL_SAKIPV2_SKPD_RENSTRA_TUJUANDET, $rec);
      if(!$res) {
        $err = $this->db->error();
        ShowJsonError($err['message']);
        exit();
      }

      ShowJsonSuccess('INPUT DATA BERHASIL');
      exit();
    } else if($mode == 'edit') {
      if(empty($uraianIndikator)) {
        ShowJsonError('PARAMETER TIDAK VALID!');
        exit();
      }

      $rec = array(
        COL_TUJINDIKATORURAIAN=> $uraianIndikator,
        COL_UPDATEDBY=>$ruser[COL_USERNAME],
        COL_UPDATEDON=>date('Y-m-d H:i:s')
      );
      $res = $this->db->where(COL_TUJINDIKATORID, $id)->update(TBL_SAKIPV2_SKPD_RENSTRA_TUJUANDET, $rec);
      if(!$res) {
        $err = $this->db->error();
        ShowJsonError($err['message']);
        exit();
      }

      ShowJsonSuccess('INPUT DATA BERHASIL');
      exit();
    } else if($mode == 'delete') {
      $res = $this->db->where(COL_TUJINDIKATORID, $id)->delete(TBL_SAKIPV2_SKPD_RENSTRA_TUJUANDET);
      if(!$res) {
        $err = $this->db->error();
        ShowJsonError($err['message']);
        exit();
      }

      ShowJsonSuccess('HAPUS DATA BERHASIL');
      exit();
    }
  }

  public function ajax_form_sasaran($mode, $idTujuan, $id=null) {
    $ruser = GetLoggedUser();
    if($ruser[COL_ROLEID]!=ROLEADMIN && $ruser[COL_ROLEID]!=ROLEKADIS) {
      $this->showErrUnathorized();
    }

    $data['mode'] = $mode;
    $data['idTujuan'] = $idTujuan;
    if(!empty($_POST)) {
      if($mode=='add') {
        $this->db->trans_begin();
        try {
          $rec = array(
            COL_IDTUJUAN=>$idTujuan,
            COL_IDSASARANPMD=>$this->input->post(COL_IDSASARANPMD),
            COL_SASARANNO=>$this->input->post(COL_SASARANNO),
            COL_SASARANURAIAN=>$this->input->post(COL_SASARANURAIAN),

            COL_CREATEDBY=>$ruser[COL_USERNAME],
            COL_CREATEDON=>date('Y-m-d H:i:s')
          );

          $arrUraian = $this->input->post('SsrIndikatorUraian');
          $arrSumberData = $this->input->post('SsrIndikatorSumberData');
          $arrFormulasi = $this->input->post('SsrIndikatorFormulasi');
          $arrSatuan = $this->input->post('SsrIndikatorSatuan');
          $arrTarget = $this->input->post('SsrIndikatorTarget');
          $arrIndikator = [];

          $res = $this->db->insert(TBL_SAKIPV2_SKPD_RENSTRA_SASARAN, $rec);
          if(!$res) {
            $err = $this->db->error();
            throw new Exception($err['message']);
          }

          $idSasaran = $this->db->insert_id();
          for($i = 0; $i<count($arrUraian); $i++) {
            $arrIndikator[] = array(
              COL_IDSASARAN=>$idSasaran,
              'SsrIndikatorUraian'=>$arrUraian[$i],
              'SsrIndikatorSumberData'=>$arrSumberData[$i],
              'SsrIndikatorFormulasi'=>$arrFormulasi[$i],
              'SsrIndikatorSatuan'=>$arrSatuan[$i],
              'SsrIndikatorTarget'=>$arrTarget[$i],

              COL_CREATEDBY=>$ruser[COL_USERNAME],
              COL_CREATEDON=>date('Y-m-d H:i:s')
            );
          }

          if(!empty($arrIndikator)) {
            $res = $this->db->insert_batch(TBL_SAKIPV2_SKPD_RENSTRA_SASARANDET, $arrIndikator);
            if(!$res) {
              $err = $this->db->error();
              throw new Exception($err['message']);
            }
          }

          $this->db->trans_commit();
          ShowJsonSuccess('INPUT DATA BERHASIL');
          exit();
        } catch(Exception $ex) {
          $this->db->trans_rollback();
          ShowJsonError($ex->getMessage());
          exit();
        }
      } else if($mode=='edit') {
        $this->db->trans_begin();
        try {
          if(empty($id)) {
            throw new Exception('PARAMETER TIDAK VALID');
          }

          $rec = array(
            COL_IDSASARANPMD=>$this->input->post(COL_IDSASARANPMD),
            COL_SASARANNO=>$this->input->post(COL_SASARANNO),
            COL_SASARANURAIAN=>$this->input->post(COL_SASARANURAIAN),

            COL_UPDATEDBY=>$ruser[COL_USERNAME],
            COL_UPDATEDON=>date('Y-m-d H:i:s')
          );

          $arrUraian = $this->input->post('SsrIndikatorUraian');
          $arrSumberData = $this->input->post('SsrIndikatorSumberData');
          $arrFormulasi = $this->input->post('SsrIndikatorFormulasi');
          $arrSatuan = $this->input->post('SsrIndikatorSatuan');
          $arrTarget = $this->input->post('SsrIndikatorTarget');
          $arrIndikator = [];

          for($i = 0; $i<count($arrUraian); $i++) {
            $arrIndikator[] = array(
              COL_IDSASARAN=>$id,
              'SsrIndikatorUraian'=>$arrUraian[$i],
              'SsrIndikatorSumberData'=>$arrSumberData[$i],
              'SsrIndikatorFormulasi'=>$arrFormulasi[$i],
              'SsrIndikatorSatuan'=>$arrSatuan[$i],
              'SsrIndikatorTarget'=>$arrTarget[$i],

              COL_CREATEDBY=>$ruser[COL_USERNAME],
              COL_CREATEDON=>date('Y-m-d H:i:s')
            );
          }

          if(!empty($arrIndikator)) {
            $res = $this->db->where(COL_IDSASARAN, $id)->delete(TBL_SAKIPV2_SKPD_RENSTRA_SASARANDET);
            if(!$res) {
              $err = $this->db->error();
              throw new Exception($err['message']);
            }

            $res = $this->db->insert_batch(TBL_SAKIPV2_SKPD_RENSTRA_SASARANDET, $arrIndikator);
            if(!$res) {
              $err = $this->db->error();
              throw new Exception($err['message']);
            }
          }

          $res = $this->db->where(COL_SASARANID, $id)->update(TBL_SAKIPV2_SKPD_RENSTRA_SASARAN, $rec);
          if(!$res) {
            $err = $this->db->error();
            throw new Exception($err['message']);
          }

          $this->db->trans_commit();
          ShowJsonSuccess('INPUT DATA BERHASIL');
          exit();
        } catch(Exception $ex) {
          $this->db->trans_rollback();
          ShowJsonError($ex->getMessage());
          exit();
        }
      }
    } else {
      if($mode=='edit') {
        $rdata = $this->db->where(COL_SASARANID, $id)->get(TBL_SAKIPV2_SKPD_RENSTRA_SASARAN)->row_array();
        if(empty($rdata)) {
          echo '<p class="text-center text-danger font-weight-bold">PARAMETER TIDAK VALID!</p>';
          exit();
        }
        $rIndikatorSasaran = $this->db->where(COL_IDSASARAN, $id)->get(TBL_SAKIPV2_SKPD_RENSTRA_SASARANDET)->result_array();
        $data['data'] = $rdata;
        $data['rIndikatorSasaran'] = $rIndikatorSasaran;
      }
      $this->load->view('sakipv2/skpd/form-sasaran', $data);
    }
  }

  public function ajax_change_sasaran($mode, $id)  {
    $ruser = GetLoggedUser();
    if($ruser[COL_ROLEID]!=ROLEADMIN && $ruser[COL_ROLEID]!=ROLEKADIS) {
      $this->showErrUnathorized();
    }

    if($mode=='delete') {
      $res = $this->db->where(COL_SASARANID, $id)->delete(TBL_SAKIPV2_SKPD_RENSTRA_SASARAN);
      if(!$res) {
        $err = $this->db->error();
        ShowJsonError($err['message']);
        exit();
      }
    }

    ShowJsonSuccess('BERHASIL MENGUBAH DATA');
    exit();
  }

  public function ajax_form_indikatorsasaran($mode, $id)  {
    $ruser = GetLoggedUser();
    if($ruser[COL_ROLEID]!=ROLEADMIN && $ruser[COL_ROLEID]!=ROLEKADIS) {
      $this->showErrUnathorized();
    }
    $uraianIndikator = $this->input->post(COL_SSRINDIKATORURAIAN);

    if($mode == 'add') {
      if(empty($uraianIndikator)) {
        ShowJsonError('PARAMETER TIDAK VALID!');
        exit();
      }

      $rec = array(
        COL_IDSASARAN=>$id,
        COL_SSRINDIKATORURAIAN=> $uraianIndikator,
        COL_CREATEDBY=>$ruser[COL_USERNAME],
        COL_CREATEDON=>date('Y-m-d H:i:s')
      );
      $res = $this->db->insert(TBL_SAKIPV2_SKPD_RENSTRA_SASARANDET, $rec);
      if(!$res) {
        $err = $this->db->error();
        ShowJsonError($err['message']);
        exit();
      }

      ShowJsonSuccess('INPUT DATA BERHASIL');
      exit();
    } else if($mode == 'edit') {
      if(empty($uraianIndikator)) {
        ShowJsonError('PARAMETER TIDAK VALID!');
        exit();
      }

      $rec = array(
        COL_SSRINDIKATORURAIAN=> $uraianIndikator,
        COL_UPDATEDBY=>$ruser[COL_USERNAME],
        COL_UPDATEDON=>date('Y-m-d H:i:s')
      );
      $res = $this->db->where(COL_SSRINDIKATORID, $id)->update(TBL_SAKIPV2_SKPD_RENSTRA_SASARANDET, $rec);
      if(!$res) {
        $err = $this->db->error();
        ShowJsonError($err['message']);
        exit();
      }

      ShowJsonSuccess('INPUT DATA BERHASIL');
      exit();
    } else if($mode == 'delete') {
      $res = $this->db->where(COL_SSRINDIKATORID, $id)->delete(TBL_SAKIPV2_SKPD_RENSTRA_SASARANDET);
      if(!$res) {
        $err = $this->db->error();
        ShowJsonError($err['message']);
        exit();
      }

      ShowJsonSuccess('HAPUS DATA BERHASIL');
      exit();
    }
  }

  public function ajax_form_dpa($mode, $id=null) {
    $ruser = GetLoggedUser();
    if($ruser[COL_ROLEID]!=ROLEADMIN && $ruser[COL_ROLEID]!=ROLEKADIS) {
      $this->showErrUnathorized();
    }

    $data['mode'] = $mode;
    if(!empty($_POST)) {
      $ruser = GetLoggedUser();
      if($mode=='add') {
        if(empty($id)) {
          ShowJsonError('PARAMETER TIDAK VALID!');
          exit();
        }

        $this->db->trans_begin();
        try {
          $rec = array(
            COL_IDRENSTRA=>$id,
            COL_DPATAHUN=>$this->input->post(COL_DPATAHUN),
            COL_DPAURAIAN=>$this->input->post(COL_DPAURAIAN),

            COL_CREATEDBY=>$ruser[COL_USERNAME],
            COL_CREATEDON=>date('Y-m-d H:i:s')
          );

          $res = $this->db->where(COL_IDRENSTRA, $rec[COL_IDRENSTRA])->where(COL_DPATAHUN, $rec[COL_DPATAHUN])->update(TBL_SAKIPV2_SKPD_RENSTRA_DPA, array(COL_DPAISAKTIF=>0));
          if(!$res) {
            $err = $this->db->error();
            throw new Exception($err['message']);
          }

          $res = $this->db->insert(TBL_SAKIPV2_SKPD_RENSTRA_DPA, $rec);
          if(!$res) {
            $err = $this->db->error();
            throw new Exception($err['message']);
          }

          $this->db->trans_commit();
          ShowJsonSuccess('INPUT DATA BERHASIL');
          exit();
        } catch(Exception $ex) {
          $this->db->trans_rollback();
          ShowJsonError($ex->getMessage());
          exit();
        }
      } else if($mode=='edit') {
        $this->db->trans_begin();
        try {
          if(empty($id)) {
            throw new Exception('PARAMETER TIDAK VALID');
          }

          $rec = array(
            COL_DPATAHUN=>$this->input->post(COL_DPATAHUN),
            COL_DPAURAIAN=>$this->input->post(COL_DPAURAIAN),

            COL_UPDATEDBY=>$ruser[COL_USERNAME],
            COL_UPDATEDON=>date('Y-m-d H:i:s')
          );

          $res = $this->db->where(COL_DPAID, $id)->update(TBL_SAKIPV2_SKPD_RENSTRA_DPA, $rec);
          if(!$res) {
            $err = $this->db->error();
            throw new Exception($err['message']);
          }

          $this->db->trans_commit();
          ShowJsonSuccess('INPUT DATA BERHASIL');
          exit();
        } catch(Exception $ex) {
          $this->db->trans_rollback();
          ShowJsonError($ex->getMessage());
          exit();
        }
      }
    } else {
      if($mode=='edit') {
        $rdata = $this->db->where(COL_DPAID, $id)->get(TBL_SAKIPV2_SKPD_RENSTRA_DPA)->row_array();
        if(empty($rdata)) {
          echo '<p class="text-center text-danger font-weight-bold">PARAMETER TIDAK VALID!</p>';
          exit();
        }
        $data['data'] = $rdata;
        $this->load->view('sakipv2/skpd/form-dpa', $data);
      } else if($mode=='duplicate') {
        $rdata = $this->db->where(COL_DPAID, $id)->get(TBL_SAKIPV2_SKPD_RENSTRA_DPA)->row_array();
        if(empty($rdata)) {
          echo '<p class="text-center text-danger font-weight-bold">PARAMETER TIDAK VALID!</p>';
          exit();
        }
        $data['data'] = $rdata;
        $this->load->view('sakipv2/skpd/form-duplicate', $data);
      } else {
        $this->load->view('sakipv2/skpd/form-dpa', $data);
      }
    }
  }

  public function ajax_change_dpa($mode, $id)  {
    $ruser = GetLoggedUser();
    if($ruser[COL_ROLEID]!=ROLEADMIN && $ruser[COL_ROLEID]!=ROLEKADIS) {
      $this->showErrUnathorized();
    }

    if($mode=='delete') {
      $rcurrent = $this->db
      ->where(COL_DPAID, $id)
      ->get(TBL_SAKIPV2_SKPD_RENSTRA_DPA)
      ->row_array();
      if(empty($rcurrent)) {
        ShowJsonError('PARAMETER TIDAK VALID');
        exit();
      }

      $res = $this->db->where(COL_DPAID, $id)->delete(TBL_SAKIPV2_SKPD_RENSTRA_DPA);
      if(!$res) {
        $err = $this->db->error();
        ShowJsonError($err['message']);
        exit();
      }

      $rexist = $this->db
      ->where(COL_DPATAHUN, $rcurrent[COL_DPATAHUN])
      ->where(COL_IDRENSTRA, $rcurrent[COL_IDRENSTRA])
      ->order_by(COL_DPAISAKTIF,'asc')
      ->order_by(COL_DPATAHUN,'desc')
      ->get(TBL_SAKIPV2_SKPD_RENSTRA_DPA)
      ->row_array();

      if(!empty($rexist) && $rexist[COL_DPAISAKTIF]==0) {
        $res = $this->db->where(COL_DPAID, $rexist[COL_DPAID])->update(TBL_SAKIPV2_SKPD_RENSTRA_DPA, array(COL_DPAISAKTIF=>1));
        if(!$res) {
          $this->db->trans_rollback();
          $err = $this->db->error();
          ShowJsonError($err['message']);
          exit();
        }
      }
    } else  if($mode=='activate') {
      $rcurrent = $this->db
      ->where(COL_DPAID, $id)
      ->get(TBL_SAKIPV2_SKPD_RENSTRA_DPA)
      ->row_array();
      if(empty($rcurrent)) {
        ShowJsonError('PARAMETER TIDAK VALID');
        exit();
      }

      $this->db->trans_begin();
      $res = $this->db->where(COL_DPAID.'!=', $id)->where(COL_IDRENSTRA, $rcurrent[COL_IDRENSTRA])->where(COL_DPATAHUN, $rcurrent[COL_DPATAHUN])->update(TBL_SAKIPV2_SKPD_RENSTRA_DPA, array(COL_DPAISAKTIF=>0));
      if(!$res) {
        $this->db->trans_rollback();
        $err = $this->db->error();
        ShowJsonError($err['message']);
        exit();
      }
      $res = $this->db->where(COL_DPAID, $id)->update(TBL_SAKIPV2_SKPD_RENSTRA_DPA, array(COL_DPAISAKTIF=>1));
      if(!$res) {
        $this->db->trans_rollback();
        $err = $this->db->error();
        ShowJsonError($err['message']);
        exit();
      }
      $this->db->trans_commit();
    } else {
      ShowJsonError('PARAMETER TIDAK VALID');
      exit();
    }

    ShowJsonSuccess('BERHASIL MENGUBAH DATA');
    exit();
  }

  public function monev() {
    $data['title'] = 'Target & Capaian SKPD';
    $this->template->load('main', 'sakipv2/skpd/monev', $data);
  }

  public function monev_data($idSKPD, $idRenstra, $idDPA, $idTW, $modeCetak='') {
    $data = array(
      'idSKPD'=>$idSKPD,
      'idRenstra'=>$idRenstra,
      'idDPA'=>$idDPA,
      'idTW'=>$idTW,
      'modCetak'=>$modeCetak
    );
    $data['title'] = 'TARGET & CAPAIAN';
    $rdpa = $this->db
    ->where(COL_DPAID, $idDPA)
    ->get(TBL_SAKIPV2_SKPD_RENSTRA_DPA)
    ->row_array();
    if(empty($rdpa)) {
      show_error('DPA TIDAK VALID!');
      exit();
    }
    $rskpd = $this->db
    ->where(COL_SKPDID, $idSKPD)
    ->get(TBL_SAKIPV2_SKPD)
    ->row_array();
    if(empty($rskpd)) {
      show_error('SKPD TIDAK VALID!');
      exit();
    }

    if($modeCetak=='RENCANA' || $modeCetak=='REALISASI') {
      if($modeCetak=='RENCANA') $data['title'] = 'RENCANA AKSI';
      else if($modeCetak=='REALISASI') $data['title'] = 'REALISASI KINERJA';

      $this->load->library('Mypdf');
      $mpdf = new Mypdf('utf-8', 'A4-L', 0,'',15,15,30,16);

      $html = $this->load->view('sakipv2/skpd/monev-partial', $data, TRUE);
      $htmlLogo = MY_IMAGEPATH.'logo.png';
      $htmlYear = $rdpa[COL_DPATAHUN];
      $htmlTitle = $data['title'].' '.strtoupper($rskpd[COL_SKPDNAMA]);
      $htmlHeader = @"
      <table style=\"border: none !important\">
        <tr>
          <td rowspan=\"2\" style=\"border: none !important; padding: 0 !important; width: 30px; white-space: nowrap; vertical-align: top\"><img src=\"$htmlLogo\" style=\"width: 25px\" /></td>
          <td style=\"border: none !important; padding: 0 !important; font-size: 10pt; font-weight: bold\">$htmlTitle</td>
        </tr>
        <tr>
          <td style=\"border: none !important; padding: 0 !important; font-size: 10pt; font-weight: bold\">KOTA TEBING TINGGI TAHUN $htmlYear</td>
        </tr>
      </table>
      <hr />
      ";
      //echo $html;
      //return;
      $mpdf->pdf->SetTitle($data['title']);
      $mpdf->pdf->SetHTMLHeader($htmlHeader);
      $mpdf->pdf->SetFooter('Dicetak melalui aplikasi '.$this->setting_web_name.' pada '.date('d-m-Y H:i'));
      $mpdf->pdf->WriteHTML($html);
      $mpdf->pdf->Output('SITALAKBAJAKUN - '.$data['title'].' '.date('YmdHis').'.pdf', 'I');
    } else {
      $this->load->view('sakipv2/skpd/monev-partial', $data);
    }
  }

  public function monev_ajax_change_sasaran($mode, $dpaTahun, $idSasaranIndikator) {
    $ruser = GetLoggedUser();
    if($ruser[COL_ROLEID]!=ROLEADMIN && $ruser[COL_ROLEID]!=ROLEKADIS) {
      $this->showErrUnathorized();
    }

    $val = $this->input->post('value');
    if($mode==COL_MONEVTARGET ||
        $mode==COL_MONEVTARGETTW1 ||
        $mode==COL_MONEVTARGETTW2 ||
        $mode==COL_MONEVTARGETTW3 ||
        $mode==COL_MONEVTARGETTW4 ||
        $mode==COL_MONEVREALISASI ||
        $mode==COL_MONEVREALISASITW1 ||
        $mode==COL_MONEVREALISASITW2 ||
        $mode==COL_MONEVREALISASITW3 ||
        $mode==COL_MONEVREALISASITW4 ||
        $mode==COL_MONEVCAPAIAN ||
        $mode==COL_MONEVCAPAIANTW1 ||
        $mode==COL_MONEVCAPAIANTW2 ||
        $mode==COL_MONEVCAPAIANTW3 ||
        $mode==COL_MONEVCAPAIANTW4 ||
        $mode==COL_MONEVCATATAN ||
        $mode==COL_MONEVCATATANTW1 ||
        $mode==COL_MONEVCATATANTW2 ||
        $mode==COL_MONEVCATATANTW3 ||
        $mode==COL_MONEVCATATANTW4
      ) {
      $rval = $this->db
      ->where(COL_IDSASARANINDIKATOR, $idSasaranIndikator)
      ->where(COL_MONEVTAHUN, $dpaTahun)
      ->order_by(COL_UNIQ, 'desc')
      ->get(TBL_SAKIPV2_SKPD_RENSTRA_SASARANMONEV)
      ->row_array();
      if(!empty($rval)) {
        $res = $this->db
        ->where(COL_UNIQ, $rval[COL_UNIQ])
        ->update(TBL_SAKIPV2_SKPD_RENSTRA_SASARANMONEV, array($mode=>$val, COL_UPDATEDBY=>$ruser[COL_USERNAME], COL_UPDATEDON=>date('Y-m-d H:i:s')));
        if(!$res) {
          $err = $this->db->error();
          ShowJsonError($err['message']);
          exit();
        }
      } else {
        $dat = array(
          COL_IDSASARANINDIKATOR=>$idSasaranIndikator,
          COL_MONEVTAHUN=>$dpaTahun,
          $mode=>$val,

          COL_CREATEDBY=>$ruser[COL_USERNAME],
          COL_CREATEDON=>date('Y-m-d H:i:s')
        );
        $res = $this->db
        ->insert(TBL_SAKIPV2_SKPD_RENSTRA_SASARANMONEV, $dat);
        if(!$res) {
          $err = $this->db->error();
          ShowJsonError($err['message']);
          exit();
        }
      }

      ShowJsonSuccess('BERHASIL MENGUPDATE DATA');
      exit();
    }
  }

  public function monev_ajax_change_anggaran($mode, $idSubKegiatan) {
    $ruser = GetLoggedUser();
    if($ruser[COL_ROLEID]!=ROLEADMIN && $ruser[COL_ROLEID]!=ROLEKADIS) {
      $this->showErrUnathorized();
    }

    $val = $this->input->post('value');
    if($mode==COL_SUBKEGPAGUTW1 ||
        $mode==COL_SUBKEGPAGUTW2 ||
        $mode==COL_SUBKEGPAGUTW3 ||
        $mode==COL_SUBKEGPAGUTW4 ||
        $mode==COL_SUBKEGREALISASITW1 ||
        $mode==COL_SUBKEGREALISASITW2 ||
        $mode==COL_SUBKEGREALISASITW3 ||
        $mode==COL_SUBKEGREALISASITW4
      ) {

      $res = $this->db
      ->where(COL_SUBKEGID, $idSubKegiatan)
      ->update(TBL_SAKIPV2_SUBBID_SUBKEGIATAN, array($mode=>$val, COL_UPDATEDBY=>$ruser[COL_USERNAME], COL_UPDATEDON=>date('Y-m-d H:i:s')));
      if(!$res) {
        $err = $this->db->error();
        ShowJsonError($err['message']);
        exit();
      }

      ShowJsonSuccess('BERHASIL MENGUPDATE DATA');
      exit();
    }
  }

  public function monev_ajax_change_sasaranprog($mode, $id) {
    $ruser = GetLoggedUser();
    if($ruser[COL_ROLEID]!=ROLEADMIN && $ruser[COL_ROLEID]!=ROLEKADIS) {
      $this->showErrUnathorized();
    }
    $val = $this->input->post('value');
    $res = $this->db
    ->where(COL_SASARANID, $id)
    ->update(TBL_SAKIPV2_BID_PROGSASARAN, array($mode=>$val, COL_UPDATEDBY=>$ruser[COL_USERNAME], COL_UPDATEDON=>date('Y-m-d H:i:s')));
    if(!$res) {
      $err = $this->db->error();
      ShowJsonError($err['message']);
      exit();
    }

    ShowJsonSuccess('BERHASIL MENGUPDATE DATA');
    exit();
  }

  public function monev_ajax_change_sasarankeg($mode, $id) {
    $ruser = GetLoggedUser();
    if($ruser[COL_ROLEID]!=ROLEADMIN && $ruser[COL_ROLEID]!=ROLEKADIS) {
      $this->showErrUnathorized();
    }

    $val = $this->input->post('value');
    $res = $this->db
    ->where(COL_SASARANID, $id)
    ->update(TBL_SAKIPV2_BID_KEGSASARAN, array($mode=>$val, COL_UPDATEDBY=>$ruser[COL_USERNAME], COL_UPDATEDON=>date('Y-m-d H:i:s')));
    if(!$res) {
      $err = $this->db->error();
      ShowJsonError($err['message']);
      exit();
    }

    ShowJsonSuccess('BERHASIL MENGUPDATE DATA');
    exit();
  }

  public function monev_ajax_change_sasaransubkeg($mode, $id) {
    $ruser = GetLoggedUser();
    if($ruser[COL_ROLEID]!=ROLEADMIN && $ruser[COL_ROLEID]!=ROLEKADIS) {
      $this->showErrUnathorized();
    }

    $val = $this->input->post('value');
    $res = $this->db
    ->where(COL_SASARANID, $id)
    ->update(TBL_SAKIPV2_SUBBID_SUBKEGSASARAN, array($mode=>$val, COL_UPDATEDBY=>$ruser[COL_USERNAME], COL_UPDATEDON=>date('Y-m-d H:i:s')));
    if(!$res) {
      $err = $this->db->error();
      ShowJsonError($err['message']);
      exit();
    }

    ShowJsonSuccess('BERHASIL MENGUPDATE DATA');
    exit();
  }

  public function ajax_duplicate_dpa($id) {
    $ruser=GetLoggedUser();
    if(!empty($_POST)) {
      $dpaId = $_POST[COL_DPAID];
      $rdpa = $this->db
      ->where(COL_DPAID, $id)
      ->get(TBL_SAKIPV2_SKPD_RENSTRA_DPA)
      ->row_array();

      if(empty($rdpa)) {
        ShowJsonError('Parameter tidak valid');
        exit();
      }

      $this->db->trans_begin();
      try{
        /* deletion */
        $res = $this->db->where(COL_IDDPA, $dpaId)->delete(TBL_SAKIPV2_BID_PROGRAM);
        if(!$res) {
          $err = $this->db->error();
          throw new Exception($err['message']);
        }
        /* deletion */

        $rprogram = $this->db
        ->where(COL_IDDPA, $id)
        ->order_by(COL_PROGRAMKODE)
        ->get(TBL_SAKIPV2_BID_PROGRAM)
        ->result_array();

        foreach($rprogram as $prg) {
          $arrProgram = array(
            COL_IDBID=>$prg[COL_IDBID],
            COL_IDDPA=>$dpaId,
            COL_IDSASARANSKPD=>$prg[COL_IDSASARANSKPD],
            COL_PROGRAMKODE=>$prg[COL_PROGRAMKODE],
            COL_PROGRAMURAIAN=>$prg[COL_PROGRAMURAIAN],
            COL_CREATEDBY=>$ruser[COL_USERNAME],
            COL_CREATEDON=>date('Y-m-d H:i:s')
          );

          $res = $this->db->insert(TBL_SAKIPV2_BID_PROGRAM, $arrProgram);
          if(!$res) {
            $err = $this->db->error();
            throw new Exception($err['message']);
          }
          $idProgram = $this->db->insert_id();

          $arrProgSasaran = array();
          $rprogsasaran = $this->db
          ->where(COL_IDPROGRAM, $prg[COL_PROGRAMID])
          ->order_by(COL_SASARANNO)
          ->get(TBL_SAKIPV2_BID_PROGSASARAN)
          ->result_array();
          foreach($rprogsasaran as $ps) {
            $arrProgSasaran[] = array(
              COL_IDPROGRAM=>$idProgram,
              COL_SASARANNO=>$ps[COL_SASARANNO],
              COL_SASARANURAIAN=>$ps[COL_SASARANURAIAN],
              COL_SASARANINDIKATOR=>$ps[COL_SASARANINDIKATOR],
              COL_SASARANSATUAN=>$ps[COL_SASARANSATUAN],
              COL_SASARANTARGET=>$ps[COL_SASARANTARGET],
              COL_SASARANTARGETTW1=>$ps[COL_SASARANTARGETTW1],
              COL_SASARANTARGETTW2=>$ps[COL_SASARANTARGETTW2],
              COL_SASARANTARGETTW3=>$ps[COL_SASARANTARGETTW3],
              COL_SASARANTARGETTW4=>$ps[COL_SASARANTARGETTW4],
              COL_SASARANREALISASITW1=>$ps[COL_SASARANREALISASITW1],
              COL_SASARANREALISASITW2=>$ps[COL_SASARANREALISASITW2],
              COL_SASARANREALISASITW3=>$ps[COL_SASARANREALISASITW3],
              COL_SASARANREALISASITW4=>$ps[COL_SASARANREALISASITW4],
              COL_CREATEDBY=>$ruser[COL_USERNAME],
              COL_CREATEDON=>date('Y-m-d H:i:s')
            );
          }
          if(!empty($arrProgSasaran)) {
            $res = $this->db->insert_batch(TBL_SAKIPV2_BID_PROGSASARAN, $arrProgSasaran);
            if(!$res) {
              $err = $this->db->error();
              throw new Exception($err['message']);
            }
          }

          $rkegiatan = $this->db
          ->where(COL_IDPROGRAM, $prg[COL_PROGRAMID])
          ->order_by(COL_KEGIATANKODE)
          ->get(TBL_SAKIPV2_BID_KEGIATAN)
          ->result_array();

          foreach($rkegiatan as $keg) {
            $arrKegiatan = array(
              COL_IDPROGRAM=>$idProgram,
              COL_KEGIATANKODE=>$keg[COL_KEGIATANKODE],
              COL_KEGIATANURAIAN=>$keg[COL_KEGIATANURAIAN],
              COL_CREATEDBY=>$ruser[COL_USERNAME],
              COL_CREATEDON=>date('Y-m-d H:i:s')
            );

            $res = $this->db->insert(TBL_SAKIPV2_BID_KEGIATAN, $arrKegiatan);
            if(!$res) {
              $err = $this->db->error();
              throw new Exception($err['message']);
            }
            $idKegiatan = $this->db->insert_id();

            $arrKegSasaran = array();
            $rkegsasaran = $this->db
            ->where(COL_IDKEGIATAN, $keg[COL_KEGIATANID])
            ->order_by(COL_SASARANNO)
            ->get(TBL_SAKIPV2_BID_KEGSASARAN)
            ->result_array();

            foreach($rkegsasaran as $ks) {
              $arrKegSasaran[] = array(
                COL_IDKEGIATAN=>$idKegiatan,
                COL_SASARANNO=>$ks[COL_SASARANNO],
                COL_SASARANURAIAN=>$ks[COL_SASARANURAIAN],
                COL_SASARANINDIKATOR=>$ks[COL_SASARANINDIKATOR],
                COL_SASARANSATUAN=>$ks[COL_SASARANSATUAN],
                COL_SASARANTARGET=>$ks[COL_SASARANTARGET],
                COL_SASARANTARGETTW1=>$ks[COL_SASARANTARGETTW1],
                COL_SASARANTARGETTW2=>$ks[COL_SASARANTARGETTW2],
                COL_SASARANTARGETTW3=>$ks[COL_SASARANTARGETTW3],
                COL_SASARANTARGETTW4=>$ks[COL_SASARANTARGETTW4],
                COL_SASARANREALISASITW1=>$ks[COL_SASARANREALISASITW1],
                COL_SASARANREALISASITW2=>$ks[COL_SASARANREALISASITW2],
                COL_SASARANREALISASITW3=>$ks[COL_SASARANREALISASITW3],
                COL_SASARANREALISASITW4=>$ks[COL_SASARANREALISASITW4],
                COL_CREATEDBY=>$ruser[COL_USERNAME],
                COL_CREATEDON=>date('Y-m-d H:i:s')
              );
            }

            if(!empty($arrKegSasaran)) {
              $res = $this->db->insert_batch(TBL_SAKIPV2_BID_KEGSASARAN, $arrKegSasaran);
              if(!$res) {
                $err = $this->db->error();
                throw new Exception($err['message']);
              }
            }

            $rsubkegiatan = $this->db
            ->where(COL_IDKEGIATAN, $keg[COL_KEGIATANID])
            ->order_by(COL_SUBKEGKODE)
            ->get(TBL_SAKIPV2_SUBBID_SUBKEGIATAN)
            ->result_array();

            foreach($rsubkegiatan as $subkeg) {
              $arrSubKegiatan = array(
                COL_IDKEGIATAN=>$idKegiatan,
                COL_IDSUBBID=>$subkeg[COL_IDSUBBID],
                COL_SUBKEGKODE=>$subkeg[COL_SUBKEGKODE],
                COL_SUBKEGURAIAN=>$subkeg[COL_SUBKEGURAIAN],
                COL_SUBKEGPAGU=>$subkeg[COL_SUBKEGPAGU],
                COL_SUBKEGREALISASI=>$subkeg[COL_SUBKEGREALISASI],
                COL_SUBKEGPAGUTW1=>$subkeg[COL_SUBKEGPAGUTW1],
                COL_SUBKEGPAGUTW2=>$subkeg[COL_SUBKEGPAGUTW2],
                COL_SUBKEGPAGUTW3=>$subkeg[COL_SUBKEGPAGUTW3],
                COL_SUBKEGPAGUTW4=>$subkeg[COL_SUBKEGPAGUTW4],
                COL_SUBKEGREALISASITW1=>$subkeg[COL_SUBKEGREALISASITW1],
                COL_SUBKEGREALISASITW2=>$subkeg[COL_SUBKEGREALISASITW2],
                COL_SUBKEGREALISASITW3=>$subkeg[COL_SUBKEGREALISASITW3],
                COL_SUBKEGREALISASITW4=>$subkeg[COL_SUBKEGREALISASITW4],
                COL_CREATEDBY=>$ruser[COL_USERNAME],
                COL_CREATEDON=>date('Y-m-d H:i:s')
              );

              $res = $this->db->insert(TBL_SAKIPV2_SUBBID_SUBKEGIATAN, $arrSubKegiatan);
              if(!$res) {
                $err = $this->db->error();
                throw new Exception($err['message']);
              }
              $idSubKegiatan = $this->db->insert_id();

              $arrSubKegSasaran = array();
              $rsubkegsasaran = $this->db
              ->where(COL_IDSUBKEG, $subkeg[COL_SUBKEGID])
              ->order_by(COL_SASARANNO)
              ->get(TBL_SAKIPV2_SUBBID_SUBKEGSASARAN)
              ->result_array();

              foreach($rsubkegsasaran as $sks) {
                $arrSubKegSasaran[] = array(
                  COL_IDSUBKEG=>$idSubKegiatan,
                  COL_SASARANNO=>$sks[COL_SASARANNO],
                  COL_SASARANURAIAN=>$sks[COL_SASARANURAIAN],
                  COL_SASARANINDIKATOR=>$sks[COL_SASARANINDIKATOR],
                  COL_SASARANSATUAN=>$sks[COL_SASARANSATUAN],
                  COL_SASARANTARGET=>$sks[COL_SASARANTARGET],
                  COL_SASARANTARGETTW1=>$sks[COL_SASARANTARGETTW1],
                  COL_SASARANTARGETTW2=>$sks[COL_SASARANTARGETTW2],
                  COL_SASARANTARGETTW3=>$sks[COL_SASARANTARGETTW3],
                  COL_SASARANTARGETTW4=>$sks[COL_SASARANTARGETTW4],
                  COL_SASARANREALISASITW1=>$sks[COL_SASARANREALISASITW1],
                  COL_SASARANREALISASITW2=>$sks[COL_SASARANREALISASITW2],
                  COL_SASARANREALISASITW3=>$sks[COL_SASARANREALISASITW3],
                  COL_SASARANREALISASITW4=>$sks[COL_SASARANREALISASITW4],
                  COL_CREATEDBY=>$ruser[COL_USERNAME],
                  COL_CREATEDON=>date('Y-m-d H:i:s')
                );
              }

              if(!empty($arrSubKegSasaran)) {
                $res = $this->db->insert_batch(TBL_SAKIPV2_SUBBID_SUBKEGSASARAN, $arrSubKegSasaran);
                if(!$res) {
                  $err = $this->db->error();
                  throw new Exception($err['message']);
                }
              }
            }
          }
        }

        $this->db->trans_commit();
        ShowJsonSuccess('Proses duplikasi <strong>BERHASIL</strong>!');
        exit();
      } catch(Exception $ex) {
        $this->db->trans_rollback();
        ShowJsonError($ex->getMessage());
        exit();
      }
    }
  }

  public function execute_dpa_duplication() {
    $ruser=GetLoggedUser();
    if(!empty($_GET)) {
      $dpaTahun = $_GET[COL_DPATAHUN];
      $dpaUraian = $_GET[COL_DPAURAIAN];

      if($_GET['key']!='server!2345') {
        echo 'KEY INVALID!!';
        exit();
      }

      $rdpa = $this->db
      ->where(COL_DPAISAKTIF, 1)
      ->where(COL_DPATAHUN, $dpaTahun)
      ->get(TBL_SAKIPV2_SKPD_RENSTRA_DPA)
      ->result_array();

      $this->db->trans_begin();
      try {
        foreach($rdpa as $dpa) {
          $arrDPA = array(
            COL_IDRENSTRA=>$dpa[COL_IDRENSTRA],
            COL_DPATAHUN=>$dpa[COL_DPATAHUN],
            COL_DPAISAKTIF=>0,
            COL_DPAURAIAN=>$dpaUraian,
            COL_CREATEDBY=>$ruser[COL_USERNAME],
            COL_CREATEDON=>date('Y-m-d H:i:s')
          );
          $res = $this->db->insert(TBL_SAKIPV2_SKPD_RENSTRA_DPA, $arrDPA);
          if(!$res) {
            $err = $this->db->error();
            throw new Exception($err['message']);
          }

          $idDPA = $this->db->insert_id();
          $rprogram = $this->db
          ->where(COL_IDDPA, $dpa[COL_DPAID])
          ->order_by(COL_PROGRAMKODE)
          ->get(TBL_SAKIPV2_BID_PROGRAM)
          ->result_array();

          foreach($rprogram as $prg) {
            $arrProgram = array(
              COL_IDBID=>$prg[COL_IDBID],
              COL_IDDPA=>$idDPA,
              COL_IDSASARANSKPD=>$prg[COL_IDSASARANSKPD],
              COL_PROGRAMKODE=>$prg[COL_PROGRAMKODE],
              COL_PROGRAMURAIAN=>$prg[COL_PROGRAMURAIAN],
              COL_CREATEDBY=>$ruser[COL_USERNAME],
              COL_CREATEDON=>date('Y-m-d H:i:s')
            );

            $res = $this->db->insert(TBL_SAKIPV2_BID_PROGRAM, $arrProgram);
            if(!$res) {
              $err = $this->db->error();
              throw new Exception($err['message']);
            }
            $idProgram = $this->db->insert_id();

            $arrProgSasaran = array();
            $rprogsasaran = $this->db
            ->where(COL_IDPROGRAM, $prg[COL_PROGRAMID])
            ->order_by(COL_SASARANNO)
            ->get(TBL_SAKIPV2_BID_PROGSASARAN)
            ->result_array();
            foreach($rprogsasaran as $ps) {
              $arrProgSasaran[] = array(
                COL_IDPROGRAM=>$idProgram,
                COL_SASARANNO=>$ps[COL_SASARANNO],
                COL_SASARANURAIAN=>$ps[COL_SASARANURAIAN],
                COL_SASARANINDIKATOR=>$ps[COL_SASARANINDIKATOR],
                COL_SASARANSATUAN=>$ps[COL_SASARANSATUAN],
                COL_SASARANTARGET=>$ps[COL_SASARANTARGET],
                COL_SASARANTARGETTW1=>$ps[COL_SASARANTARGETTW1],
                COL_SASARANTARGETTW2=>$ps[COL_SASARANTARGETTW2],
                COL_SASARANTARGETTW3=>$ps[COL_SASARANTARGETTW3],
                COL_SASARANTARGETTW4=>$ps[COL_SASARANTARGETTW4],
                COL_SASARANREALISASITW1=>$ps[COL_SASARANREALISASITW1],
                COL_SASARANREALISASITW2=>$ps[COL_SASARANREALISASITW2],
                COL_SASARANREALISASITW3=>$ps[COL_SASARANREALISASITW3],
                COL_SASARANREALISASITW4=>$ps[COL_SASARANREALISASITW4],
                COL_CREATEDBY=>$ruser[COL_USERNAME],
                COL_CREATEDON=>date('Y-m-d H:i:s')
              );
            }
            if(!empty($arrProgSasaran)) {
              $res = $this->db->insert_batch(TBL_SAKIPV2_BID_PROGSASARAN, $arrProgSasaran);
              if(!$res) {
                $err = $this->db->error();
                throw new Exception($err['message']);
              }
            }

            $rkegiatan = $this->db
            ->where(COL_IDPROGRAM, $prg[COL_PROGRAMID])
            ->order_by(COL_KEGIATANKODE)
            ->get(TBL_SAKIPV2_BID_KEGIATAN)
            ->result_array();

            foreach($rkegiatan as $keg) {
              $arrKegiatan = array(
                COL_IDPROGRAM=>$idProgram,
                COL_KEGIATANKODE=>$keg[COL_KEGIATANKODE],
                COL_KEGIATANURAIAN=>$keg[COL_KEGIATANURAIAN],
                COL_CREATEDBY=>$ruser[COL_USERNAME],
                COL_CREATEDON=>date('Y-m-d H:i:s')
              );

              $res = $this->db->insert(TBL_SAKIPV2_BID_KEGIATAN, $arrKegiatan);
              if(!$res) {
                $err = $this->db->error();
                throw new Exception($err['message']);
              }
              $idKegiatan = $this->db->insert_id();

              $arrKegSasaran = array();
              $rkegsasaran = $this->db
              ->where(COL_IDKEGIATAN, $keg[COL_KEGIATANID])
              ->order_by(COL_SASARANNO)
              ->get(TBL_SAKIPV2_BID_KEGSASARAN)
              ->result_array();

              foreach($rkegsasaran as $ks) {
                $arrKegSasaran[] = array(
                  COL_IDKEGIATAN=>$idKegiatan,
                  COL_SASARANNO=>$ks[COL_SASARANNO],
                  COL_SASARANURAIAN=>$ks[COL_SASARANURAIAN],
                  COL_SASARANINDIKATOR=>$ks[COL_SASARANINDIKATOR],
                  COL_SASARANSATUAN=>$ks[COL_SASARANSATUAN],
                  COL_SASARANTARGET=>$ks[COL_SASARANTARGET],
                  COL_SASARANTARGETTW1=>$ks[COL_SASARANTARGETTW1],
                  COL_SASARANTARGETTW2=>$ks[COL_SASARANTARGETTW2],
                  COL_SASARANTARGETTW3=>$ks[COL_SASARANTARGETTW3],
                  COL_SASARANTARGETTW4=>$ks[COL_SASARANTARGETTW4],
                  COL_SASARANREALISASITW1=>$ks[COL_SASARANREALISASITW1],
                  COL_SASARANREALISASITW2=>$ks[COL_SASARANREALISASITW2],
                  COL_SASARANREALISASITW3=>$ks[COL_SASARANREALISASITW3],
                  COL_SASARANREALISASITW4=>$ks[COL_SASARANREALISASITW4],
                  COL_CREATEDBY=>$ruser[COL_USERNAME],
                  COL_CREATEDON=>date('Y-m-d H:i:s')
                );
              }

              if(!empty($arrKegSasaran)) {
                $res = $this->db->insert_batch(TBL_SAKIPV2_BID_KEGSASARAN, $arrKegSasaran);
                if(!$res) {
                  $err = $this->db->error();
                  throw new Exception($err['message']);
                }
              }

              $rsubkegiatan = $this->db
              ->where(COL_IDKEGIATAN, $keg[COL_KEGIATANID])
              ->order_by(COL_SUBKEGKODE)
              ->get(TBL_SAKIPV2_SUBBID_SUBKEGIATAN)
              ->result_array();

              foreach($rsubkegiatan as $subkeg) {
                $arrSubKegiatan = array(
                  COL_IDKEGIATAN=>$idKegiatan,
                  COL_IDSUBBID=>$subkeg[COL_IDSUBBID],
                  COL_SUBKEGKODE=>$subkeg[COL_SUBKEGKODE],
                  COL_SUBKEGURAIAN=>$subkeg[COL_SUBKEGURAIAN],
                  COL_SUBKEGPAGU=>$subkeg[COL_SUBKEGPAGU],
                  COL_SUBKEGREALISASI=>$subkeg[COL_SUBKEGREALISASI],
                  COL_SUBKEGPAGUTW1=>$subkeg[COL_SUBKEGPAGUTW1],
                  COL_SUBKEGPAGUTW2=>$subkeg[COL_SUBKEGPAGUTW2],
                  COL_SUBKEGPAGUTW3=>$subkeg[COL_SUBKEGPAGUTW3],
                  COL_SUBKEGPAGUTW4=>$subkeg[COL_SUBKEGPAGUTW4],
                  COL_SUBKEGREALISASITW1=>$subkeg[COL_SUBKEGREALISASITW1],
                  COL_SUBKEGREALISASITW2=>$subkeg[COL_SUBKEGREALISASITW2],
                  COL_SUBKEGREALISASITW3=>$subkeg[COL_SUBKEGREALISASITW3],
                  COL_SUBKEGREALISASITW4=>$subkeg[COL_SUBKEGREALISASITW4],
                  COL_CREATEDBY=>$ruser[COL_USERNAME],
                  COL_CREATEDON=>date('Y-m-d H:i:s')
                );

                $res = $this->db->insert(TBL_SAKIPV2_SUBBID_SUBKEGIATAN, $arrSubKegiatan);
                if(!$res) {
                  $err = $this->db->error();
                  throw new Exception($err['message']);
                }
                $idSubKegiatan = $this->db->insert_id();

                $arrSubKegSasaran = array();
                $rsubkegsasaran = $this->db
                ->where(COL_IDSUBKEG, $subkeg[COL_SUBKEGID])
                ->order_by(COL_SASARANNO)
                ->get(TBL_SAKIPV2_SUBBID_SUBKEGSASARAN)
                ->result_array();

                foreach($rsubkegsasaran as $sks) {
                  $arrSubKegSasaran[] = array(
                    COL_IDSUBKEG=>$idSubKegiatan,
                    COL_SASARANNO=>$sks[COL_SASARANNO],
                    COL_SASARANURAIAN=>$sks[COL_SASARANURAIAN],
                    COL_SASARANINDIKATOR=>$sks[COL_SASARANINDIKATOR],
                    COL_SASARANSATUAN=>$sks[COL_SASARANSATUAN],
                    COL_SASARANTARGET=>$sks[COL_SASARANTARGET],
                    COL_SASARANTARGETTW1=>$sks[COL_SASARANTARGETTW1],
                    COL_SASARANTARGETTW2=>$sks[COL_SASARANTARGETTW2],
                    COL_SASARANTARGETTW3=>$sks[COL_SASARANTARGETTW3],
                    COL_SASARANTARGETTW4=>$sks[COL_SASARANTARGETTW4],
                    COL_SASARANREALISASITW1=>$sks[COL_SASARANREALISASITW1],
                    COL_SASARANREALISASITW2=>$sks[COL_SASARANREALISASITW2],
                    COL_SASARANREALISASITW3=>$sks[COL_SASARANREALISASITW3],
                    COL_SASARANREALISASITW4=>$sks[COL_SASARANREALISASITW4],
                    COL_CREATEDBY=>$ruser[COL_USERNAME],
                    COL_CREATEDON=>date('Y-m-d H:i:s')
                  );
                }

                if(!empty($arrSubKegSasaran)) {
                  $res = $this->db->insert_batch(TBL_SAKIPV2_SUBBID_SUBKEGSASARAN, $arrSubKegSasaran);
                  if(!$res) {
                    $err = $this->db->error();
                    throw new Exception($err['message']);
                  }
                }
              }
            }
          }
        }

        $this->db->trans_commit();
        echo 'DONE';
        exit();
      } catch(Exception $ex) {
        $this->db->trans_rollback();
        ShowJsonError($ex->getMessage());
        exit();
      }
    } else {
      $html = @"
      <form method='get'>
        <p>KEY <input type='text' name='key' /></p>
        <p>TAHUN <input type='text' name='DPATahun' /></p>
        <p>URAIAN <input type='text' name='DPAUraian' /></p>
        <p><button type='submit'>SUBMIT</button></p>
      </form>
      ";
      echo $html;
    }
  }

  public function anggaran() {
    if(!empty($_GET['idSasaranPmd'])) {
      $data['rpemda'] = $this->db
      ->select('sakipv2_pemda.*')
      ->join(TBL_SAKIPV2_PEMDA_TUJUAN,TBL_SAKIPV2_PEMDA_TUJUAN.'.'.COL_TUJUANID." = ".TBL_SAKIPV2_PEMDA_SASARAN.".".COL_IDTUJUAN,"inner")
      ->join(TBL_SAKIPV2_PEMDA_MISI,TBL_SAKIPV2_PEMDA_MISI.'.'.COL_MISIID." = ".TBL_SAKIPV2_PEMDA_TUJUAN.".".COL_IDMISI,"inner")
      ->join(TBL_SAKIPV2_PEMDA,TBL_SAKIPV2_PEMDA.'.'.COL_PMDID." = ".TBL_SAKIPV2_PEMDA_MISI.".".COL_IDPMD,"inner")
      ->where(COL_SASARANID, $_GET['idSasaranPmd'])
      ->get(TBL_SAKIPV2_PEMDA_SASARAN)
      ->row_array();
    } else {
      $data['rpemda'] = $this->db
      ->where(COL_PMDISAKTIF, 1)
      ->get(TBL_SAKIPV2_PEMDA)
      ->row_array();
    }

    $data['title'] = 'Realisasi Anggaran SKPD';
    $this->template->load('main', 'sakipv2/skpd/anggaran', $data);
  }

  public function anggaran_ajax_change_realisasi($id) {
    $ruser = GetLoggedUser();
    if($ruser[COL_ROLEID]!=ROLEADMIN && $ruser[COL_ROLEID]!=ROLEKADIS) {
      $this->showErrUnathorized();
    }

    $val = $this->input->post('value');
    $res = $this->db->where(COL_SUBKEGID, $id)->update(TBL_SAKIPV2_SUBBID_SUBKEGIATAN, array(COL_SUBKEGREALISASI=>toNum($val)));
    if(!$res) {
      $err = $this->db->error();
      ShowJsonError($err['message']);
      exit();
    }
    ShowJsonSuccess('Berhasil!');
    exit();
  }

  public function ajax_upload_struktur($id) {
    $rrenstra = $this->db
    ->where(COL_RENSTRAID, $id)
    ->get(TBL_SAKIPV2_SKPD_RENSTRA)
    ->row_array();

    if(!empty($_FILES)) {
      $config['upload_path'] = MY_UPLOADPATH;
      $config['allowed_types'] = "jpg|jpeg|png";
      $config['max_size']	= 5120;
      $config['max_width']  = 4000;
      $config['max_height']  = 4000;
      $config['overwrite'] = FALSE;

      $this->load->library('upload',$config);
      if(empty($rrenstra)) {
        ShowJsonError('PARAMETER TIDAK VALID!');
        exit();
      }

      $this->db->trans_begin();
      try {
        $filename = '';
        if(!empty($_FILES)) {
          $res = $this->upload->do_upload('file');
          if(!$res) {
            $err = $this->upload->display_errors('', '');
            throw new Exception($err);
          }
          $upl = $this->upload->data();
          $filename = $upl['file_name'];
        }

        if(empty($filename)) {
          throw new Exception('PARAMETER TIDAK VALID!');
        }

        $res = $this->db->where(COL_RENSTRAID, $id)->update(TBL_SAKIPV2_SKPD_RENSTRA, array(COL_RENSTRAORG=>$filename));
        if(!$res) {
          $err = $this->db->error();
          throw new Exception($err['message']);
        }

        $this->db->trans_commit();
        ShowJsonSuccess('Berhasil mengunggah Struktur Organisasi!');
        exit();
      } catch(Exception $ex) {
        $this->db->trans_rollback();
        ShowJsonError($ex->getMessage());
        exit();
      }
    } else {
      ShowJsonError('PARAMETER TIDAK VALID!');
    }
  }

  public function renaksi() {
    $data['title'] = 'Rencana Aksi';
    $this->template->load('main', 'sakipv2/skpd/renaksi', $data);
  }

  public function renaksi_data($idSKPD, $idRenstra, $idDPA, $idTW, $modeCetak='') {
    $data = array(
      'idSKPD'=>$idSKPD,
      'idRenstra'=>$idRenstra,
      'idDPA'=>$idDPA,
      'idTW'=>$idTW,
      'modCetak'=>$modeCetak
    );
    $data['title'] = 'RENCANA AKSI';
    $rdpa = $this->db
    ->where(COL_DPAID, $idDPA)
    ->get(TBL_SAKIPV2_SKPD_RENSTRA_DPA)
    ->row_array();
    if(empty($rdpa)) {
      show_error('DPA TIDAK VALID!');
      exit();
    }
    $rskpd = $this->db
    ->where(COL_SKPDID, $idSKPD)
    ->get(TBL_SAKIPV2_SKPD)
    ->row_array();
    if(empty($rskpd)) {
      show_error('SKPD TIDAK VALID!');
      exit();
    }
    if(!empty($modeCetak)) {
      $this->load->library('Mypdf');
      $mpdf = new Mypdf('utf-8', 'A4-L', 0,'',15,15,30,16);

      $html = $this->load->view('sakipv2/skpd/renaksi-partial', $data, TRUE);
      $htmlLogo = MY_IMAGEPATH.'logo.png';
      $htmlYear = $rdpa[COL_DPATAHUN];
      $htmlTitle = $data['title'].'<br />'.strtoupper($rskpd[COL_SKPDNAMA]);
      $htmlHeader = @"
      <table style=\"border: none !important\">
        <tr>
          <td rowspan=\"2\" style=\"border: none !important; padding: 0 !important; width: 30px; white-space: nowrap; vertical-align: top\"><img src=\"$htmlLogo\" style=\"width: 25px\" /></td>
          <td style=\"border: none !important; padding: 0 !important; font-size: 10pt; font-weight: bold\">$htmlTitle</td>
        </tr>
        <tr>
          <td style=\"border: none !important; padding: 0 !important; font-size: 10pt; font-weight: bold\">KOTA TEBING TINGGI TAHUN $htmlYear</td>
        </tr>
      </table>
      <hr />
      ";
      //echo $html;
      //return;
      $mpdf->pdf->SetTitle($data['title']);
      $mpdf->pdf->SetHTMLHeader($htmlHeader);
      $mpdf->pdf->SetFooter('Dicetak melalui aplikasi '.$this->setting_web_name.' pada '.date('d-m-Y H:i'));
      $mpdf->pdf->WriteHTML($html);
      $mpdf->pdf->Output('SITALAKBAJAKUN - '.$data['title'].' '.date('YmdHis').'.pdf', 'I');
    } else {
      $this->load->view('sakipv2/skpd/renaksi-partial', $data);
    }
  }

  public function renaksi_print($idSKPD, $idRenstra, $idDPA, $idTW) {
    $data = array(
      'idSKPD'=>$idSKPD,
      'idRenstra'=>$idRenstra,
      'idDPA'=>$idDPA,
      'idTW'=>$idTW,
      'modCetak'=>'renaksi'
    );
    $data['title'] = 'RENCANA AKSI';
    $rdpa = $this->db
    ->where(COL_DPAID, $idDPA)
    ->get(TBL_SAKIPV2_SKPD_RENSTRA_DPA)
    ->row_array();
    if(empty($rdpa)) {
      show_error('DPA TIDAK VALID!');
      exit();
    }
    $rskpd = $this->db
    ->where(COL_SKPDID, $idSKPD)
    ->get(TBL_SAKIPV2_SKPD)
    ->row_array();
    if(empty($rskpd)) {
      show_error('SKPD TIDAK VALID!');
      exit();
    }
    $this->load->library('Mypdf');
    $mpdf = new Mypdf('utf-8', 'A4-L', 0,'',15,15,30,16);

    $html = $this->load->view('sakipv2/skpd/renaksi-print', $data, TRUE);
    $htmlLogo = MY_IMAGEPATH.'logo.png';
    $htmlYear = $rdpa[COL_DPATAHUN];
    $htmlTitle = $data['title'].'<br />'.strtoupper($rskpd[COL_SKPDNAMA]);
    $htmlHeader = @"
    <table style=\"border: none !important\">
      <tr>
        <td rowspan=\"2\" style=\"border: none !important; padding: 0 !important; width: 30px; white-space: nowrap; vertical-align: top\"><img src=\"$htmlLogo\" style=\"width: 25px\" /></td>
        <td style=\"border: none !important; padding: 0 !important; font-size: 10pt; font-weight: bold\">$htmlTitle</td>
      </tr>
      <tr>
        <td style=\"border: none !important; padding: 0 !important; font-size: 10pt; font-weight: bold\">KOTA TEBING TINGGI TAHUN $htmlYear</td>
      </tr>
    </table>
    <hr />
    ";
    //echo $html;
    //return;
    $mpdf->pdf->SetTitle($data['title']);
    $mpdf->pdf->SetHTMLHeader($htmlHeader);
    $mpdf->pdf->SetFooter('Dicetak melalui aplikasi '.$this->setting_web_name.' pada '.date('d-m-Y H:i'));
    $mpdf->pdf->WriteHTML($html);
    $mpdf->pdf->Output('SITALAKBAJAKUN - '.$data['title'].' '.date('YmdHis').'.pdf', 'I');
  }

  public function renaksi_monev_print($idSKPD, $idRenstra, $idDPA, $idTW) {
    $data = array(
      'idSKPD'=>$idSKPD,
      'idRenstra'=>$idRenstra,
      'idDPA'=>$idDPA,
      'idTW'=>$idTW,
      'modCetak'=>'renaksi'
    );
    $data['title'] = 'MONITORING & EVALUASI';
    $rdpa = $this->db
    ->where(COL_DPAID, $idDPA)
    ->get(TBL_SAKIPV2_SKPD_RENSTRA_DPA)
    ->row_array();
    if(empty($rdpa)) {
      show_error('DPA TIDAK VALID!');
      exit();
    }
    $rskpd = $this->db
    ->where(COL_SKPDID, $idSKPD)
    ->get(TBL_SAKIPV2_SKPD)
    ->row_array();
    if(empty($rskpd)) {
      show_error('SKPD TIDAK VALID!');
      exit();
    }
    $this->load->library('Mypdf');
    $mpdf = new Mypdf('utf-8', 'A4-L', 0,'',15,15,30,16);

    $html = $this->load->view('sakipv2/skpd/renaksi-monev-print', $data, TRUE);
    $htmlLogo = MY_IMAGEPATH.'logo.png';
    $htmlYear = $rdpa[COL_DPATAHUN];
    $htmlTitle = $data['title'].'<br />'.strtoupper($rskpd[COL_SKPDNAMA]);
    $htmlHeader = @"
    <table style=\"border: none !important\">
      <tr>
        <td rowspan=\"2\" style=\"border: none !important; padding: 0 !important; width: 30px; white-space: nowrap; vertical-align: top\"><img src=\"$htmlLogo\" style=\"width: 25px\" /></td>
        <td style=\"border: none !important; padding: 0 !important; font-size: 10pt; font-weight: bold\">$htmlTitle</td>
      </tr>
      <tr>
        <td style=\"border: none !important; padding: 0 !important; font-size: 10pt; font-weight: bold\">KOTA TEBING TINGGI TAHUN $htmlYear</td>
      </tr>
    </table>
    <hr />
    ";
    //echo $html;
    //return;
    $mpdf->pdf->SetTitle($data['title']);
    $mpdf->pdf->SetHTMLHeader($htmlHeader);
    $mpdf->pdf->SetFooter('Dicetak melalui aplikasi '.$this->setting_web_name.' pada '.date('d-m-Y H:i'));
    $mpdf->pdf->WriteHTML($html);
    $mpdf->pdf->Output('SITALAKBAJAKUN - '.$data['title'].' '.date('YmdHis').'.pdf', 'I');
  }

  public function renaksi_add($idSubkeg, $idTW) {
    $ruser = GetLoggedUser();
    $rsubkeg = $this->db
    ->where(COL_SUBKEGID, $idSubkeg)
    ->get(TBL_SAKIPV2_SUBBID_SUBKEGIATAN)
    ->row_array();

    if(empty($rsubkeg)) {
      echo 'Parameter tidak valid!';
      exit();
    }

    if(!empty($_POST)) {
      $dat = array(
        COL_IDSUBKEG=>$idSubkeg,
        COL_RENAKSIURAIAN=>$this->input->post(COL_RENAKSIURAIAN),
        COL_RENAKSIINDIKATOR=>$this->input->post(COL_RENAKSIINDIKATOR),
        COL_RENAKSISATUAN=>$this->input->post(COL_RENAKSISATUAN),
        'RenaksiTarget'.($idTW!=99?'TW'.$idTW:'')=>$this->input->post('RenaksiTarget'.($idTW!=99?'TW'.$idTW:'')),
        'RenaksiCapaian'.($idTW!=99?'TW'.$idTW:'')=>$this->input->post('RenaksiCapaian'.($idTW!=99?'TW'.$idTW:'')),
        'RenaksiRemarks'.($idTW!=99?'TW'.$idTW:'')=>$this->input->post('RenaksiRemarks'.($idTW!=99?'TW'.$idTW:'')),
        COL_CREATEDBY=>$ruser[COL_USERNAME],
        COL_CREATEDON=>date('Y-m-d H:i:s')
      );

      $res = $this->db->insert(TBL_SAKIPV2_SKPD_RENAKSI, $dat);
      if(!$res) {
        $err = $this->db->error();
        ShowJsonError($err['message']);
        exit();
      }

      ShowJsonSuccess('INPUT DATA BERHASIL!');
      exit();
    } else {
      $data['subkeg'] = $rsubkeg;
      $data['tw'] = $idTW;
      $this->load->view('sakipv2/skpd/renaksi-form', $data);
    }
  }

  public function renaksi_edit($id, $idTW) {
    $ruser = GetLoggedUser();

    $rrenaksi = $this->db
    ->where(COL_UNIQ, $id)
    ->get(TBL_SAKIPV2_SKPD_RENAKSI)
    ->row_array();

    if(empty($rrenaksi)) {
      echo 'Parameter tidak valid!';
      exit();
    }

    $rsubkeg = $this->db
    ->where(COL_SUBKEGID, $rrenaksi[COL_IDSUBKEG])
    ->get(TBL_SAKIPV2_SUBBID_SUBKEGIATAN)
    ->row_array();

    if(empty($rsubkeg)) {
      echo 'Parameter tidak valid!';
      exit();
    }

    if(!empty($_POST)) {
      $dat = array(
        COL_RENAKSIURAIAN=>$this->input->post(COL_RENAKSIURAIAN),
        COL_RENAKSIINDIKATOR=>$this->input->post(COL_RENAKSIINDIKATOR),
        COL_RENAKSISATUAN=>$this->input->post(COL_RENAKSISATUAN),
        'RenaksiTarget'.($idTW!=99?'TW'.$idTW:'')=>$this->input->post('RenaksiTarget'.($idTW!=99?'TW'.$idTW:'')),
        'RenaksiCapaian'.($idTW!=99?'TW'.$idTW:'')=>$this->input->post('RenaksiCapaian'.($idTW!=99?'TW'.$idTW:'')),
        'RenaksiRemarks'.($idTW!=99?'TW'.$idTW:'')=>$this->input->post('RenaksiRemarks'.($idTW!=99?'TW'.$idTW:'')),
        COL_UPDATEDBY=>$ruser[COL_USERNAME],
        COL_UPDATEDON=>date('Y-m-d H:i:s')
      );

      $res = $this->db->where(COL_UNIQ, $id)->update(TBL_SAKIPV2_SKPD_RENAKSI, $dat);
      if(!$res) {
        $err = $this->db->error();
        ShowJsonError($err['message']);
        exit();
      }

      ShowJsonSuccess('PEMBARUAN DATA BERHASIL.');
      exit();
    } else {
      $data['subkeg'] = $rsubkeg;
      $data['tw'] = $idTW;
      $data['data'] = $rrenaksi;
      $this->load->view('sakipv2/skpd/renaksi-form', $data);
    }
  }

  public function renaksi_delete($id) {
    $res = $this->db->where(COL_UNIQ, $id)->delete(TBL_SAKIPV2_SKPD_RENAKSI);
    if(!$res) {
      $err = $this->db->error();
      ShowJsonError($err['message']);
      exit();
    }

    ShowJsonSuccess('Data berhasil dihapus.');
    exit();
  }
}
