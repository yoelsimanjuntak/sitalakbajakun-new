<?php
class Import extends MY_Controller {
  function __construct() {
      parent::__construct();
      if(!IsLogin()) {
        redirect(site_url());
      }
  }

  public function form($idSkpd=null,$idRenstra=null,$idDPA=null) {
    $ruser = GetLoggedUser();
    $data['title'] = 'Import Data Sub Kegiatan';
    if(!empty($_POST)) {
      $config['upload_path'] = MY_UPLOADPATH;
      $config['allowed_types'] = "xls|xlsx";
      $config['max_size']	= 5120;
      $config['file_name'] = 'import-'.date('YmdHis').'xls';
      $config['overwrite'] = FALSE;

      $this->load->library('upload',$config);

      $this->db->trans_begin();
      try {
        $rec = array(
          COL_IDSKPD=>$idSkpd,
          COL_IDRENSTRA=>$idRenstra,
          COL_IDDPA=>$idDPA,

          COL_CREATEDBY=>$ruser[COL_USERNAME],
          COL_CREATEDON=>date('Y-m-d H:i:s')
        );

        if($ruser[COL_ROLEID]!=ROLEADMIN&&$ruser[COL_ROLEID]!=ROLEGUEST) {
          $rec[COL_IDSKPD]=$ruser[COL_SKPDID];
        }

        if(!empty($_FILES)) {
          $res = $this->upload->do_upload('file');
          if(!$res) {
            $err = $this->upload->display_errors('', '');
            throw new Exception($err);
          }
          $upl = $this->upload->data();
          $rec[COL_IMPORTPATH] = $upl['file_name'];
        }

        $res = $this->db->insert(TBL_SAKIPV2_SKPD_IMPORTS, $rec);
        if(!$res) {
          $err = $this->db->error();
          throw new Exception($err['message']);
        }

        $this->db->trans_commit();
        ShowJsonSuccess('INPUT DATA BERHASIL');
        exit();
      } catch(Exception $ex) {
        $this->db->trans_rollback();
        ShowJsonError($ex->getMessage());
        exit();
      }
    } else {
      $data['idSkpd'] = $idSkpd;
      $data['idRenstra'] = $idRenstra;
      $data['idDPA'] = $idDPA;
      $this->template->load('main', 'sakipv2/import/form', $data);
    }

  }

  public function load($id) {
    show_error('Parameter tidak valid!');
    exit();
  }
}
