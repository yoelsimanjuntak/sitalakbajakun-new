<?php
class Bidang extends MY_Controller {
  function __construct() {
      parent::__construct();
      if(!IsLogin()) {
        redirect(site_url());
      }
  }

  public function showErrUnathorized() {
    $act = $this->router->fetch_method();
    if($this->input->is_ajax_request() && strpos($act, "_form")===false) {
      ShowJsonError('MAAF, ANDA TIDAK MEMILIKI HAK AKSES.');
      exit();
    }else{
      echo 'MAAF, ANDA TIDAK MEMILIKI HAK AKSES.';
      exit();
    }
  }

  public function index() {
    $ruser = GetLoggedUser();
    $data['title'] = 'Kinerja Bidang / Bagian';
    $opr = !empty($_GET['opr'])?$_GET['opr']:'';
    $id = !empty($_GET['id'])?$_GET['id']:'';

    switch ($opr) {
      case 'detail-bidang':
        $data['subtitle'] = 'DETIL BIDANG / BAGIAN';
        $data['rbidang'] = $rbidang = $this->db
        ->where(COL_BIDID, $id)
        ->get(TBL_SAKIPV2_BID)
        ->row_array();
        if(empty($rbidang)) {
          show_error('PARAMETER TIDAK VALID!');
          exit();
        }
        $data['navs'] = array(
          array('text'=>'DAFTAR BIDANG / BAGIAN', 'link'=>site_url('sakipv2/bidang/index')),
          array('text'=>strtoupper($rbidang[COL_BIDNAMA]))
        );
        $this->template->load('main', 'sakipv2/bidang/view-bidang', $data);
      break;

      default:
        $data['subtitle'] = 'DAFTAR BIDANG / BAGIAN';
        $this->template->load('main', 'sakipv2/bidang/index-bidang', $data);
      break;
    }
  }

  public function ajax_form_bidang($mode, $id) {
    $data['mode'] = $mode;
    $ruser = GetLoggedUser();
    if($ruser[COL_ROLEID]!=ROLEADMIN && $ruser[COL_ROLEID]!=ROLEKADIS) {
      $this->showErrUnathorized();
    }

    if(!empty($_POST)) {
      if($mode=='add') {
        if(empty($id)) {
          ShowJsonError('PARAMETER TIDAK VALID!');
          exit();
        }

        $this->db->trans_begin();
        try {
          $rec = array(
            COL_IDRENSTRA=>$id,
            COL_BIDNAMA=>$this->input->post(COL_BIDNAMA),
            COL_BIDNAMAPIMPINAN=>$this->input->post(COL_BIDNAMAPIMPINAN),
            COL_BIDNAMAJABATAN=>$this->input->post(COL_BIDNAMAJABATAN),

            COL_CREATEDBY=>$ruser[COL_USERNAME],
            COL_CREATEDON=>date('Y-m-d H:i:s')
          );

          $res = $this->db->insert(TBL_SAKIPV2_BID, $rec);
          if(!$res) {
            $err = $this->db->error();
            throw new Exception($err['message']);
          }

          $this->db->trans_commit();
          ShowJsonSuccess('INPUT DATA BERHASIL');
          exit();
        } catch(Exception $ex) {
          $this->db->trans_rollback();
          ShowJsonError($ex->getMessage());
          exit();
        }
      } else if($mode=='edit') {
        $this->db->trans_begin();
        try {
          if(empty($id)) {
            throw new Exception('PARAMETER TIDAK VALID');
          }

          $rec = array(
            COL_BIDNAMA=>$this->input->post(COL_BIDNAMA),
            COL_BIDNAMAPIMPINAN=>$this->input->post(COL_BIDNAMAPIMPINAN),
            COL_BIDNAMAJABATAN=>$this->input->post(COL_BIDNAMAJABATAN),

            COL_UPDATEDBY=>$ruser[COL_USERNAME],
            COL_UPDATEDON=>date('Y-m-d H:i:s')
          );

          if(!empty($this->input->post(COL_BIDTUGASPOKOK))) {
            $rec[COL_BIDTUGASPOKOK] = $this->input->post(COL_BIDTUGASPOKOK);
          }

          $arrFungsi = array();
          $arrIKU = array();
          $fungsiUraian = $this->input->post('FungsiUraian');
          $IKUUraian = $this->input->post('IKUUraian');
          $IKUSumberData = $this->input->post('IKUSumberData');
          $IKUFormulasi = $this->input->post('IKUFormulasi');
          $IKUSatuan = $this->input->post('IKUSatuan');
          $IKUTarget = $this->input->post('IKUTarget');

          for($i = 0; $i<count($fungsiUraian); $i++) {
            $arrFungsi[] = $fungsiUraian[$i];
          }
          for($i = 0; $i<count($IKUUraian); $i++) {
            $arrIKU[] = array(
              'Uraian'=>$IKUUraian[$i],
              'SumberData'=>$IKUSumberData[$i],
              'Formulasi'=>$IKUFormulasi[$i],
              'Satuan'=>$IKUSatuan[$i],
              'Target'=>$IKUTarget[$i]
            );
          }

          if(!empty($arrFungsi)) {
            $rec[COL_BIDFUNGSI]=json_encode($arrFungsi);
          }
          if(!empty($arrIKU)) {
            $rec[COL_BIDIKU]=json_encode($arrIKU);
          }

          $res = $this->db->where(COL_BIDID, $id)->update(TBL_SAKIPV2_BID, $rec);
          if(!$res) {
            $err = $this->db->error();
            throw new Exception($err['message']);
          }

          $this->db->trans_commit();
          ShowJsonSuccess('INPUT DATA BERHASIL');
          exit();
        } catch(Exception $ex) {
          $this->db->trans_rollback();
          ShowJsonError($ex->getMessage());
          exit();
        }
      }
    } else {
      if($mode=='edit') {
        $rdata = $this->db->where(COL_BIDID, $id)->get(TBL_SAKIPV2_BID)->row_array();
        if(empty($rdata)) {
          echo '<p class="text-center text-danger font-weight-bold">PARAMETER TIDAK VALID!</p>';
          exit();
        }
        $data['data'] = $rdata;
      }
      $this->load->view('sakipv2/bidang/form-bidang', $data);
    }
  }

  public function ajax_change_bidang($mode, $id)  {
    $ruser = GetLoggedUser();
    if($ruser[COL_ROLEID]!=ROLEADMIN && $ruser[COL_ROLEID]!=ROLEKADIS) {
      $this->showErrUnathorized();
    }

    if($mode=='delete') {
      $res = $this->db->where(COL_BIDID, $id)->delete(TBL_SAKIPV2_BID);
      if(!$res) {
        $err = $this->db->error();
        ShowJsonError($err['message']);
        exit();
      }
    } else  if($mode=='activate') {
      $this->db->trans_begin();
      $res = $this->db->where(COL_BIDID, $id)->update(TBL_SAKIPV2_BID, array(COL_BIDISAKTIF=>1));
      if(!$res) {
        $this->db->trans_rollback();
        $err = $this->db->error();
        ShowJsonError($err['message']);
        exit();
      }
      $this->db->trans_commit();
    } else  if($mode=='suspend') {
      $this->db->trans_begin();
      $res = $this->db->where(COL_BIDID, $id)->update(TBL_SAKIPV2_BID, array(COL_BIDISAKTIF=>0));
      if(!$res) {
        $this->db->trans_rollback();
        $err = $this->db->error();
        ShowJsonError($err['message']);
        exit();
      }
      $this->db->trans_commit();
    } else {
      ShowJsonError('PARAMETER TIDAK VALID');
      exit();
    }

    ShowJsonSuccess('BERHASIL MENGUBAH DATA');
    exit();
  }

  public function ajax_form_iku($mode, $id, $idx=null)  {
    $ruser = GetLoggedUser();
    if($ruser[COL_ROLEID]!=ROLEADMIN && $ruser[COL_ROLEID]!=ROLEKADIS) {
      $this->showErrUnathorized();
    }

    $uraianIKU = $this->input->post('Uraian');

    if($mode == 'add-tupoksi' || $mode == 'add-iku') {
      if(empty($uraianIKU)) {
        ShowJsonError('PARAMETER TIDAK VALID!');
        exit();
      }

      $rbidang = $this->db
      ->where(COL_BIDID, $id)
      ->get(TBL_SAKIPV2_BID)
      ->row_array();
      if(empty($rbidang)) {
        ShowJsonError('PARAMETER TIDAK VALID!');
        exit();
      }

      $arrCurrJSON = array();
      if($mode == 'add-tupoksi') {
        if(!empty($rbidang[COL_BIDTUPOKSI])) {
          $arrCurrJSON = json_decode($rbidang[COL_BIDTUPOKSI]);
        }
      } else if($mode == 'add-iku') {
        if(!empty($rbidang[COL_BIDIKU])) {
          $arrCurrJSON = json_decode($rbidang[COL_BIDIKU]);
        }
      }

      $arrCurrJSON[] = $uraianIKU;

      $rec = array();
      if($mode == 'add-tupoksi') {
        $rec = array(COL_BIDTUPOKSI=>json_encode($arrCurrJSON));
      } else if($mode == 'add-iku') {
        $rec = array(COL_BIDIKU=>json_encode($arrCurrJSON));
      }

      $res = $this->db->where(COL_BIDID, $id)->update(TBL_SAKIPV2_BID, $rec);
      if(!$res) {
        $err = $this->db->error();
        ShowJsonError($err['message']);
        exit();
      }

      ShowJsonSuccess('INPUT DATA BERHASIL');
      exit();

    } else if($mode == 'edit-tupoksi' || $mode == 'edit-iku') {
      if(empty($uraianIKU)) {
        ShowJsonError('PARAMETER TIDAK VALID!');
        exit();
      }
      if($idx==null) {
        ShowJsonError('PARAMETER TIDAK VALID!');
        exit();
      }

      $rbidang = $this->db
      ->where(COL_BIDID, $id)
      ->get(TBL_SAKIPV2_BID)
      ->row_array();
      if(empty($rbidang)) {
        ShowJsonError('PARAMETER TIDAK VALID!');
        exit();
      }

      $arrCurrJSON = array();
      if($mode == 'edit-tupoksi') {
        if(!empty($rbidang[COL_BIDTUPOKSI])) {
          $arrCurrJSON = json_decode($rbidang[COL_BIDTUPOKSI]);
        }
      } else if($mode == 'edit-iku') {
        if(!empty($rbidang[COL_BIDIKU])) {
          $arrCurrJSON = json_decode($rbidang[COL_BIDIKU]);
        }
      }

      if(isset($arrCurrJSON[$idx])) {
        $arrCurrJSON[$idx] = $uraianIKU;
      }

      $rec = array();
      if($mode == 'edit-tupoksi') {
        $rec = array(COL_BIDTUPOKSI=>json_encode($arrCurrJSON));
      } else if($mode == 'edit-iku') {
        $rec = array(COL_BIDIKU=>json_encode($arrCurrJSON));
      }

      $res = $this->db->where(COL_BIDID, $id)->update(TBL_SAKIPV2_BID, $rec);
      if(!$res) {
        $err = $this->db->error();
        ShowJsonError($err['message']);
        exit();
      }

      ShowJsonSuccess('INPUT DATA BERHASIL');
      exit();

    } else if($mode == 'delete-tupoksi' || $mode == 'delete-iku') {
      if($idx==null) {
        ShowJsonError('PARAMETER TIDAK VALID!');
        exit();
      }

      $rbidang = $this->db
      ->where(COL_BIDID, $id)
      ->get(TBL_SAKIPV2_BID)
      ->row_array();
      if(empty($rbidang)) {
        ShowJsonError('PARAMETER TIDAK VALID!');
        exit();
      }

      $arrCurrJSON = array();
      if($mode == 'delete-tupoksi') {
        if(!empty($rbidang[COL_BIDTUPOKSI])) {
          $arrCurrJSON = json_decode($rbidang[COL_BIDTUPOKSI]);
        }
      } else if($mode == 'delete-iku') {
        if(!empty($rbidang[COL_BIDIKU])) {
          $arrCurrJSON = json_decode($rbidang[COL_BIDIKU]);
        }
      }

      if(isset($arrCurrJSON[$idx])) {
        array_splice($arrCurrJSON,$idx,1);
      }

      $rec = array();
      if($mode == 'delete-tupoksi') {
        $rec = array(COL_BIDTUPOKSI=>json_encode($arrCurrJSON));
      } else if($mode == 'delete-iku') {
        $rec = array(COL_BIDIKU=>json_encode($arrCurrJSON));
      }

      $res = $this->db->where(COL_BIDID, $id)->update(TBL_SAKIPV2_BID, $rec);
      if(!$res) {
        $err = $this->db->error();
        ShowJsonError($err['message']);
        exit();
      }

      ShowJsonSuccess('PERUBAHAN DATA BERHASIL');
      exit();
    }
  }

  public function ajax_form_program($mode, $id, $idDPA=null, $idSasaran=null) {
    $ruser = GetLoggedUser();
    if($ruser[COL_ROLEID]!=ROLEADMIN && $ruser[COL_ROLEID]!=ROLEKADIS) {
      $this->showErrUnathorized();
    }

    $data['mode'] = $mode;
    if(!empty($_POST)) {
      if($mode=='add') {
        if(empty($id) || empty($idDPA)) {
          ShowJsonError('PARAMETER TIDAK VALID!');
          exit();
        }

        $this->db->trans_begin();
        try {
          $rec = array(
            COL_IDBID=>$this->input->post(COL_IDBID),
            COL_IDDPA=>$this->input->post(COL_IDDPA),
            COL_IDSASARANSKPD=>$this->input->post(COL_IDSASARANSKPD),
            COL_PROGRAMKODE=>$this->input->post(COL_PROGRAMKODE),
            COL_PROGRAMURAIAN=>$this->input->post(COL_PROGRAMURAIAN),

            COL_CREATEDBY=>$ruser[COL_USERNAME],
            COL_CREATEDON=>date('Y-m-d H:i:s')
          );

          $res = $this->db->insert(TBL_SAKIPV2_BID_PROGRAM, $rec);
          if(!$res) {
            $err = $this->db->error();
            throw new Exception($err['message']);
          }

          $this->db->trans_commit();
          ShowJsonSuccess('INPUT DATA BERHASIL');
          exit();
        } catch(Exception $ex) {
          $this->db->trans_rollback();
          ShowJsonError($ex->getMessage());
          exit();
        }
      } else if($mode=='edit') {
        $this->db->trans_begin();
        try {
          if(empty($id)) {
            throw new Exception('PARAMETER TIDAK VALID');
          }

          $rec = array(
            COL_IDBID=>$this->input->post(COL_IDBID),
            COL_IDDPA=>$this->input->post(COL_IDDPA),
            COL_IDSASARANSKPD=>$this->input->post(COL_IDSASARANSKPD),
            COL_PROGRAMKODE=>$this->input->post(COL_PROGRAMKODE),
            COL_PROGRAMURAIAN=>$this->input->post(COL_PROGRAMURAIAN),

            COL_UPDATEDBY=>$ruser[COL_USERNAME],
            COL_UPDATEDON=>date('Y-m-d H:i:s')
          );

          $res = $this->db->where(COL_PROGRAMID, $id)->update(TBL_SAKIPV2_BID_PROGRAM, $rec);
          if(!$res) {
            $err = $this->db->error();
            throw new Exception($err['message']);
          }

          $this->db->trans_commit();
          ShowJsonSuccess('INPUT DATA BERHASIL');
          exit();
        } catch(Exception $ex) {
          $this->db->trans_rollback();
          ShowJsonError($ex->getMessage());
          exit();
        }
      } else if($mode=='add-kegiatan') {
        if(empty($id)) {
          ShowJsonError('PARAMETER TIDAK VALID!');
          exit();
        }

        $this->db->trans_begin();
        try {
          $rec = array(
            COL_IDPROGRAM=>$id,
            COL_KEGIATANKODE=>$this->input->post(COL_KEGIATANKODE),
            COL_KEGIATANURAIAN=>$this->input->post(COL_KEGIATANURAIAN),

            COL_CREATEDBY=>$ruser[COL_USERNAME],
            COL_CREATEDON=>date('Y-m-d H:i:s')
          );

          $res = $this->db->insert(TBL_SAKIPV2_BID_KEGIATAN, $rec);
          if(!$res) {
            $err = $this->db->error();
            throw new Exception($err['message']);
          }

          $this->db->trans_commit();
          ShowJsonSuccess('INPUT DATA BERHASIL');
          exit();
        } catch(Exception $ex) {
          $this->db->trans_rollback();
          ShowJsonError($ex->getMessage());
          exit();
        }
      }  else if($mode=='edit-kegiatan') {
        $this->db->trans_begin();
        try {
          if(empty($id)) {
            throw new Exception('PARAMETER TIDAK VALID');
          }

          $rec = array(
            COL_KEGIATANKODE=>$this->input->post(COL_KEGIATANKODE),
            COL_KEGIATANURAIAN=>$this->input->post(COL_KEGIATANURAIAN),

            COL_UPDATEDBY=>$ruser[COL_USERNAME],
            COL_UPDATEDON=>date('Y-m-d H:i:s')
          );

          $res = $this->db->where(COL_KEGIATANID, $id)->update(TBL_SAKIPV2_BID_KEGIATAN, $rec);
          if(!$res) {
            $err = $this->db->error();
            throw new Exception($err['message']);
          }

          $this->db->trans_commit();
          ShowJsonSuccess('INPUT DATA BERHASIL');
          exit();
        } catch(Exception $ex) {
          $this->db->trans_rollback();
          ShowJsonError($ex->getMessage());
          exit();
        }
      }
    } else {
      if($mode=='edit') {
        $rdata = $this->db->where(COL_PROGRAMID, $id)->get(TBL_SAKIPV2_BID_PROGRAM)->row_array();
        if(empty($rdata)) {
          echo '<p class="text-center text-danger font-weight-bold">PARAMETER TIDAK VALID!</p>';
          exit();
        }
        $data['data'] = $rdata;
      } else if($mode=='add') {
        $data['def'] = array(
          COL_IDBID=>$id,
          COL_IDDPA=>$idDPA,
          COL_IDSASARANSKPD=>$idSasaran
        );
      } else if($mode=='edit-kegiatan') {
        $rdata = $this->db->where(COL_KEGIATANID, $id)->get(TBL_SAKIPV2_BID_KEGIATAN)->row_array();
        if(empty($rdata)) {
          echo '<p class="text-center text-danger font-weight-bold">PARAMETER TIDAK VALID!</p>';
          exit();
        }
        $data['data'] = $rdata;
      }

      if($mode=='add' || $mode=='edit') {
        $this->load->view('sakipv2/bidang/form-program', $data);
      } else if($mode=='add-kegiatan' || $mode=='edit-kegiatan') {
        $this->load->view('sakipv2/bidang/form-kegiatan', $data);
      }

    }
  }

  public function ajax_change_program($mode, $id)  {
    $ruser = GetLoggedUser();
    if($ruser[COL_ROLEID]!=ROLEADMIN && $ruser[COL_ROLEID]!=ROLEKADIS) {
      $this->showErrUnathorized();
    }

    if($mode=='delete') {
      $res = $this->db->where(COL_PROGRAMID, $id)->delete(TBL_SAKIPV2_BID_PROGRAM);
      if(!$res) {
        $err = $this->db->error();
        ShowJsonError($err['message']);
        exit();
      }
    } else if($mode=='delete-kegiatan') {
      $res = $this->db->where(COL_KEGIATANID, $id)->delete(TBL_SAKIPV2_BID_KEGIATAN);
      if(!$res) {
        $err = $this->db->error();
        ShowJsonError($err['message']);
        exit();
      }
    } else {
      ShowJsonError('PARAMETER TIDAK VALID');
      exit();
    }

    ShowJsonSuccess('BERHASIL MENGUBAH DATA');
    exit();
  }

  public function ajax_form_sasaran($mode, $id) {
    $ruser = GetLoggedUser();
    if($ruser[COL_ROLEID]!=ROLEADMIN && $ruser[COL_ROLEID]!=ROLEKADIS) {
      $this->showErrUnathorized();
    }

    $data['mode'] = $mode;
    if(!empty($_POST)) {
      $ruser = GetLoggedUser();
      if($mode=='sasaran-prog') {
        if(empty($id)) {
          ShowJsonError('PARAMETER TIDAK VALID!');
          exit();
        }

        $this->db->trans_begin();
        try {
          $arrSasaranNo = $this->input->post(COL_SASARANNO);
          $arrSasaranUraian = $this->input->post(COL_SASARANURAIAN);
          $arrSasaranIndikator = $this->input->post(COL_SASARANINDIKATOR);
          $arrSasaranSatuan = $this->input->post(COL_SASARANSATUAN);
          $arrSasaranTarget = $this->input->post(COL_SASARANTARGET);
          $arrSasaran = [];
          for($i = 0; $i<count($arrSasaranNo); $i++) {
            $arrSasaran[] = array(
              COL_IDPROGRAM => $id,
              COL_SASARANNO => $arrSasaranNo[$i],
              COL_SASARANURAIAN => $arrSasaranUraian[$i],
              COL_SASARANINDIKATOR => $arrSasaranIndikator[$i],
              COL_SASARANSATUAN => $arrSasaranSatuan[$i],
              COL_SASARANTARGET => $arrSasaranTarget[$i],

              COL_CREATEDBY=>$ruser[COL_USERNAME],
              COL_CREATEDON=>date('Y-m-d H:i:s')
            );
          }

          $res = $this->db->where(COL_IDPROGRAM, $id)->delete(TBL_SAKIPV2_BID_PROGSASARAN);
          if(!$res) {
            $err = $this->db->error();
            throw new Exception($err['message']);
          }
          if(!empty($arrSasaran)) {
            $res = $this->db->insert_batch(TBL_SAKIPV2_BID_PROGSASARAN, $arrSasaran);
            if(!$res) {
              $err = $this->db->error();
              throw new Exception($err['message']);
            }
          }

          $this->db->trans_commit();
          ShowJsonSuccess('INPUT DATA BERHASIL');
          exit();
        } catch(Exception $ex) {
          $this->db->trans_rollback();
          ShowJsonError($ex->getMessage());
          exit();
        }
      } else if($mode=='sasaran-keg') {
        if(empty($id)) {
          ShowJsonError('PARAMETER TIDAK VALID!');
          exit();
        }

        $this->db->trans_begin();
        try {
          $arrSasaranNo = $this->input->post(COL_SASARANNO);
          $arrSasaranUraian = $this->input->post(COL_SASARANURAIAN);
          $arrSasaranIndikator = $this->input->post(COL_SASARANINDIKATOR);
          $arrSasaranSatuan = $this->input->post(COL_SASARANSATUAN);
          $arrSasaranTarget = $this->input->post(COL_SASARANTARGET);
          $arrSasaran = [];
          for($i = 0; $i<count($arrSasaranNo); $i++) {
            $arrSasaran[] = array(
              COL_IDKEGIATAN => $id,
              COL_SASARANNO => $arrSasaranNo[$i],
              COL_SASARANURAIAN => $arrSasaranUraian[$i],
              COL_SASARANINDIKATOR => $arrSasaranIndikator[$i],
              COL_SASARANSATUAN => $arrSasaranSatuan[$i],
              COL_SASARANTARGET => $arrSasaranTarget[$i],

              COL_CREATEDBY=>$ruser[COL_USERNAME],
              COL_CREATEDON=>date('Y-m-d H:i:s')
            );
          }

          $res = $this->db->where(COL_IDKEGIATAN, $id)->delete(TBL_SAKIPV2_BID_KEGSASARAN);
          if(!$res) {
            $err = $this->db->error();
            throw new Exception($err['message']);
          }
          if(!empty($arrSasaran)) {
            $res = $this->db->insert_batch(TBL_SAKIPV2_BID_KEGSASARAN, $arrSasaran);
            if(!$res) {
              $err = $this->db->error();
              throw new Exception($err['message']);
            }
          }

          $this->db->trans_commit();
          ShowJsonSuccess('INPUT DATA BERHASIL');
          exit();
        } catch(Exception $ex) {
          $this->db->trans_rollback();
          ShowJsonError($ex->getMessage());
          exit();
        }
      }
    } else {
      if($mode=='sasaran-prog' || $mode=='sasaran-keg') {
        if($mode=='sasaran-prog') {
          $data['rsasaran'] = $this->db->where(COL_IDPROGRAM, $id)->get(TBL_SAKIPV2_BID_PROGSASARAN)->result_array();
        } else if($mode=='sasaran-keg') {
          $data['rsasaran'] = $this->db->where(COL_IDKEGIATAN, $id)->get(TBL_SAKIPV2_BID_KEGSASARAN)->result_array();
        }
        $this->load->view('sakipv2/bidang/form-sasaran', $data);
      }
    }
  }

  public function ajax_form_subkegiatan($mode, $id, $idDPA=null) {
    $ruser = GetLoggedUser();
    if($ruser[COL_ROLEID]!=ROLEADMIN && $ruser[COL_ROLEID]!=ROLEKADIS) {
      $this->showErrUnathorized();
    }

    $data['mode'] = $mode;
    if(!empty($_POST)) {
      if($mode=='add') {
        if(empty($id) || empty($idDPA)) {
          ShowJsonError('PARAMETER TIDAK VALID!');
          exit();
        }

        $this->db->trans_begin();
        try {
          $rec = array(
            COL_IDSUBBID=>!empty($this->input->post(COL_IDSUBBID))?$this->input->post(COL_IDSUBBID):null,
            COL_IDKEGIATAN=>$this->input->post(COL_IDKEGIATAN),
            COL_SUBKEGKODE=>$this->input->post(COL_SUBKEGKODE),
            COL_SUBKEGURAIAN=>$this->input->post(COL_SUBKEGURAIAN),
            COL_SUBKEGPAGU=>toNum($this->input->post(COL_SUBKEGPAGU)),

            COL_CREATEDBY=>$ruser[COL_USERNAME],
            COL_CREATEDON=>date('Y-m-d H:i:s')
          );

          $res = $this->db->insert(TBL_SAKIPV2_SUBBID_SUBKEGIATAN, $rec);
          if(!$res) {
            $err = $this->db->error();
            throw new Exception($err['message']);
          }

          $this->db->trans_commit();
          ShowJsonSuccess('INPUT DATA BERHASIL');
          exit();
        } catch(Exception $ex) {
          $this->db->trans_rollback();
          ShowJsonError($ex->getMessage());
          exit();
        }
      } else if($mode=='edit') {
        $this->db->trans_begin();
        try {
          if(empty($id)) {
            throw new Exception('PARAMETER TIDAK VALID');
          }

          $rec = array(
            COL_IDSUBBID=>!empty($this->input->post(COL_IDSUBBID))?$this->input->post(COL_IDSUBBID):null,
            COL_IDKEGIATAN=>$this->input->post(COL_IDKEGIATAN),
            COL_SUBKEGKODE=>$this->input->post(COL_SUBKEGKODE),
            COL_SUBKEGURAIAN=>$this->input->post(COL_SUBKEGURAIAN),
            COL_SUBKEGPAGU=>toNum($this->input->post(COL_SUBKEGPAGU)),

            COL_UPDATEDBY=>$ruser[COL_USERNAME],
            COL_UPDATEDON=>date('Y-m-d H:i:s')
          );

          $res = $this->db->where(COL_SUBKEGID, $id)->update(TBL_SAKIPV2_SUBBID_SUBKEGIATAN, $rec);
          if(!$res) {
            $err = $this->db->error();
            throw new Exception($err['message']);
          }

          $this->db->trans_commit();
          ShowJsonSuccess('INPUT DATA BERHASIL');
          exit();
        } catch(Exception $ex) {
          $this->db->trans_rollback();
          ShowJsonError($ex->getMessage());
          exit();
        }
      }
    } else {
      if($mode=='edit') {
        $rdata = $this->db
        ->select('*, COALESCE(sakipv2_bid_program.IdBid, sakipv2_subbid.IdBid) as IdBid')
        ->join(TBL_SAKIPV2_BID_KEGIATAN,TBL_SAKIPV2_BID_KEGIATAN.'.'.COL_KEGIATANID." = ".TBL_SAKIPV2_SUBBID_SUBKEGIATAN.".".COL_IDKEGIATAN,"left")
        ->join(TBL_SAKIPV2_BID_PROGRAM,TBL_SAKIPV2_BID_PROGRAM.'.'.COL_PROGRAMID." = ".TBL_SAKIPV2_BID_KEGIATAN.".".COL_IDPROGRAM,"left")
        ->join(TBL_SAKIPV2_SUBBID,TBL_SAKIPV2_SUBBID.'.'.COL_SUBBIDID." = ".TBL_SAKIPV2_SUBBID_SUBKEGIATAN.".".COL_IDSUBBID,"left")
        ->where(COL_SUBKEGID, $id)
        ->get(TBL_SAKIPV2_SUBBID_SUBKEGIATAN)
        ->row_array();
        if(empty($rdata)) {
          echo '<p class="text-center text-danger font-weight-bold">PARAMETER TIDAK VALID!</p>';
          exit();
        }
        //echo $this->db->last_query();
        //exit();
        $data['data'] = $rdata;
      } else if($mode=='add') {
        $data['def'] = array(
          COL_IDKEGIATAN=>$id,
          COL_IDDPA=>$idDPA
        );

        $rkegiatan = $this->db
        ->join(TBL_SAKIPV2_BID_PROGRAM,TBL_SAKIPV2_BID_PROGRAM.'.'.COL_PROGRAMID." = ".TBL_SAKIPV2_BID_KEGIATAN.".".COL_IDPROGRAM,"left")
        ->where(COL_KEGIATANID, $id)
        ->get(TBL_SAKIPV2_BID_KEGIATAN)
        ->row_array();
        if(!empty($rkegiatan)) {
          $data['def'][COL_IDBID] = $rkegiatan[COL_IDBID];
        }
      }

      if($mode=='add' || $mode=='edit') {
        $this->load->view('sakipv2/subbidang/form-subkegiatan', $data);
      }
    }
  }
}
?>
