<?php
class Data extends MY_Controller {
  function __construct() {
      parent::__construct();
      if(!IsLogin()) {
        redirect(site_url('lke/user/login'));
      }
  }

  public function index($tipe='unit') {
    $ruser = GetLoggedUser();
    $data['title'] = 'LKE '.strtoupper($tipe);
    $data['tipe'] = $tipe;
    $data['res'] = $this->db
    ->where(COL_FORMCATEGORY, 'AKIP')
    ->order_by(COL_LKETAHUN,'desc')
    ->order_by(COL_CREATEDON,'desc')
    ->get(TBL_LKE_DATA)
    ->result_array();
    $this->template->load('_layouts/main', 'lke/data/index', $data, FALSE, TRUE);
  }

  public function index_load($tipe='unit') {
    $start = $_POST['start'];
    $rowperpage = $_POST['length'];
    $tahun = !empty($_POST['Tahun'])?$_POST['Tahun']:date('Y');
    $stat = !empty($_POST['Status'])?$_POST['Status']:null;
    /*$IdSupplier = !empty($_POST['idSupplier'])?$_POST['idSupplier']:null;
    $dateFrom = !empty($_POST['dateFrom'])?$_POST['dateFrom']:date('Y-m-01');
    $dateTo = !empty($_POST['dateTo'])?$_POST['dateTo']:date('Y-m-d');*/

    $ruser = GetLoggedUser();
    $orderdef = array(COL_LKETAHUN=>'desc');
    $orderables = array(null,COL_LKETAHUN,COL_FORMCATEGORY,COL_LKENAMA,COL_LKESTATUS,null,COL_CREATEDON);
    $cols = array(COL_FORMCATEGORY,COL_LKENAMA,COL_LKESTATUS);

    if($tipe=='unit') {
      $orderables = array(null,COL_LKETAHUN,COL_FORMCATEGORY,COL_SKPDNAMA,COL_LKENAMA,COL_LKESTATUS,COL_CREATEDON);
      $cols = array(COL_FORMCATEGORY,COL_SKPDNAMA,COL_LKENAMA,COL_LKESTATUS);

      $this->db->join(TBL_SAKIPV2_SKPD.' opd','opd.'.COL_SKPDID." = ".TBL_LKE_DATA.".".COL_SKPDID,"left");

      if($ruser[COL_ROLEID]!=ROLEADMIN&&$ruser[COL_ROLEID]!=ROLEAPIP&&$ruser[COL_ROLEID]!=ROLEGUEST) {
        $this->db->where(TBL_LKE_DATA.'.'.COL_SKPDID, $ruser[COL_SKPDID]);
      }
    }


    $queryAll = $this->db
    ->where(COL_LKETYPE, $tipe)
    ->join(TBL__USERINFORMATION.' uc','uc.'.COL_USERNAME." = ".TBL_LKE_DATA.".".COL_CREATEDBY,"left")
    ->join(TBL_LKE_FORM.' form','form.'.COL_UNIQ." = ".TBL_LKE_DATA.".".COL_FORMID,"left")
    ->get(TBL_LKE_DATA);

    $i = 0;
    foreach($cols as $item){
      if($item == COL_CREATEDBY) $item = TBL_LKE_DATA.'.'.COL_CREATEDBY;
      if($item == COL_SKPDNAMA) $item = 'opd.'.COL_SKPDNAMA;
      if($item == COL_FORMCATEGORY) $item = TBL_LKE_DATA.'.'.COL_FORMCATEGORY;
      if(!empty($_POST['search']['value'])){
        if($i===0) {
          $this->db->group_start();
          $this->db->like($item, $_POST['search']['value']);
        } else {
          $this->db->or_like($item, $_POST['search']['value']);
        }
        if(count($cols) - 1 == $i){
          $this->db->group_end();
        }
      }
      $i++;
    }

    if(!empty($tahun)) {
      $this->db->where(TBL_LKE_DATA.'.'.COL_LKETAHUN, $tahun);
    }
    if(!empty($stat)) {
      $this->db->where(TBL_LKE_DATA.'.'.COL_LKESTATUS, $stat);
    }

    if(!empty($_POST['order'])){
      $this->db->order_by($orderables[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
    }else if(!empty($orderdef)){
        $order = $orderdef;
        $this->db->order_by(key($order), $order[key($order)]);
    }

    if($tipe=='unit') {
      $this->db->select('lke_data.*, opd.SkpdNama, uc.Name as Nm_CreatedBy');
      $this->db->join(TBL_SAKIPV2_SKPD.' opd','opd.'.COL_SKPDID." = ".TBL_LKE_DATA.".".COL_SKPDID,"left");

      if($ruser[COL_ROLEID]!=ROLEADMIN&&$ruser[COL_ROLEID]!=ROLEAPIP&&$ruser[COL_ROLEID]!=ROLEGUEST) {
        $this->db->where(TBL_LKE_DATA.'.'.COL_SKPDID, $ruser[COL_SKPDID]);
      }
    } else {
      $this->db->select('lke_data.*, uc.Name as Nm_CreatedBy');
    }

    $q = $this->db
    ->select('lke_data.*, uc.UserName, form.FormCategory, (select sum(det.LKEScoreVal) from lke_data_detail det where det.LKEId=lke_data.Uniq and FormParent is null) as TotSkor, (select sum(det.LKEWeight) from lke_data_detail det where det.LKEId=lke_data.Uniq and FormParent is null) as TotWeight')
    ->where(COL_LKETYPE, $tipe)
    ->join(TBL__USERINFORMATION.' uc','uc.'.COL_USERNAME." = ".TBL_LKE_DATA.".".COL_CREATEDBY,"left")
    ->join(TBL_LKE_FORM.' form','form.'.COL_UNIQ." = ".TBL_LKE_DATA.".".COL_FORMID,"left")
    ->order_by(TBL_LKE_DATA.".".COL_LKETAHUN, 'desc')
    ->order_by(TBL_LKE_DATA.".".COL_CREATEDON, 'desc')
    ->get_compiled_select(TBL_LKE_DATA, FALSE);
    $rec = $this->db->query($q." LIMIT $rowperpage OFFSET $start");
    $data = [];

    foreach($rec->result_array() as $r) {
      $isEditable=true;
      $badgeStat = 'badge-secondary';
      $prevStat = 'DRAFT';
      if($r[COL_LKESTATUS]==LKE_STATUS_VERIFIKASI) $badgeStat = 'badge-primary';
      if($r[COL_LKESTATUS]==LKE_STATUS_FINAL) {
        $badgeStat = 'badge-success';
        $prevStat = 'VERIFIKASI';
      }


      if($r[COL_LKESTATUS]!=LKE_STATUS_DRAFT && ($ruser[COL_ROLEID]!=ROLEADMIN&&$ruser[COL_ROLEID]!=ROLEAPIP)) $isEditable = false;
      if($r[COL_LKESTATUS]==LKE_STATUS_FINAL) $isEditable = false;
      if($tipe=='unit') {
        $link = '-';
        if($ruser[COL_ROLEID] != ROLEGUEST) {
          $link = @
          '<a href="'.site_url('lke/data/delete/'.$r[COL_UNIQ]).'" class="btn btn-xs btn-danger btn-action '.(!$isEditable?'disabled':'').'"><i class="fas fa-trash"></i></a>&nbsp;'.
          '<a href="'.site_url('lke/data/cetak/'.$r[COL_UNIQ]).'" class="btn btn-xs btn-success" target="_blank"><i class="fas fa-print"></i></a>&nbsp;'.
          /*'<a href="'.site_url('lke/data/form/'.$r[COL_UNIQ]).'" class="btn btn-xs btn-primary btn-edit"><i class="fas fa-arrow-circle-right"></i></a>&nbsp;'*/
          '<a href="'.site_url('lke/data/filing/'.$r[COL_UNIQ]).'" class="btn btn-xs btn-primary btn-edit"><i class="fas fa-link"></i></a>&nbsp;';

          if($ruser[COL_ROLEID] == ROLEADMIN || $ruser[COL_ROLEID] == ROLEAPIP) {
            $link .= '<a href="'.site_url('lke/data/evaluate/'.$r[COL_UNIQ]).'" class="btn btn-xs btn-info btn-edit"><i class="fas fa-edit"></i></a>&nbsp;';
            $link .= '<a href="'.site_url('lke/data/changestat/'.$r[COL_UNIQ]).'" class="btn btn-xs btn-warning btn-action '.($r[COL_LKESTATUS]==LKE_STATUS_DRAFT?'disabled':'').'" data-prompt="Apakah anda yakin ingin mengembalikan status LKE menjadi '.$prevStat.'?"><i class="fas fa-refresh"></i></a>';
          }

        } else {
          $link = @
          '<a href="'.site_url('lke/data/cetak/'.$r[COL_UNIQ]).'" class="btn btn-xs btn-success" target="_blank"><i class="fas fa-print"></i></a>&nbsp;'.
          '<a href="'.site_url('lke/data/form/'.$r[COL_UNIQ]).'" class="btn btn-xs btn-primary btn-search"><i class="fas fa-arrow-circle-right"></i></a>';
        }
        $data[] = array(
          $link,
          $r[COL_LKETAHUN],
          $r[COL_FORMCATEGORY],
          $r[COL_SKPDNAMA],
          $r[COL_LKENAMA],
          '<span class="badge '.$badgeStat.'">'.$r[COL_LKESTATUS].'</span>',
          (!empty($r['TotSkor'])?'<strong>'.number_format($r['TotSkor'],2).'</strong>':'<strong>--</strong>').' / '.(!empty($r['TotWeight'])?'<strong>'.number_format($r['TotWeight'],2).'</strong>':'<strong>--</strong>'),
          date('Y-m-d H:i', strtotime($r[COL_CREATEDON]))
        );
      } else {
        $link = '-';
        if($ruser[COL_ROLEID] != ROLEGUEST) {
          $link = @
          '<a href="'.site_url('lke/data/delete/'.$r[COL_UNIQ]).'" class="btn btn-xs btn-danger btn-action"><i class="fas fa-trash"></i></a>&nbsp;'.
          '<a href="'.site_url('lke/data/cetak/'.$r[COL_UNIQ]).'" class="btn btn-xs btn-success" target="_blank"><i class="fas fa-print"></i></a>&nbsp;'.
          '<a href="'.site_url('lke/data/form/'.$r[COL_UNIQ]).'" class="btn btn-xs btn-primary btn-edit"><i class="fas fa-arrow-circle-right"></i></a>';
        } else {
          $link = @
          '<a href="'.site_url('lke/data/cetak/'.$r[COL_UNIQ]).'" class="btn btn-xs btn-success" target="_blank"><i class="fas fa-print"></i></a>&nbsp;'.
          '<a href="'.site_url('lke/data/form/'.$r[COL_UNIQ]).'" class="btn btn-xs btn-primary btn-edit"><i class="fas fa-arrow-circle-right"></i></a>';
        }
        $data[] = array(
          $link,
          $r[COL_LKETAHUN],
          $r[COL_FORMCATEGORY],
          $r[COL_LKENAMA],
          '<span class="badge '.$badgeStat.'">'.$r[COL_LKESTATUS].'</span>',
          (!empty($r['TotSkor'])?'<strong>'.number_format($r['TotSkor'],2).'</strong>':'<strong>--</strong>').' / '.(!empty($r['TotWeight'])?'<strong>'.number_format($r['TotWeight'],2).'</strong>':'<strong>--</strong>'),
          date('Y-m-d H:i', strtotime($r[COL_CREATEDON]))
        );
      }

    }

    $result = array(
      "draw" => $_POST['draw'],
      "recordsFiltered" => $this->db->query($q)->num_rows(),
      "recordsTotal" => $queryAll->num_rows(),
      "data" => $data,
    );

    echo json_encode($result);
    exit();
  }

  public function add($tipe='unit') {
    $ruser = GetLoggedUser();
    $ropd = array();
    if($ruser[COL_ROLEID]!=ROLEADMIN && $ruser[COL_ROLEID]!=ROLEAPIP) {
      $ropd = $this->db
      ->where(COL_SKPDID, $ruser[COL_SKPDID])
      ->get(TBL_SAKIPV2_SKPD)
      ->row_array();
    }

    $data['ropd'] = $ropd;
    $data['tipe'] = $tipe;

    if(!empty($_POST)) {
      $this->db->where(array(
        COL_FORMID=>$this->input->post(COL_FORMID),
        COL_LKETYPE=>strtoupper($tipe),
        COL_LKETAHUN=>$this->input->post(COL_LKETAHUN)
      ));

      if($tipe=='unit') {
        $this->db->where(COL_SKPDID, $this->input->post(COL_SKPDID));
      }
      $rcheck = $this->db
      ->get(TBL_LKE_DATA)
      ->row_array();
      if(!empty($rcheck)) {
        ShowJsonError('LKE tersebut sudah terdaftar di sistem.');
        exit();
      }

      $rform = $this->db
      ->where(COL_UNIQ, $this->input->post(COL_FORMID))
      ->get(TBL_LKE_FORM)
      ->row_array();
      if(empty($rform)) {
        ShowJsonError('Parameter tidak valid!');
        exit();
      }

      $rdet = $this->db
      ->where(COL_FORMID, $this->input->post(COL_FORMID))
      ->get(TBL_LKE_FORM_DETAIL)
      ->result_array();
      if(empty($rdet)) {
        ShowJsonError('Parameter tidak valid!');
        exit();
      }

      $det = array();
      $dat = array(
        COL_FORMID=>$this->input->post(COL_FORMID),
        COL_FORMCATEGORY=>$rform[COL_FORMCATEGORY],
        COL_LKENAMA=>$rform[COL_FORMNAMA],
        COL_LKETYPE=>$tipe,
        COL_LKETAHUN=>$this->input->post(COL_LKETAHUN),
        COL_LKESTATUS=>LKE_STATUS_DRAFT,
        COL_SKPDID=>$this->input->post(COL_SKPDID),

        COL_CREATEDBY=>$ruser[COL_USERNAME],
        COL_CREATEDON=>date('Y-m-d H:i:s')
      );


      $this->db->trans_begin();
      try {
        $res = $this->db->insert(TBL_LKE_DATA, $dat);
        if(!$res) {
          $err = $this->db->error();
          throw new Exception($err['message']);
        }

        $lkeId = $this->db->insert_id();
        foreach($rdet as $d) {
          $det[] = array(
            COL_LKEID=>$lkeId,
            COL_FORMUNIQ=>$d[COL_UNIQ],
            COL_FORMPARENT=>$d[COL_FORMIDPARENT],
            COL_LKESEQ=>$d[COL_FORMSEQ],
            COL_LKEQUEST=>$d[COL_FORMQUEST],
            COL_LKEWEIGHT=>$d[COL_FORMWEIGHT],
            COL_LKECRITERIA=>$d[COL_FORMCRITERIA]
          );
        }

        $res = $this->db->insert_batch(TBL_LKE_DATA_DETAIL, $det);
        if(!$res) {
          $err = $this->db->error();
          throw new Exception($err['message']);
        }

        $this->db->trans_commit();
        ShowJsonSuccess('LKE berhasil ditambahkan.');

      } catch(Exception $ex) {
        $this->db->trans_rollback();
        ShowJsonError($ex->getMessage());
        exit();
      }

    } else {
      $this->load->view('lke/data/add', $data);
    }

  }

  public function form($id) {
    $ruser = GetLoggedUser();
    $data['title'] = 'Form Isian LKE';
    $rdata = $this->db
    ->join(TBL_SAKIPV2_SKPD.' opd','opd.'.COL_SKPDID." = ".TBL_LKE_DATA.".".COL_SKPDID,"left")
    ->where(COL_UNIQ, $id)
    ->get(TBL_LKE_DATA)
    ->row_array();

    $isEditable=true;
    if($rdata[COL_LKESTATUS]!=LKE_STATUS_DRAFT && ($ruser[COL_ROLEID]!=ROLEADMIN&&$ruser[COL_ROLEID]!=ROLEAPIP)) $isEditable = false;
    if($rdata[COL_LKESTATUS]==LKE_STATUS_FINAL) $isEditable = false;
    if($ruser[COL_ROLEID]==ROLEGUEST) $isEditable = false;

    if(!empty($_POST)) {
      if(!$isEditable) {
        ShowJsonError('Maaf, LKE ini tidak dapat diperbarui karena berstatus <strong>'.$rdata[COL_LKESTATUS].'</strong>.');
        exit();
      }

      $this->db->trans_begin();
      try {
        $mode = $this->input->post('mode');
        $uniq = $this->input->post('LKEUniq');
        $lkeScoreText = $this->input->post(COL_LKESCORETEXT);
        $lkeScoreVal = $this->input->post(COL_LKESCOREVAL);
        $lkeScoreRemarks = $this->input->post(COL_LKEREMARKS);
        $lkeScoreRemarks2 = $this->input->post(COL_LKEREMARKS2);

        for($i=0; $i<count($uniq); $i++) {
          if(!empty($lkeScoreText[$i])) {
            $det = array(
              COL_LKESCORETEXT=>$lkeScoreText[$i],
              COL_LKESCOREVAL=>$lkeScoreVal[$i],
              COL_LKEREMARKS=>$lkeScoreRemarks[$i],
              COL_LKEREMARKS2=>$lkeScoreRemarks2[$i]
            );

            $res = $this->db->where(COL_UNIQ, $uniq[$i])->update(TBL_LKE_DATA_DETAIL, $det);
            if(!$res) {
              $err = $this->db->error();
              throw new Exception($err['message']);
            }
          } else {
            $det = array(
              COL_LKEREMARKS=>$lkeScoreRemarks[$i],
              COL_LKEREMARKS2=>$lkeScoreRemarks2[$i]
            );

            $res = $this->db->where(COL_UNIQ, $uniq[$i])->update(TBL_LKE_DATA_DETAIL, $det);
            if(!$res) {
              $err = $this->db->error();
              throw new Exception($err['message']);
            }
          }
        }

        if($mode=='submit') {
          $stat = LKE_STATUS_VERIFIKASI;
          if($rdata[COL_LKESTATUS]==LKE_STATUS_VERIFIKASI) $stat = LKE_STATUS_FINAL;

          $res = $this->db
          ->where(COL_UNIQ, $id)
          ->update(TBL_LKE_DATA, array(
            COL_LKESTATUS=>$stat,
            COL_UPDATEDBY=>$ruser[COL_USERNAME],
            COL_UPDATEDON=>date('Y-m-d H:i:s'))
          );
          if(!$res) {
            $err = $this->db->error();
            throw new Exception($err['message']);
          }
        }

        // recalculate
        $rlke = $this->db
        ->select('*, (select sum(det.LKEScoreVal) from lke_data_detail det where det.LKEId=lke_data_detail.LKEId and det.FormParent=lke_data_detail.FormUniq) as TotSkor')
        ->where(COL_LKEID,$id)
        ->where(COL_FORMPARENT, null)
        ->get(TBL_LKE_DATA_DETAIL)
        ->result_array();
        foreach($rlke as $r) {
          $res = $this->db
          ->where(COL_UNIQ, $r[COL_UNIQ])
          ->update(TBL_LKE_DATA_DETAIL, array(
            COL_LKESCOREVAL=>$r['TotSkor']
          ));
          if(!$res) {
            $err = $this->db->error();
            throw new Exception($err['message']);
          }
        }

        $this->db->trans_commit();
        ShowJsonSuccess('Pembaruan data LKE berhasil.', array('redirect'=>site_url('lke/data/index/'.strtolower($rdata[COL_LKETYPE]))));
        exit();
      } catch(Exception $ex) {
        $this->db->trans_rollback();
        ShowJsonError($ex->getMessage());
        exit();
      }
    } else {
      if(empty($rdata)) {
        show_error('Parameter tidak valid!');
        exit();
      }
      $data['title'] = 'LKE '.$rdata[COL_LKETYPE].' TA. '.$rdata[COL_LKETAHUN].' <small class="text-sm font-weight-bold" style="font-size: 14pt">'.(!empty($rdata[COL_SKPDNAMA])?' '.$rdata[COL_SKPDNAMA]:'').'</small>';
      $data['data'] = $rdata;
      $data['id'] = $id;
      $data['isEditable'] = $isEditable;
      $this->template->load('_layouts/main', 'lke/data/form', $data, FALSE, TRUE);
    }
  }

  public function cetak($id) {
    $rdata = $this->db
    ->join(TBL_SAKIPV2_SKPD.' opd','opd.'.COL_SKPDID." = ".TBL_LKE_DATA.".".COL_SKPDID,"left")
    ->where(COL_UNIQ, $id)
    ->get(TBL_LKE_DATA)
    ->row_array();

    if(empty($rdata)) {
      show_error('Parameter tidak valid!');
      exit();
    }

    $rdet = $this->db
    ->select('*, (select sum(det.LKEWeight) from lke_data_detail det where det.LKEId = lke_data_detail.LKEId and det.FormParent = lke_data_detail.FormUniq) as SumWeight, (select sum(det.LKEScoreVal) from lke_data_detail det where det.LKEId = lke_data_detail.LKEId and det.FormParent = lke_data_detail.FormUniq) as SumScore')
    ->where(COL_LKEID, $rdata[COL_UNIQ])
    ->where(COL_FORMPARENT, null)
    ->order_by(COL_LKESEQ)
    ->get(TBL_LKE_DATA_DETAIL)
    ->result_array();

    $title = 'LKE - '.(!empty($rdata[COL_SKPDNAMA])?$rdata[COL_SKPDNAMA]:$rdata[COL_FORMCATEGORY].' TAHUN '.$rdata[COL_LKETAHUN]);

    $this->load->library('Mypdf');
    $mpdf = new Mypdf();

    $html = $this->load->view('data/cetak', array('data'=>$rdata, 'det'=>$rdet, 'title'=>$title), TRUE);
    $mpdf->pdf->SetTitle($title);
    $mpdf->pdf->SetFooter('Dicetak melalui aplikasi '.$this->setting_web_name);
    $mpdf->pdf->SetWatermarkImage(MY_IMAGEPATH.$this->setting_web_logo, 0.1, array(100,100));
    $mpdf->pdf->showWatermarkImage = true;
    //echo $html;
    //exit();
    $mpdf->pdf->WriteHTML($html);
    $mpdf->pdf->Output($title.'.pdf', 'I');
  }

  public function delete($id) {
    $ruser = GetLoggedUser();
    $rdata = $this->db->where(COL_UNIQ, $id)->get(TBL_LKE_DATA)->row_array();
    if(empty($rdata)) {
      ShowJsonError('Parameter tidak valid!');
      exit();
    }

    $isEditable=true;
    if($rdata[COL_LKESTATUS]!=LKE_STATUS_DRAFT && ($ruser[COL_ROLEID]!=ROLEADMIN&&$ruser[COL_ROLEID]!=ROLEAPIP)) $isEditable = false;
    if($rdata[COL_LKESTATUS]==LKE_STATUS_FINAL) $isEditable = false;

    if(!$isEditable) {
      ShowJsonError('Maaf, LKE ini tidak dapat diperbarui karena berstatus <strong>'.$rdata[COL_LKESTATUS].'</strong>.');
      exit();
    }

    $res = $this->db->where(COL_UNIQ, $id)->delete(TBL_LKE_DATA);
    if(!$res) {
      $err = $this->db->error();
      ShowJsonError($err['message']);
      exit();
    }

    ShowJsonSuccess('BERHASIL DIHAPUS');
  }

  public function changestat($id) {
    $ruser = GetLoggedUser();
    $rdata = $this->db->where(COL_UNIQ, $id)->get(TBL_LKE_DATA)->row_array();
    if(empty($rdata)) {
      ShowJsonError('Parameter tidak valid!');
      exit();
    }

    $prevStat = 'DRAFT';
    if($rdata[COL_LKESTATUS]==LKE_STATUS_FINAL) $prevStat = 'VERIFIKASI';

    $res = $this->db->where(COL_UNIQ, $id)->update(TBL_LKE_DATA, array(COL_LKESTATUS=>$prevStat, COL_UPDATEDBY=>$ruser[COL_USERNAME], COL_UPDATEDON=>date('Y-m-d H:i:s')));
    if(!$res) {
      $err = $this->db->error();
      ShowJsonError($err['message']);
      exit();
    }

    ShowJsonSuccess('PEMBARUAN STATUS BERHASIL.');
  }

  public function filing($id) {
    $ruser = GetLoggedUser();
    $data['title'] = 'Bukti Dukung';
    $rdata = $this->db
    ->join(TBL_SAKIPV2_SKPD.' opd','opd.'.COL_SKPDID." = ".TBL_LKE_DATA.".".COL_SKPDID,"left")
    ->where(COL_UNIQ, $id)
    ->get(TBL_LKE_DATA)
    ->row_array();

    $isEditable=true;
    if($rdata[COL_LKESTATUS]!=LKE_STATUS_DRAFT && ($ruser[COL_ROLEID]!=ROLEADMIN&&$ruser[COL_ROLEID]!=ROLEAPIP)) $isEditable = false;
    if($rdata[COL_LKESTATUS]==LKE_STATUS_FINAL) $isEditable = false;
    if($ruser[COL_ROLEID]==ROLEGUEST) $isEditable = false;

    if(!empty($_POST)) {
      if(!$isEditable) {
        ShowJsonError('Maaf, LKE ini tidak dapat diperbarui karena berstatus <strong>'.$rdata[COL_LKESTATUS].'</strong>.');
        exit();
      }

      $this->db->trans_begin();
      try {
        if($mode=='submit') {
          $stat = LKE_STATUS_VERIFIKASI;
          if($rdata[COL_LKESTATUS]==LKE_STATUS_VERIFIKASI) $stat = LKE_STATUS_FINAL;

          $res = $this->db
          ->where(COL_UNIQ, $id)
          ->update(TBL_LKE_DATA, array(
            COL_LKESTATUS=>$stat,
            COL_UPDATEDBY=>$ruser[COL_USERNAME],
            COL_UPDATEDON=>date('Y-m-d H:i:s'))
          );
          if(!$res) {
            $err = $this->db->error();
            throw new Exception($err['message']);
          }
        }

        // recalculate
        $rlke = $this->db
        ->select('*, (select sum(det.LKEScoreVal) from lke_data_detail det where det.LKEId=lke_data_detail.LKEId and det.FormParent=lke_data_detail.FormUniq) as TotSkor')
        ->where(COL_LKEID,$id)
        ->where(COL_FORMPARENT, null)
        ->get(TBL_LKE_DATA_DETAIL)
        ->result_array();
        foreach($rlke as $r) {
          $res = $this->db
          ->where(COL_UNIQ, $r[COL_UNIQ])
          ->update(TBL_LKE_DATA_DETAIL, array(
            COL_LKESCOREVAL=>$r['TotSkor']
          ));
          if(!$res) {
            $err = $this->db->error();
            throw new Exception($err['message']);
          }
        }

        $this->db->trans_commit();
        ShowJsonSuccess('Pengiriman LKE berhasil.', array('redirect'=>site_url('lke/data/index/'.strtolower($rdata[COL_LKETYPE]))));
        exit();
      } catch(Exception $ex) {
        $this->db->trans_rollback();
        ShowJsonError($ex->getMessage());
        exit();
      }
    } else {
      if(empty($rdata)) {
        show_error('Parameter tidak valid!');
        exit();
      }
      $data['data'] = $rdata;
      $data['id'] = $id;
      $data['isEditable'] = $isEditable;
      $this->template->load('_layouts/main', 'lke/data/form-upload', $data, FALSE, TRUE);
    }
  }

  public function filing_partial($id) {
    $rdet = $this->db
    ->where(COL_FORMPARENT, $id)
    ->get(TBL_LKE_DATA_DETAIL)
    ->result_array();

    $this->load->view('lke/data/form-uploaddet', array('rdetail'=>$rdet));
  }

  public function evidence($id) {
    $rdata = $this->db
    ->where(COL_UNIQ, $id)
    ->get(TBL_LKE_DATA_DETAIL)
    ->row_array();
    if(empty($rdata)) {
      ShowJsonError('Parameter tidak valid!');
      exit();
    }

    $res = $this->db->where(COL_UNIQ, $id)->update(TBL_LKE_DATA_DETAIL, array(COL_LKEREMARKS2=>$this->input->post(COL_LKEREMARKS2)));
    if(!$res) {
      $err = $this->db->error();
      ShowJsonError($err['message']);
      exit();
    }

    ShowJsonSuccess('Bukti dukung tersimpan.');
    exit();
  }

  public function evaluate($id) {
    $ruser = GetLoggedUser();
    $data['title'] = 'Penilaian';
    $rdata = $this->db
    ->join(TBL_SAKIPV2_SKPD.' opd','opd.'.COL_SKPDID." = ".TBL_LKE_DATA.".".COL_SKPDID,"left")
    ->where(COL_UNIQ, $id)
    ->get(TBL_LKE_DATA)
    ->row_array();

    if(!empty($_POST)) {
      $this->db->trans_begin();
      try {
        $this->db->trans_commit();
        ShowJsonSuccess('Pembaruan data LKE berhasil.', array('redirect'=>site_url('lke/data/index/'.strtolower($rdata[COL_LKETYPE]))));
        exit();
      } catch(Exception $ex) {
        $this->db->trans_rollback();
        ShowJsonError($ex->getMessage());
        exit();
      }
    } else {
      if(empty($rdata)) {
        show_error('Parameter tidak valid!');
        exit();
      }
      $data['data'] = $rdata;
      $data['id'] = $id;
      $this->template->load('_layouts/main', 'lke/data/form-eval', $data, FALSE, TRUE);
    }
  }

  public function evaluate_partial($id) {
    $rdet = $this->db
    ->where(COL_FORMPARENT, $id)
    ->get(TBL_LKE_DATA_DETAIL)
    ->result_array();

    $this->load->view('lke/data/form-evaldet', array('rdetail'=>$rdet));
  }

  public function scoring($id) {
    $rdata = $this->db
    ->where(COL_UNIQ, $id)
    ->get(TBL_LKE_DATA_DETAIL)
    ->row_array();
    if(empty($rdata)) {
      ShowJsonError('Parameter tidak valid!');
      exit();
    }

    $res = $this->db->where(COL_UNIQ, $id)->update(TBL_LKE_DATA_DETAIL, array(COL_LKESCOREVAL=>toNum($this->input->post(COL_LKESCOREVAL))));
    if(!$res) {
      $err = $this->db->error();
      ShowJsonError($err['message']);
      exit();
    }

    ShowJsonSuccess('Bukti dukung tersimpan.');
    exit();
  }

  public function kke($id) {
    $rdata = $this->db
    ->where(COL_UNIQ, $id)
    ->get(TBL_LKE_DATA_DETAIL)
    ->row_array();
    if(empty($rdata)) {
      ShowJsonError('Parameter tidak valid!');
      exit();
    }

    $this->load->view('lke/data/form-kke');
  }

  public function criterias() {
    $res = $this->db
    ->query("select ref.FormSeq as Seq1, lke_form_detail.FormSeq as Seq2, lke_form_detail.FormCriteria from lke_form_detail left join lke_form_detail ref on ref.Uniq = lke_form_detail.FormIdParent where lke_form_detail.FormId=1 and lke_form_detail.FormIdParent is not null")
    ->result_array();

    $html = "";
    $html .= "<table border=\"1\">";
    foreach($res as $r) {
      $arrCrit = explode(";",$r[COL_FORMCRITERIA]);
      if(!empty($arrCrit)) {
        $n=1;
        foreach($arrCrit as $c) {
          $html .= "<tr>";
          $html .= "<td>".$r['Seq1']."</td>";
          $html .= "<td>".$r['Seq2']."</td>";
          $html .= "<td>".$n."</td>";
          $html .= "<td>".$c."</td>";
          $html .= "</tr>";
          $n++;
        }
      }

    }
    $html .= "</table>";
    echo $html;
  }
}
?>
