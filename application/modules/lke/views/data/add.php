<form id="form-editor" method="post" action="#">
<div class="modal-header">
  <h5 class="modal-title">TAMBAH LKE BARU</h5>
  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
      <span aria-hidden="true"><i class="fa fa-close"></i></span>
  </button>
</div>
<div class="modal-body">
  <input type="hidden" value="AKIP" />
  <div class="form-group row">
    <label class="control-label col-sm-2">TAHUN</label>
    <div class="col-sm-4">
      <select class="form-control" name="<?=COL_LKETAHUN?>" style="width: 100%" required>
        <?php
        for($i=date('Y')-5; $i<=date('Y'); $i++) {
          ?>
          <option value="<?=$i?>" <?=$i==date('Y')?'selected':''?>><?=$i?></option>
          <?php
        }
        ?>
      </select>
    </div>
  </div>
  <div class="form-group row">
    <label class="control-label col-sm-2">FORMAT</label>
    <div class="col-sm-10">
      <select class="form-control" name="<?=COL_FORMID?>" style="width: 100%" required>
        <?=GetCombobox("SELECT * FROM lke_form where FormType='$tipe' and FormCategory='AKIP' and FormIsActive=1 ORDER BY FormNama", COL_UNIQ, COL_FORMNAMA)?>
      </select>
    </div>
  </div>
  <?php
  if($tipe=='unit') {
    ?>
    <div class="form-group row">
      <label class="control-label col-sm-2">SKPD</label>
      <div class="col-sm-10">
        <?php
        if(empty($ropd)) {
          ?>
          <select class="form-control" name="<?=COL_SKPDID?>" style="width: 100%" required>
            <?=GetCombobox("SELECT * FROM sakipv2_skpd where SkpdIsAktif = 1 ORDER BY SkpdNama", COL_SKPDID, COL_SKPDNAMA)?>
          </select>
          <?php
        } else {
          ?>
          <input type="hidden" name="<?=COL_SKPDID?>" value="<?=$ropd[COL_SKPDID]?>" />
          <input type="text" class="form-control" value="<?=$ropd[COL_SKPDNAMA]?>" readonly />
          <?php
        }
        ?>
      </div>
    </div>
    <?php
  }
  ?>
</div>
<div class="modal-footer">
  <button type="button" class="btn btn-outline-danger" data-dismiss="modal"><i class="far fa-times-circle"></i>&nbsp;BATAL</button>
  <button type="submit" class="btn btn-outline-primary btn-ok"><i class="far fa-arrow-circle-right"></i>&nbsp;LANJUT</button>
</div>
</form>
