<table class="table table-bordered">
  <tbody>
    <?php
    foreach($rdetail as $d_) {
      $crit = array();
      if(!empty($d_[COL_LKECRITERIA])) $crit = explode(";", $d_[COL_LKECRITERIA]);
      ?>
      <tr>
        <td class="font-italic text-right" style="width: 10px; white-space: nowrap; vertical-align: middle"><?=$d_[COL_LKESEQ]?></td>
        <td>
          <p class="font-italic mb-0 text-justify">
            <?=$d_[COL_LKEQUEST]?>
          </p>
        </td>
        <td class="text-center" style="width: 100px; white-space: nowrap; vertical-align: middle">
          <a href="<?=site_url('lke/data/evidence/'.$d_[COL_UNIQ])?>" data-val="<?=$d_[COL_LKEREMARKS2]?>" class="btn btn-<?=empty($d_[COL_LKEREMARKS2])?'outline-secondary':'primary'?> btn-xs btn-evidence"><i class="far fa-link"></i>&nbsp;EVIDENCE</a>
        </td>
      </tr>
      <?php
    }
    ?>
  </tbody>
</table>
