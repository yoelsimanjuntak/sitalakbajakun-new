<table class="table table-bordered table-sm">
  <thead>
    <tr>
      <th>No.</th>
      <th>Kriteria</th>
      <th>Nilai</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td style="width: 10px; white-space: nowrap" class="text-right">1.</td>
      <td>Perencanaan Kinerja Jangka Panjang (RPJPN atau dokumen yang dipersamakan)</td>
      <td style="width: 100px; white-space: nowrap"><input type="number" class="form-control form-control-sm text-right" placeholder="%" style="width: 100px !important;" /></td>
    </tr>
    <tr>
      <td style="width: 10px; white-space: nowrap" class="text-right">2.</td>
      <td>Perencanaan Kinerja Jangka Menengah (Renstra)</td>
      <td style="width: 100px; white-space: nowrap"><input type="number" class="form-control form-control-sm text-right" placeholder="%" style="width: 100px !important;" /></td>
    </tr>
    <tr>
      <td style="width: 10px; white-space: nowrap" class="text-right">3.</td>
      <td>Perencanaan Kinerja Jangka Pendek (Renja-RKT)</td>
      <td style="width: 100px; white-space: nowrap"><input type="number" class="form-control form-control-sm text-right" placeholder="%" style="width: 100px !important;" /></td>
    </tr>
    <tr>
      <td style="width: 10px; white-space: nowrap" class="text-right">4.</td>
      <td>Rencana Aksi</td>
      <td style="width: 100px; white-space: nowrap"><input type="number" class="form-control form-control-sm text-right" placeholder="%" style="width: 100px !important;" /></td>
    </tr>
    <tr>
      <td style="width: 10px; white-space: nowrap" class="text-right">5.</td>
      <td>Rencana Kerja dan Anggaran (RKA)</td>
      <td style="width: 100px; white-space: nowrap"><input type="number" class="form-control form-control-sm text-right" placeholder="%" style="width: 100px !important;" /></td>
    </tr>
    <tr>
      <td style="width: 10px; white-space: nowrap" class="text-right">6.</td>
      <td>Perjanjian Kinerja Pimpinan Organisasi</td>
      <td style="width: 100px; white-space: nowrap"><input type="number" class="form-control form-control-sm text-right" placeholder="%" style="width: 100px !important;" /></td>
    </tr>
    <tr>
      <td style="width: 10px; white-space: nowrap" class="text-right">7.</td>
      <td>Perjanjian Kinerja Manajemen (Administrator dan Pengawas)</td>
      <td style="width: 100px; white-space: nowrap"><input type="number" class="form-control form-control-sm text-right" placeholder="%" style="width: 100px !important;" /></td>
    </tr>
    <tr>
      <td style="width: 10px; white-space: nowrap" class="text-right">8.</td>
      <td>Perjanjian Kinerja Staf atau Sasaran Kinerja Pegawai (SKP)</td>
      <td style="width: 100px; white-space: nowrap"><input type="number" class="form-control form-control-sm text-right" placeholder="%" style="width: 100px !important;" /></td>
    </tr>
  </tbody>
</table>
