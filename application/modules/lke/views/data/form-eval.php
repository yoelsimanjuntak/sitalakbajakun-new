<?php
$rdetail = $this->db
->select('*, (select sum(det.LKEWeight) from lke_data_detail det where det.LKEId = lke_data_detail.LKEId and det.FormParent = lke_data_detail.FormUniq) as SumWeight')
->where(COL_LKEID, $data[COL_UNIQ])
->where(COL_FORMPARENT, null)
->order_by(COL_LKESEQ)
->get(TBL_LKE_DATA_DETAIL)
->result_array();
?>
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h3 class="m-0 text-dark font-weight-light"><?=$title?></h3>
      </div>
      <div class="col-sm-6 text-right">
        <a href="<?=site_url('lke/data/index/'.strtolower($data[COL_LKETYPE]))?>" class="btn btn-sm btn-secondary"><i class="far fa-arrow-circle-left"></i> KEMBALI</a>
      </div>
    </div>
  </div>
</div>
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-sm-12">
        <form id="form-lke" action="<?=current_url()?>" method="post">
          <input type="hidden" name="mode" value="save" />
          <?php
          foreach($rdetail as $d) {
            $rdet_ = $this->db
            ->where(COL_LKEID, $data[COL_UNIQ])
            ->where(COL_FORMPARENT, $d[COL_FORMUNIQ])
            ->get(TBL_LKE_DATA_DETAIL)
            ->result_array();
            ?>
            <div class="card collapsed-card" data-id="<?=$d[COL_FORMUNIQ]?>" data-url="<?=site_url('lke/data/evaluate-partial/'.$d[COL_FORMUNIQ])?>">
              <div class="card-header">
                <h5 class="card-title"><?=$d[COL_LKESEQ]?>. <strong><?=$d[COL_LKEQUEST]?></strong></h5>
                <div class="card-tools">
                  <button type="button" class="btn btn-tool text-primary" data-card-widget="collapse"><i class="fas fa-plus"></i>
                  </button>
                </div>
              </div>
              <div class="card-body p-0">

              </div>
              <div class="overlay d-none">
                <i class="fas fa-2x fa-sync-alt fa-spin"></i>
              </div>
            </div>
            <?php
          }
          ?>
        </form>
      </div>
    </div>
  </div>
</section>
<div class="modal fade" id="modal-kke" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Kertas Kerja Evaluasi</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true"><i class="fa fa-close"></i></span>
        </button>
      </div>
      <div class="modal-body">
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
function refreshCard(id) {
  var cardEl = $('.card[data-id='+id+']');
  var cardUrl = $('.card[data-id='+id+']').data('url');
  $('.overlay', cardEl).removeClass('d-none');
  $('.card-body', cardEl).load(cardUrl, function(){
    $('.overlay', cardEl).addClass('d-none');
    $('.btn-score', cardEl).click(function(){
      var href = $(this).attr('href');
      var val = $(this).data('val');
      swal({
        closeOnClickOutside: true,
        buttons: ['BATAL','SUBMIT'],
        text: "PENILAIAN",
        content: {
          element: "input",
          attributes: {
            placeholder: "SKOR PENILAIAN (%)",
            type: "text",
            value: (val||'')
          }
        },
      }).then(function(val){
        if(val) {
          $.ajax({
            url: href,
            method: "POST",
            dataType: "json",
            data: {
              LKEScoreVal: val
            }
          }).success(function(res) {
            if(res.error) {
              swal({
                title: 'ERROR',
                text: res.error,
                icon: 'error',
                buttons:false
              });
            } else {
              refreshCard(id);
            }
          }).fail(function() {
            swal({
              title: 'SERVER ERROR',
              text: 'Terjadi kesalahan pada server. Silakan coba kembali.',
              icon: 'error',
              buttons:false
            });
          }).done(function() {

          });
        }
      });
      return false;
    });

    $('.btn-kke', cardEl).click(function() {
      var href = $(this).attr('href');
      $('.modal-body', $('#modal-kke')).load(href, function() {
        $('#modal-kke').modal('show');
      });
      return false;
    });
  });
}
$(document).ready(function(){
  $('button[data-card-widget="collapse"]', $('#form-lke')).click(function(){
    var cardEl = $(this).closest('.card');
    if(cardEl.is('.collapsed-card')) {
      refreshCard(cardEl.data('id'));
    }
  });
});
</script>
