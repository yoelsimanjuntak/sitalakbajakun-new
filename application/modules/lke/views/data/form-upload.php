<?php
$rdetail = $this->db
->select('*, (select sum(det.LKEWeight) from lke_data_detail det where det.LKEId = lke_data_detail.LKEId and det.FormParent = lke_data_detail.FormUniq) as SumWeight')
->where(COL_LKEID, $data[COL_UNIQ])
->where(COL_FORMPARENT, null)
->order_by(COL_LKESEQ)
->get(TBL_LKE_DATA_DETAIL)
->result_array();
?>
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h3 class="m-0 text-dark font-weight-light"><?=$title?></h3>
      </div>
      <div class="col-sm-6 text-right">
        <a href="<?=site_url('lke/data/index/'.strtolower($data[COL_LKETYPE]))?>" class="btn btn-sm btn-secondary"><i class="far fa-arrow-circle-left"></i> KEMBALI</a>
        <!--<button type="button" class="btn btn-sm btn-primary <?=!$isEditable?'disabled':'btn-submit'?>" data-mode="save" data-prompt="Anda yakin ingin menyimpan LKE ini? LKE yang disimpan akan tetap memiliki status sebagai DRAFT."><i class="far fa-save"></i> SIMPAN</button>-->
        <button type="button" class="btn btn-sm btn-success <?=!$isEditable?'disabled':'btn-submit'?>" data-mode="submit" data-prompt="Anda yakin ingin mengirimkan LKE ini? LKE yang sudah dikirimkan akan diteruskan ke Tim Verifikator."><i class="far fa-check-circle"></i> SUBMIT</button>
      </div>
    </div>
  </div>
</div>
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-sm-12">
        <form id="form-lke" action="<?=current_url()?>" method="post">
          <input type="hidden" name="mode" value="save" />
          <?php
          foreach($rdetail as $d) {
            $rdet_ = $this->db
            ->where(COL_LKEID, $data[COL_UNIQ])
            ->where(COL_FORMPARENT, $d[COL_FORMUNIQ])
            ->get(TBL_LKE_DATA_DETAIL)
            ->result_array();
            ?>
            <div class="card collapsed-card" data-id="<?=$d[COL_FORMUNIQ]?>" data-url="<?=site_url('lke/data/filing-partial/'.$d[COL_FORMUNIQ])?>">
              <div class="card-header">
                <h5 class="card-title"><?=$d[COL_LKESEQ]?>. <strong><?=$d[COL_LKEQUEST]?></strong></h5>
                <div class="card-tools">
                  <!--<small><span class="font-weight-bold label-sumscore">--</span> / <?=$d['SumWeight']?></small>-->
                  <button type="button" class="btn btn-tool text-primary" data-card-widget="collapse"><i class="fas fa-plus"></i>
                  </button>
                </div>
              </div>
              <div class="card-body p-0">

              </div>
              <div class="overlay d-none">
                <i class="fas fa-2x fa-sync-alt fa-spin"></i>
              </div>
            </div>
            <?php
          }
          ?>
        </form>
      </div>
    </div>
  </div>
</section>
<script type="text/javascript">
function refreshCard(id) {
  var cardEl = $('.card[data-id='+id+']');
  var cardUrl = $('.card[data-id='+id+']').data('url');
  $('.overlay', cardEl).removeClass('d-none');
  $('.card-body', cardEl).load(cardUrl, function(){
    $('.overlay', cardEl).addClass('d-none');
    $('.btn-evidence', cardEl).click(function(){
      var href = $(this).attr('href');
      var val = $(this).data('val');
      swal({
        closeOnClickOutside: true,
        buttons: ['BATAL','SUBMIT'],
        text: "BUKTI DUKUNG",
        content: {
          element: "input",
          attributes: {
            placeholder: "TAUTAN / URL",
            type: "text",
            value: (val||'')
          }
        },
      }).then(function(val){
        if(val) {
          $.ajax({
            url: href,
            method: "POST",
            dataType: "json",
            data: {
              LKERemarks2: val
            }
          }).success(function(res) {
            if(res.error) {
              swal({
                title: 'ERROR',
                text: res.error,
                icon: 'error',
                buttons:false
              });
            } else {
              refreshCard(id);
            }
          }).fail(function() {
            swal({
              title: 'SERVER ERROR',
              text: 'Terjadi kesalahan pada server. Silakan coba kembali.',
              icon: 'error',
              buttons:false
            });
          }).done(function() {

          });
        }
      });
      return false;
    });
  });
}
$(document).ready(function(){
  $('button[data-card-widget="collapse"]', $('#form-lke')).click(function(){
    var cardEl = $(this).closest('.card');
    if(cardEl.is('.collapsed-card')) {
      refreshCard(cardEl.data('id'));
    }
  });
});
</script>
