<?php
$rdetail = $this->db
->select('*, (select sum(det.LKEWeight) from lke_data_detail det where det.LKEId = lke_data_detail.LKEId and det.FormParent = lke_data_detail.FormUniq) as SumWeight')
->where(COL_LKEID, $data[COL_UNIQ])
->where(COL_FORMPARENT, null)
->order_by(COL_LKESEQ)
->get(TBL_LKE_DATA_DETAIL)
->result_array();

$ropt = $this->db->get(TBL_LKE_SCORING)->result_array();
?>
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h3 class="m-0 text-dark font-weight-light"><?=$title?></h3>
      </div>
      <div class="col-sm-6 text-right">
        <a href="<?=site_url('lke/data/index/'.strtolower($data[COL_LKETYPE]))?>" class="btn btn-sm btn-secondary"><i class="far fa-arrow-circle-left"></i> KEMBALI</a>
        <button type="button" class="btn btn-sm btn-primary <?=!$isEditable?'disabled':'btn-submit'?>" data-mode="save" data-prompt="Anda yakin ingin menyimpan LKE ini? LKE yang disimpan akan tetap memiliki status sebagai DRAFT."><i class="far fa-save"></i> SIMPAN</button>
        <button type="button" class="btn btn-sm btn-success <?=!$isEditable?'disabled':'btn-submit'?>" data-mode="submit" data-prompt="Anda yakin ingin mengirimkan LKE ini? LKE yang sudah dikirimkan akan diteruskan ke Tim Verifikator."><i class="far fa-check-circle"></i> SUBMIT</button>
      </div>
    </div>
  </div>
</div>
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-sm-12">
        <form id="form-lke" action="<?=current_url()?>" method="post">
          <input type="hidden" name="mode" value="save" />
          <?php
          foreach($rdetail as $d) {
            $rdet_ = $this->db
            ->where(COL_LKEID, $data[COL_UNIQ])
            ->where(COL_FORMPARENT, $d[COL_FORMUNIQ])
            ->get(TBL_LKE_DATA_DETAIL)
            ->result_array();
            ?>
            <div class="card collapsed-card" data-id="<?=$d[COL_FORMUNIQ]?>">
              <div class="card-header">
                <h5 class="card-title"><?=$d[COL_LKESEQ]?>. <strong><?=$d[COL_LKEQUEST]?></strong></h5>
                <div class="card-tools">
                  <small><span class="font-weight-bold label-sumscore">--</span> / <?=$d['SumWeight']?></small>
                  <button type="button" class="btn btn-tool text-primary" data-card-widget="collapse"><i class="fas fa-plus"></i>
                  </button>
                </div>
              </div>
              <div class="card-body p-0">
                <table class="table table-bordered">
                  <thead>
                    <tr>
                      <th style="width: 10px; white-space: nowrap">NO.</th>
                      <th>KOMPONEN / SUB KOMPONEN</th>
                      <th style="width: 50px; white-space: nowrap">BOBOT</th>
                      <th style="width: 50px; white-space: nowrap">JAWABAN</th>
                      <th style="width: 50px; white-space: nowrap">NILAI</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php
                    foreach($rdet_ as $d_) {
                      $crit = array();
                      if(!empty($d_[COL_LKECRITERIA])) $crit = explode(";", $d_[COL_LKECRITERIA]);
                      ?>
                      <tr>
                        <td rowspan="2"><?=$d[COL_LKESEQ].'.'.$d_[COL_LKESEQ]?></td>
                        <td>
                          <p class="font-weight-bold mb-1 text-justify">
                            <?=$d_[COL_LKEQUEST]?>
                          </p>
                          <?php
                          if(!empty($crit)) {
                            ?>
                            <p class="mb-0 text-sm">Kriteria: </p>
                            <ol type="A" style="padding-left: 1rem">
                              <?php
                              foreach($crit as $c) {
                                echo '<li class="text-sm">'.$c.'</li>';
                              }
                              ?>
                            </ol>
                            <?php
                          }
                          ?>
                        </td>
                        <td class="text-right">
                          <input type="hidden" name="LKEUniq[]" value="<?=$d_[COL_UNIQ]?>" />
                          <input type="text" class="form-control text-right" style="width: 100px" value="<?=number_format($d_[COL_LKEWEIGHT],2)?>" readonly />
                        </td>
                        <td>
                          <select class="form-control" name="<?=COL_LKESCORETEXT?>[]" data-parent-id="<?=$d_[COL_FORMPARENT]?>" style="width: 100px" required>
                            <option value="">--</option>
                            <?php
                            foreach($ropt as $opt) {
                              ?>
                              <option value="<?=$opt[COL_SCORETEXT]?>" data-value="<?=$opt[COL_SCOREVALUE]/100*$d_[COL_LKEWEIGHT]?>" <?=!empty($d_[COL_LKESCORETEXT])&&$d_[COL_LKESCORETEXT]==$opt[COL_SCORETEXT]?'selected':''?>><?=$opt[COL_SCORETEXT]?></option>
                              <?php
                            }
                            ?>
                          </select>
                        </td>
                        <td class="text-right">
                          <input type="text" class="form-control text-right" name="<?=COL_LKESCOREVAL?>[]" style="width: 100px" readonly />
                        </td>
                      </tr>
                      <tr>
                        <td colspan="4">
                          <div class="row">
                            <div class="col-sm-6">
                              <div class="form-group">
                                <label class="text-sm">CATATAN</label>
                                <textarea class="form-control" rows="3" name="<?=COL_LKEREMARKS?>[]"><?=!empty($d_[COL_LKEREMARKS])?$d_[COL_LKEREMARKS]:''?></textarea>
                              </div>
                            </div>
                            <div class="col-sm-6">
                              <div class="form-group">
                                <label class="text-sm">TAUTAN / EVIDENCE</label>
                                <textarea class="form-control" rows="3" name="<?=COL_LKEREMARKS2?>[]"><?=!empty($d_[COL_LKEREMARKS2])?$d_[COL_LKEREMARKS2]:''?></textarea>
                              </div>
                            </div>
                          </div>
                        </td>
                      </tr>
                      <?php
                    }
                    ?>
                  </tbody>
                </table>
              </div>
            </div>
            <?php
          }
          ?>
        </form>
      </div>
    </div>
  </div>
</section>
<script type="text/javascript">
function calculateScore(id) {
  var elScoreText = $('[name^=LKEScoreText]', $('div[data-id='+id+']'));
  var sumScore = 0;
  if(elScoreText.length > 0) {
    for(var i=0; i<elScoreText.length; i++) {
      var elRow = elScoreText[i].closest('tr');
      var elOpt = $('option:selected', elScoreText[i]);
      if(elOpt) {
        var val = 0;
        if(elOpt.data('value')) {
          val = elOpt.data('value');
        }

        sumScore += val;
        $('input[name^=LKEScoreVal]', elRow).val(val);
      }
    }
    $('.label-sumscore', $('div[data-id='+id+']')).html(sumScore.toString());
  }
}
$(document).ready(function(){
  $('[name^=LKEScoreText]').change(function(){
    var idParent = $(this).data('parent-id');
    calculateScore(idParent);
  }).trigger('change');

  $('#form-lke').validate({
    submitHandler: function(form) {
      var btnSubmit = $('button.btn-submit', form);
      btnSubmit.attr('disabled', true);

      $(form).ajaxSubmit({
        dataType: 'json',
        type : 'post',
        success: function(res) {
          if(res.error != 0) {
            toastr.error(res.error);
          } else {
            toastr.success(res.success);
            if(res.data.redirect) {
              setTimeout(function(){
                location.href = res.data.redirect;
              }, 1000);
            }

          }
        },
        error: function() {
          toastr.error('SERVER ERROR');
        },
        complete: function() {
          btnSubmit.attr('disabled', false);
        }
      });

      return false;
    }
  });

  $('.btn-submit').click(function() {
    var mode = $(this).data('mode');
    if(mode) {
      $('input[name=mode]', $('#form-lke')).val(mode);
      $('#form-lke').submit();
    }
  });
});
</script>
