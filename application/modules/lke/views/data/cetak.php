<html>
<head>
  <title><?=$title?></title>
  <style>
  body {
    font-family: 'Source Sans Pro','Helvetica Neue',Helvetica,Arial,sans-serif;
  }
  table {
    width: 100%;
    border-collapse: collapse;
    margin-bottom: 0 !important;
  }
  table, th, td {
    border: 1px solid black;
  }
  th, td {
    padding: 5px;
  }
  </style>
</head>
<body>
  <table width="100%" style="border: 0 !important">
    <tr>
      <td colspan="2" style="text-align: center; vertical-align: middle; border: 0 !important">
        <h4>LEMBAR KERJA EVALUASI AKUNTABILITAS KINERJA</h4>
      </td>
    </tr>
  </table>
  <br />
  <table width="100%" style="border: 0 !important; font-size: 10pt !important">
    <tr>
      <td style="vertical-align: top; width: 200px; white-space: nowrap; border: 0 !important">TAHUN</td>
      <td style="vertical-align: top; width: 10px; border: 0 !important">:</td>
      <td style="vertical-align: top; font-weight: bold; border: 0 !important"><?=$data[COL_LKETAHUN]?></td>
    </tr>
    <tr>
      <td style="vertical-align: top; width: 200px; white-space: nowrap; border: 0 !important">FORMAT</td>
      <td style="vertical-align: top; width: 10px; border: 0 !important">:</td>
      <td style="vertical-align: top; font-weight: bold; border: 0 !important"><?=$data[COL_LKENAMA]?></td>
    </tr>
    <tr>
      <td style="vertical-align: top; width: 200px; white-space: nowrap; border: 0 !important">STATUS</td>
      <td style="vertical-align: top; width: 10px; border: 0 !important">:</td>
      <td style="vertical-align: top; font-weight: bold; border: 0 !important"><?=$data[COL_LKESTATUS]?></td>
    </tr>
    <tr>
      <td style="vertical-align: top; width: 200px; white-space: nowrap; border: 0 !important">UNIT KERJA</td>
      <td style="vertical-align: top; width: 10px; border: 0 !important">:</td>
      <td style="vertical-align: top; font-weight: bold; border: 0 !important"><?=!empty($data[COL_SKPDNAMA])?$data[COL_SKPDNAMA]:$this->setting_org_name?></td>
    </tr>
  </table>
  <br />
  <table width="100%" border="1" style="font-size: 10pt !important">
    <tr>
      <td style="vertical-align: top; width: 10px; font-weight: bold; white-space: nowrap">NO.</td>
      <td style="vertical-align: top; width: 100px; font-weight: bold; white-space: nowrap">KOMPONEN</td>
      <td style="vertical-align: top; width: 10px; font-weight: bold; white-space: nowrap">JAWABAN</td>
      <td style="vertical-align: top; width: 10px; font-weight: bold; white-space: nowrap">BOBOT</td>
      <td style="vertical-align: top; width: 10px; font-weight: bold; white-space: nowrap">NILAI</td>
    </tr>
    <?php
    $sumWeight = 0;
    $sumScore = 0;
    foreach($det as $d) {
      $det_ = $this->db
      ->where(COL_LKEID, $d[COL_LKEID])
      ->where(COL_FORMPARENT, $d[COL_FORMUNIQ])
      ->get(TBL_LKE_DATA_DETAIL)
      ->result_array();
      ?>
      <tr>
        <td style="vertical-align: top; white-space: nowrap; width: 10px; text-align: right; font-weight: bold"><?=$d[COL_LKESEQ]?>.</td>
        <td colspan="2" style="vertical-align: top; font-weight: bold"><?=$d[COL_LKEQUEST]?></td>
        <td style="vertical-align: top; white-space: nowrap; width: 10px; text-align: right; ; font-weight: bold"><?=number_format($d['SumWeight'], 2)?></td>
        <td style="vertical-align: top; white-space: nowrap; width: 10px; text-align: right; ; font-weight: bold"><?=number_format($d['SumScore'], 2)?></td>
      </tr>
      <?php
      foreach($det_ as $d_) {
        ?>
        <tr>
          <td style="vertical-align: top; white-space: nowrap; width: 10px; text-align: right"><?=$d_[COL_LKESEQ]?>.</td>
          <td style="vertical-align: top;">
            <?=$d_[COL_LKEQUEST]?> <br /><br />
            <p>
              <small><strong>CATATAN: </strong><?=!empty($d_[COL_LKEREMARKS])?'<br />'.nl2br($d_[COL_LKEREMARKS]):'-'?></small><br /><br />
              <small><strong>TAUTAN: </strong> <?=!empty($d_[COL_LKEREMARKS2])?'<br />'.nl2br($d_[COL_LKEREMARKS2]):'-'?></small><br />
            </p>
          </td>
          <td style="vertical-align: top; white-space: nowrap; width: 10px; text-align: center"><?=$d_[COL_LKESCORETEXT]?></td>
          <td style="vertical-align: top; white-space: nowrap; width: 10px; text-align: right"><?=number_format($d_[COL_LKEWEIGHT], 2)?></td>
          <td style="vertical-align: top; white-space: nowrap; width: 10px; text-align: right"><?=number_format($d_[COL_LKESCOREVAL], 2)?></td>
        </tr>
        <?php
        $sumWeight += $d_[COL_LKEWEIGHT];
        $sumScore += $d_[COL_LKESCOREVAL];
      }
    }
    ?>
    <tr>
      <td colspan="3" style="vertical-align: top; text-align: right; font-weight: bold">TOTAL</td>
      <td style="vertical-align: top; white-space: nowrap; width: 10px; text-align: right; ; font-weight: bold"><?=number_format($sumWeight, 2)?></td>
      <td style="vertical-align: top; white-space: nowrap; width: 10px; text-align: right; ; font-weight: bold"><?=number_format($sumScore, 2)?></td>
    </tr>
  </table>
</body>
</html>
