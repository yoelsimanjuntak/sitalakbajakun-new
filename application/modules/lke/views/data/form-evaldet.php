<table class="table table-bordered">
  <tbody>
    <?php
    foreach($rdetail as $d_) {
      $url = '#';
      if(!empty($d_[COL_LKEREMARKS2])) {
        $url = $d_[COL_LKEREMARKS2];
        $urlParsed = parse_url($d_[COL_LKEREMARKS2]);
        if (empty($urlParsed['scheme'])) {
          $url = 'http://' . ltrim($url, '/');
        }
      }
      ?>
      <tr>
        <td class="font-italic text-right" style="width: 10px; white-space: nowrap; vertical-align: middle"><?=$d_[COL_LKESEQ]?></td>
        <td>
          <p class="font-italic mb-0 text-justify">
            <?=$d_[COL_LKEQUEST]?><small class="float-right"><a href="<?=$url?>" target="_blank" class="btn btn-xs <?=empty($d_[COL_LKEREMARKS2])?'disabled':'btn-primary'?>"><i class="far fa-link"></i>&nbsp;EVIDENCE</a></small>
          </p>
        </td>
        <td style="width: 100px; white-space: nowrap">
          <a href="<?=site_url('lke/data/kke/'.$d_[COL_UNIQ])?>" class="btn btn-outline-success btn-xs btn-kke">
            <i class="far fa-tasks"></i>&nbsp;KKE
          </a>
          <a href="<?=site_url('lke/data/scoring/'.$d_[COL_UNIQ])?>" data-val="<?=$d_[COL_LKESCOREVAL]?>" class="btn btn-<?=empty($d_[COL_LKESCOREVAL])?'outline-secondary':'primary'?> btn-xs btn-score">
            <i class="far fa-edit"></i>&nbsp;<?=!empty($d_[COL_LKESCOREVAL])?number_format($d_[COL_LKESCOREVAL],2).'%':'NIHIL'?>
          </a>
        </td>
      </tr>
      <?php
    }
    ?>
  </tbody>
</table>
