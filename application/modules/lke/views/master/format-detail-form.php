<form id="form-lke" action="<?=current_url()?>" method="post">
  <div class="row">
    <div class="col-sm-6">
      <div class="form-group">
        <label>No. Urut</label>
        <input type="text" name="<?=COL_FORMSEQ?>" class="form-control uang text-right" value="<?=!empty($data)?$data[COL_FORMSEQ]:''?>" />
      </div>
    </div>
    <div class="col-sm-6">
      <div class="form-group">
        <label>Bobot</label>
        <input type="text" name="<?=COL_FORMWEIGHT?>" class="form-control uang text-right" value="<?=!empty($data)?$data[COL_FORMWEIGHT]:''?>" />
      </div>
    </div>
    <div class="col-sm-12">
      <div class="form-group">
        <label>Judul</label>
        <input type="text" name="<?=COL_FORMQUEST?>" class="form-control" value="<?=!empty($data)?$data[COL_FORMQUEST]:''?>" />
      </div>
    </div>
    <div class="col-sm-12">
      <div class="form-group">
        <label>Keterangan</label>
        <textarea name="<?=COL_FORMREMARKS?>" class="form-control"><?=!empty($data)?$data[COL_FORMREMARKS]:''?></textarea>
      </div>
    </div>
    <div class="col-sm-12">
      <div class="form-group">
        <label>Kriteria <small>(dipisah dengan "<strong>;</strong>")</small></label>
        <textarea name="<?=COL_FORMCRITERIA?>" class="form-control"><?=!empty($data)?$data[COL_FORMCRITERIA]:''?></textarea>
      </div>
    </div>
  </div>
</form>
<script type="text/javascript">
$(document).ready(function(){
  $('#form-lke').validate({
    submitHandler: function(form) {
      var modal = $(form).closest('modal');
      if(modal) {
        var btnSubmit = $('button[type=submit]', modal);
        var txtSubmit = btnSubmit.innerHTML;
        btnSubmit.html('<i class="fad fa-circle-notch fa-spin"></i>');
        btnSubmit.attr('disabled', true);
      }

      $(form).ajaxSubmit({
        dataType: 'json',
        type : 'post',
        success: function(res) {
          if(res.error != 0) {
            toastr.error(res.error);
          } else {
            toastr.success(res.success);
            setTimeout(function(){
              location.reload();
            }, 1000);
          }
        },
        error: function() {
          toastr.error('SERVER ERROR');
        },
        complete: function() {
          btnSubmit.html(txtSubmit);
          btnSubmit.attr('disabled', false);
        }
      });

      return false;
    }
  });
});
</script>
