
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title><?=!empty($title) ? 'SI EVATAS - '.$title : 'SI EVATAS - Login'?></title>
  <link rel="icon" type="image/png" href="<?=MY_IMAGEURL.$this->setting_web_logo?>">
  <link rel="stylesheet" href="<?=base_url()?>assets/themes/mazer/assets/compiled/css/app.css">
  <link rel="stylesheet" href="<?=base_url()?>assets/themes/mazer/assets/compiled/css/app-dark.css">
  <link rel="stylesheet" href="<?=base_url()?>assets/themes/mazer/assets/compiled/css/auth.css">

  <link rel="stylesheet" href="<?=base_url()?>assets/tbs/css/font-awesome.min.css" />
  <link rel="stylesheet" href="<?=base_url()?>assets/tbs/fontawesome-pro/web/css/all.min.css" />

  <script src="<?=base_url()?>assets/themes/adminlte-new/plugins/jQuery/jquery-2.2.3.min.js"></script>
  <script src="<?=base_url()?>assets/themes/mazer/assets/static/js/initTheme.js"></script>

  <link rel="stylesheet" href="<?=base_url()?>assets/themes/adminlte-new/plugins/toastr/toastr.min.css">
  <script src="<?=base_url()?>assets/themes/adminlte-new/plugins/toastr/toastr.min.js"></script>

  <style>
  .se-pre-con {
      position: fixed;
      left: 0px;
      top: 0px;
      width: 100%;
      height: 100%;
      z-index: 9999;
      background: url("<?=base_url()?>assets/preloader/images/<?=$this->setting_web_preloader?>") center no-repeat #fff;
  }
  @media screen and (max-width: 576px) {
    #auth #auth-left {
      padding: 5rem 1.5rem !important;
    }
  }
  </style>
  <script>
  $(window).load(function() {
    $(".se-pre-con").fadeOut("slow");
  });
  </script>
</head>

<body>
  <div class="se-pre-con"></div>
  <div id="auth">
    <div class="row h-100">
      <div class="col-lg-5 col-12">
        <div id="auth-left">
            <div class="mb-5 d-flex align-items-center">
              <div class="auth-logo m-0">
                <img src="<?=MY_IMAGEURL.$this->setting_web_logo?>" alt="Logo" style="height: 4rem !important">
              </div>
              <div class="p-2 pl-4">
                <h2 class="mb-0">SI EVATAS</h2>
                <p class="mb-0">Sistem Informasi Evaluasi Akuntabilitas Kinerja</p>
              </div>
            </div>
            <h3 class="auth-title mb-1" style="font-size: 20pt !important">Login</h3>
            <p class="mb-3" style="font-style: italic">Silakan masuk menggunakan akun anda.</p>
            <?= form_open(current_url(),array('id'=>'form-login')) ?>
              <div class="form-group position-relative has-icon-left mb-4">
                <input type="text" class="form-control form-control-xl" name="<?=COL_USERNAME?>" placeholder="Username" required />
                <div class="form-control-icon">
                  <i class="bi bi-person"></i>
                </div>
              </div>
              <div class="form-group position-relative has-icon-left mb-4">
                <input type="password" class="form-control form-control-xl" name="<?=COL_PASSWORD?>" placeholder="Password" required />
                <div class="form-control-icon">
                  <i class="bi bi-shield-lock"></i>
                </div>
              </div>
              <button type="submit" class="btn btn-primary btn-block btn-lg shadow-lg mt-3">Login <i class="fas fa-sign-in"></i></button>
            <?= form_close(); ?>
        </div>
      </div>
      <div class="col-lg-7 d-none d-lg-block">
        <div id="auth-right" style="background-size: cover !important; background:url('<?=MY_IMAGEURL.'bg-overlay.png'?>'),linear-gradient(90deg,#2d499d,#3f5491)">
        </div>
      </div>
    </div>
  </div>
  <script src="<?=base_url()?>assets/js/jquery.particleground.js"></script>
  <script type="text/javascript" src="<?=base_url() ?>assets/js/jquery.blockUI.js"></script>
  <script type="text/javascript" src="<?=base_url()?>assets/template/js/jquery.validate.min.js"></script>
  <script type="text/javascript" src="<?=base_url()?>assets/template/js/function.js"></script>
  <script type="text/javascript" src="<?=base_url()?>assets/template/js/jquery.form.js"></script>
  <script>
  $(document).ready(function() {
      $('#auth-right').particleground({
        dotColor: '#396EB0',
        lineColor: '#396EB0'
      });

      $('#form-login').validate({
        submitHandler: function(form) {
          var btnSubmit = $('button[type=submit]', $(form));
          var txtSubmit = btnSubmit[0].innerHTML;
          btnSubmit.html('<i class="fad fa-circle-notch fa-spin"></i>');
          $(form).ajaxSubmit({
            dataType: 'json',
            type : 'post',
            success: function(res) {
              if(res.error != 0) {
                toastr.error(res.error);
              } else {
                toastr.success(res.success);
                btnSubmit.html('Masuk...');
                if(res.redirect) {
                  setTimeout(function(){
                    location.href = res.redirect;
                  }, 1000);
                } else {
                  location.reload();
                }
              }
            },
            error: function() {
              toastr.error('SERVER ERROR');
            },
            complete: function() {
              btnSubmit.html(txtSubmit);
            }
          });

          return false;
        }
      });
  });
  </script>
</body>
</html>
