<?php
$ruser = GetLoggedUser();
$rlke1 = array();
$rlke2 = array();
$rskpd = array();
if($ruser[COL_ROLEID] != ROLEADMIN && $ruser[COL_ROLEID] != ROLEAPIP && $ruser[COL_ROLEID] != ROLEGUEST) {
  $rskpd = $this->db->where(COL_SKPDID, $ruser[COL_SKPDID])->get(TBL_SAKIPV2_SKPD)->row_array();
}

if($ruser[COL_ROLEID] == ROLEADMIN || $ruser[COL_ROLEID] == ROLEAPIP || $ruser[COL_ROLEID] == ROLEGUEST) {
  $rlke1 = $this->db
  ->select('lke_data.*, (select sum(det.LKEScoreVal) from lke_data_detail det where det.LKEId=lke_data.Uniq and FormParent is null) as TotSkor, (select sum(det.LKEWeight) from lke_data_detail det where det.LKEId=lke_data.Uniq and FormParent is null) as TotWeight')
  ->where(COL_LKETAHUN, date('Y'))
  ->where(COL_FORMCATEGORY, 'AKIP')
  ->where(COL_LKETYPE, 'INSTANSI')
  ->get(TBL_LKE_DATA)
  ->result_array();
}

if($ruser[COL_ROLEID] != ROLEADMIN && $ruser[COL_ROLEID] != ROLEAPIP && $ruser[COL_ROLEID] != ROLEGUEST) {
  $this->db->where(TBL_LKE_DATA.'.'.COL_SKPDID, $ruser[COL_SKPDID]);
}
$rlke2 = $this->db
->select('lke_data.*, opd.SkpdNama, (select sum(det.LKEScoreVal) from lke_data_detail det where det.LKEId=lke_data.Uniq and FormParent is null) as TotSkor, (select sum(det.LKEWeight) from lke_data_detail det where det.LKEId=lke_data.Uniq and FormParent is null) as TotWeight')
->join(TBL_SAKIPV2_SKPD.' opd','opd.'.COL_SKPDID." = ".TBL_LKE_DATA.".".COL_SKPDID,"left")
->where(COL_LKETAHUN, date('Y'))
->where(COL_FORMCATEGORY, 'AKIP')
->where(COL_LKETYPE, 'UNIT')
->get(TBL_LKE_DATA)
->result_array();
?>
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h3 class="m-0 font-weight-light"><?=$title?></h3>
      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <?php
          if($ruser[COL_ROLEID] != ROLEADMIN && $ruser[COL_ROLEID] != ROLEAPIP && $ruser[COL_ROLEID] != ROLEGUEST) {
            ?>
            <li class="breadcrumb-item active font-italic"><?=strtoupper($rskpd[COL_SKPDNAMA])?></li>
            <?php
          }
          ?>
        </ol>
      </div>
    </div>
  </div>
</div>
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12">
        <?php
        if($ruser[COL_ROLEID] == ROLEADMIN || $ruser[COL_ROLEID] == ROLEAPIP || $ruser[COL_ROLEID] == ROLEGUEST) {
          ?>
          <div class="card card-primary">
            <div class="card-header">
              <h3 class="card-title">DAFTAR LKE INSTANSI TH. <strong><?=date('Y')?></strong></h3>
            </div>
            <div class="card-body p-0">
              <table class="table table-bordered text-sm">
                <thead>
                  <tr>
                    <th style="width: 10px; white-space: nowrap">NO.</th>
                    <th>KATEGORI</th>
                    <th>JUDUL / FORMAT</th>
                    <th style="width: 10px; white-space: nowrap">STATUS</th>
                    <th style="width: 10px; white-space: nowrap">SKOR</th>
                    <th style="width: 10px; white-space: nowrap; text-align: center">#</th>
                  </tr>
                </thead>
                <tbody>
                  <?php
                  if(!empty($rlke1)) {
                    $no=1;
                    foreach($rlke1 as $r) {
                      $isEditable=true;
                      $badgeStat = 'badge-secondary';
                      if($r[COL_LKESTATUS]==LKE_STATUS_VERIFIKASI) $badgeStat = 'badge-primary';
                      if($r[COL_LKESTATUS]==LKE_STATUS_FINAL) $badgeStat = 'badge-success';


                      if($r[COL_LKESTATUS]!=LKE_STATUS_DRAFT && ($ruser[COL_ROLEID]!=ROLEADMIN&&$ruser[COL_ROLEID]!=ROLEAPIP)) $isEditable = false;
                      if($r[COL_LKESTATUS]==LKE_STATUS_FINAL) $isEditable = false;
                      ?>
                      <tr>
                        <td class="text-right"><?=$no?></td>
                        <td><?=$r[COL_FORMCATEGORY]?></td>
                        <td><?=$r[COL_LKENAMA]?></td>
                        <td style="width: 10px; white-space: nowrap; text-align: center"><span class="badge <?=$badgeStat?>"><?=$r[COL_LKESTATUS]?></span></td>
                        <td style="width: 10px; white-space: nowrap; text-align: center"><?=(!empty($r['TotSkor'])?'<strong>'.number_format($r['TotSkor'],2).'</strong>':'<strong>--</strong>').' / '.(!empty($r['TotWeight'])?'<strong>'.number_format($r['TotWeight'],2).'</strong>':'<strong>--</strong>')?></td>
                        <td style="width: 10px; white-space: nowrap; text-align: center"><?='<a href="'.site_url('lke/data/form/'.$r[COL_UNIQ]).'" class="btn btn-xs btn-primary"><i class="fas fa-arrow-circle-right"></i>&nbsp;LIHAT</a>'?></td>
                      </tr>
                      <?php
                      $no++;
                    }
                  } else {
                    ?>
                    <tr>
                      <td colspan="6">
                        <p class="font-italic text-center mb-0">
                          BELUM ADA DATA TERSEDIA
                        </p>
                      </td>
                    </tr>
                    <?php
                  }
                  ?>
                </tbody>
              </table>
            </div>
          </div>
          <?php
        }
        ?>

        <div class="card card-primary">
          <div class="card-header">
            <h3 class="card-title">DAFTAR LKE UNIT TH. <strong><?=date('Y')?></strong></h3>
          </div>
          <div class="card-body p-0">
            <table class="table table-bordered text-sm">
              <thead>
                <tr>
                  <th style="width: 10px; white-space: nowrap">NO.</th>
                  <th>KATEGORI</th>
                  <?php
                  if($ruser[COL_ROLEID] == ROLEADMIN || $ruser[COL_ROLEID] == ROLEAPIP || $ruser[COL_ROLEID] == ROLEGUEST) {
                    ?>
                    <th>SKPD</th>
                    <?php
                  }
                  ?>
                  <th>JUDUL / FORMAT</th>
                  <th style="width: 10px; white-space: nowrap">STATUS</th>
                  <th style="width: 10px; white-space: nowrap">SKOR</th>
                  <th style="width: 10px; white-space: nowrap; text-align: center">#</th>
                </tr>
              </thead>
              <tbody>
                <?php
                if(!empty($rlke2)) {
                  $no=1;
                  foreach($rlke2 as $r) {
                    $isEditable=true;
                    $badgeStat = 'badge-secondary';
                    if($r[COL_LKESTATUS]==LKE_STATUS_VERIFIKASI) $badgeStat = 'badge-primary';
                    if($r[COL_LKESTATUS]==LKE_STATUS_FINAL) $badgeStat = 'badge-success';


                    if($r[COL_LKESTATUS]!=LKE_STATUS_DRAFT && ($ruser[COL_ROLEID]!=ROLEADMIN&&$ruser[COL_ROLEID]!=ROLEAPIP)) $isEditable = false;
                    if($r[COL_LKESTATUS]==LKE_STATUS_FINAL) $isEditable = false;
                    ?>
                    <tr>
                      <td class="text-right"><?=$no?></td>
                      <td><?=$r[COL_FORMCATEGORY]?></td>
                      <?php
                      if($ruser[COL_ROLEID] == ROLEADMIN || $ruser[COL_ROLEID] == ROLEAPIP || $ruser[COL_ROLEID] == ROLEGUEST) {
                        ?>
                        <td><?=$r[COL_SKPDNAMA]?></td>
                        <?php
                      }
                      ?>
                      <td><?=$r[COL_LKENAMA]?></td>
                      <td style="width: 10px; white-space: nowrap; text-align: center"><span class="badge <?=$badgeStat?>"><?=$r[COL_LKESTATUS]?></span></td>
                      <td style="width: 10px; white-space: nowrap; text-align: center"><?=(!empty($r['TotSkor'])?'<strong>'.number_format($r['TotSkor'],2).'</strong>':'<strong>--</strong>').' / '.(!empty($r['TotWeight'])?'<strong>'.number_format($r['TotWeight'],2).'</strong>':'<strong>--</strong>')?></td>
                      <td style="width: 10px; white-space: nowrap; text-align: center"><?='<a href="'.site_url('lke/data/form/'.$r[COL_UNIQ]).'" class="btn btn-xs btn-primary"><i class="fas fa-arrow-circle-right"></i>&nbsp;LIHAT</a>'?></td>
                    </tr>
                    <?php
                    $no++;
                  }
                } else {
                  ?>
                  <tr>
                    <td colspan="6">
                      <p class="font-italic text-center mb-0">
                        BELUM ADA DATA TERSEDIA
                      </p>
                    </td>
                  </tr>
                  <?php
                }
                ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
