<?php
/**
 * Created by PhpStorm.
 * User: Yoel Simanjuntak
 * Date: 30/09/2018
 * Time: 17:52
 */
$ruser = GetLoggedUser();
?>
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1 class="m-0 text-dark"><?= $title ?> <small> Form</small></h1>
      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="<?=site_url()?>">Home</a></li>
          <li class="breadcrumb-item"><a href="<?=site_url('ajbk/master/subbid-index')?>"> <?=$title?></a></li>
          <li class="breadcrumb-item active"><?=$edit?'Edit':'Add'?></li>
        </ol>
      </div>
    </div>
  </div>
</div>
<section class="content">
  <div class="container-fluid">
    <div class="row">
        <div class="col-sm-12">
            <div class="card card-primary">
              <?=form_open(current_url(),array('role'=>'form','id'=>'main-form','class'=>'form-horizontal'))?>
                <div class="card-body">
                    <div class="form-group row">
                        <label class="control-label col-sm-2">OPD</label>
                        <div class="col-sm-8">
                            <div class="input-group">
                                <?php
                                $nmSub = "";
                                $strOPD = explode('.', $ruser[COL_COMPANYID]);
                                if($edit) {
                                    $eplandb = $this->load->database("eplan", true);
                                    $eplandb->where(COL_KD_URUSAN, $data[COL_KD_URUSAN]);
                                    $eplandb->where(COL_KD_BIDANG, $data[COL_KD_BIDANG]);
                                    $eplandb->where(COL_KD_UNIT, $data[COL_KD_UNIT]);
                                    $eplandb->where(COL_KD_SUB, $data[COL_KD_SUB]);
                                    $subunit = $eplandb->get("ref_sub_unit")->row_array();
                                    if($subunit) {
                                        $nmSub = $subunit["Nm_Sub_Unit"];
                                    }
                                }
                                if($ruser[COL_ROLEID] == ROLEKADIS) {
                                    $eplandb = $this->load->database("eplan", true);
                                    $eplandb->where(COL_KD_URUSAN, $strOPD[0]);
                                    $eplandb->where(COL_KD_BIDANG, $strOPD[1]);
                                    $eplandb->where(COL_KD_UNIT, $strOPD[2]);
                                    $eplandb->where(COL_KD_SUB, $strOPD[3]);
                                    $subunit = $eplandb->get("ref_sub_unit")->row_array();
                                    if($subunit) {
                                        $nmSub = $subunit["Nm_Sub_Unit"];
                                    }
                                }
                                ?>
                                <input type="text" class="form-control" name="text-opd" value="<?= $edit ? $data[COL_KD_URUSAN].".".$data[COL_KD_BIDANG].".".$data[COL_KD_UNIT].".".$data[COL_KD_SUB]." ".$nmSub : ($ruser[COL_ROLEID] == ROLEKADIS ? $strOPD[0].".".$strOPD[1].".".$strOPD[2].".".$strOPD[3]." ".$nmSub : "")?>" readonly>
                                <input type="hidden" name="<?=COL_KD_URUSAN?>" value="<?= $edit ? $data[COL_KD_URUSAN] : ($ruser[COL_ROLEID]==ROLEKADIS?$strOPD[0]:"")?>" required   >
                                <input type="hidden" name="<?=COL_KD_BIDANG?>" value="<?= $edit ? $data[COL_KD_BIDANG] : ($ruser[COL_ROLEID]==ROLEKADIS?$strOPD[1]:"")?>" required   >
                                <input type="hidden" name="<?=COL_KD_UNIT?>" value="<?= $edit ? $data[COL_KD_UNIT] : ($ruser[COL_ROLEID]==ROLEKADIS?$strOPD[2]:"")?>" required   >
                                <input type="hidden" name="<?=COL_KD_SUB?>" value="<?= $edit ? $data[COL_KD_SUB] : ($ruser[COL_ROLEID]==ROLEKADIS?$strOPD[3]:"")?>" required   >
                                <div class="input-group-btn">
                                    <button type="button" class="btn btn-default btn-flat btn-browse-opd" data-toggle="modal" data-target="#browseOPD" data-toggle="tooltip" data-placement="top" title="Pilih OPD" <?=$edit?"disabled":($ruser[COL_ROLEID] == ROLEKADIS ? "disabled" : "")?>><i class="fa fa-ellipsis-h"></i></button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="control-label col-sm-2">Bidang</label>
                        <div class="col-sm-8">
                            <div class="input-group">
                                <input type="text" class="form-control" name="text-bid" value="<?= $edit ? $data[COL_KD_BID].". ".$data[COL_NM_BID] : ""?>" readonly>
                                <input type="hidden" name="<?=COL_KD_BID?>" value="<?= $edit ? $data[COL_KD_BID] : ""?>" required   >
                                <div class="input-group-btn">
                                    <button type="button" class="btn btn-default btn-flat btn-browse-bid" data-toggle="modal" data-target="#browseBid" data-toggle="tooltip" data-placement="top" title="Pilih Bidang" <?=$edit?"disabled":""?>><i class="fa fa-ellipsis-h"></i></button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="control-label col-sm-2">Kode Sub Bidang</label>
                        <div class="col-sm-2">
                            <input type="number" class="form-control" name="<?=COL_KD_SUBBID?>" value="<?= $edit ? $data[COL_KD_SUBBID] : ""?>" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="control-label col-sm-2">Nama</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" name="<?=COL_NM_SUBBID?>" value="<?= $edit ? $data[COL_NM_SUBBID] : ""?>" required>
                        </div>
                    </div>
                </div>
                <div class="card-footer">
                  <div class="row" style="text-align: center">
                    <div class="col-md-12">
                      <a href="<?=site_url('ajbk/master/subbid-index')?>" class="btn btn-default">KEMBALI</a>
                      <button type="submit" class="btn btn-primary">SIMPAN</button>
                    </div>
                  </div>
                </div>
                <?=form_close()?>
            </div>
        </div>
    </div>
  </div>
</section>
<div class="modal fade" id="browseOPD" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Browse</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true"><i class="fa fa-close"></i></span>
        </button>
      </div>
      <div class="modal-body">
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="browseBid" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Browse</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true"><i class="fa fa-close"></i></span>
        </button>
      </div>
      <div class="modal-body">
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
$(document).ready(function() {
  $("#main-form").validate({
      submitHandler : function(form){
          $(form).find('btn').attr('disabled',true);
          $(form).ajaxSubmit({
              dataType: 'json',
              type : 'post',
              success : function(data){
                  $(form).find('btn').attr('disabled',false);
                  if(data.error != 0){
                      toastr.error(data.error);
                  }else{
                      window.location.href = data.redirect;
                  }
              },
              error : function(a,b,c){
                  toastr.error('Response Error');
              },
              complete: function() {
                $(form).find('btn').attr('disabled',false);
              }
          });
          return false;
      }
  });

  $('.modal').on('hidden.bs.modal', function (event) {
      $(this).find(".modal-body").empty();
  });

  $('#browseOPD').on('show.bs.modal', function (event) {
      var modalBody = $(".modal-body", $("#browseOPD"));
      $(this).removeData('bs.modal');
      modalBody.html("<p style='font-style: italic'>Loading..</p>");
      modalBody.load("<?=site_url("ajbk/ajax/browse-opd")?>", function () {
          $("[name=selID][type=hidden]", modalBody).unbind().change(function () {
              var kdSub = $(this).val().split('|');
              $("[name=Kd_Urusan]").val(kdSub[0]);
              $("[name=Kd_Bidang]").val(kdSub[1]);
              $("[name=Kd_Unit]").val(kdSub[2]);
              $("[name=Kd_Sub]").val(kdSub[3]);

              $("[name=Kd_Bid]").val("");
              $("[name=text-bid]").val("");
          });
          $("[name=selText][type=hidden]", modalBody).unbind().change(function () {
              $("[name=text-opd]").val($(this).val());
          });
      });
  });

  $('#browseBid').on('show.bs.modal', function (event) {
      var modalBody = $(".modal-body", $("#browseBid"));
      $(this).removeData('bs.modal');
      var kdUrusan = $("[name=Kd_Urusan]").val();
      var kdBidang = $("[name=Kd_Bidang]").val();
      var kdUnit = $("[name=Kd_Unit]").val();
      var kdSub = $("[name=Kd_Sub]").val();

      if(!kdUrusan || !kdBidang || !kdUnit || !kdSub) {
          modalBody.html("<p style='font-style: italic'>Silakan pilih OPD terlebih dahulu!</p>");
          return;
      }

      modalBody.html("<p style='font-style: italic'>Loading..</p>");
      modalBody.load("<?=site_url("ajbk/ajax/browse-bid")?>"+"?Kd_Urusan="+kdUrusan+"&Kd_Bidang="+kdBidang+"&Kd_Unit="+kdUnit+"&Kd_Sub="+kdSub, function () {
          $("[name=selID][type=hidden]", modalBody).unbind().change(function () {
              $("[name=Kd_Bid]").val($(this).val());
          });
          $("[name=selText][type=hidden]", modalBody).unbind().change(function () {
              $("[name=text-bid]").val($(this).val());
          });
      });
  });
});
</script>
