<?php
$ruser = GetLoggedUser();

$ropd = array();
$rbid = array();
$rsubbid = array();
if($ruser[COL_ROLEID] == ROLEKADIS || $ruser[COL_ROLEID] == ROLEKABID || $ruser[COL_ROLEID] == ROLEKASUBBID) {
  $strOrg = explode('.', $ruser[COL_COMPANYID]) ;
  $ropd = $this->db->where(array(
    COL_KD_URUSAN=>$strOrg[0],
    COL_KD_BIDANG=>$strOrg[1],
    COL_KD_UNIT=>$strOrg[2],
    COL_KD_SUB=>$strOrg[3]
  ))->get(TBL_AJBK_UNIT)->row_array();

  if($ruser[COL_ROLEID] == ROLEKABID || $ruser[COL_ROLEID] == ROLEKASUBBID) {
    $rbid = $this->db->where(array(
      COL_KD_URUSAN=>$strOrg[0],
      COL_KD_BIDANG=>$strOrg[1],
      COL_KD_UNIT=>$strOrg[2],
      COL_KD_SUB=>$strOrg[3],
      COL_KD_BID=>$strOrg[4]
    ))->get(TBL_AJBK_UNIT_BID)->row_array();
  }

  if($ruser[COL_ROLEID] == ROLEKASUBBID) {
    $rsubbid = $this->db->where(array(
      COL_KD_URUSAN=>$strOrg[0],
      COL_KD_BIDANG=>$strOrg[1],
      COL_KD_UNIT=>$strOrg[2],
      COL_KD_SUB=>$strOrg[3],
      COL_KD_BID=>$strOrg[4],
      COL_KD_SUBBID=>$strOrg[5]
    ))->get(TBL_SAKIP_MSUBBID)->row_array();
  }
}

$data = array();
$i = 0;
foreach ($res as $d) {
  $txtUnit = '<li style="list-style-type: disclosure-closed">'.$d[COL_NM_SUB_UNIT].'</li>';
  if(!empty($d[COL_NM_BID])) {
    $txtUnit .= '<li style="list-style-type: disclosure-closed">'.$d[COL_NM_BID].'</li>';
  }
  if(!empty($d[COL_NM_SUBBID])) {
    $txtUnit .= '<li style="list-style-type: disclosure-closed">'.$d[COL_NM_SUBBID].'</li>';
  }
  $res[$i] = array(
    $noaction ?
    anchor('ajbk/jabatan/edit/'.$d[COL_KD_JABATAN],'<i class="fa fa-eye text-primary"></i>', array('target'=>'_blank','data-toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Lihat')) :
    anchor('ajbk/jabatan/edit/'.$d[COL_KD_JABATAN],'<i class="fa fa-clipboard text-primary"></i>', array('data-toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Ubah')).'&nbsp;&nbsp;'.
    anchor('ajbk/jabatan/duplicate/'.$d[COL_KD_JABATAN],'<i class="fa fa-copy text-info"></i>', array('class'=>'btn-duplicate','data-name'=>$d[COL_NM_JABATAN],'data-toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Duplikasi')).'&nbsp;&nbsp;'.
    anchor('ajbk/jabatan/cetak/'.$d[COL_KD_JABATAN],'<i class="fa fa-print text-success"></i>', array('class'=>'btn-print', 'target'=>'_blank', 'data-toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Cetak')).'&nbsp;&nbsp;'.
    anchor('ajbk/jabatan/delete/'.$d[COL_KD_JABATAN],'<i class="fa fa-trash text-danger"></i>', array('class'=>'cekboxaction','data-toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Hapus')),
    $d[COL_NM_JABATAN],
    $d[COL_KELASJAB],
    //$d[COL_KD_TYPE]==JABATAN_TYPE_STRUKTURAL?'Struktural':($d[COL_KD_TYPE]==JABATAN_TYPE_FUNGSIONAL?'Fungsional':'??'),
    '<ul class="m-0 pl-3 text-sm">'.$txtUnit.'</ul>',
    number_format($d["Uraian"], 0),
    number_format($d["Beban"], 0),
    number_format($d["Pegawai"], 2)
  );
    $i++;
}
$data = json_encode($res);
 ?>
 <form id="form-partial" method="post" action="#">
   <table id="list-partial" class="table table-bordered table-hover">

   </table>
 </form>
 <div class="modal fade" id="modal-print" tabindex="-1" role="dialog">
   <div class="modal-dialog modal-sm">
     <div class="modal-content">
       <div class="modal-header">
         <h5 class="modal-title">Cetak Informasi Jabatan</h5>
         <button type="button" class="close" data-dismiss="modal" aria-label="Close">
           <span aria-hidden="true"><i class="fas fa-close"></i></span>
         </button>
       </div>
       <div class="modal-body">
         <div class="row">
           <div class="col-sm-12">
             <div class="form-group">
               <label>Pilih Tahun</label>
               <select name=<?=COL_TAHUN?> class="form-control" style="width: 100% !important">
                 <?php
                 for($i=date('Y')-5; $i<=date('Y')+5; $i++) {
                   ?>
                   <option value="<?=$i?>" <?=$i==date('Y')?'selected':''?>><?=$i?></option>
                   <?php
                 }
                 ?>
               </select>
             </div>
           </div>
         </div>
       </div>
       <div class="modal-footer d-block">
         <div class="row">
           <div class="col-lg-12 text-center">
             <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="far fa-times-circle"></i>&nbsp;BATAL</button>
             <a href="" target="_blank" class="btn btn-success btn-submit"><i class="far fa-print"></i>&nbsp;CETAK</a>
           </div>
         </div>
       </div>
     </div>
   </div>
 </div>
 <script type="text/javascript">
 $(document).ready(function() {
   var dataTable = $('#list-partial').dataTable({
     "autoWidth" : false,
     "aaData": <?=$data?>,
     "scrollY" : '40vh',
     "scrollX": "120%",
     "iDisplayLength": 100,
     "aLengthMenu": [[100, 1000, 5000, -1], [100, 1000, 5000, "Semua"]],
     "dom":"R<'row'<'col-sm-4'l><'col-sm-8'f>><'row'<'col-sm-12'tr>><'row'<'col-sm-5'i><'col-sm-7'p>>",
     "buttons": ['copyHtml5','excelHtml5','csvHtml5','pdfHtml5'],
     "ordering": false,
     "columnDefs":[
       {targets: [5,6], className:'dt-body-right'},
       {targets: [0,3], className:'nowrap'},
       {targets: [2,4], className:'dt-body-center'}
     ],
     "aoColumns": [
        {"sTitle": "#","width": "60px"},
        {"sTitle": "Jabatan"},
        {"sTitle": "Kelas Jabatan"},
        //{"sTitle": "Tipe"},
        {"sTitle": "Unit Kerja"},
        {"sTitle": "Uraian Tugas"},
        {"sTitle": "ABK <small>(Jam / Tahun)</small>"},
        {"sTitle": "ABK <small>(Pegawai)</small>"}
     ],
     "fnCreatedRow" : function( nRow, aData, iDataIndex) {
       $('.btn-print', $(nRow)).click(function(){
         var a = $(this);
         var modalPrint = $("#modal-print");

         modalPrint.modal("show");
         $('a.btn-submit', modalPrint).attr('href', a.attr('href'));
         $('[name=Tahun]', modalPrint).change(function(){
           var th = $(this).val();
           $('a.btn-submit', modalPrint).attr('href', (a.attr('href')+'?Tahun='+th));
         }).trigger('change');
         return false;
       });
       $('.btn-duplicate', $(nRow)).click(function(){
         var a = $(this);
         var name = $(this).data('name');
         var modalDuplicate = $("#modal-duplicate");

         modalDuplicate.modal("show");
         $('form', modalDuplicate).attr('action', a.attr('href'));
         $('[name=Nm_Jabatan]', modalDuplicate).val(name);
         return false;
       });
       $('.cekboxaction', $(nRow)).click(function(){
           var a = $(this);
           var confirmDialog = $("#confirmDialog");

           confirmDialog.on("hidden.bs.modal", function(){
               $(".modal-body", confirmDialog).html("");
           });

           $(".modal-body", confirmDialog).html('Yakin ingin menghapus informasi jabatan ini?<br  />NB : <span class="text-danger">Data bezetting yang sudah diinput atas jabatan ini akan ikut terhapus.</span>');
           confirmDialog.modal("show");
           $(".btn-ok", confirmDialog).unbind('click').click(function() {
             var thisBtn = $(this);
               thisBtn.html("Loading...").attr("disabled", true);
               $('#form-partial').ajaxSubmit({
                   dataType: 'json',
                   url : a.attr('href'),
                   success : function(data){
                       if(data.error==0){
                           RefreshData();
                       }else{
                           toastr.error(data.error);
                       }
                   },
                   error : function() {
                     toastr.error('Server error.');
                   },
                   complete: function(){
                       thisBtn.html("OK").attr("disabled", false);
                       confirmDialog.modal("hide");
                   }
               });
           });
           return false;
       });
     }
   });
 });
 </script>
