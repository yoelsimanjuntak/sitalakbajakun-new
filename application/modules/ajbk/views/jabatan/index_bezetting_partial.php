<?php
$data = array();
$i = 0;
foreach ($res as $d) {
  $txtUnit = '<li style="list-style-type: disclosure-closed">'.$d[COL_NM_SUB_UNIT].'</li>';
  if(!empty($d[COL_NM_BID])) {
    $txtUnit .= '<li style="list-style-type: disclosure-closed">'.$d[COL_NM_BID].'</li>';
  }
  if(!empty($d[COL_NM_SUBBID])) {
    $txtUnit .= '<li style="list-style-type: disclosure-closed">'.$d[COL_NM_SUBBID].'</li>';
  }

  $res[$i] = array(
    $d[COL_NM_JABATAN],
    //$d[COL_ID_JABATAN],
    //$d[COL_KD_TYPE]==JABATAN_TYPE_STRUKTURAL?'Struktural':($d[COL_KD_TYPE]==JABATAN_TYPE_FUNGSIONAL?'Fungsional':'??'),
    '<ul class="m-0 pl-3 text-sm">'.$txtUnit.'</ul>',
    number_format($d["Pegawai"], 0),
    $noaction ? ($d["Bezetting"]!=null?number_format($d["Bezetting"], 0):'(belum diisi)') : anchor('ajbk/jabatan/set-bezetting/'.$d[COL_KD_JABATAN],($d["Bezetting"]!=null?number_format($d["Bezetting"], 0):'(belum diisi)'),array('class' => 'modal-popup-edit', 'data-jumlah' => $d["Bezetting"], 'data-tahun'=>$d[COL_TAHUN])),
  );
  $i++;
}
$data = json_encode($res);
 ?>
 <form id="form-bezetting-partial" method="post" action="#">
   <table id="list-bezetting-partial" class="table table-bordered table-hover">

   </table>
 </form>
 <div class="modal fade" id="modal-editor" tabindex="-1" role="dialog">
   <div class="modal-dialog">
     <div class="modal-content">
       <div class="modal-header">
         <h4 class="modal-title">Atur Bezetting</h4>
         <button type="button" class="close" data-dismiss="modal" aria-label="Close">
           <span aria-hidden="true"><i class="fa fa-close"></i></span>
         </button>
       </div>
       <div class="modal-body">
         <form id="form-editor" method="post" action="#">
           <div class="form-group">
               <label>Tahun</label>
               <div class="input-group">
                 <input type="number" class="form-control" name="<?=COL_TAHUN?>" value="<?=date('Y')?>" required />
               </div>
           </div>
           <div class="form-group">
               <label>Jumlah</label>
               <div class="input-group">
                 <input type="text" class="form-control uang" name="<?=COL_JLH_PEGAWAI?>" required />
               </div>
           </div>
         </form>
       </div>
       <div class="modal-footer">
         <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Cancel</button>
         <button type="button" class="btn btn-primary btn-flat btn-ok">Simpan</button>
       </div>
     </div>
   </div>
 </div>
 <script type="text/javascript">
     $(document).ready(function() {
         var dataTable = $('#list-bezetting-partial').dataTable({
           "autoWidth" : false,
           "aaData": <?=$data?>,
           "scrollY" : '40vh',
           "scrollX": "120%",
           "iDisplayLength": 100,
           "aLengthMenu": [[100, 1000, 5000, -1], [100, 1000, 5000, "Semua"]],
           "dom":"R<'row'<'col-sm-4'l><'col-sm-8'f>><'row'<'col-sm-12'tr>><'row'<'col-sm-5'i><'col-sm-7'p>>",
           "buttons": ['copyHtml5','excelHtml5','csvHtml5','pdfHtml5'],
           "ordering": false,
           "columnDefs":[
             {targets: [2,3], className:'dt-body-center nowrap'},
             {targets: [1], className:'nowrap'}
           ],
           "aoColumns": [
              {"sTitle": "Jabatan"},
              //{"sTitle": "Kode"},
              //{"sTitle": "Tipe"},
              {"sTitle": "Unit Kerja"},
              {"sTitle": "ABK<br />(Pegawai)"},
              {"sTitle": "Bezetting"}
           ],
           "fnCreatedRow" : function( nRow, aData, iDataIndex) {
             $('.modal-popup, .modal-popup-edit', $(nRow)).click(function(){
               var a = $(this);
               var bezetting = $(this).data('jumlah');
               var tahun = $(this).data('tahun');
               var editor = $("#modal-editor");

               $('[name=<?=COL_JLH_PEGAWAI?>]', editor).val(bezetting);
               if(tahun) $('[name=<?=COL_TAHUN?>]', editor).val(tahun);
               editor.modal("show");
               $(".btn-ok", editor).unbind('click').click(function() {
                 var dis = $(this);
                 dis.html("Loading...").attr("disabled", true);
                 $('#form-editor').ajaxSubmit({
                   dataType: 'json',
                   url : a.attr('href'),
                   success : function(data){
                     if(data.error==0){
                       toastr.success(data.success);
                     }else{
                       toastr.error(data.error);
                     }
                   },
                   error: function() {
                     toastr.error('Server error.');
                   },
                   complete: function() {
                     dis.html("Simpan").attr("disabled", false);
                     editor.modal("hide");
                     setTimeout(function(){ RefreshData(); }, 1000);
                   }
                 });
               });
               return false;
             });
           }
         });
     });
 </script>
