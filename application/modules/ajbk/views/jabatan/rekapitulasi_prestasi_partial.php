<?php
$data = array();
$i = 0;
$sumBeban=0;
$sumPegawai=0;
$sumBezetting=0;
foreach ($res as $d) {
  $txtUnit = '<li style="list-style-type: disclosure-closed">'.$d[COL_NM_SUB_UNIT].'</li>';
  if(!empty($d[COL_NM_BID])) {
    $txtUnit .= '<li style="list-style-type: disclosure-closed">'.$d[COL_NM_BID].'</li>';
  }
  if(!empty($d[COL_NM_SUBBID])) {
    $txtUnit .= '<li style="list-style-type: disclosure-closed">'.$d[COL_NM_SUBBID].'</li>';
  }
  $peg = $d["Beban"]/1350;
  $eff = round($peg)>0?$d["Beban"]/(round($peg)*1350):0;
  $eff1 = 'E';
  $eff2 = 'KURANG';
  if(round($eff,8)>=0.5) {
    $eff1 = 'D';
    $eff2 = 'SEDANG';
  }
  if(round($eff,8)>=0.7) {
    $eff1 = 'C';
    $eff2 = 'CUKUP';
  }
  if(round($eff,8)>=0.9) {
    $eff1 = 'B';
    $eff2 = 'BAIK';
  }
  if(round($eff,8)>1) {
    $eff1 = 'A';
    $eff2 = 'SANGAT BAIK';
  }
  $res[$i] = array(
    $d[COL_NM_JABATAN],
    //$d[COL_ID_JABATAN],
    //$d[COL_KD_TYPE]==JABATAN_TYPE_STRUKTURAL?'Struktural':($d[COL_KD_TYPE]==JABATAN_TYPE_FUNGSIONAL?'Fungsional':'??'),
    //$d[COL_NM_SUB_UNIT],
    //$d[COL_NM_BID],
    //$d[COL_NM_SUBBID],
    round($d["Beban"]),
    round($peg),
    ($d["Bezetting"]!=null?$d["Bezetting"]:'-'),
    ($d["Bezetting"]!=null?$d["Bezetting"]:0)-round($d["Pegawai"]),
    $eff!=0?round($eff,2):'-',
    $eff1,
    $eff2
  );

  $sumBeban+=$d["Beban"];
  $sumPegawai+=round($peg);
  $sumBezetting+=($d["Bezetting"]!=null?$d["Bezetting"]:0);
  $i++;
}
$data = json_encode($res);
 ?>
 <style>
 .tbl-ket th {
   padding : .5rem !important;
 }
 </style>
 <form id="form-bezetting-partial" method="post" action="#">
   <table id="list-bezetting-partial" class="table table-bordered table-hover">
     <thead>
       <tr>
         <th>Jabatan</th>
         <!--<th colspan="3" class="text-center" style="border-bottom-width: 0; border-right-width: 0">Unit Kerja</th>-->
         <th>Jlh. Beban Kerja</th>
         <th>Jlh. Kebutuhan</th>
         <th>Jlh. Pegawai</th>
         <th>+/-</th>
         <th>Efektivitas & Efisiensi Jabatan</th>
         <th>Prestasi Kerja</th>
         <th>Keterangan</th>
       </tr>
       <!--<tr>
         <th>Pratama</th>
         <th>Administrator</th>
         <th>Pengawas</th>
       </tr>-->
     </thead>
     <tfoot>
       <tr>
         <th class="text-right">TOTAL</th>
         <th class="text-right"><?=number_format($sumBeban)?></th>
         <th class="text-right"><?=number_format($sumPegawai)?></th>
         <th class="text-right"><?=$sumBezetting?></th>
         <th></th>
         <th></th>
         <th></th>
         <th></th>
       </tr>
     </tfoot>
   </table>
 </form>
 <div id="div-print" class="d-none">
   <a href="<?=site_url('ajbk/jabatan/rekapitulasi-prestasi-partial/-1/'.$kdOPD.'/-1/0/'.$tahun.'/1')?>" target="_blank" class="btn btn-success btn-sm"><i class="far fa-download"></i>&nbsp;DOWNLOAD</a>
 </div>
 <br />
 <div class="row">
   <div class="col-sm-6">
     <table class="table table-bordered text-sm tbl-ket">
       <thead>
         <tr>
           <th colspan="2">KETERANGAN</th>
         </tr>
         <tr>
           <th>NILAI EFISIENSI</th>
           <th style="width: 10px; white-space: nowrap">PRESTASI KERJA</th>
         </tr>
       </thead>
       <tbody>
         <tr>
           <td>> 1.00</td>
           <td style="width: 10px; white-space: nowrap">A</td>
         </tr>
         <tr>
           <td>0.90 - 1.00</td>
           <td style="width: 10px; white-space: nowrap">B</td>
         </tr>
         <tr>
           <td>0.70 - 0.89</td>
           <td style="width: 10px; white-space: nowrap">C</td>
         </tr>
         <tr>
           <td>0.50 - 0.69</td>
           <td style="width: 10px; white-space: nowrap">D</td>
         </tr>
         <tr>
           <td>< 0.50</td>
           <td style="width: 10px; white-space: nowrap">E</td>
         </tr>
       </tbody>
     </table>
   </div>
 </div>
 <script type="text/javascript">
 $(document).ready(function() {
   var dataTable = $('#list-bezetting-partial').dataTable({
     "autoWidth" : false,
     "aaData": <?=$data?>,
     "scrollY" : '40vh',
     "scrollX": "120%",
     "iDisplayLength": 100,
     "aLengthMenu": [[100, 1000, 5000, -1], [100, 1000, 5000, "Semua"]],
     //"dom":"R<'row'<'col-sm-4'l><'col-sm-4'B><'col-sm-4'f>><'row'<'col-sm-12'tr>><'row'<'col-sm-5'i><'col-sm-7'p>>",
     "dom":"R<'row'<'col-sm-4 div-print'><'col-sm-4 text-center'l><'col-sm-4'f>><'row'<'col-sm-12'tr>><'row'<'col-sm-5'i><'col-sm-7'p>>",
     "buttons": ['copyHtml5','excelHtml5','csvHtml5','pdfHtml5'],
     "ordering": false,
     "columnDefs":[
       {targets: [1,2,3,4,5], className:'dt-body-right nowrap'},
       {targets: [6], className:'dt-body-center nowrap'},
       //{targets: [1,2,3], className:'nowrap'},
       {targets: [1,2,3,4,5], width:'100px'},
       {targets: [0], width:'200px'},
     ],
     "fnCreatedRow" : function( nRow, aData, iDataIndex) {
       $('.modal-popup, .modal-popup-edit', $(nRow)).click(function(){
         var a = $(this);
         var bezetting = $(this).data('jumlah');
         var tahun = $(this).data('tahun');
         var editor = $("#modal-editor");

         $('[name=<?=COL_JLH_PEGAWAI?>]', editor).val(bezetting);
         if(tahun) $('[name=<?=COL_TAHUN?>]', editor).val(tahun);
         editor.modal("show");
         $(".btn-ok", editor).unbind('click').click(function() {
           var dis = $(this);
           dis.html("Loading...").attr("disabled", true);
           $('#form-editor').ajaxSubmit({
             dataType: 'json',
             url : a.attr('href'),
             success : function(data){
               if(data.error==0){
                 toastr.success(data.success);
               }else{
                 toastr.error(data.error);
               }
             },
             error: function() {
               toastr.error('Server error.');
             },
             complete: function() {
               dis.html("Simpan").attr("disabled", false);
               editor.modal("hide");
               setTimeout(function(){ RefreshData(); }, 1000);
             }
           });
         });
         return false;
       });
     }
   });
 });
 $("div.div-print").html($('#div-print').html());
 </script>
