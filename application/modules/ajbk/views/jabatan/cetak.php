<?php
$eff1 = 'E';
$eff2 = 'KURANG';
?>
<html>
<head>
  <title>Informasi Jabatan - <?=$data['NM_JAB']?></title>
  <style>
  body {
    font-family: 'Source Sans Pro','Helvetica Neue',Helvetica,Arial,sans-serif;
  }
  th, td {
    padding: 5px;
  }
  </style>
</head>
<body>
<table width="100%">
  <tr>
    <td colspan="2" style="text-align: center; vertical-align: middle">
      <h4 style="text-decoration: underline">INFORMASI JABATAN</h4>
    </td>
  </tr>
</table>
<br />
<table width="100%" border="0">
  <tr>
    <td style="vertical-align: top; width: 10px; font-weight: bold">1.</td>
    <td style="vertical-align: top; width: 200px; white-space: nowrap">NAMA JABATAN</td>
    <td style="vertical-align: top; width: 10px">:</td>
    <td style="vertical-align: top;"><?=$data['NM_JAB']?></td>
  </tr>
  <tr>
    <td style="vertical-align: top; width: 10px; font-weight: bold">2.</td>
    <td style="vertical-align: top; width: 200px; white-space: nowrap">KODE JABATAN</td>
    <td style="vertical-align: top; width: 10px">:</td>
    <td style="vertical-align: top;"><?=$data[COL_ID_JABATAN]?></td>
  </tr>
  <tr>
    <td style="vertical-align: top; width: 10px; font-weight: bold">3.</td>
    <td style="vertical-align: top; width: 200px; white-space: nowrap">UNIT KERJA</td>
    <td style="vertical-align: top; width: 10px">:</td>
    <td style="vertical-align: top;"></td>
  </tr>
  <tr>
    <td></td>
    <td style="vertical-align: top; width: 100px; white-space: nowrap"><strong>a.</strong> JPT Utama</td>
    <td style="vertical-align: top; width: 10px">:</td>
    <td></td>
  </tr>
  <tr>
    <td></td>
    <td style="vertical-align: top; width: 100px; white-space: nowrap"><strong>b.</strong> JPT Madya</td>
    <td style="vertical-align: top; width: 10px">:</td>
    <td><?=$data[COL_NM_SUB_UNIT]?></td>
  </tr>
  <tr>
    <td></td>
    <td style="vertical-align: top; width: 100px; white-space: nowrap"><strong>c.</strong> JPT Pratama</td>
    <td style="vertical-align: top; width: 10px">:</td>
    <td>PEMERINTAH KOTA TEBING TINGGI</td>
  </tr>

  <tr>
    <td></td>
    <td style="vertical-align: top; width: 100px; white-space: nowrap"><strong>d.</strong> Administrator</td>
    <td style="vertical-align: top; width: 10px">:</td>
    <td><?=($data[COL_NM_BID]?$data[COL_NM_BID]:'-')?></td>
  </tr>
  <tr>
    <td></td>
    <td style="vertical-align: top; width: 100px; white-space: nowrap"><strong>e.</strong> Pengawas</td>
    <td style="vertical-align: top; width: 10px">:</td>
    <td><?=($data[COL_NM_SUBBID]?$data[COL_NM_SUBBID]:'-')?></td>
  </tr>
  <tr>
    <td></td>
    <td style="vertical-align: top; width: 100px; white-space: nowrap"><strong>f.</strong> Pelaksana / Fungsional</td>
    <td style="vertical-align: top; width: 10px">:</td>
    <td><?=$data[COL_KD_TYPE]!='STR'?$data['NM_JAB']:'-'?></td>
  </tr>
  <tr>
    <td style="vertical-align: top; width: 10px; font-weight: bold">4.</td>
    <td style="vertical-align: top; width: 200px; white-space: nowrap">IKHTISAR JABATAN</td>
    <td style="vertical-align: top; width: 10px">:</td>
    <td style="vertical-align: top;"><?=$data[COL_NM_IKHTISAR]?></td>
  </tr>
  <tr>
    <td style="vertical-align: top; width: 10px; font-weight: bold">5.</td>
    <td style="vertical-align: top; width: 200px; white-space: nowrap">KUALIFIKASI JABATAN</td>
    <td style="vertical-align: top; width: 10px">:</td>
    <td style="vertical-align: top;"></td>
  </tr>
  <tr>
    <?php
    $htmlJurusan = '';
    $rjurusan = $this->db
    ->where(COL_KD_JABATAN, $data[COL_KD_JABATAN])
    ->join(TBL_AJBK_OPTION,TBL_AJBK_OPTION.'.'.COL_UNIQ." = ".TBL_AJBK_JABATAN_JURUSAN.".".COL_KD_JURUSAN,"left")
    ->get(TBL_AJBK_JABATAN_JURUSAN)
    ->result_array();
    foreach($rjurusan as $s) {
      $htmlJurusan .= $data['NM_EDU'].' - '.$s[COL_OPT_NAME].'<br />';
    }
    echo $htmlJurusan;
    ?>
    <!--<tr>
      <td></td>
      <td style="vertical-align: top; width: 100px; white-space: nowrap"><strong>a.</strong> Pendidikan Formal</td>
      <td style="vertical-align: top; width: 10px">:</td>
      <td><?=$data['NM_EDU']?></td>
    </tr>-->
    <td></td>
    <td style="vertical-align: top; width: 100px; white-space: nowrap"><strong>a.</strong> Pendidikan Formal</td>
    <td style="vertical-align: top; width: 10px">:</td>
    <td><?=(!empty($htmlJurusan)?$htmlJurusan:$data['NM_EDU'])?></td>
  </tr>
  <tr>
    <td></td>
    <td style="vertical-align: top; width: 100px; white-space: nowrap"><strong>b.</strong> Pendidikan & Pelatihan</td>
    <td style="vertical-align: top; width: 10px">:</td>
    <td>
      <?php
      $htmlDiklat = '';
      $rdiklat = $this->db
      ->where(COL_KD_JABATAN, $data[COL_KD_JABATAN])
      ->join(TBL_AJBK_OPTION,TBL_AJBK_OPTION.'.'.COL_UNIQ." = ".TBL_AJBK_JABATAN_DIKLAT.".".COL_KD_DIKLAT,"left")
      ->get(TBL_AJBK_JABATAN_DIKLAT)
      ->result_array();
      foreach($rdiklat as $s) {
        $htmlDiklat = $s[COL_OPT_NAME].'<br />';
      }
      echo $htmlDiklat;
      ?>
    </td>
  </tr>
  <tr>
    <td></td>
    <td style="vertical-align: top; width: 100px; white-space: nowrap"><strong>c.</strong> Pengalaman Kerja</td>
    <td style="vertical-align: top; width: 10px">:</td>
    <td><?=$data[COL_KD_PENGALAMAN]?></td>
  </tr>
  <tr>
    <td colspan="4"></td>
  </tr>
  <tr>
    <td style="vertical-align: top; width: 10px; font-weight: bold">6.</td>
    <td style="vertical-align: top; width: 200px; white-space: nowrap">TUGAS POKOK</td>
    <td style="vertical-align: top; width: 10px">:</td>
    <td style="vertical-align: top;"></td>
  </tr>
  <tr>
    <td></td>
    <td colspan="3" style="padding: 0px !important">
      <table style="font-size: 9pt; border: 1px solid #000; border-spacing: 0" border="1" width="100%">
        <tr>
          <td style="vertical-align: middle; width: 10px; font-weight: bold; text-align: center">NO.</td>
          <td style="vertical-align: middle; width: 300px; font-weight: bold; text-align: center">URAIAN TUGAS</td>
          <td style="vertical-align: middle; font-weight: bold; text-align: center">HASIL KERJA</td>
          <td style="vertical-align: middle; font-weight: bold; text-align: center">JUMLAH<br />HASIL</td>
          <td style="vertical-align: middle; font-weight: bold; text-align: center">WAKTU<br />SELESAI<br />(JAM)</td>
          <td style="vertical-align: middle; font-weight: bold; text-align: center">WAKTU<br />EFEKTIF<br />(JAM)</td>
          <td style="vertical-align: middle; font-weight: bold; text-align: center">KEBUTUHAN<br />PEGAWAI</td>
        </tr>
        <?php
        $det = $this->db
        ->select('*, unit.Opt_Name as NM_UNIT')
        ->where(COL_KD_JABATAN, $data[COL_KD_JABATAN])
        ->join(TBL_AJBK_OPTION.' unit','unit.'.COL_UNIQ." = ".TBL_AJBK_JABATAN_URAIAN.".".COL_KD_SATUAN,"left")
        ->get(TBL_AJBK_JABATAN_URAIAN)
        ->result_array();
        $i=1;
        $sumdet = 0;
        $sumbeban = 0;
        if(empty($det)) {
          echo '<tr><td colspan="7" style="text-align: center; font-style:italic">(kosong)</td></tr>';
        }
        foreach($det as $m) {
          ?>
          <tr>
            <td style="vertical-align: top; width: 10px; text-align: center"><?=$i?></td>
            <td style="vertical-align: top; width: 200px;"><?=$m[COL_NM_URAIAN]?></td>
            <td style="vertical-align: top;"><?=$m['NM_UNIT']?></td>
            <td style="vertical-align: top; text-align: center"><?=number_format($m[COL_JLH_BEBAN])?></td>
            <td style="vertical-align: top; text-align: center"><?=number_format($m[COL_JLH_JAM])?></td>
            <td style="vertical-align: top; text-align: center"><?=number_format($m[COL_JLH_JAM]*$m[COL_JLH_BEBAN])?></td>
            <td style="vertical-align: top; text-align: center"><?=number_format(($m[COL_JLH_JAM]*$m[COL_JLH_BEBAN])/HOURPERYEAR, 2)?></td>
          </tr>
          <?php
          $i++;
          $sumdet += (($m[COL_JLH_JAM]*$m[COL_JLH_BEBAN])/1350);
          $sumbeban += $m[COL_JLH_JAM]*$m[COL_JLH_BEBAN];
        }
        $peg = $sumbeban/1350;
        $eff = round($peg)>0?$sumbeban/(round($peg)*1350):0;

        //$peg = $d["Beban"]/1350;
        //$eff = round($peg)>0?$d["Beban"]/(round($peg)*1350):0;

        if(round($eff,8)>=0.5) {
          $eff1 = 'D';
          $eff2 = 'SEDANG';
        }
        if(round($eff,8)>=0.7) {
          $eff1 = 'C';
          $eff2 = 'CUKUP';
        }
        if(round($eff,8)>=0.9) {
          $eff1 = 'B';
          $eff2 = 'BAIK';
        }
        if(round($eff,8)>1) {
          $eff1 = 'A';
          $eff2 = 'SANGAT BAIK';
        }
        ?>
        <tr>
          <td colspan="5" rowspan="2" style="vertical-align: middle; width: 10px; font-weight: bold; text-align: right">TOTAL</td>
          <td rowspan="2" style="vertical-align: middle; width: 10px; font-weight: bold; text-align: center"><?=number_format($sumbeban, 2)?></td>
          <td style="vertical-align: top; width: 10px; font-weight: bold; text-align: center"><?=number_format($sumdet, 2)?></td>
        </tr>
        <tr>
          <td style="vertical-align: top; width: 10px; font-weight: bold; text-align: center; color: blue"><?=round($sumdet)?></td>
        </tr>
        <?php
        if(!empty($_GET['Tahun'])) {
          $rbezetting = $this->db
          ->where(COL_KD_JABATAN, $data[COL_KD_JABATAN])
          ->where(COL_TAHUN, $_GET['Tahun'])
          ->get(TBL_AJBK_JABATAN_BEZETTING)
          ->row_array();
          ?>
          <tr>
            <td colspan="6" style="vertical-align: middle; width: 10px; font-weight: bold; text-align: right">JLH. PEGAWAI TH. <?=$_GET['Tahun']?></td>
            <td style="vertical-align: top; width: 10px; font-weight: bold; text-align: center"><?=!empty($rbezetting)?number_format($rbezetting[COL_JLH_PEGAWAI]):'-'?></td>
          </tr>
          <?php
        }
        ?>
      </table>
    </td>
  </tr>
  <tr>
    <td colspan="4"></td>
  </tr>
  <tr>
    <td style="vertical-align: top; width: 10px; font-weight: bold">7.</td>
    <td style="vertical-align: top; width: 200px; white-space: nowrap">HASIL KERJA</td>
    <td style="vertical-align: top; width: 10px">:</td>
    <td style="vertical-align: top;"></td>
  </tr>
  <tr>
    <td></td>
    <td colspan="3" style="padding: 0px !important">
      <table style="font-size: 9pt; border: 1px solid #000; border-spacing: 0" border="1" width="100%">
        <tr>
          <td style="vertical-align: middle; width: 10px; font-weight: bold; text-align: center">NO.</td>
          <td style="vertical-align: middle; font-weight: bold; text-align: center">HASIL KERJA</td>
        </tr>
        <?php
        $detHasilKerja = json_decode($data[COL_NM_HASILKERJA]);
        $i=1;
        if(!empty($detHasilKerja) && count($detHasilKerja) > 0) {
          foreach($detHasilKerja as $m) {
            ?>
            <tr>
              <td style="vertical-align: top; width: 10px; text-align: center"><?=$i?>.</td>
              <td style="vertical-align: top;"><?=$m->HasilKerja?></td>
            </tr>
            <?php
            $i++;
          }
        } else {

        }
        ?>
      </table>
    </td>
  </tr>
  <tr>
    <td colspan="4"></td>
  </tr>
  <tr>
    <td style="vertical-align: top; width: 10px; font-weight: bold">8.</td>
    <td style="vertical-align: top; width: 200px; white-space: nowrap">BAHAN KERJA</td>
    <td style="vertical-align: top; width: 10px">:</td>
    <td style="vertical-align: top;"></td>
  </tr>
  <tr>
    <td></td>
    <td colspan="3" style="padding: 0px !important">
      <table style="font-size: 9pt; border: 1px solid #000; border-spacing: 0" border="1" width="100%">
        <tr>
          <td style="vertical-align: middle; width: 10px; font-weight: bold; text-align: center">NO.</td>
          <td style="vertical-align: middle; font-weight: bold; text-align: center">BAHAN KERJA</td>
          <td style="vertical-align: middle; font-weight: bold; text-align: center">PENGGUNAAN DALAM TUGAS</td>
        </tr>
        <?php
        $detBahan = json_decode($data[COL_NM_BAHANKERJA]);
        $i=1;
        if(empty($detBahan)) {
          echo '<tr><td colspan="3" style="text-align: center; font-style:italic">(kosong)</td></tr>';
        }
        foreach($detBahan as $m) {
          ?>
          <tr>
            <td style="vertical-align: top; width: 10px; text-align: center"><?=$i?></td>
            <td style="vertical-align: top;"><?=$m->BahanKerja?></td>
            <td style="vertical-align: top;"><?=$m->Kegunaan?></td>
          </tr>
          <?php
          $i++;
        }
        ?>
      </table>
    </td>
  </tr>
  <tr>
    <td colspan="4"></td>
  </tr>
  <tr>
    <td style="vertical-align: top; width: 10px; font-weight: bold">9.</td>
    <td style="vertical-align: top; width: 200px; white-space: nowrap">PERANGKAT KERJA</td>
    <td style="vertical-align: top; width: 10px">:</td>
    <td style="vertical-align: top;"></td>
  </tr>
  <tr>
    <td></td>
    <td colspan="3" style="padding: 0px !important">
      <table style="font-size: 9pt; border: 1px solid #000; border-spacing: 0" border="1" width="100%">
        <tr>
          <td style="vertical-align: middle; width: 10px; font-weight: bold; text-align: center">NO.</td>
          <td style="vertical-align: middle; font-weight: bold; text-align: center">PERANGKAT KERJA</td>
          <td style="vertical-align: middle; font-weight: bold; text-align: center">PENGGUNAAN UNTUK TUGAS</td>
        </tr>
        <?php
        $detPerangkat = json_decode($data[COL_NM_PERANGKATKERJA]);
        $i=1;
        if(empty($detPerangkat)) {
          echo '<tr><td colspan="3" style="text-align: center; font-style:italic">(kosong)</td></tr>';
        }
        foreach($detPerangkat as $m) {
          ?>
          <tr>
            <td style="vertical-align: top; width: 10px; text-align: center"><?=$i?></td>
            <td style="vertical-align: top;"><?=$m->PerangkatKerja?></td>
            <td style="vertical-align: top;"><?=$m->Kegunaan?></td>
          </tr>
          <?php
          $i++;
        }
        ?>
      </table>
    </td>
  </tr>
  <tr>
    <td colspan="4"></td>
  </tr>
  <tr>
    <td style="vertical-align: top; width: 10px; font-weight: bold">10.</td>
    <td style="vertical-align: top; width: 200px; white-space: nowrap">TANGGUNG JAWAB</td>
    <td style="vertical-align: top; width: 10px">:</td>
    <td style="vertical-align: top;"></td>
  </tr>
  <tr>
    <td></td>
    <td colspan="3" style="padding: 0px !important">
      <table style="font-size: 9pt; border: 1px solid #000; border-spacing: 0" border="1" width="100%">
        <tr>
          <td style="vertical-align: middle; width: 10px; font-weight: bold; text-align: center">NO.</td>
          <td style="vertical-align: middle; font-weight: bold; text-align: center">TANGGUNG JAWAB</td>
        </tr>
        <?php
        $detTanggungJawab = json_decode($data[COL_NM_TANGGUNGJAWAB]);
        $i=1;
        if(!empty($detTanggungJawab) && count($detTanggungJawab) > 0) {
          foreach($detTanggungJawab as $m) {
            ?>
            <tr>
              <td style="vertical-align: top; width: 10px; text-align: center"><?=$i?>.</td>
              <td style="vertical-align: top;"><?=$m->TanggungJawab?></td>
            </tr>
            <?php
            $i++;
          }
        } else {

        }
        ?>
      </table>
    </td>
  </tr>
  <tr>
    <td colspan="4"></td>
  </tr>
  <tr>
    <td style="vertical-align: top; width: 10px; font-weight: bold">11.</td>
    <td style="vertical-align: top; width: 200px; white-space: nowrap">WEWENANG</td>
    <td style="vertical-align: top; width: 10px">:</td>
    <td style="vertical-align: top;"></td>
  </tr>
  <tr>
    <td></td>
    <td colspan="3" style="padding: 0px !important">
      <table style="font-size: 9pt; border: 1px solid #000; border-spacing: 0" border="1" width="100%">
        <tr>
          <td style="vertical-align: middle; width: 10px; font-weight: bold; text-align: center">NO.</td>
          <td style="vertical-align: middle; font-weight: bold; text-align: center">WEWENANG</td>
        </tr>
        <?php
        $detWewenang = json_decode($data[COL_NM_WEWENANG]);
        $i=1;
        foreach($detWewenang as $m) {
          ?>
          <tr>
            <td style="vertical-align: top; width: 10px; text-align: center"><?=$i?>.</td>
            <td style="vertical-align: top;"><?=$m->Wewenang?></td>
          </tr>
          <?php
          $i++;
        }
        ?>
      </table>
    </td>
  </tr>
  <tr>
    <td colspan="4"></td>
  </tr>
  <tr>
    <td style="vertical-align: top; width: 10px; font-weight: bold">12.</td>
    <td style="vertical-align: top; width: 200px; white-space: nowrap">KORELASI JABATAN</td>
    <td style="vertical-align: top; width: 10px">:</td>
    <td style="vertical-align: top;"></td>
  </tr>
  <tr>
    <td></td>
    <td colspan="3" style="padding: 0px !important">
      <table style="font-size: 9pt; border: 1px solid #000; border-spacing: 0" border="1" width="100%">
        <tr>
          <td style="vertical-align: middle; width: 10px; font-weight: bold; text-align: center">NO.</td>
          <td style="vertical-align: middle; font-weight: bold; text-align: center">NAMA JABATAN</td>
          <td style="vertical-align: middle; font-weight: bold; text-align: center">UNIT KERJA / INSTANSI</td>
          <td style="vertical-align: middle; font-weight: bold; text-align: center">DALAM HAL</td>
        </tr>
        <?php
        $detKorelasi = json_decode($data[COL_NM_KORELASI]);
        $i=1;
        if(empty($detKorelasi)) {
          echo '<tr><td colspan="4" style="text-align: center; font-style:italic">(kosong)</td></tr>';
        }
        foreach($detKorelasi as $m) {
          ?>
          <tr>
            <td style="vertical-align: top; width: 10px; text-align: center"><?=$i?></td>
            <td style="vertical-align: top;"><?=$m->Jabatan?></td>
            <td style="vertical-align: top;"><?=$m->Instansi?></td>
            <td style="vertical-align: top;"><?=isset($m->Hal)?$m->Hal:'-'?></td>
          </tr>
          <?php
          $i++;
        }
        ?>
      </table>
    </td>
  </tr>
  <tr>
    <td colspan="4"></td>
  </tr>
  <tr>
    <td style="vertical-align: top; width: 10px; font-weight: bold">13.</td>
    <td style="vertical-align: top; width: 200px; white-space: nowrap">KONDISI LINGKUNGAN KERJA</td>
    <td style="vertical-align: top; width: 10px">:</td>
    <td style="vertical-align: top;"></td>
  </tr>
  <tr>
    <td></td>
    <td colspan="3" style="padding: 0px !important">
      <?php
      $detLingkungan = json_decode($data[COL_NM_KONDISI]);
      if(!empty($detLingkungan)) {
        ?>
        <table style="font-size: 9pt; border: 1px solid #000; border-spacing: 0" border="1" width="100%">
          <tr>
            <td style="vertical-align: middle; width: 10px; font-weight: bold; text-align: center">NO.</td>
            <td style="vertical-align: middle; font-weight: bold; text-align: center">ASPEK</td>
            <td style="vertical-align: middle; font-weight: bold; text-align: center">FAKTOR</td>
          </tr>
          <tr>
            <td style="vertical-align: top; width: 10px; text-align: center">a.</td>
            <td style="vertical-align: top; width: 200px;">Tempat Kerja</td>
            <td style="vertical-align: top;">
              <?php
              $ropt = $this->db->where(COL_UNIQ, $detLingkungan->TempatKerja)->get(TBL_AJBK_OPTION)->row_array();
              if(!empty($ropt)) {
                echo $ropt[COL_OPT_NAME];
              }
              ?>
            </td>
          </tr>
          <tr>
            <td style="vertical-align: top; width: 10px; text-align: center">b.</td>
            <td style="vertical-align: top; width: 200px;">Suhu</td>
            <td style="vertical-align: top;">
              <?php
              $ropt = $this->db->where(COL_UNIQ, $detLingkungan->Suhu)->get(TBL_AJBK_OPTION)->row_array();
              if(!empty($ropt)) {
                echo $ropt[COL_OPT_NAME];
              }
              ?>
            </td>
          </tr>
          <tr>
            <td style="vertical-align: top; width: 10px; text-align: center">c.</td>
            <td style="vertical-align: top; width: 200px;">Udara</td>
            <td style="vertical-align: top;">
              <?php
              $ropt = $this->db->where(COL_UNIQ, $detLingkungan->Udara)->get(TBL_AJBK_OPTION)->row_array();
              if(!empty($ropt)) {
                echo $ropt[COL_OPT_NAME];
              }
              ?>
            </td>
          </tr>
          <tr>
            <td style="vertical-align: top; width: 10px; text-align: center">d.</td>
            <td style="vertical-align: top; width: 200px;">Keadaan Ruangan</td>
            <td style="vertical-align: top;">
              <?php
              $ropt = $this->db->where(COL_UNIQ, $detLingkungan->KeadaanRuangan)->get(TBL_AJBK_OPTION)->row_array();
              if(!empty($ropt)) {
                echo $ropt[COL_OPT_NAME];
              }
              ?>
            </td>
          </tr>
          <tr>
            <td style="vertical-align: top; width: 10px; text-align: center">e.</td>
            <td style="vertical-align: top; width: 200px;">Letak</td>
            <td style="vertical-align: top;">
              <?php
              $ropt = $this->db->where(COL_UNIQ, $detLingkungan->Letak)->get(TBL_AJBK_OPTION)->row_array();
              if(!empty($ropt)) {
                echo $ropt[COL_OPT_NAME];
              }
              ?>
            </td>
          </tr>
          <tr>
            <td style="vertical-align: top; width: 10px; text-align: center">f.</td>
            <td style="vertical-align: top; width: 200px;">Penerangan</td>
            <td style="vertical-align: top;">
              <?php
              $ropt = $this->db->where(COL_UNIQ, $detLingkungan->Penerangan)->get(TBL_AJBK_OPTION)->row_array();
              if(!empty($ropt)) {
                echo $ropt[COL_OPT_NAME];
              }
              ?>
            </td>
          </tr>
          <tr>
            <td style="vertical-align: top; width: 10px; text-align: center">g.</td>
            <td style="vertical-align: top; width: 200px;">Suara</td>
            <td style="vertical-align: top;">
              <?php
              $ropt = $this->db->where(COL_UNIQ, $detLingkungan->Suara)->get(TBL_AJBK_OPTION)->row_array();
              if(!empty($ropt)) {
                echo $ropt[COL_OPT_NAME];
              }
              ?>
            </td>
          </tr>
          <tr>
            <td style="vertical-align: top; width: 10px; text-align: center">h.</td>
            <td style="vertical-align: top; width: 200px;">Getaran</td>
            <td style="vertical-align: top;">
              <?php
              $ropt = $this->db->where(COL_UNIQ, $detLingkungan->Getaran)->get(TBL_AJBK_OPTION)->row_array();
              if(!empty($ropt)) {
                echo $ropt[COL_OPT_NAME];
              }
              ?>
            </td>
          </tr>
        </table>
        <?php
      }
      ?>
    </td>
  </tr>
  <tr>
    <td colspan="4"></td>
  </tr>
  <tr>
    <td style="vertical-align: top; width: 10px; font-weight: bold">14.</td>
    <td style="vertical-align: top; width: 200px; white-space: nowrap">RESIKO BAHAYA</td>
    <td style="vertical-align: top; width: 10px">:</td>
    <td style="vertical-align: top;"></td>
  </tr>
  <tr>
    <td></td>
    <td colspan="3" style="padding: 0px !important">
      <table style="font-size: 9pt; border: 1px solid #000; border-spacing: 0" border="1" width="100%">
        <tr>
          <td style="vertical-align: middle; width: 10px; font-weight: bold; text-align: center">NO.</td>
          <td style="vertical-align: middle; font-weight: bold; text-align: center">NAMA RESIKO</td>
          <td style="vertical-align: middle; font-weight: bold; text-align: center">PENYEBAB</td>
        </tr>
        <?php
        $detResiko = json_decode($data[COL_NM_RESIKO]);
        $i=1;
        if(empty($detResiko)) {
          echo '<tr><td colspan="3" style="text-align: center; font-style:italic">(kosong)</td></tr>';
        }
        foreach($detResiko as $m) {
          ?>
          <tr>
            <td style="vertical-align: top; width: 10px; text-align: center"><?=$i?></td>
            <td style="vertical-align: top;"><?=$m->Aspek?></td>
            <td style="vertical-align: top;"><?=$m->Penyebab?></td>
          </tr>
          <?php
          $i++;
        }
        ?>
      </table>
    </td>
  </tr>
  <tr>
    <td colspan="4"></td>
  </tr>
  <tr>
    <td style="vertical-align: top; width: 10px; font-weight: bold">15.</td>
    <td style="vertical-align: top; width: 200px; white-space: nowrap" colspan="3">SYARAT JABATAN</td>
  </tr>
  <tr>
    <td style="vertical-align: top; width: 10px; font-weight: bold"></td>
    <td style="vertical-align: top; width: 200px; white-space: nowrap">Keterampilan Kerja</td>
    <td style="vertical-align: top; width: 10px">:</td>
    <td style="vertical-align: top;"><?=$data[COL_NM_KETERAMPILAN]?></td>
  </tr>
  <tr>
    <td style="vertical-align: top; width: 10px; font-weight: bold"></td>
    <td style="vertical-align: top; width: 200px; white-space: nowrap">Bakat Kerja</td>
    <td style="vertical-align: top; width: 10px">:</td>
    <td style="vertical-align: top;">
      <ol>
        <?php
        $detBakat = json_decode($data[COL_NM_BAKAT]);
        if($detBakat) {
          foreach ($detBakat as $d) {
            $rbakat = $this->db->where(COL_UNIQ,$d)->get(TBL_AJBK_OPTION)->row_array();
            if($rbakat) {
              echo '<li>'.$rbakat[COL_OPT_DESC].'</li>';
            }
          }
        }
        ?>
      </ol>
    </td>
  </tr>
  <tr>
    <td style="vertical-align: top; width: 10px; font-weight: bold"></td>
    <td style="vertical-align: top; width: 200px; white-space: nowrap">Tempramen Kerja</td>
    <td style="vertical-align: top; width: 10px">:</td>
    <td style="vertical-align: top;">
      <ol>
        <?php
        $detTemp = json_decode($data[COL_NM_TEMPRAMEN]);
        if($detTemp) {
          foreach ($detTemp as $d) {
            $rtemp = $this->db->where(COL_UNIQ,$d)->get(TBL_AJBK_OPTION)->row_array();
            if($rtemp) {
              echo '<li>'.$rtemp[COL_OPT_DESC].'</li>';
            }
          }
        }
        ?>
      </ol>
    </td>
  </tr>
  <tr>
    <td style="vertical-align: top; width: 10px; font-weight: bold"></td>
    <td style="vertical-align: top; width: 200px; white-space: nowrap">Minat Kerja</td>
    <td style="vertical-align: top; width: 10px">:</td>
    <td style="vertical-align: top;">
      <ol>
        <?php
        $detMinat = json_decode($data[COL_NM_MINAT]);
        if($detMinat) {
          foreach ($detMinat as $d) {
            $rminat = $this->db->where(COL_UNIQ,$d)->get(TBL_AJBK_OPTION)->row_array();
            if($rminat) {
              echo '<li>'.$rminat[COL_OPT_DESC].'</li>';
            }
          }
        }
        ?>
      </ol>
    </td>
  </tr>
  <tr>
    <td style="vertical-align: top; width: 10px; font-weight: bold"></td>
    <td style="vertical-align: top; width: 200px; white-space: nowrap">Upaya Fisik</td>
    <td style="vertical-align: top; width: 10px">:</td>
    <td style="vertical-align: top; white-space: nowrap">
      <ol>
        <?php
        $detFisik = json_decode($data[COL_NM_UPAYAFISIK]);
        if($detFisik) {
          foreach ($detFisik as $d) {
            $rfisik = $this->db->where(COL_UNIQ,$d)->get(TBL_AJBK_OPTION)->row_array();
            if($rfisik) {
              echo '<li>'.$rfisik[COL_OPT_DESC].'</li>';
            }
          }
        }
        ?>
      </ol>
    </td>
  </tr>
  <tr>
    <td style="vertical-align: top; width: 10px; font-weight: bold"></td>
    <td style="vertical-align: top; width: 200px; white-space: nowrap">Kondisi Fisik</td>
    <td style="vertical-align: top; width: 10px">:</td>
    <td style="vertical-align: top; padding: 0 !important">
      <?php
      $detKondFisik = json_decode($data[COL_NM_KONDISIFISIK]);
      $jkelamin = '-';
      if($detKondFisik) {
        $rkelamin = $this->db->where(COL_UNIQ,$detKondFisik->JenisKelamin)->get(TBL_AJBK_OPTION)->row_array();
        if($rkelamin) {
          $jkelamin = $rkelamin[COL_OPT_NAME];
        }
      }
      ?>
      <table>
        <tr>
          <td style="padding: 0 5px !important; vertical-align: top; white-space: nowrap">Jenis Kelamin</td>
          <td style="padding: 0 5px !important; vertical-align: top; width: 10px">:</td>
          <td style="padding: 0 5px !important; vertical-align: top; white-space: nowrap"><?=$jkelamin?></td>
        </tr>
        <tr>
          <td style="padding: 0 5px !important; vertical-align: top; white-space: nowrap">Umur</td>
          <td style="padding: 0 5px !important; vertical-align: top; width: 10px">:</td>
          <td style="padding: 0 5px !important; vertical-align: top; white-space: nowrap"><?=!empty($detKondFisik)?$detKondFisik->Umur:'-'?></td>
        </tr>
        <tr>
          <td style="padding: 0 5px !important; vertical-align: top; white-space: nowrap">Tinggi Badan</td>
          <td style="padding: 0 5px !important; vertical-align: top; width: 10px">:</td>
          <td style="padding: 0 5px !important; vertical-align: top; white-space: nowrap"><?=!empty($detKondFisik)?$detKondFisik->TinggiBadan:'-'?></td>
        </tr>
        <tr>
          <td style="padding: 0 5px !important; vertical-align: top; white-space: nowrap">Berat Badan</td>
          <td style="padding: 0 5px !important; vertical-align: top; width: 10px">:</td>
          <td style="padding: 0 5px !important; vertical-align: top; white-space: nowrap"><?=!empty($detKondFisik)?$detKondFisik->BeratBadan:'-'?></td>
        </tr>
        <tr>
          <td style="padding: 0 5px !important; vertical-align: top; white-space: nowrap">Postur Badan</td>
          <td style="padding: 0 5px !important; vertical-align: top; width: 10px">:</td>
          <td style="padding: 0 5px !important; vertical-align: top; white-space: nowrap"><?=!empty($detKondFisik)?$detKondFisik->PosturBadan:'-'?></td>
        </tr>
        <tr>
          <td style="padding: 0 5px !important; vertical-align: top; white-space: nowrap">Penampilan</td>
          <td style="padding: 0 5px !important; vertical-align: top; width: 10px">:</td>
          <td style="padding: 0 5px !important; vertical-align: top; white-space: nowrap"><?=!empty($detKondFisik)?$detKondFisik->Penampilan:'-'?></td>
        </tr>
      </table>
    </td>
  </tr>
  <tr>
    <td style="vertical-align: top; width: 10px; font-weight: bold"></td>
    <td style="vertical-align: top; width: 200px; white-space: nowrap">Fungsi Pekerjaan</td>
    <td style="vertical-align: top; width: 10px">:</td>
    <td style="vertical-align: top;"><?=$data[COL_NM_FUNGSIPEKERJAAN]?></td>
  </tr>
  <tr>
    <td style="vertical-align: top; width: 10px; font-weight: bold">16.</td>
    <td style="vertical-align: top; width: 200px; white-space: nowrap">PRESTASI KERJA<br />YANG DIHARAPKAN</td>
    <td style="vertical-align: top; width: 10px">:</td>
    <td style="vertical-align: top;"><?=$eff1.' ('.$eff2.')'?></td>
  </tr>
  <tr>
    <td style="vertical-align: top; width: 10px; font-weight: bold">17.</td>
    <td style="vertical-align: top; width: 200px; white-space: nowrap">KELAS JABATAN</td>
    <td style="vertical-align: top; width: 10px">:</td>
    <td style="vertical-align: top;"><?=!empty($data[COL_KELASJAB])?$data[COL_KELASJAB]:'-'?></td>
  </tr>
</table>
<?php
if(!empty($data[COL_VERIFIED])&&$data[COL_VERIFIED]==1) {
  ?>
  <table style="margin-top: 50px">
    <tr>
      <td style="font-size: 10px; font-style: italic">
        Dokumen ini telah diverifikasi pada:<br />Tanggal <strong><?=date('d-m-Y', strtotime($data[COL_VERIFIED_ON]))?></strong> <br />Pukul <strong><?=date('H:i', strtotime($data[COL_VERIFIED_ON]))?></strong> <br />Oleh <strong><?=$data['NmVerified']?></strong></strong>
      </td>
    </tr>
  </table>
  <?php
}
?>
</body>
