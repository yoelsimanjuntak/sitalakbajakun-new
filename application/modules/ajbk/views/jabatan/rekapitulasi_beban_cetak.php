<?php
header("Content-type: application/vnd-ms-excel");
header("Content-Disposition: attachment; filename=SITALAKBAJAKUN - Rekapitulasi Beban Kerja ".date('YmdHi').".xls");

$data = array();
$i = 0;
$sumBeban=0;
$sumPegawai=0;
$sumBezetting=0;
$sumResiko=0;
 ?>
 <table id="list-bezetting-partial" class="table table-bordered table-hover" border="1">
   <thead>
     <tr>
       <th rowspan="2">Jabatan</th>
       <th colspan="3" class="text-center" style="border-bottom-width: 0; border-right-width: 0">Unit Kerja</th>
       <th rowspan="2" style="border-left-width: 1px">ABK (Jam)</th>
       <th rowspan="2">ABK (Pegawai)</th>
       <th rowspan="2">Bezetting</th>
       <th rowspan="2">Resiko Kerja</th>
     </tr>
     <tr>
       <th>Pratama</th>
       <th>Administrator</th>
       <th>Pengawas</th>
     </tr>
   </thead>
   <tbody>
     <?php
     foreach($res as $d) {
       $arrResiko = !empty($d[COL_NM_RESIKO])?json_decode($d[COL_NM_RESIKO]):[];
       ?>
       <tr>
         <td><?=$d[COL_NM_JABATAN]?></td>
         <td><?=$d[COL_NM_SUB_UNIT]?></td>
         <td><?=$d[COL_NM_BID]?></td>
         <td><?=$d[COL_NM_SUBBID]?></td>
         <td><?=round($d["Beban"])?></td>
         <td><?=$d["Pegawai"]?></td>
         <td><?=($d["Bezetting"]!=null?$d["Bezetting"]:'-')?></td>
         <td><?=number_format(count($arrResiko))?></td>
       </tr>
       <?php
       $sumBeban+=$d["Beban"];
       $sumPegawai+=$d["Pegawai"];
       $sumBezetting+=($d["Bezetting"]!=null?$d["Bezetting"]:0);
       $sumResiko+=count($arrResiko);
     }
     ?>
   </tbody>
   <tfoot>
     <tr>
       <th colspan="4" class="text-right">TOTAL</th>
       <th class="text-right"><?=round($sumBeban)?></th>
       <th class="text-right"><?=$sumPegawai?></th>
       <th class="text-right"><?=$sumBezetting?></th>
       <th class="text-right"><?=$sumResiko?></th>
     </tr>
   </tfoot>
 </table>
