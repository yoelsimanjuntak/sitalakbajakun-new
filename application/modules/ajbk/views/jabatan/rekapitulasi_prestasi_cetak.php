<?php
header("Content-type: application/vnd-ms-excel");
header("Content-Disposition: attachment; filename=SITALAKBAJAKUN - Rekapitulasi Prestasi Kerja ".date('YmdHi').".xls");

$data = array();
$i = 0;
$sumBeban=0;
$sumPegawai=0;
$sumBezetting=0;
 ?>
 <table id="list-prestasi-partial" class="table table-bordered table-hover" border="1">
   <thead>
     <tr>
       <th>Jabatan</th>
       <th>Jlh. Beban Kerja</th>
       <th>Jlh. Kebutuhan</th>
       <th>Jlh. Pegawai</th>
       <th>+/-</th>
       <th>Efektivitas & Efisiensi Jabatan</th>
       <th>Prestasi Kerja</th>
       <th>Keterangan</th>
     </tr>
   </thead>
   <tbody>
     <?php
     foreach($res as $d) {
       $txtUnit = '<li style="list-style-type: disclosure-closed">'.$d[COL_NM_SUB_UNIT].'</li>';
       if(!empty($d[COL_NM_BID])) {
         $txtUnit .= '<li style="list-style-type: disclosure-closed">'.$d[COL_NM_BID].'</li>';
       }
       if(!empty($d[COL_NM_SUBBID])) {
         $txtUnit .= '<li style="list-style-type: disclosure-closed">'.$d[COL_NM_SUBBID].'</li>';
       }
       $peg = $d["Beban"]/1350;
       $eff = round($peg)>0?$d["Beban"]/(round($peg)*1350):0;
       $eff1 = 'E';
       $eff2 = 'KURANG';
       if(round($eff,8)>=0.5) {
         $eff1 = 'D';
         $eff2 = 'SEDANG';
       }
       if(round($eff,8)>=0.7) {
         $eff1 = 'C';
         $eff2 = 'CUKUP';
       }
       if(round($eff,8)>=0.9) {
         $eff1 = 'B';
         $eff2 = 'BAIK';
       }
       if(round($eff,8)>1) {
         $eff1 = 'A';
         $eff2 = 'SANGAT BAIK';
       }
       ?>
       <tr>
         <td><?=$d[COL_NM_JABATAN]?></td>
         <td><?=round($d["Beban"])?></td>
         <td><?=round($peg)?></td>
         <td><?=($d["Bezetting"]!=null?$d["Bezetting"]:'-')?></td>
         <td><?=($d["Bezetting"]!=null?$d["Bezetting"]:0)-round($d["Pegawai"])?></td>
         <td><?=$eff!=0?round($eff,2):'-'?></td>
         <td><?=$eff1?></td>
         <td><?=$eff2?></td>
       </tr>
       <?php
       $sumBeban+=$d["Beban"];
       $sumPegawai+=round($peg);
       $sumBezetting+=($d["Bezetting"]!=null?$d["Bezetting"]:0);
       $i++;
     }
     ?>
   </tbody>
   <tfoot>
     <tr>
       <th>TOTAL</th>
       <th class="text-right"><?=round($sumBeban)?></th>
       <th class="text-right"><?=$sumPegawai?></th>
       <th class="text-right"><?=$sumBezetting?></th>
       <th></th>
       <th></th>
       <th></th>
       <th></th>
     </tr>
   </tfoot>
 </table>
