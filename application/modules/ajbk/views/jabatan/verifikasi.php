<?php
$ruser = GetLoggedUser();

$ropd = array();
$rbid = array();
$rsubbid = array();

if(!empty($_GET['KdOPD'])) {
  $ropd = $this->db->where(COL_UNIQ, $_GET['KdOPD'])->get(TBL_AJBK_UNIT)->row_array();
  if(!empty($ropd) && !empty($_GET['KdBidang'])) {
    $rbid = $this->db->where(COL_UNIQ, $_GET['KdBidang'])->get(TBL_AJBK_UNIT_BID)->row_array();
    if(!empty($rbid) && !empty($_GET['KdSubBidang'])) {
      $rsubbid = $this->db->where(COL_UNIQ, $_GET['KdSubBidang'])->get(TBL_AJBK_UNIT_SUBBID)->row_array();
    }
  }
}

$data = array();
$i = 0;
foreach ($res as $d) {
  $txtUnit = '<li style="list-style-type: disclosure-closed">'.$d[COL_NM_SUB_UNIT].'</li>';
  if(!empty($d[COL_NM_BID])) {
    $txtUnit .= '<li style="list-style-type: disclosure-closed">'.$d[COL_NM_BID].'</li>';
  }
  if(!empty($d[COL_NM_SUBBID])) {
    $txtUnit .= '<li style="list-style-type: disclosure-closed">'.$d[COL_NM_SUBBID].'</li>';
  }

  $res[$i] = array(
    '<a href="'.site_url('ajbk/jabatan/change-status/'.$d[COL_KD_JABATAN]).'" class="btn btn-xs btn-primary modal-status" data-status="'.$d[COL_VERIFIED].'"><i class="far fa-sync"></i> STATUS</a>',
    '<a target="_blank" href="'.site_url('ajbk/jabatan/cetak/'.$d[COL_KD_JABATAN]).'">'.$d[COL_NM_JABATAN].'</a>',
    '<ul class="m-0 pl-3 text-sm">'.$txtUnit.'</ul>',
    number_format($d["Pegawai"], 0),
    $d[COL_VERIFIED]?'Sudah':'Belum'
  );
  $i++;
}
$data = json_encode($res);
?>
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1 class="m-0 text-dark">Verifikasi <small class="text-sm">Informasi Jabatan</small></h1>
      </div>
    </div>
  </div>
</div>
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-sm-12">
        <div class="card card-default">
          <div class="card-header">
            <h3 class="card-title">UNIT KERJA</h3>
            <div class="card-tools">
              <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
            </div>
          </div>
          <div class="card-body">
            <?=form_open(current_url(),array('role'=>'form','id'=>'form-filter','class'=>'form-horizontal','method'=>'get'))?>
            <div class="form-group row">
              <label class="control-label col-sm-2">Pratama</label>
              <div class="col-sm-8">
                <select name="KdOPD" class="form-control">
                  <?=GetCombobox("SELECT * FROM ajbk_unit where IsAktif=1", COL_UNIQ, COL_NM_SUB_UNIT, (!empty($ropd)?$ropd[COL_UNIQ]:null), true, false, '-- PILIH --')?>
                </select>
              </div>
            </div>
            <div class="form-group row">
              <label class="control-label col-sm-2">Administrator</label>
              <div class="col-sm-8">
                <select name="KdBidang" class="form-control">
                  <?php
                  if(!empty($ropd)) {
                    echo GetCombobox("SELECT * FROM ajbk_unit_bid where Kd_Urusan=".$ropd[COL_KD_URUSAN]." and Kd_Bidang=".$ropd[COL_KD_BIDANG]." and Kd_Unit=".$ropd[COL_KD_UNIT]." and Kd_Sub=".$ropd[COL_KD_SUB], COL_UNIQ, COL_NM_BID, (!empty($rbid)?$rbid[COL_UNIQ]:null), true, false, '-- PILIH --');
                  } else {
                    ?>
                    <option value="">-- PILIH --</option>
                    <?php
                  }
                  ?>
                </select>
              </div>
            </div>
            <div class="form-group row">
              <label class="control-label col-sm-2">Pengawas</label>
              <div class="col-sm-8">
                <select name="KdSubBidang" class="form-control">
                  <?php
                  if(!empty($rbid)) {
                    echo GetCombobox("SELECT * FROM ajbk_unit_subbid where Kd_Urusan=".$rbid[COL_KD_URUSAN]." and Kd_Bidang=".$rbid[COL_KD_BIDANG]." and Kd_Unit=".$rbid[COL_KD_UNIT]." and Kd_Sub=".$rbid[COL_KD_SUB]." and Kd_Bid=".$rbid[COL_KD_BID], COL_UNIQ, COL_NM_SUBBID, (!empty($rsubbid)?$rsubbid[COL_UNIQ]:null), true, false, '-- PILIH --');
                  } else {
                    ?>
                    <option value="">-- PILIH --</option>
                    <?php
                  }
                  ?>
                </select>
              </div>
            </div>
            <div class="form-group row">
              <label class="control-label col-sm-2">Status</label>
              <div class="col-sm-8">
                <select name="Verified" class="form-control">
                  <option value="">-- SEMUA --</option>
                  <option value="1" <?=isset($_GET['Verified'])&&$_GET['Verified']==1?'selected':''?>>VERIFIED</option>
                  <option value="2" <?=isset($_GET['Verified'])&&$_GET['Verified']==2?'selected':''?>>BELUM VERIFIED</option>
                </select>
              </div>
            </div>
            <?=form_close()?>
          </div>
        </div>
        <div id="card-data" class="card card-default">
          <div class="card-body">
            <form id="form-verifikasi" method="post" action="#">
              <table id="verifikasi" class="table table-bordered table-hover">

              </table>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<div class="modal fade" id="modal-verifikasi" tabindex="-1" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Ubah Status Verifikasi</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true"><i class="fa fa-close"></i></span>
        </button>
      </div>
      <div class="modal-body">
        <form id="form-status" method="post" action="#">
          <div class="form-group">
            <label>Status</label>
            <select name="Status" class="form-control no-select2">
              <option value="1">VERIFIED</option>
              <option value="0">BELUM VERIFIED</option>
            </select>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">BATAL</button>
        <button type="button" class="btn btn-primary btn-flat btn-ok">SIMPAN</button>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
$(document).ready(function() {
  $('[name=KdOPD],[name=KdBidang],[name=KdSubBidang],[name=Verified]').change(function() {
    if($(this).attr('name')=='KdOPD') {
      $('[name=KdBidang]').val('');
      $('[name=KdSubBidang]').val('');
    } else if($(this).attr('name')=='KdBidang') {
      $('[name=KdSubBidang]').val('');
    }
    $('#form-filter').submit();
  });

  var dataTable = $('#verifikasi').dataTable({
    "autoWidth" : false,
    "aaData": <?=$data?>,
    "scrollY" : '40vh',
    "scrollX": "120%",
    "iDisplayLength": 100,
    "aLengthMenu": [[100, 1000, 5000, -1], [100, 1000, 5000, "Semua"]],
    "dom":"R<'row'<'col-sm-4'l><'col-sm-8'f>><'row'<'col-sm-12'tr>><'row'<'col-sm-5'i><'col-sm-7'p>>",
    "buttons": ['copyHtml5','excelHtml5','csvHtml5','pdfHtml5'],
    "ordering": false,
    "columnDefs":[
      {targets: [3,4], className:'dt-body-center nowrap'},
      {targets: [0], className:'nowrap'}
    ],
    "aoColumns": [
      {"sTitle": "#", "sWidth": "10px"},
      {"sTitle": "Jabatan"},
      {"sTitle": "Unit Kerja"},
      {"sTitle": "ABK<br />(Pegawai)"},
      {"sTitle": "Status<br />Verifikasi"},
    ],
    "fnCreatedRow" : function( nRow, aData, iDataIndex) {
      $('.modal-status', $(nRow)).click(function(){
        var a = $(this);
        var status = $(this).data('status');
        var editor = $("#modal-verifikasi");

        $('[name=Status]', editor).val(status);
        editor.modal("show");
        $(".btn-ok", editor).unbind('click').click(function() {
          var dis = $(this);
          dis.html("Loading...").attr("disabled", true);
          $('#form-status').ajaxSubmit({
            dataType: 'json',
            url : a.attr('href'),
            success : function(data){
              if(data.error==0){
                toastr.success(data.success);
              }else{
                toastr.error(data.error);
              }
            },
            error: function() {
              toastr.error('Server error.');
            },
            complete: function() {
              dis.html("Simpan").attr("disabled", false);
              editor.modal("hide");
              location.reload();
            }
          });
        });
        return false;
      });
    }
  });
});
</script>
