<?php
$data = array();
$i = 0;
/*foreach ($res as $d) {
    $data[$i] = array(
      $d[COL_NM_SUB_UNIT],
      anchor(site_url('ajbk/jabatan/index-partial/STR/'.$d["IdOPD"].'/-1/1'), number_format($d['ABK_STR']), array('class'=>'link-popup-detail', 'data-opd'=>$d[COL_NM_SUB_UNIT])),
      anchor(site_url('ajbk/jabatan/index-partial/FUNG/'.$d["IdOPD"].'/-1/1'), number_format($d['ABK_PEL']), array('class'=>'link-popup-detail', 'data-opd'=>$d[COL_NM_SUB_UNIT])),
      anchor(site_url('ajbk/jabatan/index-bezetting-partial/STR/'.$d["IdOPD"].'/-1/1'), number_format($d['BEZ_STR']), array('class'=>'link-popup-detail', 'data-opd'=>$d[COL_NM_SUB_UNIT])),
      anchor(site_url('ajbk/jabatan/index-bezetting-partial/FUNG/'.$d["IdOPD"].'/-1/1'), number_format($d['BEZ_PEL']), array('class'=>'link-popup-detail', 'data-opd'=>$d[COL_NM_SUB_UNIT])),
      number_format($d['ABK_STR']-$d['BEZ_STR']),
      number_format($d['ABK_PEL']-$d['BEZ_PEL']),
    );
    $i++;
}
foreach ($res2 as $d) {
    $data[$i] = array(
      $d[COL_NM_BID],
      anchor(site_url('ajbk/jabatan/index-partial/STR/'.$d["IdOPD"].'/'.$d["IdBid"].'/1'), number_format($d['ABK_STR']), array('class'=>'link-popup-detail', 'data-opd'=>$d[COL_NM_BID])),
      anchor(site_url('ajbk/jabatan/index-partial/FUNG/'.$d["IdOPD"].'/'.$d["IdBid"].'/1'), number_format($d['ABK_PEL']), array('class'=>'link-popup-detail', 'data-opd'=>$d[COL_NM_BID])),
      anchor(site_url('ajbk/jabatan/index-bezetting-partial/STR/'.$d["IdOPD"].'/'.$d["IdBid"].'/1'), number_format($d['BEZ_STR']), array('class'=>'link-popup-detail', 'data-opd'=>$d[COL_NM_BID])),
      anchor(site_url('ajbk/jabatan/index-bezetting-partial/FUNG/'.$d["IdOPD"].'/'.$d["IdBid"].'/1'), number_format($d['BEZ_PEL']), array('class'=>'link-popup-detail', 'data-opd'=>$d[COL_NM_BID])),
      number_format($d['ABK_STR']-$d['BEZ_STR']),
      number_format($d['ABK_PEL']-$d['BEZ_PEL']),
    );
    $i++;
}*/
foreach($opd as $o) {
  $sumABK_STR = 0;
  $sumABK_PEL = 0;
  $sumBEZ_STR = 0;
  $sumBEZ_PEL = 0;
  if($o[COL_UNIQ] == 20) {
    continue;
  }
  $qJabatan = $this->db
  ->where(array(
    COL_KD_URUSAN=>$o[COL_KD_URUSAN],
    COL_KD_BIDANG=>$o[COL_KD_BIDANG],
    COL_KD_UNIT=>$o[COL_KD_UNIT],
    COL_KD_SUB=>$o[COL_KD_SUB]
  ));
  if($o[COL_UNIQ] == 2) {
    $qJabatan = $qJabatan
    ->group_start()
    ->where_not_in(TBL_AJBK_JABATAN.".".COL_KD_BID, array(5,6,7,8,9,10,11,12,13,14,15))
    ->or_where(TBL_AJBK_JABATAN.".".COL_KD_BID,null)
    ->group_end();
  }
  if($o[COL_UNIQ] == 1) {
    $qJabatan = $qJabatan
    ->group_start()
    ->where_in(TBL_AJBK_JABATAN.".".COL_KD_BID, array(1,2,3,4))
    ->or_where(TBL_AJBK_JABATAN.".".COL_KD_BID,null)
    ->group_end();
  }
  $rJabatan = $qJabatan->get(TBL_AJBK_JABATAN)->result_array();
  foreach($rJabatan as $jab) {
    $rUraian = $this->db
    ->where(COL_KD_JABATAN, $jab[COL_KD_JABATAN])
    ->get(TBL_AJBK_JABATAN_URAIAN)
    ->result_array();

    $rBezetting = $this->db
    ->where(COL_TAHUN, date('Y'))
    ->where(COL_KD_JABATAN, $jab[COL_KD_JABATAN])
    ->get(TBL_AJBK_JABATAN_BEZETTING)
    ->row_array();

    $abkSTR = 0;
    $abkPEL = 0;
    foreach ($rUraian as $u) {
      if($jab[COL_KD_TYPE] == 'STR') $abkSTR += ($u[COL_JLH_BEBAN]*$u[COL_JLH_JAM])/1250;
      else if($jab[COL_KD_TYPE] == 'FUNG') $abkPEL += ($u[COL_JLH_BEBAN]*$u[COL_JLH_JAM])/1250;
    }

    $sumABK_STR += round($abkSTR);
    $sumABK_PEL += round($abkPEL);
    if($jab[COL_KD_TYPE] == 'STR') $sumBEZ_STR += $rBezetting[COL_JLH_PEGAWAI];
    else if($jab[COL_KD_TYPE] == 'FUNG') $sumBEZ_PEL += $rBezetting[COL_JLH_PEGAWAI];
  }

  $data[$i] = array(
    $o[COL_NM_SUB_UNIT],
    anchor(site_url('ajbk/jabatan/index-partial/STR/'.$o[COL_UNIQ].'/-1/1'), number_format($sumABK_STR), array('class'=>'link-popup-detail', 'data-opd'=>$o[COL_NM_SUB_UNIT])),
    anchor(site_url('ajbk/jabatan/index-partial/FUNG/'.$o[COL_UNIQ].'/-1/1'), number_format($sumABK_PEL), array('class'=>'link-popup-detail', 'data-opd'=>$o[COL_NM_SUB_UNIT])),
    anchor(site_url('ajbk/jabatan/index-bezetting-partial/STR/'.$o[COL_UNIQ].'/-1/1'), number_format($sumBEZ_STR), array('class'=>'link-popup-detail', 'data-opd'=>$o[COL_NM_SUB_UNIT])),
    anchor(site_url('ajbk/jabatan/index-bezetting-partial/FUNG/'.$o[COL_UNIQ].'/-1/1'), number_format($sumBEZ_PEL), array('class'=>'link-popup-detail', 'data-opd'=>$o[COL_NM_SUB_UNIT])),
    number_format($sumABK_STR-$sumBEZ_STR),
    number_format($sumABK_PEL-$sumBEZ_PEL)
  );
  $i++;
}

foreach($bag as $o) {
  $sumABK_STR = 0;
  $sumABK_PEL = 0;
  $sumBEZ_STR = 0;
  $sumBEZ_PEL = 0;
  $qJabatan = $this->db
  ->where(array(
    COL_KD_URUSAN=>$o[COL_KD_URUSAN],
    COL_KD_BIDANG=>$o[COL_KD_BIDANG],
    COL_KD_UNIT=>$o[COL_KD_UNIT],
    COL_KD_SUB=>$o[COL_KD_SUB],
    COL_KD_BID=>$o[COL_KD_BID]
  ));
  $rJabatan = $qJabatan->get(TBL_AJBK_JABATAN)->result_array();
  foreach($rJabatan as $jab) {
    $rUraian = $this->db
    ->where(COL_KD_JABATAN, $jab[COL_KD_JABATAN])
    ->get(TBL_AJBK_JABATAN_URAIAN)
    ->result_array();

    $rBezetting = $this->db
    ->where(COL_TAHUN, date('Y'))
    ->where(COL_KD_JABATAN, $jab[COL_KD_JABATAN])
    ->get(TBL_AJBK_JABATAN_BEZETTING)
    ->row_array();

    $abkSTR = 0;
    $abkPEL = 0;
    foreach ($rUraian as $u) {
      if($jab[COL_KD_TYPE] == 'STR') $abkSTR += ($u[COL_JLH_BEBAN]*$u[COL_JLH_JAM])/1250;
      else if($jab[COL_KD_TYPE] == 'FUNG') $abkPEL += ($u[COL_JLH_BEBAN]*$u[COL_JLH_JAM])/1250;
    }

    $sumABK_STR += round($abkSTR);
    $sumABK_PEL += round($abkPEL);
    if($jab[COL_KD_TYPE] == 'STR') $sumBEZ_STR += $rBezetting[COL_JLH_PEGAWAI];
    else if($jab[COL_KD_TYPE] == 'FUNG') $sumBEZ_PEL += $rBezetting[COL_JLH_PEGAWAI];
  }

  $data[$i] = array(
    $o[COL_NM_BID],
    anchor(site_url('ajbk/jabatan/index-partial/STR/'.$o['IdOPD'].'/'.$o[COL_UNIQ].'/1'), number_format($sumABK_STR), array('class'=>'link-popup-detail', 'data-opd'=>$o[COL_NM_BID])),
    anchor(site_url('ajbk/jabatan/index-partial/FUNG/'.$o['IdOPD'].'/'.$o[COL_UNIQ].'/1'), number_format($sumABK_PEL), array('class'=>'link-popup-detail', 'data-opd'=>$o[COL_NM_BID])),
    anchor(site_url('ajbk/jabatan/index-bezetting-partial/STR/'.$o['IdOPD'].'/'.$o[COL_UNIQ].'/1'), number_format($sumBEZ_STR), array('class'=>'link-popup-detail', 'data-opd'=>$o[COL_NM_BID])),
    anchor(site_url('ajbk/jabatan/index-bezetting-partial/FUNG/'.$o['IdOPD'].'/'.$o[COL_UNIQ].'/1'), number_format($sumBEZ_PEL), array('class'=>'link-popup-detail', 'data-opd'=>$o[COL_NM_BID])),
    number_format($sumABK_STR-$sumBEZ_STR),
    number_format($sumABK_PEL-$sumBEZ_PEL)
  );
  $i++;
}

foreach($kec as $o) {
  $sumABK_STR = 0;
  $sumABK_PEL = 0;
  $sumBEZ_STR = 0;
  $sumBEZ_PEL = 0;
  $qJabatan = $this->db
  ->where(array(
    COL_KD_URUSAN=>$o[COL_KD_URUSAN],
    COL_KD_BIDANG=>$o[COL_KD_BIDANG],
    COL_KD_UNIT=>$o[COL_KD_UNIT],
    COL_KD_SUB=>$o[COL_KD_SUB],
    COL_KD_BID=>$o[COL_KD_BID]
  ));
  $rJabatan = $qJabatan->get(TBL_AJBK_JABATAN)->result_array();
  foreach($rJabatan as $jab) {
    $rUraian = $this->db
    ->where(COL_KD_JABATAN, $jab[COL_KD_JABATAN])
    ->get(TBL_AJBK_JABATAN_URAIAN)
    ->result_array();

    $rBezetting = $this->db
    ->where(COL_TAHUN, date('Y'))
    ->where(COL_KD_JABATAN, $jab[COL_KD_JABATAN])
    ->get(TBL_AJBK_JABATAN_BEZETTING)
    ->row_array();

    $abkSTR = 0;
    $abkPEL = 0;
    foreach ($rUraian as $u) {
      if($jab[COL_KD_TYPE] == 'STR') $abkSTR += ($u[COL_JLH_BEBAN]*$u[COL_JLH_JAM])/1250;
      else if($jab[COL_KD_TYPE] == 'FUNG') $abkPEL += ($u[COL_JLH_BEBAN]*$u[COL_JLH_JAM])/1250;
    }

    $sumABK_STR += round($abkSTR);
    $sumABK_PEL += round($abkPEL);
    if($jab[COL_KD_TYPE] == 'STR') $sumBEZ_STR += $rBezetting[COL_JLH_PEGAWAI];
    else if($jab[COL_KD_TYPE] == 'FUNG') $sumBEZ_PEL += $rBezetting[COL_JLH_PEGAWAI];
  }

  $data[$i] = array(
    $o[COL_NM_BID],
    anchor(site_url('ajbk/jabatan/index-partial/STR/'.$o['IdOPD'].'/'.$o[COL_UNIQ].'/1'), number_format($sumABK_STR), array('class'=>'link-popup-detail', 'data-opd'=>$o[COL_NM_BID])),
    anchor(site_url('ajbk/jabatan/index-partial/FUNG/'.$o['IdOPD'].'/'.$o[COL_UNIQ].'/1'), number_format($sumABK_PEL), array('class'=>'link-popup-detail', 'data-opd'=>$o[COL_NM_BID])),
    anchor(site_url('ajbk/jabatan/index-bezetting-partial/STR/'.$o['IdOPD'].'/'.$o[COL_UNIQ].'/1'), number_format($sumBEZ_STR), array('class'=>'link-popup-detail', 'data-opd'=>$o[COL_NM_BID])),
    anchor(site_url('ajbk/jabatan/index-bezetting-partial/FUNG/'.$o['IdOPD'].'/'.$o[COL_UNIQ].'/1'), number_format($sumBEZ_PEL), array('class'=>'link-popup-detail', 'data-opd'=>$o[COL_NM_BID])),
    number_format($sumABK_STR-$sumBEZ_STR),
    number_format($sumABK_PEL-$sumBEZ_PEL)
  );
  $i++;
}

foreach($uptkes as $o) {
  $sumABK_STR = 0;
  $sumABK_PEL = 0;
  $sumBEZ_STR = 0;
  $sumBEZ_PEL = 0;
  $qJabatan = $this->db
  ->where(array(
    COL_KD_URUSAN=>$o[COL_KD_URUSAN],
    COL_KD_BIDANG=>$o[COL_KD_BIDANG],
    COL_KD_UNIT=>$o[COL_KD_UNIT],
    COL_KD_SUB=>$o[COL_KD_SUB],
    COL_KD_BID=>$o[COL_KD_BID]
  ));
  $rJabatan = $qJabatan->get(TBL_AJBK_JABATAN)->result_array();
  foreach($rJabatan as $jab) {
    $rUraian = $this->db
    ->where(COL_KD_JABATAN, $jab[COL_KD_JABATAN])
    ->get(TBL_AJBK_JABATAN_URAIAN)
    ->result_array();

    $rBezetting = $this->db
    ->where(COL_TAHUN, date('Y'))
    ->where(COL_KD_JABATAN, $jab[COL_KD_JABATAN])
    ->get(TBL_AJBK_JABATAN_BEZETTING)
    ->row_array();

    $abkSTR = 0;
    $abkPEL = 0;
    foreach ($rUraian as $u) {
      if($jab[COL_KD_TYPE] == 'STR') $abkSTR += ($u[COL_JLH_BEBAN]*$u[COL_JLH_JAM])/1250;
      else if($jab[COL_KD_TYPE] == 'FUNG') $abkPEL += ($u[COL_JLH_BEBAN]*$u[COL_JLH_JAM])/1250;
    }

    $sumABK_STR += round($abkSTR);
    $sumABK_PEL += round($abkPEL);
    if($jab[COL_KD_TYPE] == 'STR') $sumBEZ_STR += $rBezetting[COL_JLH_PEGAWAI];
    else if($jab[COL_KD_TYPE] == 'FUNG') $sumBEZ_PEL += $rBezetting[COL_JLH_PEGAWAI];
  }

  $data[$i] = array(
    $o[COL_NM_BID],
    anchor(site_url('ajbk/jabatan/index-partial/STR/'.$o['IdOPD'].'/'.$o[COL_UNIQ].'/1'), number_format($sumABK_STR), array('class'=>'link-popup-detail', 'data-opd'=>$o[COL_NM_BID])),
    anchor(site_url('ajbk/jabatan/index-partial/FUNG/'.$o['IdOPD'].'/'.$o[COL_UNIQ].'/1'), number_format($sumABK_PEL), array('class'=>'link-popup-detail', 'data-opd'=>$o[COL_NM_BID])),
    anchor(site_url('ajbk/jabatan/index-bezetting-partial/STR/'.$o['IdOPD'].'/'.$o[COL_UNIQ].'/1'), number_format($sumBEZ_STR), array('class'=>'link-popup-detail', 'data-opd'=>$o[COL_NM_BID])),
    anchor(site_url('ajbk/jabatan/index-bezetting-partial/FUNG/'.$o['IdOPD'].'/'.$o[COL_UNIQ].'/1'), number_format($sumBEZ_PEL), array('class'=>'link-popup-detail', 'data-opd'=>$o[COL_NM_BID])),
    number_format($sumABK_STR-$sumBEZ_STR),
    number_format($sumABK_PEL-$sumBEZ_PEL)
  );
  $i++;
}

foreach($sekolah as $o) {
  $sumABK_STR = 0;
  $sumABK_PEL = 0;
  $sumBEZ_STR = 0;
  $sumBEZ_PEL = 0;
  $qJabatan = $this->db
  ->where(array(
    COL_KD_URUSAN=>$o[COL_KD_URUSAN],
    COL_KD_BIDANG=>$o[COL_KD_BIDANG],
    COL_KD_UNIT=>$o[COL_KD_UNIT],
    COL_KD_SUB=>$o[COL_KD_SUB],
    COL_KD_BID=>$o[COL_KD_BID]
  ));
  $rJabatan = $qJabatan->get(TBL_AJBK_JABATAN)->result_array();
  foreach($rJabatan as $jab) {
    $rUraian = $this->db
    ->where(COL_KD_JABATAN, $jab[COL_KD_JABATAN])
    ->get(TBL_AJBK_JABATAN_URAIAN)
    ->result_array();

    $rBezetting = $this->db
    ->where(COL_TAHUN, date('Y'))
    ->where(COL_KD_JABATAN, $jab[COL_KD_JABATAN])
    ->get(TBL_AJBK_JABATAN_BEZETTING)
    ->row_array();

    $abkSTR = 0;
    $abkPEL = 0;
    foreach ($rUraian as $u) {
      if($jab[COL_KD_TYPE] == 'STR') $abkSTR += ($u[COL_JLH_BEBAN]*$u[COL_JLH_JAM])/1250;
      else if($jab[COL_KD_TYPE] == 'FUNG') $abkPEL += ($u[COL_JLH_BEBAN]*$u[COL_JLH_JAM])/1250;
    }

    $sumABK_STR += round($abkSTR);
    $sumABK_PEL += round($abkPEL);
    if($jab[COL_KD_TYPE] == 'STR') $sumBEZ_STR += $rBezetting[COL_JLH_PEGAWAI];
    else if($jab[COL_KD_TYPE] == 'FUNG') $sumBEZ_PEL += $rBezetting[COL_JLH_PEGAWAI];
  }

  $data[$i] = array(
    $o[COL_NM_BID],
    anchor(site_url('ajbk/jabatan/index-partial/STR/'.$o['IdOPD'].'/'.$o[COL_UNIQ].'/1'), number_format($sumABK_STR), array('class'=>'link-popup-detail', 'data-opd'=>$o[COL_NM_BID])),
    anchor(site_url('ajbk/jabatan/index-partial/FUNG/'.$o['IdOPD'].'/'.$o[COL_UNIQ].'/1'), number_format($sumABK_PEL), array('class'=>'link-popup-detail', 'data-opd'=>$o[COL_NM_BID])),
    anchor(site_url('ajbk/jabatan/index-bezetting-partial/STR/'.$o['IdOPD'].'/'.$o[COL_UNIQ].'/1'), number_format($sumBEZ_STR), array('class'=>'link-popup-detail', 'data-opd'=>$o[COL_NM_BID])),
    anchor(site_url('ajbk/jabatan/index-bezetting-partial/FUNG/'.$o['IdOPD'].'/'.$o[COL_UNIQ].'/1'), number_format($sumBEZ_PEL), array('class'=>'link-popup-detail', 'data-opd'=>$o[COL_NM_BID])),
    number_format($sumABK_STR-$sumBEZ_STR),
    number_format($sumABK_PEL-$sumBEZ_PEL)
  );
  $i++;
}
$data = json_encode($data);
 ?>
 <div class="content-header">
   <div class="container-fluid">
     <div class="row mb-2">
       <div class="col-sm-6">
         <h1 class="m-0 text-dark"><?= $title ?></h1>
       </div>
       <div class="col-sm-6">
         <ol class="breadcrumb float-sm-right">
           <li class="breadcrumb-item"><a href="<?=site_url()?>">Home</a></li>
           <li class="breadcrumb-item active"><?=$title?></li>
         </ol>
       </div>
     </div>
   </div>
 </div>
 <section class="content">
   <div class="container-fluid">
     <div class="row">
       <div class="col-sm-12">
         <div class="card card-default">
           <div class="card-body">
             <form id="dataform" method="post" action="#">
               <table id="datalist" class="table table-bordered table-hover">
                 <thead>
                   <tr>
                     <th rowspan="2">OPD / Unit Kerja</th>
                     <th colspan="2">Beban Kerja</th>
                     <th colspan="2">Bezetting</th>
                     <th colspan="2">Kebutuhan</th>
                   </tr>
                   <tr>
                     <th>Struktural</th>
                     <th>Pelaksana</th>
                     <th>Struktural</th>
                     <th>Pelaksana</th>
                     <th>Struktural</th>
                     <th>Pelaksana</th>
                   </tr>
                 </thead>
               </table>
             </form>
           </div>
         </div>
       </div>
     </div>
   </div>
 </section>
 <div class="modal fade" id="modal-detail" tabindex="-1" role="dialog">
   <div class="modal-dialog modal-xl">
     <div class="modal-content">
       <div class="modal-header">
         <h4 class="modal-title font-weight-light">Detail</h4>
         <button type="button" class="close" data-dismiss="modal" aria-label="Close">
           <span aria-hidden="true"><i class="fas fa-close"></i></span>
         </button>
       </div>
       <div class="modal-body">
         <div id="map"></div>
       </div>
     </div>
   </div>
 </div>
 <script type="text/javascript">
 $(document).ready(function() {
   $('.modal').on('hidden.bs.modal', function (e) {
     $('.modal-body', $(this)).empty();
   });

   var dataTable = $('#datalist').dataTable({
     "autoWidth" : false,
     "aaData": <?=$data?>,
     //"scrollY" : '40vh',
     "scrollX": "120%",
     "iDisplayLength": 100,
     "aLengthMenu": [[100, 1000, 5000, -1], [100, 1000, 5000, "Semua"]],
     "dom":"R<'row'<'col-sm-8'B><'col-sm-4'f>><'row'<'col-sm-12'tr>>",
     "buttons": ['copyHtml5','excelHtml5','csvHtml5','pdfHtml5'],
     "ordering": false,
     "columnDefs":[
       {targets: [1,2,3,4,5,6], className:'text-right'},
       {targets: [0], className:'nowrap'}
     ],
     /*"aoColumns": [
        {"sTitle": "OPD"},
        {"sTitle": "Beban Kerja<br  />(Struktural)"},
        {"sTitle": "Beban Kerja<br  />(Pelaksana / Fungsional)"},
        {"sTitle": "Bezetting<br  />(Struktural)"},
        {"sTitle": "Bezetting<br  />(Pelaksana / Fungsional)"},
        {"sTitle": "Kebutuhan<br  />(Struktural)"},
        {"sTitle": "Kebutuhan<br  />(Pelaksana / Fungsional)"}
     ]*/
     "createdRow": function(row, data, dataIndex) {
       $('.link-popup-detail', $(row)).click(function() {
         var url = $(this).attr('href');
         var opd = $(this).data('opd');

         $('.modal-title', $('#modal-detail')).html(opd);
         $('.modal-body', $('#modal-detail')).load(url, function(){
           $('#modal-detail').modal('show');
           setTimeout(function() {
             $($.fn.dataTable.tables(true)).DataTable().columns.adjust();
           }, 200);
         });
         return false;
       });
     }
   });
 });
 </script>
