<?php
$ruser = GetLoggedUser();
$strOPD = explode('.', $ruser[COL_COMPANYID]);
 ?>
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1 class="m-0 text-dark"><?= $title ?> <small> Form</small></h1>
      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="<?=site_url()?>">Home</a></li>
          <li class="breadcrumb-item"><a href="<?=site_url('ajbk/jabatan/index')?>"> <?=$title?></a></li>
          <li class="breadcrumb-item active"><?=$edit?'Edit':'Add'?></li>
        </ol>
      </div>
    </div>
  </div>
</div>
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-sm-12">
        <div class="card card-primary">
          <?=form_open(current_url(),array('role'=>'form','id'=>'main-form','class'=>'form-horizontal'))?>
          <div class="card-body">
            <div id="accordion">
              <div class="card card-default">
                <div class="card-header">
                  <h4 class="card-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#UnitKerja">
                      UNIT KERJA
                    </a>
                  </h4>
                </div>
                <div id="UnitKerja" class="panel-collapse in collapse show">
                  <div class="card-body">
                    <div class="form-group row">
                      <label class="control-label col-sm-2">Pratama</label>
                      <div class="col-sm-8">
                          <div class="input-group">
                              <?php
                              $nmSub = "";
                              $nmBid = "";
                              $nmSubBid = "";
                              if($edit) {
                                  $this->db->where(COL_KD_URUSAN, $data[COL_KD_URUSAN]);
                                  $this->db->where(COL_KD_BIDANG, $data[COL_KD_BIDANG]);
                                  $this->db->where(COL_KD_UNIT, $data[COL_KD_UNIT]);
                                  $this->db->where(COL_KD_SUB, $data[COL_KD_SUB]);
                                  $subunit = $this->db->get("ajbk_unit")->row_array();
                                  if($subunit) {
                                      $nmSub = $subunit["Nm_Sub_Unit"];
                                  }
                              }
                              if($ruser[COL_ROLEID] != ROLEADMIN) {
                                  $this->db->where(COL_KD_URUSAN, $strOPD[0]);
                                  $this->db->where(COL_KD_BIDANG, $strOPD[1]);
                                  $this->db->where(COL_KD_UNIT, $strOPD[2]);
                                  $this->db->where(COL_KD_SUB, $strOPD[3]);
                                  $subunit = $this->db->get("ajbk_unit")->row_array();
                                  if($subunit) {
                                      $nmSub = $subunit["Nm_Sub_Unit"];
                                  }
                              }
                              if($ruser[COL_ROLEID] == ROLEKABID || $ruser[COL_ROLEID] == ROLEKASUBBID) {
                                  $this->db->where(COL_KD_URUSAN, $strOPD[0]);
                                  $this->db->where(COL_KD_BIDANG, $strOPD[1]);
                                  $this->db->where(COL_KD_UNIT, $strOPD[2]);
                                  $this->db->where(COL_KD_SUB, $strOPD[3]);
                                  $this->db->where(COL_KD_BID, $strOPD[4]);
                                  $bid = $this->db->get("ajbk_unit_bid")->row_array();
                                  if($bid) {
                                      $nmBid = $bid["Nm_Bid"];
                                  }
                              }
                              if($ruser[COL_ROLEID] == ROLEKASUBBID) {
                                  $this->db->where(COL_KD_URUSAN, $strOPD[0]);
                                  $this->db->where(COL_KD_BIDANG, $strOPD[1]);
                                  $this->db->where(COL_KD_UNIT, $strOPD[2]);
                                  $this->db->where(COL_KD_SUB, $strOPD[3]);
                                  $this->db->where(COL_KD_BID, $strOPD[4]);
                                  $this->db->where(COL_KD_SUBBID, $strOPD[5]);
                                  $subbid = $this->db->get("ajbk_unit_subbid")->row_array();
                                  if($subbid) {
                                      $nmSubBid = $subbid["Nm_Subbid"];
                                  }
                              }
                              ?>
                              <input type="text" class="form-control" name="text-opd" value="<?= $edit ? $data[COL_KD_URUSAN].".".$data[COL_KD_BIDANG].".".$data[COL_KD_UNIT].".".$data[COL_KD_SUB]." ".$nmSub : ($ruser[COL_ROLEID] != ROLEADMIN ? $strOPD[0].".".$strOPD[1].".".$strOPD[2].".".$strOPD[3]." ".$nmSub : "")?>" readonly>
                              <input type="hidden" name="<?=COL_KD_URUSAN?>" value="<?= $edit ? $data[COL_KD_URUSAN] : ($ruser[COL_ROLEID]!=ROLEADMIN?$strOPD[0]:"")?>" required   >
                              <input type="hidden" name="<?=COL_KD_BIDANG?>" value="<?= $edit ? $data[COL_KD_BIDANG] : ($ruser[COL_ROLEID]!=ROLEADMIN?$strOPD[1]:"")?>" required   >
                              <input type="hidden" name="<?=COL_KD_UNIT?>" value="<?= $edit ? $data[COL_KD_UNIT] : ($ruser[COL_ROLEID]!=ROLEADMIN?$strOPD[2]:"")?>" required   >
                              <input type="hidden" name="<?=COL_KD_SUB?>" value="<?= $edit ? $data[COL_KD_SUB] : ($ruser[COL_ROLEID]!=ROLEADMIN?$strOPD[3]:"")?>" required   >
                              <div class="input-group-append">
                                  <button type="button" class="btn btn-default btn-flat btn-browse-opd" data-toggle="modal" data-target="#browseOPD" data-toggle="tooltip" data-placement="top" title="Pilih" <?=($ruser[COL_ROLEID] != ROLEADMIN ? "disabled" : "")?>><i class="fa fa-ellipsis-h"></i></button>
                                  <button type="button" class="btn btn-default btn-browse-del" title="Hapus"><i class="fa fa-times" <?=($ruser[COL_ROLEID] != ROLEADMIN ? "disabled" : "")?>></i></button>
                              </div>
                          </div>
                      </div>
                    </div>
                    <div class="form-group row">
                        <label class="control-label col-sm-2">Administrator</label>
                        <div class="col-sm-8">
                            <div class="input-group">
                                <input type="text" class="form-control" name="text-bid" value="<?= $edit && !empty($data[COL_KD_BID]) ? $data[COL_KD_BID].". ".$data[COL_NM_BID] : ($ruser[COL_ROLEID]==ROLEKABID || $ruser[COL_ROLEID]==ROLEKASUBBID?$bid[COL_KD_BID].". ".$bid[COL_NM_BID]:"")?>" readonly>
                                <input type="hidden" name="<?=COL_KD_BID?>" value="<?= $edit && !empty($data[COL_KD_BID]) ? $data[COL_KD_BID] : ($ruser[COL_ROLEID]==ROLEKABID || $ruser[COL_ROLEID]==ROLEKASUBBID?$strOPD[4]:"")?>">
                                <div class="input-group-append">
                                    <button type="button" class="btn btn-default btn-flat btn-browse-bid" data-toggle="modal" data-target="#browseBid" data-toggle="tooltip" data-placement="top" title="Pilih" <?=($ruser[COL_ROLEID] != ROLEADMIN && $ruser[COL_ROLEID] != ROLEKADIS ? "disabled" : "")?>><i class="fa fa-ellipsis-h"></i></button>
                                    <button type="button" class="btn btn-default btn-browse-del" title="Hapus" <?=($ruser[COL_ROLEID] != ROLEADMIN && $ruser[COL_ROLEID] != ROLEKADIS ? "disabled" : "")?>><i class="fa fa-times"></i></button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="control-label col-sm-2">Pengawas</label>
                        <div class="col-sm-8">
                            <div class="input-group">
                                <input type="text" class="form-control" name="text-subbid" value="<?= $edit && !empty($data[COL_KD_SUBBID]) ? $data[COL_KD_SUBBID].". ".$data[COL_NM_SUBBID] : ($ruser[COL_ROLEID]==ROLEKASUBBID?$subbid[COL_KD_SUBBID].". ".$subbid[COL_NM_SUBBID]:"")?>" readonly>
                                <input type="hidden" name="<?=COL_KD_SUBBID?>" value="<?= $edit && !empty($data[COL_KD_SUBBID]) ? $data[COL_KD_SUBBID] : ($ruser[COL_ROLEID]==ROLEKASUBBID?$strOPD[5]:"")?>">
                                <div class="input-group-append">
                                    <button type="button" class="btn btn-default btn-flat btn-browse-subbid" data-toggle="modal" data-target="#browseSubBid" data-toggle="tooltip" data-placement="top" title="Pilih" <?=($ruser[COL_ROLEID] != ROLEADMIN && $ruser[COL_ROLEID] != ROLEKADIS  && $ruser[COL_ROLEID] != ROLEKABID ? "disabled" : "")?>><i class="fa fa-ellipsis-h"></i></button>
                                    <button type="button" class="btn btn-default btn-browse-del" title="Hapus" <?=($ruser[COL_ROLEID] != ROLEADMIN && $ruser[COL_ROLEID] != ROLEKADIS  && $ruser[COL_ROLEID] != ROLEKABID ? "disabled" : "")?>><i class="fa fa-times"></i></button>
                                </div>
                            </div>
                        </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="card card-default">
                <div class="card-header">
                  <h4 class="card-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#Detail">
                      DETAIL
                    </a>
                  </h4>
                </div>
                <div id="Detail" class="panel-collapse in collapse">
                  <div class="card-body">
                    <div class="form-group row">
                        <label class="control-label col-sm-2">Jenis Jabatan</label>
                        <div class="col-sm-2">
                          <div class="form-check">
                            <input class="form-check-input" type="radio" id="STR" name="<?=COL_KD_TYPE?>" value="<?=JABATAN_TYPE_STRUKTURAL?>" <?=$edit?($data[COL_KD_TYPE]==JABATAN_TYPE_STRUKTURAL?'checked':''):'checked'?>>
                            <label class="form-check-label" for="STR">Struktural</label>
                          </div>
                        </div>
                        <div class="col-sm-2">
                          <div class="form-check">
                            <input class="form-check-input" type="radio" id="FUNG" name="<?=COL_KD_TYPE?>" value="<?=JABATAN_TYPE_FUNGSIONAL?>" <?=$edit?($data[COL_KD_TYPE]==JABATAN_TYPE_FUNGSIONAL?'checked':''):''?>>
                            <label class="form-check-label" for="FUNG">Fungsional</label>
                          </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="control-label col-sm-2">Nama Jabatan / Kode</label>
                        <div class="col-sm-6 type-struktural">
                            <input type="text" class="form-control" placeholder="Nama" name="<?=COL_NM_JABATAN?>" value="<?= $edit ? $data[COL_NM_JABATAN] : ""?>" />
                        </div>
                        <div class="col-sm-6 type-fungsional">
                          <select name="<?=COL_KD_NOMENKLATUR?>" class="form-control">
                            <?=GetCombobox("select * from ajbk_nomenklatur order by Nm_Nomenklatur", COL_KD_NOMENKLATUR, COL_NM_NOMENKLATUR, ($edit ? $data[COL_KD_NOMENKLATUR] : null), true, false, '-- Pilih Nomenklatur Jabatan --')?>
                          </select>
                        </div>
                        <div class="col-sm-2">
                            <input type="text" class="form-control" placeholder="Kode" name="<?=COL_ID_JABATAN?>" value="<?= $edit ? $data[COL_ID_JABATAN] : ""?>" />
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="control-label col-sm-2">Ikhtisar</label>
                        <div class="col-sm-8">
                            <textarea class="form-control" rows="3" placeholder="Ikhtisar" name="<?=COL_NM_IKHTISAR?>"><?= $edit ? $data[COL_NM_IKHTISAR] : ""?></textarea>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="control-label col-sm-2">Kelas Jabatan</label>
                        <div class="col-sm-3">
                          <select name="<?=COL_KELASJAB?>" class="form-control">
                            <option value="">-- Pilih Kelas Jabatan --</option>
                            <?php
                            for($i=1; $i<=15; $i++) {
                              ?>
                              <option value="<?=$i?>" <?= $edit && $data[COL_KELASJAB]==$i ? "selected" : ""?>><?=$i?></option>
                              <?php
                            }
                            ?>
                          </select>
                        </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="card card-default">
                <div class="card-header">
                  <h4 class="card-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#Syarat">
                      SYARAT
                    </a>
                  </h4>
                </div>
                <div id="Syarat" class="panel-collapse in collapse">
                  <div class="card-body">
                    <div class="form-group row">
                        <label class="control-label col-sm-2">Pendidikan</label>
                        <div class="col-sm-6">
                          <select name="<?=COL_KD_PENDIDIKAN?>" class="form-control">
                            <?=GetCombobox("select * from ajbk_option where Opt_Code = '".OPT_CODE_PENDIDIKAN."' order by Opt_Name", COL_UNIQ, COL_OPT_NAME, ($edit ? $data[COL_KD_PENDIDIKAN] : null), true, false, '-- Pendidikan --')?>
                          </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="control-label col-sm-2">Jurusan</label>
                        <div class="col-sm-6">
                          <select name="<?=COL_KD_JURUSAN?>[]" class="form-control" multiple>
                            <?php
                            $arrJurusan = array();
                            if($edit) {
                              $rjurusan = $this->db->where(COL_KD_JABATAN, $data[COL_KD_JABATAN])->get(TBL_AJBK_JABATAN_JURUSAN)->result_array();
                              foreach($rjurusan as $s) {
                                $arrJurusan[] = $s[COL_KD_JURUSAN];
                              }
                            }
                            ?>
                            <?=GetCombobox("select * from ajbk_option where Opt_Code = '".OPT_CODE_JURUSAN."' order by Opt_Name", COL_UNIQ, COL_OPT_NAME, $arrJurusan)?>
                          </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="control-label col-sm-2">Pelatihan</label>
                        <div class="col-sm-6">
                          <select name="<?=COL_KD_DIKLAT?>[]" class="form-control" multiple>
                            <?php
                            $arrDiklat = array();
                            if($edit) {
                              $rdiklat = $this->db->where(COL_KD_JABATAN, $data[COL_KD_JABATAN])->get(TBL_AJBK_JABATAN_DIKLAT)->result_array();
                              foreach($rdiklat as $s) {
                                $arrDiklat[] = $s[COL_KD_DIKLAT];
                              }
                            }
                            ?>
                            <?=GetCombobox("select * from ajbk_option where Opt_Code = '".OPT_CODE_DIKLAT."' order by Opt_Name", COL_UNIQ, COL_OPT_NAME, $arrDiklat)?>
                          </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="control-label col-sm-2">Pengalaman</label>
                        <div class="col-sm-6">
                          <input type="text" class="form-control" name="<?=COL_KD_PENGALAMAN?>" value="<?=$edit ? $data[COL_KD_PENGALAMAN] : null?>" />
                        </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="card card-default">
                <div class="card-header">
                  <h4 class="card-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#Uraian">
                      URAIAN
                    </a>
                  </h4>
                </div>
                <div id="Uraian" class="panel-collapse in collapse">
                  <div class="card-body p-0">
                    <table id="tbl-uraian" class="table table-bordered">
                      <thead>
                        <tr>
                          <th style="width: 40px">
                            <button type="button" id="btn-add-uraian" class="btn btn-default btn-flat"><i class="fa fa-plus"></i></button>
                          </th>
                          <th>Tugas</th>
                          <th>Hasil</th>
                          <th>Jumlah Beban Kerja<br /><small>(1 Tahun)</small></th>
                          <th>Waktu Penyelesaian<br /><small>(Jam)</small></th>
                          <th>Waktu Total<br /><small>(Jam)</small></th>
                          <th>Kebutuhan Pegawai</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr class="tr-blueprint-det" style="display: none">
                          <td><button type="button" class="btn btn-default btn-flat btn-del-uraian"><i class="fa fa-minus"></i></button></td>
                          <td><textarea name="text-det-desc" rows="2" class="form-control" placeholder="Deskripsi Tugas" style="width: 360px"></textarea></td>
                          <td>
                            <select name="select-det-satuan" class="form-control no-select2" style="width: 100%">
                                <?=GetCombobox("select * from ajbk_option where Opt_Code = '".OPT_CODE_SATUAN."' order by Opt_Name", COL_UNIQ, COL_OPT_NAME)?>
                            </select>
                          </td>
                          <td><input type="text" name="text-det-jumlah" class="form-control uang text-right" value="0" /></td>
                          <td><input type="text" name="text-det-waktu" class="form-control money text-right" value="0"/></td>
                          <td><input type="text" name="text-det-subtotal" class="form-control money text-right" value="0" readonly /></td>
                          <td><input type="text" name="text-det-total" class="form-control money text-right" value="0" readonly /></td>
                        </tr>
                        <?php
                        $det = $this->db->where(COL_KD_JABATAN, ($edit?$data[COL_KD_JABATAN]:-999))->get(TBL_AJBK_JABATAN_URAIAN)->result_array();
                        foreach($det as $m) {
                          ?>
                          <tr>
                            <td><button type="button" class="btn btn-default btn-flat btn-del-uraian"><i class="fa fa-minus"></i></button></td>
                            <td><textarea name="text-det-desc" rows="2" class="form-control" placeholder="Deskripsi Tugas" style="width: 360px"><?=$m[COL_NM_URAIAN]?></textarea></td>
                            <td>
                              <select name="select-det-satuan" class="form-control" style="width: 100%">
                                  <?=GetCombobox("select * from ajbk_option where Opt_Code = '".OPT_CODE_SATUAN."' order by Opt_Name", COL_UNIQ, COL_OPT_NAME, $m[COL_KD_SATUAN])?>
                              </select>
                            </td>
                            <td><input type="text" name="text-det-jumlah" class="form-control uang text-right" value="<?=$m[COL_JLH_BEBAN]?>" /></td>
                            <td><input type="text" name="text-det-waktu" class="form-control money text-right" value="<?=$m[COL_JLH_JAM]?>"/></td>
                            <td><input type="text" name="text-det-subtotal" class="form-control money text-right" value="<?=$m[COL_JLH_JAM]*$m[COL_JLH_BEBAN]?>" readonly /></td>
                            <td><input type="text" name="text-det-total" class="form-control money text-right" value="<?=($m[COL_JLH_JAM]*$m[COL_JLH_BEBAN])/HOURPERYEAR?>" readonly /></td>
                          </tr>
                          <?php
                        }
                         ?>
                      </tbody>
                      <tfoot>
                        <tr>
                          <td colspan="5" class="text-right"><strong>TOTAL</strong></td>
                          <td class="text-center"><span id="text-totaljam" style="font-weight: bold"></span></td>
                          <td class="text-center"><span id="text-totalpeg" style="font-weight: bold"></span></td>
                        </tr>
                      </tfoot>
                    </table>
                  </div>
                </div>
              </div>
              <div class="card card-default">
                <div class="card-header">
                  <h4 class="card-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#BahanKerja">
                      BAHAN KERJA
                    </a>
                  </h4>
                </div>
                <div id="BahanKerja" class="panel-collapse in collapse">
                  <div class="card-body p-0">
                    <input type="hidden" name="<?=COL_NM_BAHANKERJA?>" />
                    <table id="tbl-bahankerja" class="table table-condensed">
                      <thead class="font-weight-bold">
                        <tr>
                          <td style="width: 40px">#</td>
                          <td>Bahan Kerja</td>
                          <td>Kegunaan</td>
                        </tr>
                      </thead>
                      <tbody>
                      </tbody>
                      <tfoot>
                        <tr>
                          <td><button type="button" id="btn-add-bahankerja" class="btn btn-sm btn-default"><i class="fas fa-plus"></i></button></td>
                          <td>
                            <textarea name="bahankerja-text" class="form-control" placeholder="Bahan Kerja"></textarea>
                          </td>
                          <td>
                            <textarea name="bahankerja-kegunaan" class="form-control" placeholder="Kegunaan"></textarea>
                          </td>
                        </tr>
                      </tfoot>
                    </table>
                  </div>
                </div>
              </div>

              <div class="card card-default">
                <div class="card-header">
                  <h4 class="card-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#HasilKerja">
                      HASIL KERJA
                    </a>
                  </h4>
                </div>
                <div id="HasilKerja" class="panel-collapse in collapse">
                  <div class="card-body p-0">
                    <input type="hidden" name="<?=COL_NM_HASILKERJA?>" />
                    <table id="tbl-hasilkerja" class="table table-condensed">
                      <thead class="font-weight-bold">
                        <tr>
                          <td style="width: 40px">#</td>
                          <td>Hasil Kerja</td>
                        </tr>
                      </thead>
                      <tbody>
                      </tbody>
                      <tfoot>
                        <tr>
                          <td><button type="button" id="btn-add-hasilkerja" class="btn btn-sm btn-default"><i class="fas fa-plus"></i></button></td>
                          <td>
                            <textarea name="hasilkerja-text" class="form-control" placeholder="Hasil Kerja"></textarea>
                          </td>
                        </tr>
                      </tfoot>
                    </table>
                  </div>
                </div>
              </div>

              <div class="card card-default">
                <div class="card-header">
                  <h4 class="card-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#Perangkat">
                      PERANGKAT KERJA
                    </a>
                  </h4>
                </div>
                <div id="Perangkat" class="panel-collapse in collapse">
                  <div class="card-body p-0">
                    <input type="hidden" name="<?=COL_NM_PERANGKATKERJA?>" />
                    <table id="tbl-perangkatkerja" class="table table-condensed">
                      <thead class="font-weight-bold">
                        <tr>
                          <td style="width: 40px">#</td>
                          <td>Perangkat Kerja</td>
                          <td>Kegunaan</td>
                        </tr>
                      </thead>
                      <tbody>
                      </tbody>
                      <tfoot>
                        <tr>
                          <td><button type="button" id="btn-add-perangkatkerja" class="btn btn-sm btn-default"><i class="fas fa-plus"></i></button></td>
                          <td>
                            <textarea name="perangkatkerja-text" class="form-control" placeholder="Perangkat Kerja"></textarea>
                          </td>
                          <td>
                            <textarea name="perangkatkerja-kegunaan" class="form-control" placeholder="Kegunaan"></textarea>
                          </td>
                        </tr>
                      </tfoot>
                    </table>
                  </div>
                </div>
              </div>

              <div class="card card-default">
                <div class="card-header">
                  <h4 class="card-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#TanggungJawab">
                      TANGGUNG JAWAB
                    </a>
                  </h4>
                </div>
                <div id="TanggungJawab" class="panel-collapse in collapse">
                  <div class="card-body p-0">
                    <input type="hidden" name="<?=COL_NM_TANGGUNGJAWAB?>" />
                    <table id="tbl-tanggungjawab" class="table table-condensed">
                      <thead class="font-weight-bold">
                        <tr>
                          <td style="width: 40px">#</td>
                          <td>Tanggung Jawab</td>
                        </tr>
                      </thead>
                      <tbody>
                      </tbody>
                      <tfoot>
                        <tr>
                          <td><button type="button" id="btn-add-tanggungjawab" class="btn btn-sm btn-default"><i class="fas fa-plus"></i></button></td>
                          <td>
                            <textarea name="tanggungjawab-text" class="form-control" placeholder="Tanggung Jawab"></textarea>
                          </td>
                        </tr>
                      </tfoot>
                    </table>
                  </div>
                </div>
              </div>

              <div class="card card-default">
                <div class="card-header">
                  <h4 class="card-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#Wewenang">
                      WEWENANG
                    </a>
                  </h4>
                </div>
                <div id="Wewenang" class="panel-collapse in collapse">
                  <div class="card-body p-0">
                    <input type="hidden" name="<?=COL_NM_WEWENANG?>" />
                    <table id="tbl-wewenang" class="table table-condensed">
                      <thead class="font-weight-bold">
                        <tr>
                          <td style="width: 40px">#</td>
                          <td>Wewenang</td>
                        </tr>
                      </thead>
                      <tbody>
                      </tbody>
                      <tfoot>
                        <tr>
                          <td><button type="button" id="btn-add-wewenang" class="btn btn-sm btn-default"><i class="fas fa-plus"></i></button></td>
                          <td>
                            <textarea name="wewenang-text" class="form-control" placeholder="Wewenang"></textarea>
                          </td>
                        </tr>
                      </tfoot>
                    </table>
                  </div>
                </div>
              </div>

              <div class="card card-default">
                <div class="card-header">
                  <h4 class="card-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#Korelasi">
                      KORELASI JABATAN
                    </a>
                  </h4>
                </div>
                <div id="Korelasi" class="panel-collapse in collapse">
                  <div class="card-body p-0">
                    <input type="hidden" name="<?=COL_NM_KORELASI?>" />
                    <table id="tbl-korelasi" class="table table-condensed">
                      <thead class="font-weight-bold">
                        <tr>
                          <td style="width: 40px">#</td>
                          <td>Jabatan</td>
                          <td>Unit Kerja / Instansi</td>
                          <td>Dalam Hal</td>
                        </tr>
                      </thead>
                      <tbody>
                      </tbody>
                      <tfoot>
                        <tr>
                          <td><button type="button" id="btn-add-korelasi" class="btn btn-sm btn-default"><i class="fas fa-plus"></i></button></td>
                          <td>
                            <textarea name="korelasi-jabatan" class="form-control" placeholder="Jabatan"></textarea>
                          </td>
                          <td>
                            <textarea name="korelasi-instansi" class="form-control" placeholder="Unit Kerja / Instansi"></textarea>
                          </td>
                          <td>
                            <textarea name="korelasi-hal" class="form-control" placeholder="Dalam Hal"></textarea>
                          </td>
                        </tr>
                      </tfoot>
                    </table>
                  </div>
                </div>
              </div>

              <div class="card card-default">
                <div class="card-header">
                  <h4 class="card-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#Kondisi">
                      KONDISI LINGKUNGAN KERJA
                    </a>
                  </h4>
                </div>
                <div id="Kondisi" class="panel-collapse in collapse">
                  <div class="card-body p-0">
                    <?php
                    $datKondisi = array();
                    if($edit) {
                      $datKondisi = json_decode($data[COL_NM_KONDISI]);
                    }
                    ?>
                    <table id="tbl-kondisi" class="table table-condensed">
                      <thead class="font-weight-bold">
                        <tr>
                          <td>Aspek</td>
                          <td>Faktor</td>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td style="width: 250px">Tempat Kerja</td>
                          <td>
                            <select name="kondisi-tempatkerja" class="form-control form-sm" style="width: 100%">
                                <?=GetCombobox("select * from ajbk_option where Opt_Code = '".OPT_CODE_WSPACE."' order by Opt_Name", COL_UNIQ, COL_OPT_NAME, ($edit?$datKondisi->TempatKerja:null), true, false, '--')?>
                            </select>
                          </td>
                        </tr>
                        <tr>
                          <td style="width: 250px">Suhu</td>
                          <td>
                            <select name="kondisi-suhu" class="form-control form-sm" style="width: 100%">
                                <?=GetCombobox("select * from ajbk_option where Opt_Code = '".OPT_CODE_SUHU."' order by Opt_Name", COL_UNIQ, COL_OPT_NAME, ($edit?$datKondisi->Suhu:null), true, false, '--')?>
                            </select>
                          </td>
                        </tr>
                        <tr>
                          <td style="width: 250px">Udara</td>
                          <td>
                            <select name="kondisi-udara" class="form-control form-sm" style="width: 100%">
                                <?=GetCombobox("select * from ajbk_option where Opt_Code = '".OPT_CODE_HMDITY."' order by Opt_Name", COL_UNIQ, COL_OPT_NAME, ($edit?$datKondisi->Udara:null), true, false, '--')?>
                            </select>
                          </td>
                        </tr>
                        <tr>
                          <td style="width: 250px">Keadaan Ruangan</td>
                          <td>
                            <select name="kondisi-cond" class="form-control form-sm" style="width: 100%">
                                <?=GetCombobox("select * from ajbk_option where Opt_Code = '".OPT_CODE_COND."' order by Opt_Name", COL_UNIQ, COL_OPT_NAME, ($edit?$datKondisi->KeadaanRuangan:null), true, false, '--')?>
                            </select>
                          </td>
                        </tr>
                        <tr>
                          <td style="width: 250px">Letak</td>
                          <td>
                            <select name="kondisi-letak" class="form-control form-sm" style="width: 100%">
                                <?=GetCombobox("select * from ajbk_option where Opt_Code = '".OPT_CODE_POS."' order by Opt_Name", COL_UNIQ, COL_OPT_NAME, ($edit?$datKondisi->Letak:null), true, false, '--')?>
                            </select>
                          </td>
                        </tr>
                        <tr>
                          <td style="width: 250px">Penerangan</td>
                          <td>
                            <select name="kondisi-penerangan" class="form-control form-sm" style="width: 100%">
                                <?=GetCombobox("select * from ajbk_option where Opt_Code = '".OPT_CODE_LIGHT."' order by Opt_Name", COL_UNIQ, COL_OPT_NAME, ($edit?$datKondisi->Penerangan:null), true, false, '--')?>
                            </select>
                          </td>
                        </tr>
                        <tr>
                          <td style="width: 250px">Suara</td>
                          <td>
                            <select name="kondisi-suara" class="form-control form-sm" style="width: 100%">
                                <?=GetCombobox("select * from ajbk_option where Opt_Code = '".OPT_CODE_NOISE."' order by Opt_Name", COL_UNIQ, COL_OPT_NAME, ($edit?$datKondisi->Suara:null), true, false, '--')?>
                            </select>
                          </td>
                        </tr>
                        <tr>
                          <td style="width: 250px">Getaran</td>
                          <td>
                            <select name="kondisi-getaran" class="form-control form-sm" style="width: 100%">
                                <?=GetCombobox("select * from ajbk_option where Opt_Code = '".OPT_CODE_VIBR."' order by Opt_Name", COL_UNIQ, COL_OPT_NAME, ($edit?$datKondisi->Getaran:null), true, false, '--')?>
                            </select>
                          </td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>

              <div class="card card-default">
                <div class="card-header">
                  <h4 class="card-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#Resiko">
                      RESIKO BAHAYA
                    </a>
                  </h4>
                </div>
                <div id="Resiko" class="panel-collapse in collapse">
                  <div class="card-body p-0">
                    <input type="hidden" name="<?=COL_NM_RESIKO?>" />
                    <table id="tbl-resiko" class="table table-condensed">
                      <thead class="font-weight-bold">
                        <tr>
                          <td style="width: 40px">#</td>
                          <td>Fisik / Mental</td>
                          <td>Penyebab</td>
                        </tr>
                      </thead>
                      <tbody>
                      </tbody>
                      <tfoot>
                        <tr>
                          <td><button type="button" id="btn-add-resiko" class="btn btn-sm btn-default"><i class="fas fa-plus"></i></button></td>
                          <td>
                            <textarea name="resiko-aspek" class="form-control" placeholder="Aspek Fisik / Mental"></textarea>
                          </td>
                          <td>
                            <textarea name="resiko-penyebab" class="form-control" placeholder="Penyebab"></textarea>
                          </td>
                        </tr>
                      </tfoot>
                    </table>
                  </div>
                </div>
              </div>

              <div class="card card-default">
                <div class="card-header">
                  <h4 class="card-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#SyaratLain">
                      SYARAT JABATAN
                    </a>
                  </h4>
                </div>
                <div id="SyaratLain" class="panel-collapse in collapse">
                  <div class="card-body">
                    <div class="row">
                      <div class="col-sm-12">
                        <div class="form-group row">
                            <label class="control-label col-sm-2">Keterampilan</label>
                            <div class="col-sm-10">
                              <input name="<?=COL_NM_KETERAMPILAN?>" class="form-control" value="<?=$edit?$data[COL_NM_KETERAMPILAN]:''?>" />
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="control-label col-sm-2">Bakat</label>
                            <div class="col-sm-10">
                              <select name="<?=COL_NM_BAKAT?>[]" class="form-control" multiple>
                                <?=GetCombobox("select * from ajbk_option where Opt_Code = '".OPT_CODE_BKT."' order by Opt_Name", COL_UNIQ, array(COL_OPT_NAME, COL_OPT_DESC), ($edit?json_decode($data[COL_NM_BAKAT]):''))?>
                              </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="control-label col-sm-2">Tempramen</label>
                            <div class="col-sm-10">
                              <select name="<?=COL_NM_TEMPRAMEN?>[]" class="form-control" multiple>
                                <?=GetCombobox("select * from ajbk_option where Opt_Code = '".OPT_CODE_TEMPR."' order by Opt_Name", COL_UNIQ, array(COL_OPT_NAME, COL_OPT_DESC), ($edit?json_decode($data[COL_NM_TEMPRAMEN]):''))?>
                              </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="control-label col-sm-2">Minat</label>
                            <div class="col-sm-10">
                              <select name="<?=COL_NM_MINAT?>[]" class="form-control" multiple>
                                <?=GetCombobox("select * from ajbk_option where Opt_Code = '".OPT_CODE_MINAT."' order by Opt_Name", COL_UNIQ, array(COL_OPT_NAME, COL_OPT_DESC), ($edit?json_decode($data[COL_NM_MINAT]):''))?>
                              </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="control-label col-sm-2">Upaya Fisik</label>
                            <div class="col-sm-10">
                              <select name="<?=COL_NM_UPAYAFISIK?>[]" class="form-control" multiple>
                                <?=GetCombobox("select * from ajbk_option where Opt_Code = '".OPT_CODE_UPFISIK."' order by Opt_Name", COL_UNIQ, array(COL_OPT_NAME, COL_OPT_DESC), ($edit?json_decode($data[COL_NM_UPAYAFISIK]):''))?>
                              </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="control-label col-sm-2">Kondisi Fisik</label>
                            <div class="col-sm-10">
                              <?php
                              $datKondisiFisik = array();
                              if($edit) {
                                $datKondisiFisik = json_decode($data[COL_NM_KONDISIFISIK]);
                              }
                              ?>
                              <table id="tbl-kondisifisik" class="table table-bordered">
                                <tbody>
                                  <tr>
                                    <td style="width: 250px">Jenis Kelamin</td>
                                    <td>
                                      <select name="kondisi-jeniskelamin" class="form-control form-sm" style="width: 100%">
                                          <?=GetCombobox("select * from ajbk_option where Opt_Code = 'GENDER' order by Uniq", COL_UNIQ, COL_OPT_NAME, ($edit?$datKondisiFisik->JenisKelamin:null))?>
                                      </select>
                                    </td>
                                  </tr>
                                  <tr>
                                    <td style="width: 250px">Umur</td>
                                    <td>
                                      <input name="kondisi-umur" class="form-control" value="<?=$edit?$datKondisiFisik->Umur:''?>" />
                                    </td>
                                  </tr>
                                  <tr>
                                    <td style="width: 250px">Tinggi Badan</td>
                                    <td>
                                      <input name="kondisi-tinggi" class="form-control" value="<?=$edit?$datKondisiFisik->TinggiBadan:''?>" />
                                    </td>
                                  </tr>
                                  <tr>
                                    <td style="width: 250px">Berat Badan</td>
                                    <td>
                                      <input name="kondisi-berat" class="form-control" value="<?=$edit?$datKondisiFisik->BeratBadan:''?>" />
                                    </td>
                                  </tr>
                                  <tr>
                                    <td style="width: 250px">Postur Badan</td>
                                    <td>
                                      <input name="kondisi-postur" class="form-control" value="<?=$edit?$datKondisiFisik->PosturBadan:''?>" />
                                    </td>
                                  </tr>
                                  <tr>
                                    <td style="width: 250px">Penampilan</td>
                                    <td>
                                      <input name="kondisi-penampilan" class="form-control" value="<?=$edit?$datKondisiFisik->Penampilan:''?>" />
                                    </td>
                                  </tr>
                                </tbody>
                              </table>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="control-label col-sm-2">Fungsi Pekerjaan</label>
                            <div class="col-sm-10">
                              <textarea name="<?=COL_NM_FUNGSIPEKERJAAN?>" class="form-control"><?=$edit?$data[COL_NM_FUNGSIPEKERJAAN]:''?></textarea>
                            </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="card-footer">
            <div class="row" style="text-align: center">
              <div class="col-md-12">
                <a href="<?=site_url('ajbk/jabatan/index')?>" class="btn btn-default"><i class="fas fa-arrow-left"></i>&nbsp;KEMBALI</a>
                <?php
                if(empty($data)||empty($data[COL_VERIFIED])) {
                  ?>
                  <button type="submit" class="btn btn-primary"><i class="fad fa-save"></i>&nbsp;SIMPAN</button>
                  <?php
                }
                ?>
                <?php
                if(!empty($data)) {
                  ?>
                  <a href="<?=site_url('ajbk/jabatan/cetak/'.$data[COL_KD_JABATAN])?>" class="btn btn-success" target="_blank">
                    <i class="fad fa-print"></i>&nbsp;CETAK
                  </a>
                  <?php
                }
                ?>
              </div>
            </div>
          </div>
          <?=form_close()?>
        </div>
      </div>
    </div>
  </div>
</section>
<div class="modal fade" id="browseOPD" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Browse</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true"><i class="fa fa-close"></i></span>
        </button>
      </div>
      <div class="modal-body">
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="browseBid" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Browse</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true"><i class="fa fa-close"></i></span>
        </button>
      </div>
      <div class="modal-body">
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="browseSubBid" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Browse</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true"><i class="fa fa-close"></i></span>
        </button>
      </div>
      <div class="modal-body">
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
function TotalPegawai() {
  var rowDetUraian = $("#tbl-uraian>tbody").find("tr:not(.tr-blueprint-det)");
  var totalPeg = 0;
  var sumJam = 0;
  for (var i = 0; i < rowDetUraian.length; i++) {
      var detJumlah = $("[name=text-det-jumlah]", $(rowDetUraian[i])).val();
      var detWaktu = $("[name=text-det-waktu]", $(rowDetUraian[i])).val();
      var totalJam = toNum(detJumlah)*toNum(detWaktu);
      sumJam += totalJam;
      totalPeg += parseFloat(totalJam/<?=HOURPERYEAR?>);
  }

  $('#text-totalpeg', $('#tbl-uraian')).html((desimal(totalPeg,2)));
  $('#text-totaljam', $('#tbl-uraian')).html((desimal(sumJam,0)));
}

$(document).ready(function() {
  TotalPegawai();
  $('.modal').on('hidden.bs.modal', function (event) {
    $(this).find(".modal-body").empty();
  });
  $('.btn-browse-del').on('click', function (event) {
    var dis = $(this);
    dis.closest('.form-group').find('input').val('');
  });
  $('[data-toggle="collapse"]', $('#accordion')).click(function() {
    var dis = $(this);
    if(!$(dis.attr('href')).hasClass('show')) {
      $('.panel-collapse.show').closest('.card').find('[data-toggle="collapse"]').click();
    }
  });
  $('[name=<?=COL_KD_TYPE?>]').change(function() {
    var val = $('[name=<?=COL_KD_TYPE?>]:checked').val();
    if(val == 'STR') {
      $('.type-struktural').show();
      $('.type-fungsional').hide();
    } else if(val == 'FUNG') {
      $('.type-fungsional').show();
      $('.type-struktural').hide();
    }
  }).trigger('change');

  $('[name=<?=COL_KD_NOMENKLATUR?>]').change(function() {
    var val = $(this).val();
    if (val) {
      $.post('<?=site_url('ajbk/ajax/get-nomenklatur-detail')?>', {"Kd_Nomenklatur": val}, function(res) {
        res = JSON.parse(res);
        if(res && res.Kd_Pendidikan) {
          $('[name=<?=COL_KD_PENDIDIKAN?>]').val(res.Kd_Pendidikan).trigger('change');
        }
      });
    }
  });

  <?php
  if(!$edit) {
    ?>
    $('[name=<?=COL_KD_NOMENKLATUR?>]').trigger('change');
    <?php
  }
  ?>

  var tblUraian = $('#tbl-uraian');
  $("[name=text-det-jumlah],[name=text-det-waktu]", tblUraian).change(function () {
      var row = $(this).closest("tr");
      var detJumlah = $("[name=text-det-jumlah]", row).val();
      var detWaktu = $("[name=text-det-waktu]", row).val();
      var totalJam = toNum(detJumlah)*toNum(detWaktu);
      var totalPeg = totalJam/<?=HOURPERYEAR?>;

      $("[name=text-det-subtotal]", row).val(totalJam);
      $("[name=text-det-total]", row).val(totalPeg);
      TotalPegawai();
  });

  $(".btn-del-uraian", tblUraian).click(function () {
      var row = $(this).closest("tr");
      row.remove();
  });
  $("#btn-add-uraian", tblUraian).click(function () {
      var tbl = $(this).closest("table");
      var blueprint = tbl.find(".tr-blueprint-det", tbl).first().clone();

      blueprint.appendTo(tbl).removeClass("tr-blueprint-det").show();
      $('select', blueprint).select2({ width: '100%', theme: 'bootstrap4' });
      $('select', blueprint).removeClass('.no-select2');
      $(".uang", blueprint).number(true, 0, '.', ',');
      $(".money").number(true, 2, '.', ',');
      $(".btn-del-uraian", blueprint).click(function () {
          var row = $(this).closest("tr");
          row.remove();
      });

      $("[name=text-det-jumlah],[name=text-det-waktu]", tblUraian).change(function () {
          var row = $(this).closest("tr");
          var detJumlah = $("[name=text-det-jumlah]", row).val();
          var detWaktu = $("[name=text-det-waktu]", row).val();
          var totalJam = toNum(detJumlah)*toNum(detWaktu);
          var totalPeg = totalJam/<?=HOURPERYEAR?>;

          $("[name=text-det-subtotal]", row).val(totalJam);
          $("[name=text-det-total]", row).val(totalPeg);
          TotalPegawai();
      });
  });

  $('#browseOPD').on('show.bs.modal', function (event) {
      var modalBody = $(".modal-body", $("#browseOPD"));
      $(this).removeData('bs.modal');
      modalBody.html("<p style='font-style: italic'>Loading..</p>");
      modalBody.load("<?=site_url("sakip/ajax/browse-opd")?>", function () {
          $("[name=selID][type=hidden]", modalBody).unbind().change(function () {
              var kdSub = $(this).val().split('|');
              $("[name=Kd_Urusan]").val(kdSub[0]);
              $("[name=Kd_Bidang]").val(kdSub[1]);
              $("[name=Kd_Unit]").val(kdSub[2]);
              $("[name=Kd_Sub]").val(kdSub[3]);

              $("[name=Kd_Bid]").val("");
              $("[name=text-bid]").val("");
          });
          $("[name=selText][type=hidden]", modalBody).unbind().change(function () {
              $("[name=text-opd]").val($(this).val());
          });
      });
  });

  $('#browseBid').on('show.bs.modal', function (event) {
      var modalBody = $(".modal-body", $("#browseBid"));
      $(this).removeData('bs.modal');
      var kdUrusan = $("[name=Kd_Urusan]").val();
      var kdBidang = $("[name=Kd_Bidang]").val();
      var kdUnit = $("[name=Kd_Unit]").val();
      var kdSub = $("[name=Kd_Sub]").val();

      if(!kdUrusan || !kdBidang || !kdUnit || !kdSub) {
          modalBody.html("<p style='font-style: italic'>Silakan pilih Unit Kerja Pratama terlebih dahulu!</p>");
          return;
      }

      modalBody.html("<p style='font-style: italic'>Loading..</p>");
      modalBody.load("<?=site_url("ajbk/ajax/browse-bid")?>"+"?Kd_Urusan="+kdUrusan+"&Kd_Bidang="+kdBidang+"&Kd_Unit="+kdUnit+"&Kd_Sub="+kdSub, function () {
          $("[name=selID][type=hidden]", modalBody).unbind().change(function () {
              $("[name=Kd_Bid]").val($(this).val());

              $("[name=Kd_Subbid]").val("");
              $("[name=text-subbid]").val("");
          });
          $("[name=selText][type=hidden]", modalBody).unbind().change(function () {
              $("[name=text-bid]").val($(this).val());
          });
      });
  });

  $('#browseSubBid').on('show.bs.modal', function (event) {
      var modalBody = $(".modal-body", $("#browseSubBid"));
      $(this).removeData('bs.modal');
      var kdUrusan = $("[name=Kd_Urusan]").val();
      var kdBidang = $("[name=Kd_Bidang]").val();
      var kdUnit = $("[name=Kd_Unit]").val();
      var kdSub = $("[name=Kd_Sub]").val();
      var kdBid = $("[name=Kd_Bid]").val();

      if(!kdUrusan || !kdBidang || !kdUnit || !kdSub || !kdBid) {
          modalBody.html("<p style='font-style: italic'>Silakan pilih Unit Kerja Administrator terlebih dahulu!</p>");
          return;
      }

      modalBody.html("<p style='font-style: italic'>Loading..</p>");
      modalBody.load("<?=site_url("ajbk/ajax/browse-subbid")?>"+"?Kd_Urusan="+kdUrusan+"&Kd_Bidang="+kdBidang+"&Kd_Unit="+kdUnit+"&Kd_Sub="+kdSub+"&Kd_Bid="+kdBid, function () {
          $("[name=selID][type=hidden]", modalBody).unbind().change(function () {
              $("[name=Kd_Subbid]").val($(this).val());
          });
          $("[name=selText][type=hidden]", modalBody).unbind().change(function () {
              $("[name=text-subbid]").val($(this).val());
          });
      });
  });

  $('button[type=submit]', $('#main-form')).click(function(e) {
    var err = 0;
    $.each($('select, input', ), function(){
      if ($(this).context.attributes["required"] != undefined && $(this).val() === ""){
        err++;
      }
    });
    if(err > 0) {
      toastr.error('Mohon periksa kembali isian anda.');
      e.preventDefault();
    }
  });

  $("#main-form").validate({
    submitHandler : function(form){
        $(form).find('btn').attr('disabled',true);
        var detUraian = [];
        var rowDetUraian = $("#tbl-uraian>tbody").find("tr:not(.tr-blueprint-det)");

        for (var i = 0; i < rowDetUraian.length; i++) {
            var descDet = $("[name=text-det-desc]", $(rowDetUraian[i])).val();
            var satuanDet = $("[name=select-det-satuan]", $(rowDetUraian[i])).val();
            var jumlahDet = $("[name=text-det-jumlah]", $(rowDetUraian[i])).val();
            var waktuDet = $("[name=text-det-waktu]", $(rowDetUraian[i])).val();
            detUraian.push({
                Nm_Uraian: descDet,
                Kd_Satuan: satuanDet,
                Jlh_Beban: jumlahDet,
                Jlh_Jam: waktuDet
            });
        }
        $(".appended", $("#main-form")).remove();
        for (var i = 0; i < detUraian.length; i++) {
            var newEl = "";
            newEl += "<input type='hidden' class='appended' name=DetDesc[" + i + "] value='" + detUraian[i].Nm_Uraian + "' />";
            newEl += "<input type='hidden' class='appended' name=DetSatuan[" + i + "] value='" + detUraian[i].Kd_Satuan + "' />";
            newEl += "<input type='hidden' class='appended' name=DetJumlah[" + i + "] value='" + detUraian[i].Jlh_Beban + "' />";
            newEl += "<input type='hidden' class='appended' name=DetWaktu[" + i + "] value='" + detUraian[i].Jlh_Jam + "' />";
            $("#main-form").append(newEl);
        }
        //console.log(detUraian);

        $(form).ajaxSubmit({
            dataType: 'json',
            type : 'post',
            success : function(data){
                if(data.error != 0){
                    toastr.error(data.error);
                    console.log(data.error);
                }else{
                    window.location.href = data.redirect;
                }
            },
            error : function(a,b,c){
                toastr.error(a.responseText);
                console.log(a.responseText);
            },
            complete: function() {
              $(form).find('btn').attr('disabled',false);
            }
        });
        return false;
    }
  });

  $('[name=<?=COL_NM_BAHANKERJA?>]', $('#BahanKerja')).change(function() {
    writeBahanKerja();
  }).val(encodeURIComponent('<?=$edit?str_replace('\t', '', str_replace('\n','',preg_replace("/\s+/", " ", $data[COL_NM_BAHANKERJA]))):'[]'?>')).trigger('change');
  $('#btn-add-bahankerja', $('#tbl-bahankerja')).click(function() {
    var dis = $(this);
    var arr = $('[name=<?=COL_NM_BAHANKERJA?>]', $('#BahanKerja')).val();
    if(arr) arr = JSON.parse(decodeURIComponent(arr));
    else arr = [];

    var row = dis.closest('tr');
    var text = $('[name=bahankerja-text]', row).val();
    var kegunaan = $('[name=bahankerja-kegunaan]', row).val();
    if(text) {
      arr.push({'BahanKerja': $.trim(text), 'Kegunaan':$.trim(kegunaan)});
      $('[name=<?=COL_NM_BAHANKERJA?>]', $('#BahanKerja')).val(encodeURIComponent(JSON.stringify(arr))).trigger('change');
      $('input, textarea', row).val('');
    }
  });

  $('[name=<?=COL_NM_PERANGKATKERJA?>]', $('#Perangkat')).change(function() {
    writePerangkatKerja();
  }).val(encodeURIComponent('<?=$edit?str_replace('\t', '', str_replace('\n','',preg_replace("/\s+/", " ", $data[COL_NM_PERANGKATKERJA]))):'[]'?>')).trigger('change');
  $('#btn-add-perangkatkerja', $('#tbl-perangkatkerja')).click(function() {
    var dis = $(this);
    var arr = $('[name=<?=COL_NM_PERANGKATKERJA?>]', $('#Perangkat')).val();
    if(arr) arr = JSON.parse(decodeURIComponent(arr));
    else arr = [];

    var row = dis.closest('tr');
    var text = $('[name=perangkatkerja-text]', row).val();
    var kegunaan = $('[name=perangkatkerja-kegunaan]', row).val();
    if(text) {
      arr.push({'PerangkatKerja': $.trim(text), 'Kegunaan':$.trim(kegunaan)});
      $('[name=<?=COL_NM_PERANGKATKERJA?>]', $('#Perangkat')).val(encodeURIComponent(JSON.stringify(arr))).trigger('change');
      $('input, textarea', row).val('');
    }
  });

  $('[name=<?=COL_NM_TANGGUNGJAWAB?>]', $('#TanggungJawab')).change(function() {
    writeTanggungJawab();
  }).val(encodeURIComponent('<?=$edit?str_replace('\t', '', str_replace('\n','',preg_replace("/\s+/", " ", $data[COL_NM_TANGGUNGJAWAB]))):'[]'?>')).trigger('change');
  $('#btn-add-tanggungjawab', $('#tbl-tanggungjawab')).click(function() {
    var dis = $(this);
    var arr = $('[name=<?=COL_NM_TANGGUNGJAWAB?>]', $('#TanggungJawab')).val();
    if(arr) arr = JSON.parse(decodeURIComponent(arr));
    else arr = [];

    var row = dis.closest('tr');
    var text = $('[name=tanggungjawab-text]', row).val();
    if(text) {
      arr.push({'TanggungJawab': $.trim(text)});
      $('[name=<?=COL_NM_TANGGUNGJAWAB?>]', $('#TanggungJawab')).val(encodeURIComponent(JSON.stringify(arr))).trigger('change');
      $('input, textarea', row).val('');
    }
  });

  $('[name=<?=COL_NM_HASILKERJA?>]', $('#HasilKerja')).change(function() {
    writeHasilKerja();
  }).val(encodeURIComponent('<?=$edit?str_replace('\t', '', str_replace('\n', '', preg_replace("/\s+/", " ", $data[COL_NM_HASILKERJA]))):'[]'?>')).trigger('change');
  $('#btn-add-hasilkerja', $('#tbl-hasilkerja')).click(function() {
    var dis = $(this);
    var arr = $('[name=<?=COL_NM_HASILKERJA?>]', $('#HasilKerja')).val();
    if(arr) arr = JSON.parse(decodeURIComponent(arr));
    else arr = [];

    var row = dis.closest('tr');
    var text = $('[name=hasilkerja-text]', row).val();
    if(text) {
      arr.push({'HasilKerja': $.trim(text)});
      $('[name=<?=COL_NM_HASILKERJA?>]', $('#HasilKerja')).val(encodeURIComponent(JSON.stringify(arr))).trigger('change');
      $('input, textarea', row).val('');
    }
  });

  $('[name=<?=COL_NM_WEWENANG?>]', $('#Wewenang')).change(function() {
    writeWewenang();
  }).val(encodeURIComponent('<?=$edit?str_replace('\t', '', str_replace('\n','',preg_replace("/\s+/", " ", $data[COL_NM_WEWENANG]))):'[]'?>')).trigger('change');
  $('#btn-add-wewenang', $('#tbl-wewenang')).click(function() {
    var dis = $(this);
    var arr = $('[name=<?=COL_NM_WEWENANG?>]', $('#Wewenang')).val();
    if(arr) arr = JSON.parse(decodeURIComponent(arr));
    else arr = [];

    var row = dis.closest('tr');
    var text = $('[name=wewenang-text]', row).val();
    if(text) {
      arr.push({'Wewenang': $.trim(text)});
      $('[name=<?=COL_NM_WEWENANG?>]', $('#Wewenang')).val(encodeURIComponent(JSON.stringify(arr))).trigger('change');
      $('input, textarea', row).val('');
    }
  });

  $('[name=<?=COL_NM_KORELASI?>]', $('#Korelasi')).change(function() {
    writeKorelasi();
  }).val(encodeURIComponent('<?=$edit?str_replace('\t', '', str_replace('\n','',preg_replace("/\s+/", " ", $data[COL_NM_KORELASI]))):'[]'?>')).trigger('change');
  $('#btn-add-korelasi', $('#tbl-korelasi')).click(function() {
    var dis = $(this);
    var arr = $('[name=<?=COL_NM_KORELASI?>]', $('#Korelasi')).val();
    if(arr) arr = JSON.parse(decodeURIComponent(arr));
    else arr = [];

    var row = dis.closest('tr');
    var text = $('[name=korelasi-jabatan]', row).val();
    var kegunaan = $('[name=korelasi-instansi]', row).val();
    var hal = $('[name=korelasi-hal]', row).val();
    if(text) {
      arr.push({'Jabatan': $.trim(text), 'Instansi':$.trim(kegunaan), 'Hal':$.trim(hal)});
      $('[name=<?=COL_NM_KORELASI?>]', $('#Korelasi')).val(encodeURIComponent(JSON.stringify(arr))).trigger('change');
      $('input, textarea', row).val('');
    }
  });

  $('[name=<?=COL_NM_RESIKO?>]', $('#Resiko')).change(function() {
    writeResiko();
  }).val(encodeURIComponent('<?=$edit?str_replace('\t', '', str_replace('\n','',preg_replace("/\s+/", " ", $data[COL_NM_RESIKO]))):'[]'?>')).trigger('change');
  $('#btn-add-resiko', $('#tbl-resiko')).click(function() {
    var dis = $(this);
    var arr = $('[name=<?=COL_NM_RESIKO?>]', $('#Resiko')).val();
    if(arr) arr = JSON.parse(decodeURIComponent(arr));
    else arr = [];

    var row = dis.closest('tr');
    var text = $('[name=resiko-aspek]', row).val();
    var kegunaan = $('[name=resiko-penyebab]', row).val();
    if(text) {
      arr.push({'Aspek': $.trim(text), 'Penyebab':$.trim(kegunaan)});
      $('[name=<?=COL_NM_RESIKO?>]', $('#Resiko')).val(encodeURIComponent(JSON.stringify(arr))).trigger('change');
      $('input, textarea', row).val('');
    }
  });

});

function writeBahanKerja() {
  var tbl = $('#tbl-bahankerja>tbody');
  var arr = $('[name=<?=COL_NM_BAHANKERJA?>]', $('#BahanKerja')).val();
  if(arr && arr.length > 0) {
    arr = JSON.parse(decodeURIComponent(arr));
    var html = '';
    for (var i=0; i<arr.length; i++) {
      html += '<tr>';
      html += '<td style="padding: .75rem !important"><button type="button" class="btn btn-sm btn-outline-danger btn-del"><i class="fas fa-minus"></i></button><input type="hidden" name="idx" value="'+arr[i].BahanKerja+'" /></td>';
      html += '<td>'+arr[i].BahanKerja+'</td>';
      html += '<td>'+arr[i].Kegunaan+'</td>';
      html += '</tr>';
    }
    tbl.html(html);

    $('.btn-del', tbl).click(function() {
      var row = $(this).closest('tr');
      var idx = $('[name=idx]', row).val();
      if(idx) {
        var arr = $('[name=<?=COL_NM_BAHANKERJA?>]', $('#BahanKerja')).val();
        arr = JSON.parse(decodeURIComponent(arr));

        var arrNew = $.grep(arr, function(e){ return e.BahanKerja != idx; });
        $('[name=<?=COL_NM_BAHANKERJA?>]', $('#BahanKerja')).val(encodeURIComponent(JSON.stringify(arrNew))).trigger('change');
      }
    });
  } else {
    tbl.html('<td colspan="3"><p class="font-italic text-center m-0">Belum ada data</p></td>');
  }
}

function writePerangkatKerja() {
  var tbl = $('#tbl-perangkatkerja>tbody');
  var arr = $('[name=<?=COL_NM_PERANGKATKERJA?>]', $('#Perangkat')).val();
  if(arr && arr.length > 0) {
    arr = JSON.parse(decodeURIComponent(arr));
    var html = '';
    for (var i=0; i<arr.length; i++) {
      html += '<tr>';
      html += '<td style="padding: .75rem !important"><button type="button" class="btn btn-sm btn-outline-danger btn-del"><i class="fas fa-minus"></i></button><input type="hidden" name="idx" value="'+arr[i].PerangkatKerja+'" /></td>';
      html += '<td>'+arr[i].PerangkatKerja+'</td>';
      html += '<td>'+arr[i].Kegunaan+'</td>';
      html += '</tr>';
    }
    tbl.html(html);

    $('.btn-del', tbl).click(function() {
      var row = $(this).closest('tr');
      var idx = $('[name=idx]', row).val();
      if(idx) {
        var arr = $('[name=<?=COL_NM_PERANGKATKERJA?>]', $('#Perangkat')).val();
        arr = JSON.parse(decodeURIComponent(arr));

        var arrNew = $.grep(arr, function(e){ return e.PerangkatKerja != idx; });
        $('[name=<?=COL_NM_PERANGKATKERJA?>]', $('#Perangkat')).val(encodeURIComponent(JSON.stringify(arrNew))).trigger('change');
      }
    });
  } else {
    tbl.html('<td colspan="3"><p class="font-italic text-center m-0">Belum ada data</p></td>');
  }
}

function writeTanggungJawab() {
  var tbl = $('#tbl-tanggungjawab>tbody');
  var arr = $('[name=<?=COL_NM_TANGGUNGJAWAB?>]', $('#TanggungJawab')).val();
  if(arr && arr.length > 0) {
    arr = JSON.parse(decodeURIComponent(arr));
    var html = '';
    for (var i=0; i<arr.length; i++) {
      html += '<tr>';
      html += '<td style="padding: .75rem !important"><button type="button" class="btn btn-sm btn-outline-danger btn-del"><i class="fas fa-minus"></i></button><input type="hidden" name="idx" value="'+arr[i].TanggungJawab+'" /></td>';
      html += '<td>'+arr[i].TanggungJawab+'</td>';
      html += '</tr>';
    }
    tbl.html(html);

    $('.btn-del', tbl).click(function() {
      var row = $(this).closest('tr');
      var idx = $('[name=idx]', row).val();
      if(idx) {
        var arr = $('[name=<?=COL_NM_TANGGUNGJAWAB?>]', $('#TanggungJawab')).val();
        arr = JSON.parse(decodeURIComponent(arr));

        var arrNew = $.grep(arr, function(e){ return e.TanggungJawab != idx; });
        $('[name=<?=COL_NM_TANGGUNGJAWAB?>]', $('#TanggungJawab')).val(encodeURIComponent(JSON.stringify(arrNew))).trigger('change');
      }
    });
  } else {
    tbl.html('<td colspan="3"><p class="font-italic text-center m-0">Belum ada data</p></td>');
  }
}

function writeHasilKerja() {
  var tbl = $('#tbl-hasilkerja>tbody');
  var arr = $('[name=<?=COL_NM_HASILKERJA?>]', $('#HasilKerja')).val();
  if(arr && arr.length > 0) {
    arr = JSON.parse(decodeURIComponent(arr));
    var html = '';
    for (var i=0; i<arr.length; i++) {
      html += '<tr>';
      html += '<td style="padding: .75rem !important"><button type="button" class="btn btn-sm btn-outline-danger btn-del"><i class="fas fa-minus"></i></button><input type="hidden" name="idx" value="'+arr[i].HasilKerja+'" /></td>';
      html += '<td>'+arr[i].HasilKerja+'</td>';
      html += '</tr>';
    }
    tbl.html(html);

    $('.btn-del', tbl).click(function() {
      var row = $(this).closest('tr');
      var idx = $('[name=idx]', row).val();
      if(idx) {
        var arr = $('[name=<?=COL_NM_HASILKERJA?>]', $('#HasilKerja')).val();
        arr = JSON.parse(decodeURIComponent(arr));

        var arrNew = $.grep(arr, function(e){ return e.HasilKerja != idx; });
        $('[name=<?=COL_NM_HASILKERJA?>]', $('#HasilKerja')).val(encodeURIComponent(JSON.stringify(arrNew))).trigger('change');
      }
    });
  } else {
    tbl.html('<td colspan="3"><p class="font-italic text-center m-0">Belum ada data</p></td>');
  }
}

function writeWewenang() {
  var tbl = $('#tbl-wewenang>tbody');
  var arr = $('[name=<?=COL_NM_WEWENANG?>]', $('#Wewenang')).val();
  if(arr && arr.length > 0) {
    arr = JSON.parse(decodeURIComponent(arr));
    var html = '';
    for (var i=0; i<arr.length; i++) {
      html += '<tr>';
      html += '<td style="padding: .75rem !important"><button type="button" class="btn btn-sm btn-outline-danger btn-del"><i class="fas fa-minus"></i></button><input type="hidden" name="idx" value="'+arr[i].Wewenang+'" /></td>';
      html += '<td>'+arr[i].Wewenang+'</td>';
      html += '</tr>';
    }
    tbl.html(html);

    $('.btn-del', tbl).click(function() {
      var row = $(this).closest('tr');
      var idx = $('[name=idx]', row).val();
      if(idx) {
        var arr = $('[name=<?=COL_NM_WEWENANG?>]', $('#Wewenang')).val();
        arr = JSON.parse(decodeURIComponent(arr));

        var arrNew = $.grep(arr, function(e){ return e.Wewenang != idx; });
        $('[name=<?=COL_NM_WEWENANG?>]', $('#Wewenang')).val(encodeURIComponent(JSON.stringify(arrNew))).trigger('change');
      }
    });
  } else {
    tbl.html('<td colspan="3"><p class="font-italic text-center m-0">Belum ada data</p></td>');
  }
}

function writeKorelasi() {
  var tbl = $('#tbl-korelasi>tbody');
  var arr = $('[name=<?=COL_NM_KORELASI?>]', $('#Korelasi')).val();
  if(arr && arr.length > 0) {
    arr = JSON.parse(decodeURIComponent(arr));
    var html = '';
    for (var i=0; i<arr.length; i++) {
      html += '<tr>';
      html += '<td style="padding: .75rem !important"><button type="button" class="btn btn-sm btn-outline-danger btn-del"><i class="fas fa-minus"></i></button><input type="hidden" name="idx" value="'+arr[i].Jabatan+'" /></td>';
      html += '<td>'+arr[i].Jabatan+'</td>';
      html += '<td>'+arr[i].Instansi+'</td>';
      html += '<td>'+(arr[i].Hal?arr[i].Hal:'')+'</td>';
      html += '</tr>';
    }
    tbl.html(html);

    $('.btn-del', tbl).click(function() {
      var row = $(this).closest('tr');
      var idx = $('[name=idx]', row).val();
      if(idx) {
        var arr = $('[name=<?=COL_NM_KORELASI?>]', $('#Korelasi')).val();
        arr = JSON.parse(decodeURIComponent(arr));

        var arrNew = $.grep(arr, function(e){ return e.Jabatan != idx; });
        $('[name=<?=COL_NM_KORELASI?>]', $('#Korelasi')).val(encodeURIComponent(JSON.stringify(arrNew))).trigger('change');
      }
    });
  } else {
    tbl.html('<td colspan="3"><p class="font-italic text-center m-0">Belum ada data</p></td>');
  }
}

function writeResiko() {
  var tbl = $('#tbl-resiko>tbody');
  var arr = $('[name=<?=COL_NM_RESIKO?>]', $('#Resiko')).val();
  if(arr && arr.length > 0) {
    arr = JSON.parse(decodeURIComponent(arr));
    var html = '';
    for (var i=0; i<arr.length; i++) {
      html += '<tr>';
      html += '<td style="padding: .75rem !important"><button type="button" class="btn btn-sm btn-outline-danger btn-del"><i class="fas fa-minus"></i></button><input type="hidden" name="idx" value="'+arr[i].Aspek+'" /></td>';
      html += '<td>'+arr[i].Aspek+'</td>';
      html += '<td>'+arr[i].Penyebab+'</td>';
      html += '</tr>';
    }
    tbl.html(html);

    $('.btn-del', tbl).click(function() {
      var row = $(this).closest('tr');
      var idx = $('[name=idx]', row).val();
      if(idx) {
        var arr = $('[name=<?=COL_NM_RESIKO?>]', $('#Resiko')).val();
        arr = JSON.parse(decodeURIComponent(arr));

        var arrNew = $.grep(arr, function(e){ return e.Aspek != idx; });
        $('[name=<?=COL_NM_RESIKO?>]', $('#Resiko')).val(encodeURIComponent(JSON.stringify(arrNew))).trigger('change');
      }
    });
  } else {
    tbl.html('<td colspan="3"><p class="font-italic text-center m-0">Belum ada data</p></td>');
  }
}
</script>
