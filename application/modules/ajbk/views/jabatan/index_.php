<?php
$ruser = GetLoggedUser();
//var_dump($ruser);
//exit();
$ropd = array();
$rbid = array();
$rsubbid = array();
if($ruser[COL_ROLEID] == ROLEKADIS || $ruser[COL_ROLEID] == ROLEKABID || $ruser[COL_ROLEID] == ROLEKASUBBID) {
  $strOrg = explode('.', $ruser[COL_COMPANYID]) ;
  $ropd = $this->db->where(array(
    COL_KD_URUSAN=>$strOrg[0],
    COL_KD_BIDANG=>$strOrg[1],
    COL_KD_UNIT=>$strOrg[2],
    COL_KD_SUB=>$strOrg[3]
  ))->get(TBL_AJBK_UNIT)->row_array();

  if($ruser[COL_ROLEID] == ROLEKABID || $ruser[COL_ROLEID] == ROLEKASUBBID) {
    $rbid = $this->db->where(array(
      COL_KD_URUSAN=>$strOrg[0],
      COL_KD_BIDANG=>$strOrg[1],
      COL_KD_UNIT=>$strOrg[2],
      COL_KD_SUB=>$strOrg[3],
      COL_KD_BID=>$strOrg[4]
    ))->get(TBL_AJBK_UNIT_BID)->row_array();
  }

  if($ruser[COL_ROLEID] == ROLEKASUBBID) {
    $rsubbid = $this->db->where(array(
      COL_KD_URUSAN=>$strOrg[0],
      COL_KD_BIDANG=>$strOrg[1],
      COL_KD_UNIT=>$strOrg[2],
      COL_KD_SUB=>$strOrg[3],
      COL_KD_BID=>$strOrg[4],
      COL_KD_SUBBID=>$strOrg[5]
    ))->get(TBL_SAKIP_MSUBBID)->row_array();
  }
}
?>
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1 class="m-0 text-dark"><?= $title ?> <small> Data</small></h1>
      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="<?=site_url()?>">Home</a></li>
          <li class="breadcrumb-item active"><?=$title?></li>
        </ol>
      </div>
    </div>
  </div>
</div>
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-sm-12">
        <div class="card card-default" id="card-filter">
          <div class="card-header">
            <h3 class="card-title">UNIT KERJA</h3>
            <div class="card-tools">
              <!--<button type="button" class="btn btn-tool" data-card-widget="maximize"><i class="fas fa-expand"></i></button>-->
              <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
            </div>
          </div>
          <div class="card-body">
            <?=form_open(current_url(),array('role'=>'form','id'=>'filter-form','class'=>'form-horizontal'))?>
            <div class="form-group row">
                <label class="control-label col-sm-2">Madya</label>
                <div class="col-sm-8">
                    <input type="text" class="form-control" value="<?=$this->setting_org_name?>" readonly />
                </div>
            </div>
            <div class="form-group row">
                <label class="control-label col-sm-2">Pratama</label>
                <div class="col-sm-8">
                  <?php
                  if(!empty($ropd)) {
                    ?>
                    <input type="hidden" name="KdOPD" value="<?=$ropd[COL_UNIQ]?>" />
                    <input type="text" class="form-control" value="<?=$ropd[COL_NM_SUB_UNIT]?>" readonly />
                    <?php
                  } else {
                    ?>
                    <select name="KdOPD" class="form-control">
                      <?=GetCombobox("SELECT * FROM ref_sub_unit where IsAktif=1", COL_UNIQ, COL_NM_SUB_UNIT, null, true, false, '-- Semua --')?>
                    </select>
                    <?php
                  }
                   ?>
                </div>
            </div>
            <div class="form-group row">
                <label class="control-label col-sm-2">Administrator</label>
                <div class="col-sm-8">
                  <?php
                  if(!empty($rbid)) {
                    ?>
                    <input type="hidden" name="KdBidang" value="<?=$rbid[COL_UNIQ]?>" />
                    <input type="text" class="form-control" value="<?=$rbid[COL_NM_BID]?>" readonly />
                    <?php
                  } else {
                    ?>
                    <select name="KdBidang" class="form-control"></select>
                    <?php
                  }
                   ?>
                </div>
            </div>
            <div class="form-group row">
                <label class="control-label col-sm-2">Pengawas</label>
                <div class="col-sm-8">
                  <?php
                  if(!empty($rsubbid)) {
                    ?>
                    <input type="hidden" name="KdSubBidang" value="<?=$rsubbid[COL_UNIQ]?>" />
                    <input type="text" class="form-control" value="<?=$rsubbid[COL_NM_SUBBID]?>" readonly />
                    <?php
                  } else {
                    ?>
                    <select name="KdSubBidang" class="form-control"></select>
                    <?php
                  }
                   ?>
                </div>
            </div>
            <?=form_close()?>
          </div>
        </div>

        <div id="card-data" class="card card-default">
          <div class="card-header">
            <h3 class="card-title">
              Informasi Jabatan
            </h3>
            <div class="card-tools">
              <?=anchor('ajbk/jabatan/add','<i class="fa fa-plus"></i> INFORMASI JABATAN',array('class'=>'btn btn-tool text-primary'))?>
              <button type="button" class="btn btn-tool" data-card-widget="maximize"><i class="fas fa-expand"></i></button>
              <!--<button type="button" class="btn btn-tool btn-refresh"><i class="fas fa-sync-alt"></i></button>-->
            </div>
          </div>
          <div class="card-body">

          </div>
          <div class="overlay" style="display: none" >
            <i class="fas fa-2x fa-sync-alt fa-spin"></i>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<div class="modal fade" id="modal-duplicate" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">Duplikasi Informasi Jabatan</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true"><i class="fas fa-close"></i></span>
          </button>
        </div>
        <form id="form-duplicate" method="post">
          <div class="modal-body">
            <div class="form-group row">
              <label class="control-label col-sm-2">Nomenklatur</label>
              <div class="col-sm-10">
                <input type="text" name="Nm_Jabatan" class="form-control" readonly />
              </div>
            </div>
            <div class="row">
              <div class="col-sm-10 offset-sm-2">
                <p>
                  <strong>Catatan:</strong><br />
                  <span class="font-italic">Silakan pilih unit kerja tujuan. Duplikasi akan menyalin informasi jabatan diatas ke unit kerja yang anda pilih.</span>
                </p>
              </div>
            </div>
            <div class="form-group row">
              <label class="control-label col-sm-2">Madya</label>
              <div class="col-sm-10">
                <input type="text" class="form-control" value="<?=$this->setting_org_name?>" readonly />
              </div>
            </div>
            <div class="form-group row">
              <label class="control-label col-sm-2">Pratama</label>
              <div class="col-sm-10">
                <?php
                if(!empty($ropd)) {
                  ?>
                  <input type="hidden" name="KdOPD" value="<?=$ropd[COL_UNIQ]?>" />
                  <input type="text" class="form-control" value="<?=$ropd[COL_NM_SUB_UNIT]?>" readonly />
                  <?php
                } else {
                  ?>
                  <select name="KdOPD" class="form-control">
                    <?=GetCombobox("SELECT * FROM ref_sub_unit", COL_UNIQ, COL_NM_SUB_UNIT, null, true, false, '-- Semua --')?>
                  </select>
                  <?php
                }
                 ?>
              </div>
            </div>
            <div class="form-group row">
              <label class="control-label col-sm-2">Administrator</label>
              <div class="col-sm-10">
                <?php
                if(!empty($rbid)) {
                  ?>
                  <input type="hidden" name="KdBidang" value="<?=$rbid[COL_UNIQ]?>" />
                  <input type="text" class="form-control" value="<?=$rbid[COL_NM_BID]?>" readonly />
                  <?php
                } else {
                  ?>
                  <select name="KdBidang" class="form-control" style="width: 100%"></select>
                  <?php
                }
                 ?>
              </div>
            </div>
            <div class="form-group row">
              <label class="control-label col-sm-2">Pengawas</label>
              <div class="col-sm-10">
                <?php
                if(!empty($rsubbid)) {
                  ?>
                  <input type="hidden" name="KdSubBidang" value="<?=$rsubbid[COL_UNIQ]?>" />
                  <input type="text" class="form-control" value="<?=$rsubbid[COL_NM_SUBBID]?>" readonly />
                  <?php
                } else {
                  ?>
                  <select name="KdSubBidang" class="form-control"></select>
                  <?php
                }
                 ?>
              </div>
            </div>
          </div>
          <div class="modal-footer d-block">
            <div class="row">
              <div class="col-lg-12 text-center">
                <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="far fa-times-circle"></i>&nbsp;BATAL</button>
                <button type="submit" class="btn btn-primary btn-submit"><i class="far fa-plus-circle"></i>&nbsp;SUBMIT</button>
              </div>
            </div>
          </div>
        </form>
    </div>
  </div>
</div>
<script type="text/javascript">
function RefreshData() {
  var card = $('#card-data');
  var elOPD = $('[name=KdOPD]', $('#card-filter'));
  var elBid = $('[name=KdBidang]', $('#card-filter'));
  var elSubbid = $('[name=KdSubBidang]', $('#card-filter'));

  $('.overlay', card).toggle();
  $('.card-body', card).load('<?=site_url('ajbk/jabatan/index-partial')?>', {KdOPD: elOPD.val(), KdBidang: elBid.val(), KdSubBidang: elSubbid.val()}, function() {
    $('.overlay', card).toggle();
    $("select", card).not('.no-select2, .custom-select').select2({ width: '100%', theme: 'bootstrap4' });
    $('[data-toggle="tooltip"]', card).tooltip();
  });
}
$(document).ready(function() {
  $('[name=KdOPD],[name=KdBidang],[name=KdSubBidang]', $('#card-filter')).change(function() {
    RefreshData();
  });

  $('[name=KdOPD]', $('#card-filter')).change(function() {
    dis = $(this);
    $('select[name=KdBidang]', $('#card-filter')).load('<?=site_url("ajbk/ajax/get-opt-bidang")?>', {KdOPD: dis.val()}, function() {
      $("select[name=KdBidang]", $('#card-filter')).select2({ width: 'resolve', theme: 'bootstrap4' });
    });
  }).trigger('change');
  $('[name=KdBidang]', $('#card-filter')).change(function() {
    dis = $(this);
    $('select[name=KdSubBidang]', $('#card-filter')).load('<?=site_url("ajbk/ajax/get-opt-subbidang")?>', {KdBid: dis.val()}, function() {

    });
  }).trigger('change');
  $('[name=KdSubBidang]', $('#card-filter')).trigger('change');

  $('[name=KdOPD]', $('#form-duplicate')).change(function() {
    dis = $(this);
    $('select[name=KdBidang]', $('#form-duplicate')).load('<?=site_url("ajbk/ajax/get-opt-bidang")?>', {KdOPD: dis.val()}, function() {
      $("select[name=KdBidang]", $('#form-duplicate')).select2({ width: 'resolve', theme: 'bootstrap4' });
    });
  }).trigger('change');
  $('[name=KdBidang]', $('#form-duplicate')).change(function() {
    dis = $(this);
    $('select[name=KdSubBidang]', $('#form-duplicate')).load('<?=site_url("ajbk/ajax/get-opt-subbidang")?>', {KdBid: dis.val()}, function() {

    });
  }).trigger('change');
  $('[name=KdSubBidang]', $('#form-duplicate')).trigger('change');

  $("#form-duplicate").validate({
    submitHandler : function(form){
      $(form).find('button[type=submit]').attr('disabled',true);
      $(form).ajaxSubmit({
        dataType: 'json',
        type : 'post',
        success : function(data){
          if(data.error != 0) {
            toastr.error(data.error);
          } else {
            toastr.success(data.success);
            $(form).closest('.modal').modal('hide');
            RefreshData();
          }
        },
        error : function(a,b,c){
          toastr.error(a.responseText);
        },
        complete: function() {
          $(form).find('button[type=submit]').attr('disabled',false);
        }
      });
      return false;
    }
  });
});
</script>
