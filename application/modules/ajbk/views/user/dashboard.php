<?php
$org = $user[COL_COMPANYID];
$org_arr = array();
$ropd = array();
$rbid = array();
$rsubbid = array();
$condUnit = array();
if($user[COL_ROLEID] != ROLEADMIN && $user[COL_ROLEID] != ROLEGUEST) {
  $org_arr = explode('.', $org);
  $ropd = $this->db->where(array(
    COL_KD_URUSAN=>$org_arr[0],
    COL_KD_BIDANG=>$org_arr[1],
    COL_KD_UNIT=>$org_arr[2],
    COL_KD_SUB=>$org_arr[3]
  ))->get(TBL_REF_SUB_UNIT)->row_array();
  $condUnit = array(
    COL_KD_URUSAN=>$org_arr[0],
    COL_KD_BIDANG=>$org_arr[1],
    COL_KD_UNIT=>$org_arr[2],
    COL_KD_SUB=>$org_arr[3]
  );

  if($user[COL_ROLEID] == ROLEKABID || $user[COL_ROLEID] == ROLEKASUBBID) {
    $rbid = $this->db->where(array(
      COL_KD_URUSAN=>$org_arr[0],
      COL_KD_BIDANG=>$org_arr[1],
      COL_KD_UNIT=>$org_arr[2],
      COL_KD_SUB=>$org_arr[3],
      COL_KD_BID=>$org_arr[4]
    ))->get(TBL_SAKIP_MBID)->row_array();

    $condUnit[COL_KD_BID] = $org_arr[4];
  }
  if($user[COL_ROLEID] == ROLEKASUBBID) {
    $rsubbid = $this->db->where(array(
      COL_KD_URUSAN=>$org_arr[0],
      COL_KD_BIDANG=>$org_arr[1],
      COL_KD_UNIT=>$org_arr[2],
      COL_KD_SUB=>$org_arr[3],
      COL_KD_BID=>$org_arr[4],
      COL_KD_SUBBID=>$org_arr[5]
    ))->get(TBL_SAKIP_MSUBBID)->row_array();

    $condUnit[COL_KD_SUBBID] = $org_arr[5];
  }
}

$countJabatan = $this->db
->where($condUnit)
->count_all_results(TBL_AJBK_JABATAN);

$rBebanKerja = $this->db
->select('sum(ajbk_jabatan_uraian.Jlh_Beban*ajbk_jabatan_uraian.Jlh_Jam) as Beban')
->where($condUnit)
->join(TBL_AJBK_JABATAN,TBL_AJBK_JABATAN.'.'.COL_KD_JABATAN." = ".TBL_AJBK_JABATAN_URAIAN.".".COL_KD_JABATAN,"left")
->get(TBL_AJBK_JABATAN_URAIAN)
->row_array();

$rBezetting = $this->db
->select('sum(ajbk_jabatan_bezetting.Jlh_Pegawai) as Bezetting')
->where($condUnit)
->where(COL_TAHUN, date('Y'))
->join(TBL_AJBK_JABATAN,TBL_AJBK_JABATAN.'.'.COL_KD_JABATAN." = ".TBL_AJBK_JABATAN_BEZETTING.".".COL_KD_JABATAN,"left")
->get(TBL_AJBK_JABATAN_BEZETTING)
->row_array();

$rBebanPegawai = $this->db
->select('sum(ajbk_jabatan_uraian.Jlh_Beban*ajbk_jabatan_uraian.Jlh_Jam) as Beban')
->where($condUnit)
->join(TBL_AJBK_JABATAN,TBL_AJBK_JABATAN.'.'.COL_KD_JABATAN." = ".TBL_AJBK_JABATAN_URAIAN.".".COL_KD_JABATAN,"left")
->group_by(TBL_AJBK_JABATAN.'.'.COL_KD_JABATAN)
->get(TBL_AJBK_JABATAN_URAIAN)
->result_array();
$kebutuhanPeg = 0;
foreach ($rBebanPegawai as $b) {
  $kebutuhanPeg += round($b["Beban"]/HOURPERYEAR);
}
?>
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1 class="m-0 text-dark"><?=$title?></h1>

      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li id="datetime" class="breadcrumb-item active"></li>
        </ol>
      </div>
    </div>
  </div>
</div>
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <!--<div class="col-12 col-sm-6 col-md-3">
        <div class="info-box mb-3">
          <span class="info-box-icon bg-info elevation-1"><i class="fas fa-cog"></i></span>
          <div class="info-box-content">
            <span class="info-box-text">CPU Traffic</span>
            <span class="info-box-number">
              10
              <small>%</small>
            </span>
          </div>
        </div>
      </div>

      <div class="col-12 col-sm-6 col-md-3">
        <div class="info-box mb-3">
          <span class="info-box-icon bg-danger elevation-1"><i class="fas fa-thumbs-up"></i></span>

          <div class="info-box-content">
            <span class="info-box-text">Likes</span>
            <span class="info-box-number">41,410</span>
          </div>
        </div>
      </div>

      <div class="col-12 col-sm-6 col-md-3">
        <div class="info-box mb-3">
          <span class="info-box-icon bg-success elevation-1"><i class="fas fa-shopping-cart"></i></span>

          <div class="info-box-content">
            <span class="info-box-text">Sales</span>
            <span class="info-box-number">760</span>
          </div>
        </div>
      </div>

      <div class="col-12 col-sm-6 col-md-3">
        <div class="info-box mb-3">
          <span class="info-box-icon bg-warning elevation-1"><i class="fas fa-users"></i></span>

          <div class="info-box-content">
            <span class="info-box-text">New Members</span>
            <span class="info-box-number">2,000</span>
          </div>
        </div>
      </div>-->

      <div class="col-lg-12 col-sm-6 col-md-3">
        <div class="card card-outline card-primary">
          <div class="card-header">
            <h3 class="card-title">Rekapitulasi Data <span style="text-decoration: underline"><?=date('Y')?></span></h3>
            <div class="card-tools">
              <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
              </button>
            </div>
          </div>
          <div class="card-body p-0">
            <?php
            $txt = anchor(site_url('ajbk/data'),number_format(rand(1,50), 0))." Unit Kerja, ".anchor(site_url('ajbk/data'),number_format(rand(50,1000), 0))." Jabatan";
            ?>
            <table class="table table-bordered">
              <tbody>
                <tr>
                  <td>JPT Madya</td>
                  <td style="width: 10px">:</td>
                  <td>
                    <?=$this->setting_org_name?>
                  </td>
                </tr>
                <tr>
                  <td>JPT Pratama</td>
                  <td style="width: 10px">:</td>
                  <td>
                    <?=!empty($ropd)?$ropd[COL_NM_SUB_UNIT]:'-'?>
                  </td>
                </tr>
                 <tr>
                   <td>Administrator</td>
                   <td style="width: 10px">:</td>
                   <td>
                     <?=!empty($rbid)?$rbid[COL_NM_BID]:'-'?>
                   </td>
                 </tr>
                 <tr>
                   <td>Pengawas</td>
                   <td style="width: 10px">:</td>
                   <td>
                     <?=!empty($rsubbid)?$rsubbid[COL_NM_SUBBID]:'-'?>
                   </td>
                 </tr>
                 <tr>
                   <td>Pelaksana</td>
                   <td style="width: 10px">:</td>
                   <td>-</td>
                 </tr>
              </tbody>
            </table>
          </div>
          <div class="card-footer">
            <div class="row">
              <div class="col-sm-6 col-lg-3">
                <div class="description-block border-right">
                  <span class="description-percentage text-warning"><?=number_format($countJabatan, 0)?></span>
                  <h5 class="description-header">JABATAN</h5>
                  <!--<span class="description-text"></span>-->
                </div>
              </div>
              <div class="col-sm-6 col-lg-3">
                <div class="description-block border-right">
                  <span class="description-percentage text-primary"><?=number_format(($rBebanKerja?$rBebanKerja["Beban"]:0), 0)?></span>
                  <h5 class="description-header">BEBAN KERJA&nbsp;<small>(Jam)</small></h5>
                  <!--<span class="description-text"></span>-->
                </div>
              </div>
              <div class="col-sm-6 col-lg-3">
                <div class="description-block border-right">
                  <span class="description-percentage text-success"><?=number_format(($rBezetting?$rBezetting["Bezetting"]:0), 0)?></span>
                  <h5 class="description-header">BEZETTING&nbsp;<small>(Orang)</small></h5>
                  <!--<span class="description-text"></span>-->
                </div>
              </div>
              <div class="col-sm-6 col-lg-3">
                <div class="description-block border-right">
                  <span class="description-percentage text-danger"><?=number_format($kebutuhanPeg, 0)?></span>
                  <h5 class="description-header">KEBUTUHAN&nbsp;<small>(Orang)</small></h5>
                  <!--<span class="description-text"></span>-->
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<script>
function startTime() {
  $('#datetime').load('<?=site_url('ajbk/ajax/now')?>', function(data){

  });
  var t = setTimeout(startTime, 500);
}
$(document).ready(function(){
  startTime();
});
</script>
