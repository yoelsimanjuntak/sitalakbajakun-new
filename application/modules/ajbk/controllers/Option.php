<?php
class Option extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        if (!IsLogin()) {
            redirect('site/user/login');
        }
        if (GetLoggedUser()[COL_ROLEID]!=ROLEADMIN) {
            show_error('Anda tidak memiliki akses terhadap modul ini.');
            return;
        }
    }

    public function opd_index()
    {
        $data['title'] = "OPD";
        $this->db->order_by(TBL_REF_SUB_UNIT.".".COL_NM_SUB_UNIT, 'asc');
        $data['res'] = $this->db->get(TBL_REF_SUB_UNIT)->result_array();
        $this->template->load('backend', 'master/index_opd', $data);
    }

    public function opd_add()
    {
        $data['title'] = 'OPD';
        $data['edit'] = false;
        if (!empty($_POST)) {
            $data['data'] = $_POST;
            $rec = array(
              COL_KD_URUSAN=>$this->input->post(COL_KD_URUSAN),
              COL_KD_BIDANG=>$this->input->post(COL_KD_BIDANG),
              COL_KD_UNIT=>$this->input->post(COL_KD_UNIT),
              COL_KD_SUB=>$this->input->post(COL_KD_SUB),
              COL_NM_SUB_UNIT=>$this->input->post(COL_NM_SUB_UNIT),
              COL_NM_PIMPINAN=>$this->input->post(COL_NM_PIMPINAN)
            );

            $res = $this->db->insert(TBL_REF_SUB_UNIT, $rec);
            if ($res) {
                redirect('site/master/opd-index');
            } else {
                $this->template->load('backend', 'master/form_opd', $data);
            }
        } else {
            $this->template->load('backend', 'master/form_opd', $data);
        }
    }

    public function opd_edit($id)
    {
        $data['title'] = 'OPD';
        $data['edit'] = true;

        $data['data'] = $rdata = $this->db->where(COL_UNIQ, $id)->get(TBL_REF_SUB_UNIT)->row_array();
        if (empty($rdata)) {
            show_404();
            return;
        }
        if (!empty($_POST)) {
            $data['data'] = $_POST;
            $rec = array(
              COL_KD_URUSAN=>$this->input->post(COL_KD_URUSAN),
              COL_KD_BIDANG=>$this->input->post(COL_KD_BIDANG),
              COL_KD_UNIT=>$this->input->post(COL_KD_UNIT),
              COL_KD_SUB=>$this->input->post(COL_KD_SUB),
              COL_NM_SUB_UNIT=>$this->input->post(COL_NM_SUB_UNIT),
              COL_NM_PIMPINAN=>$this->input->post(COL_NM_PIMPINAN)
            );

            $res = $this->db->where(COL_UNIQ, $id)->update(TBL_REF_SUB_UNIT, $rec);
            if ($res) {
                redirect('site/master/opd-index');
            } else {
                $this->template->load('backend', 'master/form_opd', $data);
            }
        } else {
            $this->template->load('backend', 'master/form_opd', $data);
        }
    }

    public function opd_delete()
    {
        $data = $this->input->post('cekbox');
        $deleted = 0;
        foreach ($data as $datum) {
            $this->db->delete(TBL_REF_SUB_UNIT, array(COL_UNIQ => $datum));
            $deleted++;
        }
        if ($deleted) {
            ShowJsonSuccess($deleted." data dihapus");
        } else {
            ShowJsonError("Tidak ada dihapus");
        }
    }
}
