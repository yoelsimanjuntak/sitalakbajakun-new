<?php
class Jabatan extends MY_Controller
{
  public function __construct()
  {
      parent::__construct();
      if (!IsLogin()) {
          redirect('site/home');
      }
  }

  public function index()
  {
      $ruser = GetLoggedUser();
      $strOPD = explode('.', $ruser[COL_COMPANYID]);
      $data['title'] = 'Informasi Jabatan';
      $this->template->load('backend', 'ajbk/jabatan/index_', $data);
  }

  public function index_partial($_tipe='', $_opd='', $_bid=-1, $noaction = 0)
  {
    $hourperyear = HOURPERYEAR;
    $kdOPD_ = !empty($this->input->post("KdOPD")) ? $this->input->post("KdOPD") : $_opd;
    $kdBidang_ = !empty($this->input->post("KdBidang")) ? $this->input->post("KdBidang") : ($_bid > 0 ? $_bid : null);
    $kdSubBidang_ = $this->input->post("KdSubBidang");
    $tipe_ = !empty($this->input->post("tipe")) ? $this->input->post("tipe") : $_tipe;

    if(!empty($kdSubBidang_)) {
      $rsubbid = $this->db->where(COL_UNIQ, $kdSubBidang_)->get(TBL_AJBK_UNIT_SUBBID)->row_array();
      if(empty($rsubbid)) {
        echo 'Filter tidak valid.';
        return;
      }

      $kdUrusan = $rsubbid[COL_KD_URUSAN];
      $kdBidang = $rsubbid[COL_KD_BIDANG];
      $kdUnit = $rsubbid[COL_KD_UNIT];
      $kdSub = $rsubbid[COL_KD_SUB];
      $kdBid = $rsubbid[COL_KD_BID];
      $kdSubbid = $rsubbid[COL_KD_SUBBID];
    } else if(!empty($kdBidang_)) {
      $rbid = $this->db->where(COL_UNIQ, $kdBidang_)->get(TBL_AJBK_UNIT_BID)->row_array();
      if(empty($rbid)) {
        echo 'Filter tidak valid.';
        return;
      }

      $kdUrusan = $rbid[COL_KD_URUSAN];
      $kdBidang = $rbid[COL_KD_BIDANG];
      $kdUnit = $rbid[COL_KD_UNIT];
      $kdSub = $rbid[COL_KD_SUB];
      $kdBid = $rbid[COL_KD_BID];
    } else if(!empty($kdOPD_)) {
      $ropd = $this->db->where(COL_UNIQ, $kdOPD_)->get(TBL_AJBK_UNIT)->row_array();
      if(empty($ropd)) {
        echo 'Filter tidak valid.';
        return;
      }

      $kdUrusan = $ropd[COL_KD_URUSAN];
      $kdBidang = $ropd[COL_KD_BIDANG];
      $kdUnit = $ropd[COL_KD_UNIT];
      $kdSub = $ropd[COL_KD_SUB];
    }

    $this->db->select("
    ajbk_jabatan.*,
    COALESCE(ajbk_jabatan.Nm_Jabatan, ajbk_nomenklatur.Nm_Nomenklatur) as Nm_Jabatan,
    ajbk_unit.Nm_Sub_Unit,
    ajbk_unit_bid.Nm_Bid,
    ajbk_unit_subbid.Nm_Subbid,
    (select count(*) from ajbk_jabatan_uraian u where u.Kd_Jabatan = ajbk_jabatan.Kd_Jabatan) as Uraian,
    (select sum(Jlh_Beban*Jlh_Jam) from ajbk_jabatan_uraian u where u.Kd_Jabatan = ajbk_jabatan.Kd_Jabatan) as Beban,
    (select sum(Jlh_Beban*Jlh_Jam) from ajbk_jabatan_uraian u where u.Kd_Jabatan = ajbk_jabatan.Kd_Jabatan)/$hourperyear as Pegawai,
    CONVERT(ajbk_jabatan.KelasJab, DECIMAL) as Kelas
    ");
    if(isset($kdUrusan)) $this->db->where(TBL_AJBK_JABATAN.".".COL_KD_URUSAN, $kdUrusan);
    if(isset($kdBidang)) $this->db->where(TBL_AJBK_JABATAN.".".COL_KD_BIDANG, $kdBidang);
    if(isset($kdUnit)) $this->db->where(TBL_AJBK_JABATAN.".".COL_KD_UNIT, $kdUnit);
    if(isset($kdSub)) $this->db->where(TBL_AJBK_JABATAN.".".COL_KD_SUB, $kdSub);
    if(isset($kdBid)) $this->db->where(TBL_AJBK_JABATAN.".".COL_KD_BID, $kdBid);
    if(isset($kdSubbid)) $this->db->where(TBL_AJBK_JABATAN.".".COL_KD_SUBBID, $kdSubbid);
    if(!empty($tipe_)) $this->db->where(TBL_AJBK_JABATAN.".".COL_KD_TYPE, $tipe_);
    if($noaction && $_opd == 20 && $_bid <= 0) {
      $this->db->where_not_in(TBL_AJBK_JABATAN.".".COL_KD_BID, array(10,11,12,13,14));
    }
    if($noaction && $_opd == 2 && $_bid <= 0) {
      $this->db
      ->group_start()
      ->where_not_in(TBL_AJBK_JABATAN.".".COL_KD_BID, array(5,6,7,8,9,10,11,12,13,14,15))
      ->or_where(TBL_AJBK_JABATAN.".".COL_KD_BID,null)
      ->group_end();
    }
    if($noaction && $_opd == 1 && $_bid <= 0) {
      $this->db
      ->group_start()
      ->where_in(TBL_AJBK_JABATAN.".".COL_KD_BID, array(1,2,3,4))
      ->or_where(TBL_AJBK_JABATAN.".".COL_KD_BID,null)
      ->group_end();
    }
    $this->db->join(TBL_AJBK_NOMENKLATUR,TBL_AJBK_NOMENKLATUR.'.'.COL_KD_NOMENKLATUR." = ".TBL_AJBK_JABATAN.".".COL_KD_NOMENKLATUR,"left");
    $this->db->join(TBL_AJBK_UNIT,
    TBL_AJBK_UNIT.'.'.COL_KD_URUSAN." = ".TBL_AJBK_JABATAN.".".COL_KD_URUSAN." AND ".
    TBL_AJBK_UNIT.'.'.COL_KD_BIDANG." = ".TBL_AJBK_JABATAN.".".COL_KD_BIDANG." AND ".
    TBL_AJBK_UNIT.'.'.COL_KD_UNIT." = ".TBL_AJBK_JABATAN.".".COL_KD_UNIT." AND ".
    TBL_AJBK_UNIT.'.'.COL_KD_SUB." = ".TBL_AJBK_JABATAN.".".COL_KD_SUB
    ,"left");
    $this->db->join(TBL_AJBK_UNIT_BID,
    TBL_AJBK_UNIT_BID.'.'.COL_KD_URUSAN." = ".TBL_AJBK_JABATAN.".".COL_KD_URUSAN." AND ".
    TBL_AJBK_UNIT_BID.'.'.COL_KD_BIDANG." = ".TBL_AJBK_JABATAN.".".COL_KD_BIDANG." AND ".
    TBL_AJBK_UNIT_BID.'.'.COL_KD_UNIT." = ".TBL_AJBK_JABATAN.".".COL_KD_UNIT." AND ".
    TBL_AJBK_UNIT_BID.'.'.COL_KD_SUB." = ".TBL_AJBK_JABATAN.".".COL_KD_SUB." AND ".
    TBL_AJBK_UNIT_BID.'.'.COL_KD_BID." = ".TBL_AJBK_JABATAN.".".COL_KD_BID
    ,"left");
    $this->db->join(TBL_AJBK_UNIT_SUBBID,
    TBL_AJBK_UNIT_SUBBID.'.'.COL_KD_URUSAN." = ".TBL_AJBK_JABATAN.".".COL_KD_URUSAN." AND ".
    TBL_AJBK_UNIT_SUBBID.'.'.COL_KD_BIDANG." = ".TBL_AJBK_JABATAN.".".COL_KD_BIDANG." AND ".
    TBL_AJBK_UNIT_SUBBID.'.'.COL_KD_UNIT." = ".TBL_AJBK_JABATAN.".".COL_KD_UNIT." AND ".
    TBL_AJBK_UNIT_SUBBID.'.'.COL_KD_SUB." = ".TBL_AJBK_JABATAN.".".COL_KD_SUB." AND ".
    TBL_AJBK_UNIT_SUBBID.'.'.COL_KD_BID." = ".TBL_AJBK_JABATAN.".".COL_KD_BID." AND ".
    TBL_AJBK_UNIT_SUBBID.'.'.COL_KD_SUBBID." = ".TBL_AJBK_JABATAN.".".COL_KD_SUBBID
    ,"left");

    $data['noaction'] = $noaction;
    $data['res'] = $this->db
    //->order_by(TBL_AJBK_JABATAN.'.'.COL_KD_TYPE,'desc')
    //->order_by('ajbk_unit.Nm_Sub_Unit', 'asc')
    //->order_by('ajbk_unit_bid.Nm_Bid', 'asc')
    //->order_by('ajbk_unit_subbid.Nm_Subbid', 'asc')
    ->order_by('Kelas', 'desc')
    ->order_by('ajbk_jabatan.Nm_Jabatan', 'asc')
    ->order_by('ajbk_nomenklatur.Nm_Nomenklatur', 'asc')
    ->group_by('ajbk_jabatan.Kd_Jabatan')
    ->get(TBL_AJBK_JABATAN)
    ->result_array();

    //echo $this->db->last_query();
    //exit();
    $this->load->view('ajbk/jabatan/index_partial', $data);
  }

  public function add() {
    $data['title'] = 'Informasi Jabatan';
    $data['edit'] = false;

    $ruser = GetLoggedUser();
    if(!empty($_POST)) {
      $resp = array();
      $resp['error'] = 0;
      $resp['success'] = 1;
      $resp['redirect'] = site_url('ajbk/jabatan/index');
      $cond = array(
        "TempatKerja" => $this->input->post("kondisi-tempatkerja"),
        "Suhu" => $this->input->post("kondisi-suhu"),
        "Udara" => $this->input->post("kondisi-udara"),
        "KeadaanRuangan" => $this->input->post("kondisi-cond"),
        "Letak" => $this->input->post("kondisi-letak"),
        "Penerangan" => $this->input->post("kondisi-penerangan"),
        "Suara" => $this->input->post("kondisi-suara"),
        "Getaran" => $this->input->post("kondisi-getaran")
      );
      $condFisik = array(
        "JenisKelamin" => $this->input->post("kondisi-jeniskelamin"),
        "Umur" => $this->input->post("kondisi-umur"),
        "TinggiBadan" => $this->input->post("kondisi-tinggi"),
        "BeratBadan" => $this->input->post("kondisi-berat"),
        "PosturBadan" => $this->input->post("kondisi-postur"),
        "Penampilan" => $this->input->post("kondisi-penampilan")
      );

      $data = array(
          COL_KD_URUSAN => $this->input->post(COL_KD_URUSAN),
          COL_KD_BIDANG => $this->input->post(COL_KD_BIDANG),
          COL_KD_UNIT => $this->input->post(COL_KD_UNIT),
          COL_KD_SUB => $this->input->post(COL_KD_SUB),
          COL_KD_BID => $this->input->post(COL_KD_BID)?$this->input->post(COL_KD_BID):null,
          COL_KD_SUBBID => $this->input->post(COL_KD_SUBBID)?$this->input->post(COL_KD_SUBBID):null,

          COL_ID_JABATAN => $this->input->post(COL_ID_JABATAN),
          COL_KD_TYPE => $this->input->post(COL_KD_TYPE),
          COL_NM_IKHTISAR => $this->input->post(COL_NM_IKHTISAR),
          COL_KD_PENDIDIKAN => $this->input->post(COL_KD_PENDIDIKAN),
          COL_KD_PENGALAMAN => $this->input->post(COL_KD_PENGALAMAN),
          COL_KELASJAB => $this->input->post(COL_KELASJAB),

          COL_NM_BAHANKERJA => $this->input->post(COL_NM_BAHANKERJA) ? urldecode($this->input->post(COL_NM_BAHANKERJA)) : null,
          COL_NM_HASILKERJA => $this->input->post(COL_NM_HASILKERJA) ? urldecode($this->input->post(COL_NM_HASILKERJA)) : null,
          COL_NM_PERANGKATKERJA => $this->input->post(COL_NM_PERANGKATKERJA) ? urldecode($this->input->post(COL_NM_PERANGKATKERJA)) : null,
          COL_NM_TANGGUNGJAWAB => $this->input->post(COL_NM_TANGGUNGJAWAB) ? urldecode($this->input->post(COL_NM_TANGGUNGJAWAB)) : null,
          COL_NM_WEWENANG => $this->input->post(COL_NM_WEWENANG) ? urldecode($this->input->post(COL_NM_WEWENANG)) : null,
          COL_NM_KORELASI => $this->input->post(COL_NM_KORELASI) ? urldecode($this->input->post(COL_NM_KORELASI)) : null,
          COL_NM_KONDISI => json_encode($cond),
          COL_NM_RESIKO => $this->input->post(COL_NM_RESIKO) ? urldecode($this->input->post(COL_NM_RESIKO)) : null,
          COL_NM_KETERAMPILAN => $this->input->post(COL_NM_KETERAMPILAN),
          COL_NM_BAKAT => json_encode($this->input->post(COL_NM_BAKAT)),
          COL_NM_TEMPRAMEN => json_encode($this->input->post(COL_NM_TEMPRAMEN)),
          COL_NM_MINAT => json_encode($this->input->post(COL_NM_MINAT)),
          COL_NM_UPAYAFISIK => json_encode($this->input->post(COL_NM_UPAYAFISIK)),
          COL_NM_KONDISIFISIK => json_encode($condFisik),
          COL_NM_FUNGSIPEKERJAAN => $this->input->post(COL_NM_FUNGSIPEKERJAAN) ? urldecode($this->input->post(COL_NM_FUNGSIPEKERJAAN)) : null,

          COL_CREATE_BY => $ruser[COL_USERNAME],
          COL_CREATE_DATE => date('Y-m-d H:i:s')
      );
      if($data[COL_KD_TYPE]=='STR') {
        $data[COL_NM_JABATAN] = $this->input->post(COL_NM_JABATAN);
        $data[COL_KD_NOMENKLATUR] = null;
      } else if($data[COL_KD_TYPE]=='FUNG') {
        $data[COL_NM_JABATAN] =  null;
        $data[COL_KD_NOMENKLATUR] = $this->input->post(COL_KD_NOMENKLATUR);
      }

      $this->db->trans_begin();
      try {
        /* CHECK OPD */
        $ropd = $this->db->where(array(
          COL_KD_URUSAN=>$this->input->post(COL_KD_URUSAN),
          COL_KD_BIDANG=>$this->input->post(COL_KD_BIDANG),
          COL_KD_UNIT=>$this->input->post(COL_KD_UNIT),
          COL_KD_SUB=>$this->input->post(COL_KD_SUB)
        ))
        ->get(TBL_AJBK_UNIT)
        ->row_array();
        if(!$ropd) {
          throw new Exception("OPD tidak valid!");
        }
        if($ropd[COL_ISAKTIF]!=1) {
          throw new Exception("Gagal menginput data. OPD dalam keadaan SUSPEND / NONAKTIF!");
        }
        /* --CHECK OPD-- */

        $res = $this->db->insert(TBL_AJBK_JABATAN, $data);
        if(!$res) {
          $err = $this->db->error();
          throw new Exception("Database error: ".$err['message']);
        }

        $kdJabatan = $this->db->insert_id();
        $detUraian = $this->input->post("DetDesc");
        $detSatuan = $this->input->post("DetSatuan");
        $detJumlah = $this->input->post("DetJumlah");
        $detWaktu = $this->input->post("DetWaktu");
        $jurusan = $this->input->post(COL_KD_JURUSAN);
        $diklat = $this->input->post(COL_KD_DIKLAT);

        $arrJurusan = array();
        $arrDiklat = array();
        $det = array();
        for($i = 0; $i<count($detUraian); $i++) {
            $det[] = array(
                COL_KD_JABATAN => $kdJabatan,
                COL_NM_URAIAN => $detUraian[$i],
                COL_KD_SATUAN => $detSatuan[$i],
                COL_JLH_BEBAN => $detJumlah[$i],
                COL_JLH_JAM => $detWaktu[$i]
            );
        }
        if(!empty($jurusan)) {
          foreach($jurusan as $s) {
            $arrJurusan[] = array(COL_KD_JABATAN=>$kdJabatan, COL_KD_JURUSAN=>$s);
          }
        }

        if(!empty($diklat)) {
          foreach($diklat as $s) {
            $arrDiklat[] = array(COL_KD_JABATAN=>$kdJabatan, COL_KD_DIKLAT=>$s);
          }
        }

        if(count($det) > 0) {
            $resDet = $this->db->insert_batch(TBL_AJBK_JABATAN_URAIAN, $det);
            if(!$resDet) {
              $err = $this->db->error();
              throw new Exception("Database error: ".$err['message']);
            }
        }
        if(count($arrJurusan) > 0) {
            $resDet = $this->db->insert_batch(TBL_AJBK_JABATAN_JURUSAN, $arrJurusan);
            if(!$resDet) {
              $err = $this->db->error();
              throw new Exception("Database error: ".$err['message']);
            }
        }
        if(count($arrDiklat) > 0) {
            $resDet = $this->db->insert_batch(TBL_AJBK_JABATAN_DIKLAT, $arrDiklat);
            if(!$resDet) {
              $err = $this->db->error();
              throw new Exception("Database error: ".$err['message']);
            }
        }

        $this->db->trans_commit();
        echo json_encode($resp);
        return;
      } catch (Exception $e) {
          $this->db->trans_rollback();
          $resp['error'] = $e->getMessage();
          $resp['success'] = 0;
          echo json_encode($resp);
          return;
      }
    } else {
      $this->template->load('backend', 'ajbk/jabatan/form_', $data);
    }
  }

  public function edit($id) {
    $ruser = GetLoggedUser();
    $strOPD = explode('.', $ruser[COL_COMPANYID]);

    if($ruser[COL_ROLEID] != ROLEADMIN) {
        $this->db->where(TBL_AJBK_JABATAN.".".COL_KD_URUSAN, $strOPD[0]);
        $this->db->where(TBL_AJBK_JABATAN.".".COL_KD_BIDANG, $strOPD[1]);
        $this->db->where(TBL_AJBK_JABATAN.".".COL_KD_UNIT, $strOPD[2]);
        $this->db->where(TBL_AJBK_JABATAN.".".COL_KD_SUB, $strOPD[3]);
        if($ruser[COL_ROLEID] == ROLEKABID) $this->db->where(TBL_AJBK_JABATAN.".".COL_KD_BID, $strOPD[4]);
        if($ruser[COL_ROLEID] == ROLEKASUBBID) $this->db->where(TBL_AJBK_JABATAN.".".COL_KD_SUBBID, $strOPD[5]);
    }
    $this->db->join(TBL_AJBK_UNIT,
    TBL_AJBK_UNIT.'.'.COL_KD_URUSAN." = ".TBL_AJBK_JABATAN.".".COL_KD_URUSAN." AND ".
    TBL_AJBK_UNIT.'.'.COL_KD_BIDANG." = ".TBL_AJBK_JABATAN.".".COL_KD_BIDANG." AND ".
    TBL_AJBK_UNIT.'.'.COL_KD_UNIT." = ".TBL_AJBK_JABATAN.".".COL_KD_UNIT." AND ".
    TBL_AJBK_UNIT.'.'.COL_KD_SUB." = ".TBL_AJBK_JABATAN.".".COL_KD_SUB
    ,"left");
    $this->db->join(TBL_AJBK_UNIT_BID,
    TBL_AJBK_UNIT_BID.'.'.COL_KD_URUSAN." = ".TBL_AJBK_JABATAN.".".COL_KD_URUSAN." AND ".
    TBL_AJBK_UNIT_BID.'.'.COL_KD_BIDANG." = ".TBL_AJBK_JABATAN.".".COL_KD_BIDANG." AND ".
    TBL_AJBK_UNIT_BID.'.'.COL_KD_UNIT." = ".TBL_AJBK_JABATAN.".".COL_KD_UNIT." AND ".
    TBL_AJBK_UNIT_BID.'.'.COL_KD_SUB." = ".TBL_AJBK_JABATAN.".".COL_KD_SUB." AND ".
    TBL_AJBK_UNIT_BID.'.'.COL_KD_BID." = ".TBL_AJBK_JABATAN.".".COL_KD_BID
    ,"left");
    $this->db->join(TBL_AJBK_UNIT_SUBBID,
    TBL_AJBK_UNIT_SUBBID.'.'.COL_KD_URUSAN." = ".TBL_AJBK_JABATAN.".".COL_KD_URUSAN." AND ".
    TBL_AJBK_UNIT_SUBBID.'.'.COL_KD_BIDANG." = ".TBL_AJBK_JABATAN.".".COL_KD_BIDANG." AND ".
    TBL_AJBK_UNIT_SUBBID.'.'.COL_KD_UNIT." = ".TBL_AJBK_JABATAN.".".COL_KD_UNIT." AND ".
    TBL_AJBK_UNIT_SUBBID.'.'.COL_KD_SUB." = ".TBL_AJBK_JABATAN.".".COL_KD_SUB." AND ".
    TBL_AJBK_UNIT_SUBBID.'.'.COL_KD_BID." = ".TBL_AJBK_JABATAN.".".COL_KD_BID." AND ".
    TBL_AJBK_UNIT_SUBBID.'.'.COL_KD_SUBBID." = ".TBL_AJBK_JABATAN.".".COL_KD_SUBBID
    ,"left");

    $this->db->select("ajbk_jabatan.*, ajbk_unit.Nm_Sub_Unit, ajbk_unit_bid.Nm_Bid, ajbk_unit_subbid.Nm_Subbid");
    $data['data'] = $rdata = $this->db->where(TBL_AJBK_JABATAN.".".COL_KD_JABATAN, $id)->get(TBL_AJBK_JABATAN)->row_array();
    if(empty($rdata)){
        show_404();
        return;
    }

    $data['title'] = "Informasi Jabatan";
    $data['edit'] = TRUE;
    if(!empty($_POST)) {
      $resp = array();
      $resp['error'] = 0;
      $resp['success'] = 1;
      $resp['redirect'] = site_url('ajbk/jabatan');

      $cond = array(
        "TempatKerja" => $this->input->post("kondisi-tempatkerja"),
        "Suhu" => $this->input->post("kondisi-suhu"),
        "Udara" => $this->input->post("kondisi-udara"),
        "KeadaanRuangan" => $this->input->post("kondisi-cond"),
        "Letak" => $this->input->post("kondisi-letak"),
        "Penerangan" => $this->input->post("kondisi-penerangan"),
        "Suara" => $this->input->post("kondisi-suara"),
        "Getaran" => $this->input->post("kondisi-getaran")
      );
      $condFisik = array(
        "JenisKelamin" => $this->input->post("kondisi-jeniskelamin"),
        "Umur" => $this->input->post("kondisi-umur"),
        "TinggiBadan" => $this->input->post("kondisi-tinggi"),
        "BeratBadan" => $this->input->post("kondisi-berat"),
        "PosturBadan" => $this->input->post("kondisi-postur"),
        "Penampilan" => $this->input->post("kondisi-penampilan")
      );

      $data = array(
          COL_KD_URUSAN => $this->input->post(COL_KD_URUSAN),
          COL_KD_BIDANG => $this->input->post(COL_KD_BIDANG),
          COL_KD_UNIT => $this->input->post(COL_KD_UNIT),
          COL_KD_SUB => $this->input->post(COL_KD_SUB),
          COL_KD_BID => $this->input->post(COL_KD_BID)?$this->input->post(COL_KD_BID):null,
          COL_KD_SUBBID => $this->input->post(COL_KD_SUBBID)?$this->input->post(COL_KD_SUBBID):null,

          COL_ID_JABATAN => $this->input->post(COL_ID_JABATAN),
          COL_KD_TYPE => $this->input->post(COL_KD_TYPE),
          COL_NM_IKHTISAR => $this->input->post(COL_NM_IKHTISAR),
          COL_KD_PENDIDIKAN => $this->input->post(COL_KD_PENDIDIKAN),
          COL_KD_PENGALAMAN => $this->input->post(COL_KD_PENGALAMAN),
          COL_KELASJAB => $this->input->post(COL_KELASJAB),

          COL_NM_BAHANKERJA => $this->input->post(COL_NM_BAHANKERJA) ? urldecode($this->input->post(COL_NM_BAHANKERJA)) : null,
          COL_NM_HASILKERJA => $this->input->post(COL_NM_HASILKERJA) ? urldecode($this->input->post(COL_NM_HASILKERJA)) : null,
          COL_NM_PERANGKATKERJA => $this->input->post(COL_NM_PERANGKATKERJA) ? urldecode($this->input->post(COL_NM_PERANGKATKERJA)) : null,
          COL_NM_TANGGUNGJAWAB => $this->input->post(COL_NM_TANGGUNGJAWAB) ? urldecode($this->input->post(COL_NM_TANGGUNGJAWAB)) : null,
          COL_NM_WEWENANG => $this->input->post(COL_NM_WEWENANG) ? urldecode($this->input->post(COL_NM_WEWENANG)) : null,
          COL_NM_KORELASI => $this->input->post(COL_NM_KORELASI) ? urldecode($this->input->post(COL_NM_KORELASI)) : null,
          COL_NM_KONDISI => json_encode($cond),
          COL_NM_RESIKO => $this->input->post(COL_NM_RESIKO) ? urldecode($this->input->post(COL_NM_RESIKO)) : null,
          COL_NM_KETERAMPILAN => $this->input->post(COL_NM_KETERAMPILAN),
          COL_NM_BAKAT => json_encode($this->input->post(COL_NM_BAKAT)),
          COL_NM_TEMPRAMEN => json_encode($this->input->post(COL_NM_TEMPRAMEN)),
          COL_NM_MINAT => json_encode($this->input->post(COL_NM_MINAT)),
          COL_NM_UPAYAFISIK => json_encode($this->input->post(COL_NM_UPAYAFISIK)),
          COL_NM_KONDISIFISIK => json_encode($condFisik),
          COL_NM_FUNGSIPEKERJAAN => $this->input->post(COL_NM_FUNGSIPEKERJAAN) ? urldecode($this->input->post(COL_NM_FUNGSIPEKERJAAN)) : null,

          COL_EDIT_BY => $ruser[COL_USERNAME],
          COL_EDIT_DATE => date('Y-m-d H:i:s')
      );
      if($data[COL_KD_TYPE]=='STR') {
        $data[COL_NM_JABATAN] = $this->input->post(COL_NM_JABATAN);
        $data[COL_KD_NOMENKLATUR] = null;
      } else if($data[COL_KD_TYPE]=='FUNG') {
        $data[COL_NM_JABATAN] =  null;
        $data[COL_KD_NOMENKLATUR] = $this->input->post(COL_KD_NOMENKLATUR);
      }

      $this->db->trans_begin();
      try {
        /* CHECK OPD */
        $ropd = $this->db->where(array(
          COL_KD_URUSAN=>$rdata[COL_KD_URUSAN],
          COL_KD_BIDANG=>$rdata[COL_KD_BIDANG],
          COL_KD_UNIT=>$rdata[COL_KD_UNIT],
          COL_KD_SUB=>$rdata[COL_KD_SUB]
        ))
        ->get(TBL_AJBK_UNIT)
        ->row_array();
        if(!$ropd) {
          throw new Exception("OPD tidak valid!");
        }
        if($ropd[COL_ISAKTIF]!=1) {
          throw new Exception("Gagal mengubah data. OPD dalam keadaan SUSPEND / NONAKTIF!");
        }
        /* --CHECK OPD-- */

        $this->db->where(COL_KD_JABATAN, $id)->delete(TBL_AJBK_JABATAN_JURUSAN);
        $this->db->where(COL_KD_JABATAN, $id)->delete(TBL_AJBK_JABATAN_DIKLAT);

        $res = $this->db->where(COL_KD_JABATAN, $id)->update(TBL_AJBK_JABATAN, $data);
        $resDel = $this->db->delete(TBL_AJBK_JABATAN_URAIAN, array(COL_KD_JABATAN => $id));
        if(!$res) {
          $err = $this->db->error();
          throw new Exception("Database error : ".$err['message'].'. on query : '.$this->db->last_query());
        }
        if(!$resDel) {
          $err = $this->db->error();
          throw new Exception("Database error : ".$err['message'].'. on query : '.$this->db->last_query());
        }

        $kdJabatan = $id;
        $detUraian = $this->input->post("DetDesc");
        $detSatuan = $this->input->post("DetSatuan");
        $detJumlah = $this->input->post("DetJumlah");
        $detWaktu = $this->input->post("DetWaktu");
        $jurusan = $this->input->post(COL_KD_JURUSAN);
        $diklat = $this->input->post(COL_KD_DIKLAT);

        $arrJurusan = array();
        $arrDiklat = array();
        $det = [];
        for($i = 0; $i<count($detUraian); $i++) {
            $det[] = array(
                COL_KD_JABATAN => $kdJabatan,
                COL_NM_URAIAN => $detUraian[$i],
                COL_KD_SATUAN => $detSatuan[$i],
                COL_JLH_BEBAN => $detJumlah[$i],
                COL_JLH_JAM => $detWaktu[$i]
            );
        }
        if(!empty($jurusan)) {
          foreach($jurusan as $s) {
            $arrJurusan[] = array(COL_KD_JABATAN=>$kdJabatan, COL_KD_JURUSAN=>$s);
          }
        }

        if(!empty($diklat)) {
          foreach($diklat as $s) {
            $arrDiklat[] = array(COL_KD_JABATAN=>$kdJabatan, COL_KD_DIKLAT=>$s);
          }
        }

        if(count($det) > 0) {
            $resDet = $this->db->insert_batch(TBL_AJBK_JABATAN_URAIAN, $det);
            if(!$resDet) {
              $err = $this->db->error();
              throw new Exception("Database error: ".$err['message'].'. on query : '.$this->db->last_query());
            }
        }
        if(count($arrJurusan) > 0) {
            $resDet = $this->db->insert_batch(TBL_AJBK_JABATAN_JURUSAN, $arrJurusan);
            if(!$resDet) {
              $err = $this->db->error();
              throw new Exception("Database error: ".$err['message'].'. on query : '.$this->db->last_query());
            }
        }
        if(count($arrDiklat) > 0) {
            $resDet = $this->db->insert_batch(TBL_AJBK_JABATAN_DIKLAT, $arrDiklat);
            if(!$resDet) {
              $err = $this->db->error();
              throw new Exception("Database error: ".$err['message'].'. on query : '.$this->db->last_query());
            }
        }

        $this->db->trans_commit();
        echo json_encode($resp);
        return;
      } catch (Exception $e) {
          $this->db->trans_rollback();
          $resp['error'] = $e->getMessage();
          $resp['success'] = 0;
          echo json_encode($resp);
          return;
      }

    } else {
      $this->template->load('backend', 'ajbk/jabatan/form_', $data);
    }
  }

  public function delete($id) {
    /* CHECK OPD */
    $rdata = $this->db->where(COL_KD_JABATAN,$id)->get(TBL_AJBK_JABATAN)->row_array();
    if(!$rdata) {
      ShowJsonError("Parameter tidak valid!");
      exit();
    }
    $ropd = $this->db->where(array(
      COL_KD_URUSAN=>$rdata[COL_KD_URUSAN],
      COL_KD_BIDANG=>$rdata[COL_KD_BIDANG],
      COL_KD_UNIT=>$rdata[COL_KD_UNIT],
      COL_KD_SUB=>$rdata[COL_KD_SUB]
    ))
    ->get(TBL_AJBK_UNIT)
    ->row_array();
    if(!$ropd) {
      ShowJsonError("OPD tidak valid!");
      exit();
    }
    if($ropd[COL_ISAKTIF]!=1) {
      ShowJsonError("Gagal menghapus data. OPD dalam keadaan SUSPEND / NONAKTIF!");
      exit();
    }
    /* --CHECK OPD-- */

    $res = $this->db->delete(TBL_AJBK_JABATAN, array(COL_KD_JABATAN => $id));
    ShowJsonSuccess('Berhasil. ID: '.$id);
  }

  public function index_bezetting()
  {
      $ruser = GetLoggedUser();
      $strOPD = explode('.', $ruser[COL_COMPANYID]);
      $data['title'] = 'Bezetting';
      $this->template->load('backend', 'ajbk/jabatan/index_bezetting', $data);
  }

  public function index_bezetting_partial($_tipe='', $_opd='', $_bid=-1, $noaction = 0)
  {
    $hourperyear = HOURPERYEAR;
    $kdOPD_ = !empty($this->input->post("KdOPD")) ? $this->input->post("KdOPD") : $_opd;
    $kdBidang_ = !empty($this->input->post("KdBidang")) ? $this->input->post("KdBidang") : ($_bid > 0 ? $_bid : null);
    $kdSubBidang_ = $this->input->post("KdSubBidang");
    $Tahun = $this->input->post(COL_TAHUN);
    $tipe_ = !empty($this->input->post("tipe")) ? $this->input->post("tipe") : $_tipe;

    if(!empty($kdSubBidang_)) {
      $rsubbid = $this->db->where(COL_UNIQ, $kdSubBidang_)->get(TBL_AJBK_UNIT_SUBBID)->row_array();
      if(empty($rsubbid)) {
        echo 'Filter tidak valid.';
        return;
      }

      $kdUrusan = $rsubbid[COL_KD_URUSAN];
      $kdBidang = $rsubbid[COL_KD_BIDANG];
      $kdUnit = $rsubbid[COL_KD_UNIT];
      $kdSub = $rsubbid[COL_KD_SUB];
      $kdBid = $rsubbid[COL_KD_BID];
      $kdSubbid = $rsubbid[COL_KD_SUBBID];
    } else if(!empty($kdBidang_)) {
      $rbid = $this->db->where(COL_UNIQ, $kdBidang_)->get(TBL_AJBK_UNIT_BID)->row_array();
      if(empty($rbid)) {
        echo 'Filter tidak valid.';
        return;
      }

      $kdUrusan = $rbid[COL_KD_URUSAN];
      $kdBidang = $rbid[COL_KD_BIDANG];
      $kdUnit = $rbid[COL_KD_UNIT];
      $kdSub = $rbid[COL_KD_SUB];
      $kdBid = $rbid[COL_KD_BID];
    } else if(!empty($kdOPD_)) {
      $ropd = $this->db->where(COL_UNIQ, $kdOPD_)->get(TBL_AJBK_UNIT)->row_array();
      if(empty($ropd)) {
        echo 'Filter tidak valid.';
        return;
      }

      $kdUrusan = $ropd[COL_KD_URUSAN];
      $kdBidang = $ropd[COL_KD_BIDANG];
      $kdUnit = $ropd[COL_KD_UNIT];
      $kdSub = $ropd[COL_KD_SUB];
    }

    $this->db->select("
    ajbk_jabatan.*,
    COALESCE(ajbk_jabatan.Nm_Jabatan, ajbk_nomenklatur.Nm_Nomenklatur) as Nm_Jabatan,
    ajbk_unit.Nm_Sub_Unit,
    ajbk_unit_bid.Nm_Bid,
    ajbk_unit_subbid.Nm_Subbid,
    (select count(*) from ajbk_jabatan_uraian u where u.Kd_Jabatan = ajbk_jabatan.Kd_Jabatan) as Uraian,
    (select sum(Jlh_Beban*Jlh_Jam) from ajbk_jabatan_uraian u where u.Kd_Jabatan = ajbk_jabatan.Kd_Jabatan) as Beban,
    (select sum(Jlh_Beban*Jlh_Jam) from ajbk_jabatan_uraian u where u.Kd_Jabatan = ajbk_jabatan.Kd_Jabatan)/$hourperyear as Pegawai,
    ajbk_jabatan_bezetting.Jlh_Pegawai as Bezetting,
    ajbk_jabatan_bezetting.Tahun
    ");
    if(isset($kdUrusan)) $this->db->where(TBL_AJBK_JABATAN.".".COL_KD_URUSAN, $kdUrusan);
    if(isset($kdBidang)) $this->db->where(TBL_AJBK_JABATAN.".".COL_KD_BIDANG, $kdBidang);
    if(isset($kdUnit)) $this->db->where(TBL_AJBK_JABATAN.".".COL_KD_UNIT, $kdUnit);
    if(isset($kdSub)) $this->db->where(TBL_AJBK_JABATAN.".".COL_KD_SUB, $kdSub);
    if(isset($kdBid)) $this->db->where(TBL_AJBK_JABATAN.".".COL_KD_BID, $kdBid);
    if(isset($kdSubbid)) $this->db->where(TBL_AJBK_JABATAN.".".COL_KD_SUBBID, $kdSubbid);
    if(!empty($tipe_)) $this->db->where(TBL_AJBK_JABATAN.".".COL_KD_TYPE, $tipe_);
    if($noaction && $_opd == 20 && $_bid <= 0) {
      $this->db->where_not_in(TBL_AJBK_JABATAN.".".COL_KD_BID, array(10,11,12,13,14));
    }
    if($noaction && $_opd == 2 && $_bid <= 0) {
      $this->db
      ->group_start()
      ->where_not_in(TBL_AJBK_JABATAN.".".COL_KD_BID, array(5,6,7,8,9,10,11,12,13,14,15))
      ->or_where(TBL_AJBK_JABATAN.".".COL_KD_BID,null)
      ->group_end();
    }
    if($noaction && $_opd == 1 && $_bid <= 0) {
      $this->db
      ->group_start()
      ->where_in(TBL_AJBK_JABATAN.".".COL_KD_BID, array(1,2,3,4))
      ->or_where(TBL_AJBK_JABATAN.".".COL_KD_BID,null)
      ->group_end();
    }

    $this->db->join(TBL_AJBK_NOMENKLATUR,TBL_AJBK_NOMENKLATUR.'.'.COL_KD_NOMENKLATUR." = ".TBL_AJBK_JABATAN.".".COL_KD_NOMENKLATUR,"left");
    $this->db->join(TBL_AJBK_UNIT,
    TBL_AJBK_UNIT.'.'.COL_KD_URUSAN." = ".TBL_AJBK_JABATAN.".".COL_KD_URUSAN." AND ".
    TBL_AJBK_UNIT.'.'.COL_KD_BIDANG." = ".TBL_AJBK_JABATAN.".".COL_KD_BIDANG." AND ".
    TBL_AJBK_UNIT.'.'.COL_KD_UNIT." = ".TBL_AJBK_JABATAN.".".COL_KD_UNIT." AND ".
    TBL_AJBK_UNIT.'.'.COL_KD_SUB." = ".TBL_AJBK_JABATAN.".".COL_KD_SUB
    ,"left");
    $this->db->join(TBL_AJBK_UNIT_BID,
    TBL_AJBK_UNIT_BID.'.'.COL_KD_URUSAN." = ".TBL_AJBK_JABATAN.".".COL_KD_URUSAN." AND ".
    TBL_AJBK_UNIT_BID.'.'.COL_KD_BIDANG." = ".TBL_AJBK_JABATAN.".".COL_KD_BIDANG." AND ".
    TBL_AJBK_UNIT_BID.'.'.COL_KD_UNIT." = ".TBL_AJBK_JABATAN.".".COL_KD_UNIT." AND ".
    TBL_AJBK_UNIT_BID.'.'.COL_KD_SUB." = ".TBL_AJBK_JABATAN.".".COL_KD_SUB." AND ".
    TBL_AJBK_UNIT_BID.'.'.COL_KD_BID." = ".TBL_AJBK_JABATAN.".".COL_KD_BID
    ,"left");
    $this->db->join(TBL_AJBK_UNIT_SUBBID,
    TBL_AJBK_UNIT_SUBBID.'.'.COL_KD_URUSAN." = ".TBL_AJBK_JABATAN.".".COL_KD_URUSAN." AND ".
    TBL_AJBK_UNIT_SUBBID.'.'.COL_KD_BIDANG." = ".TBL_AJBK_JABATAN.".".COL_KD_BIDANG." AND ".
    TBL_AJBK_UNIT_SUBBID.'.'.COL_KD_UNIT." = ".TBL_AJBK_JABATAN.".".COL_KD_UNIT." AND ".
    TBL_AJBK_UNIT_SUBBID.'.'.COL_KD_SUB." = ".TBL_AJBK_JABATAN.".".COL_KD_SUB." AND ".
    TBL_AJBK_UNIT_SUBBID.'.'.COL_KD_BID." = ".TBL_AJBK_JABATAN.".".COL_KD_BID." AND ".
    TBL_AJBK_UNIT_SUBBID.'.'.COL_KD_SUBBID." = ".TBL_AJBK_JABATAN.".".COL_KD_SUBBID
    ,"left");
    $this->db->join(TBL_AJBK_JABATAN_BEZETTING,
    TBL_AJBK_JABATAN_BEZETTING.'.'.COL_KD_JABATAN." = ".TBL_AJBK_JABATAN.".".COL_KD_JABATAN." AND ".
    TBL_AJBK_JABATAN_BEZETTING.'.'.COL_TAHUN." = ".(!empty($Tahun)?$Tahun:date('Y'))
    ,"left");

    $data['noaction'] = $noaction;
    $data['res'] = $this->db
    ->order_by(TBL_AJBK_JABATAN.'.'.COL_KD_TYPE,'desc')
    ->order_by('ajbk_unit.Nm_Sub_Unit', 'asc')
    ->order_by('ajbk_unit_bid.Nm_Bid', 'asc')
    ->order_by('ajbk_unit_subbid.Nm_Subbid', 'asc')

    ->order_by('ajbk_jabatan.Nm_Jabatan', 'asc')
    ->order_by('ajbk_nomenklatur.Nm_Nomenklatur', 'asc')
    ->get(TBL_AJBK_JABATAN)
    ->result_array();
    $this->load->view('ajbk/jabatan/index_bezetting_partial', $data);
  }

  public function set_bezetting($id) {
    $ruser = GetLoggedUser();
    $jlhPegawai = $this->input->post(COL_JLH_PEGAWAI);
    $Tahun = $this->input->post(COL_TAHUN);

    $res = true;
    /* CHECK OPD */
    $rdata = $this->db->where(COL_KD_JABATAN,$id)->get(TBL_AJBK_JABATAN)->row_array();
    if(!$rdata) {
      ShowJsonError("Parameter tidak valid!");
      exit();
    }
    $ropd = $this->db->where(array(
      COL_KD_URUSAN=>$rdata[COL_KD_URUSAN],
      COL_KD_BIDANG=>$rdata[COL_KD_BIDANG],
      COL_KD_UNIT=>$rdata[COL_KD_UNIT],
      COL_KD_SUB=>$rdata[COL_KD_SUB]
    ))
    ->get(TBL_AJBK_UNIT)
    ->row_array();
    if(!$ropd) {
      ShowJsonError("OPD tidak valid!");
      exit();
    }
    if($ropd[COL_ISAKTIF]!=1) {
      ShowJsonError("Gagal menghapus data. OPD dalam keadaan SUSPEND / NONAKTIF!");
      exit();
    }
    /* --CHECK OPD-- */

    $rdata = $this->db->where(array(COL_KD_JABATAN=>$id,COL_TAHUN=>$Tahun))->get(TBL_AJBK_JABATAN_BEZETTING)->row_array();
    if(!empty($rdata)) {
      $res = $this->db
      ->where(array(COL_KD_JABATAN=>$id,COL_TAHUN=>$Tahun))
      ->update(TBL_AJBK_JABATAN_BEZETTING, array(
        COL_JLH_PEGAWAI=>toNum($jlhPegawai),
        COL_EDIT_BY=>$ruser[COL_USERNAME],
        COL_EDIT_DATE=>date('Y-m-d H:i:s')
      ));
    } else {
      $res = $this->db
      ->insert(TBL_AJBK_JABATAN_BEZETTING, array(
        COL_KD_JABATAN=>$id,
        COL_TAHUN=>$Tahun,
        COL_JLH_PEGAWAI=>$jlhPegawai,
        COL_CREATE_BY=>$ruser[COL_USERNAME],
        COL_CREATE_DATE =>date('Y-m-d H:i:s')
      ));
    }

    if(!$res) {
      ShowJsonError("Gagal mengupdate data.");
    } else {
      ShowJsonSuccess("Berhasil");
    }
  }

  public function cetak($id) {
    $rdata = $dat['data'] = $this->db
    ->select('*, COALESCE(ajbk_jabatan.Nm_Jabatan, ajbk_nomenklatur.Nm_Nomenklatur) as NM_JAB, edu.Opt_Name as NM_EDU, _userinformation.Name as NmVerified')
    ->join(TBL__USERINFORMATION,TBL__USERINFORMATION.'.'.COL_USERNAME." = ".TBL_AJBK_JABATAN.".".COL_VERIFIED_BY,"left")
    ->join(TBL_AJBK_NOMENKLATUR,TBL_AJBK_NOMENKLATUR.'.'.COL_KD_NOMENKLATUR." = ".TBL_AJBK_JABATAN.".".COL_KD_NOMENKLATUR,"left")
    ->join(TBL_AJBK_UNIT,
    TBL_AJBK_UNIT.'.'.COL_KD_URUSAN." = ".TBL_AJBK_JABATAN.".".COL_KD_URUSAN." AND ".
    TBL_AJBK_UNIT.'.'.COL_KD_BIDANG." = ".TBL_AJBK_JABATAN.".".COL_KD_BIDANG." AND ".
    TBL_AJBK_UNIT.'.'.COL_KD_UNIT." = ".TBL_AJBK_JABATAN.".".COL_KD_UNIT." AND ".
    TBL_AJBK_UNIT.'.'.COL_KD_SUB." = ".TBL_AJBK_JABATAN.".".COL_KD_SUB
    ,"left")
    ->join(TBL_AJBK_UNIT_BID,
    TBL_AJBK_UNIT_BID.'.'.COL_KD_URUSAN." = ".TBL_AJBK_JABATAN.".".COL_KD_URUSAN." AND ".
    TBL_AJBK_UNIT_BID.'.'.COL_KD_BIDANG." = ".TBL_AJBK_JABATAN.".".COL_KD_BIDANG." AND ".
    TBL_AJBK_UNIT_BID.'.'.COL_KD_UNIT." = ".TBL_AJBK_JABATAN.".".COL_KD_UNIT." AND ".
    TBL_AJBK_UNIT_BID.'.'.COL_KD_SUB." = ".TBL_AJBK_JABATAN.".".COL_KD_SUB." AND ".
    TBL_AJBK_UNIT_BID.'.'.COL_KD_BID." = ".TBL_AJBK_JABATAN.".".COL_KD_BID
    ,"left")
    ->join(TBL_AJBK_UNIT_SUBBID,
    TBL_AJBK_UNIT_SUBBID.'.'.COL_KD_URUSAN." = ".TBL_AJBK_JABATAN.".".COL_KD_URUSAN." AND ".
    TBL_AJBK_UNIT_SUBBID.'.'.COL_KD_BIDANG." = ".TBL_AJBK_JABATAN.".".COL_KD_BIDANG." AND ".
    TBL_AJBK_UNIT_SUBBID.'.'.COL_KD_UNIT." = ".TBL_AJBK_JABATAN.".".COL_KD_UNIT." AND ".
    TBL_AJBK_UNIT_SUBBID.'.'.COL_KD_SUB." = ".TBL_AJBK_JABATAN.".".COL_KD_SUB." AND ".
    TBL_AJBK_UNIT_SUBBID.'.'.COL_KD_BID." = ".TBL_AJBK_JABATAN.".".COL_KD_BID." AND ".
    TBL_AJBK_UNIT_SUBBID.'.'.COL_KD_SUBBID." = ".TBL_AJBK_JABATAN.".".COL_KD_SUBBID
    ,"left")
    ->join(TBL_AJBK_OPTION.' edu','edu.'.COL_UNIQ." = ".TBL_AJBK_JABATAN.".".COL_KD_PENDIDIKAN,"left")
    ->where(array(COL_KD_JABATAN=>$id))
    ->get(TBL_AJBK_JABATAN)
    ->row_array();

    if(empty($rdata)) {
      show_error('Data tidak ditemukan');
      return;
    }

    $this->load->library('Mypdf');
    $mpdf = new Mypdf();

    $html = $this->load->view('jabatan/cetak', $dat, TRUE);
    //echo $html;
    //return;
    $mpdf->pdf->SetTitle('Informasi Jabatan - '.$rdata["NM_JAB"]);
    $mpdf->pdf->SetFooter('Dicetak melalui aplikasi '.$this->setting_web_name);
    $mpdf->pdf->SetWatermarkImage(MY_IMAGEURL.$this->setting_web_logo, 0.1, array(100,100));
    $mpdf->pdf->showWatermarkImage = true;
    $mpdf->pdf->WriteHTML($html);
    $mpdf->pdf->Output('Informasi Jabatan - '.$rdata["NM_JAB"].'.pdf', 'I');
  }

  public function rekapitulasi_opd() {
    $Tahun = date('Y');
    /*$q = @"
    select
    tbl.Nm_Sub_Unit,
    tbl.Kd_Urusan,
    tbl.Kd_Bidang,
    tbl.Kd_Unit,
    tbl.Kd_Sub,
    tbl.IdOPD,
    sum(round(tbl.ABK_STR)) as ABK_STR,
    sum(round(tbl.ABK_PEL)) as ABK_PEL,
    sum(tbl.BEZ_STR) as BEZ_STR,
    sum(tbl.BEZ_PEL) as BEZ_PEL
    from (
      select
      opd.Uniq as IdOPD,
      opd.Nm_Sub_Unit,
      opd.Kd_Urusan,
      opd.Kd_Bidang,
      opd.Kd_Unit,
      opd.Kd_Sub,
      (select sum((ur.Jlh_Beban*ur.Jlh_Jam)/1250)
      	from ajbk_jabatan_uraian ur
      	left join ajbk_jabatan jab_ on jab_.Kd_Jabatan = ur.Kd_Jabatan
      	where
      		ur.Kd_Jabatan = jab.Kd_Jabatan
      		and jab_.Kd_Urusan = opd.Kd_Urusan
      		and jab_.Kd_Bidang = opd.Kd_Bidang
      		and jab_.Kd_Unit = opd.Kd_Unit
      		and jab_.Kd_Sub = opd.Kd_Sub
      		and jab_.Kd_Type = 'STR'
          and CONCAT(jab_.Kd_Urusan,jab_.Kd_Bidang,jab_.Kd_Unit,jab_.Kd_Sub,jab_.Kd_Bid) not in (411110,411111,411112,411113,411114)
      ) as ABK_STR,
      (select sum((ur.Jlh_Beban*ur.Jlh_Jam)/1250)
      	from ajbk_jabatan_uraian ur
      	left join ajbk_jabatan jab_ on jab_.Kd_Jabatan = ur.Kd_Jabatan
      	where
      		ur.Kd_Jabatan = jab.Kd_Jabatan
      		and jab_.Kd_Urusan = opd.Kd_Urusan
      		and jab_.Kd_Bidang = opd.Kd_Bidang
      		and jab_.Kd_Unit = opd.Kd_Unit
      		and jab_.Kd_Sub = opd.Kd_Sub
      		and jab_.Kd_Type = 'FUNG'
          and CONCAT(jab_.Kd_Urusan,jab_.Kd_Bidang,jab_.Kd_Unit,jab_.Kd_Sub,jab_.Kd_Bid) not in (411110,411111,411112,411113,411114)
      ) as ABK_PEL,
      (select sum(bez.Jlh_Pegawai)
      	from ajbk_jabatan_bezetting bez
      	left join ajbk_jabatan jab_ on jab_.Kd_Jabatan = bez.Kd_Jabatan
      	where
      		bez.Kd_Jabatan = jab.Kd_Jabatan
      		and bez.Tahun = $Tahun
      		and jab_.Kd_Urusan = opd.Kd_Urusan
      		and jab_.Kd_Bidang = opd.Kd_Bidang
      		and jab_.Kd_Unit = opd.Kd_Unit
      		and jab_.Kd_Sub = opd.Kd_Sub
      		and jab_.Kd_Type = 'STR'
          and CONCAT(jab_.Kd_Urusan,jab_.Kd_Bidang,jab_.Kd_Unit,jab_.Kd_Sub,jab_.Kd_Bid) not in (411110,411111,411112,411113,411114)
      ) as BEZ_STR,
      (select sum(bez.Jlh_Pegawai)
      	from ajbk_jabatan_bezetting bez
      	left join ajbk_jabatan jab_ on jab_.Kd_Jabatan = bez.Kd_Jabatan
      	where
      		bez.Kd_Jabatan = jab.Kd_Jabatan
      		and bez.Tahun = $Tahun
      		and jab_.Kd_Urusan = opd.Kd_Urusan
      		and jab_.Kd_Bidang = opd.Kd_Bidang
      		and jab_.Kd_Unit = opd.Kd_Unit
      		and jab_.Kd_Sub = opd.Kd_Sub
      		and jab_.Kd_Type = 'FUNG'
          and CONCAT(jab_.Kd_Urusan,jab_.Kd_Bidang,jab_.Kd_Unit,jab_.Kd_Sub,jab_.Kd_Bid) not in (411110,411111,411112,411113,411114)
      ) as BEZ_PEL
      from ajbk_unit opd
      left join ajbk_jabatan jab on jab.Kd_Urusan = opd.Kd_Urusan and jab.Kd_Bidang = opd.Kd_Bidang and jab.Kd_Unit = opd.Kd_Unit and jab.Kd_Sub = opd.Kd_Sub
    ) tbl
    group by tbl.Kd_Urusan, tbl.Kd_Bidang, tbl.Kd_Unit, tbl.Kd_Sub
    ";
    $data['res'] = $this->db->query($q)->result_array();

    $q = @"
    select
    tbl.Nm_Bid,
    tbl.Kd_Urusan,
    tbl.Kd_Bidang,
    tbl.Kd_Unit,
    tbl.Kd_Sub,
    tbl.Kd_Bid,
    tbl.IdBid,
    tbl.IdOPD,
    sum(round(tbl.ABK_STR)) as ABK_STR,
    sum(round(tbl.ABK_PEL)) as ABK_PEL,
    sum(tbl.BEZ_STR) as BEZ_STR,
    sum(tbl.BEZ_PEL) as BEZ_PEL
    from (
      select
      opd.Uniq as IdOPD,
      bid.Uniq as IdBid,
      bid.Nm_Bid,
      bid.Kd_Urusan,
      bid.Kd_Bidang,
      bid.Kd_Unit,
      bid.Kd_Sub,
      bid.Kd_Bid,
      (select sum((ur.Jlh_Beban*ur.Jlh_Jam)/1250)
      	from ajbk_jabatan_uraian ur
      	left join ajbk_jabatan jab_ on jab_.Kd_Jabatan = ur.Kd_Jabatan
      	where
      		ur.Kd_Jabatan = jab.Kd_Jabatan
      		and jab_.Kd_Urusan = bid.Kd_Urusan
      		and jab_.Kd_Bidang = bid.Kd_Bidang
      		and jab_.Kd_Unit = bid.Kd_Unit
      		and jab_.Kd_Sub = bid.Kd_Sub
          and jab_.Kd_Bid = bid.Kd_Bid
      		and jab_.Kd_Type = 'STR'
      ) as ABK_STR,
      (select sum((ur.Jlh_Beban*ur.Jlh_Jam)/1250)
      	from ajbk_jabatan_uraian ur
      	left join ajbk_jabatan jab_ on jab_.Kd_Jabatan = ur.Kd_Jabatan
      	where
      		ur.Kd_Jabatan = jab.Kd_Jabatan
      		and jab_.Kd_Urusan = bid.Kd_Urusan
      		and jab_.Kd_Bidang = bid.Kd_Bidang
      		and jab_.Kd_Unit = bid.Kd_Unit
      		and jab_.Kd_Sub = bid.Kd_Sub
          and jab_.Kd_Bid = bid.Kd_Bid
      		and jab_.Kd_Type = 'FUNG'
      ) as ABK_PEL,
      (select sum(bez.Jlh_Pegawai)
      	from ajbk_jabatan_bezetting bez
      	left join ajbk_jabatan jab_ on jab_.Kd_Jabatan = bez.Kd_Jabatan
      	where
      		bez.Kd_Jabatan = jab.Kd_Jabatan
      		and bez.Tahun = $Tahun
      		and jab_.Kd_Urusan = bid.Kd_Urusan
      		and jab_.Kd_Bidang = bid.Kd_Bidang
      		and jab_.Kd_Unit = bid.Kd_Unit
      		and jab_.Kd_Sub = bid.Kd_Sub
          and jab_.Kd_Bid = bid.Kd_Bid
      		and jab_.Kd_Type = 'STR'
      ) as BEZ_STR,
      (select sum(bez.Jlh_Pegawai)
      	from ajbk_jabatan_bezetting bez
      	left join ajbk_jabatan jab_ on jab_.Kd_Jabatan = bez.Kd_Jabatan
      	where
      		bez.Kd_Jabatan = jab.Kd_Jabatan
      		and bez.Tahun = $Tahun
      		and jab_.Kd_Urusan = bid.Kd_Urusan
      		and jab_.Kd_Bidang = bid.Kd_Bidang
      		and jab_.Kd_Unit = bid.Kd_Unit
      		and jab_.Kd_Sub = bid.Kd_Sub
          and jab_.Kd_Bid = bid.Kd_Bid
      		and jab_.Kd_Type = 'FUNG'
      ) as BEZ_PEL
      from ajbk_unit_bid bid
      left join ajbk_jabatan jab on jab.Kd_Urusan = bid.Kd_Urusan and jab.Kd_Bidang = bid.Kd_Bidang and jab.Kd_Unit = bid.Kd_Unit and jab.Kd_Sub = bid.Kd_Sub and jab.Kd_Bid = bid.Kd_Bid
      left join ajbk_unit opd on opd.Kd_Urusan = bid.Kd_Urusan and opd.Kd_Bidang = bid.Kd_Bidang and opd.Kd_Unit = bid.Kd_Unit and opd.Kd_Sub = bid.Kd_Sub
      where CONCAT(bid.Kd_Urusan,bid.Kd_Bidang,bid.Kd_Unit,bid.Kd_Sub,bid.Kd_Bid) in (411110,411111,411112,411113,411114)
    ) tbl
    group by tbl.Kd_Urusan, tbl.Kd_Bidang, tbl.Kd_Unit, tbl.Kd_Sub, tbl.Kd_Bid
    ";
    $data['res2'] = $this->db->query($q)->result_array();*/
    $data['opd'] = $this->db
    ->where(COL_ISAKTIF, 1)
    ->get(TBL_AJBK_UNIT)
    ->result_array();
    $data['bag'] = $this->db
    ->select('ajbk_unit_bid.*, ajbk_unit.Uniq as IdOPD')
    ->join(TBL_AJBK_UNIT,
    TBL_AJBK_UNIT.'.'.COL_KD_URUSAN." = ".TBL_AJBK_UNIT_BID.".".COL_KD_URUSAN." AND ".
    TBL_AJBK_UNIT.'.'.COL_KD_BIDANG." = ".TBL_AJBK_UNIT_BID.".".COL_KD_BIDANG." AND ".
    TBL_AJBK_UNIT.'.'.COL_KD_UNIT." = ".TBL_AJBK_UNIT_BID.".".COL_KD_UNIT." AND ".
    TBL_AJBK_UNIT.'.'.COL_KD_SUB." = ".TBL_AJBK_UNIT_BID.".".COL_KD_SUB
    ,"left")
    ->where(array(
      TBL_AJBK_UNIT_BID.'.'.COL_KD_URUSAN=>4,
      TBL_AJBK_UNIT_BID.'.'.COL_KD_BIDANG=>1,
      TBL_AJBK_UNIT_BID.'.'.COL_KD_UNIT=>1,
      TBL_AJBK_UNIT_BID.'.'.COL_KD_SUB=>1
    ))
    ->where_in(COL_KD_BID, array(1,2,3,4,5,6,7,8,9))
    ->get(TBL_AJBK_UNIT_BID)
    ->result_array();

    $data['kec'] = $this->db
    ->select('ajbk_unit_bid.*, ajbk_unit.Uniq as IdOPD')
    ->join(TBL_AJBK_UNIT,
    TBL_AJBK_UNIT.'.'.COL_KD_URUSAN." = ".TBL_AJBK_UNIT_BID.".".COL_KD_URUSAN." AND ".
    TBL_AJBK_UNIT.'.'.COL_KD_BIDANG." = ".TBL_AJBK_UNIT_BID.".".COL_KD_BIDANG." AND ".
    TBL_AJBK_UNIT.'.'.COL_KD_UNIT." = ".TBL_AJBK_UNIT_BID.".".COL_KD_UNIT." AND ".
    TBL_AJBK_UNIT.'.'.COL_KD_SUB." = ".TBL_AJBK_UNIT_BID.".".COL_KD_SUB
    ,"left")
    ->where(array(
      TBL_AJBK_UNIT_BID.'.'.COL_KD_URUSAN=>4,
      TBL_AJBK_UNIT_BID.'.'.COL_KD_BIDANG=>1,
      TBL_AJBK_UNIT_BID.'.'.COL_KD_UNIT=>1,
      TBL_AJBK_UNIT_BID.'.'.COL_KD_SUB=>1
    ))
    ->where_in(COL_KD_BID, array(10,11,12,13,14))
    ->get(TBL_AJBK_UNIT_BID)
    ->result_array();

    $data['uptkes'] = $this->db
    ->select('ajbk_unit_bid.*, ajbk_unit.Uniq as IdOPD')
    ->join(TBL_AJBK_UNIT,
    TBL_AJBK_UNIT.'.'.COL_KD_URUSAN." = ".TBL_AJBK_UNIT_BID.".".COL_KD_URUSAN." AND ".
    TBL_AJBK_UNIT.'.'.COL_KD_BIDANG." = ".TBL_AJBK_UNIT_BID.".".COL_KD_BIDANG." AND ".
    TBL_AJBK_UNIT.'.'.COL_KD_UNIT." = ".TBL_AJBK_UNIT_BID.".".COL_KD_UNIT." AND ".
    TBL_AJBK_UNIT.'.'.COL_KD_SUB." = ".TBL_AJBK_UNIT_BID.".".COL_KD_SUB
    ,"left")
    ->where(array(
      TBL_AJBK_UNIT_BID.'.'.COL_KD_URUSAN=>1,
      TBL_AJBK_UNIT_BID.'.'.COL_KD_BIDANG=>2,
      TBL_AJBK_UNIT_BID.'.'.COL_KD_UNIT=>1,
      TBL_AJBK_UNIT_BID.'.'.COL_KD_SUB=>1
    ))
    ->where_in(COL_KD_BID, array(5,6,7,8,9,10,11,12,13,14,15))
    ->get(TBL_AJBK_UNIT_BID)
    ->result_array();

    $data['sekolah'] = array();
    $data['title'] = 'Rekapitulasi OPD';
    $this->template->load('backend', 'ajbk/jabatan/rekapitulasi_opd', $data);
  }

  public function rekapitulasi_disdik() {
    $Tahun = date('Y');
    $data['opd'] = $this->db
    ->where(COL_UNIQ, 1)
    ->where(COL_UNIQ, 1)
    ->get(TBL_AJBK_UNIT)
    ->result_array();
    $data['bag'] = array();
    $data['kec'] = array();
    $data['uptkes'] = array();

    $data['sekolah'] = $this->db
    ->select('ajbk_unit_bid.*, ajbk_unit.Uniq as IdOPD')
    ->join(TBL_AJBK_UNIT,
    TBL_AJBK_UNIT.'.'.COL_KD_URUSAN." = ".TBL_AJBK_UNIT_BID.".".COL_KD_URUSAN." AND ".
    TBL_AJBK_UNIT.'.'.COL_KD_BIDANG." = ".TBL_AJBK_UNIT_BID.".".COL_KD_BIDANG." AND ".
    TBL_AJBK_UNIT.'.'.COL_KD_UNIT." = ".TBL_AJBK_UNIT_BID.".".COL_KD_UNIT." AND ".
    TBL_AJBK_UNIT.'.'.COL_KD_SUB." = ".TBL_AJBK_UNIT_BID.".".COL_KD_SUB
    ,"left")
    ->where(array(
      TBL_AJBK_UNIT_BID.'.'.COL_KD_URUSAN=>1,
      TBL_AJBK_UNIT_BID.'.'.COL_KD_BIDANG=>1,
      TBL_AJBK_UNIT_BID.'.'.COL_KD_UNIT=>1,
      TBL_AJBK_UNIT_BID.'.'.COL_KD_SUB=>1
    ))
    ->where_not_in(COL_KD_BID, array(1,2,3,4))
    ->order_by(TBL_AJBK_UNIT_BID.'.'.COL_NM_BID)
    ->get(TBL_AJBK_UNIT_BID)
    ->result_array();
    $data['title'] = 'Rekapitulasi Dinas Pendidikan';
    $this->template->load('backend', 'ajbk/jabatan/rekapitulasi_opd', $data);
  }

  public function list_jabatan_opd($tipe, $kdUrusan, $kdBidang, $kdUnit, $kdSub) {
    echo 'Hello';
  }

  public function rekapitulasi_beban()
  {
    $ruser = GetLoggedUser();
    $strOPD = explode('.', $ruser[COL_COMPANYID]);
    $data['title'] = 'Rekapitulasi Beban Kerja';
    $data['load'] = site_url('ajbk/jabatan/rekapitulasi-beban-partial');
    $this->template->load('backend', 'ajbk/jabatan/rekapitulasi_', $data);
  }

  public function rekapitulasi_beban_partial($_opd='', $isCetak=0)
  {
    $hourperyear = HOURPERYEAR;
    $kdOPD_ = !empty($this->input->post("KdOPD")) ? $this->input->post("KdOPD") : $_opd;
    $kdBidang_ = !empty($this->input->post("KdBidang")) ? $this->input->post("KdBidang") : null;
    $kdSubBidang_ = $this->input->post("KdSubBidang");
    $Tahun = $this->input->post(COL_TAHUN);
    //$tipe_ = !empty($this->input->post("tipe")) ? $this->input->post("tipe") : $_tipe;

    if(!empty($kdSubBidang_)) {
      $rsubbid = $this->db->where(COL_UNIQ, $kdSubBidang_)->get(TBL_AJBK_UNIT_SUBBID)->row_array();
      if(empty($rsubbid)) {
        echo 'Filter tidak valid.';
        return;
      }

      $kdUrusan = $rsubbid[COL_KD_URUSAN];
      $kdBidang = $rsubbid[COL_KD_BIDANG];
      $kdUnit = $rsubbid[COL_KD_UNIT];
      $kdSub = $rsubbid[COL_KD_SUB];
      $kdBid = $rsubbid[COL_KD_BID];
      $kdSubbid = $rsubbid[COL_KD_SUBBID];
    } else if(!empty($kdBidang_)) {
      $rbid = $this->db->where(COL_UNIQ, $kdBidang_)->get(TBL_AJBK_UNIT_BID)->row_array();
      if(empty($rbid)) {
        echo 'Filter tidak valid.';
        return;
      }

      $kdUrusan = $rbid[COL_KD_URUSAN];
      $kdBidang = $rbid[COL_KD_BIDANG];
      $kdUnit = $rbid[COL_KD_UNIT];
      $kdSub = $rbid[COL_KD_SUB];
      $kdBid = $rbid[COL_KD_BID];
    } else if(!empty($kdOPD_)) {
      $ropd = $this->db->where(COL_UNIQ, $kdOPD_)->get(TBL_AJBK_UNIT)->row_array();
      if(empty($ropd)) {
        echo 'Filter tidak valid.';
        return;
      }

      $kdUrusan = $ropd[COL_KD_URUSAN];
      $kdBidang = $ropd[COL_KD_BIDANG];
      $kdUnit = $ropd[COL_KD_UNIT];
      $kdSub = $ropd[COL_KD_SUB];
    }

    $this->db->select("
    ajbk_jabatan.*,
    COALESCE(ajbk_jabatan.Nm_Jabatan, ajbk_nomenklatur.Nm_Nomenklatur) as Nm_Jabatan,
    ajbk_unit.Nm_Sub_Unit,
    ajbk_unit_bid.Nm_Bid,
    ajbk_unit_subbid.Nm_Subbid,
    (select count(*) from ajbk_jabatan_uraian u where u.Kd_Jabatan = ajbk_jabatan.Kd_Jabatan) as Uraian,
    (select sum(Jlh_Beban*Jlh_Jam) from ajbk_jabatan_uraian u where u.Kd_Jabatan = ajbk_jabatan.Kd_Jabatan) as Beban,
    round((select sum(Jlh_Beban*Jlh_Jam) from ajbk_jabatan_uraian u where u.Kd_Jabatan = ajbk_jabatan.Kd_Jabatan)/$hourperyear) as Pegawai,
    ajbk_jabatan_bezetting.Jlh_Pegawai as Bezetting,
    ajbk_jabatan_bezetting.Tahun,
    CONVERT(ajbk_jabatan.KelasJab, DECIMAL) as Kelas
    ");
    if(isset($kdUrusan)) $this->db->where(TBL_AJBK_JABATAN.".".COL_KD_URUSAN, $kdUrusan);
    if(isset($kdBidang)) $this->db->where(TBL_AJBK_JABATAN.".".COL_KD_BIDANG, $kdBidang);
    if(isset($kdUnit)) $this->db->where(TBL_AJBK_JABATAN.".".COL_KD_UNIT, $kdUnit);
    if(isset($kdSub)) $this->db->where(TBL_AJBK_JABATAN.".".COL_KD_SUB, $kdSub);
    if(isset($kdBid)) $this->db->where(TBL_AJBK_JABATAN.".".COL_KD_BID, $kdBid);
    if(isset($kdSubbid)) $this->db->where(TBL_AJBK_JABATAN.".".COL_KD_SUBBID, $kdSubbid);
    //if(!empty($tipe_)) $this->db->where(TBL_AJBK_JABATAN.".".COL_KD_TYPE, $tipe_);

    $this->db->join(TBL_AJBK_NOMENKLATUR,TBL_AJBK_NOMENKLATUR.'.'.COL_KD_NOMENKLATUR." = ".TBL_AJBK_JABATAN.".".COL_KD_NOMENKLATUR,"left");
    $this->db->join(TBL_AJBK_UNIT,
    TBL_AJBK_UNIT.'.'.COL_KD_URUSAN." = ".TBL_AJBK_JABATAN.".".COL_KD_URUSAN." AND ".
    TBL_AJBK_UNIT.'.'.COL_KD_BIDANG." = ".TBL_AJBK_JABATAN.".".COL_KD_BIDANG." AND ".
    TBL_AJBK_UNIT.'.'.COL_KD_UNIT." = ".TBL_AJBK_JABATAN.".".COL_KD_UNIT." AND ".
    TBL_AJBK_UNIT.'.'.COL_KD_SUB." = ".TBL_AJBK_JABATAN.".".COL_KD_SUB
    ,"left");
    $this->db->join(TBL_AJBK_UNIT_BID,
    TBL_AJBK_UNIT_BID.'.'.COL_KD_URUSAN." = ".TBL_AJBK_JABATAN.".".COL_KD_URUSAN." AND ".
    TBL_AJBK_UNIT_BID.'.'.COL_KD_BIDANG." = ".TBL_AJBK_JABATAN.".".COL_KD_BIDANG." AND ".
    TBL_AJBK_UNIT_BID.'.'.COL_KD_UNIT." = ".TBL_AJBK_JABATAN.".".COL_KD_UNIT." AND ".
    TBL_AJBK_UNIT_BID.'.'.COL_KD_SUB." = ".TBL_AJBK_JABATAN.".".COL_KD_SUB." AND ".
    TBL_AJBK_UNIT_BID.'.'.COL_KD_BID." = ".TBL_AJBK_JABATAN.".".COL_KD_BID
    ,"left");
    $this->db->join(TBL_AJBK_UNIT_SUBBID,
    TBL_AJBK_UNIT_SUBBID.'.'.COL_KD_URUSAN." = ".TBL_AJBK_JABATAN.".".COL_KD_URUSAN." AND ".
    TBL_AJBK_UNIT_SUBBID.'.'.COL_KD_BIDANG." = ".TBL_AJBK_JABATAN.".".COL_KD_BIDANG." AND ".
    TBL_AJBK_UNIT_SUBBID.'.'.COL_KD_UNIT." = ".TBL_AJBK_JABATAN.".".COL_KD_UNIT." AND ".
    TBL_AJBK_UNIT_SUBBID.'.'.COL_KD_SUB." = ".TBL_AJBK_JABATAN.".".COL_KD_SUB." AND ".
    TBL_AJBK_UNIT_SUBBID.'.'.COL_KD_BID." = ".TBL_AJBK_JABATAN.".".COL_KD_BID." AND ".
    TBL_AJBK_UNIT_SUBBID.'.'.COL_KD_SUBBID." = ".TBL_AJBK_JABATAN.".".COL_KD_SUBBID
    ,"left");
    $this->db->join(TBL_AJBK_JABATAN_BEZETTING,
    TBL_AJBK_JABATAN_BEZETTING.'.'.COL_KD_JABATAN." = ".TBL_AJBK_JABATAN.".".COL_KD_JABATAN." AND ".
    TBL_AJBK_JABATAN_BEZETTING.'.'.COL_TAHUN." = ".(!empty($Tahun)?$Tahun:date('Y'))
    ,"left");

    $data['kdOPD'] = $kdOPD_;
    $data['res'] = $this->db
    ->order_by('Kelas','desc')
    ->order_by(TBL_AJBK_JABATAN.'.'.COL_KD_TYPE,'desc')
    ->order_by('ajbk_unit.Nm_Sub_Unit', 'asc')
    ->order_by('ajbk_unit_bid.Nm_Bid', 'asc')
    ->order_by('ajbk_unit_subbid.Nm_Subbid', 'asc')

    ->order_by('ajbk_jabatan.Nm_Jabatan', 'asc')
    ->order_by('ajbk_nomenklatur.Nm_Nomenklatur', 'asc')
    ->get(TBL_AJBK_JABATAN)
    ->result_array();

    if($isCetak == 1) {
      $this->load->view('ajbk/jabatan/rekapitulasi_beban_cetak', $data);
    } else {
      $this->load->view('ajbk/jabatan/rekapitulasi_beban_partial', $data);
    }
  }

  public function rekapitulasi_prestasi()
  {
    $ruser = GetLoggedUser();
    $strOPD = explode('.', $ruser[COL_COMPANYID]);
    $data['title'] = 'Rekapitulasi Prestasi Jabatan';
    $data['load'] = site_url('ajbk/jabatan/rekapitulasi-prestasi-partial');
    $this->template->load('backend', 'ajbk/jabatan/rekapitulasi_', $data);
  }

  public function rekapitulasi_prestasi_partial($_tipe=-1, $_opd='', $_bid=-1, $noaction = 0, $_tahun=0, $isCetak=0)
  {
    $kdOPD_ = !empty($this->input->post("KdOPD")) ? $this->input->post("KdOPD") : $_opd;
    $kdBidang_ = !empty($this->input->post("KdBidang")) ? $this->input->post("KdBidang") : ($_bid > 0 ? $_bid : null);
    $kdSubBidang_ = $this->input->post("KdSubBidang");
    $Tahun = !empty($this->input->post(COL_TAHUN)) ? $this->input->post(COL_TAHUN) : ($_tahun > 0 ? $_tahun : date('Y'));
    $tipe_ = !empty($this->input->post("tipe")) ? $this->input->post("tipe") : $_tipe;

    if(!empty($kdSubBidang_)) {
      $rsubbid = $this->db->where(COL_UNIQ, $kdSubBidang_)->get(TBL_AJBK_UNIT_SUBBID)->row_array();
      if(empty($rsubbid)) {
        echo 'Filter tidak valid.';
        return;
      }

      $kdUrusan = $rsubbid[COL_KD_URUSAN];
      $kdBidang = $rsubbid[COL_KD_BIDANG];
      $kdUnit = $rsubbid[COL_KD_UNIT];
      $kdSub = $rsubbid[COL_KD_SUB];
      $kdBid = $rsubbid[COL_KD_BID];
      $kdSubbid = $rsubbid[COL_KD_SUBBID];
    } else if(!empty($kdBidang_)) {
      $rbid = $this->db->where(COL_UNIQ, $kdBidang_)->get(TBL_AJBK_UNIT_BID)->row_array();
      if(empty($rbid)) {
        echo 'Filter tidak valid.';
        return;
      }

      $kdUrusan = $rbid[COL_KD_URUSAN];
      $kdBidang = $rbid[COL_KD_BIDANG];
      $kdUnit = $rbid[COL_KD_UNIT];
      $kdSub = $rbid[COL_KD_SUB];
      $kdBid = $rbid[COL_KD_BID];
    } else if(!empty($kdOPD_)) {
      $ropd = $this->db->where(COL_UNIQ, $kdOPD_)->get(TBL_AJBK_UNIT)->row_array();
      if(empty($ropd)) {
        echo 'Filter tidak valid : '.$kdOPD_.'.';
        return;
      }

      $kdUrusan = $ropd[COL_KD_URUSAN];
      $kdBidang = $ropd[COL_KD_BIDANG];
      $kdUnit = $ropd[COL_KD_UNIT];
      $kdSub = $ropd[COL_KD_SUB];
    }

    $this->db->select("
    ajbk_jabatan.*,
    COALESCE(ajbk_jabatan.Nm_Jabatan, ajbk_nomenklatur.Nm_Nomenklatur) as Nm_Jabatan,
    ajbk_unit.Nm_Sub_Unit,
    ajbk_unit_bid.Nm_Bid,
    ajbk_unit_subbid.Nm_Subbid,
    (select count(*) from ajbk_jabatan_uraian u where u.Kd_Jabatan = ajbk_jabatan.Kd_Jabatan) as Uraian,
    (select sum(Jlh_Beban*Jlh_Jam) from ajbk_jabatan_uraian u where u.Kd_Jabatan = ajbk_jabatan.Kd_Jabatan) as Beban,
    (select sum(Jlh_Beban*Jlh_Jam) from ajbk_jabatan_uraian u where u.Kd_Jabatan = ajbk_jabatan.Kd_Jabatan)/1300 as Pegawai,
    ajbk_jabatan_bezetting.Jlh_Pegawai as Bezetting,
    ajbk_jabatan_bezetting.Tahun,
    CONVERT(ajbk_jabatan.KelasJab, DECIMAL) as Kelas
    ");
    if(isset($kdUrusan)) $this->db->where(TBL_AJBK_JABATAN.".".COL_KD_URUSAN, $kdUrusan);
    if(isset($kdBidang)) $this->db->where(TBL_AJBK_JABATAN.".".COL_KD_BIDANG, $kdBidang);
    if(isset($kdUnit)) $this->db->where(TBL_AJBK_JABATAN.".".COL_KD_UNIT, $kdUnit);
    if(isset($kdSub)) $this->db->where(TBL_AJBK_JABATAN.".".COL_KD_SUB, $kdSub);
    if(isset($kdBid)) $this->db->where(TBL_AJBK_JABATAN.".".COL_KD_BID, $kdBid);
    if(isset($kdSubbid)) $this->db->where(TBL_AJBK_JABATAN.".".COL_KD_SUBBID, $kdSubbid);
    if(!empty($tipe_)&&$tipe_!=-1) $this->db->where(TBL_AJBK_JABATAN.".".COL_KD_TYPE, $tipe_);
    if($noaction && $_opd == 20 && $_bid <= 0) {
      $this->db->where_not_in(TBL_AJBK_JABATAN.".".COL_KD_BID, array(10,11,12,13,14));
    }
    if($noaction && $_opd == 2 && $_bid <= 0) {
      $this->db
      ->group_start()
      ->where_not_in(TBL_AJBK_JABATAN.".".COL_KD_BID, array(5,6,7,8,9,10,11,12,13,14,15))
      ->or_where(TBL_AJBK_JABATAN.".".COL_KD_BID,null)
      ->group_end();
    }
    if($noaction && $_opd == 1 && $_bid <= 0) {
      $this->db
      ->group_start()
      ->where_in(TBL_AJBK_JABATAN.".".COL_KD_BID, array(1,2,3,4))
      ->or_where(TBL_AJBK_JABATAN.".".COL_KD_BID,null)
      ->group_end();
    }

    $this->db->join(TBL_AJBK_NOMENKLATUR,TBL_AJBK_NOMENKLATUR.'.'.COL_KD_NOMENKLATUR." = ".TBL_AJBK_JABATAN.".".COL_KD_NOMENKLATUR,"left");
    $this->db->join(TBL_AJBK_UNIT,
    TBL_AJBK_UNIT.'.'.COL_KD_URUSAN." = ".TBL_AJBK_JABATAN.".".COL_KD_URUSAN." AND ".
    TBL_AJBK_UNIT.'.'.COL_KD_BIDANG." = ".TBL_AJBK_JABATAN.".".COL_KD_BIDANG." AND ".
    TBL_AJBK_UNIT.'.'.COL_KD_UNIT." = ".TBL_AJBK_JABATAN.".".COL_KD_UNIT." AND ".
    TBL_AJBK_UNIT.'.'.COL_KD_SUB." = ".TBL_AJBK_JABATAN.".".COL_KD_SUB
    ,"left");
    $this->db->join(TBL_AJBK_UNIT_BID,
    TBL_AJBK_UNIT_BID.'.'.COL_KD_URUSAN." = ".TBL_AJBK_JABATAN.".".COL_KD_URUSAN." AND ".
    TBL_AJBK_UNIT_BID.'.'.COL_KD_BIDANG." = ".TBL_AJBK_JABATAN.".".COL_KD_BIDANG." AND ".
    TBL_AJBK_UNIT_BID.'.'.COL_KD_UNIT." = ".TBL_AJBK_JABATAN.".".COL_KD_UNIT." AND ".
    TBL_AJBK_UNIT_BID.'.'.COL_KD_SUB." = ".TBL_AJBK_JABATAN.".".COL_KD_SUB." AND ".
    TBL_AJBK_UNIT_BID.'.'.COL_KD_BID." = ".TBL_AJBK_JABATAN.".".COL_KD_BID
    ,"left");
    $this->db->join(TBL_AJBK_UNIT_SUBBID,
    TBL_AJBK_UNIT_SUBBID.'.'.COL_KD_URUSAN." = ".TBL_AJBK_JABATAN.".".COL_KD_URUSAN." AND ".
    TBL_AJBK_UNIT_SUBBID.'.'.COL_KD_BIDANG." = ".TBL_AJBK_JABATAN.".".COL_KD_BIDANG." AND ".
    TBL_AJBK_UNIT_SUBBID.'.'.COL_KD_UNIT." = ".TBL_AJBK_JABATAN.".".COL_KD_UNIT." AND ".
    TBL_AJBK_UNIT_SUBBID.'.'.COL_KD_SUB." = ".TBL_AJBK_JABATAN.".".COL_KD_SUB." AND ".
    TBL_AJBK_UNIT_SUBBID.'.'.COL_KD_BID." = ".TBL_AJBK_JABATAN.".".COL_KD_BID." AND ".
    TBL_AJBK_UNIT_SUBBID.'.'.COL_KD_SUBBID." = ".TBL_AJBK_JABATAN.".".COL_KD_SUBBID
    ,"left");
    $this->db->join(TBL_AJBK_JABATAN_BEZETTING,
    TBL_AJBK_JABATAN_BEZETTING.'.'.COL_KD_JABATAN." = ".TBL_AJBK_JABATAN.".".COL_KD_JABATAN." AND ".
    TBL_AJBK_JABATAN_BEZETTING.'.'.COL_TAHUN." = ".(!empty($Tahun)?$Tahun:date('Y'))
    ,"left");

    $data['tipe'] = $tipe_;
    $data['tahun'] = $Tahun;
    $data['kdOPD'] = $kdOPD_;
    $data['noaction'] = $noaction;
    $data['res'] = $this->db
    ->order_by('Kelas','desc')
    ->order_by(TBL_AJBK_JABATAN.'.'.COL_KD_TYPE,'desc')
    ->order_by('ajbk_unit.Nm_Sub_Unit', 'asc')
    ->order_by('ajbk_unit_bid.Nm_Bid', 'asc')
    ->order_by('ajbk_unit_subbid.Nm_Subbid', 'asc')

    ->order_by('ajbk_jabatan.Nm_Jabatan', 'asc')
    ->order_by('ajbk_nomenklatur.Nm_Nomenklatur', 'asc')
    ->get(TBL_AJBK_JABATAN)
    ->result_array();

    if($isCetak == 1) {
      $this->load->view('ajbk/jabatan/rekapitulasi_prestasi_cetak', $data);
    } else {
      $this->load->view('ajbk/jabatan/rekapitulasi_prestasi_partial', $data);
    }

  }

  public function duplicate($id) {
    if(!empty($_POST)) {
      $rduplicated = $this->db
      ->where(COL_KD_JABATAN, $id)
      ->get(TBL_AJBK_JABATAN)
      ->row_array();

      if(empty($rduplicated)) {
        ShowJsonError('Parameter tidak valid!');
        return;
      }

      $rduplicated[COL_KD_JABATAN]=null;

      $kdOPD_ = !empty($this->input->post("KdOPD"))?$this->input->post("KdOPD"):null;
      $kdBidang_ = !empty($this->input->post("KdBidang"))?$this->input->post("KdBidang"):null;
      $kdSubBidang_ = !empty($this->input->post("KdSubBidang"))?$this->input->post("KdSubBidang"):null;;

      if(!empty($kdSubBidang_)) {
        $rsubbid = $this->db->where(COL_UNIQ, $kdSubBidang_)->get(TBL_AJBK_UNIT_SUBBID)->row_array();
        if(empty($rsubbid)) {
          ShowJsonError('Parameter tidak valid!');
          return;
        }

        $rduplicated[COL_KD_URUSAN] = $rsubbid[COL_KD_URUSAN];
        $rduplicated[COL_KD_BIDANG] = $rsubbid[COL_KD_BIDANG];
        $rduplicated[COL_KD_UNIT] = $rsubbid[COL_KD_UNIT];
        $rduplicated[COL_KD_SUB] = $rsubbid[COL_KD_SUB];
        $rduplicated[COL_KD_BID] = $rsubbid[COL_KD_BID];
        $rduplicated[COL_KD_SUBBID] = $rsubbid[COL_KD_SUBBID];
      } else if(!empty($kdBidang_)) {
        $rbid = $this->db->where(COL_UNIQ, $kdBidang_)->get(TBL_AJBK_UNIT_BID)->row_array();
        if(empty($rbid)) {
          ShowJsonError('Parameter tidak valid!');
          return;
        }

        $rduplicated[COL_KD_URUSAN] = $rbid[COL_KD_URUSAN];
        $rduplicated[COL_KD_BIDANG] = $rbid[COL_KD_BIDANG];
        $rduplicated[COL_KD_UNIT] = $rbid[COL_KD_UNIT];
        $rduplicated[COL_KD_SUB] = $rbid[COL_KD_SUB];
        $rduplicated[COL_KD_BID] = $rbid[COL_KD_BID];
      } else if(!empty($kdOPD_)) {
        $ropd = $this->db->where(COL_UNIQ, $kdOPD_)->get(TBL_AJBK_UNIT)->row_array();
        if(empty($ropd)) {
          ShowJsonError('Parameter tidak valid!');
          return;
        }

        $rduplicated[COL_KD_URUSAN] = $ropd[COL_KD_URUSAN];
        $rduplicated[COL_KD_BIDANG] = $ropd[COL_KD_BIDANG];
        $rduplicated[COL_KD_UNIT] = $ropd[COL_KD_UNIT];
        $rduplicated[COL_KD_SUB] = $ropd[COL_KD_SUB];
      } else {
        ShowJsonError('Parameter tidak valid!');
        return;
      }

      $rUraian = $this->db->where(COL_KD_JABATAN, $id)->get(TBL_AJBK_JABATAN_URAIAN)->result_array();
      $rJurusan = $this->db->where(COL_KD_JABATAN, $id)->get(TBL_AJBK_JABATAN_JURUSAN)->result_array();
      $rDiklat = $this->db->where(COL_KD_JABATAN, $id)->get(TBL_AJBK_JABATAN_DIKLAT)->result_array();
      $arrUraian = array();
      $arrJurusan = array();
      $arrDiklat = array();

      $this->db->trans_begin();
      try{
        $res = $this->db->insert(TBL_AJBK_JABATAN, $rduplicated);
        if(!$res) {
          $err = $this->db->error();
          throw new Exception("Database error: ".$err['message']);
        }
        $idInserted = $this->db->insert_id();

        foreach($rUraian as $u) {
          $arrUraian[]=array(
            COL_KD_JABATAN=>$idInserted,
            COL_NM_URAIAN=>$u[COL_NM_URAIAN],
            COL_KD_SATUAN=>$u[COL_KD_SATUAN],
            COL_JLH_BEBAN=>$u[COL_JLH_BEBAN],
            COL_JLH_JAM=>$u[COL_JLH_JAM]
          );
        }
        foreach($rJurusan as $u) {
          $arrJurusan[]=array(
            COL_KD_JABATAN=>$idInserted,
            COL_KD_JURUSAN=>$u[COL_KD_JURUSAN]
          );
        }
        foreach($rDiklat as $u) {
          $arrDiklat[]=array(
            COL_KD_JABATAN=>$idInserted,
            COL_KD_DIKLAT=>$u[COL_KD_DIKLAT]
          );
        }

        if(!empty($arrUraian)) {
          $res = $this->db->insert_batch(TBL_AJBK_JABATAN_URAIAN, $arrUraian);
          if(!$res) {
            $err = $this->db->error();
            throw new Exception("Database error 1: ".$err['message'].'; '.$this->db->last_query());
          }
        }
        if(!empty($arrJurusan)) {
          $res = $this->db->insert_batch(TBL_AJBK_JABATAN_JURUSAN, $arrJurusan);
          if(!$res) {
            $err = $this->db->error();
            throw new Exception("Database error 2: ".$err['message'].'; '.$this->db->last_query());
          }
        }
        if(!empty($arrDiklat)) {
          $res = $this->db->insert_batch(TBL_AJBK_JABATAN_DIKLAT, $arrDiklat);
          if(!$res) {
            $err = $this->db->error();
            throw new Exception("Database error 3: ".$err['message'].'; '.$this->db->last_query());
          }
        }

        $this->db->trans_commit();

        $rdatInserted = $this->db
        ->select('COALESCE(ajbk_jabatan.Nm_Jabatan, ajbk_nomenklatur.Nm_Nomenklatur) as Desc_Jabatan')
        ->join(TBL_AJBK_NOMENKLATUR,TBL_AJBK_NOMENKLATUR.'.'.COL_KD_NOMENKLATUR." = ".TBL_AJBK_JABATAN.".".COL_KD_NOMENKLATUR,"left")
        ->where(TBL_AJBK_JABATAN.".".COL_KD_JABATAN, $idInserted)
        ->get(TBL_AJBK_JABATAN)
        ->row_array();
        ShowJsonSuccess('Duplikasi '.$rdatInserted['Desc_Jabatan'].' berhasil! ID = '.$idInserted.', BID = '.$kdBidang_);
        exit();
      } catch (Exception $e) {
        $this->db->trans_rollback();
        ShowJsonError($e->getMessage());
        exit();
      }
    } else {
      ShowJsonError('Parameter tidak valid!');
      exit();
    }
    ShowJsonSuccess('Duplicate #'.$id);
    exit();
  }

  public function peta()
  {
    $ruser = GetLoggedUser();
    $strOPD = explode('.', $ruser[COL_COMPANYID]);
    $data['title'] = 'Peta Jabatan';
    $data['load'] = site_url('ajbk/jabatan/peta-partial');
    $this->template->load('backend', 'ajbk/jabatan/peta_', $data);
  }

  public function peta_partial($_opd='', $isCetak=0)
  {
    $hourperyear = HOURPERYEAR;
    $kdOPD_ = !empty($this->input->post("KdOPD")) ? $this->input->post("KdOPD") : $_opd;
    //$kdBidang_ = !empty($this->input->post("KdBidang")) ? $this->input->post("KdBidang") : ($_bid > 0 ? $_bid : null);
    //$kdSubBidang_ = $this->input->post("KdSubBidang");
    $Tahun = $this->input->post(COL_TAHUN);
    //$tipe_ = !empty($this->input->post("tipe")) ? $this->input->post("tipe") : $_tipe;

    if(!empty($kdSubBidang_)) {
      $rsubbid = $this->db->where(COL_UNIQ, $kdSubBidang_)->get(TBL_AJBK_UNIT_SUBBID)->row_array();
      if(empty($rsubbid)) {
        echo 'Filter tidak valid.';
        return;
      }

      $kdUrusan = $rsubbid[COL_KD_URUSAN];
      $kdBidang = $rsubbid[COL_KD_BIDANG];
      $kdUnit = $rsubbid[COL_KD_UNIT];
      $kdSub = $rsubbid[COL_KD_SUB];
      $kdBid = $rsubbid[COL_KD_BID];
      $kdSubbid = $rsubbid[COL_KD_SUBBID];
    } else if(!empty($kdBidang_)) {
      $rbid = $this->db->where(COL_UNIQ, $kdBidang_)->get(TBL_AJBK_UNIT_BID)->row_array();
      if(empty($rbid)) {
        echo 'Filter tidak valid.';
        return;
      }

      $kdUrusan = $rbid[COL_KD_URUSAN];
      $kdBidang = $rbid[COL_KD_BIDANG];
      $kdUnit = $rbid[COL_KD_UNIT];
      $kdSub = $rbid[COL_KD_SUB];
      $kdBid = $rbid[COL_KD_BID];
    } else if(!empty($kdOPD_)) {
      $ropd = $this->db->where(COL_UNIQ, $kdOPD_)->get(TBL_AJBK_UNIT)->row_array();
      if(empty($ropd)) {
        echo 'Filter tidak valid.';
        return;
      }

      $kdUrusan = $ropd[COL_KD_URUSAN];
      $kdBidang = $ropd[COL_KD_BIDANG];
      $kdUnit = $ropd[COL_KD_UNIT];
      $kdSub = $ropd[COL_KD_SUB];
    }

    $this->db->select("
    ajbk_jabatan.*,
    COALESCE(ajbk_jabatan.Nm_Jabatan, ajbk_nomenklatur.Nm_Nomenklatur) as Nm_Jabatan,
    ajbk_unit.Nm_Sub_Unit,
    ajbk_unit_bid.Nm_Bid,
    ajbk_unit_subbid.Nm_Subbid,
    (select count(*) from ajbk_jabatan_uraian u where u.Kd_Jabatan = ajbk_jabatan.Kd_Jabatan) as Uraian,
    (select sum(Jlh_Beban*Jlh_Jam) from ajbk_jabatan_uraian u where u.Kd_Jabatan = ajbk_jabatan.Kd_Jabatan) as Beban,
    round((select sum(Jlh_Beban*Jlh_Jam) from ajbk_jabatan_uraian u where u.Kd_Jabatan = ajbk_jabatan.Kd_Jabatan)/$hourperyear) as Pegawai,
    ajbk_jabatan_bezetting.Jlh_Pegawai as Bezetting,
    ajbk_jabatan_bezetting.Tahun
    ");
    if(isset($kdUrusan)) $this->db->where(TBL_AJBK_JABATAN.".".COL_KD_URUSAN, $kdUrusan);
    if(isset($kdBidang)) $this->db->where(TBL_AJBK_JABATAN.".".COL_KD_BIDANG, $kdBidang);
    if(isset($kdUnit)) $this->db->where(TBL_AJBK_JABATAN.".".COL_KD_UNIT, $kdUnit);
    if(isset($kdSub)) $this->db->where(TBL_AJBK_JABATAN.".".COL_KD_SUB, $kdSub);
    /*if(isset($kdBid)) $this->db->where(TBL_AJBK_JABATAN.".".COL_KD_BID, $kdBid);
    if(isset($kdSubbid)) $this->db->where(TBL_AJBK_JABATAN.".".COL_KD_SUBBID, $kdSubbid);*/
    //if(!empty($tipe_)) $this->db->where(TBL_AJBK_JABATAN.".".COL_KD_TYPE, $tipe_);

    $this->db->where(TBL_AJBK_JABATAN.".".COL_KD_TYPE, 'STR');
    $this->db->where(TBL_AJBK_JABATAN.".".COL_KD_BID, null);
    $this->db->where(TBL_AJBK_JABATAN.".".COL_KD_SUBBID, null);

    $this->db->join(TBL_AJBK_NOMENKLATUR,TBL_AJBK_NOMENKLATUR.'.'.COL_KD_NOMENKLATUR." = ".TBL_AJBK_JABATAN.".".COL_KD_NOMENKLATUR,"left");
    $this->db->join(TBL_AJBK_UNIT,
    TBL_AJBK_UNIT.'.'.COL_KD_URUSAN." = ".TBL_AJBK_JABATAN.".".COL_KD_URUSAN." AND ".
    TBL_AJBK_UNIT.'.'.COL_KD_BIDANG." = ".TBL_AJBK_JABATAN.".".COL_KD_BIDANG." AND ".
    TBL_AJBK_UNIT.'.'.COL_KD_UNIT." = ".TBL_AJBK_JABATAN.".".COL_KD_UNIT." AND ".
    TBL_AJBK_UNIT.'.'.COL_KD_SUB." = ".TBL_AJBK_JABATAN.".".COL_KD_SUB
    ,"left");
    $this->db->join(TBL_AJBK_UNIT_BID,
    TBL_AJBK_UNIT_BID.'.'.COL_KD_URUSAN." = ".TBL_AJBK_JABATAN.".".COL_KD_URUSAN." AND ".
    TBL_AJBK_UNIT_BID.'.'.COL_KD_BIDANG." = ".TBL_AJBK_JABATAN.".".COL_KD_BIDANG." AND ".
    TBL_AJBK_UNIT_BID.'.'.COL_KD_UNIT." = ".TBL_AJBK_JABATAN.".".COL_KD_UNIT." AND ".
    TBL_AJBK_UNIT_BID.'.'.COL_KD_SUB." = ".TBL_AJBK_JABATAN.".".COL_KD_SUB." AND ".
    TBL_AJBK_UNIT_BID.'.'.COL_KD_BID." = ".TBL_AJBK_JABATAN.".".COL_KD_BID
    ,"left");
    $this->db->join(TBL_AJBK_UNIT_SUBBID,
    TBL_AJBK_UNIT_SUBBID.'.'.COL_KD_URUSAN." = ".TBL_AJBK_JABATAN.".".COL_KD_URUSAN." AND ".
    TBL_AJBK_UNIT_SUBBID.'.'.COL_KD_BIDANG." = ".TBL_AJBK_JABATAN.".".COL_KD_BIDANG." AND ".
    TBL_AJBK_UNIT_SUBBID.'.'.COL_KD_UNIT." = ".TBL_AJBK_JABATAN.".".COL_KD_UNIT." AND ".
    TBL_AJBK_UNIT_SUBBID.'.'.COL_KD_SUB." = ".TBL_AJBK_JABATAN.".".COL_KD_SUB." AND ".
    TBL_AJBK_UNIT_SUBBID.'.'.COL_KD_BID." = ".TBL_AJBK_JABATAN.".".COL_KD_BID." AND ".
    TBL_AJBK_UNIT_SUBBID.'.'.COL_KD_SUBBID." = ".TBL_AJBK_JABATAN.".".COL_KD_SUBBID
    ,"left");
    $this->db->join(TBL_AJBK_JABATAN_BEZETTING,
    TBL_AJBK_JABATAN_BEZETTING.'.'.COL_KD_JABATAN." = ".TBL_AJBK_JABATAN.".".COL_KD_JABATAN." AND ".
    TBL_AJBK_JABATAN_BEZETTING.'.'.COL_TAHUN." = ".(!empty($Tahun)?$Tahun:date('Y'))
    ,"left");

    $data['kdOPD'] = $kdOPD_;
    $data['tahun'] = $Tahun;
    $data['res'] = $this->db
    ->order_by(TBL_AJBK_JABATAN.'.'.COL_KD_TYPE,'desc')
    ->order_by('ajbk_unit.Nm_Sub_Unit', 'asc')
    ->order_by('ajbk_unit_bid.Nm_Bid', 'asc')
    ->order_by('ajbk_unit_subbid.Nm_Subbid', 'asc')

    ->order_by('ajbk_jabatan.Nm_Jabatan', 'asc')
    ->order_by('ajbk_nomenklatur.Nm_Nomenklatur', 'asc')
    ->get(TBL_AJBK_JABATAN)
    ->result_array();

    $this->load->view('ajbk/jabatan/peta_partial', $data);
  }

  public function verifikasi() {
    $hourperyear = HOURPERYEAR;
    $ruser = GetLoggedUser();
    if($ruser[COL_ROLEID]!=ROLEADMIN) {
      show_error('Anda tidak memiliki hak akses.');
      exit();
    }

    $data['title'] = 'Verifikasi Informasi Jabatan';
    $data['res'] = array();
    if(!empty($_GET)) {
      $kdOPD_ = !empty($this->input->get("KdOPD")) ? $this->input->get("KdOPD") : $_opd;
      $kdBidang_ = !empty($this->input->get("KdBidang")) ? $this->input->get("KdBidang") : null;
      $kdSubBidang_ = $this->input->get("KdSubBidang");
      $status = $this->input->get("Verified");

      if(!empty($kdSubBidang_)) {
        $rsubbid = $this->db->where(COL_UNIQ, $kdSubBidang_)->get(TBL_AJBK_UNIT_SUBBID)->row_array();
        if(empty($rsubbid)) {
          echo 'Filter tidak valid.';
          return;
        }

        $kdUrusan = $rsubbid[COL_KD_URUSAN];
        $kdBidang = $rsubbid[COL_KD_BIDANG];
        $kdUnit = $rsubbid[COL_KD_UNIT];
        $kdSub = $rsubbid[COL_KD_SUB];
        $kdBid = $rsubbid[COL_KD_BID];
        $kdSubbid = $rsubbid[COL_KD_SUBBID];
      } else if(!empty($kdBidang_)) {
        $rbid = $this->db->where(COL_UNIQ, $kdBidang_)->get(TBL_AJBK_UNIT_BID)->row_array();
        if(empty($rbid)) {
          echo 'Filter tidak valid.';
          return;
        }

        $kdUrusan = $rbid[COL_KD_URUSAN];
        $kdBidang = $rbid[COL_KD_BIDANG];
        $kdUnit = $rbid[COL_KD_UNIT];
        $kdSub = $rbid[COL_KD_SUB];
        $kdBid = $rbid[COL_KD_BID];
      } else if(!empty($kdOPD_)) {
        $ropd = $this->db->where(COL_UNIQ, $kdOPD_)->get(TBL_AJBK_UNIT)->row_array();
        if(empty($ropd)) {
          echo 'Filter tidak valid.';
          return;
        }

        $kdUrusan = $ropd[COL_KD_URUSAN];
        $kdBidang = $ropd[COL_KD_BIDANG];
        $kdUnit = $ropd[COL_KD_UNIT];
        $kdSub = $ropd[COL_KD_SUB];
      }

      $this->db->select("
      ajbk_jabatan.*,
      COALESCE(ajbk_jabatan.Nm_Jabatan, ajbk_nomenklatur.Nm_Nomenklatur) as Nm_Jabatan,
      ajbk_unit.Nm_Sub_Unit,
      ajbk_unit_bid.Nm_Bid,
      ajbk_unit_subbid.Nm_Subbid,
      (select count(*) from ajbk_jabatan_uraian u where u.Kd_Jabatan = ajbk_jabatan.Kd_Jabatan) as Uraian,
      (select sum(Jlh_Beban*Jlh_Jam) from ajbk_jabatan_uraian u where u.Kd_Jabatan = ajbk_jabatan.Kd_Jabatan) as Beban,
      (select sum(Jlh_Beban*Jlh_Jam) from ajbk_jabatan_uraian u where u.Kd_Jabatan = ajbk_jabatan.Kd_Jabatan)/$hourperyear as Pegawai,
      ");
      if(isset($kdUrusan)) $this->db->where(TBL_AJBK_JABATAN.".".COL_KD_URUSAN, $kdUrusan);
      if(isset($kdBidang)) $this->db->where(TBL_AJBK_JABATAN.".".COL_KD_BIDANG, $kdBidang);
      if(isset($kdUnit)) $this->db->where(TBL_AJBK_JABATAN.".".COL_KD_UNIT, $kdUnit);
      if(isset($kdSub)) $this->db->where(TBL_AJBK_JABATAN.".".COL_KD_SUB, $kdSub);
      if(isset($kdBid)) $this->db->where(TBL_AJBK_JABATAN.".".COL_KD_BID, $kdBid);
      if(isset($kdSubbid)) $this->db->where(TBL_AJBK_JABATAN.".".COL_KD_SUBBID, $kdSubbid);
      if(isset($status)) {
        if($status==1) $this->db->where(TBL_AJBK_JABATAN.".".COL_VERIFIED, 1);
        else if($status==2) $this->db->where(TBL_AJBK_JABATAN.".".COL_VERIFIED, 0);
      }

      $this->db->join(TBL_AJBK_NOMENKLATUR,TBL_AJBK_NOMENKLATUR.'.'.COL_KD_NOMENKLATUR." = ".TBL_AJBK_JABATAN.".".COL_KD_NOMENKLATUR,"left");
      $this->db->join(TBL_AJBK_UNIT,
      TBL_AJBK_UNIT.'.'.COL_KD_URUSAN." = ".TBL_AJBK_JABATAN.".".COL_KD_URUSAN." AND ".
      TBL_AJBK_UNIT.'.'.COL_KD_BIDANG." = ".TBL_AJBK_JABATAN.".".COL_KD_BIDANG." AND ".
      TBL_AJBK_UNIT.'.'.COL_KD_UNIT." = ".TBL_AJBK_JABATAN.".".COL_KD_UNIT." AND ".
      TBL_AJBK_UNIT.'.'.COL_KD_SUB." = ".TBL_AJBK_JABATAN.".".COL_KD_SUB
      ,"left");
      $this->db->join(TBL_AJBK_UNIT_BID,
      TBL_AJBK_UNIT_BID.'.'.COL_KD_URUSAN." = ".TBL_AJBK_JABATAN.".".COL_KD_URUSAN." AND ".
      TBL_AJBK_UNIT_BID.'.'.COL_KD_BIDANG." = ".TBL_AJBK_JABATAN.".".COL_KD_BIDANG." AND ".
      TBL_AJBK_UNIT_BID.'.'.COL_KD_UNIT." = ".TBL_AJBK_JABATAN.".".COL_KD_UNIT." AND ".
      TBL_AJBK_UNIT_BID.'.'.COL_KD_SUB." = ".TBL_AJBK_JABATAN.".".COL_KD_SUB." AND ".
      TBL_AJBK_UNIT_BID.'.'.COL_KD_BID." = ".TBL_AJBK_JABATAN.".".COL_KD_BID
      ,"left");
      $this->db->join(TBL_AJBK_UNIT_SUBBID,
      TBL_AJBK_UNIT_SUBBID.'.'.COL_KD_URUSAN." = ".TBL_AJBK_JABATAN.".".COL_KD_URUSAN." AND ".
      TBL_AJBK_UNIT_SUBBID.'.'.COL_KD_BIDANG." = ".TBL_AJBK_JABATAN.".".COL_KD_BIDANG." AND ".
      TBL_AJBK_UNIT_SUBBID.'.'.COL_KD_UNIT." = ".TBL_AJBK_JABATAN.".".COL_KD_UNIT." AND ".
      TBL_AJBK_UNIT_SUBBID.'.'.COL_KD_SUB." = ".TBL_AJBK_JABATAN.".".COL_KD_SUB." AND ".
      TBL_AJBK_UNIT_SUBBID.'.'.COL_KD_BID." = ".TBL_AJBK_JABATAN.".".COL_KD_BID." AND ".
      TBL_AJBK_UNIT_SUBBID.'.'.COL_KD_SUBBID." = ".TBL_AJBK_JABATAN.".".COL_KD_SUBBID
      ,"left");

      $data['res'] = $this->db
      ->order_by('ajbk_jabatan.Verified', 'desc')
      ->order_by(TBL_AJBK_JABATAN.'.'.COL_KD_TYPE,'desc')
      ->order_by('ajbk_unit.Nm_Sub_Unit', 'asc')
      ->order_by('ajbk_unit_bid.Nm_Bid', 'asc')
      ->order_by('ajbk_unit_subbid.Nm_Subbid', 'asc')

      ->order_by('ajbk_jabatan.Nm_Jabatan', 'asc')
      ->order_by('ajbk_nomenklatur.Nm_Nomenklatur', 'asc')
      ->get(TBL_AJBK_JABATAN)
      ->result_array();
    }
    $this->template->load('backend', 'ajbk/jabatan/verifikasi', $data);
  }

  public function change_status($id) {
    $ruser = GetLoggedUser();
    $status = $this->input->post('Status');

    $res = true;
    /* CHECK OPD */
    $rdata = $this->db->where(COL_KD_JABATAN,$id)->get(TBL_AJBK_JABATAN)->row_array();
    if(!$rdata) {
      ShowJsonError("Parameter tidak valid!");
      exit();
    }
    $ropd = $this->db->where(array(
      COL_KD_URUSAN=>$rdata[COL_KD_URUSAN],
      COL_KD_BIDANG=>$rdata[COL_KD_BIDANG],
      COL_KD_UNIT=>$rdata[COL_KD_UNIT],
      COL_KD_SUB=>$rdata[COL_KD_SUB]
    ))
    ->get(TBL_AJBK_UNIT)
    ->row_array();
    if(!$ropd) {
      ShowJsonError("OPD tidak valid!");
      exit();
    }
    if($ropd[COL_ISAKTIF]!=1) {
      ShowJsonError("Gagal menghapus data. OPD dalam keadaan SUSPEND / NONAKTIF!");
      exit();
    }
    /* --CHECK OPD-- */

    $res = $this->db
    ->where(array(COL_KD_JABATAN=>$id))
    ->update(TBL_AJBK_JABATAN, array(
      COL_VERIFIED=>$status,
      COL_VERIFIED_BY=>$status==1?$ruser[COL_USERNAME]:null,
      COL_VERIFIED_ON=>$status==1?date('Y-m-d H:i:s'):null
    ));

    if(!$res) {
      ShowJsonError("Gagal mengupdate data.");
    } else {
      ShowJsonSuccess("Berhasil");
    }
  }
}
