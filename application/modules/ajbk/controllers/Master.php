<?php
class Master extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('encrypt');
        $this->load->model('muser');
        if (!IsLogin()) {
            redirect('site/home');
        }
    }

    public function nomenklatur_index()
    {
        if (GetLoggedUser()[COL_ROLEID] != ROLEADMIN) {
            redirect('site/home');
        }
        $data['title'] = "Nomenklatur Jabatan";
        $data['res'] = $this->db
        ->join(TBL_AJBK_OPTION,TBL_AJBK_OPTION.'.'.COL_UNIQ." = ".TBL_AJBK_NOMENKLATUR.".".COL_KD_PENDIDIKAN,"left")
        ->get(TBL_AJBK_NOMENKLATUR)
        ->result_array();
        $this->template->load('backend', 'master/index_nomenklatur', $data);
    }

    public function nomenklatur_add()
    {
        $user = GetLoggedUser();
        if ($user[COL_ROLEID] != ROLEADMIN) {
            redirect('site/home');
        }
        if (!empty($_POST)) {
          $data = array(
            COL_NM_NOMENKLATUR => $this->input->post(COL_NM_NOMENKLATUR),
            COL_KD_PENDIDIKAN => $this->input->post(COL_KD_PENDIDIKAN),

            COL_CREATE_BY => $user[COL_USERNAME],
            COL_CREATE_DATE => date('Y-m-d H:i:s')
          );
          $res = $this->db->insert(TBL_AJBK_NOMENKLATUR, $data);
          if ($res) {
              ShowJsonSuccess("Berhasil");
          } else {
              ShowJsonError("Gagal");
          }
      }
    }

    public function nomenklatur_edit($id)
    {
        $user = GetLoggedUser();
        if ($user[COL_ROLEID] != ROLEADMIN) {
            redirect('site/home');
        }
        if (!empty($_POST)) {
          $data = array(
            COL_NM_NOMENKLATUR => $this->input->post(COL_NM_NOMENKLATUR),
            COL_KD_PENDIDIKAN => $this->input->post(COL_KD_PENDIDIKAN),

            COL_EDIT_BY => $user[COL_USERNAME],
            COL_EDIT_DATE => date('Y-m-d H:i:s')
          );

          $res = $this->db->where(COL_KD_NOMENKLATUR, $id)->update(TBL_AJBK_NOMENKLATUR, $data);
          if ($res) {
              ShowJsonSuccess("Berhasil");
          } else {
              ShowJsonError("Gagal");
          }
        }
    }

    public function nomenklatur_delete()
    {
        $user = GetLoggedUser();
        if ($user[COL_ROLEID] != ROLEADMIN) {
            redirect('site/home');
        }
        $data = $this->input->post('cekbox');
        $deleted = 0;
        foreach ($data as $datum) {
            $this->db->delete(TBL_AJBK_NOMENKLATUR, array(COL_KD_NOMENKLATUR => $datum));
            $deleted++;
        }
        if ($deleted) {
            ShowJsonSuccess($deleted." data dihapus");
        } else {
            ShowJsonError("Tidak ada dihapus");
        }
    }

    public function option_index($optcode)
    {
        if (GetLoggedUser()[COL_ROLEID] != ROLEADMIN) {
            redirect('site/home');
        }
        $data['title'] = "Master";
        $data['optcode'] = $optcode;
        $data['res'] = $this->db->where(COL_OPT_CODE, $optcode)->get(TBL_AJBK_OPTION)->result_array();
        $this->template->load('backend', 'master/index_option', $data);
    }

    public function option_add()
    {
        $user = GetLoggedUser();
        if ($user[COL_ROLEID] != ROLEADMIN) {
            redirect('site/home');
        }
        if (!empty($_POST)) {
            $data = array(
          COL_OPT_CODE => $this->input->post(COL_OPT_CODE),
          COL_OPT_NAME => $this->input->post(COL_OPT_NAME),
          COL_OPT_DESC => $this->input->post(COL_OPT_DESC),

          COL_CREATE_BY => $user[COL_USERNAME],
          COL_CREATE_DATE => date('Y-m-d H:i:s')
      );

            $res = $this->db->insert(TBL_AJBK_OPTION, $data);
            if ($res) {
                ShowJsonSuccess("Berhasil");
            } else {
                ShowJsonError("Gagal");
            }
        }
    }

    public function option_edit($id)
    {
        $user = GetLoggedUser();
        if ($user[COL_ROLEID] != ROLEADMIN) {
            redirect('site/home');
        }
        if (!empty($_POST)) {
            $data = array(
        COL_OPT_NAME => $this->input->post(COL_OPT_NAME),
        COL_OPT_DESC => $this->input->post(COL_OPT_DESC),

        COL_EDIT_BY => $user[COL_USERNAME],
        COL_EDIT_DATE => date('Y-m-d H:i:s')
      );

            $res = $this->db->where(COL_UNIQ, $id)->update(TBL_AJBK_OPTION, $data);
            if ($res) {
                ShowJsonSuccess("Berhasil");
            } else {
                ShowJsonError("Gagal");
            }
        }
    }

    public function option_delete()
    {
        $user = GetLoggedUser();
        if ($user[COL_ROLEID] != ROLEADMIN) {
            redirect('site/home');
        }
        $data = $this->input->post('cekbox');
        $deleted = 0;
        foreach ($data as $datum) {
            $this->db->delete(TBL_AJBK_OPTION, array(COL_UNIQ => $datum));
            $deleted++;
        }
        if ($deleted) {
            ShowJsonSuccess($deleted." data dihapus");
        } else {
            ShowJsonError("Tidak ada dihapus");
        }
    }

    public function bid_index()
    {
        $ruser = GetLoggedUser();
        if($ruser[COL_ROLEID] != ROLEADMIN && $ruser[COL_ROLEID] != ROLEKADIS) {
          redirect('site/home');
        }
        $strOPD = explode('.', $ruser[COL_COMPANYID]);
        $data['title'] = 'Unit Kerja Administrator';
        $this->db->select('*,'.TBL_AJBK_UNIT_BID.'.'.COL_UNIQ.' as ID');
        if ($ruser[COL_ROLEID] != ROLEADMIN) {
            $this->db->where(TBL_AJBK_UNIT_BID.".".COL_KD_URUSAN, $strOPD[0]);
            $this->db->where(TBL_AJBK_UNIT_BID.".".COL_KD_BIDANG, $strOPD[1]);
            $this->db->where(TBL_AJBK_UNIT_BID.".".COL_KD_UNIT, $strOPD[2]);
            $this->db->where(TBL_AJBK_UNIT_BID.".".COL_KD_SUB, $strOPD[3]);
        }
        $this->db->order_by(COL_KD_URUSAN, 'asc');
        $this->db->order_by(COL_KD_BIDANG, 'asc');
        $this->db->order_by(COL_KD_UNIT, 'asc');
        $this->db->order_by(COL_KD_SUB, 'asc');
        $this->db->order_by(COL_KD_BID, 'asc');
        $data['res'] = $this->db->get(TBL_AJBK_UNIT_BID)->result_array();
        $this->template->load('backend', 'ajbk/master/index_bid', $data);
    }

    public function bid_add()
    {
        $data['title'] = 'Unit Kerja Administrator';
        $data['edit'] = false;
        $ruser = GetLoggedUser();
        if($ruser[COL_ROLEID] != ROLEADMIN && $ruser[COL_ROLEID] != ROLEKADIS) {
          redirect('site/home');
        }

        if (!empty($_POST)) {
          /* CHECK OPD */
          $ropd = $this->db->where(array(
            COL_KD_URUSAN=>$this->input->post(COL_KD_URUSAN),
            COL_KD_BIDANG=>$this->input->post(COL_KD_BIDANG),
            COL_KD_UNIT=>$this->input->post(COL_KD_UNIT),
            COL_KD_SUB=>$this->input->post(COL_KD_SUB)
          ))
          ->get(TBL_AJBK_UNIT)
          ->row_array();
          if(!$ropd) {
            ShowJsonError("OPD tidak valid!");
            exit();
          }
          if($ropd[COL_ISAKTIF]!=1) {
            ShowJsonError("Gagal menginput data. OPD dalam keadaan SUSPEND / NONAKTIF!");
            exit();
          }
          /* --CHECK OPD-- */

            $resp = array();
            $resp['error'] = 0;
            $resp['success'] = 1;
            $resp['redirect'] = site_url('ajbk/master/bid-index');
            $data = array(
              COL_KD_URUSAN => $this->input->post(COL_KD_URUSAN),
              COL_KD_BIDANG => $this->input->post(COL_KD_BIDANG),
              COL_KD_UNIT => $this->input->post(COL_KD_UNIT),
              COL_KD_SUB => $this->input->post(COL_KD_SUB),
              COL_KD_BID => $this->input->post(COL_KD_BID),
              COL_NM_BID => $this->input->post(COL_NM_BID)
          );
            if (!$this->db->insert(TBL_AJBK_UNIT_BID, $data)) {
                $resp['error'] = 1;
                $resp['success'] = 0;
            }
            echo json_encode($resp);
        } else {
            $this->template->load('backend', 'ajbk/master/form_bid', $data);
        }
    }

    public function bid_edit($id)
    {
        $data['title'] = 'Unit Kerja Administrator';
        $data['edit'] = true;
        $ruser = GetLoggedUser();
        if($ruser[COL_ROLEID] != ROLEADMIN && $ruser[COL_ROLEID] != ROLEKADIS) {
          redirect('site/home');
        }
        if ($ruser[COL_ROLEID] != ROLEADMIN && $ruser[COL_ROLEID] != ROLEBAPPEDA) {
            $strOPD = explode('.', $ruser[COL_COMPANYID]);
            $this->db->where(TBL_AJBK_UNIT_BID.".".COL_KD_URUSAN, $strOPD[0]);
            $this->db->where(TBL_AJBK_UNIT_BID.".".COL_KD_BIDANG, $strOPD[1]);
            $this->db->where(TBL_AJBK_UNIT_BID.".".COL_KD_UNIT, $strOPD[2]);
            $this->db->where(TBL_AJBK_UNIT_BID.".".COL_KD_SUB, $strOPD[3]);
        }
        $rdata = $data['data'] = $this->db->where(COL_UNIQ, $id)->get(TBL_AJBK_UNIT_BID)->row_array();
        if (empty($rdata)) {
            show_404();
            return;
        }

        if (!empty($_POST)) {
          /* CHECK OPD */
          $ropd = $this->db->where(array(
            COL_KD_URUSAN=>$rdata[COL_KD_URUSAN],
            COL_KD_BIDANG=>$rdata[COL_KD_BIDANG],
            COL_KD_UNIT=>$rdata[COL_KD_UNIT],
            COL_KD_SUB=>$rdata[COL_KD_SUB]
          ))
          ->get(TBL_AJBK_UNIT)
          ->row_array();
          if(!$ropd) {
            ShowJsonError("OPD tidak valid!");
            exit();
          }
          if($ropd[COL_ISAKTIF]!=1) {
            ShowJsonError("Gagal mengubah data. OPD dalam keadaan SUSPEND / NONAKTIF!");
            exit();
          }
          /* --CHECK OPD-- */

            $resp = array();
            $resp['error'] = 0;
            $resp['success'] = 1;
            $resp['redirect'] = site_url('ajbk/master/bid-index');
            $data = array(
              COL_KD_URUSAN => $this->input->post(COL_KD_URUSAN),
              COL_KD_BIDANG => $this->input->post(COL_KD_BIDANG),
              COL_KD_UNIT => $this->input->post(COL_KD_UNIT),
              COL_KD_SUB => $this->input->post(COL_KD_SUB),
              COL_KD_BID => $this->input->post(COL_KD_BID),
              COL_NM_BID => $this->input->post(COL_NM_BID)
          );
            if (!$this->db->where(COL_UNIQ, $id)->update(TBL_AJBK_UNIT_BID, $data)) {
                $resp['error'] = 1;
                $resp['success'] = 0;
            }
            echo json_encode($resp);
        } else {
            $this->template->load('backend', 'ajbk/master/form_bid', $data);
        }
    }

    public function bid_del()
    {
        $ruser = GetLoggedUser();
        if($ruser[COL_ROLEID] != ROLEADMIN && $ruser[COL_ROLEID] != ROLEKADIS) {
          redirect('site/home');
        }
        $data = $this->input->post('cekbox');
        $deleted = 0;
        foreach ($data as $datum) {
          /* CHECK OPD */
          $rdata = $this->db->where(COL_UNIQ,$datum)->get(TBL_AJBK_UNIT_BID)->row_array();
          if(!$rdata) {
            ShowJsonError("Parameter tidak valid!");
            exit();
          }
          $ropd = $this->db->where(array(
            COL_KD_URUSAN=>$rdata[COL_KD_URUSAN],
            COL_KD_BIDANG=>$rdata[COL_KD_BIDANG],
            COL_KD_UNIT=>$rdata[COL_KD_UNIT],
            COL_KD_SUB=>$rdata[COL_KD_SUB]
          ))
          ->get(TBL_AJBK_UNIT)
          ->row_array();
          if(!$ropd) {
            ShowJsonError("OPD tidak valid!");
            exit();
          }
          if($ropd[COL_ISAKTIF]!=1) {
            ShowJsonError("Gagal menghapus data. OPD dalam keadaan SUSPEND / NONAKTIF!");
            exit();
          }
          /* --CHECK OPD-- */

            $this->db->delete(TBL_AJBK_UNIT_BID, array(COL_UNIQ => $datum));
            $deleted++;
        }
        if ($deleted) {
            ShowJsonSuccess($deleted." data dihapus");
        } else {
            ShowJsonError("Tidak ada dihapus");
        }
    }

    function subbid_index() {
        $ruser = GetLoggedUser();
        if($ruser[COL_ROLEID] != ROLEADMIN && $ruser[COL_ROLEID] != ROLEKADIS && $ruser[COL_ROLEID] != ROLEKABID) {
          redirect('site/home');
        }
        $strOPD = explode('.', $ruser[COL_COMPANYID]);

        $data['title'] = 'Unit Kerja Pengawas';
        $this->db->select('*,'.TBL_AJBK_UNIT_SUBBID.'.'.COL_UNIQ.' as ID');
        $this->db->join(TBL_AJBK_UNIT,
            TBL_AJBK_UNIT.'.'.COL_KD_URUSAN." = ".TBL_AJBK_UNIT_SUBBID.".".COL_KD_URUSAN." AND ".
            TBL_AJBK_UNIT.'.'.COL_KD_BIDANG." = ".TBL_AJBK_UNIT_SUBBID.".".COL_KD_BIDANG." AND ".
            TBL_AJBK_UNIT.'.'.COL_KD_UNIT." = ".TBL_AJBK_UNIT_SUBBID.".".COL_KD_UNIT." AND ".
            TBL_AJBK_UNIT.'.'.COL_KD_SUB." = ".TBL_AJBK_UNIT_SUBBID.".".COL_KD_SUB
            ,"inner");
        $this->db->join(TBL_AJBK_UNIT_BID,
            TBL_AJBK_UNIT_BID.'.'.COL_KD_URUSAN." = ".TBL_AJBK_UNIT_SUBBID.".".COL_KD_URUSAN." AND ".
            TBL_AJBK_UNIT_BID.'.'.COL_KD_BIDANG." = ".TBL_AJBK_UNIT_SUBBID.".".COL_KD_BIDANG." AND ".
            TBL_AJBK_UNIT_BID.'.'.COL_KD_UNIT." = ".TBL_AJBK_UNIT_SUBBID.".".COL_KD_UNIT." AND ".
            TBL_AJBK_UNIT_BID.'.'.COL_KD_SUB." = ".TBL_AJBK_UNIT_SUBBID.".".COL_KD_SUB." AND ".
            TBL_AJBK_UNIT_BID.'.'.COL_KD_BID." = ".TBL_AJBK_UNIT_SUBBID.".".COL_KD_BID
            ,"inner");
        if($ruser[COL_ROLEID] != ROLEADMIN) {
            $this->db->where(TBL_AJBK_UNIT_SUBBID.".".COL_KD_URUSAN, $strOPD[0]);
            $this->db->where(TBL_AJBK_UNIT_SUBBID.".".COL_KD_BIDANG, $strOPD[1]);
            $this->db->where(TBL_AJBK_UNIT_SUBBID.".".COL_KD_UNIT, $strOPD[2]);
            $this->db->where(TBL_AJBK_UNIT_SUBBID.".".COL_KD_SUB, $strOPD[3]);
        }
        if($ruser[COL_ROLEID] == ROLEKABID) {
            $this->db->where(TBL_AJBK_UNIT_SUBBID.".".COL_KD_URUSAN, $strOPD[0]);
            $this->db->where(TBL_AJBK_UNIT_SUBBID.".".COL_KD_BIDANG, $strOPD[1]);
            $this->db->where(TBL_AJBK_UNIT_SUBBID.".".COL_KD_UNIT, $strOPD[2]);
            $this->db->where(TBL_AJBK_UNIT_SUBBID.".".COL_KD_SUB, $strOPD[3]);
            $this->db->where(TBL_AJBK_UNIT_SUBBID.".".COL_KD_BID, $strOPD[4]);
        }
        $this->db->order_by(TBL_AJBK_UNIT_SUBBID.".".COL_KD_URUSAN, 'asc');
        $this->db->order_by(TBL_AJBK_UNIT_SUBBID.".".COL_KD_BIDANG, 'asc');
        $this->db->order_by(TBL_AJBK_UNIT_SUBBID.".".COL_KD_UNIT, 'asc');
        $this->db->order_by(TBL_AJBK_UNIT_SUBBID.".".COL_KD_SUB, 'asc');
        $this->db->order_by(TBL_AJBK_UNIT_SUBBID.".".COL_KD_BID, 'asc');
        $this->db->order_by(TBL_AJBK_UNIT_SUBBID.".".COL_KD_SUBBID, 'asc');
        $data['res'] = $this->db->get(TBL_AJBK_UNIT_SUBBID)->result_array();
        $this->template->load('backend', 'ajbk/master/index_subbid', $data);
    }

    function subbid_add() {
        $ruser = GetLoggedUser();
        if($ruser[COL_ROLEID] != ROLEADMIN && $ruser[COL_ROLEID] != ROLEKADIS && $ruser[COL_ROLEID] != ROLEKABID) {
          redirect('site/home');
        }

        $data['title'] = 'Unit Kerja Pengawas';
        $data['edit'] = FALSE;
        if(!empty($_POST)){
          /* CHECK OPD */
          $ropd = $this->db->where(array(
            COL_KD_URUSAN=>$this->input->post(COL_KD_URUSAN),
            COL_KD_BIDANG=>$this->input->post(COL_KD_BIDANG),
            COL_KD_UNIT=>$this->input->post(COL_KD_UNIT),
            COL_KD_SUB=>$this->input->post(COL_KD_SUB)
          ))
          ->get(TBL_AJBK_UNIT)
          ->row_array();
          if(!$ropd) {
            ShowJsonError("OPD tidak valid!");
            exit();
          }
          if($ropd[COL_ISAKTIF]!=1) {
            ShowJsonError("Gagal menginput data. OPD dalam keadaan SUSPEND / NONAKTIF!");
            exit();
          }
          /* --CHECK OPD-- */

            $resp = array();
            $resp['error'] = 0;
            $resp['success'] = 1;
            $resp['redirect'] = site_url('ajbk/master/subbid-index');
            $data = array(
                COL_KD_URUSAN => $this->input->post(COL_KD_URUSAN),
                COL_KD_BIDANG => $this->input->post(COL_KD_BIDANG),
                COL_KD_UNIT => $this->input->post(COL_KD_UNIT),
                COL_KD_SUB => $this->input->post(COL_KD_SUB),
                COL_KD_BID => $this->input->post(COL_KD_BID),
                COL_KD_SUBBID => $this->input->post(COL_KD_SUBBID),
                COL_NM_SUBBID => $this->input->post(COL_NM_SUBBID)
            );
            if(!$this->db->insert(TBL_AJBK_UNIT_SUBBID, $data)){
                $resp['error'] = 1;
                $resp['success'] = 0;
            }
            echo json_encode($resp);
        }else{
            $this->template->load('backend', 'ajbk/master/form_subbid', $data);
        }
    }

    function subbid_edit($id) {
        $ruser = GetLoggedUser();
        if($ruser[COL_ROLEID] != ROLEADMIN && $ruser[COL_ROLEID] != ROLEKADIS && $ruser[COL_ROLEID] != ROLEKABID) {
          redirect('site/home');
        }
        $strOPD = explode('.', $ruser[COL_COMPANYID]);

        $this->db->select(TBL_AJBK_UNIT_SUBBID.'.*,'.TBL_AJBK_UNIT_BID.'.'.COL_NM_BID);
        if($ruser[COL_ROLEID] != ROLEADMIN && $ruser[COL_ROLEID] != ROLEBAPPEDA) {
            $this->db->where(TBL_AJBK_UNIT_BID.".".COL_KD_URUSAN, $strOPD[0]);
            $this->db->where(TBL_AJBK_UNIT_BID.".".COL_KD_BIDANG, $strOPD[1]);
            $this->db->where(TBL_AJBK_UNIT_BID.".".COL_KD_UNIT, $strOPD[2]);
            $this->db->where(TBL_AJBK_UNIT_BID.".".COL_KD_SUB, $strOPD[3]);
        }

        $this->db->join(TBL_AJBK_UNIT_BID,
                TBL_AJBK_UNIT_BID.'.'.COL_KD_URUSAN." = ".TBL_AJBK_UNIT_SUBBID.".".COL_KD_URUSAN." AND ".
                TBL_AJBK_UNIT_BID.'.'.COL_KD_BIDANG." = ".TBL_AJBK_UNIT_SUBBID.".".COL_KD_BIDANG." AND ".
                TBL_AJBK_UNIT_BID.'.'.COL_KD_UNIT." = ".TBL_AJBK_UNIT_SUBBID.".".COL_KD_UNIT." AND ".
                TBL_AJBK_UNIT_BID.'.'.COL_KD_SUB." = ".TBL_AJBK_UNIT_SUBBID.".".COL_KD_SUB." AND ".
                TBL_AJBK_UNIT_BID.'.'.COL_KD_BID." = ".TBL_AJBK_UNIT_SUBBID.".".COL_KD_BID
                ,"inner")
            ->where(TBL_AJBK_UNIT_SUBBID.".".COL_UNIQ, $id);
        $rdata = $data['data'] = $this->db->get(TBL_AJBK_UNIT_SUBBID)->row_array();
        if(empty($rdata)){
            show_404();
            return;
        }

        $data['title'] = 'Unit Kerja Pengawas';
        $data['edit'] = TRUE;
        if(!empty($_POST)){
          /* CHECK OPD */
          $ropd = $this->db->where(array(
            COL_KD_URUSAN=>$rdata[COL_KD_URUSAN],
            COL_KD_BIDANG=>$rdata[COL_KD_BIDANG],
            COL_KD_UNIT=>$rdata[COL_KD_UNIT],
            COL_KD_SUB=>$rdata[COL_KD_SUB]
          ))
          ->get(TBL_AJBK_UNIT)
          ->row_array();
          if(!$ropd) {
            ShowJsonError("OPD tidak valid!");
            exit();
          }
          if($ropd[COL_ISAKTIF]!=1) {
            ShowJsonError("Gagal mengubah data. OPD dalam keadaan SUSPEND / NONAKTIF!");
            exit();
          }
          /* --CHECK OPD-- */

            $resp = array();
            $resp['error'] = 0;
            $resp['success'] = 1;
            $resp['redirect'] = site_url('ajbk/master/subbid-index');
            $data = array(
                COL_KD_URUSAN => $this->input->post(COL_KD_URUSAN),
                COL_KD_BIDANG => $this->input->post(COL_KD_BIDANG),
                COL_KD_UNIT => $this->input->post(COL_KD_UNIT),
                COL_KD_SUB => $this->input->post(COL_KD_SUB),
                COL_KD_BID => $this->input->post(COL_KD_BID),
                COL_KD_SUBBID => $this->input->post(COL_KD_SUBBID),
                COL_NM_SUBBID => $this->input->post(COL_NM_SUBBID),
                //COL_NM_KASUBBID => $this->input->post(COL_NM_KASUBBID)
            );
            if(!$this->db->where(COL_UNIQ, $id)->update(TBL_AJBK_UNIT_SUBBID, $data)){
                $resp['error'] = 1;
                $resp['success'] = 0;
            }
            echo json_encode($resp);
        }else{
            $this->template->load('backend', 'ajbk/master/form_subbid', $data);
        }
    }

    function subbid_del(){
        $data = $this->input->post('cekbox');
        $deleted = 0;
        foreach ($data as $datum) {
          /* CHECK OPD */
          $rdata = $this->db->where(COL_UNIQ,$datum)->get(TBL_AJBK_UNIT_SUBBID)->row_array();
          if(!$rdata) {
            ShowJsonError("Parameter tidak valid!");
            exit();
          }
          $ropd = $this->db->where(array(
            COL_KD_URUSAN=>$rdata[COL_KD_URUSAN],
            COL_KD_BIDANG=>$rdata[COL_KD_BIDANG],
            COL_KD_UNIT=>$rdata[COL_KD_UNIT],
            COL_KD_SUB=>$rdata[COL_KD_SUB]
          ))
          ->get(TBL_AJBK_UNIT)
          ->row_array();
          if(!$ropd) {
            ShowJsonError("OPD tidak valid!");
            exit();
          }
          if($ropd[COL_ISAKTIF]!=1) {
            ShowJsonError("Gagal menghapus data. OPD dalam keadaan SUSPEND / NONAKTIF!");
            exit();
          }
          /* --CHECK OPD-- */

            $this->db->delete(TBL_AJBK_UNIT_SUBBID, array(COL_UNIQ => $datum));
            $deleted++;
        }
        if($deleted){
            ShowJsonSuccess($deleted." data dihapus");
        }else{
            ShowJsonError("Tidak ada dihapus");
        }
    }
}
