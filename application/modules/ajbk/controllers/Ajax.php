<?php
class Ajax extends MY_Controller {
  function now() {
    echo date('D, d M Y H:i:s');
  }

  function get_opt_bidang() {
    $kdOPD = $this->input->post("KdOPD");
    $ropd = $this->db->where(COL_UNIQ, $kdOPD)->get(TBL_AJBK_UNIT)->row_array();
    if($ropd) {
      $kdUrusan = $ropd[COL_KD_URUSAN];
      $kdBidang = $ropd[COL_KD_BIDANG];
      $kdUnit = $ropd[COL_KD_UNIT];
      $kdSub = $ropd[COL_KD_SUB];
      echo GetCombobox("SELECT * FROM ajbk_unit_bid where Kd_Urusan = $kdUrusan and Kd_Bidang = $kdBidang and Kd_Unit = $kdUnit and Kd_Sub = $kdSub order by Kd_Bid asc", COL_UNIQ, COL_NM_BID , null, true, false, '-- Semua --');
    } else {
      echo "<option value=''>-- Semua --</option>";
    }
  }

  function get_opt_subbidang() {
    $kdBidang = $this->input->post("KdBid");
    $rbid = $this->db->where(COL_UNIQ, $kdBidang)->get(TBL_AJBK_UNIT_BID)->row_array();
    if($rbid) {
      $kdUrusan = $rbid[COL_KD_URUSAN];
      $kdBidang = $rbid[COL_KD_BIDANG];
      $kdUnit = $rbid[COL_KD_UNIT];
      $kdSub = $rbid[COL_KD_SUB];
      $kdBid = $rbid[COL_KD_BID];
      echo GetCombobox("SELECT * FROM ajbk_unit_subbid where Kd_Urusan = $kdUrusan and Kd_Bidang = $kdBidang and Kd_Unit = $kdUnit and Kd_Sub = $kdSub and Kd_Bid = $kdBid order by Kd_Subbid asc", COL_UNIQ, COL_NM_SUBBID , null, true, false, '-- Semua --');
    } else {
      echo "<option value=''>-- Semua --</option>";
    }
  }

  function get_nomenklatur_detail() {
    $kdNomenklatur = $this->input->post(COL_KD_NOMENKLATUR);
    $rnomenklatur = $this->db->where(COL_KD_NOMENKLATUR, $kdNomenklatur)->get(TBL_AJBK_NOMENKLATUR)->row_array();
    echo json_encode($rnomenklatur);
  }

  function browse_opd() {
      $eplandb = $this->load->database("eplan", true);
      $eplandb->select('*, CONCAT('.COL_KD_URUSAN.',\'|\','.COL_KD_BIDANG.',\'|\','.COL_KD_UNIT.',\'|\','.COL_KD_SUB.') AS ID, CONCAT('.COL_KD_URUSAN.',\'.\','.COL_KD_BIDANG.',\'.\','.COL_KD_UNIT.',\'.\','.COL_KD_SUB.',\' \',Nm_Sub_Unit) AS Text');
      $eplandb->order_by(COL_KD_URUSAN, 'asc');
      $eplandb->order_by(COL_KD_BIDANG, 'asc');
      $eplandb->order_by(COL_KD_UNIT, 'asc');
      $eplandb->order_by(COL_KD_SUB, 'asc');
      $data['res'] = $eplandb->get("ajbk_unit")->result_array();
      $this->load->view('ajax/browse', $data);
  }

  function browse_bid() {
      $kdUrusan = $this->input->get("Kd_Urusan");
      $kdBidang = $this->input->get("Kd_Bidang");
      $kdUnit = $this->input->get("Kd_Unit");
      $kdSub = $this->input->get("Kd_Sub");

      $this->db->select('*,'.TBL_AJBK_UNIT_BID.'.'.COL_KD_BID.' AS ID,
          CONCAT('.TBL_AJBK_UNIT_BID.'.'.COL_KD_BID.',\'. \','.TBL_AJBK_UNIT_BID.'.'.COL_NM_BID.') AS Text');
      $this->db->where(COL_KD_URUSAN, $kdUrusan);
      $this->db->where(COL_KD_BIDANG, $kdBidang);
      $this->db->where(COL_KD_UNIT, $kdUnit);
      $this->db->where(COL_KD_SUB, $kdSub);
      $data['res'] = $this->db->get(TBL_AJBK_UNIT_BID)->result_array();
      $this->load->view('ajax/browse', $data);
  }

  function browse_subbid() {
      $kdUrusan = $this->input->get("Kd_Urusan");
      $kdBidang = $this->input->get("Kd_Bidang");
      $kdUnit = $this->input->get("Kd_Unit");
      $kdSub = $this->input->get("Kd_Sub");
      $kdBid = $this->input->get("Kd_Bid");

      $this->db->select('*,'.TBL_AJBK_UNIT_SUBBID.'.'.COL_KD_SUBBID.' AS ID,
          CONCAT('.TBL_AJBK_UNIT_SUBBID.'.'.COL_KD_BID.',\'.\','.TBL_AJBK_UNIT_SUBBID.".".COL_KD_SUBBID.',\'. \','.TBL_AJBK_UNIT_SUBBID.'.'.COL_NM_SUBBID.') AS Text');
      $this->db->where(COL_KD_URUSAN, $kdUrusan);
      $this->db->where(COL_KD_BIDANG, $kdBidang);
      $this->db->where(COL_KD_UNIT, $kdUnit);
      $this->db->where(COL_KD_SUB, $kdSub);
      $this->db->where(COL_KD_BID, $kdBid);
      $data['res'] = $this->db->get(TBL_AJBK_UNIT_SUBBID)->result_array();
      $this->load->view('ajax/browse', $data);
  }
}
