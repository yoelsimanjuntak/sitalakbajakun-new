<?php
/**
 * Created by PhpStorm.
 * User: Yoel Simanjuntak
 * Date: 30/09/2018
 * Time: 17:04
 */
class Mbid extends MY_Controller {
    function __construct() {
        parent::__construct();
        if(!IsLogin() || (GetLoggedUser()[COL_ROLEID] != ROLEADMIN && GetLoggedUser()[COL_ROLEID] != ROLEBAPPEDA && GetLoggedUser()[COL_ROLEID] != ROLEKADIS && GetLoggedUser()[COL_ROLEID] != ROLEKABID)) {
            redirect('sakip/user/dashboard');
        }
    }

    function program() {
        $ruser = GetLoggedUser();
        $data['title'] = 'Program Bidang OPD';
        $this->db->select('*,'.TBL_SAKIP_MBID_PROGRAM.'.'.COL_UNIQ.' as ID,'.TBL_SAKIP_MBID_PROGRAM.'.'.COL_KD_TAHUN.' as Kd_Tahun');
        $this->db->join(TBL_SAKIP_MPEMDA,TBL_SAKIP_MPEMDA.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_PEMDA,"inner");
        $this->db->join(TBL_SAKIP_MPMD_MISI,TBL_SAKIP_MPMD_MISI.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_PEMDA." AND ".TBL_SAKIP_MPMD_MISI.'.'.COL_KD_MISI." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_MISI,"inner");
        $this->db->join(TBL_SAKIP_MPMD_TUJUAN,TBL_SAKIP_MPMD_TUJUAN.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_PEMDA." AND ".TBL_SAKIP_MPMD_TUJUAN.'.'.COL_KD_MISI." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_MISI." AND ".TBL_SAKIP_MPMD_TUJUAN.'.'.COL_KD_TUJUAN." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_TUJUAN,"inner");
        $this->db->join(TBL_SAKIP_MPMD_IKTUJUAN,TBL_SAKIP_MPMD_IKTUJUAN.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_PEMDA." AND ".TBL_SAKIP_MPMD_IKTUJUAN.'.'.COL_KD_MISI." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_MISI." AND ".TBL_SAKIP_MPMD_IKTUJUAN.'.'.COL_KD_TUJUAN." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_TUJUAN." AND ".TBL_SAKIP_MPMD_IKTUJUAN.'.'.COL_KD_INDIKATORTUJUAN." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_INDIKATORTUJUAN,"inner");
        $this->db->join(TBL_SAKIP_MPMD_SASARAN,TBL_SAKIP_MPMD_SASARAN.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_PEMDA." AND ".TBL_SAKIP_MPMD_SASARAN.'.'.COL_KD_MISI." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_MISI." AND ".TBL_SAKIP_MPMD_SASARAN.'.'.COL_KD_TUJUAN." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_TUJUAN." AND ".TBL_SAKIP_MPMD_SASARAN.'.'.COL_KD_INDIKATORTUJUAN." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_INDIKATORTUJUAN." AND ".TBL_SAKIP_MPMD_SASARAN.'.'.COL_KD_SASARAN." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_SASARAN,"inner");
        $this->db->join(TBL_SAKIP_MPMD_IKSASARAN,TBL_SAKIP_MPMD_IKSASARAN.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_PEMDA." AND ".TBL_SAKIP_MPMD_IKSASARAN.'.'.COL_KD_MISI." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_MISI." AND ".TBL_SAKIP_MPMD_IKSASARAN.'.'.COL_KD_TUJUAN." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_TUJUAN." AND ".TBL_SAKIP_MPMD_IKSASARAN.'.'.COL_KD_INDIKATORTUJUAN." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_INDIKATORTUJUAN." AND ".TBL_SAKIP_MPMD_IKSASARAN.'.'.COL_KD_SASARAN." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_SASARAN." AND ".TBL_SAKIP_MPMD_IKSASARAN.'.'.COL_KD_INDIKATORSASARAN." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_INDIKATORSASARAN,"inner");
        $this->db->join(TBL_SAKIP_MOPD_TUJUAN,
            TBL_SAKIP_MOPD_TUJUAN.'.'.COL_KD_URUSAN." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_URUSAN." AND ".
            TBL_SAKIP_MOPD_TUJUAN.'.'.COL_KD_BIDANG." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_BIDANG." AND ".
            TBL_SAKIP_MOPD_TUJUAN.'.'.COL_KD_UNIT." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_UNIT." AND ".
            TBL_SAKIP_MOPD_TUJUAN.'.'.COL_KD_SUB." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_SUB." AND ".

            TBL_SAKIP_MOPD_TUJUAN.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_PEMDA." AND ".
            TBL_SAKIP_MOPD_TUJUAN.'.'.COL_KD_MISI." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_MISI." AND ".
            TBL_SAKIP_MOPD_TUJUAN.'.'.COL_KD_TUJUAN." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_TUJUAN." AND ".
            TBL_SAKIP_MOPD_TUJUAN.'.'.COL_KD_INDIKATORTUJUAN." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_INDIKATORTUJUAN." AND ".
            TBL_SAKIP_MOPD_TUJUAN.'.'.COL_KD_SASARAN." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_SASARAN." AND ".
            TBL_SAKIP_MOPD_TUJUAN.'.'.COL_KD_INDIKATORSASARAN." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_INDIKATORSASARAN. " AND ".
            TBL_SAKIP_MOPD_TUJUAN.'.'.COL_KD_TUJUANOPD." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_TUJUANOPD
            ,"inner");
        $this->db->join(TBL_SAKIP_MOPD_IKTUJUAN,
            TBL_SAKIP_MOPD_IKTUJUAN.'.'.COL_KD_URUSAN." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_URUSAN." AND ".
            TBL_SAKIP_MOPD_IKTUJUAN.'.'.COL_KD_BIDANG." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_BIDANG." AND ".
            TBL_SAKIP_MOPD_IKTUJUAN.'.'.COL_KD_UNIT." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_UNIT." AND ".
            TBL_SAKIP_MOPD_IKTUJUAN.'.'.COL_KD_SUB." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_SUB." AND ".

            TBL_SAKIP_MOPD_IKTUJUAN.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_PEMDA." AND ".
            TBL_SAKIP_MOPD_IKTUJUAN.'.'.COL_KD_MISI." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_MISI." AND ".
            TBL_SAKIP_MOPD_IKTUJUAN.'.'.COL_KD_TUJUAN." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_TUJUAN." AND ".
            TBL_SAKIP_MOPD_IKTUJUAN.'.'.COL_KD_INDIKATORTUJUAN." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_INDIKATORTUJUAN." AND ".
            TBL_SAKIP_MOPD_IKTUJUAN.'.'.COL_KD_SASARAN." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_SASARAN." AND ".
            TBL_SAKIP_MOPD_IKTUJUAN.'.'.COL_KD_INDIKATORSASARAN." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_INDIKATORSASARAN." AND ".
            TBL_SAKIP_MOPD_IKTUJUAN.'.'.COL_KD_TUJUANOPD." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_TUJUANOPD." AND ".
            TBL_SAKIP_MOPD_IKTUJUAN.'.'.COL_KD_INDIKATORTUJUANOPD." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_INDIKATORTUJUANOPD
            ,"inner");
        $this->db->join(TBL_SAKIP_MOPD_SASARAN,
            TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_URUSAN." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_URUSAN." AND ".
            TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_BIDANG." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_BIDANG." AND ".
            TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_UNIT." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_UNIT." AND ".
            TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_SUB." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_SUB." AND ".

            TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_PEMDA." AND ".
            TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_MISI." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_MISI." AND ".
            TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_TUJUAN." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_TUJUAN." AND ".
            TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_INDIKATORTUJUAN." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_INDIKATORTUJUAN." AND ".
            TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_SASARAN." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_SASARAN." AND ".
            TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_INDIKATORSASARAN." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_INDIKATORSASARAN." AND ".
            TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_TUJUANOPD." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_TUJUANOPD." AND ".
            TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_INDIKATORTUJUANOPD." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_INDIKATORTUJUANOPD." AND ".
            TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_SASARANOPD." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_SASARANOPD
            ,"inner");
        $this->db->join(TBL_SAKIP_MOPD_IKSASARAN,
            TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_URUSAN." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_URUSAN." AND ".
            TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_BIDANG." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_BIDANG." AND ".
            TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_UNIT." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_UNIT." AND ".
            TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_SUB." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_SUB." AND ".

            TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_PEMDA." AND ".
            TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_MISI." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_MISI." AND ".
            TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_TUJUAN." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_TUJUAN." AND ".
            TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_INDIKATORTUJUAN." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_INDIKATORTUJUAN." AND ".
            TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_SASARAN." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_SASARAN." AND ".
            TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_INDIKATORSASARAN." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_INDIKATORSASARAN." AND ".
            TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_TUJUANOPD." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_TUJUANOPD." AND ".
            TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_INDIKATORTUJUANOPD." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_INDIKATORTUJUANOPD." AND ".
            TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_SASARANOPD." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_SASARANOPD." AND ".
            TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_INDIKATORSASARANOPD." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_INDIKATORSASARANOPD
            ,"inner");
        $this->db->join(TBL_SAKIP_MBID,
            TBL_SAKIP_MBID.'.'.COL_KD_URUSAN." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_URUSAN." AND ".
            TBL_SAKIP_MBID.'.'.COL_KD_BIDANG." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_BIDANG." AND ".
            TBL_SAKIP_MBID.'.'.COL_KD_UNIT." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_UNIT." AND ".
            TBL_SAKIP_MBID.'.'.COL_KD_SUB." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_SUB." AND ".
            TBL_SAKIP_MBID.'.'.COL_KD_BID." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_BID
            ,"inner");
        $this->db->where(TBL_SAKIP_MPEMDA.".".COL_KD_TAHUN_FROM." <=", date("Y"));
        $this->db->where(TBL_SAKIP_MPEMDA.".".COL_KD_TAHUN_TO." >=", date("Y"));
        if($ruser[COL_ROLEID] != ROLEADMIN && $ruser[COL_ROLEID] != ROLEBAPPEDA) {
            $strOPD = explode('.', $ruser[COL_COMPANYID]);
            $this->db->where(TBL_SAKIP_MBID_PROGRAM.".".COL_KD_URUSAN, $strOPD[0]);
            $this->db->where(TBL_SAKIP_MBID_PROGRAM.".".COL_KD_BIDANG, $strOPD[1]);
            $this->db->where(TBL_SAKIP_MBID_PROGRAM.".".COL_KD_UNIT, $strOPD[2]);
            $this->db->where(TBL_SAKIP_MBID_PROGRAM.".".COL_KD_SUB, $strOPD[3]);
            if($ruser[COL_ROLEID] == ROLEKABID) $this->db->where(TBL_SAKIP_MBID_PROGRAM.".".COL_KD_BID, $strOPD[4]);
        }
        $this->db->order_by(TBL_SAKIP_MBID_PROGRAM.".".COL_KD_TAHUN, 'desc');
        $this->db->order_by(TBL_SAKIP_MBID_PROGRAM.".".COL_KD_URUSAN, 'asc');
        $this->db->order_by(TBL_SAKIP_MBID_PROGRAM.".".COL_KD_BIDANG, 'asc');
        $this->db->order_by(TBL_SAKIP_MBID_PROGRAM.".".COL_KD_UNIT, 'asc');
        $this->db->order_by(TBL_SAKIP_MBID_PROGRAM.".".COL_KD_SUB, 'asc');
        $this->db->order_by(TBL_SAKIP_MBID_PROGRAM.".".COL_KD_BID, 'asc');
        $this->db->order_by(TBL_SAKIP_MPEMDA.".".COL_KD_TAHUN_FROM, 'desc');
        $this->db->order_by(TBL_SAKIP_MBID_PROGRAM.".".COL_KD_MISI, 'asc');
        $this->db->order_by(TBL_SAKIP_MBID_PROGRAM.".".COL_KD_TUJUAN, 'asc');
        $this->db->order_by(TBL_SAKIP_MBID_PROGRAM.".".COL_KD_INDIKATORTUJUAN, 'asc');
        $this->db->order_by(TBL_SAKIP_MBID_PROGRAM.".".COL_KD_SASARAN, 'asc');
        $this->db->order_by(TBL_SAKIP_MBID_PROGRAM.".".COL_KD_INDIKATORSASARAN, 'asc');
        $this->db->order_by(TBL_SAKIP_MBID_PROGRAM.".".COL_KD_TUJUANOPD, 'asc');
        $this->db->order_by(TBL_SAKIP_MBID_PROGRAM.".".COL_KD_INDIKATORTUJUANOPD, 'asc');
        $this->db->order_by(TBL_SAKIP_MBID_PROGRAM.".".COL_KD_SASARANOPD, 'asc');
        $this->db->order_by(TBL_SAKIP_MBID_PROGRAM.".".COL_KD_INDIKATORSASARANOPD, 'asc');
        $this->db->order_by(TBL_SAKIP_MBID_PROGRAM.".".COL_KD_PROGRAMOPD, 'asc');
        $data['res'] = $this->db->get(TBL_SAKIP_MBID_PROGRAM)->result_array();
        $this->load->view('mbid/program', $data);
    }

    function program_add() {
        $data['title'] = "Program Bidang OPD";
        $data['edit'] = FALSE;
        $ruser = GetLoggedUser();

        if(!empty($_POST)){
            $ruser = GetLoggedUser();
            $resp = array();
            $resp['error'] = 0;
            $resp['success'] = 1;
            $resp['redirect'] = site_url('mbid/program');
            $isbulk = $this->input->post("IsBulk");

            $this->db->trans_begin();
            try {
                if($isbulk) {
                    $rpemda = $this->db->where(COL_KD_PEMDA, $this->input->post(COL_KD_PEMDA))->get(TBL_SAKIP_MPEMDA)->row_array();
                    if (!$rpemda) {
                        throw new Exception("Periode Salah!");
                    }

                    $data = [];
                    $dataTarget = $this->input->post(COL_TARGET);
                    $dataTarget_N1 = $this->input->post(COL_TARGET_N1);

                    for ($i = $rpemda[COL_KD_TAHUN_FROM] + 1; $i <= $rpemda[COL_KD_TAHUN_TO]; $i++) {
                        $arr_check = array(
                            COL_KD_TAHUN => $i, //$this->input->post(COL_KD_TAHUN),
                            COL_KD_URUSAN => $this->input->post(COL_KD_URUSAN),
                            COL_KD_BIDANG => $this->input->post(COL_KD_BIDANG),
                            COL_KD_UNIT => $this->input->post(COL_KD_UNIT),
                            COL_KD_SUB => $this->input->post(COL_KD_SUB),
                            COL_KD_BID => $this->input->post(COL_KD_BID),

                            COL_KD_PEMDA => $this->input->post(COL_KD_PEMDA),
                            COL_KD_MISI => $this->input->post(COL_KD_MISI),
                            COL_KD_TUJUAN => $this->input->post(COL_KD_TUJUAN),
                            COL_KD_INDIKATORTUJUAN => $this->input->post(COL_KD_INDIKATORTUJUAN),
                            COL_KD_SASARAN => $this->input->post(COL_KD_SASARAN),
                            COL_KD_INDIKATORSASARAN => $this->input->post(COL_KD_INDIKATORSASARAN),
                            COL_KD_TUJUANOPD => $this->input->post(COL_KD_TUJUANOPD),
                            COL_KD_INDIKATORTUJUANOPD => $this->input->post(COL_KD_INDIKATORTUJUANOPD),
                            COL_KD_SASARANOPD => $this->input->post(COL_KD_SASARANOPD),
                            COL_KD_INDIKATORSASARANOPD => $this->input->post(COL_KD_INDIKATORSASARANOPD),
                            COL_KD_PROGRAMOPD => $this->input->post(COL_KD_PROGRAMOPD)
                        );
                        $rprogram_existing = $this->db->where($arr_check)->get(TBL_SAKIP_MBID_PROGRAM)->row_array();
                        if ($rprogram_existing) {
                            continue;
                        }
                        $data[] = array(
                            COL_KD_TAHUN => $i, //$this->input->post(COL_KD_TAHUN),
                            COL_KD_URUSAN => $this->input->post(COL_KD_URUSAN),
                            COL_KD_BIDANG => $this->input->post(COL_KD_BIDANG),
                            COL_KD_UNIT => $this->input->post(COL_KD_UNIT),
                            COL_KD_SUB => $this->input->post(COL_KD_SUB),
                            COL_KD_BID => $this->input->post(COL_KD_BID),

                            COL_KD_PEMDA => $this->input->post(COL_KD_PEMDA),
                            COL_KD_MISI => $this->input->post(COL_KD_MISI),
                            COL_KD_TUJUAN => $this->input->post(COL_KD_TUJUAN),
                            COL_KD_INDIKATORTUJUAN => $this->input->post(COL_KD_INDIKATORTUJUAN),
                            COL_KD_SASARAN => $this->input->post(COL_KD_SASARAN),
                            COL_KD_INDIKATORSASARAN => $this->input->post(COL_KD_INDIKATORSASARAN),
                            COL_KD_TUJUANOPD => $this->input->post(COL_KD_TUJUANOPD),
                            COL_KD_INDIKATORTUJUANOPD => $this->input->post(COL_KD_INDIKATORTUJUANOPD),
                            COL_KD_SASARANOPD => $this->input->post(COL_KD_SASARANOPD),
                            COL_KD_INDIKATORSASARANOPD => $this->input->post(COL_KD_INDIKATORSASARANOPD),
                            COL_KD_PROGRAMOPD => $this->input->post(COL_KD_PROGRAMOPD),
                            COL_NM_PROGRAMOPD => $this->input->post(COL_NM_PROGRAMOPD),
                            //COL_NM_SASARANPROGRAM => $this->input->post(COL_NM_SASARANPROGRAM),
                            COL_ISEPLAN => !empty($this->input->post(COL_ISEPLAN)) ? 1 : 0,
                            //COL_KD_SATUAN => $this->input->post(COL_KD_SATUAN),
                            //COL_NM_INDIKATORPROGRAM => $this->input->post(COL_NM_INDIKATORPROGRAM),
                            //COL_AWAL => $this->input->post(COL_AWAL),
                            COL_REMARKS => $this->input->post(COL_REMARKS),
                            //COL_TARGET => $dataTarget[$i],
                            //COL_TARGET_N1 => $dataTarget_N1[$i],

                            COL_CREATE_BY => $ruser[COL_USERNAME],
                            COL_CREATE_DATE => date('Y-m-d H:i:s')
                        );
                    }

                    if (!$this->db->insert_batch(TBL_SAKIP_MBID_PROGRAM, $data)) {
                        throw new Exception("Database error: ".$this->db->error());
                    }
                } else {
                    $data = array(
                        COL_KD_TAHUN => $this->input->post(COL_KD_TAHUN),
                        COL_KD_URUSAN => $this->input->post(COL_KD_URUSAN),
                        COL_KD_BIDANG => $this->input->post(COL_KD_BIDANG),
                        COL_KD_UNIT => $this->input->post(COL_KD_UNIT),
                        COL_KD_SUB => $this->input->post(COL_KD_SUB),
                        COL_KD_BID => $this->input->post(COL_KD_BID),

                        COL_KD_PEMDA => $this->input->post(COL_KD_PEMDA),
                        COL_KD_MISI => $this->input->post(COL_KD_MISI),
                        COL_KD_TUJUAN => $this->input->post(COL_KD_TUJUAN),
                        COL_KD_INDIKATORTUJUAN => $this->input->post(COL_KD_INDIKATORTUJUAN),
                        COL_KD_SASARAN => $this->input->post(COL_KD_SASARAN),
                        COL_KD_INDIKATORSASARAN => $this->input->post(COL_KD_INDIKATORSASARAN),
                        COL_KD_TUJUANOPD => $this->input->post(COL_KD_TUJUANOPD),
                        COL_KD_INDIKATORTUJUANOPD => $this->input->post(COL_KD_INDIKATORTUJUANOPD),
                        COL_KD_SASARANOPD => $this->input->post(COL_KD_SASARANOPD),
                        COL_KD_INDIKATORSASARANOPD => $this->input->post(COL_KD_INDIKATORSASARANOPD),
                        COL_KD_PROGRAMOPD => $this->input->post(COL_KD_PROGRAMOPD),
                        COL_NM_PROGRAMOPD => $this->input->post(COL_NM_PROGRAMOPD),
                        //COL_NM_SASARANPROGRAM => $this->input->post(COL_NM_SASARANPROGRAM),
                        COL_ISEPLAN => !empty($this->input->post(COL_ISEPLAN))?1:0,
                        //COL_KD_SATUAN => $this->input->post(COL_KD_SATUAN),
                        //COL_NM_INDIKATORPROGRAM => $this->input->post(COL_NM_INDIKATORPROGRAM),
                        //COL_AWAL => $this->input->post(COL_AWAL),
                        COL_REMARKS => $this->input->post(COL_REMARKS),
                        //COL_TARGET => $this->input->post(COL_TARGET),
                        //COL_TARGET_N1 => $this->input->post(COL_TARGET_N1),

                        COL_CREATE_BY => $ruser[COL_USERNAME],
                        COL_CREATE_DATE => date('Y-m-d H:i:s')
                    );
                    if(!$this->db->insert(TBL_SAKIP_MBID_PROGRAM, $data)){
                        throw new Exception("Database error: ".$this->db->error());
                    }

                    $detNo = $this->input->post("NoDet");
                    $detDesc = $this->input->post("KetDet");
                    $det = [];
                    for($i = 0; $i<count($detNo); $i++) {
                        $det[] = array(
                            COL_KD_TAHUN => $this->input->post(COL_KD_TAHUN),
                            COL_KD_URUSAN => $this->input->post(COL_KD_URUSAN),
                            COL_KD_BIDANG => $this->input->post(COL_KD_BIDANG),
                            COL_KD_UNIT => $this->input->post(COL_KD_UNIT),
                            COL_KD_SUB => $this->input->post(COL_KD_SUB),
                            COL_KD_BID => $this->input->post(COL_KD_BID),

                            COL_KD_PEMDA => $this->input->post(COL_KD_PEMDA),
                            COL_KD_MISI => $this->input->post(COL_KD_MISI),
                            COL_KD_TUJUAN => $this->input->post(COL_KD_TUJUAN),
                            COL_KD_INDIKATORTUJUAN => $this->input->post(COL_KD_INDIKATORTUJUAN),
                            COL_KD_SASARAN => $this->input->post(COL_KD_SASARAN),
                            COL_KD_INDIKATORSASARAN => $this->input->post(COL_KD_INDIKATORSASARAN),
                            COL_KD_TUJUANOPD => $this->input->post(COL_KD_TUJUANOPD),
                            COL_KD_INDIKATORTUJUANOPD => $this->input->post(COL_KD_INDIKATORTUJUANOPD),
                            COL_KD_SASARANOPD => $this->input->post(COL_KD_SASARANOPD),
                            COL_KD_INDIKATORSASARANOPD => $this->input->post(COL_KD_INDIKATORSASARANOPD),
                            COL_KD_PROGRAMOPD => $this->input->post(COL_KD_PROGRAMOPD),
                            COL_KD_INDIKATORPROGRAMOPD => $detNo[$i],
                            COL_NM_INDIKATORPROGRAMOPD => $detDesc[$i]
                        );
                    }
                    if(count($det) > 0) {
                        $res = $this->db->insert_batch(TBL_SAKIP_MBID_PROGRAM_INDIKATOR, $det);
                        if(!$res) {
                            throw new Exception("Database error: ".$this->db->error());
                        }
                    }

                    $this->db->trans_commit();
                    echo json_encode($resp);
                }
            } catch (Exception $e) {
                $this->db->trans_rollback();
                $resp['error'] = $e->getMessage();
                $resp['success'] = 0;
                echo json_encode($resp);
                return;
            }
        }else{
            $this->db->where(COL_KD_TAHUN_FROM." <=", date("Y"));
            $this->db->where(COL_KD_TAHUN_TO." >=", date("Y"));
            $this->db->order_by(COL_KD_TAHUN_FROM, 'desc');
            $rpemda = $this->db->get(TBL_SAKIP_MPEMDA)->row_array();

            if(!empty($rpemda)) {
                $data['data'] = array(
                    COL_KD_PEMDA => $rpemda[COL_KD_PEMDA],
                    "DefPeriod" => $rpemda[COL_KD_TAHUN_FROM]." s.d ".$rpemda[COL_KD_TAHUN_TO]." : ".$rpemda[COL_NM_PEJABAT]
                );
            }
            $this->load->view('mbid/program_form',$data);
        }
    }

    function program_edit($id) {
        $ruser = GetLoggedUser();
        $this->db
            ->select(TBL_SAKIP_MBID_PROGRAM.'.*,'.
                TBL_SAKIP_MPMD_MISI.'.'.COL_NM_MISI.','.
                TBL_SAKIP_MPEMDA.'.'.COL_KD_TAHUN_FROM.','.
                TBL_SAKIP_MPEMDA.'.'.COL_KD_TAHUN_TO.','.
                TBL_SAKIP_MPEMDA.'.'.COL_NM_PEJABAT.', '.
                TBL_SAKIP_MPMD_TUJUAN.'.'.COL_NM_TUJUAN.', '.
                TBL_SAKIP_MPMD_IKTUJUAN.'.'.COL_NM_INDIKATORTUJUAN.', '.
                TBL_SAKIP_MPMD_SASARAN.'.'.COL_NM_SASARAN.', '.
                TBL_SAKIP_MPMD_IKSASARAN.'.'.COL_NM_INDIKATORSASARAN.', '.
                TBL_SAKIP_MOPD_TUJUAN.'.'.COL_NM_TUJUANOPD.', '.
                TBL_SAKIP_MOPD_IKTUJUAN.'.'.COL_NM_INDIKATORTUJUANOPD.', '.
                TBL_SAKIP_MOPD_SASARAN.'.'.COL_NM_SASARANOPD.', '.
                TBL_SAKIP_MBID.'.'.COL_NM_BID.','.
                TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_NM_INDIKATORSASARANOPD
            )
            ->join(TBL_SAKIP_MBID,
                TBL_SAKIP_MBID.'.'.COL_KD_URUSAN." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_URUSAN." AND ".
                TBL_SAKIP_MBID.'.'.COL_KD_BIDANG." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_BIDANG." AND ".
                TBL_SAKIP_MBID.'.'.COL_KD_UNIT." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_UNIT." AND ".
                TBL_SAKIP_MBID.'.'.COL_KD_SUB." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_SUB." AND ".
                TBL_SAKIP_MBID.'.'.COL_KD_BID." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_BID
                ,"inner")
            ->join(TBL_SAKIP_MPEMDA,TBL_SAKIP_MPEMDA.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_PEMDA,"inner")
            ->join(TBL_SAKIP_MPMD_MISI,TBL_SAKIP_MPMD_MISI.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_PEMDA." AND ".TBL_SAKIP_MPMD_MISI.'.'.COL_KD_MISI." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_MISI,"inner")
            ->join(TBL_SAKIP_MPMD_TUJUAN,TBL_SAKIP_MPMD_TUJUAN.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_PEMDA." AND ".TBL_SAKIP_MPMD_TUJUAN.'.'.COL_KD_MISI." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_MISI." AND ".TBL_SAKIP_MPMD_TUJUAN.'.'.COL_KD_TUJUAN." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_TUJUAN,"inner")
            ->join(TBL_SAKIP_MPMD_IKTUJUAN,TBL_SAKIP_MPMD_IKTUJUAN.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_PEMDA." AND ".TBL_SAKIP_MPMD_IKTUJUAN.'.'.COL_KD_MISI." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_MISI." AND ".TBL_SAKIP_MPMD_IKTUJUAN.'.'.COL_KD_TUJUAN." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_TUJUAN." AND ".TBL_SAKIP_MPMD_IKTUJUAN.'.'.COL_KD_INDIKATORTUJUAN." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_INDIKATORTUJUAN,"inner")
            ->join(TBL_SAKIP_MPMD_SASARAN,TBL_SAKIP_MPMD_SASARAN.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_PEMDA." AND ".TBL_SAKIP_MPMD_SASARAN.'.'.COL_KD_MISI." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_MISI." AND ".TBL_SAKIP_MPMD_SASARAN.'.'.COL_KD_TUJUAN." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_TUJUAN." AND ".TBL_SAKIP_MPMD_SASARAN.'.'.COL_KD_INDIKATORTUJUAN." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_INDIKATORTUJUAN." AND ".TBL_SAKIP_MPMD_SASARAN.'.'.COL_KD_SASARAN." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_SASARAN,"inner")
            ->join(TBL_SAKIP_MPMD_IKSASARAN,TBL_SAKIP_MPMD_IKSASARAN.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_PEMDA." AND ".TBL_SAKIP_MPMD_IKSASARAN.'.'.COL_KD_MISI." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_MISI." AND ".TBL_SAKIP_MPMD_IKSASARAN.'.'.COL_KD_TUJUAN." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_TUJUAN." AND ".TBL_SAKIP_MPMD_IKSASARAN.'.'.COL_KD_INDIKATORTUJUAN." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_INDIKATORTUJUAN." AND ".TBL_SAKIP_MPMD_IKSASARAN.'.'.COL_KD_SASARAN." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_SASARAN." AND ".TBL_SAKIP_MPMD_IKSASARAN.'.'.COL_KD_INDIKATORSASARAN." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_INDIKATORSASARAN,"inner")
            ->join(TBL_SAKIP_MOPD_TUJUAN,
                TBL_SAKIP_MOPD_TUJUAN.'.'.COL_KD_URUSAN." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_URUSAN." AND ".
                TBL_SAKIP_MOPD_TUJUAN.'.'.COL_KD_BIDANG." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_BIDANG." AND ".
                TBL_SAKIP_MOPD_TUJUAN.'.'.COL_KD_UNIT." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_UNIT." AND ".
                TBL_SAKIP_MOPD_TUJUAN.'.'.COL_KD_SUB." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_SUB." AND ".

                TBL_SAKIP_MOPD_TUJUAN.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_PEMDA." AND ".
                TBL_SAKIP_MOPD_TUJUAN.'.'.COL_KD_MISI." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_MISI." AND ".
                TBL_SAKIP_MOPD_TUJUAN.'.'.COL_KD_TUJUAN." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_TUJUAN." AND ".
                TBL_SAKIP_MOPD_TUJUAN.'.'.COL_KD_INDIKATORTUJUAN." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_INDIKATORTUJUAN." AND ".
                TBL_SAKIP_MOPD_TUJUAN.'.'.COL_KD_SASARAN." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_SASARAN." AND ".
                TBL_SAKIP_MOPD_TUJUAN.'.'.COL_KD_INDIKATORSASARAN." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_INDIKATORSASARAN." AND ".
                TBL_SAKIP_MOPD_TUJUAN.'.'.COL_KD_TUJUANOPD." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_TUJUANOPD
                ,"inner")
            ->join(TBL_SAKIP_MOPD_IKTUJUAN,
                TBL_SAKIP_MOPD_IKTUJUAN.'.'.COL_KD_URUSAN." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_URUSAN." AND ".
                TBL_SAKIP_MOPD_IKTUJUAN.'.'.COL_KD_BIDANG." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_BIDANG." AND ".
                TBL_SAKIP_MOPD_IKTUJUAN.'.'.COL_KD_UNIT." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_UNIT." AND ".
                TBL_SAKIP_MOPD_IKTUJUAN.'.'.COL_KD_SUB." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_SUB." AND ".

                TBL_SAKIP_MOPD_IKTUJUAN.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_PEMDA." AND ".
                TBL_SAKIP_MOPD_IKTUJUAN.'.'.COL_KD_MISI." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_MISI." AND ".
                TBL_SAKIP_MOPD_IKTUJUAN.'.'.COL_KD_TUJUAN." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_TUJUAN." AND ".
                TBL_SAKIP_MOPD_IKTUJUAN.'.'.COL_KD_INDIKATORTUJUAN." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_INDIKATORTUJUAN." AND ".
                TBL_SAKIP_MOPD_IKTUJUAN.'.'.COL_KD_SASARAN." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_SASARAN." AND ".
                TBL_SAKIP_MOPD_IKTUJUAN.'.'.COL_KD_INDIKATORSASARAN." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_INDIKATORSASARAN." AND ".
                TBL_SAKIP_MOPD_IKTUJUAN.'.'.COL_KD_TUJUANOPD." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_TUJUANOPD." AND ".
                TBL_SAKIP_MOPD_IKTUJUAN.'.'.COL_KD_INDIKATORTUJUANOPD." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_INDIKATORTUJUANOPD
                ,"inner")
            ->join(TBL_SAKIP_MOPD_SASARAN,
                TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_URUSAN." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_URUSAN." AND ".
                TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_BIDANG." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_BIDANG." AND ".
                TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_UNIT." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_UNIT." AND ".
                TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_SUB." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_SUB." AND ".

                TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_PEMDA." AND ".
                TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_MISI." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_MISI." AND ".
                TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_TUJUAN." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_TUJUAN." AND ".
                TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_INDIKATORTUJUAN." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_INDIKATORTUJUAN." AND ".
                TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_SASARAN." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_SASARAN." AND ".
                TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_INDIKATORSASARAN." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_INDIKATORSASARAN." AND ".
                TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_TUJUANOPD." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_TUJUANOPD." AND ".
                TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_INDIKATORTUJUANOPD." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_INDIKATORTUJUANOPD." AND ".
                TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_SASARANOPD." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_SASARANOPD
                ,"inner")
            ->join(TBL_SAKIP_MOPD_IKSASARAN,
                TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_URUSAN." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_URUSAN." AND ".
                TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_BIDANG." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_BIDANG." AND ".
                TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_UNIT." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_UNIT." AND ".
                TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_SUB." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_SUB." AND ".

                TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_PEMDA." AND ".
                TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_MISI." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_MISI." AND ".
                TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_TUJUAN." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_TUJUAN." AND ".
                TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_INDIKATORTUJUAN." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_INDIKATORTUJUAN." AND ".
                TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_SASARAN." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_SASARAN." AND ".
                TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_INDIKATORSASARAN." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_INDIKATORSASARAN." AND ".
                TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_TUJUANOPD." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_TUJUANOPD." AND ".
                TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_INDIKATORTUJUANOPD." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_INDIKATORTUJUANOPD." AND ".
                TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_SASARANOPD." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_SASARANOPD." AND ".
                TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_INDIKATORSASARANOPD." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_INDIKATORSASARANOPD
                ,"inner")
            ->where(TBL_SAKIP_MBID_PROGRAM.".".COL_UNIQ, $id);

        if($ruser[COL_ROLEID] != ROLEADMIN && $ruser[COL_ROLEID] != ROLEBAPPEDA) {
            $strOPD = explode('.', $ruser[COL_COMPANYID]);
            $this->db->where(TBL_SAKIP_MBID_PROGRAM.".".COL_KD_URUSAN, $strOPD[0]);
            $this->db->where(TBL_SAKIP_MBID_PROGRAM.".".COL_KD_BIDANG, $strOPD[1]);
            $this->db->where(TBL_SAKIP_MBID_PROGRAM.".".COL_KD_UNIT, $strOPD[2]);
            $this->db->where(TBL_SAKIP_MBID_PROGRAM.".".COL_KD_SUB, $strOPD[3]);
            if($ruser[COL_ROLEID] == ROLEKABID) $this->db->where(TBL_SAKIP_MBID_PROGRAM.".".COL_KD_BID, $strOPD[4]);
        }

        $rdata = $data['data'] = $this->db->get(TBL_SAKIP_MBID_PROGRAM)->row_array();
        if(empty($rdata)){
            show_404();
            return;
        }

        $data['title'] = "Program Bidang OPD";;
        $data['edit'] = TRUE;
        if(!empty($_POST)){
            $ruser = GetLoggedUser();
            $resp = array();
            $resp['error'] = 0;
            $resp['success'] = 1;
            $resp['redirect'] = site_url('mbid/program');
            $data = array(
                COL_KD_TAHUN => $this->input->post(COL_KD_TAHUN),
                COL_KD_URUSAN => $this->input->post(COL_KD_URUSAN),
                COL_KD_BIDANG => $this->input->post(COL_KD_BIDANG),
                COL_KD_UNIT => $this->input->post(COL_KD_UNIT),
                COL_KD_SUB => $this->input->post(COL_KD_SUB),
                COL_KD_BID => $this->input->post(COL_KD_BID),

                COL_KD_PEMDA => $this->input->post(COL_KD_PEMDA),
                COL_KD_MISI => $this->input->post(COL_KD_MISI),
                COL_KD_TUJUAN => $this->input->post(COL_KD_TUJUAN),
                COL_KD_INDIKATORTUJUAN => $this->input->post(COL_KD_INDIKATORTUJUAN),
                COL_KD_SASARAN => $this->input->post(COL_KD_SASARAN),
                COL_KD_INDIKATORSASARAN => $this->input->post(COL_KD_INDIKATORSASARAN),
                COL_KD_TUJUANOPD => $this->input->post(COL_KD_TUJUANOPD),
                COL_KD_INDIKATORTUJUANOPD => $this->input->post(COL_KD_INDIKATORTUJUANOPD),
                COL_KD_SASARANOPD => $this->input->post(COL_KD_SASARANOPD),
                COL_KD_INDIKATORSASARANOPD => $this->input->post(COL_KD_INDIKATORSASARANOPD),
                COL_KD_PROGRAMOPD => $this->input->post(COL_KD_PROGRAMOPD),
                COL_NM_PROGRAMOPD => $this->input->post(COL_NM_PROGRAMOPD),
                //COL_NM_SASARANPROGRAM => $this->input->post(COL_NM_SASARANPROGRAM),
                COL_ISEPLAN => !empty($this->input->post(COL_ISEPLAN))?1:0,
                //COL_KD_SATUAN => $this->input->post(COL_KD_SATUAN),
                //COL_NM_INDIKATORPROGRAM => $this->input->post(COL_NM_INDIKATORPROGRAM),
                //COL_AWAL => $this->input->post(COL_AWAL),
                //COL_TARGET => $this->input->post(COL_TARGET),
                //COL_TARGET_N1 => $this->input->post(COL_TARGET_N1),
                COL_REMARKS => $this->input->post(COL_REMARKS),

                COL_EDIT_BY => $ruser[COL_USERNAME],
                COL_EDIT_DATE => date('Y-m-d H:i:s')
            );

            $this->db->trans_begin();
            try {
                if(!$this->db->where(COL_UNIQ, $id)->update(TBL_SAKIP_MBID_PROGRAM, $data)){
                    throw new Exception("Database error: ".$this->db->error());
                }

                $this->db->trans_commit();
                echo json_encode($resp);
            } catch(Exception $e) {
                $this->db->trans_rollback();
                $resp['error'] = $e->getMessage();
                $resp['success'] = 0;
                echo json_encode($resp);
                return;
            }

        }else{
            $this->load->view('mbid/program_form',$data);
        }
    }

    function program_del(){
        $data = $this->input->post('cekbox');
        $deleted = 0;
        foreach ($data as $datum) {
            $this->db->delete(TBL_SAKIP_MBID_PROGRAM, array(COL_UNIQ => $datum));
            $deleted++;
        }
        if($deleted){
            ShowJsonSuccess($deleted." data dihapus");
        }else{
            ShowJsonError("Tidak ada dihapus");
        }
    }

    function sasaran() {
        $ruser = GetLoggedUser();
        $data['title'] = 'Sasaran Eselon III';
        $this->db->select('*,'.TBL_SAKIP_MBID_SASARAN.'.'.COL_UNIQ.' as ID,'.TBL_SAKIP_MBID_SASARAN.'.'.COL_KD_TAHUN.' as Kd_Tahun');
        $this->db->join(TBL_SAKIP_MPEMDA,TBL_SAKIP_MPEMDA.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_MBID_SASARAN.".".COL_KD_PEMDA,"inner");
        $this->db->join(TBL_SAKIP_MPMD_MISI,TBL_SAKIP_MPMD_MISI.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_MBID_SASARAN.".".COL_KD_PEMDA." AND ".TBL_SAKIP_MPMD_MISI.'.'.COL_KD_MISI." = ".TBL_SAKIP_MBID_SASARAN.".".COL_KD_MISI,"inner");
        $this->db->join(TBL_SAKIP_MPMD_TUJUAN,TBL_SAKIP_MPMD_TUJUAN.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_MBID_SASARAN.".".COL_KD_PEMDA." AND ".TBL_SAKIP_MPMD_TUJUAN.'.'.COL_KD_MISI." = ".TBL_SAKIP_MBID_SASARAN.".".COL_KD_MISI." AND ".TBL_SAKIP_MPMD_TUJUAN.'.'.COL_KD_TUJUAN." = ".TBL_SAKIP_MBID_SASARAN.".".COL_KD_TUJUAN,"inner");
        $this->db->join(TBL_SAKIP_MPMD_IKTUJUAN,TBL_SAKIP_MPMD_IKTUJUAN.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_MBID_SASARAN.".".COL_KD_PEMDA." AND ".TBL_SAKIP_MPMD_IKTUJUAN.'.'.COL_KD_MISI." = ".TBL_SAKIP_MBID_SASARAN.".".COL_KD_MISI." AND ".TBL_SAKIP_MPMD_IKTUJUAN.'.'.COL_KD_TUJUAN." = ".TBL_SAKIP_MBID_SASARAN.".".COL_KD_TUJUAN." AND ".TBL_SAKIP_MPMD_IKTUJUAN.'.'.COL_KD_INDIKATORTUJUAN." = ".TBL_SAKIP_MBID_SASARAN.".".COL_KD_INDIKATORTUJUAN,"inner");
        $this->db->join(TBL_SAKIP_MPMD_SASARAN,TBL_SAKIP_MPMD_SASARAN.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_MBID_SASARAN.".".COL_KD_PEMDA." AND ".TBL_SAKIP_MPMD_SASARAN.'.'.COL_KD_MISI." = ".TBL_SAKIP_MBID_SASARAN.".".COL_KD_MISI." AND ".TBL_SAKIP_MPMD_SASARAN.'.'.COL_KD_TUJUAN." = ".TBL_SAKIP_MBID_SASARAN.".".COL_KD_TUJUAN." AND ".TBL_SAKIP_MPMD_SASARAN.'.'.COL_KD_INDIKATORTUJUAN." = ".TBL_SAKIP_MBID_SASARAN.".".COL_KD_INDIKATORTUJUAN." AND ".TBL_SAKIP_MPMD_SASARAN.'.'.COL_KD_SASARAN." = ".TBL_SAKIP_MBID_SASARAN.".".COL_KD_SASARAN,"inner");
        $this->db->join(TBL_SAKIP_MPMD_IKSASARAN,TBL_SAKIP_MPMD_IKSASARAN.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_MBID_SASARAN.".".COL_KD_PEMDA." AND ".TBL_SAKIP_MPMD_IKSASARAN.'.'.COL_KD_MISI." = ".TBL_SAKIP_MBID_SASARAN.".".COL_KD_MISI." AND ".TBL_SAKIP_MPMD_IKSASARAN.'.'.COL_KD_TUJUAN." = ".TBL_SAKIP_MBID_SASARAN.".".COL_KD_TUJUAN." AND ".TBL_SAKIP_MPMD_IKSASARAN.'.'.COL_KD_INDIKATORTUJUAN." = ".TBL_SAKIP_MBID_SASARAN.".".COL_KD_INDIKATORTUJUAN." AND ".TBL_SAKIP_MPMD_IKSASARAN.'.'.COL_KD_SASARAN." = ".TBL_SAKIP_MBID_SASARAN.".".COL_KD_SASARAN." AND ".TBL_SAKIP_MPMD_IKSASARAN.'.'.COL_KD_INDIKATORSASARAN." = ".TBL_SAKIP_MBID_SASARAN.".".COL_KD_INDIKATORSASARAN,"inner");
        $this->db->join(TBL_SAKIP_MOPD_TUJUAN,
            TBL_SAKIP_MOPD_TUJUAN.'.'.COL_KD_URUSAN." = ".TBL_SAKIP_MBID_SASARAN.".".COL_KD_URUSAN." AND ".
            TBL_SAKIP_MOPD_TUJUAN.'.'.COL_KD_BIDANG." = ".TBL_SAKIP_MBID_SASARAN.".".COL_KD_BIDANG." AND ".
            TBL_SAKIP_MOPD_TUJUAN.'.'.COL_KD_UNIT." = ".TBL_SAKIP_MBID_SASARAN.".".COL_KD_UNIT." AND ".
            TBL_SAKIP_MOPD_TUJUAN.'.'.COL_KD_SUB." = ".TBL_SAKIP_MBID_SASARAN.".".COL_KD_SUB." AND ".

            TBL_SAKIP_MOPD_TUJUAN.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_MBID_SASARAN.".".COL_KD_PEMDA." AND ".
            TBL_SAKIP_MOPD_TUJUAN.'.'.COL_KD_MISI." = ".TBL_SAKIP_MBID_SASARAN.".".COL_KD_MISI." AND ".
            TBL_SAKIP_MOPD_TUJUAN.'.'.COL_KD_TUJUAN." = ".TBL_SAKIP_MBID_SASARAN.".".COL_KD_TUJUAN." AND ".
            TBL_SAKIP_MOPD_TUJUAN.'.'.COL_KD_INDIKATORTUJUAN." = ".TBL_SAKIP_MBID_SASARAN.".".COL_KD_INDIKATORTUJUAN." AND ".
            TBL_SAKIP_MOPD_TUJUAN.'.'.COL_KD_SASARAN." = ".TBL_SAKIP_MBID_SASARAN.".".COL_KD_SASARAN." AND ".
            TBL_SAKIP_MOPD_TUJUAN.'.'.COL_KD_INDIKATORSASARAN." = ".TBL_SAKIP_MBID_SASARAN.".".COL_KD_INDIKATORSASARAN. " AND ".
            TBL_SAKIP_MOPD_TUJUAN.'.'.COL_KD_TUJUANOPD." = ".TBL_SAKIP_MBID_SASARAN.".".COL_KD_TUJUANOPD
            ,"inner");
        $this->db->join(TBL_SAKIP_MOPD_IKTUJUAN,
            TBL_SAKIP_MOPD_IKTUJUAN.'.'.COL_KD_URUSAN." = ".TBL_SAKIP_MBID_SASARAN.".".COL_KD_URUSAN." AND ".
            TBL_SAKIP_MOPD_IKTUJUAN.'.'.COL_KD_BIDANG." = ".TBL_SAKIP_MBID_SASARAN.".".COL_KD_BIDANG." AND ".
            TBL_SAKIP_MOPD_IKTUJUAN.'.'.COL_KD_UNIT." = ".TBL_SAKIP_MBID_SASARAN.".".COL_KD_UNIT." AND ".
            TBL_SAKIP_MOPD_IKTUJUAN.'.'.COL_KD_SUB." = ".TBL_SAKIP_MBID_SASARAN.".".COL_KD_SUB." AND ".

            TBL_SAKIP_MOPD_IKTUJUAN.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_MBID_SASARAN.".".COL_KD_PEMDA." AND ".
            TBL_SAKIP_MOPD_IKTUJUAN.'.'.COL_KD_MISI." = ".TBL_SAKIP_MBID_SASARAN.".".COL_KD_MISI." AND ".
            TBL_SAKIP_MOPD_IKTUJUAN.'.'.COL_KD_TUJUAN." = ".TBL_SAKIP_MBID_SASARAN.".".COL_KD_TUJUAN." AND ".
            TBL_SAKIP_MOPD_IKTUJUAN.'.'.COL_KD_INDIKATORTUJUAN." = ".TBL_SAKIP_MBID_SASARAN.".".COL_KD_INDIKATORTUJUAN." AND ".
            TBL_SAKIP_MOPD_IKTUJUAN.'.'.COL_KD_SASARAN." = ".TBL_SAKIP_MBID_SASARAN.".".COL_KD_SASARAN." AND ".
            TBL_SAKIP_MOPD_IKTUJUAN.'.'.COL_KD_INDIKATORSASARAN." = ".TBL_SAKIP_MBID_SASARAN.".".COL_KD_INDIKATORSASARAN." AND ".
            TBL_SAKIP_MOPD_IKTUJUAN.'.'.COL_KD_TUJUANOPD." = ".TBL_SAKIP_MBID_SASARAN.".".COL_KD_TUJUANOPD." AND ".
            TBL_SAKIP_MOPD_IKTUJUAN.'.'.COL_KD_INDIKATORTUJUANOPD." = ".TBL_SAKIP_MBID_SASARAN.".".COL_KD_INDIKATORTUJUANOPD
            ,"inner");
        $this->db->join(TBL_SAKIP_MOPD_SASARAN,
            TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_URUSAN." = ".TBL_SAKIP_MBID_SASARAN.".".COL_KD_URUSAN." AND ".
            TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_BIDANG." = ".TBL_SAKIP_MBID_SASARAN.".".COL_KD_BIDANG." AND ".
            TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_UNIT." = ".TBL_SAKIP_MBID_SASARAN.".".COL_KD_UNIT." AND ".
            TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_SUB." = ".TBL_SAKIP_MBID_SASARAN.".".COL_KD_SUB." AND ".

            TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_MBID_SASARAN.".".COL_KD_PEMDA." AND ".
            TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_MISI." = ".TBL_SAKIP_MBID_SASARAN.".".COL_KD_MISI." AND ".
            TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_TUJUAN." = ".TBL_SAKIP_MBID_SASARAN.".".COL_KD_TUJUAN." AND ".
            TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_INDIKATORTUJUAN." = ".TBL_SAKIP_MBID_SASARAN.".".COL_KD_INDIKATORTUJUAN." AND ".
            TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_SASARAN." = ".TBL_SAKIP_MBID_SASARAN.".".COL_KD_SASARAN." AND ".
            TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_INDIKATORSASARAN." = ".TBL_SAKIP_MBID_SASARAN.".".COL_KD_INDIKATORSASARAN." AND ".
            TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_TUJUANOPD." = ".TBL_SAKIP_MBID_SASARAN.".".COL_KD_TUJUANOPD." AND ".
            TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_INDIKATORTUJUANOPD." = ".TBL_SAKIP_MBID_SASARAN.".".COL_KD_INDIKATORTUJUANOPD." AND ".
            TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_SASARANOPD." = ".TBL_SAKIP_MBID_SASARAN.".".COL_KD_SASARANOPD
            ,"inner");
        $this->db->join(TBL_SAKIP_MOPD_IKSASARAN,
            TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_URUSAN." = ".TBL_SAKIP_MBID_SASARAN.".".COL_KD_URUSAN." AND ".
            TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_BIDANG." = ".TBL_SAKIP_MBID_SASARAN.".".COL_KD_BIDANG." AND ".
            TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_UNIT." = ".TBL_SAKIP_MBID_SASARAN.".".COL_KD_UNIT." AND ".
            TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_SUB." = ".TBL_SAKIP_MBID_SASARAN.".".COL_KD_SUB." AND ".

            TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_MBID_SASARAN.".".COL_KD_PEMDA." AND ".
            TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_MISI." = ".TBL_SAKIP_MBID_SASARAN.".".COL_KD_MISI." AND ".
            TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_TUJUAN." = ".TBL_SAKIP_MBID_SASARAN.".".COL_KD_TUJUAN." AND ".
            TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_INDIKATORTUJUAN." = ".TBL_SAKIP_MBID_SASARAN.".".COL_KD_INDIKATORTUJUAN." AND ".
            TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_SASARAN." = ".TBL_SAKIP_MBID_SASARAN.".".COL_KD_SASARAN." AND ".
            TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_INDIKATORSASARAN." = ".TBL_SAKIP_MBID_SASARAN.".".COL_KD_INDIKATORSASARAN." AND ".
            TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_TUJUANOPD." = ".TBL_SAKIP_MBID_SASARAN.".".COL_KD_TUJUANOPD." AND ".
            TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_INDIKATORTUJUANOPD." = ".TBL_SAKIP_MBID_SASARAN.".".COL_KD_INDIKATORTUJUANOPD." AND ".
            TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_SASARANOPD." = ".TBL_SAKIP_MBID_SASARAN.".".COL_KD_SASARANOPD." AND ".
            TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_INDIKATORSASARANOPD." = ".TBL_SAKIP_MBID_SASARAN.".".COL_KD_INDIKATORSASARANOPD
            ,"inner");
        $this->db->join(TBL_SAKIP_MBID,
            TBL_SAKIP_MBID.'.'.COL_KD_URUSAN." = ".TBL_SAKIP_MBID_SASARAN.".".COL_KD_URUSAN." AND ".
            TBL_SAKIP_MBID.'.'.COL_KD_BIDANG." = ".TBL_SAKIP_MBID_SASARAN.".".COL_KD_BIDANG." AND ".
            TBL_SAKIP_MBID.'.'.COL_KD_UNIT." = ".TBL_SAKIP_MBID_SASARAN.".".COL_KD_UNIT." AND ".
            TBL_SAKIP_MBID.'.'.COL_KD_SUB." = ".TBL_SAKIP_MBID_SASARAN.".".COL_KD_SUB." AND ".
            TBL_SAKIP_MBID.'.'.COL_KD_BID." = ".TBL_SAKIP_MBID_SASARAN.".".COL_KD_BID
            ,"inner");
        /*$this->db->join(TBL_SAKIP_MBID_PROGRAM,
            TBL_SAKIP_MBID_PROGRAM.'.'.COL_KD_URUSAN." = ".TBL_SAKIP_MBID_SASARAN.".".COL_KD_URUSAN." AND ".
            TBL_SAKIP_MBID_PROGRAM.'.'.COL_KD_BIDANG." = ".TBL_SAKIP_MBID_SASARAN.".".COL_KD_BIDANG." AND ".
            TBL_SAKIP_MBID_PROGRAM.'.'.COL_KD_UNIT." = ".TBL_SAKIP_MBID_SASARAN.".".COL_KD_UNIT." AND ".
            TBL_SAKIP_MBID_PROGRAM.'.'.COL_KD_SUB." = ".TBL_SAKIP_MBID_SASARAN.".".COL_KD_SUB." AND ".

            TBL_SAKIP_MBID_PROGRAM.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_MBID_SASARAN.".".COL_KD_PEMDA." AND ".
            TBL_SAKIP_MBID_PROGRAM.'.'.COL_KD_MISI." = ".TBL_SAKIP_MBID_SASARAN.".".COL_KD_MISI." AND ".
            TBL_SAKIP_MBID_PROGRAM.'.'.COL_KD_TUJUAN." = ".TBL_SAKIP_MBID_SASARAN.".".COL_KD_TUJUAN." AND ".
            TBL_SAKIP_MBID_PROGRAM.'.'.COL_KD_INDIKATORTUJUAN." = ".TBL_SAKIP_MBID_SASARAN.".".COL_KD_INDIKATORTUJUAN." AND ".
            TBL_SAKIP_MBID_PROGRAM.'.'.COL_KD_SASARAN." = ".TBL_SAKIP_MBID_SASARAN.".".COL_KD_SASARAN." AND ".
            TBL_SAKIP_MBID_PROGRAM.'.'.COL_KD_INDIKATORSASARAN." = ".TBL_SAKIP_MBID_SASARAN.".".COL_KD_INDIKATORSASARAN." AND ".
            TBL_SAKIP_MBID_PROGRAM.'.'.COL_KD_TUJUANOPD." = ".TBL_SAKIP_MBID_SASARAN.".".COL_KD_TUJUANOPD." AND ".
            TBL_SAKIP_MBID_PROGRAM.'.'.COL_KD_INDIKATORTUJUANOPD." = ".TBL_SAKIP_MBID_SASARAN.".".COL_KD_INDIKATORTUJUANOPD." AND ".
            TBL_SAKIP_MBID_PROGRAM.'.'.COL_KD_SASARANOPD." = ".TBL_SAKIP_MBID_SASARAN.".".COL_KD_SASARANOPD." AND ".
            TBL_SAKIP_MBID_PROGRAM.'.'.COL_KD_INDIKATORSASARANOPD." = ".TBL_SAKIP_MBID_SASARAN.".".COL_KD_INDIKATORSASARANOPD." AND ".
            TBL_SAKIP_MBID_PROGRAM.'.'.COL_KD_BID." = ".TBL_SAKIP_MBID_SASARAN.".".COL_KD_BID." AND ".
            TBL_SAKIP_MBID_PROGRAM.'.'.COL_KD_PROGRAMOPD." = ".TBL_SAKIP_MBID_SASARAN.".".COL_KD_PROGRAMOPD
            ,"inner");*/
        if($ruser[COL_ROLEID] != ROLEADMIN && $ruser[COL_ROLEID] != ROLEBAPPEDA) {
            $strOPD = explode('.', $ruser[COL_COMPANYID]);
            $this->db->where(TBL_SAKIP_MBID.".".COL_KD_URUSAN, $strOPD[0]);
            $this->db->where(TBL_SAKIP_MBID.".".COL_KD_BIDANG, $strOPD[1]);
            $this->db->where(TBL_SAKIP_MBID.".".COL_KD_UNIT, $strOPD[2]);
            $this->db->where(TBL_SAKIP_MBID.".".COL_KD_SUB, $strOPD[3]);
            if($ruser[COL_ROLEID] == ROLEKABID) $this->db->where(TBL_SAKIP_MBID.".".COL_KD_BID, $strOPD[4]);
        }
        $this->db->where(TBL_SAKIP_MPEMDA.".".COL_KD_TAHUN_FROM." <=", date("Y"));
        $this->db->where(TBL_SAKIP_MPEMDA.".".COL_KD_TAHUN_TO." >=", date("Y"));
        $this->db->order_by(TBL_SAKIP_MBID_SASARAN.".".COL_KD_TAHUN, 'desc');
        $this->db->order_by(TBL_SAKIP_MBID_SASARAN.".".COL_KD_URUSAN, 'asc');
        $this->db->order_by(TBL_SAKIP_MBID_SASARAN.".".COL_KD_BIDANG, 'asc');
        $this->db->order_by(TBL_SAKIP_MBID_SASARAN.".".COL_KD_UNIT, 'asc');
        $this->db->order_by(TBL_SAKIP_MBID_SASARAN.".".COL_KD_SUB, 'asc');
        $this->db->order_by(TBL_SAKIP_MBID_SASARAN.".".COL_KD_BID, 'asc');
        $this->db->order_by(TBL_SAKIP_MPEMDA.".".COL_KD_TAHUN_FROM, 'desc');
        $this->db->order_by(TBL_SAKIP_MBID_SASARAN.".".COL_KD_MISI, 'asc');
        $this->db->order_by(TBL_SAKIP_MBID_SASARAN.".".COL_KD_TUJUAN, 'asc');
        $this->db->order_by(TBL_SAKIP_MBID_SASARAN.".".COL_KD_INDIKATORTUJUAN, 'asc');
        $this->db->order_by(TBL_SAKIP_MBID_SASARAN.".".COL_KD_SASARAN, 'asc');
        $this->db->order_by(TBL_SAKIP_MBID_SASARAN.".".COL_KD_INDIKATORSASARAN, 'asc');
        $this->db->order_by(TBL_SAKIP_MBID_SASARAN.".".COL_KD_TUJUANOPD, 'asc');
        $this->db->order_by(TBL_SAKIP_MBID_SASARAN.".".COL_KD_INDIKATORTUJUANOPD, 'asc');
        $this->db->order_by(TBL_SAKIP_MBID_SASARAN.".".COL_KD_SASARANOPD, 'asc');
        $this->db->order_by(TBL_SAKIP_MBID_SASARAN.".".COL_KD_INDIKATORSASARANOPD, 'asc');
        $this->db->order_by(TBL_SAKIP_MBID_SASARAN.".".COL_KD_SASARANPROGRAMOPD, 'asc');
        $data['res'] = $this->db->get(TBL_SAKIP_MBID_SASARAN)->result_array();
        $this->load->view('mbid/sasaran', $data);
    }

    function sasaran_add() {
        $data['title'] = "Sasaran Bidang OPD";
        $data['edit'] = FALSE;

        if(!empty($_POST)){
            $ruser = GetLoggedUser();
            $resp = array();
            $resp['error'] = 0;
            $resp['success'] = 1;
            $resp['redirect'] = site_url('mbid/sasaran');
            $data = array(
                COL_KD_TAHUN => $this->input->post(COL_KD_TAHUN),
                COL_KD_URUSAN => $this->input->post(COL_KD_URUSAN),
                COL_KD_BIDANG => $this->input->post(COL_KD_BIDANG),
                COL_KD_UNIT => $this->input->post(COL_KD_UNIT),
                COL_KD_SUB => $this->input->post(COL_KD_SUB),
                COL_KD_BID => $this->input->post(COL_KD_BID),

                COL_KD_PEMDA => $this->input->post(COL_KD_PEMDA),
                COL_KD_MISI => $this->input->post(COL_KD_MISI),
                COL_KD_TUJUAN => $this->input->post(COL_KD_TUJUAN),
                COL_KD_INDIKATORTUJUAN => $this->input->post(COL_KD_INDIKATORTUJUAN),
                COL_KD_SASARAN => $this->input->post(COL_KD_SASARAN),
                COL_KD_INDIKATORSASARAN => $this->input->post(COL_KD_INDIKATORSASARAN),
                COL_KD_TUJUANOPD => $this->input->post(COL_KD_TUJUANOPD),
                COL_KD_INDIKATORTUJUANOPD => $this->input->post(COL_KD_INDIKATORTUJUANOPD),
                COL_KD_SASARANOPD => $this->input->post(COL_KD_SASARANOPD),
                COL_KD_INDIKATORSASARANOPD => $this->input->post(COL_KD_INDIKATORSASARANOPD),
                //COL_KD_PROGRAMOPD => $this->input->post(COL_KD_PROGRAMOPD),
                COL_KD_SASARANPROGRAMOPD => $this->input->post(COL_KD_SASARANPROGRAMOPD),
                COL_NM_SASARANPROGRAMOPD => $this->input->post(COL_NM_SASARANPROGRAMOPD),
            );

            $this->db->trans_begin();
            try {
                if(!$this->db->insert(TBL_SAKIP_MBID_SASARAN, $data)){
                    throw new Exception("Database error: ".$this->db->error());
                }

                $detNo = $this->input->post("NoDet");
                $detDesc = $this->input->post("KetDet");
                $det = [];
                for($i = 0; $i<count($detNo); $i++) {
                    $det[] = array(
                        COL_KD_TAHUN => $this->input->post(COL_KD_TAHUN),
                        COL_KD_URUSAN => $this->input->post(COL_KD_URUSAN),
                        COL_KD_BIDANG => $this->input->post(COL_KD_BIDANG),
                        COL_KD_UNIT => $this->input->post(COL_KD_UNIT),
                        COL_KD_SUB => $this->input->post(COL_KD_SUB),
                        COL_KD_BID => $this->input->post(COL_KD_BID),

                        COL_KD_PEMDA => $this->input->post(COL_KD_PEMDA),
                        COL_KD_MISI => $this->input->post(COL_KD_MISI),
                        COL_KD_TUJUAN => $this->input->post(COL_KD_TUJUAN),
                        COL_KD_INDIKATORTUJUAN => $this->input->post(COL_KD_INDIKATORTUJUAN),
                        COL_KD_SASARAN => $this->input->post(COL_KD_SASARAN),
                        COL_KD_INDIKATORSASARAN => $this->input->post(COL_KD_INDIKATORSASARAN),
                        COL_KD_TUJUANOPD => $this->input->post(COL_KD_TUJUANOPD),
                        COL_KD_INDIKATORTUJUANOPD => $this->input->post(COL_KD_INDIKATORTUJUANOPD),
                        COL_KD_SASARANOPD => $this->input->post(COL_KD_SASARANOPD),
                        COL_KD_INDIKATORSASARANOPD => $this->input->post(COL_KD_INDIKATORSASARANOPD),
                        //COL_KD_PROGRAMOPD => $this->input->post(COL_KD_PROGRAMOPD),
                        COL_KD_SASARANPROGRAMOPD => $this->input->post(COL_KD_SASARANPROGRAMOPD),
                        COL_KD_INDIKATORPROGRAMOPD => $detNo[$i],
                        COL_NM_INDIKATORPROGRAMOPD => $detDesc[$i]
                    );
                }
                if(count($det) > 0) {
                    $res = $this->db->insert_batch(TBL_SAKIP_MBID_INDIKATOR, $det);
                    if(!$res) {
                        throw new Exception("Database error: ".$this->db->error());
                    }
                }

                $this->db->trans_commit();
                echo json_encode($resp);

            } catch (Exception $e) {
                $this->db->trans_rollback();
                $resp['error'] = $e->getMessage();
                $resp['success'] = 0;
                echo json_encode($resp);
                return;
            }
        }else{
            $this->load->view('mbid/sasaran_form',$data);
        }
    }

    function sasaran_edit($id) {
        $ruser = GetLoggedUser();
        $this->db
            ->select(TBL_SAKIP_MBID_SASARAN.'.*,'.TBL_SAKIP_MBID.'.'.COL_NM_BID.','.TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_NM_INDIKATORSASARANOPD/*.','.TBL_SAKIP_MBID_PROGRAM.'.'.COL_NM_PROGRAMOPD.','.TBL_SAKIP_MBID_PROGRAM.'.'.COL_ISEPLAN*/)
            ->join(TBL_SAKIP_MBID,
                TBL_SAKIP_MBID.'.'.COL_KD_URUSAN." = ".TBL_SAKIP_MBID_SASARAN.".".COL_KD_URUSAN." AND ".
                TBL_SAKIP_MBID.'.'.COL_KD_BIDANG." = ".TBL_SAKIP_MBID_SASARAN.".".COL_KD_BIDANG." AND ".
                TBL_SAKIP_MBID.'.'.COL_KD_UNIT." = ".TBL_SAKIP_MBID_SASARAN.".".COL_KD_UNIT." AND ".
                TBL_SAKIP_MBID.'.'.COL_KD_SUB." = ".TBL_SAKIP_MBID_SASARAN.".".COL_KD_SUB." AND ".
                TBL_SAKIP_MBID.'.'.COL_KD_BID." = ".TBL_SAKIP_MBID_SASARAN.".".COL_KD_BID
                ,"inner")
            ->join(TBL_SAKIP_MOPD_IKSASARAN,
                TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_URUSAN." = ".TBL_SAKIP_MBID_SASARAN.".".COL_KD_URUSAN." AND ".
                TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_BIDANG." = ".TBL_SAKIP_MBID_SASARAN.".".COL_KD_BIDANG." AND ".
                TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_UNIT." = ".TBL_SAKIP_MBID_SASARAN.".".COL_KD_UNIT." AND ".
                TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_SUB." = ".TBL_SAKIP_MBID_SASARAN.".".COL_KD_SUB." AND ".

                TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_MBID_SASARAN.".".COL_KD_PEMDA." AND ".
                TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_MISI." = ".TBL_SAKIP_MBID_SASARAN.".".COL_KD_MISI." AND ".
                TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_TUJUAN." = ".TBL_SAKIP_MBID_SASARAN.".".COL_KD_TUJUAN." AND ".
                TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_INDIKATORTUJUAN." = ".TBL_SAKIP_MBID_SASARAN.".".COL_KD_INDIKATORTUJUAN." AND ".
                TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_SASARAN." = ".TBL_SAKIP_MBID_SASARAN.".".COL_KD_SASARAN." AND ".
                TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_INDIKATORSASARAN." = ".TBL_SAKIP_MBID_SASARAN.".".COL_KD_INDIKATORSASARAN." AND ".
                TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_TUJUANOPD." = ".TBL_SAKIP_MBID_SASARAN.".".COL_KD_TUJUANOPD." AND ".
                TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_INDIKATORTUJUANOPD." = ".TBL_SAKIP_MBID_SASARAN.".".COL_KD_INDIKATORTUJUANOPD." AND ".
                TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_SASARANOPD." = ".TBL_SAKIP_MBID_SASARAN.".".COL_KD_SASARANOPD." AND ".
                TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_INDIKATORSASARANOPD." = ".TBL_SAKIP_MBID_SASARAN.".".COL_KD_INDIKATORSASARANOPD
                ,"inner")
            /*->join(TBL_SAKIP_MBID_PROGRAM,
            TBL_SAKIP_MBID_PROGRAM.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_MBID_SASARAN.".".COL_KD_PEMDA." AND ".
            TBL_SAKIP_MBID_PROGRAM.'.'.COL_KD_MISI." = ".TBL_SAKIP_MBID_SASARAN.".".COL_KD_MISI." AND ".
            TBL_SAKIP_MBID_PROGRAM.'.'.COL_KD_TUJUAN." = ".TBL_SAKIP_MBID_SASARAN.".".COL_KD_TUJUAN." AND ".
            TBL_SAKIP_MBID_PROGRAM.'.'.COL_KD_INDIKATORTUJUAN." = ".TBL_SAKIP_MBID_SASARAN.".".COL_KD_INDIKATORTUJUAN." AND ".
            TBL_SAKIP_MBID_PROGRAM.'.'.COL_KD_SASARAN." = ".TBL_SAKIP_MBID_SASARAN.".".COL_KD_SASARAN." AND ".
            TBL_SAKIP_MBID_PROGRAM.'.'.COL_KD_INDIKATORSASARAN." = ".TBL_SAKIP_MBID_SASARAN.".".COL_KD_INDIKATORSASARAN." AND ".
            TBL_SAKIP_MBID_PROGRAM.'.'.COL_KD_TUJUANOPD." = ".TBL_SAKIP_MBID_SASARAN.".".COL_KD_TUJUANOPD." AND ".
            TBL_SAKIP_MBID_PROGRAM.'.'.COL_KD_INDIKATORTUJUANOPD." = ".TBL_SAKIP_MBID_SASARAN.".".COL_KD_INDIKATORTUJUANOPD." AND ".
            TBL_SAKIP_MBID_PROGRAM.'.'.COL_KD_SASARANOPD." = ".TBL_SAKIP_MBID_SASARAN.".".COL_KD_SASARANOPD." AND ".
            TBL_SAKIP_MBID_PROGRAM.'.'.COL_KD_INDIKATORSASARANOPD." = ".TBL_SAKIP_MBID_SASARAN.".".COL_KD_INDIKATORSASARANOPD." AND ".
            TBL_SAKIP_MBID_PROGRAM.'.'.COL_KD_BID." = ".TBL_SAKIP_MBID_SASARAN.".".COL_KD_BID." AND ".
            TBL_SAKIP_MBID_PROGRAM.'.'.COL_KD_PROGRAMOPD." = ".TBL_SAKIP_MBID_SASARAN.".".COL_KD_PROGRAMOPD
            ,"inner")*/
            ->where(TBL_SAKIP_MBID_SASARAN.".".COL_UNIQ, $id);
        if($ruser[COL_ROLEID] != ROLEADMIN && $ruser[COL_ROLEID] != ROLEBAPPEDA) {
            $strOPD = explode('.', $ruser[COL_COMPANYID]);
            $this->db->where(TBL_SAKIP_MBID_SASARAN.".".COL_KD_URUSAN, $strOPD[0]);
            $this->db->where(TBL_SAKIP_MBID_SASARAN.".".COL_KD_BIDANG, $strOPD[1]);
            $this->db->where(TBL_SAKIP_MBID_SASARAN.".".COL_KD_UNIT, $strOPD[2]);
            $this->db->where(TBL_SAKIP_MBID_SASARAN.".".COL_KD_SUB, $strOPD[3]);
            if($ruser[COL_ROLEID] == ROLEKABID) $this->db->where(TBL_SAKIP_MBID_SASARAN.".".COL_KD_BID, $strOPD[4]);
        }
        $rdata = $data['data'] = $this->db->get(TBL_SAKIP_MBID_SASARAN)->row_array();
        if(empty($rdata)){
            show_404();
            return;
        }

        $data['title'] = "Sasaran Bidang OPD";;
        $data['edit'] = TRUE;
        if(!empty($_POST)){
            $resp = array();
            $resp['error'] = 0;
            $resp['success'] = 1;
            $resp['redirect'] = site_url('mbid/sasaran');
            $data = array(
                COL_KD_TAHUN => $this->input->post(COL_KD_TAHUN),
                COL_KD_URUSAN => $this->input->post(COL_KD_URUSAN),
                COL_KD_BIDANG => $this->input->post(COL_KD_BIDANG),
                COL_KD_UNIT => $this->input->post(COL_KD_UNIT),
                COL_KD_SUB => $this->input->post(COL_KD_SUB),
                COL_KD_BID => $this->input->post(COL_KD_BID),

                COL_KD_PEMDA => $this->input->post(COL_KD_PEMDA),
                COL_KD_MISI => $this->input->post(COL_KD_MISI),
                COL_KD_TUJUAN => $this->input->post(COL_KD_TUJUAN),
                COL_KD_INDIKATORTUJUAN => $this->input->post(COL_KD_INDIKATORTUJUAN),
                COL_KD_SASARAN => $this->input->post(COL_KD_SASARAN),
                COL_KD_INDIKATORSASARAN => $this->input->post(COL_KD_INDIKATORSASARAN),
                COL_KD_TUJUANOPD => $this->input->post(COL_KD_TUJUANOPD),
                COL_KD_INDIKATORTUJUANOPD => $this->input->post(COL_KD_INDIKATORTUJUANOPD),
                COL_KD_SASARANOPD => $this->input->post(COL_KD_SASARANOPD),
                COL_KD_INDIKATORSASARANOPD => $this->input->post(COL_KD_INDIKATORSASARANOPD),
                //COL_KD_PROGRAMOPD => $this->input->post(COL_KD_PROGRAMOPD),
                COL_KD_SASARANPROGRAMOPD => $this->input->post(COL_KD_SASARANPROGRAMOPD),
                COL_NM_SASARANPROGRAMOPD => $this->input->post(COL_NM_SASARANPROGRAMOPD),
            );

            $this->db->trans_begin();
            try {
                if(!$this->db->where(COL_UNIQ, $id)->update(TBL_SAKIP_MBID_SASARAN, $data)){
                    throw new Exception("Database error: ".$this->db->error());
                }
                $detNo = $this->input->post("NoDet");
                $detKet = $this->input->post("KetDet");
                $arrDet = [];

                /* update / delete */
                $det = $this->db
                    //->select(COL_KD_PEMDA.",".COL_KD_MISI.",".COL_KD_TUJUAN.",".COL_KD_INDIKATORTUJUAN.",".COL_KD_SASARAN.",".COL_KD_INDIKATORSASARAN)
                    ->where(COL_KD_URUSAN, $rdata[COL_KD_URUSAN])
                    ->where(COL_KD_BIDANG, $rdata[COL_KD_BIDANG])
                    ->where(COL_KD_UNIT, $rdata[COL_KD_UNIT])
                    ->where(COL_KD_SUB, $rdata[COL_KD_SUB])
                    ->where(COL_KD_BID, $rdata[COL_KD_BID])

                    ->where(COL_KD_PEMDA, $rdata[COL_KD_PEMDA])
                    ->where(COL_KD_MISI, $rdata[COL_KD_MISI])
                    ->where(COL_KD_TUJUAN, $rdata[COL_KD_TUJUAN])
                    ->where(COL_KD_INDIKATORTUJUAN, $rdata[COL_KD_INDIKATORTUJUAN])
                    ->where(COL_KD_SASARAN, $rdata[COL_KD_SASARAN])
                    ->where(COL_KD_INDIKATORSASARAN, $rdata[COL_KD_INDIKATORSASARAN])
                    ->where(COL_KD_TUJUANOPD, $rdata[COL_KD_TUJUANOPD])
                    ->where(COL_KD_INDIKATORTUJUANOPD, $rdata[COL_KD_INDIKATORTUJUANOPD])
                    ->where(COL_KD_SASARANOPD, $rdata[COL_KD_SASARANOPD])
                    ->where(COL_KD_INDIKATORSASARANOPD, $rdata[COL_KD_INDIKATORSASARANOPD])
                    //->where(COL_KD_TAHUN, $rdata[COL_KD_TAHUN])
                    //->where(COL_KD_PROGRAMOPD, $rdata[COL_KD_PROGRAMOPD])
                    ->where(COL_KD_SASARANPROGRAMOPD, $rdata[COL_KD_SASARANPROGRAMOPD])
                    ->get(TBL_SAKIP_MBID_INDIKATOR)
                    ->result_array();
                $detUpdated = [];
                foreach($det as $d) {
                    /*$arrCond = array(
                        COL_KD_URUSAN => $d[COL_KD_URUSAN],
                        COL_KD_BIDANG => $d[COL_KD_BIDANG],
                        COL_KD_UNIT => $d[COL_KD_UNIT],
                        COL_KD_SUB => $d[COL_KD_SUB],
                        COL_KD_BID => $d[COL_KD_BID],

                        COL_KD_PEMDA=>$d[COL_KD_PEMDA],
                        COL_KD_MISI=>$d[COL_KD_MISI],
                        COL_KD_TUJUAN=>$d[COL_KD_TUJUAN],
                        COL_KD_INDIKATORTUJUAN=>$d[COL_KD_INDIKATORTUJUAN],
                        COL_KD_SASARAN=>$d[COL_KD_SASARAN],
                        COL_KD_INDIKATORSASARAN=>$d[COL_KD_INDIKATORSASARAN],
                        COL_KD_TUJUANOPD=>$d[COL_KD_TUJUANOPD],
                        COL_KD_INDIKATORTUJUANOPD=>$d[COL_KD_INDIKATORTUJUANOPD],
                        COL_KD_SASARANOPD=>$d[COL_KD_SASARANOPD],
                        COL_KD_INDIKATORSASARANOPD=>$d[COL_KD_INDIKATORSASARANOPD],
                        COL_KD_TAHUN=>$d[COL_KD_TAHUN],
                        //COL_KD_PROGRAMOPD=>$d[COL_KD_PROGRAMOPD],
                        COL_KD_SASARANPROGRAMOPD=>$d[COL_KD_SASARANPROGRAMOPD],
                        COL_KD_INDIKATORPROGRAMOPD=>$d[COL_KD_INDIKATORPROGRAMOPD]
                    );
                    if(!empty($detNo) && in_array($d[COL_KD_INDIKATORPROGRAMOPD], $detNo)) {
                        $res = $this->db
                            ->where($arrCond)
                            ->update(TBL_SAKIP_MBID_INDIKATOR, array(
                                COL_KD_URUSAN => $this->input->post(COL_KD_URUSAN),
                                COL_KD_BIDANG => $this->input->post(COL_KD_BIDANG),
                                COL_KD_UNIT => $this->input->post(COL_KD_UNIT),
                                COL_KD_SUB => $this->input->post(COL_KD_SUB),
                                COL_KD_BID => $this->input->post(COL_KD_BID),

                                COL_KD_PEMDA => $this->input->post(COL_KD_PEMDA),
                                COL_KD_MISI => $this->input->post(COL_KD_MISI),
                                COL_KD_TUJUAN => $this->input->post(COL_KD_TUJUAN),
                                COL_KD_INDIKATORTUJUAN => $this->input->post(COL_KD_INDIKATORTUJUAN),
                                COL_KD_SASARAN => $this->input->post(COL_KD_SASARAN),
                                COL_KD_INDIKATORSASARAN => $this->input->post(COL_KD_INDIKATORSASARAN),
                                COL_KD_TUJUANOPD => $this->input->post(COL_KD_TUJUANOPD),
                                COL_KD_INDIKATORTUJUANOPD => $this->input->post(COL_KD_INDIKATORTUJUANOPD),
                                COL_KD_SASARANOPD => $this->input->post(COL_KD_SASARANOPD),
                                COL_KD_INDIKATORSASARANOPD => $this->input->post(COL_KD_INDIKATORSASARANOPD),
                                COL_KD_TAHUN => $this->input->post(COL_KD_TAHUN),
                                //COL_KD_PROGRAMOPD => $this->input->post(COL_KD_PROGRAMOPD),
                                COL_KD_SASARANPROGRAMOPD => $this->input->post(COL_KD_SASARANPROGRAMOPD),

                                COL_KD_INDIKATORPROGRAMOPD => $d[COL_KD_INDIKATORPROGRAMOPD],
                                COL_NM_INDIKATORPROGRAMOPD => $detKet[array_search($d[COL_KD_INDIKATORPROGRAMOPD], $detNo)]
                            ));

                        if(!$res) {
                            throw new Exception("Database error: ".$this->db->error());
                        }
                    }
                    else {
                        $res = $this->db->delete(TBL_SAKIP_MBID_INDIKATOR, $arrCond);
                        if(!$res) {
                            throw new Exception("Database error: ".$this->db->error());
                        }
                    }*/
                    $detUpdated[] = $d[COL_KD_INDIKATORPROGRAMOPD];
                }
                /* update / delete */

                if(!$this->db->where(COL_UNIQ, $id)->update(TBL_SAKIP_MBID_SASARAN, $data)){
                    throw new Exception("Database error: ".$this->db->error());
                }
                
                /* insert */
                for($i = 0; $i<count($detNo); $i++) {
                    if(!in_array($detNo[$i], $detUpdated)) {
                        $arrDet[] = array(
                            COL_KD_URUSAN => $this->input->post(COL_KD_URUSAN),
                            COL_KD_BIDANG => $this->input->post(COL_KD_BIDANG),
                            COL_KD_UNIT => $this->input->post(COL_KD_UNIT),
                            COL_KD_SUB => $this->input->post(COL_KD_SUB),
                            COL_KD_BID => $this->input->post(COL_KD_BID),

                            COL_KD_PEMDA => $this->input->post(COL_KD_PEMDA),
                            COL_KD_MISI => $this->input->post(COL_KD_MISI),
                            COL_KD_TUJUAN => $this->input->post(COL_KD_TUJUAN),
                            COL_KD_INDIKATORTUJUAN => $this->input->post(COL_KD_INDIKATORTUJUAN),
                            COL_KD_SASARAN => $this->input->post(COL_KD_SASARAN),
                            COL_KD_INDIKATORSASARAN => $this->input->post(COL_KD_INDIKATORSASARAN),
                            COL_KD_TUJUANOPD => $this->input->post(COL_KD_TUJUANOPD),
                            COL_KD_INDIKATORTUJUANOPD => $this->input->post(COL_KD_INDIKATORTUJUANOPD),
                            COL_KD_SASARANOPD => $this->input->post(COL_KD_SASARANOPD),
                            COL_KD_INDIKATORSASARANOPD => $this->input->post(COL_KD_INDIKATORSASARANOPD),
                            COL_KD_TAHUN => $this->input->post(COL_KD_TAHUN),
                            //COL_KD_PROGRAMOPD => $this->input->post(COL_KD_PROGRAMOPD),
                            COL_KD_SASARANPROGRAMOPD => $this->input->post(COL_KD_SASARANPROGRAMOPD),

                            COL_KD_INDIKATORPROGRAMOPD => $detNo[$i],
                            COL_NM_INDIKATORPROGRAMOPD => $detKet[$i]
                        );
                    }
                }
                //throw new Exception("Det: ". json_encode($arrDet));
                if(count($arrDet) > 0) {
                    $res = $this->db->insert_batch(TBL_SAKIP_MBID_INDIKATOR, $arrDet);
                    //throw new Exception("Det: ".json_encode($arrDet));
                    if(!$res) {
                        throw new Exception("Database error: ".$this->db->error());
                    }
                }
                /* insert */
                $this->db->trans_commit();
                echo json_encode($resp);

            } catch (Exception $e) {
                $this->db->trans_rollback();
                $resp['error'] = $e->getMessage();
                $resp['success'] = 0;
                echo json_encode($resp);
                return;
            }
        }else{
            $this->load->view('mbid/sasaran_form',$data);
        }
    }

    function sasaran_del(){
        $data = $this->input->post('cekbox');
        $deleted = 0;
        foreach ($data as $datum) {
            $rsasaran = $this->db->where(COL_UNIQ, $datum)->get(TBL_SAKIP_MBID_SASARAN)->row_array();
            $arrCond = array(
                COL_KD_URUSAN => $rsasaran[COL_KD_URUSAN],
                COL_KD_BIDANG => $rsasaran[COL_KD_BIDANG],
                COL_KD_UNIT => $rsasaran[COL_KD_UNIT],
                COL_KD_SUB => $rsasaran[COL_KD_SUB],
                COL_KD_BID => $rsasaran[COL_KD_BID],

                COL_KD_PEMDA=>$rsasaran[COL_KD_PEMDA],
                COL_KD_MISI=>$rsasaran[COL_KD_MISI],
                COL_KD_TUJUAN=>$rsasaran[COL_KD_TUJUAN],
                COL_KD_INDIKATORTUJUAN=>$rsasaran[COL_KD_INDIKATORTUJUAN],
                COL_KD_SASARAN=>$rsasaran[COL_KD_SASARAN],
                COL_KD_INDIKATORSASARAN=>$rsasaran[COL_KD_INDIKATORSASARAN],
                COL_KD_TUJUANOPD=>$rsasaran[COL_KD_TUJUANOPD],
                COL_KD_INDIKATORTUJUANOPD=>$rsasaran[COL_KD_INDIKATORTUJUANOPD],
                COL_KD_SASARANOPD=>$rsasaran[COL_KD_SASARANOPD],
                COL_KD_INDIKATORSASARANOPD=>$rsasaran[COL_KD_INDIKATORSASARANOPD],
                COL_KD_TAHUN=>$rsasaran[COL_KD_TAHUN],
                //COL_KD_PROGRAMOPD=>$rsasaran[COL_KD_PROGRAMOPD],
                COL_KD_SASARANPROGRAMOPD=>$rsasaran[COL_KD_SASARANPROGRAMOPD]
            );

            $this->db->delete(TBL_SAKIP_MBID_INDIKATOR, $arrCond);
            $this->db->delete(TBL_SAKIP_MBID_SASARAN, array(COL_UNIQ => $datum));
            $deleted++;
        }
        if($deleted){
            ShowJsonSuccess($deleted." data dihapus");
        }else{
            ShowJsonError("Tidak ada dihapus");
        }
    }

    function sasaran_program() {
        $ruser = GetLoggedUser();
        $data['title'] = 'Sasaran Program OPD';
        $this->db->select('*,'.TBL_SAKIP_MBID_PROGRAM_SASARAN.'.'.COL_UNIQ.' as ID,'.TBL_SAKIP_MBID_PROGRAM_SASARAN.'.'.COL_KD_TAHUN.' as Kd_Tahun');
        $this->db->join(TBL_SAKIP_MPEMDA,TBL_SAKIP_MPEMDA.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_MBID_PROGRAM_SASARAN.".".COL_KD_PEMDA,"inner");
        $this->db->join(TBL_SAKIP_MPMD_MISI,TBL_SAKIP_MPMD_MISI.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_MBID_PROGRAM_SASARAN.".".COL_KD_PEMDA." AND ".TBL_SAKIP_MPMD_MISI.'.'.COL_KD_MISI." = ".TBL_SAKIP_MBID_PROGRAM_SASARAN.".".COL_KD_MISI,"inner");
        $this->db->join(TBL_SAKIP_MPMD_TUJUAN,TBL_SAKIP_MPMD_TUJUAN.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_MBID_PROGRAM_SASARAN.".".COL_KD_PEMDA." AND ".TBL_SAKIP_MPMD_TUJUAN.'.'.COL_KD_MISI." = ".TBL_SAKIP_MBID_PROGRAM_SASARAN.".".COL_KD_MISI." AND ".TBL_SAKIP_MPMD_TUJUAN.'.'.COL_KD_TUJUAN." = ".TBL_SAKIP_MBID_PROGRAM_SASARAN.".".COL_KD_TUJUAN,"inner");
        $this->db->join(TBL_SAKIP_MPMD_IKTUJUAN,TBL_SAKIP_MPMD_IKTUJUAN.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_MBID_PROGRAM_SASARAN.".".COL_KD_PEMDA." AND ".TBL_SAKIP_MPMD_IKTUJUAN.'.'.COL_KD_MISI." = ".TBL_SAKIP_MBID_PROGRAM_SASARAN.".".COL_KD_MISI." AND ".TBL_SAKIP_MPMD_IKTUJUAN.'.'.COL_KD_TUJUAN." = ".TBL_SAKIP_MBID_PROGRAM_SASARAN.".".COL_KD_TUJUAN." AND ".TBL_SAKIP_MPMD_IKTUJUAN.'.'.COL_KD_INDIKATORTUJUAN." = ".TBL_SAKIP_MBID_PROGRAM_SASARAN.".".COL_KD_INDIKATORTUJUAN,"inner");
        $this->db->join(TBL_SAKIP_MPMD_SASARAN,TBL_SAKIP_MPMD_SASARAN.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_MBID_PROGRAM_SASARAN.".".COL_KD_PEMDA." AND ".TBL_SAKIP_MPMD_SASARAN.'.'.COL_KD_MISI." = ".TBL_SAKIP_MBID_PROGRAM_SASARAN.".".COL_KD_MISI." AND ".TBL_SAKIP_MPMD_SASARAN.'.'.COL_KD_TUJUAN." = ".TBL_SAKIP_MBID_PROGRAM_SASARAN.".".COL_KD_TUJUAN." AND ".TBL_SAKIP_MPMD_SASARAN.'.'.COL_KD_INDIKATORTUJUAN." = ".TBL_SAKIP_MBID_PROGRAM_SASARAN.".".COL_KD_INDIKATORTUJUAN." AND ".TBL_SAKIP_MPMD_SASARAN.'.'.COL_KD_SASARAN." = ".TBL_SAKIP_MBID_PROGRAM_SASARAN.".".COL_KD_SASARAN,"inner");
        $this->db->join(TBL_SAKIP_MPMD_IKSASARAN,TBL_SAKIP_MPMD_IKSASARAN.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_MBID_PROGRAM_SASARAN.".".COL_KD_PEMDA." AND ".TBL_SAKIP_MPMD_IKSASARAN.'.'.COL_KD_MISI." = ".TBL_SAKIP_MBID_PROGRAM_SASARAN.".".COL_KD_MISI." AND ".TBL_SAKIP_MPMD_IKSASARAN.'.'.COL_KD_TUJUAN." = ".TBL_SAKIP_MBID_PROGRAM_SASARAN.".".COL_KD_TUJUAN." AND ".TBL_SAKIP_MPMD_IKSASARAN.'.'.COL_KD_INDIKATORTUJUAN." = ".TBL_SAKIP_MBID_PROGRAM_SASARAN.".".COL_KD_INDIKATORTUJUAN." AND ".TBL_SAKIP_MPMD_IKSASARAN.'.'.COL_KD_SASARAN." = ".TBL_SAKIP_MBID_PROGRAM_SASARAN.".".COL_KD_SASARAN." AND ".TBL_SAKIP_MPMD_IKSASARAN.'.'.COL_KD_INDIKATORSASARAN." = ".TBL_SAKIP_MBID_PROGRAM_SASARAN.".".COL_KD_INDIKATORSASARAN,"inner");
        $this->db->join(TBL_SAKIP_MOPD_TUJUAN,
            TBL_SAKIP_MOPD_TUJUAN.'.'.COL_KD_URUSAN." = ".TBL_SAKIP_MBID_PROGRAM_SASARAN.".".COL_KD_URUSAN." AND ".
            TBL_SAKIP_MOPD_TUJUAN.'.'.COL_KD_BIDANG." = ".TBL_SAKIP_MBID_PROGRAM_SASARAN.".".COL_KD_BIDANG." AND ".
            TBL_SAKIP_MOPD_TUJUAN.'.'.COL_KD_UNIT." = ".TBL_SAKIP_MBID_PROGRAM_SASARAN.".".COL_KD_UNIT." AND ".
            TBL_SAKIP_MOPD_TUJUAN.'.'.COL_KD_SUB." = ".TBL_SAKIP_MBID_PROGRAM_SASARAN.".".COL_KD_SUB." AND ".

            TBL_SAKIP_MOPD_TUJUAN.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_MBID_PROGRAM_SASARAN.".".COL_KD_PEMDA." AND ".
            TBL_SAKIP_MOPD_TUJUAN.'.'.COL_KD_MISI." = ".TBL_SAKIP_MBID_PROGRAM_SASARAN.".".COL_KD_MISI." AND ".
            TBL_SAKIP_MOPD_TUJUAN.'.'.COL_KD_TUJUAN." = ".TBL_SAKIP_MBID_PROGRAM_SASARAN.".".COL_KD_TUJUAN." AND ".
            TBL_SAKIP_MOPD_TUJUAN.'.'.COL_KD_INDIKATORTUJUAN." = ".TBL_SAKIP_MBID_PROGRAM_SASARAN.".".COL_KD_INDIKATORTUJUAN." AND ".
            TBL_SAKIP_MOPD_TUJUAN.'.'.COL_KD_SASARAN." = ".TBL_SAKIP_MBID_PROGRAM_SASARAN.".".COL_KD_SASARAN." AND ".
            TBL_SAKIP_MOPD_TUJUAN.'.'.COL_KD_INDIKATORSASARAN." = ".TBL_SAKIP_MBID_PROGRAM_SASARAN.".".COL_KD_INDIKATORSASARAN. " AND ".
            TBL_SAKIP_MOPD_TUJUAN.'.'.COL_KD_TUJUANOPD." = ".TBL_SAKIP_MBID_PROGRAM_SASARAN.".".COL_KD_TUJUANOPD
            ,"inner");
        $this->db->join(TBL_SAKIP_MOPD_IKTUJUAN,
            TBL_SAKIP_MOPD_IKTUJUAN.'.'.COL_KD_URUSAN." = ".TBL_SAKIP_MBID_PROGRAM_SASARAN.".".COL_KD_URUSAN." AND ".
            TBL_SAKIP_MOPD_IKTUJUAN.'.'.COL_KD_BIDANG." = ".TBL_SAKIP_MBID_PROGRAM_SASARAN.".".COL_KD_BIDANG." AND ".
            TBL_SAKIP_MOPD_IKTUJUAN.'.'.COL_KD_UNIT." = ".TBL_SAKIP_MBID_PROGRAM_SASARAN.".".COL_KD_UNIT." AND ".
            TBL_SAKIP_MOPD_IKTUJUAN.'.'.COL_KD_SUB." = ".TBL_SAKIP_MBID_PROGRAM_SASARAN.".".COL_KD_SUB." AND ".

            TBL_SAKIP_MOPD_IKTUJUAN.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_MBID_PROGRAM_SASARAN.".".COL_KD_PEMDA." AND ".
            TBL_SAKIP_MOPD_IKTUJUAN.'.'.COL_KD_MISI." = ".TBL_SAKIP_MBID_PROGRAM_SASARAN.".".COL_KD_MISI." AND ".
            TBL_SAKIP_MOPD_IKTUJUAN.'.'.COL_KD_TUJUAN." = ".TBL_SAKIP_MBID_PROGRAM_SASARAN.".".COL_KD_TUJUAN." AND ".
            TBL_SAKIP_MOPD_IKTUJUAN.'.'.COL_KD_INDIKATORTUJUAN." = ".TBL_SAKIP_MBID_PROGRAM_SASARAN.".".COL_KD_INDIKATORTUJUAN." AND ".
            TBL_SAKIP_MOPD_IKTUJUAN.'.'.COL_KD_SASARAN." = ".TBL_SAKIP_MBID_PROGRAM_SASARAN.".".COL_KD_SASARAN." AND ".
            TBL_SAKIP_MOPD_IKTUJUAN.'.'.COL_KD_INDIKATORSASARAN." = ".TBL_SAKIP_MBID_PROGRAM_SASARAN.".".COL_KD_INDIKATORSASARAN." AND ".
            TBL_SAKIP_MOPD_IKTUJUAN.'.'.COL_KD_TUJUANOPD." = ".TBL_SAKIP_MBID_PROGRAM_SASARAN.".".COL_KD_TUJUANOPD." AND ".
            TBL_SAKIP_MOPD_IKTUJUAN.'.'.COL_KD_INDIKATORTUJUANOPD." = ".TBL_SAKIP_MBID_PROGRAM_SASARAN.".".COL_KD_INDIKATORTUJUANOPD
            ,"inner");
        $this->db->join(TBL_SAKIP_MOPD_SASARAN,
            TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_URUSAN." = ".TBL_SAKIP_MBID_PROGRAM_SASARAN.".".COL_KD_URUSAN." AND ".
            TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_BIDANG." = ".TBL_SAKIP_MBID_PROGRAM_SASARAN.".".COL_KD_BIDANG." AND ".
            TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_UNIT." = ".TBL_SAKIP_MBID_PROGRAM_SASARAN.".".COL_KD_UNIT." AND ".
            TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_SUB." = ".TBL_SAKIP_MBID_PROGRAM_SASARAN.".".COL_KD_SUB." AND ".

            TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_MBID_PROGRAM_SASARAN.".".COL_KD_PEMDA." AND ".
            TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_MISI." = ".TBL_SAKIP_MBID_PROGRAM_SASARAN.".".COL_KD_MISI." AND ".
            TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_TUJUAN." = ".TBL_SAKIP_MBID_PROGRAM_SASARAN.".".COL_KD_TUJUAN." AND ".
            TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_INDIKATORTUJUAN." = ".TBL_SAKIP_MBID_PROGRAM_SASARAN.".".COL_KD_INDIKATORTUJUAN." AND ".
            TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_SASARAN." = ".TBL_SAKIP_MBID_PROGRAM_SASARAN.".".COL_KD_SASARAN." AND ".
            TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_INDIKATORSASARAN." = ".TBL_SAKIP_MBID_PROGRAM_SASARAN.".".COL_KD_INDIKATORSASARAN." AND ".
            TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_TUJUANOPD." = ".TBL_SAKIP_MBID_PROGRAM_SASARAN.".".COL_KD_TUJUANOPD." AND ".
            TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_INDIKATORTUJUANOPD." = ".TBL_SAKIP_MBID_PROGRAM_SASARAN.".".COL_KD_INDIKATORTUJUANOPD." AND ".
            TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_SASARANOPD." = ".TBL_SAKIP_MBID_PROGRAM_SASARAN.".".COL_KD_SASARANOPD
            ,"inner");
        $this->db->join(TBL_SAKIP_MOPD_IKSASARAN,
            TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_URUSAN." = ".TBL_SAKIP_MBID_PROGRAM_SASARAN.".".COL_KD_URUSAN." AND ".
            TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_BIDANG." = ".TBL_SAKIP_MBID_PROGRAM_SASARAN.".".COL_KD_BIDANG." AND ".
            TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_UNIT." = ".TBL_SAKIP_MBID_PROGRAM_SASARAN.".".COL_KD_UNIT." AND ".
            TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_SUB." = ".TBL_SAKIP_MBID_PROGRAM_SASARAN.".".COL_KD_SUB." AND ".

            TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_MBID_PROGRAM_SASARAN.".".COL_KD_PEMDA." AND ".
            TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_MISI." = ".TBL_SAKIP_MBID_PROGRAM_SASARAN.".".COL_KD_MISI." AND ".
            TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_TUJUAN." = ".TBL_SAKIP_MBID_PROGRAM_SASARAN.".".COL_KD_TUJUAN." AND ".
            TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_INDIKATORTUJUAN." = ".TBL_SAKIP_MBID_PROGRAM_SASARAN.".".COL_KD_INDIKATORTUJUAN." AND ".
            TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_SASARAN." = ".TBL_SAKIP_MBID_PROGRAM_SASARAN.".".COL_KD_SASARAN." AND ".
            TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_INDIKATORSASARAN." = ".TBL_SAKIP_MBID_PROGRAM_SASARAN.".".COL_KD_INDIKATORSASARAN." AND ".
            TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_TUJUANOPD." = ".TBL_SAKIP_MBID_PROGRAM_SASARAN.".".COL_KD_TUJUANOPD." AND ".
            TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_INDIKATORTUJUANOPD." = ".TBL_SAKIP_MBID_PROGRAM_SASARAN.".".COL_KD_INDIKATORTUJUANOPD." AND ".
            TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_SASARANOPD." = ".TBL_SAKIP_MBID_PROGRAM_SASARAN.".".COL_KD_SASARANOPD." AND ".
            TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_INDIKATORSASARANOPD." = ".TBL_SAKIP_MBID_PROGRAM_SASARAN.".".COL_KD_INDIKATORSASARANOPD
            ,"inner");
        $this->db->join(TBL_SAKIP_MBID_PROGRAM,
            TBL_SAKIP_MBID_PROGRAM.'.'.COL_KD_URUSAN." = ".TBL_SAKIP_MBID_PROGRAM_SASARAN.".".COL_KD_URUSAN." AND ".
            TBL_SAKIP_MBID_PROGRAM.'.'.COL_KD_BIDANG." = ".TBL_SAKIP_MBID_PROGRAM_SASARAN.".".COL_KD_BIDANG." AND ".
            TBL_SAKIP_MBID_PROGRAM.'.'.COL_KD_UNIT." = ".TBL_SAKIP_MBID_PROGRAM_SASARAN.".".COL_KD_UNIT." AND ".
            TBL_SAKIP_MBID_PROGRAM.'.'.COL_KD_SUB." = ".TBL_SAKIP_MBID_PROGRAM_SASARAN.".".COL_KD_SUB." AND ".
            TBL_SAKIP_MBID_PROGRAM.'.'.COL_KD_BID." = ".TBL_SAKIP_MBID_PROGRAM_SASARAN.".".COL_KD_BID." AND ".
            TBL_SAKIP_MBID_PROGRAM.'.'.COL_KD_TAHUN." = ".TBL_SAKIP_MBID_PROGRAM_SASARAN.".".COL_KD_TAHUN." AND ".

            TBL_SAKIP_MBID_PROGRAM.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_MBID_PROGRAM_SASARAN.".".COL_KD_PEMDA." AND ".
            TBL_SAKIP_MBID_PROGRAM.'.'.COL_KD_MISI." = ".TBL_SAKIP_MBID_PROGRAM_SASARAN.".".COL_KD_MISI." AND ".
            TBL_SAKIP_MBID_PROGRAM.'.'.COL_KD_TUJUAN." = ".TBL_SAKIP_MBID_PROGRAM_SASARAN.".".COL_KD_TUJUAN." AND ".
            TBL_SAKIP_MBID_PROGRAM.'.'.COL_KD_INDIKATORTUJUAN." = ".TBL_SAKIP_MBID_PROGRAM_SASARAN.".".COL_KD_INDIKATORTUJUAN." AND ".
            TBL_SAKIP_MBID_PROGRAM.'.'.COL_KD_SASARAN." = ".TBL_SAKIP_MBID_PROGRAM_SASARAN.".".COL_KD_SASARAN." AND ".
            TBL_SAKIP_MBID_PROGRAM.'.'.COL_KD_INDIKATORSASARAN." = ".TBL_SAKIP_MBID_PROGRAM_SASARAN.".".COL_KD_INDIKATORSASARAN." AND ".
            TBL_SAKIP_MBID_PROGRAM.'.'.COL_KD_TUJUANOPD." = ".TBL_SAKIP_MBID_PROGRAM_SASARAN.".".COL_KD_TUJUANOPD." AND ".
            TBL_SAKIP_MBID_PROGRAM.'.'.COL_KD_INDIKATORTUJUANOPD." = ".TBL_SAKIP_MBID_PROGRAM_SASARAN.".".COL_KD_INDIKATORTUJUANOPD." AND ".
            TBL_SAKIP_MBID_PROGRAM.'.'.COL_KD_SASARANOPD." = ".TBL_SAKIP_MBID_PROGRAM_SASARAN.".".COL_KD_SASARANOPD." AND ".
            TBL_SAKIP_MBID_PROGRAM.'.'.COL_KD_INDIKATORSASARANOPD." = ".TBL_SAKIP_MBID_PROGRAM_SASARAN.".".COL_KD_INDIKATORSASARANOPD." AND ".
            TBL_SAKIP_MBID_PROGRAM.'.'.COL_KD_PROGRAMOPD." = ".TBL_SAKIP_MBID_PROGRAM_SASARAN.".".COL_KD_PROGRAMOPD

            ,"inner");
        $this->db->join(TBL_SAKIP_MBID,
            TBL_SAKIP_MBID.'.'.COL_KD_URUSAN." = ".TBL_SAKIP_MBID_PROGRAM_SASARAN.".".COL_KD_URUSAN." AND ".
            TBL_SAKIP_MBID.'.'.COL_KD_BIDANG." = ".TBL_SAKIP_MBID_PROGRAM_SASARAN.".".COL_KD_BIDANG." AND ".
            TBL_SAKIP_MBID.'.'.COL_KD_UNIT." = ".TBL_SAKIP_MBID_PROGRAM_SASARAN.".".COL_KD_UNIT." AND ".
            TBL_SAKIP_MBID.'.'.COL_KD_SUB." = ".TBL_SAKIP_MBID_PROGRAM_SASARAN.".".COL_KD_SUB." AND ".
            TBL_SAKIP_MBID.'.'.COL_KD_BID." = ".TBL_SAKIP_MBID_PROGRAM_SASARAN.".".COL_KD_BID
            ,"inner");
        $this->db->where(TBL_SAKIP_MPEMDA.".".COL_KD_TAHUN_FROM." <=", date("Y"));
        $this->db->where(TBL_SAKIP_MPEMDA.".".COL_KD_TAHUN_TO." >=", date("Y"));
        if($ruser[COL_ROLEID] != ROLEADMIN && $ruser[COL_ROLEID] != ROLEBAPPEDA) {
            $strOPD = explode('.', $ruser[COL_COMPANYID]);
            $this->db->where(TBL_SAKIP_MBID_PROGRAM_SASARAN.".".COL_KD_URUSAN, $strOPD[0]);
            $this->db->where(TBL_SAKIP_MBID_PROGRAM_SASARAN.".".COL_KD_BIDANG, $strOPD[1]);
            $this->db->where(TBL_SAKIP_MBID_PROGRAM_SASARAN.".".COL_KD_UNIT, $strOPD[2]);
            $this->db->where(TBL_SAKIP_MBID_PROGRAM_SASARAN.".".COL_KD_SUB, $strOPD[3]);
            if($ruser[COL_ROLEID] == ROLEKABID) $this->db->where(TBL_SAKIP_MBID_PROGRAM_SASARAN.".".COL_KD_BID, $strOPD[4]);
        }
        $this->db->order_by(TBL_SAKIP_MBID_PROGRAM_SASARAN.".".COL_KD_TAHUN, 'desc');
        $this->db->order_by(TBL_SAKIP_MBID_PROGRAM_SASARAN.".".COL_KD_URUSAN, 'asc');
        $this->db->order_by(TBL_SAKIP_MBID_PROGRAM_SASARAN.".".COL_KD_BIDANG, 'asc');
        $this->db->order_by(TBL_SAKIP_MBID_PROGRAM_SASARAN.".".COL_KD_UNIT, 'asc');
        $this->db->order_by(TBL_SAKIP_MBID_PROGRAM_SASARAN.".".COL_KD_SUB, 'asc');
        $this->db->order_by(TBL_SAKIP_MBID_PROGRAM_SASARAN.".".COL_KD_BID, 'asc');
        $this->db->order_by(TBL_SAKIP_MPEMDA.".".COL_KD_TAHUN_FROM, 'desc');
        $this->db->order_by(TBL_SAKIP_MBID_PROGRAM_SASARAN.".".COL_KD_MISI, 'asc');
        $this->db->order_by(TBL_SAKIP_MBID_PROGRAM_SASARAN.".".COL_KD_TUJUAN, 'asc');
        $this->db->order_by(TBL_SAKIP_MBID_PROGRAM_SASARAN.".".COL_KD_INDIKATORTUJUAN, 'asc');
        $this->db->order_by(TBL_SAKIP_MBID_PROGRAM_SASARAN.".".COL_KD_SASARAN, 'asc');
        $this->db->order_by(TBL_SAKIP_MBID_PROGRAM_SASARAN.".".COL_KD_INDIKATORSASARAN, 'asc');
        $this->db->order_by(TBL_SAKIP_MBID_PROGRAM_SASARAN.".".COL_KD_TUJUANOPD, 'asc');
        $this->db->order_by(TBL_SAKIP_MBID_PROGRAM_SASARAN.".".COL_KD_INDIKATORTUJUANOPD, 'asc');
        $this->db->order_by(TBL_SAKIP_MBID_PROGRAM_SASARAN.".".COL_KD_SASARANOPD, 'asc');
        $this->db->order_by(TBL_SAKIP_MBID_PROGRAM_SASARAN.".".COL_KD_INDIKATORSASARANOPD, 'asc');
        $this->db->order_by(TBL_SAKIP_MBID_PROGRAM_SASARAN.".".COL_KD_PROGRAMOPD, 'asc');
        $this->db->order_by(TBL_SAKIP_MBID_PROGRAM_SASARAN.".".COL_KD_SASARANPROGRAMOPD, 'asc');
        $data['res'] = $this->db->get(TBL_SAKIP_MBID_PROGRAM_SASARAN)->result_array();
        $this->load->view('mbid/sasaran_program', $data);
    }

    function sasaran_program_add() {
        $data['title'] = "Sasaran Program OPD";
        $data['edit'] = FALSE;

        if(!empty($_POST)){
            $ruser = GetLoggedUser();
            $resp = array();
            $resp['error'] = 0;
            $resp['success'] = 1;
            $resp['redirect'] = site_url('mbid/sasaran-program');
            $data = array(
                COL_KD_TAHUN => $this->input->post(COL_KD_TAHUN),
                COL_KD_URUSAN => $this->input->post(COL_KD_URUSAN),
                COL_KD_BIDANG => $this->input->post(COL_KD_BIDANG),
                COL_KD_UNIT => $this->input->post(COL_KD_UNIT),
                COL_KD_SUB => $this->input->post(COL_KD_SUB),
                COL_KD_BID => $this->input->post(COL_KD_BID),

                COL_KD_PEMDA => $this->input->post(COL_KD_PEMDA),
                COL_KD_MISI => $this->input->post(COL_KD_MISI),
                COL_KD_TUJUAN => $this->input->post(COL_KD_TUJUAN),
                COL_KD_INDIKATORTUJUAN => $this->input->post(COL_KD_INDIKATORTUJUAN),
                COL_KD_SASARAN => $this->input->post(COL_KD_SASARAN),
                COL_KD_INDIKATORSASARAN => $this->input->post(COL_KD_INDIKATORSASARAN),
                COL_KD_TUJUANOPD => $this->input->post(COL_KD_TUJUANOPD),
                COL_KD_INDIKATORTUJUANOPD => $this->input->post(COL_KD_INDIKATORTUJUANOPD),
                COL_KD_SASARANOPD => $this->input->post(COL_KD_SASARANOPD),
                COL_KD_INDIKATORSASARANOPD => $this->input->post(COL_KD_INDIKATORSASARANOPD),
                COL_KD_PROGRAMOPD => $this->input->post(COL_KD_PROGRAMOPD),
                COL_KD_SASARANPROGRAMOPD => $this->input->post(COL_KD_SASARANPROGRAMOPD),
                COL_NM_SASARANPROGRAMOPD => $this->input->post(COL_NM_SASARANPROGRAMOPD),
                COL_KD_SATUAN => $this->input->post(COL_KD_SATUAN),
                COL_AWAL => !empty($this->input->post(COL_AWAL))?$this->input->post(COL_AWAL):0,
                COL_TARGET => $this->input->post(COL_TARGET),
                COL_AKHIR => !empty($this->input->post(COL_AKHIR))?$this->input->post(COL_AKHIR):0,
            );

            $this->db->trans_begin();
            try {
                if(!$this->db->insert(TBL_SAKIP_MBID_PROGRAM_SASARAN, $data)){
                    throw new Exception("Database error: ".$this->db->error());
                }
                $detNo = $this->input->post("NoDet");
                $detDesc = $this->input->post("KetDet");
                $detFormula = $this->input->post("FormulaDet");
                $detSumber = $this->input->post("SumberDet");
                $detPIC = $this->input->post("PICDet");
                $detSatuan = $this->input->post("SatuanDet");
                $detTarget = $this->input->post("TargetDet");
                $detAwal = $this->input->post("AwalDet");
                $detAkhir = $this->input->post("AkhirDet");
                $det = [];
                for($i = 0; $i<count($detNo); $i++) {
                    $det[] = array(
                        COL_KD_TAHUN => $this->input->post(COL_KD_TAHUN),
                        COL_KD_URUSAN => $this->input->post(COL_KD_URUSAN),
                        COL_KD_BIDANG => $this->input->post(COL_KD_BIDANG),
                        COL_KD_UNIT => $this->input->post(COL_KD_UNIT),
                        COL_KD_SUB => $this->input->post(COL_KD_SUB),
                        COL_KD_BID => $this->input->post(COL_KD_BID),

                        COL_KD_PEMDA => $this->input->post(COL_KD_PEMDA),
                        COL_KD_MISI => $this->input->post(COL_KD_MISI),
                        COL_KD_TUJUAN => $this->input->post(COL_KD_TUJUAN),
                        COL_KD_INDIKATORTUJUAN => $this->input->post(COL_KD_INDIKATORTUJUAN),
                        COL_KD_SASARAN => $this->input->post(COL_KD_SASARAN),
                        COL_KD_INDIKATORSASARAN => $this->input->post(COL_KD_INDIKATORSASARAN),
                        COL_KD_TUJUANOPD => $this->input->post(COL_KD_TUJUANOPD),
                        COL_KD_INDIKATORTUJUANOPD => $this->input->post(COL_KD_INDIKATORTUJUANOPD),
                        COL_KD_SASARANOPD => $this->input->post(COL_KD_SASARANOPD),
                        COL_KD_INDIKATORSASARANOPD => $this->input->post(COL_KD_INDIKATORSASARANOPD),
                        COL_KD_PROGRAMOPD => $this->input->post(COL_KD_PROGRAMOPD),
                        COL_KD_SASARANPROGRAMOPD => $this->input->post(COL_KD_SASARANPROGRAMOPD),
                        COL_KD_INDIKATORPROGRAMOPD => $detNo[$i],
                        COL_NM_INDIKATORPROGRAMOPD => $detDesc[$i],
                        COL_NM_FORMULA => $detFormula[$i],
                        COL_NM_SUMBERDATA => $detSumber[$i],
                        COL_NM_PENANGGUNGJAWAB => $detPIC[$i],
                        COL_KD_SATUAN => $detSatuan[$i],
                        COL_TARGET => $detTarget[$i],
                        COL_AWAL => $detAwal[$i],
                        COL_AKHIR => $detAkhir[$i]
                    );
                }
                if(count($det) > 0) {
                    $res = $this->db->insert_batch(TBL_SAKIP_MBID_PROGRAM_INDIKATOR, $det);
                    if(!$res) {
                        throw new Exception("Database error: ".$this->db->error());
                    }
                }

                $this->db->trans_commit();
                echo json_encode($resp);
            } catch (Exception $e) {
                $this->db->trans_rollback();
                $resp['error'] = $e->getMessage();
                $resp['success'] = 0;
                echo json_encode($resp);
                return;
            }
        }else{
            $this->db->where(COL_KD_TAHUN_FROM." <=", date("Y"));
            $this->db->where(COL_KD_TAHUN_TO." >=", date("Y"));
            $this->db->order_by(COL_KD_TAHUN_FROM, 'desc');
            $rpemda = $this->db->get(TBL_SAKIP_MPEMDA)->row_array();

            if(!empty($rpemda)) {
                $data['data'] = array(
                    COL_KD_PEMDA => $rpemda[COL_KD_PEMDA],
                    "DefPeriod" => $rpemda[COL_KD_TAHUN_FROM]." s.d ".$rpemda[COL_KD_TAHUN_TO]." : ".$rpemda[COL_NM_PEJABAT]
                );
            }

            $this->load->view('mbid/sasaran_program_form',$data);
        }
    }

    function sasaran_program_edit($id) {
        $ruser = GetLoggedUser();
        $this->db
            ->select(TBL_SAKIP_MBID_PROGRAM_SASARAN.'.*,'.
                TBL_SAKIP_MPMD_MISI.'.'.COL_NM_MISI.','.
                TBL_SAKIP_MPEMDA.'.'.COL_KD_TAHUN_FROM.','.
                TBL_SAKIP_MPEMDA.'.'.COL_KD_TAHUN_TO.','.
                TBL_SAKIP_MPEMDA.'.'.COL_NM_PEJABAT.', '.
                TBL_SAKIP_MPMD_TUJUAN.'.'.COL_NM_TUJUAN.', '.
                TBL_SAKIP_MPMD_IKTUJUAN.'.'.COL_NM_INDIKATORTUJUAN.', '.
                TBL_SAKIP_MPMD_SASARAN.'.'.COL_NM_SASARAN.', '.
                TBL_SAKIP_MPMD_IKSASARAN.'.'.COL_NM_INDIKATORSASARAN.', '.
                TBL_SAKIP_MOPD_TUJUAN.'.'.COL_NM_TUJUANOPD.', '.
                TBL_SAKIP_MOPD_IKTUJUAN.'.'.COL_NM_INDIKATORTUJUANOPD.', '.
                TBL_SAKIP_MOPD_SASARAN.'.'.COL_NM_SASARANOPD.', '.
                TBL_SAKIP_MBID.'.'.COL_NM_BID.','.
                TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_NM_INDIKATORSASARANOPD.','.
                TBL_SAKIP_MBID_PROGRAM.'.'.COL_NM_PROGRAMOPD
            )
           // ->select(TBL_SAKIP_MBID_PROGRAM_SASARAN.'.*,'.TBL_SAKIP_MBID.'.'.COL_NM_BID.','.TBL_SAKIP_MBID_PROGRAM.'.'.COL_ISEPLAN.','.TBL_SAKIP_MBID_PROGRAM.'.'.COL_NM_PROGRAMOPD.','.TBL_SAKIP_MPEMDA.'.'.COL_KD_TAHUN_FROM.','.TBL_SAKIP_MPEMDA.'.'.COL_KD_TAHUN_TO)
            ->join(TBL_SAKIP_MPEMDA,TBL_SAKIP_MPEMDA.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_MBID_PROGRAM_SASARAN.".".COL_KD_PEMDA,"inner")
            ->join(TBL_SAKIP_MBID,
                TBL_SAKIP_MBID.'.'.COL_KD_URUSAN." = ".TBL_SAKIP_MBID_PROGRAM_SASARAN.".".COL_KD_URUSAN." AND ".
                TBL_SAKIP_MBID.'.'.COL_KD_BIDANG." = ".TBL_SAKIP_MBID_PROGRAM_SASARAN.".".COL_KD_BIDANG." AND ".
                TBL_SAKIP_MBID.'.'.COL_KD_UNIT." = ".TBL_SAKIP_MBID_PROGRAM_SASARAN.".".COL_KD_UNIT." AND ".
                TBL_SAKIP_MBID.'.'.COL_KD_SUB." = ".TBL_SAKIP_MBID_PROGRAM_SASARAN.".".COL_KD_SUB." AND ".
                TBL_SAKIP_MBID.'.'.COL_KD_BID." = ".TBL_SAKIP_MBID_PROGRAM_SASARAN.".".COL_KD_BID
                ,"inner")
            ->join(TBL_SAKIP_MBID_PROGRAM,
                TBL_SAKIP_MBID_PROGRAM.'.'.COL_KD_URUSAN." = ".TBL_SAKIP_MBID_PROGRAM_SASARAN.".".COL_KD_URUSAN." AND ".
                TBL_SAKIP_MBID_PROGRAM.'.'.COL_KD_BIDANG." = ".TBL_SAKIP_MBID_PROGRAM_SASARAN.".".COL_KD_BIDANG." AND ".
                TBL_SAKIP_MBID_PROGRAM.'.'.COL_KD_UNIT." = ".TBL_SAKIP_MBID_PROGRAM_SASARAN.".".COL_KD_UNIT." AND ".
                TBL_SAKIP_MBID_PROGRAM.'.'.COL_KD_SUB." = ".TBL_SAKIP_MBID_PROGRAM_SASARAN.".".COL_KD_SUB." AND ".
                TBL_SAKIP_MBID_PROGRAM.'.'.COL_KD_BID." = ".TBL_SAKIP_MBID_PROGRAM_SASARAN.".".COL_KD_BID." AND ".

            TBL_SAKIP_MBID_PROGRAM.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_MBID_PROGRAM_SASARAN.".".COL_KD_PEMDA." AND ".
            TBL_SAKIP_MBID_PROGRAM.'.'.COL_KD_MISI." = ".TBL_SAKIP_MBID_PROGRAM_SASARAN.".".COL_KD_MISI." AND ".
            TBL_SAKIP_MBID_PROGRAM.'.'.COL_KD_TUJUAN." = ".TBL_SAKIP_MBID_PROGRAM_SASARAN.".".COL_KD_TUJUAN." AND ".
            TBL_SAKIP_MBID_PROGRAM.'.'.COL_KD_INDIKATORTUJUAN." = ".TBL_SAKIP_MBID_PROGRAM_SASARAN.".".COL_KD_INDIKATORTUJUAN." AND ".
            TBL_SAKIP_MBID_PROGRAM.'.'.COL_KD_SASARAN." = ".TBL_SAKIP_MBID_PROGRAM_SASARAN.".".COL_KD_SASARAN." AND ".
            TBL_SAKIP_MBID_PROGRAM.'.'.COL_KD_INDIKATORSASARAN." = ".TBL_SAKIP_MBID_PROGRAM_SASARAN.".".COL_KD_INDIKATORSASARAN." AND ".
            TBL_SAKIP_MBID_PROGRAM.'.'.COL_KD_TUJUANOPD." = ".TBL_SAKIP_MBID_PROGRAM_SASARAN.".".COL_KD_TUJUANOPD." AND ".
            TBL_SAKIP_MBID_PROGRAM.'.'.COL_KD_INDIKATORTUJUANOPD." = ".TBL_SAKIP_MBID_PROGRAM_SASARAN.".".COL_KD_INDIKATORTUJUANOPD." AND ".
            TBL_SAKIP_MBID_PROGRAM.'.'.COL_KD_SASARANOPD." = ".TBL_SAKIP_MBID_PROGRAM_SASARAN.".".COL_KD_SASARANOPD." AND ".
            TBL_SAKIP_MBID_PROGRAM.'.'.COL_KD_INDIKATORSASARANOPD." = ".TBL_SAKIP_MBID_PROGRAM_SASARAN.".".COL_KD_INDIKATORSASARANOPD." AND ".
            TBL_SAKIP_MBID_PROGRAM.'.'.COL_KD_PROGRAMOPD." = ".TBL_SAKIP_MBID_PROGRAM_SASARAN.".".COL_KD_PROGRAMOPD." AND ".
            TBL_SAKIP_MBID_PROGRAM.'.'.COL_KD_TAHUN." = ".TBL_SAKIP_MBID_PROGRAM_SASARAN.".".COL_KD_TAHUN
            ,"inner")
            ->join(TBL_SAKIP_MPMD_MISI,TBL_SAKIP_MPMD_MISI.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_PEMDA." AND ".TBL_SAKIP_MPMD_MISI.'.'.COL_KD_MISI." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_MISI,"inner")
            ->join(TBL_SAKIP_MPMD_TUJUAN,TBL_SAKIP_MPMD_TUJUAN.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_PEMDA." AND ".TBL_SAKIP_MPMD_TUJUAN.'.'.COL_KD_MISI." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_MISI." AND ".TBL_SAKIP_MPMD_TUJUAN.'.'.COL_KD_TUJUAN." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_TUJUAN,"inner")
            ->join(TBL_SAKIP_MPMD_IKTUJUAN,TBL_SAKIP_MPMD_IKTUJUAN.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_PEMDA." AND ".TBL_SAKIP_MPMD_IKTUJUAN.'.'.COL_KD_MISI." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_MISI." AND ".TBL_SAKIP_MPMD_IKTUJUAN.'.'.COL_KD_TUJUAN." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_TUJUAN." AND ".TBL_SAKIP_MPMD_IKTUJUAN.'.'.COL_KD_INDIKATORTUJUAN." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_INDIKATORTUJUAN,"inner")
            ->join(TBL_SAKIP_MPMD_SASARAN,TBL_SAKIP_MPMD_SASARAN.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_PEMDA." AND ".TBL_SAKIP_MPMD_SASARAN.'.'.COL_KD_MISI." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_MISI." AND ".TBL_SAKIP_MPMD_SASARAN.'.'.COL_KD_TUJUAN." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_TUJUAN." AND ".TBL_SAKIP_MPMD_SASARAN.'.'.COL_KD_INDIKATORTUJUAN." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_INDIKATORTUJUAN." AND ".TBL_SAKIP_MPMD_SASARAN.'.'.COL_KD_SASARAN." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_SASARAN,"inner")
            ->join(TBL_SAKIP_MPMD_IKSASARAN,TBL_SAKIP_MPMD_IKSASARAN.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_PEMDA." AND ".TBL_SAKIP_MPMD_IKSASARAN.'.'.COL_KD_MISI." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_MISI." AND ".TBL_SAKIP_MPMD_IKSASARAN.'.'.COL_KD_TUJUAN." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_TUJUAN." AND ".TBL_SAKIP_MPMD_IKSASARAN.'.'.COL_KD_INDIKATORTUJUAN." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_INDIKATORTUJUAN." AND ".TBL_SAKIP_MPMD_IKSASARAN.'.'.COL_KD_SASARAN." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_SASARAN." AND ".TBL_SAKIP_MPMD_IKSASARAN.'.'.COL_KD_INDIKATORSASARAN." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_INDIKATORSASARAN,"inner")
            ->join(TBL_SAKIP_MOPD_TUJUAN,
                TBL_SAKIP_MOPD_TUJUAN.'.'.COL_KD_URUSAN." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_URUSAN." AND ".
                TBL_SAKIP_MOPD_TUJUAN.'.'.COL_KD_BIDANG." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_BIDANG." AND ".
                TBL_SAKIP_MOPD_TUJUAN.'.'.COL_KD_UNIT." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_UNIT." AND ".
                TBL_SAKIP_MOPD_TUJUAN.'.'.COL_KD_SUB." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_SUB." AND ".

                TBL_SAKIP_MOPD_TUJUAN.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_PEMDA." AND ".
                TBL_SAKIP_MOPD_TUJUAN.'.'.COL_KD_MISI." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_MISI." AND ".
                TBL_SAKIP_MOPD_TUJUAN.'.'.COL_KD_TUJUAN." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_TUJUAN." AND ".
                TBL_SAKIP_MOPD_TUJUAN.'.'.COL_KD_INDIKATORTUJUAN." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_INDIKATORTUJUAN." AND ".
                TBL_SAKIP_MOPD_TUJUAN.'.'.COL_KD_SASARAN." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_SASARAN." AND ".
                TBL_SAKIP_MOPD_TUJUAN.'.'.COL_KD_INDIKATORSASARAN." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_INDIKATORSASARAN." AND ".
                TBL_SAKIP_MOPD_TUJUAN.'.'.COL_KD_TUJUANOPD." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_TUJUANOPD
                ,"inner")
            ->join(TBL_SAKIP_MOPD_IKTUJUAN,
                TBL_SAKIP_MOPD_IKTUJUAN.'.'.COL_KD_URUSAN." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_URUSAN." AND ".
                TBL_SAKIP_MOPD_IKTUJUAN.'.'.COL_KD_BIDANG." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_BIDANG." AND ".
                TBL_SAKIP_MOPD_IKTUJUAN.'.'.COL_KD_UNIT." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_UNIT." AND ".
                TBL_SAKIP_MOPD_IKTUJUAN.'.'.COL_KD_SUB." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_SUB." AND ".

                TBL_SAKIP_MOPD_IKTUJUAN.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_PEMDA." AND ".
                TBL_SAKIP_MOPD_IKTUJUAN.'.'.COL_KD_MISI." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_MISI." AND ".
                TBL_SAKIP_MOPD_IKTUJUAN.'.'.COL_KD_TUJUAN." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_TUJUAN." AND ".
                TBL_SAKIP_MOPD_IKTUJUAN.'.'.COL_KD_INDIKATORTUJUAN." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_INDIKATORTUJUAN." AND ".
                TBL_SAKIP_MOPD_IKTUJUAN.'.'.COL_KD_SASARAN." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_SASARAN." AND ".
                TBL_SAKIP_MOPD_IKTUJUAN.'.'.COL_KD_INDIKATORSASARAN." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_INDIKATORSASARAN." AND ".
                TBL_SAKIP_MOPD_IKTUJUAN.'.'.COL_KD_TUJUANOPD." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_TUJUANOPD." AND ".
                TBL_SAKIP_MOPD_IKTUJUAN.'.'.COL_KD_INDIKATORTUJUANOPD." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_INDIKATORTUJUANOPD
                ,"inner")
            ->join(TBL_SAKIP_MOPD_SASARAN,
                TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_URUSAN." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_URUSAN." AND ".
                TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_BIDANG." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_BIDANG." AND ".
                TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_UNIT." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_UNIT." AND ".
                TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_SUB." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_SUB." AND ".

                TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_PEMDA." AND ".
                TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_MISI." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_MISI." AND ".
                TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_TUJUAN." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_TUJUAN." AND ".
                TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_INDIKATORTUJUAN." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_INDIKATORTUJUAN." AND ".
                TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_SASARAN." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_SASARAN." AND ".
                TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_INDIKATORSASARAN." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_INDIKATORSASARAN." AND ".
                TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_TUJUANOPD." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_TUJUANOPD." AND ".
                TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_INDIKATORTUJUANOPD." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_INDIKATORTUJUANOPD." AND ".
                TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_SASARANOPD." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_SASARANOPD
                ,"inner")
            ->join(TBL_SAKIP_MOPD_IKSASARAN,
                TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_URUSAN." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_URUSAN." AND ".
                TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_BIDANG." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_BIDANG." AND ".
                TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_UNIT." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_UNIT." AND ".
                TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_SUB." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_SUB." AND ".

                TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_PEMDA." AND ".
                TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_MISI." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_MISI." AND ".
                TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_TUJUAN." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_TUJUAN." AND ".
                TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_INDIKATORTUJUAN." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_INDIKATORTUJUAN." AND ".
                TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_SASARAN." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_SASARAN." AND ".
                TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_INDIKATORSASARAN." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_INDIKATORSASARAN." AND ".
                TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_TUJUANOPD." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_TUJUANOPD." AND ".
                TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_INDIKATORTUJUANOPD." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_INDIKATORTUJUANOPD." AND ".
                TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_SASARANOPD." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_SASARANOPD." AND ".
                TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_INDIKATORSASARANOPD." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_INDIKATORSASARANOPD
                ,"inner")
            ->where(TBL_SAKIP_MBID_PROGRAM_SASARAN.".".COL_UNIQ, $id);
        if($ruser[COL_ROLEID] != ROLEADMIN && $ruser[COL_ROLEID] != ROLEBAPPEDA) {
            $strOPD = explode('.', $ruser[COL_COMPANYID]);
            $this->db->where(TBL_SAKIP_MBID_PROGRAM_SASARAN.".".COL_KD_URUSAN, $strOPD[0]);
            $this->db->where(TBL_SAKIP_MBID_PROGRAM_SASARAN.".".COL_KD_BIDANG, $strOPD[1]);
            $this->db->where(TBL_SAKIP_MBID_PROGRAM_SASARAN.".".COL_KD_UNIT, $strOPD[2]);
            $this->db->where(TBL_SAKIP_MBID_PROGRAM_SASARAN.".".COL_KD_SUB, $strOPD[3]);
            if($ruser[COL_ROLEID] == ROLEKABID) $this->db->where(TBL_SAKIP_MBID_PROGRAM_SASARAN.".".COL_KD_BID, $strOPD[4]);
        }
        $rdata = $data['data'] = $this->db->get(TBL_SAKIP_MBID_PROGRAM_SASARAN)->row_array();
        if(empty($rdata)){
            show_404();
            return;
        }

        $data['title'] = "Sasaran Program OPD";;
        $data['edit'] = TRUE;
        if(!empty($_POST)){
            $resp = array();
            $resp['error'] = 0;
            $resp['success'] = 1;
            $resp['redirect'] = site_url('mbid/sasaran_program');
            $data = array(
                COL_KD_TAHUN => $this->input->post(COL_KD_TAHUN),
                COL_KD_URUSAN => $this->input->post(COL_KD_URUSAN),
                COL_KD_BIDANG => $this->input->post(COL_KD_BIDANG),
                COL_KD_UNIT => $this->input->post(COL_KD_UNIT),
                COL_KD_SUB => $this->input->post(COL_KD_SUB),
                COL_KD_BID => $this->input->post(COL_KD_BID),

                COL_KD_PEMDA => $this->input->post(COL_KD_PEMDA),
                COL_KD_MISI => $this->input->post(COL_KD_MISI),
                COL_KD_TUJUAN => $this->input->post(COL_KD_TUJUAN),
                COL_KD_INDIKATORTUJUAN => $this->input->post(COL_KD_INDIKATORTUJUAN),
                COL_KD_SASARAN => $this->input->post(COL_KD_SASARAN),
                COL_KD_INDIKATORSASARAN => $this->input->post(COL_KD_INDIKATORSASARAN),
                COL_KD_TUJUANOPD => $this->input->post(COL_KD_TUJUANOPD),
                COL_KD_INDIKATORTUJUANOPD => $this->input->post(COL_KD_INDIKATORTUJUANOPD),
                COL_KD_SASARANOPD => $this->input->post(COL_KD_SASARANOPD),
                COL_KD_INDIKATORSASARANOPD => $this->input->post(COL_KD_INDIKATORSASARANOPD),
                COL_KD_PROGRAMOPD => $this->input->post(COL_KD_PROGRAMOPD),
                COL_KD_SASARANPROGRAMOPD => $this->input->post(COL_KD_SASARANPROGRAMOPD),
                COL_NM_SASARANPROGRAMOPD => $this->input->post(COL_NM_SASARANPROGRAMOPD),
                COL_KD_SATUAN => $this->input->post(COL_KD_SATUAN),
                COL_AWAL => !empty($this->input->post(COL_AWAL))?$this->input->post(COL_AWAL):0,
                COL_TARGET => $this->input->post(COL_TARGET),
                COL_AKHIR => !empty($this->input->post(COL_AKHIR))?$this->input->post(COL_AKHIR):0,
            );

            $this->db->trans_begin();
            try {
                if(!$this->db->where(COL_UNIQ, $id)->update(TBL_SAKIP_MBID_PROGRAM_SASARAN, $data)){
                    throw new Exception("Database error: ".$this->db->error());
                }

                $detNo = $this->input->post("NoDet");
                $detKet = $this->input->post("KetDet");
                $detFormula = $this->input->post("FormulaDet");
                $detSumber = $this->input->post("SumberDet");
                $detPIC = $this->input->post("PICDet");
                $detSatuan = $this->input->post("SatuanDet");
                $detTarget = $this->input->post("TargetDet");
                $detAwal = $this->input->post("AwalDet");
                $detAkhir = $this->input->post("AkhirDet");
                $arrDet = [];

                /* update / delete */
                $det = $this->db
                    //->select(COL_KD_PEMDA.",".COL_KD_MISI.",".COL_KD_TUJUAN.",".COL_KD_INDIKATORTUJUAN.",".COL_KD_SASARAN.",".COL_KD_INDIKATORSASARAN)
                    ->where(COL_KD_URUSAN, $rdata[COL_KD_URUSAN])
                    ->where(COL_KD_BIDANG, $rdata[COL_KD_BIDANG])
                    ->where(COL_KD_UNIT, $rdata[COL_KD_UNIT])
                    ->where(COL_KD_SUB, $rdata[COL_KD_SUB])
                    ->where(COL_KD_BID, $rdata[COL_KD_BID])

                    ->where(COL_KD_PEMDA, $rdata[COL_KD_PEMDA])
                    ->where(COL_KD_MISI, $rdata[COL_KD_MISI])
                    ->where(COL_KD_TUJUAN, $rdata[COL_KD_TUJUAN])
                    ->where(COL_KD_INDIKATORTUJUAN, $rdata[COL_KD_INDIKATORTUJUAN])
                    ->where(COL_KD_SASARAN, $rdata[COL_KD_SASARAN])
                    ->where(COL_KD_INDIKATORSASARAN, $rdata[COL_KD_INDIKATORSASARAN])
                    ->where(COL_KD_TUJUANOPD, $rdata[COL_KD_TUJUANOPD])
                    ->where(COL_KD_INDIKATORTUJUANOPD, $rdata[COL_KD_INDIKATORTUJUANOPD])
                    ->where(COL_KD_SASARANOPD, $rdata[COL_KD_SASARANOPD])
                    ->where(COL_KD_INDIKATORSASARANOPD, $rdata[COL_KD_INDIKATORSASARANOPD])
                    ->where(COL_KD_TAHUN, $rdata[COL_KD_TAHUN])
                    ->where(COL_KD_PROGRAMOPD, $rdata[COL_KD_PROGRAMOPD])
                    ->where(COL_KD_SASARANPROGRAMOPD, $rdata[COL_KD_SASARANPROGRAMOPD])
                    ->get(TBL_SAKIP_MBID_PROGRAM_INDIKATOR)
                    ->result_array();
                $detUpdated = [];
                foreach($det as $d) {
                    $arrCond = array(
                        COL_KD_URUSAN => $d[COL_KD_URUSAN],
                        COL_KD_BIDANG => $d[COL_KD_BIDANG],
                        COL_KD_UNIT => $d[COL_KD_UNIT],
                        COL_KD_SUB => $d[COL_KD_SUB],
                        COL_KD_BID => $d[COL_KD_BID],

                        COL_KD_PEMDA=>$d[COL_KD_PEMDA],
                        COL_KD_MISI=>$d[COL_KD_MISI],
                        COL_KD_TUJUAN=>$d[COL_KD_TUJUAN],
                        COL_KD_INDIKATORTUJUAN=>$d[COL_KD_INDIKATORTUJUAN],
                        COL_KD_SASARAN=>$d[COL_KD_SASARAN],
                        COL_KD_INDIKATORSASARAN=>$d[COL_KD_INDIKATORSASARAN],
                        COL_KD_TUJUANOPD=>$d[COL_KD_TUJUANOPD],
                        COL_KD_INDIKATORTUJUANOPD=>$d[COL_KD_INDIKATORTUJUANOPD],
                        COL_KD_SASARANOPD=>$d[COL_KD_SASARANOPD],
                        COL_KD_INDIKATORSASARANOPD=>$d[COL_KD_INDIKATORSASARANOPD],
                        COL_KD_TAHUN=>$d[COL_KD_TAHUN],
                        COL_KD_PROGRAMOPD=>$d[COL_KD_PROGRAMOPD],
                        COL_KD_SASARANPROGRAMOPD=>$d[COL_KD_SASARANPROGRAMOPD],
                        COL_KD_INDIKATORPROGRAMOPD=>$d[COL_KD_INDIKATORPROGRAMOPD]
                    );
                    if(!empty($detNo) && in_array($d[COL_KD_INDIKATORPROGRAMOPD], $detNo)) {
                        $res = $this->db
                            ->where($arrCond)
                            ->update(TBL_SAKIP_MBID_PROGRAM_INDIKATOR, array(
                                COL_KD_URUSAN => $this->input->post(COL_KD_URUSAN),
                                COL_KD_BIDANG => $this->input->post(COL_KD_BIDANG),
                                COL_KD_UNIT => $this->input->post(COL_KD_UNIT),
                                COL_KD_SUB => $this->input->post(COL_KD_SUB),
                                COL_KD_BID => $this->input->post(COL_KD_BID),

                                COL_KD_PEMDA => $this->input->post(COL_KD_PEMDA),
                                COL_KD_MISI => $this->input->post(COL_KD_MISI),
                                COL_KD_TUJUAN => $this->input->post(COL_KD_TUJUAN),
                                COL_KD_INDIKATORTUJUAN => $this->input->post(COL_KD_INDIKATORTUJUAN),
                                COL_KD_SASARAN => $this->input->post(COL_KD_SASARAN),
                                COL_KD_INDIKATORSASARAN => $this->input->post(COL_KD_INDIKATORSASARAN),
                                COL_KD_TUJUANOPD => $this->input->post(COL_KD_TUJUANOPD),
                                COL_KD_INDIKATORTUJUANOPD => $this->input->post(COL_KD_INDIKATORTUJUANOPD),
                                COL_KD_SASARANOPD => $this->input->post(COL_KD_SASARANOPD),
                                COL_KD_INDIKATORSASARANOPD => $this->input->post(COL_KD_INDIKATORSASARANOPD),
                                COL_KD_TAHUN => $this->input->post(COL_KD_TAHUN),
                                COL_KD_PROGRAMOPD => $this->input->post(COL_KD_PROGRAMOPD),
                                COL_KD_SASARANPROGRAMOPD => $this->input->post(COL_KD_SASARANPROGRAMOPD),

                                COL_KD_INDIKATORPROGRAMOPD => $d[COL_KD_INDIKATORPROGRAMOPD],
                                COL_NM_INDIKATORPROGRAMOPD => $detKet[array_search($d[COL_KD_INDIKATORPROGRAMOPD], $detNo)],
                                COL_NM_FORMULA => $detFormula[array_search($d[COL_KD_INDIKATORPROGRAMOPD], $detNo)],
                                COL_NM_SUMBERDATA => $detSumber[array_search($d[COL_KD_INDIKATORPROGRAMOPD], $detNo)],
                                COL_NM_PENANGGUNGJAWAB => $detPIC[array_search($d[COL_KD_INDIKATORPROGRAMOPD], $detNo)],
                                COL_KD_SATUAN => $detSatuan[array_search($d[COL_KD_INDIKATORPROGRAMOPD], $detNo)],
                                COL_TARGET => $detTarget[array_search($d[COL_KD_INDIKATORPROGRAMOPD], $detNo)],
                                COL_AWAL => $detAwal[array_search($d[COL_KD_INDIKATORPROGRAMOPD], $detNo)],
                                COL_AKHIR => $detAkhir[array_search($d[COL_KD_INDIKATORPROGRAMOPD], $detNo)]
                            ));
                        if(!$res) {
                            throw new Exception("Database error: ".$this->db->error());
                        }
                    }
                    else {
                        $res = $this->db->delete(TBL_SAKIP_MBID_PROGRAM_INDIKATOR, $arrCond);
                        if(!$res) {
                            throw new Exception("Database error: ".$this->db->error());
                        }
                    }
                    $detUpdated[] = $d[COL_KD_INDIKATORPROGRAMOPD];
                }
                /* update / delete */

                /* insert */
                for($i = 0; $i<count($detNo); $i++) {
                    if(!in_array($detNo[$i], $detUpdated)) {
                        $arrDet[] = array(
                            COL_KD_URUSAN => $this->input->post(COL_KD_URUSAN),
                            COL_KD_BIDANG => $this->input->post(COL_KD_BIDANG),
                            COL_KD_UNIT => $this->input->post(COL_KD_UNIT),
                            COL_KD_SUB => $this->input->post(COL_KD_SUB),
                            COL_KD_BID => $this->input->post(COL_KD_BID),

                            COL_KD_PEMDA => $this->input->post(COL_KD_PEMDA),
                            COL_KD_MISI => $this->input->post(COL_KD_MISI),
                            COL_KD_TUJUAN => $this->input->post(COL_KD_TUJUAN),
                            COL_KD_INDIKATORTUJUAN => $this->input->post(COL_KD_INDIKATORTUJUAN),
                            COL_KD_SASARAN => $this->input->post(COL_KD_SASARAN),
                            COL_KD_INDIKATORSASARAN => $this->input->post(COL_KD_INDIKATORSASARAN),
                            COL_KD_TUJUANOPD => $this->input->post(COL_KD_TUJUANOPD),
                            COL_KD_INDIKATORTUJUANOPD => $this->input->post(COL_KD_INDIKATORTUJUANOPD),
                            COL_KD_SASARANOPD => $this->input->post(COL_KD_SASARANOPD),
                            COL_KD_INDIKATORSASARANOPD => $this->input->post(COL_KD_INDIKATORSASARANOPD),
                            COL_KD_TAHUN => $this->input->post(COL_KD_TAHUN),
                            COL_KD_PROGRAMOPD => $this->input->post(COL_KD_PROGRAMOPD),
                            COL_KD_SASARANPROGRAMOPD => $this->input->post(COL_KD_SASARANPROGRAMOPD),

                            COL_KD_INDIKATORPROGRAMOPD => $detNo[$i],
                            COL_NM_INDIKATORPROGRAMOPD => $detKet[$i],
                            COL_NM_FORMULA => $detFormula[$i],
                            COL_NM_SUMBERDATA => $detSumber[$i],
                            COL_NM_PENANGGUNGJAWAB => $detPIC[$i],
                            COL_KD_SATUAN => $detSatuan[$i],
                            COL_TARGET => $detTarget[$i],
                            COL_AWAL => $detAwal[$i],
                            COL_AKHIR => $detAkhir[$i]
                        );
                    }
                }
                if(count($arrDet) > 0) {
                    $res = $this->db->insert_batch(TBL_SAKIP_MBID_PROGRAM_INDIKATOR, $arrDet);
                    if(!$res) {
                        throw new Exception("Database error: ".$this->db->error());
                    }
                }
                /* insert */
                $this->db->trans_commit();
                echo json_encode($resp);
            } catch (Exception $e) {
                $this->db->trans_rollback();
                $resp['error'] = $e->getMessage();
                $resp['success'] = 0;
                echo json_encode($resp);
                return;
            }
        }else{
            $this->load->view('mbid/sasaran_program_form',$data);
        }
    }

    function sasaran_program_del(){
        $data = $this->input->post('cekbox');
        $deleted = 0;
        foreach ($data as $datum) {
            $rsasaran = $this->db->where(COL_UNIQ, $datum)->get(TBL_SAKIP_MBID_PROGRAM_SASARAN)->row_array();
            $arrCond = array(
                COL_KD_URUSAN => $rsasaran[COL_KD_URUSAN],
                COL_KD_BIDANG => $rsasaran[COL_KD_BIDANG],
                COL_KD_UNIT => $rsasaran[COL_KD_UNIT],
                COL_KD_SUB => $rsasaran[COL_KD_SUB],
                COL_KD_BID => $rsasaran[COL_KD_BID],

                COL_KD_PEMDA=>$rsasaran[COL_KD_PEMDA],
                COL_KD_MISI=>$rsasaran[COL_KD_MISI],
                COL_KD_TUJUAN=>$rsasaran[COL_KD_TUJUAN],
                COL_KD_INDIKATORTUJUAN=>$rsasaran[COL_KD_INDIKATORTUJUAN],
                COL_KD_SASARAN=>$rsasaran[COL_KD_SASARAN],
                COL_KD_INDIKATORSASARAN=>$rsasaran[COL_KD_INDIKATORSASARAN],
                COL_KD_TUJUANOPD=>$rsasaran[COL_KD_TUJUANOPD],
                COL_KD_INDIKATORTUJUANOPD=>$rsasaran[COL_KD_INDIKATORTUJUANOPD],
                COL_KD_SASARANOPD=>$rsasaran[COL_KD_SASARANOPD],
                COL_KD_INDIKATORSASARANOPD=>$rsasaran[COL_KD_INDIKATORSASARANOPD],
                COL_KD_TAHUN=>$rsasaran[COL_KD_TAHUN],
                COL_KD_PROGRAMOPD=>$rsasaran[COL_KD_PROGRAMOPD],
                COL_KD_SASARANPROGRAMOPD=>$rsasaran[COL_KD_SASARANPROGRAMOPD]
            );

            $this->db->delete(TBL_SAKIP_MBID_PROGRAM_INDIKATOR, $arrCond);
            $this->db->delete(TBL_SAKIP_MBID_PROGRAM_SASARAN, array(COL_UNIQ => $datum));
            $deleted++;
        }
        if($deleted){
            ShowJsonSuccess($deleted." data dihapus");
        }else{
            ShowJsonError("Tidak ada dihapus");
        }
    }
}
