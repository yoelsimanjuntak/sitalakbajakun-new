<?php
class Renja extends MY_Controller {
  function __construct() {
      parent::__construct();
      if(!IsLogin() ||
          (GetLoggedUser()[COL_ROLEID] != ROLEADMIN &&
              GetLoggedUser()[COL_ROLEID] != ROLEBAPPEDA &&
              GetLoggedUser()[COL_ROLEID] != ROLEKEUANGAN &&
              GetLoggedUser()[COL_ROLEID] != ROLEKADIS &&
              GetLoggedUser()[COL_ROLEID] != ROLEKABID &&
              GetLoggedUser()[COL_ROLEID] != ROLAKASUBBID
          )
      ) {
          redirect('sakip/user/dashboard');
      }
  }

  public function index_renja() {
    $ruser = GetLoggedUser();
    $period = $this->input->get('Period');
    $tahun = $this->input->get(COL_KD_TAHUN);
    $kdUrusan = $this->input->get(COL_KD_URUSAN);
    $kdBidang = $this->input->get(COL_KD_BIDANG);
    $kdUnit = $this->input->get(COL_KD_UNIT);
    $kdSub = $this->input->get(COL_KD_SUB);

    $data['title'] = 'Renja';
    /*if(!empty($period)) {
      $this->db->where(TBL_SAKIP_MPEMDA.'.'.COL_KD_PEMDA, $period);
    } else {
      $this->db->where(TBL_SAKIP_MPEMDA.'.'.COL_KD_TAHUN_FROM.'<=', date('Y'));
      $this->db->where(TBL_SAKIP_MPEMDA.'.'.COL_KD_TAHUN_TO.'>=', date('Y'));
    }

    if($ruser[COL_ROLEID] != ROLEADMIN && $ruser[COL_ROLEID] != ROLEBAPPEDA) {
      $strOPD = explode('.', $ruser[COL_COMPANYID]);
      $this->db->where(TBL_SAKIP_MOPD_TUJUAN.".".COL_KD_URUSAN, $strOPD[0]);
      $this->db->where(TBL_SAKIP_MOPD_TUJUAN.".".COL_KD_BIDANG, $strOPD[1]);
      $this->db->where(TBL_SAKIP_MOPD_TUJUAN.".".COL_KD_UNIT, $strOPD[2]);
      $this->db->where(TBL_SAKIP_MOPD_TUJUAN.".".COL_KD_SUB, $strOPD[3]);
    }

    $data['misi'] = $this->db
    ->join(TBL_SAKIP_MPEMDA,TBL_SAKIP_MPEMDA.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_MOPD_TUJUAN.".".COL_KD_PEMDA,"left")
    ->join(TBL_SAKIP_MPMD_MISI,TBL_SAKIP_MPMD_MISI.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_MOPD_TUJUAN.".".COL_KD_PEMDA." AND ".TBL_SAKIP_MPMD_MISI.'.'.COL_KD_MISI." = ".TBL_SAKIP_MOPD_TUJUAN.".".COL_KD_MISI,"left")
    ->order_by(TBL_SAKIP_MPEMDA.'.'.COL_KD_TAHUN_FROM, 'desc')
    ->group_by(array(TBL_SAKIP_MOPD_TUJUAN.'.'.COL_KD_PEMDA, TBL_SAKIP_MOPD_TUJUAN.'.'.COL_KD_MISI))
    ->get(TBL_SAKIP_MOPD_TUJUAN)
    ->result_array();*/

    if($ruser[COL_ROLEID] != ROLEADMIN && $ruser[COL_ROLEID] != ROLEBAPPEDA) {
      $strOPD = explode('.', $ruser[COL_COMPANYID]);
      $kdUrusan = $strOPD[0];
      $kdBidang = $strOPD[1];
      $kdUnit = $strOPD[2];
      $kdSub = $strOPD[3];
    }

    $data['iku'] = array();
    if(!empty($kdUrusan) && !empty($kdBidang) && !empty($kdUnit) && !empty($kdSub)) {
      if(!empty($period)) {
        $this->db->where(TBL_SAKIP_MPEMDA.'.'.COL_KD_PEMDA, $period);
      } else {
        $this->db->where(TBL_SAKIP_MPEMDA.'.'.COL_KD_TAHUN_FROM.'<=', date('Y'));
        $this->db->where(TBL_SAKIP_MPEMDA.'.'.COL_KD_TAHUN_TO.'>=', date('Y'));
      }

      $this->db->where(TBL_SAKIP_MOPD_IKTUJUAN.".".COL_KD_URUSAN, $kdUrusan);
      $this->db->where(TBL_SAKIP_MOPD_IKTUJUAN.".".COL_KD_BIDANG, $kdBidang);
      $this->db->where(TBL_SAKIP_MOPD_IKTUJUAN.".".COL_KD_UNIT, $kdUnit);
      $this->db->where(TBL_SAKIP_MOPD_IKTUJUAN.".".COL_KD_SUB, $kdSub);

      $data['iku'] = $this->db
      ->select('sakip_mopd_iktujuan.*,
                sakip_mpemda.*,
                sakip_mpmd_misi.Nm_Misi,
                sakip_mpmd_tujuan.Nm_Tujuan,
                sakip_mpmd_iktujuan.Nm_IndikatorTujuan,
                sakip_mpmd_sasaran.Nm_Sasaran,
                sakip_mpmd_iksasaran.Nm_IndikatorSasaran,
                sakip_mopd_tujuan.Nm_TujuanOPD')
      ->join(TBL_SAKIP_MPEMDA,TBL_SAKIP_MPEMDA.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_MOPD_IKTUJUAN.".".COL_KD_PEMDA,"left")
      ->join(TBL_SAKIP_MPMD_MISI,TBL_SAKIP_MPMD_MISI.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_MOPD_IKTUJUAN.".".COL_KD_PEMDA." AND ".TBL_SAKIP_MPMD_MISI.'.'.COL_KD_MISI." = ".TBL_SAKIP_MOPD_IKTUJUAN.".".COL_KD_MISI,"left")
      ->join(TBL_SAKIP_MPMD_TUJUAN,
              TBL_SAKIP_MPMD_TUJUAN.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_MOPD_IKTUJUAN.".".COL_KD_PEMDA." AND ".
              TBL_SAKIP_MPMD_TUJUAN.'.'.COL_KD_MISI." = ".TBL_SAKIP_MOPD_IKTUJUAN.".".COL_KD_MISI." AND ".
              TBL_SAKIP_MPMD_TUJUAN.'.'.COL_KD_TUJUAN." = ".TBL_SAKIP_MOPD_IKTUJUAN.".".COL_KD_TUJUAN
            ,"left")
      ->join(TBL_SAKIP_MPMD_IKTUJUAN,
              TBL_SAKIP_MPMD_IKTUJUAN.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_MOPD_IKTUJUAN.".".COL_KD_PEMDA." AND ".
              TBL_SAKIP_MPMD_IKTUJUAN.'.'.COL_KD_MISI." = ".TBL_SAKIP_MOPD_IKTUJUAN.".".COL_KD_MISI." AND ".
              TBL_SAKIP_MPMD_IKTUJUAN.'.'.COL_KD_TUJUAN." = ".TBL_SAKIP_MOPD_IKTUJUAN.".".COL_KD_TUJUAN." AND ".
              TBL_SAKIP_MPMD_IKTUJUAN.'.'.COL_KD_INDIKATORTUJUAN." = ".TBL_SAKIP_MOPD_IKTUJUAN.".".COL_KD_INDIKATORTUJUAN
            ,"left")
      ->join(TBL_SAKIP_MPMD_SASARAN,
              TBL_SAKIP_MPMD_SASARAN.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_MOPD_IKTUJUAN.".".COL_KD_PEMDA." AND ".
              TBL_SAKIP_MPMD_SASARAN.'.'.COL_KD_MISI." = ".TBL_SAKIP_MOPD_IKTUJUAN.".".COL_KD_MISI." AND ".
              TBL_SAKIP_MPMD_SASARAN.'.'.COL_KD_TUJUAN." = ".TBL_SAKIP_MOPD_IKTUJUAN.".".COL_KD_TUJUAN." AND ".
              TBL_SAKIP_MPMD_SASARAN.'.'.COL_KD_INDIKATORTUJUAN." = ".TBL_SAKIP_MOPD_IKTUJUAN.".".COL_KD_INDIKATORTUJUAN." AND ".
              TBL_SAKIP_MPMD_SASARAN.'.'.COL_KD_SASARAN." = ".TBL_SAKIP_MOPD_IKTUJUAN.".".COL_KD_SASARAN
            ,"left")
      ->join(TBL_SAKIP_MPMD_IKSASARAN,
              TBL_SAKIP_MPMD_IKSASARAN.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_MOPD_IKTUJUAN.".".COL_KD_PEMDA." AND ".
              TBL_SAKIP_MPMD_IKSASARAN.'.'.COL_KD_MISI." = ".TBL_SAKIP_MOPD_IKTUJUAN.".".COL_KD_MISI." AND ".
              TBL_SAKIP_MPMD_IKSASARAN.'.'.COL_KD_TUJUAN." = ".TBL_SAKIP_MOPD_IKTUJUAN.".".COL_KD_TUJUAN." AND ".
              TBL_SAKIP_MPMD_IKSASARAN.'.'.COL_KD_INDIKATORTUJUAN." = ".TBL_SAKIP_MOPD_IKTUJUAN.".".COL_KD_INDIKATORTUJUAN." AND ".
              TBL_SAKIP_MPMD_IKSASARAN.'.'.COL_KD_SASARAN." = ".TBL_SAKIP_MOPD_IKTUJUAN.".".COL_KD_SASARAN." AND ".
              TBL_SAKIP_MPMD_IKSASARAN.'.'.COL_KD_INDIKATORSASARAN." = ".TBL_SAKIP_MOPD_IKTUJUAN.".".COL_KD_INDIKATORSASARAN
            ,"left")
      ->join(TBL_SAKIP_MOPD_TUJUAN,
              TBL_SAKIP_MOPD_TUJUAN.'.'.COL_KD_URUSAN." = ".TBL_SAKIP_MOPD_IKTUJUAN.".".COL_KD_URUSAN." AND ".
              TBL_SAKIP_MOPD_TUJUAN.'.'.COL_KD_BIDANG." = ".TBL_SAKIP_MOPD_IKTUJUAN.".".COL_KD_BIDANG." AND ".
              TBL_SAKIP_MOPD_TUJUAN.'.'.COL_KD_UNIT." = ".TBL_SAKIP_MOPD_IKTUJUAN.".".COL_KD_UNIT." AND ".
              TBL_SAKIP_MOPD_TUJUAN.'.'.COL_KD_SUB." = ".TBL_SAKIP_MOPD_IKTUJUAN.".".COL_KD_SUB." AND ".

              TBL_SAKIP_MOPD_TUJUAN.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_MOPD_IKTUJUAN.".".COL_KD_PEMDA." AND ".
              TBL_SAKIP_MOPD_TUJUAN.'.'.COL_KD_MISI." = ".TBL_SAKIP_MOPD_IKTUJUAN.".".COL_KD_MISI." AND ".
              TBL_SAKIP_MOPD_TUJUAN.'.'.COL_KD_TUJUAN." = ".TBL_SAKIP_MOPD_IKTUJUAN.".".COL_KD_TUJUAN." AND ".
              TBL_SAKIP_MOPD_TUJUAN.'.'.COL_KD_INDIKATORTUJUAN." = ".TBL_SAKIP_MOPD_IKTUJUAN.".".COL_KD_INDIKATORTUJUAN." AND ".
              TBL_SAKIP_MOPD_TUJUAN.'.'.COL_KD_SASARAN." = ".TBL_SAKIP_MOPD_IKTUJUAN.".".COL_KD_SASARAN." AND ".
              TBL_SAKIP_MOPD_TUJUAN.'.'.COL_KD_INDIKATORSASARAN." = ".TBL_SAKIP_MOPD_IKTUJUAN.".".COL_KD_INDIKATORSASARAN." AND ".
              TBL_SAKIP_MOPD_TUJUAN.'.'.COL_KD_TUJUANOPD." = ".TBL_SAKIP_MOPD_IKTUJUAN.".".COL_KD_TUJUANOPD
            ,"left")

      ->order_by(TBL_SAKIP_MOPD_IKTUJUAN.'.'.COL_KD_PEMDA)
      ->order_by(TBL_SAKIP_MOPD_IKTUJUAN.'.'.COL_KD_MISI)
      ->order_by(TBL_SAKIP_MOPD_IKTUJUAN.'.'.COL_KD_TUJUAN)
      ->order_by(TBL_SAKIP_MOPD_IKTUJUAN.'.'.COL_KD_INDIKATORTUJUAN)
      ->order_by(TBL_SAKIP_MOPD_IKTUJUAN.'.'.COL_KD_SASARAN)
      ->order_by(TBL_SAKIP_MOPD_IKTUJUAN.'.'.COL_KD_INDIKATORSASARAN)
      ->order_by(TBL_SAKIP_MOPD_IKTUJUAN.'.'.COL_KD_TUJUANOPD)
      ->order_by(TBL_SAKIP_MOPD_IKTUJUAN.'.'.COL_KD_INDIKATORTUJUANOPD)
      /*->group_by(array(
        TBL_SAKIP_MOPD_IKTUJUAN.'.'.COL_KD_PEMDA,
        TBL_SAKIP_MOPD_IKTUJUAN.'.'.COL_KD_MISI,
        TBL_SAKIP_MOPD_IKTUJUAN.'.'.COL_KD_TUJUAN,
        TBL_SAKIP_MOPD_IKTUJUAN.'.'.COL_KD_INDIKATORTUJUAN,
        TBL_SAKIP_MOPD_IKTUJUAN.'.'.COL_KD_SASARAN,
        TBL_SAKIP_MOPD_IKTUJUAN.'.'.COL_KD_INDIKATORSASARAN,
        TBL_SAKIP_MOPD_IKTUJUAN.'.'.COL_KD_TUJUANOPD,
        TBL_SAKIP_MOPD_IKTUJUAN.'.'.COL_KD_INDIKATORTUJUANOPD
      ))*/
      ->get(TBL_SAKIP_MOPD_IKTUJUAN)
      ->result_array();
    }

    $data['period'] = $period;
    $data['tahun'] = !empty($tahun) ? $tahun : date('Y');
    $this->load->view('renja/index', $data);
  }

  public function index_load($id) {
    $key = explode('.', $id);
    $cond = array(
      COL_KD_PEMDA=>$key[0],
      COL_KD_TAHUN=>$key[1],
      COL_KD_URUSAN=>$key[2],
      COL_KD_BIDANG=>$key[3],
      COL_KD_UNIT=>$key[4],
      COL_KD_SUB=>$key[5],
      COL_KD_MISI=>$key[6],
      COL_KD_TUJUAN=>$key[7],
      COL_KD_INDIKATORTUJUAN=>$key[8],
      COL_KD_SASARAN=>$key[9],
      COL_KD_INDIKATORSASARAN=>$key[10],
      COL_KD_TUJUANOPD=>$key[11],
      COL_KD_INDIKATORTUJUANOPD=>$key[12],
      COL_KD_SASARANOPD=>$key[13],
      COL_KD_INDIKATORSASARANOPD=>$key[14]
    );

    $data['rprogram'] = $rprogram = $this->db
    ->where($cond)
    ->order_by(TBL_SAKIP_MBID_PROGRAM.'.'.COL_KD_PROGRAMOPD, 'asc')
    ->get(TBL_SAKIP_MBID_PROGRAM)
    ->result_array();
    $data['expid'] = $this->input->post('expid');

    $this->load->view('renja/index_program', $data);
  }

  public function tambah_program($id) {
    $key = explode('.', $id);
    $data['kdUrusan'] = $key[2];
    $data['kdBidang'] = $key[3];
    $data['kdUnit'] = $key[4];
    $data['kdSub'] = $key[5];
    if(!empty($_POST)) {
      $rec = array(
        COL_KD_PEMDA=>$key[0],
        COL_KD_TAHUN=>$key[1],
        COL_KD_URUSAN=>$key[2],
        COL_KD_BIDANG=>$key[3],
        COL_KD_UNIT=>$key[4],
        COL_KD_SUB=>$key[5],
        COL_KD_MISI=>$key[6],
        COL_KD_TUJUAN=>$key[7],
        COL_KD_INDIKATORTUJUAN=>$key[8],
        COL_KD_SASARAN=>$key[9],
        COL_KD_INDIKATORSASARAN=>$key[10],
        COL_KD_TUJUANOPD=>$key[11],
        COL_KD_INDIKATORTUJUANOPD=>$key[12],
        COL_KD_SASARANOPD=>$key[13],
        COL_KD_INDIKATORSASARANOPD=>$key[14],

        COL_KD_BID=>$this->input->post(COL_KD_BID),
        COL_KD_PROGRAMOPD=>$this->input->post(COL_KD_PROGRAMOPD),
        COL_NM_PROGRAMOPD=>$this->input->post(COL_NM_PROGRAMOPD),
        COL_REMARKS=>$this->input->post(COL_REMARKS)
      );

      $res = $this->db->insert(TBL_SAKIP_MBID_PROGRAM, $rec);
      if(!$res) {
        ShowJsonError($this->db->error());
        return;
      }
      ShowJsonSuccess('Berhasil');
      return;
    } else {
      $this->load->view('renja/form_program_partial', $data);
    }
  }

  public function ubah_program($uniq) {
    $data['data'] = $rdata = $this->db->where(COL_UNIQ, $uniq)->get(TBL_SAKIP_MBID_PROGRAM)->row_array();
    if(empty($rdata)) {
      echo 'Program tidak valid.';
      return;
    }

    $condDpa = array(
      COL_KD_PEMDA=>$rdata[COL_KD_PEMDA],
      COL_KD_TAHUN=>$rdata[COL_KD_TAHUN],
      COL_KD_URUSAN=>$rdata[COL_KD_URUSAN],
      COL_KD_BIDANG=>$rdata[COL_KD_BIDANG],
      COL_KD_UNIT=>$rdata[COL_KD_UNIT],
      COL_KD_SUB=>$rdata[COL_KD_SUB],
      COL_KD_MISI=>$rdata[COL_KD_MISI],
      COL_KD_TUJUAN=>$rdata[COL_KD_TUJUAN],
      COL_KD_INDIKATORTUJUAN=>$rdata[COL_KD_INDIKATORTUJUAN],
      COL_KD_SASARAN=>$rdata[COL_KD_SASARAN],
      COL_KD_INDIKATORSASARAN=>$rdata[COL_KD_INDIKATORSASARAN],
      COL_KD_TUJUANOPD=>$rdata[COL_KD_TUJUANOPD],
      COL_KD_INDIKATORTUJUANOPD=>$rdata[COL_KD_INDIKATORTUJUANOPD],
      COL_KD_SASARANOPD=>$rdata[COL_KD_SASARANOPD],
      COL_KD_INDIKATORSASARANOPD=>$rdata[COL_KD_INDIKATORSASARANOPD],

      COL_KD_BID=>$rdata[COL_KD_BID],
      COL_KD_PROGRAMOPD=>$rdata[COL_KD_PROGRAMOPD]
    );
    $data['rdpa'] = $rdpa = $this->db->where($condDpa)->get(TBL_SAKIP_DPA_PROGRAM)->row_array();

    $data['kdUrusan'] = $rdata[COL_KD_URUSAN];
    $data['kdBidang'] = $rdata[COL_KD_BIDANG];
    $data['kdUnit'] = $rdata[COL_KD_UNIT];
    $data['kdSub'] = $rdata[COL_KD_SUB];

    if(!empty($_POST)) {
      $rec = array(
        COL_KD_BID=>$this->input->post(COL_KD_BID),
        COL_KD_PROGRAMOPD=>$this->input->post(COL_KD_PROGRAMOPD),
        COL_NM_PROGRAMOPD=>$this->input->post(COL_NM_PROGRAMOPD)
      );

      $this->db->trans_begin();
      try {
        $res = $this->db->where(COL_UNIQ, $uniq)->update(TBL_SAKIP_MBID_PROGRAM, array_merge($rec, array(COL_REMARKS=>$this->input->post(COL_REMARKS))));
        if(!$res) {
          $err = $this->db->error();
          throw new Exception("Database error: ".$err['message']);
        }

        if(!empty($rdpa)) {
          $res = $this->db->where(array_merge($condDpa, array(COL_IS_RENJA=>1)))->update(TBL_SAKIP_DPA_PROGRAM, $rec);
          if(!$res) {
            $err = $this->db->error();
            throw new Exception("Database error: ".$err['message']);
          }
        }

      } catch(Exception $e) {
        $this->db->trans_rollback();
        ShowJsonError($e->getMessage());
        return;
      }

      $this->db->trans_commit();
      ShowJsonSuccess('Berhasil');
      return;
    } else {
      $this->load->view('renja/form_program_partial', $data);
    }
  }

  public function tambah_dpa_program($uniq) {
    $data['data'] = $rdata = $this->db->where(COL_UNIQ, $uniq)->get(TBL_SAKIP_MBID_PROGRAM)->row_array();
    if(empty($rdata)) {
      echo 'Program tidak valid.';
      return;
    }
    $mainArr = array(
      COL_KD_PEMDA=>$rdata[COL_KD_PEMDA],
      COL_KD_TAHUN=>$rdata[COL_KD_TAHUN],
      COL_KD_URUSAN=>$rdata[COL_KD_URUSAN],
      COL_KD_BIDANG=>$rdata[COL_KD_BIDANG],
      COL_KD_UNIT=>$rdata[COL_KD_UNIT],
      COL_KD_SUB=>$rdata[COL_KD_SUB],
      COL_KD_MISI=>$rdata[COL_KD_MISI],
      COL_KD_TUJUAN=>$rdata[COL_KD_TUJUAN],
      COL_KD_INDIKATORTUJUAN=>$rdata[COL_KD_INDIKATORTUJUAN],
      COL_KD_SASARAN=>$rdata[COL_KD_SASARAN],
      COL_KD_INDIKATORSASARAN=>$rdata[COL_KD_INDIKATORSASARAN],
      COL_KD_TUJUANOPD=>$rdata[COL_KD_TUJUANOPD],
      COL_KD_INDIKATORTUJUANOPD=>$rdata[COL_KD_INDIKATORTUJUANOPD],
      COL_KD_SASARANOPD=>$rdata[COL_KD_SASARANOPD],
      COL_KD_INDIKATORSASARANOPD=>$rdata[COL_KD_INDIKATORSASARANOPD],

      COL_KD_BID=>$rdata[COL_KD_BID],
      COL_KD_PROGRAMOPD=>$rdata[COL_KD_PROGRAMOPD]
    );
    $rdpa = $this->db->where($mainArr)->get(TBL_SAKIP_DPA_PROGRAM)->row_array();
    if(!empty($rdpa)) {
      echo 'Program ini sudah ada di DPA.';
      return;
    }

    $data['kdUrusan'] = $rdata[COL_KD_URUSAN];
    $data['kdBidang'] = $rdata[COL_KD_BIDANG];
    $data['kdUnit'] = $rdata[COL_KD_UNIT];
    $data['kdSub'] = $rdata[COL_KD_SUB];
    $data['dpa'] = true;

    if(!empty($_POST)) {
      $recSasaran = array();
      $recIndikator = array();
      $rec = array_merge($mainArr, array(
        COL_NM_PROGRAMOPD=>$rdata[COL_NM_PROGRAMOPD],
        COL_IS_RENJA=>1
      ));

      $rsasaran = $this->db->where($mainArr)->get(TBL_SAKIP_MBID_PROGRAM_SASARAN)->result_array();
      $rindikator = $this->db->where($mainArr)->get(TBL_SAKIP_MBID_PROGRAM_INDIKATOR)->result_array();
      foreach($rsasaran as $r) {
        $recSasaran[] = array_merge($mainArr, array(
          COL_KD_SASARANPROGRAMOPD=>$r[COL_KD_SASARANPROGRAMOPD],
          COL_NM_SASARANPROGRAMOPD=>$r[COL_NM_SASARANPROGRAMOPD]
        ));
      }
      foreach($rindikator as $r) {
        $recIndikator[] = array_merge($mainArr, array(
          COL_KD_SASARANPROGRAMOPD=>$r[COL_KD_SASARANPROGRAMOPD],
          COL_KD_INDIKATORPROGRAMOPD=>$r[COL_KD_INDIKATORPROGRAMOPD],
          COL_NM_INDIKATORPROGRAMOPD=>$r[COL_NM_INDIKATORPROGRAMOPD],
          COL_NM_FORMULA => $r[COL_NM_FORMULA],
          COL_NM_SUMBERDATA => $r[COL_NM_SUMBERDATA],
          COL_NM_PENANGGUNGJAWAB => $r[COL_NM_PENANGGUNGJAWAB],
          COL_KD_SATUAN => $r[COL_KD_SATUAN],
          COL_TARGET => $r[COL_TARGET]
        ));
      }

      $this->db->trans_begin();
      try {
        $res = $this->db->insert(TBL_SAKIP_DPA_PROGRAM, $rec);
        if(!$res) {
          throw new Exception("Database error: ".$this->db->error());
        }

        if(!empty($recSasaran)) {
          $res = $this->db->insert_batch(TBL_SAKIP_DPA_PROGRAM_SASARAN, $recSasaran);
          if(!$res) {
            throw new Exception("Database error: ".$this->db->error());
          }
        }

        if(!empty($recIndikator)) {
          $res = $this->db->insert_batch(TBL_SAKIP_DPA_PROGRAM_INDIKATOR, $recIndikator);
          if(!$res) {
            throw new Exception("Database error: ".$this->db->error());
          }
        }
      } catch(Exception $e) {
        $this->db->trans_rollback();
        ShowJsonError($e->getMessage());
        return;
      }

      $this->db->trans_commit();
      ShowJsonSuccess('Berhasil');
      return;
    } else {
      $this->load->view('renja/form_program_partial', $data);
    }
  }

  public function hapus_program($uniq) {
    $rdata = $this->db->where(COL_UNIQ, $uniq)->get(TBL_SAKIP_MBID_PROGRAM)->row_array();
    if(empty($rdata)) {
      ShowJsonError('Program tidak ditemukan.');
      return;
    }

    $condDpa = array(
      COL_KD_PEMDA=>$rdata[COL_KD_PEMDA],
      COL_KD_TAHUN=>$rdata[COL_KD_TAHUN],
      COL_KD_URUSAN=>$rdata[COL_KD_URUSAN],
      COL_KD_BIDANG=>$rdata[COL_KD_BIDANG],
      COL_KD_UNIT=>$rdata[COL_KD_UNIT],
      COL_KD_SUB=>$rdata[COL_KD_SUB],
      COL_KD_MISI=>$rdata[COL_KD_MISI],
      COL_KD_TUJUAN=>$rdata[COL_KD_TUJUAN],
      COL_KD_INDIKATORTUJUAN=>$rdata[COL_KD_INDIKATORTUJUAN],
      COL_KD_SASARAN=>$rdata[COL_KD_SASARAN],
      COL_KD_INDIKATORSASARAN=>$rdata[COL_KD_INDIKATORSASARAN],
      COL_KD_TUJUANOPD=>$rdata[COL_KD_TUJUANOPD],
      COL_KD_INDIKATORTUJUANOPD=>$rdata[COL_KD_INDIKATORTUJUANOPD],
      COL_KD_SASARANOPD=>$rdata[COL_KD_SASARANOPD],
      COL_KD_INDIKATORSASARANOPD=>$rdata[COL_KD_INDIKATORSASARANOPD],

      COL_KD_BID=>$rdata[COL_KD_BID],
      COL_KD_PROGRAMOPD=>$rdata[COL_KD_PROGRAMOPD]
    );

    $rkegiatan = $this->db->where($condDpa)->get(TBL_SAKIP_MSUBBID_KEGIATAN)->row_array();
    if(!empty($rkegiatan)) {
      ShowJsonError('Silakan hapus kegiatan terlebih dahulu.');
      return;
    }

    $this->db->trans_begin();
    try {
      $res = $this->db->where(COL_UNIQ, $uniq)->delete(TBL_SAKIP_MBID_PROGRAM);
      if(!$res) {
        throw new Exception("Database error:". $this->db->error());
      }

      $res = $this->db->where(array_merge($condDpa, array(COL_IS_RENJA=>1)))->delete(TBL_SAKIP_DPA_PROGRAM);
      if(!$res) {
        throw new Exception("Database error:".$this->db->error());
      }
    } catch(Exception $e) {
      $this->db->trans_rollback();
      ShowJsonError($e->getMessage());
      return;
    }

    $this->db->trans_commit();
    ShowJsonSuccess('Berhasil');
  }

  public function tambah_sasaran_program($uniq) {
    $data['program'] = $rprogram = $this->db->where(COL_UNIQ, $uniq)->get(TBL_SAKIP_MBID_PROGRAM)->row_array();
    if(empty($rprogram)) {
      echo 'Program tidak valid.';
      return;
    }

    $condDpa = array(
      COL_KD_PEMDA=>$rprogram[COL_KD_PEMDA],
      COL_KD_TAHUN=>$rprogram[COL_KD_TAHUN],
      COL_KD_URUSAN=>$rprogram[COL_KD_URUSAN],
      COL_KD_BIDANG=>$rprogram[COL_KD_BIDANG],
      COL_KD_UNIT=>$rprogram[COL_KD_UNIT],
      COL_KD_SUB=>$rprogram[COL_KD_SUB],
      COL_KD_MISI=>$rprogram[COL_KD_MISI],
      COL_KD_TUJUAN=>$rprogram[COL_KD_TUJUAN],
      COL_KD_INDIKATORTUJUAN=>$rprogram[COL_KD_INDIKATORTUJUAN],
      COL_KD_SASARAN=>$rprogram[COL_KD_SASARAN],
      COL_KD_INDIKATORSASARAN=>$rprogram[COL_KD_INDIKATORSASARAN],
      COL_KD_TUJUANOPD=>$rprogram[COL_KD_TUJUANOPD],
      COL_KD_INDIKATORTUJUANOPD=>$rprogram[COL_KD_INDIKATORTUJUANOPD],
      COL_KD_SASARANOPD=>$rprogram[COL_KD_SASARANOPD],
      COL_KD_INDIKATORSASARANOPD=>$rprogram[COL_KD_INDIKATORSASARANOPD],

      COL_KD_BID=>$rprogram[COL_KD_BID],
      COL_KD_PROGRAMOPD=>$rprogram[COL_KD_PROGRAMOPD]
    );
    $data['dpa'] = $rdpa = $this->db->where($condDpa)->get(TBL_SAKIP_DPA_PROGRAM)->row_array();
    if(!empty($rdpa)) {
      echo 'Program sudah ada di DPA. Silakan hapus program dari DPA terlebih dahulu untuk menambah sasaran.';
      return;
    }

    if(!empty($_POST)) {
      $rec = array(
        COL_KD_PEMDA=>$rprogram[COL_KD_PEMDA],
        COL_KD_TAHUN=>$rprogram[COL_KD_TAHUN],
        COL_KD_URUSAN=>$rprogram[COL_KD_URUSAN],
        COL_KD_BIDANG=>$rprogram[COL_KD_BIDANG],
        COL_KD_UNIT=>$rprogram[COL_KD_UNIT],
        COL_KD_SUB=>$rprogram[COL_KD_SUB],
        COL_KD_MISI=>$rprogram[COL_KD_MISI],
        COL_KD_TUJUAN=>$rprogram[COL_KD_TUJUAN],
        COL_KD_INDIKATORTUJUAN=>$rprogram[COL_KD_INDIKATORTUJUAN],
        COL_KD_SASARAN=>$rprogram[COL_KD_SASARAN],
        COL_KD_INDIKATORSASARAN=>$rprogram[COL_KD_INDIKATORSASARAN],
        COL_KD_TUJUANOPD=>$rprogram[COL_KD_TUJUANOPD],
        COL_KD_INDIKATORTUJUANOPD=>$rprogram[COL_KD_INDIKATORTUJUANOPD],
        COL_KD_SASARANOPD=>$rprogram[COL_KD_SASARANOPD],
        COL_KD_INDIKATORSASARANOPD=>$rprogram[COL_KD_INDIKATORSASARANOPD],
        COL_KD_BID=>$rprogram[COL_KD_BID],
        COL_KD_PROGRAMOPD=>$rprogram[COL_KD_PROGRAMOPD],
        COL_KD_SASARANPROGRAMOPD=>$this->input->post(COL_KD_SASARANPROGRAMOPD),
        COL_NM_SASARANPROGRAMOPD=>$this->input->post(COL_NM_SASARANPROGRAMOPD)
      );

      $this->db->trans_begin();
      try {
        if(!$this->db->insert(TBL_SAKIP_MBID_PROGRAM_SASARAN, $rec)){
            throw new Exception("Database error: ".$this->db->error());
        }

        $detNo = $this->input->post("NoDet");
        $detDesc = $this->input->post("KetDet");
        $detFormula = $this->input->post("FormulaDet");
        $detSumber = $this->input->post("SumberDet");
        $detPIC = $this->input->post("PICDet");
        $detSatuan = $this->input->post("SatuanDet");
        $detTarget = $this->input->post("TargetDet");
        $detAwal = $this->input->post("AwalDet");
        $detAkhir = $this->input->post("AkhirDet");
        $det = [];
        for($i = 0; $i<count($detNo); $i++) {
            $det[] = array(
              COL_KD_PEMDA=>$rprogram[COL_KD_PEMDA],
              COL_KD_TAHUN=>$rprogram[COL_KD_TAHUN],
              COL_KD_URUSAN=>$rprogram[COL_KD_URUSAN],
              COL_KD_BIDANG=>$rprogram[COL_KD_BIDANG],
              COL_KD_UNIT=>$rprogram[COL_KD_UNIT],
              COL_KD_SUB=>$rprogram[COL_KD_SUB],
              COL_KD_MISI=>$rprogram[COL_KD_MISI],
              COL_KD_TUJUAN=>$rprogram[COL_KD_TUJUAN],
              COL_KD_INDIKATORTUJUAN=>$rprogram[COL_KD_INDIKATORTUJUAN],
              COL_KD_SASARAN=>$rprogram[COL_KD_SASARAN],
              COL_KD_INDIKATORSASARAN=>$rprogram[COL_KD_INDIKATORSASARAN],
              COL_KD_TUJUANOPD=>$rprogram[COL_KD_TUJUANOPD],
              COL_KD_INDIKATORTUJUANOPD=>$rprogram[COL_KD_INDIKATORTUJUANOPD],
              COL_KD_SASARANOPD=>$rprogram[COL_KD_SASARANOPD],
              COL_KD_INDIKATORSASARANOPD=>$rprogram[COL_KD_INDIKATORSASARANOPD],
              COL_KD_BID=>$rprogram[COL_KD_BID],
              COL_KD_PROGRAMOPD=>$rprogram[COL_KD_PROGRAMOPD],

              COL_KD_SASARANPROGRAMOPD => $this->input->post(COL_KD_SASARANPROGRAMOPD),
              COL_KD_INDIKATORPROGRAMOPD => $detNo[$i],
              COL_NM_INDIKATORPROGRAMOPD => $detDesc[$i],
              COL_NM_FORMULA => $detFormula[$i],
              COL_NM_SUMBERDATA => $detSumber[$i],
              COL_NM_PENANGGUNGJAWAB => $detPIC[$i],
              COL_KD_SATUAN => $detSatuan[$i],
              COL_TARGET => $detTarget[$i],
              COL_AWAL => $detAwal[$i],
              COL_AKHIR => $detAkhir[$i]
            );
        }
        if(count($det) > 0) {
          $res = $this->db->insert_batch(TBL_SAKIP_MBID_PROGRAM_INDIKATOR, $det);
          if(!$res) {
              throw new Exception("Database error: ".$this->db->error());
          }
        }

        $this->db->trans_commit();
        ShowJsonSuccess('Berhasil');
        return;
      } catch (Exception $e) {
          $this->db->trans_rollback();
          ShowJsonError($e->getMessage());
          return;
      }
    } else {
      $this->load->view('renja/form_program_sasaran_partial', $data);
    }
  }

  public function list_sasaran_program($uniq) {
    $data['program'] = $rprogram = $this->db->where(COL_UNIQ, $uniq)->get(TBL_SAKIP_MBID_PROGRAM)->row_array();
    if(empty($rprogram)) {
      echo 'Program tidak valid.';
      return;
    }

    $cond = array(
      COL_KD_PEMDA=>$rprogram[COL_KD_PEMDA],
      COL_KD_TAHUN=>$rprogram[COL_KD_TAHUN],
      COL_KD_URUSAN=>$rprogram[COL_KD_URUSAN],
      COL_KD_BIDANG=>$rprogram[COL_KD_BIDANG],
      COL_KD_UNIT=>$rprogram[COL_KD_UNIT],
      COL_KD_SUB=>$rprogram[COL_KD_SUB],
      COL_KD_MISI=>$rprogram[COL_KD_MISI],
      COL_KD_TUJUAN=>$rprogram[COL_KD_TUJUAN],
      COL_KD_INDIKATORTUJUAN=>$rprogram[COL_KD_INDIKATORTUJUAN],
      COL_KD_SASARAN=>$rprogram[COL_KD_SASARAN],
      COL_KD_INDIKATORSASARAN=>$rprogram[COL_KD_INDIKATORSASARAN],
      COL_KD_TUJUANOPD=>$rprogram[COL_KD_TUJUANOPD],
      COL_KD_INDIKATORTUJUANOPD=>$rprogram[COL_KD_INDIKATORTUJUANOPD],
      COL_KD_SASARANOPD=>$rprogram[COL_KD_SASARANOPD],
      COL_KD_INDIKATORSASARANOPD=>$rprogram[COL_KD_INDIKATORSASARANOPD],
      COL_KD_BID=>$rprogram[COL_KD_BID],
      COL_KD_PROGRAMOPD=>$rprogram[COL_KD_PROGRAMOPD]
    );
    $data['sasaran'] = $rsasaran = $this->db->where($cond)->get(TBL_SAKIP_MBID_PROGRAM_SASARAN)->result_array();
    $data['dpa'] = $rdpa = $this->db->where($cond)->get(TBL_SAKIP_DPA_PROGRAM)->result_array();
    $this->load->view('renja/index_program_sasaran', $data);
  }

  public function hapus_sasaran_program($uniq) {
    $res = $this->db->where(COL_UNIQ, $uniq)->delete(TBL_SAKIP_MBID_PROGRAM_SASARAN);
    if(!$res) {
      ShowJsonError($this->db->error());
      return;
    }
    ShowJsonSuccess('Berhasil');
  }

  public function tambah_kegiatan($uniq) {
    $data['program'] = $rprogram = $this->db->where(COL_UNIQ, $uniq)->get(TBL_SAKIP_MBID_PROGRAM)->row_array();
    if(empty($rprogram)) {
      echo 'Program tidak valid.';
      return;
    }

    $data['kdUrusan'] = $rprogram[COL_KD_URUSAN];
    $data['kdBidang'] = $rprogram[COL_KD_BIDANG];
    $data['kdUnit'] = $rprogram[COL_KD_UNIT];
    $data['kdSub'] = $rprogram[COL_KD_SUB];
    $data['kdBid'] = $rprogram[COL_KD_BID];
    if(!empty($_POST)) {
      $rec = array(
        COL_KD_PEMDA=>$rprogram[COL_KD_PEMDA],
        COL_KD_TAHUN=>$rprogram[COL_KD_TAHUN],
        COL_KD_URUSAN=>$rprogram[COL_KD_URUSAN],
        COL_KD_BIDANG=>$rprogram[COL_KD_BIDANG],
        COL_KD_UNIT=>$rprogram[COL_KD_UNIT],
        COL_KD_SUB=>$rprogram[COL_KD_SUB],
        COL_KD_MISI=>$rprogram[COL_KD_MISI],
        COL_KD_TUJUAN=>$rprogram[COL_KD_TUJUAN],
        COL_KD_INDIKATORTUJUAN=>$rprogram[COL_KD_INDIKATORTUJUAN],
        COL_KD_SASARAN=>$rprogram[COL_KD_SASARAN],
        COL_KD_INDIKATORSASARAN=>$rprogram[COL_KD_INDIKATORSASARAN],
        COL_KD_TUJUANOPD=>$rprogram[COL_KD_TUJUANOPD],
        COL_KD_INDIKATORTUJUANOPD=>$rprogram[COL_KD_INDIKATORTUJUANOPD],
        COL_KD_SASARANOPD=>$rprogram[COL_KD_SASARANOPD],
        COL_KD_INDIKATORSASARANOPD=>$rprogram[COL_KD_INDIKATORSASARANOPD],
        COL_KD_BID=>$rprogram[COL_KD_BID],
        COL_KD_PROGRAMOPD=>$rprogram[COL_KD_PROGRAMOPD],

        COL_KD_SASARANPROGRAMOPD=>$this->input->post(COL_KD_SASARANPROGRAMOPD),
        COL_KD_SUBBID=>$this->input->post(COL_KD_SUBBID),
        COL_KD_KEGIATANOPD=>$this->input->post(COL_KD_KEGIATANOPD),
        COL_NM_KEGIATANOPD=>$this->input->post(COL_NM_KEGIATANOPD),
        COL_KD_SUMBERDANA=>$this->input->post(COL_KD_SUMBERDANA),
        COL_TOTAL=>$this->input->post(COL_TOTAL),
        COL_REMARKS=>$this->input->post(COL_REMARKS)
      );

      $kdSubKegiatan = $this->input->post("KdSubKegiatan");
      $nmSubKegiatan = $this->input->post("NmSubKegiatan");
      $nmIndikatorSubKegiatan = $this->input->post("NmIndikatorSubKegiatan");
      $totalSubKegiatan = $this->input->post("TotalSubKegiatan");
      $arrSubKegiatan = [];
      for($i = 0; $i<count($kdSubKegiatan); $i++) {
        $arrSubKegiatan[] = array(
          'KdSubKegiatan' => $kdSubKegiatan[$i],
          'NmSubKegiatan' => $nmSubKegiatan[$i],
          'NmIndikatorSubKegiatan' => $nmIndikatorSubKegiatan[$i],
          'TotalSubKegiatan' => $totalSubKegiatan[$i]
        );
      }
      if(!empty($arrSubKegiatan)) {
        $rec[COL_NM_ARRSUBKEGIATAN]=json_encode($arrSubKegiatan);
      }

      $res = $this->db->insert(TBL_SAKIP_MSUBBID_KEGIATAN, $rec);
      if(!$res) {
        ShowJsonError($this->db->error());
        return;
      }
      ShowJsonSuccess('Berhasil');
      return;
    } else {
      $this->load->view('renja/form_kegiatan_partial', $data);
    }
  }

  public function ubah_kegiatan($uniq, $uniqprog) {
    $data['data'] = $rdata = $this->db->where(COL_UNIQ, $uniq)->get(TBL_SAKIP_MSUBBID_KEGIATAN)->row_array();
    if(empty($rdata)) {
      echo 'Kegiatan tidak valid.';
      return;
    }
    $data['program'] = $rprogram = $this->db->where(COL_UNIQ, $uniqprog)->get(TBL_SAKIP_MBID_PROGRAM)->row_array();
    if(empty($rprogram)) {
      echo 'Program tidak valid.';
      return;
    }

    $condDpa = array(
      COL_KD_PEMDA=>$rdata[COL_KD_PEMDA],
      COL_KD_TAHUN=>$rdata[COL_KD_TAHUN],
      COL_KD_URUSAN=>$rdata[COL_KD_URUSAN],
      COL_KD_BIDANG=>$rdata[COL_KD_BIDANG],
      COL_KD_UNIT=>$rdata[COL_KD_UNIT],
      COL_KD_SUB=>$rdata[COL_KD_SUB],
      COL_KD_MISI=>$rdata[COL_KD_MISI],
      COL_KD_TUJUAN=>$rdata[COL_KD_TUJUAN],
      COL_KD_INDIKATORTUJUAN=>$rdata[COL_KD_INDIKATORTUJUAN],
      COL_KD_SASARAN=>$rdata[COL_KD_SASARAN],
      COL_KD_INDIKATORSASARAN=>$rdata[COL_KD_INDIKATORSASARAN],
      COL_KD_TUJUANOPD=>$rdata[COL_KD_TUJUANOPD],
      COL_KD_INDIKATORTUJUANOPD=>$rdata[COL_KD_INDIKATORTUJUANOPD],
      COL_KD_SASARANOPD=>$rdata[COL_KD_SASARANOPD],
      COL_KD_INDIKATORSASARANOPD=>$rdata[COL_KD_INDIKATORSASARANOPD],
      COL_KD_BID=>$rdata[COL_KD_BID],
      COL_KD_PROGRAMOPD=>$rdata[COL_KD_PROGRAMOPD],
      COL_KD_SASARANPROGRAMOPD=>$rdata[COL_KD_SASARANPROGRAMOPD],
      COL_KD_SUBBID=>$rdata[COL_KD_SUBBID],
      COL_KD_KEGIATANOPD=>$rdata[COL_KD_KEGIATANOPD]
    );
    $data['rdpa'] = $rdpa = $this->db->where($condDpa)->get(TBL_SAKIP_DPA_KEGIATAN)->row_array();

    $data['kdUrusan'] = $rdata[COL_KD_URUSAN];
    $data['kdBidang'] = $rdata[COL_KD_BIDANG];
    $data['kdUnit'] = $rdata[COL_KD_UNIT];
    $data['kdSub'] = $rdata[COL_KD_SUB];
    $data['kdBid'] = $rdata[COL_KD_BID];

    if(!empty($_POST)) {
      $rec = array(
        COL_KD_SASARANPROGRAMOPD=>$this->input->post(COL_KD_SASARANPROGRAMOPD),
        COL_KD_SUBBID=>$this->input->post(COL_KD_SUBBID),
        COL_KD_KEGIATANOPD=>$this->input->post(COL_KD_KEGIATANOPD),
        COL_NM_KEGIATANOPD=>$this->input->post(COL_NM_KEGIATANOPD),
        COL_KD_SUMBERDANA=>$this->input->post(COL_KD_SUMBERDANA)//,
        //COL_TOTAL=>$this->input->post(COL_TOTAL),
        //COL_REMARKS=>$this->input->post(COL_REMARKS)
      );

      $kdSubKegiatan = $this->input->post("KdSubKegiatan");
      $nmSubKegiatan = $this->input->post("NmSubKegiatan");
      $nmIndikatorSubKegiatan = $this->input->post("NmIndikatorSubKegiatan");
      $totalSubKegiatan = $this->input->post("TotalSubKegiatan");
      $arrSubKegiatan = [];
      for($i = 0; $i<count($kdSubKegiatan); $i++) {
        $arrSubKegiatan[] = array(
          'KdSubKegiatan' => $kdSubKegiatan[$i],
          'NmSubKegiatan' => $nmSubKegiatan[$i],
          'NmIndikatorSubKegiatan' => $nmIndikatorSubKegiatan[$i],
          'TotalSubKegiatan' => $totalSubKegiatan[$i]
        );
      }
      if(!empty($arrSubKegiatan)) {
        $rec[COL_NM_ARRSUBKEGIATAN]=json_encode($arrSubKegiatan);
      }

      $this->db->trans_begin();
      try {
        $res = $this->db
        ->where(COL_UNIQ, $uniq)
        ->update(TBL_SAKIP_MSUBBID_KEGIATAN, array_merge($rec, array(COL_TOTAL=>$this->input->post(COL_TOTAL), COL_REMARKS=>$this->input->post(COL_REMARKS))));
        if(!$res) {
          throw new Exception("Database error: ".$this->db->error());
        }

        if(!empty($rdpa)) {
          $res = $this->db
          ->where(array_merge($condDpa, array(COL_IS_RENJA=>1)))
          ->update(TBL_SAKIP_DPA_KEGIATAN, array_merge($rec, array(COL_BUDGET=>$this->input->post(COL_BUDGET))));
          if(!$res) {
            throw new Exception("Database error: ".$this->db->error());
          }
        }

      } catch(Exception $e) {
        $this->db->trans_rollback();
        ShowJsonError($e->getMessage());
        return;
      }

      $this->db->trans_commit();
      ShowJsonSuccess('Berhasil');
      return;
    } else {
      $this->load->view('renja/form_kegiatan_partial', $data);
    }
  }

  public function tambah_dpa_kegiatan($uniq, $uniqprog) {
    $data['data'] = $rdata = $this->db->where(COL_UNIQ, $uniq)->get(TBL_SAKIP_MSUBBID_KEGIATAN)->row_array();
    if(empty($rdata)) {
      echo 'Kegiatan tidak valid.';
      return;
    }

    $prgArr = array(
      COL_KD_PEMDA=>$rdata[COL_KD_PEMDA],
      COL_KD_TAHUN=>$rdata[COL_KD_TAHUN],
      COL_KD_URUSAN=>$rdata[COL_KD_URUSAN],
      COL_KD_BIDANG=>$rdata[COL_KD_BIDANG],
      COL_KD_UNIT=>$rdata[COL_KD_UNIT],
      COL_KD_SUB=>$rdata[COL_KD_SUB],
      COL_KD_MISI=>$rdata[COL_KD_MISI],
      COL_KD_TUJUAN=>$rdata[COL_KD_TUJUAN],
      COL_KD_INDIKATORTUJUAN=>$rdata[COL_KD_INDIKATORTUJUAN],
      COL_KD_SASARAN=>$rdata[COL_KD_SASARAN],
      COL_KD_INDIKATORSASARAN=>$rdata[COL_KD_INDIKATORSASARAN],
      COL_KD_TUJUANOPD=>$rdata[COL_KD_TUJUANOPD],
      COL_KD_INDIKATORTUJUANOPD=>$rdata[COL_KD_INDIKATORTUJUANOPD],
      COL_KD_SASARANOPD=>$rdata[COL_KD_SASARANOPD],
      COL_KD_INDIKATORSASARANOPD=>$rdata[COL_KD_INDIKATORSASARANOPD],
      COL_KD_BID=>$rdata[COL_KD_BID],
      COL_KD_PROGRAMOPD=>$rdata[COL_KD_PROGRAMOPD]
    );
    $mainArr = array_merge($prgArr, array(
      COL_KD_SASARANPROGRAMOPD=>$rdata[COL_KD_SASARANPROGRAMOPD],
      COL_KD_SUBBID=>$rdata[COL_KD_SUBBID],
      COL_KD_KEGIATANOPD=>$rdata[COL_KD_KEGIATANOPD]
    ));

    $data['program'] = $rprogram = $this->db->where($prgArr)->get(TBL_SAKIP_DPA_PROGRAM)->row_array();
    if(empty($rprogram)) {
      echo 'Program belum diinput dalam DPA.';
      return;
    }
    $rdpa = $this->db->where($mainArr)->get(TBL_SAKIP_DPA_KEGIATAN)->row_array();
    if(!empty($rdpa)) {
      echo 'Kegiatan ini sudah ada di DPA.';
      return;
    }

    $data['kdUrusan'] = $rdata[COL_KD_URUSAN];
    $data['kdBidang'] = $rdata[COL_KD_BIDANG];
    $data['kdUnit'] = $rdata[COL_KD_UNIT];
    $data['kdSub'] = $rdata[COL_KD_SUB];
    $data['kdBid'] = $rdata[COL_KD_BID];
    $data['dpa'] = true;

    if(!empty($_POST)) {
      $recSasaran = array();
      $recIndikator = array();
      $rec = array_merge($mainArr, array(
        COL_NM_KEGIATANOPD=>$rdata[COL_NM_KEGIATANOPD],
        COL_KD_SUMBERDANA=>$rdata[COL_KD_SUMBERDANA],
        COL_BUDGET=>$this->input->post(COL_TOTAL),
        COL_IS_RENJA=>1
      ));

      $rsasaran = $this->db->where($mainArr)->get(TBL_SAKIP_MSUBBID_KEGIATAN_SASARAN)->result_array();
      $rindikator = $this->db->where($mainArr)->get(TBL_SAKIP_MSUBBID_KEGIATAN_INDIKATOR)->result_array();
      foreach($rsasaran as $r) {
        $recSasaran[] = array_merge($mainArr, array(
          COL_KD_SASARANKEGIATANOPD=>$r[COL_KD_SASARANKEGIATANOPD],
          COL_NM_SASARANKEGIATANOPD=>$r[COL_NM_SASARANKEGIATANOPD]
        ));
      }
      foreach($rindikator as $r) {
        $recIndikator[] = array_merge($mainArr, array(
          COL_KD_SASARANKEGIATANOPD=>$r[COL_KD_SASARANKEGIATANOPD],
          COL_KD_INDIKATORKEGIATANOPD=>$r[COL_KD_INDIKATORKEGIATANOPD],
          COL_NM_INDIKATORKEGIATANOPD=>$r[COL_NM_INDIKATORKEGIATANOPD],
          COL_NM_FORMULA => $r[COL_NM_FORMULA],
          COL_NM_SUMBERDATA => $r[COL_NM_SUMBERDATA],
          COL_NM_PENANGGUNGJAWAB => $r[COL_NM_PENANGGUNGJAWAB],
          COL_KD_SATUAN => $r[COL_KD_SATUAN],
          COL_TARGET => $r[COL_TARGET]
        ));
      }

      $kdSubKegiatan = $this->input->post("KdSubKegiatan");
      $nmSubKegiatan = $this->input->post("NmSubKegiatan");
      $nmIndikatorSubKegiatan = $this->input->post("NmIndikatorSubKegiatan");
      $totalSubKegiatan = $this->input->post("TotalSubKegiatan");
      $arrSubKegiatan = [];
      for($i = 0; $i<count($kdSubKegiatan); $i++) {
        $arrSubKegiatan[] = array(
          'KdSubKegiatan' => $kdSubKegiatan[$i],
          'NmSubKegiatan' => $nmSubKegiatan[$i],
          'NmIndikatorSubKegiatan' => $nmIndikatorSubKegiatan[$i],
          'TotalSubKegiatan' => $totalSubKegiatan[$i]
        );
      }
      if(!empty($arrSubKegiatan)) {
        $rec[COL_NM_ARRSUBKEGIATAN]=json_encode($arrSubKegiatan);
      }

      $this->db->trans_begin();
      try {
        $res = $this->db->insert(TBL_SAKIP_DPA_KEGIATAN, $rec);
        if(!$res) {
          throw new Exception("Database error: ".$this->db->error());
        }

        if(!empty($recSasaran)) {
          $res = $this->db->insert_batch(TBL_SAKIP_DPA_KEGIATAN_SASARAN, $recSasaran);
          if(!$res) {
            throw new Exception("Database error: ".$this->db->error());
          }
        }

        if(!empty($recIndikator)) {
          $res = $this->db->insert_batch(TBL_SAKIP_DPA_KEGIATAN_INDIKATOR, $recIndikator);
          if(!$res) {
            throw new Exception("Database error: ".$this->db->error());
          }
        }
      } catch(Exception $e) {
        $this->db->trans_rollback();
        ShowJsonError($e->getMessage());
        return;
      }

      $this->db->trans_commit();
      ShowJsonSuccess('Berhasil');
      return;
    } else {
      $this->load->view('renja/form_kegiatan_partial', $data);
    }
  }

  public function hapus_kegiatan($uniq) {
    $rdata = $this->db->where(COL_UNIQ, $uniq)->get(TBL_SAKIP_MSUBBID_KEGIATAN)->row_array();
    if(empty($rdata)) {
      ShowJsonError('Kegiatan tidak ditemukan.');
      return;
    }

    $condDpa = array(
      COL_KD_PEMDA=>$rdata[COL_KD_PEMDA],
      COL_KD_TAHUN=>$rdata[COL_KD_TAHUN],
      COL_KD_URUSAN=>$rdata[COL_KD_URUSAN],
      COL_KD_BIDANG=>$rdata[COL_KD_BIDANG],
      COL_KD_UNIT=>$rdata[COL_KD_UNIT],
      COL_KD_SUB=>$rdata[COL_KD_SUB],
      COL_KD_MISI=>$rdata[COL_KD_MISI],
      COL_KD_TUJUAN=>$rdata[COL_KD_TUJUAN],
      COL_KD_INDIKATORTUJUAN=>$rdata[COL_KD_INDIKATORTUJUAN],
      COL_KD_SASARAN=>$rdata[COL_KD_SASARAN],
      COL_KD_INDIKATORSASARAN=>$rdata[COL_KD_INDIKATORSASARAN],
      COL_KD_TUJUANOPD=>$rdata[COL_KD_TUJUANOPD],
      COL_KD_INDIKATORTUJUANOPD=>$rdata[COL_KD_INDIKATORTUJUANOPD],
      COL_KD_SASARANOPD=>$rdata[COL_KD_SASARANOPD],
      COL_KD_INDIKATORSASARANOPD=>$rdata[COL_KD_INDIKATORSASARANOPD],

      COL_KD_BID=>$rdata[COL_KD_BID],
      COL_KD_PROGRAMOPD=>$rdata[COL_KD_PROGRAMOPD],
      COL_KD_SASARANPROGRAMOPD=>$rdata[COL_KD_SASARANPROGRAMOPD],
      COL_KD_SUBBID=>$rdata[COL_KD_SUBBID],
      COL_KD_KEGIATANOPD=>$rdata[COL_KD_KEGIATANOPD]
    );

    $this->db->trans_begin();
    try {
      $res = $this->db->where(COL_UNIQ, $uniq)->delete(TBL_SAKIP_MSUBBID_KEGIATAN);
      if(!$res) {
        throw new Exception("Database error:". $this->db->error());
      }

      $res = $this->db->where(array_merge($condDpa, array(COL_IS_RENJA=>1)))->delete(TBL_SAKIP_DPA_KEGIATAN);
      if(!$res) {
        throw new Exception("Database error:".$this->db->error());
      }
    } catch(Exception $e) {
      $this->db->trans_rollback();
      ShowJsonError($e->getMessage());
      return;
    }

    $this->db->trans_commit();
    ShowJsonSuccess('Berhasil');
  }

  public function list_sasaran_kegiatan($uniq) {
    $data['kegiatan'] = $rkegiatan = $this->db->where(COL_UNIQ, $uniq)->get(TBL_SAKIP_MSUBBID_KEGIATAN)->row_array();
    if(empty($rkegiatan)) {
      echo 'Kegiatan tidak valid.';
      return;
    }

    $cond = array(
      COL_KD_PEMDA=>$rkegiatan[COL_KD_PEMDA],
      COL_KD_TAHUN=>$rkegiatan[COL_KD_TAHUN],
      COL_KD_URUSAN=>$rkegiatan[COL_KD_URUSAN],
      COL_KD_BIDANG=>$rkegiatan[COL_KD_BIDANG],
      COL_KD_UNIT=>$rkegiatan[COL_KD_UNIT],
      COL_KD_SUB=>$rkegiatan[COL_KD_SUB],
      COL_KD_MISI=>$rkegiatan[COL_KD_MISI],
      COL_KD_TUJUAN=>$rkegiatan[COL_KD_TUJUAN],
      COL_KD_INDIKATORTUJUAN=>$rkegiatan[COL_KD_INDIKATORTUJUAN],
      COL_KD_SASARAN=>$rkegiatan[COL_KD_SASARAN],
      COL_KD_INDIKATORSASARAN=>$rkegiatan[COL_KD_INDIKATORSASARAN],
      COL_KD_TUJUANOPD=>$rkegiatan[COL_KD_TUJUANOPD],
      COL_KD_INDIKATORTUJUANOPD=>$rkegiatan[COL_KD_INDIKATORTUJUANOPD],
      COL_KD_SASARANOPD=>$rkegiatan[COL_KD_SASARANOPD],
      COL_KD_INDIKATORSASARANOPD=>$rkegiatan[COL_KD_INDIKATORSASARANOPD],
      COL_KD_BID=>$rkegiatan[COL_KD_BID],
      COL_KD_PROGRAMOPD=>$rkegiatan[COL_KD_PROGRAMOPD],
      COL_KD_SASARANPROGRAMOPD=>$rkegiatan[COL_KD_SASARANPROGRAMOPD],
      COL_KD_SUBBID=>$rkegiatan[COL_KD_SUBBID],
      COL_KD_KEGIATANOPD=>$rkegiatan[COL_KD_KEGIATANOPD]
    );
    $data['sasaran'] = $rsasaran = $this->db->where($cond)->get(TBL_SAKIP_MSUBBID_KEGIATAN_SASARAN)->result_array();
    $data['dpa'] = $rsasaran = $this->db->where($cond)->get(TBL_SAKIP_DPA_KEGIATAN)->result_array();
    $this->load->view('renja/index_kegiatan_sasaran', $data);
  }

  public function hapus_sasaran_kegiatan($uniq) {
    $res = $this->db->where(COL_UNIQ, $uniq)->delete(TBL_SAKIP_MSUBBID_KEGIATAN_SASARAN);
    if(!$res) {
      ShowJsonError($this->db->error());
      return;
    }
    ShowJsonSuccess('Berhasil');
  }

  public function tambah_sasaran_kegiatan($uniq) {
    $data['kegiatan'] = $rkegiatan = $this->db->where(COL_UNIQ, $uniq)->get(TBL_SAKIP_MSUBBID_KEGIATAN)->row_array();
    if(empty($rkegiatan)) {
      echo 'Kegiatan tidak valid.';
      return;
    }

    $condDpa = array(
      COL_KD_PEMDA=>$rkegiatan[COL_KD_PEMDA],
      COL_KD_TAHUN=>$rkegiatan[COL_KD_TAHUN],
      COL_KD_URUSAN=>$rkegiatan[COL_KD_URUSAN],
      COL_KD_BIDANG=>$rkegiatan[COL_KD_BIDANG],
      COL_KD_UNIT=>$rkegiatan[COL_KD_UNIT],
      COL_KD_SUB=>$rkegiatan[COL_KD_SUB],
      COL_KD_MISI=>$rkegiatan[COL_KD_MISI],
      COL_KD_TUJUAN=>$rkegiatan[COL_KD_TUJUAN],
      COL_KD_INDIKATORTUJUAN=>$rkegiatan[COL_KD_INDIKATORTUJUAN],
      COL_KD_SASARAN=>$rkegiatan[COL_KD_SASARAN],
      COL_KD_INDIKATORSASARAN=>$rkegiatan[COL_KD_INDIKATORSASARAN],
      COL_KD_TUJUANOPD=>$rkegiatan[COL_KD_TUJUANOPD],
      COL_KD_INDIKATORTUJUANOPD=>$rkegiatan[COL_KD_INDIKATORTUJUANOPD],
      COL_KD_SASARANOPD=>$rkegiatan[COL_KD_SASARANOPD],
      COL_KD_INDIKATORSASARANOPD=>$rkegiatan[COL_KD_INDIKATORSASARANOPD],

      COL_KD_BID=>$rkegiatan[COL_KD_BID],
      COL_KD_PROGRAMOPD=>$rkegiatan[COL_KD_PROGRAMOPD],
      COL_KD_SASARANPROGRAMOPD=>$rkegiatan[COL_KD_SASARANPROGRAMOPD],
      COL_KD_SUBBID=>$rkegiatan[COL_KD_SUBBID],
      COL_KD_KEGIATANOPD=>$rkegiatan[COL_KD_KEGIATANOPD]
    );
    $data['dpa'] = $rdpa = $this->db->where($condDpa)->get(TBL_SAKIP_DPA_KEGIATAN)->row_array();
    if(!empty($rdpa)) {
      echo 'Kegiatan sudah ada di DPA. Silakan hapus kegiatan dari DPA terlebih dahulu untuk menambah sasaran.';
      return;
    }

    if(!empty($_POST)) {
      $rec = array(
        COL_KD_PEMDA=>$rkegiatan[COL_KD_PEMDA],
        COL_KD_TAHUN=>$rkegiatan[COL_KD_TAHUN],
        COL_KD_URUSAN=>$rkegiatan[COL_KD_URUSAN],
        COL_KD_BIDANG=>$rkegiatan[COL_KD_BIDANG],
        COL_KD_UNIT=>$rkegiatan[COL_KD_UNIT],
        COL_KD_SUB=>$rkegiatan[COL_KD_SUB],
        COL_KD_MISI=>$rkegiatan[COL_KD_MISI],
        COL_KD_TUJUAN=>$rkegiatan[COL_KD_TUJUAN],
        COL_KD_INDIKATORTUJUAN=>$rkegiatan[COL_KD_INDIKATORTUJUAN],
        COL_KD_SASARAN=>$rkegiatan[COL_KD_SASARAN],
        COL_KD_INDIKATORSASARAN=>$rkegiatan[COL_KD_INDIKATORSASARAN],
        COL_KD_TUJUANOPD=>$rkegiatan[COL_KD_TUJUANOPD],
        COL_KD_INDIKATORTUJUANOPD=>$rkegiatan[COL_KD_INDIKATORTUJUANOPD],
        COL_KD_SASARANOPD=>$rkegiatan[COL_KD_SASARANOPD],
        COL_KD_INDIKATORSASARANOPD=>$rkegiatan[COL_KD_INDIKATORSASARANOPD],
        COL_KD_BID=>$rkegiatan[COL_KD_BID],
        COL_KD_PROGRAMOPD=>$rkegiatan[COL_KD_PROGRAMOPD],
        COL_KD_SASARANPROGRAMOPD=>$rkegiatan[COL_KD_SASARANPROGRAMOPD],
        COL_KD_SUBBID=>$rkegiatan[COL_KD_SUBBID],
        COL_KD_KEGIATANOPD=>$rkegiatan[COL_KD_KEGIATANOPD],
        COL_KD_SASARANKEGIATANOPD=>$this->input->post(COL_KD_SASARANKEGIATANOPD),
        COL_NM_SASARANKEGIATANOPD=>$this->input->post(COL_NM_SASARANKEGIATANOPD)
      );

      $this->db->trans_begin();
      try {
        if(!$this->db->insert(TBL_SAKIP_MSUBBID_KEGIATAN_SASARAN, $rec)){
            throw new Exception("Database error: ".$this->db->error());
        }

        $detNo = $this->input->post("NoDet");
        $detDesc = $this->input->post("KetDet");
        $detFormula = $this->input->post("FormulaDet");
        $detSumber = $this->input->post("SumberDet");
        $detPIC = $this->input->post("PICDet");
        $detSatuan = $this->input->post("SatuanDet");
        $detTarget = $this->input->post("TargetDet");
        $detAwal = $this->input->post("AwalDet");
        $detAkhir = $this->input->post("AkhirDet");
        $det = [];
        for($i = 0; $i<count($detNo); $i++) {
            $det[] = array(
              COL_KD_PEMDA=>$rkegiatan[COL_KD_PEMDA],
              COL_KD_TAHUN=>$rkegiatan[COL_KD_TAHUN],
              COL_KD_URUSAN=>$rkegiatan[COL_KD_URUSAN],
              COL_KD_BIDANG=>$rkegiatan[COL_KD_BIDANG],
              COL_KD_UNIT=>$rkegiatan[COL_KD_UNIT],
              COL_KD_SUB=>$rkegiatan[COL_KD_SUB],
              COL_KD_MISI=>$rkegiatan[COL_KD_MISI],
              COL_KD_TUJUAN=>$rkegiatan[COL_KD_TUJUAN],
              COL_KD_INDIKATORTUJUAN=>$rkegiatan[COL_KD_INDIKATORTUJUAN],
              COL_KD_SASARAN=>$rkegiatan[COL_KD_SASARAN],
              COL_KD_INDIKATORSASARAN=>$rkegiatan[COL_KD_INDIKATORSASARAN],
              COL_KD_TUJUANOPD=>$rkegiatan[COL_KD_TUJUANOPD],
              COL_KD_INDIKATORTUJUANOPD=>$rkegiatan[COL_KD_INDIKATORTUJUANOPD],
              COL_KD_SASARANOPD=>$rkegiatan[COL_KD_SASARANOPD],
              COL_KD_INDIKATORSASARANOPD=>$rkegiatan[COL_KD_INDIKATORSASARANOPD],
              COL_KD_BID=>$rkegiatan[COL_KD_BID],
              COL_KD_PROGRAMOPD=>$rkegiatan[COL_KD_PROGRAMOPD],
              COL_KD_SASARANPROGRAMOPD=>$rkegiatan[COL_KD_SASARANPROGRAMOPD],
              COL_KD_SUBBID=>$rkegiatan[COL_KD_SUBBID],
              COL_KD_KEGIATANOPD=>$rkegiatan[COL_KD_KEGIATANOPD],

              COL_KD_SASARANKEGIATANOPD => $this->input->post(COL_KD_SASARANKEGIATANOPD),
              COL_KD_INDIKATORKEGIATANOPD => $detNo[$i],
              COL_NM_INDIKATORKEGIATANOPD => $detDesc[$i],
              COL_NM_FORMULA => $detFormula[$i],
              COL_NM_SUMBERDATA => $detSumber[$i],
              COL_NM_PENANGGUNGJAWAB => $detPIC[$i],
              COL_KD_SATUAN => $detSatuan[$i],
              COL_TARGET => $detTarget[$i],
              COL_AWAL => $detAwal[$i],
              COL_AKHIR => $detAkhir[$i]
            );
        }
        if(count($det) > 0) {
          $res = $this->db->insert_batch(TBL_SAKIP_MSUBBID_KEGIATAN_INDIKATOR, $det);
          if(!$res) {
            throw new Exception("Database error: ".$this->db->error());
          }
        }

        $this->db->trans_commit();
        ShowJsonSuccess('Berhasil');
        return;
      } catch (Exception $e) {
          $this->db->trans_rollback();
          ShowJsonError($e->getMessage());
          return;
      }
    } else {
      $this->load->view('renja/form_kegiatan_sasaran_partial', $data);
    }
  }

  public function index_dpa($expandid='') {
    $ruser = GetLoggedUser();
    $period = $this->input->get('Period');
    $tahun = $this->input->get(COL_KD_TAHUN);
    $kdUrusan = $this->input->get(COL_KD_URUSAN);
    $kdBidang = $this->input->get(COL_KD_BIDANG);
    $kdUnit = $this->input->get(COL_KD_UNIT);
    $kdSub = $this->input->get(COL_KD_SUB);

    $data['title'] = 'DPA';
    $data['expandid'] = $expandid;

    if($ruser[COL_ROLEID] != ROLEADMIN && $ruser[COL_ROLEID] != ROLEBAPPEDA) {
      $strOPD = explode('.', $ruser[COL_COMPANYID]);
      $kdUrusan = $strOPD[0];
      $kdBidang = $strOPD[1];
      $kdUnit = $strOPD[2];
      $kdSub = $strOPD[3];
    }

    $data['iku'] = array();
    if(!empty($kdUrusan) && !empty($kdBidang) && !empty($kdUnit) && !empty($kdSub)) {
      if(!empty($period)) {
        $this->db->where(TBL_SAKIP_MPEMDA.'.'.COL_KD_PEMDA, $period);
      } else {
        $this->db->where(TBL_SAKIP_MPEMDA.'.'.COL_KD_TAHUN_FROM.'<=', date('Y'));
        $this->db->where(TBL_SAKIP_MPEMDA.'.'.COL_KD_TAHUN_TO.'>=', date('Y'));
      }

      $this->db->where(TBL_SAKIP_MOPD_IKTUJUAN.".".COL_KD_URUSAN, $kdUrusan);
      $this->db->where(TBL_SAKIP_MOPD_IKTUJUAN.".".COL_KD_BIDANG, $kdBidang);
      $this->db->where(TBL_SAKIP_MOPD_IKTUJUAN.".".COL_KD_UNIT, $kdUnit);
      $this->db->where(TBL_SAKIP_MOPD_IKTUJUAN.".".COL_KD_SUB, $kdSub);

      $data['iku'] = $this->db
      ->select('sakip_mopd_iktujuan.*,
                sakip_mpemda.*,
                sakip_mpmd_misi.Nm_Misi,
                sakip_mpmd_tujuan.Nm_Tujuan,
                sakip_mpmd_iktujuan.Nm_IndikatorTujuan,
                sakip_mpmd_sasaran.Nm_Sasaran,
                sakip_mpmd_iksasaran.Nm_IndikatorSasaran,
                sakip_mopd_tujuan.Nm_TujuanOPD')
      ->join(TBL_SAKIP_MPEMDA,TBL_SAKIP_MPEMDA.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_MOPD_IKTUJUAN.".".COL_KD_PEMDA,"left")
      ->join(TBL_SAKIP_MPMD_MISI,TBL_SAKIP_MPMD_MISI.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_MOPD_IKTUJUAN.".".COL_KD_PEMDA." AND ".TBL_SAKIP_MPMD_MISI.'.'.COL_KD_MISI." = ".TBL_SAKIP_MOPD_IKTUJUAN.".".COL_KD_MISI,"left")
      ->join(TBL_SAKIP_MPMD_TUJUAN,
              TBL_SAKIP_MPMD_TUJUAN.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_MOPD_IKTUJUAN.".".COL_KD_PEMDA." AND ".
              TBL_SAKIP_MPMD_TUJUAN.'.'.COL_KD_MISI." = ".TBL_SAKIP_MOPD_IKTUJUAN.".".COL_KD_MISI." AND ".
              TBL_SAKIP_MPMD_TUJUAN.'.'.COL_KD_TUJUAN." = ".TBL_SAKIP_MOPD_IKTUJUAN.".".COL_KD_TUJUAN
            ,"left")
      ->join(TBL_SAKIP_MPMD_IKTUJUAN,
              TBL_SAKIP_MPMD_IKTUJUAN.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_MOPD_IKTUJUAN.".".COL_KD_PEMDA." AND ".
              TBL_SAKIP_MPMD_IKTUJUAN.'.'.COL_KD_MISI." = ".TBL_SAKIP_MOPD_IKTUJUAN.".".COL_KD_MISI." AND ".
              TBL_SAKIP_MPMD_IKTUJUAN.'.'.COL_KD_TUJUAN." = ".TBL_SAKIP_MOPD_IKTUJUAN.".".COL_KD_TUJUAN." AND ".
              TBL_SAKIP_MPMD_IKTUJUAN.'.'.COL_KD_INDIKATORTUJUAN." = ".TBL_SAKIP_MOPD_IKTUJUAN.".".COL_KD_INDIKATORTUJUAN
            ,"left")
      ->join(TBL_SAKIP_MPMD_SASARAN,
              TBL_SAKIP_MPMD_SASARAN.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_MOPD_IKTUJUAN.".".COL_KD_PEMDA." AND ".
              TBL_SAKIP_MPMD_SASARAN.'.'.COL_KD_MISI." = ".TBL_SAKIP_MOPD_IKTUJUAN.".".COL_KD_MISI." AND ".
              TBL_SAKIP_MPMD_SASARAN.'.'.COL_KD_TUJUAN." = ".TBL_SAKIP_MOPD_IKTUJUAN.".".COL_KD_TUJUAN." AND ".
              TBL_SAKIP_MPMD_SASARAN.'.'.COL_KD_INDIKATORTUJUAN." = ".TBL_SAKIP_MOPD_IKTUJUAN.".".COL_KD_INDIKATORTUJUAN." AND ".
              TBL_SAKIP_MPMD_SASARAN.'.'.COL_KD_SASARAN." = ".TBL_SAKIP_MOPD_IKTUJUAN.".".COL_KD_SASARAN
            ,"left")
      ->join(TBL_SAKIP_MPMD_IKSASARAN,
              TBL_SAKIP_MPMD_IKSASARAN.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_MOPD_IKTUJUAN.".".COL_KD_PEMDA." AND ".
              TBL_SAKIP_MPMD_IKSASARAN.'.'.COL_KD_MISI." = ".TBL_SAKIP_MOPD_IKTUJUAN.".".COL_KD_MISI." AND ".
              TBL_SAKIP_MPMD_IKSASARAN.'.'.COL_KD_TUJUAN." = ".TBL_SAKIP_MOPD_IKTUJUAN.".".COL_KD_TUJUAN." AND ".
              TBL_SAKIP_MPMD_IKSASARAN.'.'.COL_KD_INDIKATORTUJUAN." = ".TBL_SAKIP_MOPD_IKTUJUAN.".".COL_KD_INDIKATORTUJUAN." AND ".
              TBL_SAKIP_MPMD_IKSASARAN.'.'.COL_KD_SASARAN." = ".TBL_SAKIP_MOPD_IKTUJUAN.".".COL_KD_SASARAN." AND ".
              TBL_SAKIP_MPMD_IKSASARAN.'.'.COL_KD_INDIKATORSASARAN." = ".TBL_SAKIP_MOPD_IKTUJUAN.".".COL_KD_INDIKATORSASARAN
            ,"left")
      ->join(TBL_SAKIP_MOPD_TUJUAN,
              TBL_SAKIP_MOPD_TUJUAN.'.'.COL_KD_URUSAN." = ".TBL_SAKIP_MOPD_IKTUJUAN.".".COL_KD_URUSAN." AND ".
              TBL_SAKIP_MOPD_TUJUAN.'.'.COL_KD_BIDANG." = ".TBL_SAKIP_MOPD_IKTUJUAN.".".COL_KD_BIDANG." AND ".
              TBL_SAKIP_MOPD_TUJUAN.'.'.COL_KD_UNIT." = ".TBL_SAKIP_MOPD_IKTUJUAN.".".COL_KD_UNIT." AND ".
              TBL_SAKIP_MOPD_TUJUAN.'.'.COL_KD_SUB." = ".TBL_SAKIP_MOPD_IKTUJUAN.".".COL_KD_SUB." AND ".

              TBL_SAKIP_MOPD_TUJUAN.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_MOPD_IKTUJUAN.".".COL_KD_PEMDA." AND ".
              TBL_SAKIP_MOPD_TUJUAN.'.'.COL_KD_MISI." = ".TBL_SAKIP_MOPD_IKTUJUAN.".".COL_KD_MISI." AND ".
              TBL_SAKIP_MOPD_TUJUAN.'.'.COL_KD_TUJUAN." = ".TBL_SAKIP_MOPD_IKTUJUAN.".".COL_KD_TUJUAN." AND ".
              TBL_SAKIP_MOPD_TUJUAN.'.'.COL_KD_INDIKATORTUJUAN." = ".TBL_SAKIP_MOPD_IKTUJUAN.".".COL_KD_INDIKATORTUJUAN." AND ".
              TBL_SAKIP_MOPD_TUJUAN.'.'.COL_KD_SASARAN." = ".TBL_SAKIP_MOPD_IKTUJUAN.".".COL_KD_SASARAN." AND ".
              TBL_SAKIP_MOPD_TUJUAN.'.'.COL_KD_INDIKATORSASARAN." = ".TBL_SAKIP_MOPD_IKTUJUAN.".".COL_KD_INDIKATORSASARAN." AND ".
              TBL_SAKIP_MOPD_TUJUAN.'.'.COL_KD_TUJUANOPD." = ".TBL_SAKIP_MOPD_IKTUJUAN.".".COL_KD_TUJUANOPD
            ,"left")

      ->order_by(TBL_SAKIP_MOPD_IKTUJUAN.'.'.COL_KD_PEMDA)
      ->order_by(TBL_SAKIP_MOPD_IKTUJUAN.'.'.COL_KD_MISI)
      ->order_by(TBL_SAKIP_MOPD_IKTUJUAN.'.'.COL_KD_TUJUAN)
      ->order_by(TBL_SAKIP_MOPD_IKTUJUAN.'.'.COL_KD_INDIKATORTUJUAN)
      ->order_by(TBL_SAKIP_MOPD_IKTUJUAN.'.'.COL_KD_SASARAN)
      ->order_by(TBL_SAKIP_MOPD_IKTUJUAN.'.'.COL_KD_INDIKATORSASARAN)
      ->order_by(TBL_SAKIP_MOPD_IKTUJUAN.'.'.COL_KD_TUJUANOPD)
      ->order_by(TBL_SAKIP_MOPD_IKTUJUAN.'.'.COL_KD_INDIKATORTUJUANOPD)
      ->get(TBL_SAKIP_MOPD_IKTUJUAN)
      ->result_array();
    }

    $data['period'] = $period;
    $data['tahun'] = !empty($tahun) ? $tahun : date('Y');
    $this->load->view('renja/index_dpa', $data);
  }

  public function index_dpa_load($id) {
    $key = explode('.', $id);
    $cond = array(
      COL_KD_PEMDA=>$key[0],
      COL_KD_TAHUN=>$key[1],
      COL_KD_URUSAN=>$key[2],
      COL_KD_BIDANG=>$key[3],
      COL_KD_UNIT=>$key[4],
      COL_KD_SUB=>$key[5],
      COL_KD_MISI=>$key[6],
      COL_KD_TUJUAN=>$key[7],
      COL_KD_INDIKATORTUJUAN=>$key[8],
      COL_KD_SASARAN=>$key[9],
      COL_KD_INDIKATORSASARAN=>$key[10],
      COL_KD_TUJUANOPD=>$key[11],
      COL_KD_INDIKATORTUJUANOPD=>$key[12],
      COL_KD_SASARANOPD=>$key[13],
      COL_KD_INDIKATORSASARANOPD=>$key[14]
    );

    $data['rprogram'] = $rprogram = $this->db
    ->where($cond)
    ->order_by(TBL_SAKIP_DPA_PROGRAM.'.'.COL_KD_PROGRAMOPD, 'asc')
    ->get(TBL_SAKIP_DPA_PROGRAM)
    ->result_array();
    $data['expid'] = $this->input->post('expid');

    $this->load->view('renja/index_dpa_program', $data);
  }

  public function tambah_dpa_prog($id) {
    $key = explode('.', $id);
    $data['kdUrusan'] = $key[2];
    $data['kdBidang'] = $key[3];
    $data['kdUnit'] = $key[4];
    $data['kdSub'] = $key[5];
    $data['id'] = $id;

    $mainArr = array(
      COL_KD_PEMDA=>$key[0],
      COL_KD_TAHUN=>$key[1],
      COL_KD_URUSAN=>$key[2],
      COL_KD_BIDANG=>$key[3],
      COL_KD_UNIT=>$key[4],
      COL_KD_SUB=>$key[5],
      COL_KD_MISI=>$key[6],
      COL_KD_TUJUAN=>$key[7],
      COL_KD_INDIKATORTUJUAN=>$key[8],
      COL_KD_SASARAN=>$key[9],
      COL_KD_INDIKATORSASARAN=>$key[10],
      COL_KD_TUJUANOPD=>$key[11],
      COL_KD_INDIKATORTUJUANOPD=>$key[12],
      COL_KD_SASARANOPD=>$key[13],
      COL_KD_INDIKATORSASARANOPD=>$key[14]
    );

    if(!empty($_POST)) {
      $recSasaran = array();
      $recIndikator = array();
      $rec = array_merge($mainArr, array(
        COL_IS_RENJA=>!empty($this->input->post(COL_IS_RENJA)) ? 1 : 0,
        COL_KD_BID=>$this->input->post(COL_KD_BID),
        COL_KD_PROGRAMOPD=>$this->input->post(COL_KD_PROGRAMOPD),
        COL_NM_PROGRAMOPD=>$this->input->post(COL_NM_PROGRAMOPD)
      ));

      if($rec[COL_IS_RENJA]) {
        $rrenja = $this->db
        ->where(array_merge($mainArr, array(COL_KD_BID=>$rec[COL_KD_BID], COL_KD_PROGRAMOPD=>$rec[COL_KD_PROGRAMOPD])))
        ->get(TBL_SAKIP_MBID_PROGRAM)
        ->row_array();

        if(empty($rrenja)) {
          ShowJsonError('Renja tidak ditemukan.');
          return;
        }

        $rec[COL_NM_PROGRAMOPD] = $rrenja[COL_NM_PROGRAMOPD];

        $rsasaran = $this->db
        ->where(array_merge($mainArr, array(COL_KD_BID=>$rec[COL_KD_BID], COL_KD_PROGRAMOPD=>$rec[COL_KD_PROGRAMOPD])))
        ->get(TBL_SAKIP_MBID_PROGRAM_SASARAN)
        ->result_array();

        $rindikator = $this->db
        ->where(array_merge($mainArr, array(COL_KD_BID=>$rec[COL_KD_BID], COL_KD_PROGRAMOPD=>$rec[COL_KD_PROGRAMOPD])))
        ->get(TBL_SAKIP_MBID_PROGRAM_INDIKATOR)
        ->result_array();

        foreach($rsasaran as $r) {
          $recSasaran[] = array_merge($mainArr, array(
            COL_KD_BID=>$rec[COL_KD_BID],
            COL_KD_PROGRAMOPD=>$rec[COL_KD_PROGRAMOPD],
            COL_KD_SASARANPROGRAMOPD=>$r[COL_KD_SASARANPROGRAMOPD],
            COL_NM_SASARANPROGRAMOPD=>$r[COL_NM_SASARANPROGRAMOPD]
          ));
        }
        foreach($rindikator as $r) {
          $recIndikator[] = array_merge($mainArr, array(
            COL_KD_BID=>$rec[COL_KD_BID],
            COL_KD_PROGRAMOPD=>$rec[COL_KD_PROGRAMOPD],
            COL_KD_SASARANPROGRAMOPD=>$r[COL_KD_SASARANPROGRAMOPD],
            COL_KD_INDIKATORPROGRAMOPD=>$r[COL_KD_INDIKATORPROGRAMOPD],
            COL_NM_INDIKATORPROGRAMOPD=>$r[COL_NM_INDIKATORPROGRAMOPD],
            COL_NM_FORMULA => $r[COL_NM_FORMULA],
            COL_NM_SUMBERDATA => $r[COL_NM_SUMBERDATA],
            COL_NM_PENANGGUNGJAWAB => $r[COL_NM_PENANGGUNGJAWAB],
            COL_KD_SATUAN => $r[COL_KD_SATUAN],
            COL_TARGET => $r[COL_TARGET]
          ));
        }
      }

      $this->db->trans_begin();
      try {
        $res = $this->db->insert(TBL_SAKIP_DPA_PROGRAM, $rec);
        if(!$res) {
          throw new Exception("Database error: ".$this->db->error());
        }

        if(!empty($recSasaran)) {
          $res = $this->db->insert_batch(TBL_SAKIP_DPA_PROGRAM_SASARAN, $recSasaran);
          if(!$res) {
            throw new Exception("Database error: ".$this->db->error());
          }
        }

        if(!empty($recIndikator)) {
          $res = $this->db->insert_batch(TBL_SAKIP_DPA_PROGRAM_INDIKATOR, $recIndikator);
          if(!$res) {
            throw new Exception("Database error: ".$this->db->error());
          }
        }
      } catch(Exception $e) {
        $this->db->trans_rollback();
        ShowJsonError($e->getMessage());
        return;
      }

      $this->db->trans_commit();
      ShowJsonSuccess('Berhasil');
      return;
    } else {
      $this->load->view('renja/form_dpa_program_partial', $data);
    }
  }

  public function hapus_dpa_program($uniq) {
    $rdata = $this->db->where(COL_UNIQ, $uniq)->get(TBL_SAKIP_DPA_PROGRAM)->row_array();
    if(empty($rdata)) {
      ShowJsonError('Program tidak ditemukan.');
      return;
    }

    $condDpa = array(
      COL_KD_PEMDA=>$rdata[COL_KD_PEMDA],
      COL_KD_TAHUN=>$rdata[COL_KD_TAHUN],
      COL_KD_URUSAN=>$rdata[COL_KD_URUSAN],
      COL_KD_BIDANG=>$rdata[COL_KD_BIDANG],
      COL_KD_UNIT=>$rdata[COL_KD_UNIT],
      COL_KD_SUB=>$rdata[COL_KD_SUB],
      COL_KD_MISI=>$rdata[COL_KD_MISI],
      COL_KD_TUJUAN=>$rdata[COL_KD_TUJUAN],
      COL_KD_INDIKATORTUJUAN=>$rdata[COL_KD_INDIKATORTUJUAN],
      COL_KD_SASARAN=>$rdata[COL_KD_SASARAN],
      COL_KD_INDIKATORSASARAN=>$rdata[COL_KD_INDIKATORSASARAN],
      COL_KD_TUJUANOPD=>$rdata[COL_KD_TUJUANOPD],
      COL_KD_INDIKATORTUJUANOPD=>$rdata[COL_KD_INDIKATORTUJUANOPD],
      COL_KD_SASARANOPD=>$rdata[COL_KD_SASARANOPD],
      COL_KD_INDIKATORSASARANOPD=>$rdata[COL_KD_INDIKATORSASARANOPD],

      COL_KD_BID=>$rdata[COL_KD_BID],
      COL_KD_PROGRAMOPD=>$rdata[COL_KD_PROGRAMOPD]
    );

    $rkegiatan = $this->db->where($condDpa)->get(TBL_SAKIP_DPA_KEGIATAN)->row_array();
    if(!empty($rkegiatan)) {
      ShowJsonError('Silakan hapus kegiatan terlebih dahulu.');
      return;
    }

    $this->db->trans_begin();
    try {
      $res = $this->db->where(COL_UNIQ, $uniq)->delete(TBL_SAKIP_DPA_PROGRAM);
      if(!$res) {
        throw new Exception("Database error:". $this->db->error());
      }
    } catch(Exception $e) {
      $this->db->trans_rollback();
      ShowJsonError($e->getMessage());
      return;
    }

    $this->db->trans_commit();
    ShowJsonSuccess('Berhasil');
  }

  public function list_dpa_sasaran_program($uniq) {
    $data['program'] = $rprogram = $this->db->where(COL_UNIQ, $uniq)->get(TBL_SAKIP_DPA_PROGRAM)->row_array();
    if(empty($rprogram)) {
      echo 'Program tidak valid.';
      return;
    }

    $cond = array(
      COL_KD_PEMDA=>$rprogram[COL_KD_PEMDA],
      COL_KD_TAHUN=>$rprogram[COL_KD_TAHUN],
      COL_KD_URUSAN=>$rprogram[COL_KD_URUSAN],
      COL_KD_BIDANG=>$rprogram[COL_KD_BIDANG],
      COL_KD_UNIT=>$rprogram[COL_KD_UNIT],
      COL_KD_SUB=>$rprogram[COL_KD_SUB],
      COL_KD_MISI=>$rprogram[COL_KD_MISI],
      COL_KD_TUJUAN=>$rprogram[COL_KD_TUJUAN],
      COL_KD_INDIKATORTUJUAN=>$rprogram[COL_KD_INDIKATORTUJUAN],
      COL_KD_SASARAN=>$rprogram[COL_KD_SASARAN],
      COL_KD_INDIKATORSASARAN=>$rprogram[COL_KD_INDIKATORSASARAN],
      COL_KD_TUJUANOPD=>$rprogram[COL_KD_TUJUANOPD],
      COL_KD_INDIKATORTUJUANOPD=>$rprogram[COL_KD_INDIKATORTUJUANOPD],
      COL_KD_SASARANOPD=>$rprogram[COL_KD_SASARANOPD],
      COL_KD_INDIKATORSASARANOPD=>$rprogram[COL_KD_INDIKATORSASARANOPD],
      COL_KD_BID=>$rprogram[COL_KD_BID],
      COL_KD_PROGRAMOPD=>$rprogram[COL_KD_PROGRAMOPD]
    );
    $data['sasaran'] = $rsasaran = $this->db->where($cond)->get(TBL_SAKIP_DPA_PROGRAM_SASARAN)->result_array();
    $this->load->view('renja/index_dpa_program_sasaran', $data);
  }

  public function hapus_dpa_sasaran_program($uniq) {
    $res = $this->db->where(COL_UNIQ, $uniq)->delete(TBL_SAKIP_DPA_PROGRAM_SASARAN);
    if(!$res) {
      ShowJsonError($this->db->error());
      return;
    }
    ShowJsonSuccess('Berhasil');
  }

  public function ubah_dpa_program($uniq, $id) {
    $data['data'] = $rdata = $this->db->where(COL_UNIQ, $uniq)->get(TBL_SAKIP_DPA_PROGRAM)->row_array();
    if(empty($rdata)) {
      echo 'Program tidak valid.';
      return;
    }

    $data['id'] = $id;
    $data['cond'] = $cond = array(
      COL_KD_PEMDA=>$rdata[COL_KD_PEMDA],
      COL_KD_TAHUN=>$rdata[COL_KD_TAHUN],
      COL_KD_URUSAN=>$rdata[COL_KD_URUSAN],
      COL_KD_BIDANG=>$rdata[COL_KD_BIDANG],
      COL_KD_UNIT=>$rdata[COL_KD_UNIT],
      COL_KD_SUB=>$rdata[COL_KD_SUB],
      COL_KD_MISI=>$rdata[COL_KD_MISI],
      COL_KD_TUJUAN=>$rdata[COL_KD_TUJUAN],
      COL_KD_INDIKATORTUJUAN=>$rdata[COL_KD_INDIKATORTUJUAN],
      COL_KD_SASARAN=>$rdata[COL_KD_SASARAN],
      COL_KD_INDIKATORSASARAN=>$rdata[COL_KD_INDIKATORSASARAN],
      COL_KD_TUJUANOPD=>$rdata[COL_KD_TUJUANOPD],
      COL_KD_INDIKATORTUJUANOPD=>$rdata[COL_KD_INDIKATORTUJUANOPD],
      COL_KD_SASARANOPD=>$rdata[COL_KD_SASARANOPD],
      COL_KD_INDIKATORSASARANOPD=>$rdata[COL_KD_INDIKATORSASARANOPD]
    );

    $data['kdUrusan'] = $rdata[COL_KD_URUSAN];
    $data['kdBidang'] = $rdata[COL_KD_BIDANG];
    $data['kdUnit'] = $rdata[COL_KD_UNIT];
    $data['kdSub'] = $rdata[COL_KD_SUB];

    if(!empty($_POST)) {
      $rec = array(
        COL_IS_RENJA=>!empty($this->input->post(COL_IS_RENJA)) ? 1 : 0,
        COL_KD_BID=>$this->input->post(COL_KD_BID),
        COL_KD_PROGRAMOPD=>$this->input->post(COL_KD_PROGRAMOPD),
        COL_NM_PROGRAMOPD=>$this->input->post(COL_NM_PROGRAMOPD)
      );

      if($rec[COL_IS_RENJA]) {
        $rrenja = $this->db
        ->where(array_merge($cond, array(COL_KD_BID=>$rec[COL_KD_BID], COL_KD_PROGRAMOPD=>$rec[COL_KD_PROGRAMOPD])))
        ->get(TBL_SAKIP_MBID_PROGRAM)
        ->row_array();

        if(empty($rrenja)) {
          ShowJsonError('Renja tidak ditemukan.');
          return;
        }
        $rec[COL_NM_PROGRAMOPD] = $rrenja[COL_NM_PROGRAMOPD];
      }

      $this->db->trans_begin();
      try {
        $res = $this->db->where(COL_UNIQ, $uniq)->update(TBL_SAKIP_DPA_PROGRAM, $rec);
        if(!$res) {
          throw new Exception("Database error: ".$this->db->error());
        }

      } catch(Exception $e) {
        $this->db->trans_rollback();
        ShowJsonError($e->getMessage());
        return;
      }

      $this->db->trans_commit();
      ShowJsonSuccess('Berhasil');
      return;
    } else {
      $this->load->view('renja/form_dpa_program_partial', $data);
    }
  }

  public function tambah_dpa_sasaran_program($uniq) {
    $data['program'] = $rprogram = $this->db->where(COL_UNIQ, $uniq)->get(TBL_SAKIP_DPA_PROGRAM)->row_array();
    if(empty($rprogram)) {
      echo 'Program tidak valid.';
      return;
    }

    if(!empty($_POST)) {
      $rec = array(
        COL_KD_PEMDA=>$rprogram[COL_KD_PEMDA],
        COL_KD_TAHUN=>$rprogram[COL_KD_TAHUN],
        COL_KD_URUSAN=>$rprogram[COL_KD_URUSAN],
        COL_KD_BIDANG=>$rprogram[COL_KD_BIDANG],
        COL_KD_UNIT=>$rprogram[COL_KD_UNIT],
        COL_KD_SUB=>$rprogram[COL_KD_SUB],
        COL_KD_MISI=>$rprogram[COL_KD_MISI],
        COL_KD_TUJUAN=>$rprogram[COL_KD_TUJUAN],
        COL_KD_INDIKATORTUJUAN=>$rprogram[COL_KD_INDIKATORTUJUAN],
        COL_KD_SASARAN=>$rprogram[COL_KD_SASARAN],
        COL_KD_INDIKATORSASARAN=>$rprogram[COL_KD_INDIKATORSASARAN],
        COL_KD_TUJUANOPD=>$rprogram[COL_KD_TUJUANOPD],
        COL_KD_INDIKATORTUJUANOPD=>$rprogram[COL_KD_INDIKATORTUJUANOPD],
        COL_KD_SASARANOPD=>$rprogram[COL_KD_SASARANOPD],
        COL_KD_INDIKATORSASARANOPD=>$rprogram[COL_KD_INDIKATORSASARANOPD],
        COL_KD_BID=>$rprogram[COL_KD_BID],
        COL_KD_PROGRAMOPD=>$rprogram[COL_KD_PROGRAMOPD],
        COL_KD_SASARANPROGRAMOPD=>$this->input->post(COL_KD_SASARANPROGRAMOPD),
        COL_NM_SASARANPROGRAMOPD=>$this->input->post(COL_NM_SASARANPROGRAMOPD)
      );

      $this->db->trans_begin();
      try {
        if(!$this->db->insert(TBL_SAKIP_DPA_PROGRAM_SASARAN, $rec)){
            throw new Exception("Database error: ".$this->db->error());
        }

        $detNo = $this->input->post("NoDet");
        $detDesc = $this->input->post("KetDet");
        $detFormula = $this->input->post("FormulaDet");
        $detSumber = $this->input->post("SumberDet");
        $detPIC = $this->input->post("PICDet");
        $detSatuan = $this->input->post("SatuanDet");
        $detTarget = $this->input->post("TargetDet");
        $detAwal = $this->input->post("AwalDet");
        $detAkhir = $this->input->post("AkhirDet");
        $det = [];
        for($i = 0; $i<count($detNo); $i++) {
            $det[] = array(
              COL_KD_PEMDA=>$rprogram[COL_KD_PEMDA],
              COL_KD_TAHUN=>$rprogram[COL_KD_TAHUN],
              COL_KD_URUSAN=>$rprogram[COL_KD_URUSAN],
              COL_KD_BIDANG=>$rprogram[COL_KD_BIDANG],
              COL_KD_UNIT=>$rprogram[COL_KD_UNIT],
              COL_KD_SUB=>$rprogram[COL_KD_SUB],
              COL_KD_MISI=>$rprogram[COL_KD_MISI],
              COL_KD_TUJUAN=>$rprogram[COL_KD_TUJUAN],
              COL_KD_INDIKATORTUJUAN=>$rprogram[COL_KD_INDIKATORTUJUAN],
              COL_KD_SASARAN=>$rprogram[COL_KD_SASARAN],
              COL_KD_INDIKATORSASARAN=>$rprogram[COL_KD_INDIKATORSASARAN],
              COL_KD_TUJUANOPD=>$rprogram[COL_KD_TUJUANOPD],
              COL_KD_INDIKATORTUJUANOPD=>$rprogram[COL_KD_INDIKATORTUJUANOPD],
              COL_KD_SASARANOPD=>$rprogram[COL_KD_SASARANOPD],
              COL_KD_INDIKATORSASARANOPD=>$rprogram[COL_KD_INDIKATORSASARANOPD],
              COL_KD_BID=>$rprogram[COL_KD_BID],
              COL_KD_PROGRAMOPD=>$rprogram[COL_KD_PROGRAMOPD],

              COL_KD_SASARANPROGRAMOPD => $this->input->post(COL_KD_SASARANPROGRAMOPD),
              COL_KD_INDIKATORPROGRAMOPD => $detNo[$i],
              COL_NM_INDIKATORPROGRAMOPD => $detDesc[$i],
              COL_NM_FORMULA => $detFormula[$i],
              COL_NM_SUMBERDATA => $detSumber[$i],
              COL_NM_PENANGGUNGJAWAB => $detPIC[$i],
              COL_KD_SATUAN => $detSatuan[$i],
              COL_TARGET => $detTarget[$i],
              /*COL_AWAL => $detAwal[$i],
              COL_AKHIR => $detAkhir[$i]*/
            );
        }
        if(count($det) > 0) {
          $res = $this->db->insert_batch(TBL_SAKIP_DPA_PROGRAM_INDIKATOR, $det);
          if(!$res) {
              throw new Exception("Database error: ".$this->db->error());
          }
        }

        $this->db->trans_commit();
        ShowJsonSuccess('Berhasil');
        return;
      } catch (Exception $e) {
          $this->db->trans_rollback();
          ShowJsonError($e->getMessage());
          return;
      }
    } else {
      $this->load->view('renja/form_dpa_program_sasaran_partial', $data);
    }
  }

  public function tambah_dpa_keg($uniq, $id) {
    $data['program'] = $rprogram = $this->db->where(COL_UNIQ, $uniq)->get(TBL_SAKIP_DPA_PROGRAM)->row_array();
    if(empty($rprogram)) {
      echo 'Program tidak valid.';
      return;
    }
    $data['id'] = $id;

    $data['kdUrusan'] = $rprogram[COL_KD_URUSAN];
    $data['kdBidang'] = $rprogram[COL_KD_BIDANG];
    $data['kdUnit'] = $rprogram[COL_KD_UNIT];
    $data['kdSub'] = $rprogram[COL_KD_SUB];
    $data['kdBid'] = $rprogram[COL_KD_BID];

    $mainArr = array(
      COL_KD_PEMDA=>$rprogram[COL_KD_PEMDA],
      COL_KD_TAHUN=>$rprogram[COL_KD_TAHUN],
      COL_KD_URUSAN=>$rprogram[COL_KD_URUSAN],
      COL_KD_BIDANG=>$rprogram[COL_KD_BIDANG],
      COL_KD_UNIT=>$rprogram[COL_KD_UNIT],
      COL_KD_SUB=>$rprogram[COL_KD_SUB],
      COL_KD_MISI=>$rprogram[COL_KD_MISI],
      COL_KD_TUJUAN=>$rprogram[COL_KD_TUJUAN],
      COL_KD_INDIKATORTUJUAN=>$rprogram[COL_KD_INDIKATORTUJUAN],
      COL_KD_SASARAN=>$rprogram[COL_KD_SASARAN],
      COL_KD_INDIKATORSASARAN=>$rprogram[COL_KD_INDIKATORSASARAN],
      COL_KD_TUJUANOPD=>$rprogram[COL_KD_TUJUANOPD],
      COL_KD_INDIKATORTUJUANOPD=>$rprogram[COL_KD_INDIKATORTUJUANOPD],
      COL_KD_SASARANOPD=>$rprogram[COL_KD_SASARANOPD],
      COL_KD_INDIKATORSASARANOPD=>$rprogram[COL_KD_INDIKATORSASARANOPD],
      COL_KD_PROGRAMOPD=>$rprogram[COL_KD_PROGRAMOPD],
      COL_KD_BID=>$rprogram[COL_KD_BID]
    );

    if(!empty($_POST)) {
      $recSasaran = array();
      $recIndikator = array();
      $rec = array_merge($mainArr, array(
        COL_IS_RENJA=>!empty($this->input->post(COL_IS_RENJA)) ? 1 : 0,
        COL_KD_SASARANPROGRAMOPD=>$this->input->post(COL_KD_SASARANPROGRAMOPD),
        COL_KD_SUBBID=>$this->input->post(COL_KD_SUBBID),
        COL_KD_KEGIATANOPD=>$this->input->post(COL_KD_KEGIATANOPD),
        COL_NM_KEGIATANOPD=>$this->input->post(COL_NM_KEGIATANOPD),
        COL_KD_SUMBERDANA=>$this->input->post(COL_KD_SUMBERDANA),
        COL_BUDGET=>$this->input->post(COL_BUDGET)
      ));

      if($rec[COL_IS_RENJA]) {
        $rrenja = $this->db
        ->where(array_merge($mainArr, array(COL_KD_SASARANPROGRAMOPD=>$rec[COL_KD_SASARANPROGRAMOPD], COL_KD_SUBBID=>$rec[COL_KD_SUBBID], COL_KD_KEGIATANOPD=>$rec[COL_KD_KEGIATANOPD])))
        ->get(TBL_SAKIP_MSUBBID_KEGIATAN)
        ->row_array();

        if(empty($rrenja)) {
          ShowJsonError('Renja tidak ditemukan.');
          return;
        }

        $rec[COL_NM_KEGIATANOPD] = $rrenja[COL_NM_KEGIATANOPD];

        $rsasaran = $this->db
        ->where(array_merge($mainArr, array(COL_KD_SASARANPROGRAMOPD=>$rec[COL_KD_SASARANPROGRAMOPD], COL_KD_SUBBID=>$rec[COL_KD_SUBBID], COL_KD_KEGIATANOPD=>$rec[COL_KD_KEGIATANOPD])))
        ->get(TBL_SAKIP_MSUBBID_KEGIATAN_SASARAN)
        ->result_array();

        $rindikator = $this->db
        ->where(array_merge($mainArr, array(COL_KD_SASARANPROGRAMOPD=>$rec[COL_KD_SASARANPROGRAMOPD], COL_KD_SUBBID=>$rec[COL_KD_SUBBID], COL_KD_KEGIATANOPD=>$rec[COL_KD_KEGIATANOPD])))
        ->get(TBL_SAKIP_MSUBBID_KEGIATAN_INDIKATOR)
        ->result_array();

        foreach($rsasaran as $r) {
          $recSasaran[] = array_merge($mainArr, array(
            COL_KD_SASARANPROGRAMOPD=>$rec[COL_KD_SASARANPROGRAMOPD],
            COL_KD_SUBBID=>$rec[COL_KD_SUBBID],
            COL_KD_KEGIATANOPD=>$rec[COL_KD_KEGIATANOPD],
            COL_KD_SASARANKEGIATANOPD=>$r[COL_KD_SASARANKEGIATANOPD],
            COL_NM_SASARANKEGIATANOPD=>$r[COL_NM_SASARANKEGIATANOPD]
          ));
        }
        foreach($rindikator as $r) {
          $recIndikator[] = array_merge($mainArr, array(
            COL_KD_SASARANPROGRAMOPD=>$rec[COL_KD_SASARANPROGRAMOPD],
            COL_KD_SUBBID=>$rec[COL_KD_SUBBID],
            COL_KD_KEGIATANOPD=>$rec[COL_KD_KEGIATANOPD],
            COL_KD_SASARANKEGIATANOPD=>$r[COL_KD_SASARANKEGIATANOPD],
            COL_KD_INDIKATORKEGIATANOPD=>$r[COL_KD_INDIKATORKEGIATANOPD],
            COL_NM_INDIKATORKEGIATANOPD=>$r[COL_NM_INDIKATORKEGIATANOPD],
            COL_NM_FORMULA => $r[COL_NM_FORMULA],
            COL_NM_SUMBERDATA => $r[COL_NM_SUMBERDATA],
            COL_NM_PENANGGUNGJAWAB => $r[COL_NM_PENANGGUNGJAWAB],
            COL_KD_SATUAN => $r[COL_KD_SATUAN],
            COL_TARGET => $r[COL_TARGET]
          ));
        }
      } else {
        $kdSubKegiatan = $this->input->post("KdSubKegiatan");
        $nmSubKegiatan = $this->input->post("NmSubKegiatan");
        $nmIndikatorSubKegiatan = $this->input->post("NmIndikatorSubKegiatan");
        $totalSubKegiatan = $this->input->post("TotalSubKegiatan");
        $arrSubKegiatan = [];
        for($i = 0; $i<count($kdSubKegiatan); $i++) {
          $arrSubKegiatan[] = array(
            'KdSubKegiatan' => $kdSubKegiatan[$i],
            'NmSubKegiatan' => $nmSubKegiatan[$i],
            'NmIndikatorSubKegiatan' => $nmIndikatorSubKegiatan[$i],
            'TotalSubKegiatan' => $totalSubKegiatan[$i]
          );
        }
        if(!empty($arrSubKegiatan)) {
          $rec[COL_NM_ARRSUBKEGIATAN]=json_encode($arrSubKegiatan);
        }
      }

      $this->db->trans_begin();
      try {
        $res = $this->db->insert(TBL_SAKIP_DPA_KEGIATAN, $rec);
        if(!$res) {
          throw new Exception("Database error: ".$this->db->error());
        }

        if(!empty($recSasaran)) {
          $res = $this->db->insert_batch(TBL_SAKIP_DPA_KEGIATAN_SASARAN, $recSasaran);
          if(!$res) {
            throw new Exception("Database error: ".$this->db->error());
          }
        }

        if(!empty($recIndikator)) {
          $res = $this->db->insert_batch(TBL_SAKIP_DPA_KEGIATAN_INDIKATOR, $recIndikator);
          if(!$res) {
            throw new Exception("Database error: ".$this->db->error());
          }
        }
      } catch(Exception $e) {
        $this->db->trans_rollback();
        ShowJsonError($e->getMessage());
        return;
      }

      $this->db->trans_commit();
      ShowJsonSuccess('Berhasil');
      return;
    } else {
      $this->load->view('renja/form_dpa_kegiatan_partial', $data);
    }
  }

  public function hapus_dpa_kegiatan($uniq) {
    $rdata = $this->db->where(COL_UNIQ, $uniq)->get(TBL_SAKIP_DPA_KEGIATAN)->row_array();
    if(empty($rdata)) {
      ShowJsonError('Kegiatan tidak ditemukan.');
      return;
    }

    $this->db->trans_begin();
    try {
      $res = $this->db->where(COL_UNIQ, $uniq)->delete(TBL_SAKIP_DPA_KEGIATAN);
      if(!$res) {
        throw new Exception("Database error:". $this->db->error());
      }
    } catch(Exception $e) {
      $this->db->trans_rollback();
      ShowJsonError($e->getMessage());
      return;
    }

    $this->db->trans_commit();
    ShowJsonSuccess('Berhasil');
  }

  public function ubah_dpa_keg($uniq, $id) {
    $data['data'] = $rdata = $this->db->where(COL_UNIQ, $uniq)->get(TBL_SAKIP_DPA_KEGIATAN)->row_array();
    if(empty($rdata)) {
      echo 'Program tidak valid.';
      return;
    }

    $data['id'] = $id;
    $data['cond'] = $cond = array(
      COL_KD_PEMDA=>$rdata[COL_KD_PEMDA],
      COL_KD_TAHUN=>$rdata[COL_KD_TAHUN],
      COL_KD_URUSAN=>$rdata[COL_KD_URUSAN],
      COL_KD_BIDANG=>$rdata[COL_KD_BIDANG],
      COL_KD_UNIT=>$rdata[COL_KD_UNIT],
      COL_KD_SUB=>$rdata[COL_KD_SUB],
      COL_KD_MISI=>$rdata[COL_KD_MISI],
      COL_KD_TUJUAN=>$rdata[COL_KD_TUJUAN],
      COL_KD_INDIKATORTUJUAN=>$rdata[COL_KD_INDIKATORTUJUAN],
      COL_KD_SASARAN=>$rdata[COL_KD_SASARAN],
      COL_KD_INDIKATORSASARAN=>$rdata[COL_KD_INDIKATORSASARAN],
      COL_KD_TUJUANOPD=>$rdata[COL_KD_TUJUANOPD],
      COL_KD_INDIKATORTUJUANOPD=>$rdata[COL_KD_INDIKATORTUJUANOPD],
      COL_KD_SASARANOPD=>$rdata[COL_KD_SASARANOPD],
      COL_KD_INDIKATORSASARANOPD=>$rdata[COL_KD_INDIKATORSASARANOPD],
      COL_KD_PROGRAMOPD=>$rdata[COL_KD_PROGRAMOPD],
      COL_KD_BID=>$rdata[COL_KD_BID],
      COL_KD_SASARANPROGRAMOPD=>$rdata[COL_KD_SASARANPROGRAMOPD]
    );

    $data['program'] = $mainArr = array(
      COL_KD_PEMDA=>$rdata[COL_KD_PEMDA],
      COL_KD_TAHUN=>$rdata[COL_KD_TAHUN],
      COL_KD_URUSAN=>$rdata[COL_KD_URUSAN],
      COL_KD_BIDANG=>$rdata[COL_KD_BIDANG],
      COL_KD_UNIT=>$rdata[COL_KD_UNIT],
      COL_KD_SUB=>$rdata[COL_KD_SUB],
      COL_KD_MISI=>$rdata[COL_KD_MISI],
      COL_KD_TUJUAN=>$rdata[COL_KD_TUJUAN],
      COL_KD_INDIKATORTUJUAN=>$rdata[COL_KD_INDIKATORTUJUAN],
      COL_KD_SASARAN=>$rdata[COL_KD_SASARAN],
      COL_KD_INDIKATORSASARAN=>$rdata[COL_KD_INDIKATORSASARAN],
      COL_KD_TUJUANOPD=>$rdata[COL_KD_TUJUANOPD],
      COL_KD_INDIKATORTUJUANOPD=>$rdata[COL_KD_INDIKATORTUJUANOPD],
      COL_KD_SASARANOPD=>$rdata[COL_KD_SASARANOPD],
      COL_KD_INDIKATORSASARANOPD=>$rdata[COL_KD_INDIKATORSASARANOPD],
      COL_KD_PROGRAMOPD=>$rdata[COL_KD_PROGRAMOPD],
      COL_KD_BID=>$rdata[COL_KD_BID]
    );

    $data['kdUrusan'] = $rdata[COL_KD_URUSAN];
    $data['kdBidang'] = $rdata[COL_KD_BIDANG];
    $data['kdUnit'] = $rdata[COL_KD_UNIT];
    $data['kdSub'] = $rdata[COL_KD_SUB];
    $data['kdBid'] = $rdata[COL_KD_BID];

    if(!empty($_POST)) {
      $rec = array_merge($mainArr, array(
        COL_IS_RENJA=>!empty($this->input->post(COL_IS_RENJA)) ? 1 : 0,
        COL_KD_SASARANPROGRAMOPD=>$this->input->post(COL_KD_SASARANPROGRAMOPD),
        COL_KD_SUBBID=>$this->input->post(COL_KD_SUBBID),
        COL_KD_KEGIATANOPD=>$this->input->post(COL_KD_KEGIATANOPD),
        COL_NM_KEGIATANOPD=>$this->input->post(COL_NM_KEGIATANOPD),
        COL_KD_SUMBERDANA=>$this->input->post(COL_KD_SUMBERDANA),
        COL_BUDGET=>$this->input->post(COL_BUDGET),
        COL_PERGESERAN=>$this->input->post(COL_PERGESERAN)
      ));

      if($rec[COL_IS_RENJA]) {
        $rrenja = $this->db
        ->where(array_merge($mainArr, array(COL_KD_SASARANPROGRAMOPD=>$rec[COL_KD_SASARANPROGRAMOPD], COL_KD_SUBBID=>$rec[COL_KD_SUBBID], COL_KD_KEGIATANOPD=>$rec[COL_KD_KEGIATANOPD])))
        ->get(TBL_SAKIP_MSUBBID_KEGIATAN)
        ->row_array();

        if(empty($rrenja)) {
          ShowJsonError('Renja tidak ditemukan.');
          return;
        }

        $rec[COL_NM_KEGIATANOPD] = $rrenja[COL_NM_KEGIATANOPD];
      }

      $kdSubKegiatan = $this->input->post("KdSubKegiatan");
      $nmSubKegiatan = $this->input->post("NmSubKegiatan");
      $nmIndikatorSubKegiatan = $this->input->post("NmIndikatorSubKegiatan");
      $totalSubKegiatan = $this->input->post("TotalSubKegiatan");
      $arrSubKegiatan = [];
      for($i = 0; $i<count($kdSubKegiatan); $i++) {
        $arrSubKegiatan[] = array(
          'KdSubKegiatan' => $kdSubKegiatan[$i],
          'NmSubKegiatan' => $nmSubKegiatan[$i],
          'NmIndikatorSubKegiatan' => $nmIndikatorSubKegiatan[$i],
          'TotalSubKegiatan' => $totalSubKegiatan[$i]
        );
      }
      if(!empty($arrSubKegiatan)) {
        $rec[COL_NM_ARRSUBKEGIATAN]=json_encode($arrSubKegiatan);
      }

      $this->db->trans_begin();
      try {
        $res = $this->db->where(COL_UNIQ, $uniq)->update(TBL_SAKIP_DPA_KEGIATAN, $rec);
        if(!$res) {
          throw new Exception("Database error: ".$this->db->error());
        }

      } catch(Exception $e) {
        $this->db->trans_rollback();
        ShowJsonError($e->getMessage());
        return;
      }

      $this->db->trans_commit();
      ShowJsonSuccess('Berhasil');
      return;
    } else {
      $this->load->view('renja/form_dpa_kegiatan_partial', $data);
    }
  }

  public function list_dpa_sasaran_kegiatan($uniq) {
    $data['kegiatan'] = $rkegiatan = $this->db->where(COL_UNIQ, $uniq)->get(TBL_SAKIP_DPA_KEGIATAN)->row_array();
    if(empty($rkegiatan)) {
      echo 'Kegiatan tidak valid.';
      return;
    }

    $cond = array(
      COL_KD_PEMDA=>$rkegiatan[COL_KD_PEMDA],
      COL_KD_TAHUN=>$rkegiatan[COL_KD_TAHUN],
      COL_KD_URUSAN=>$rkegiatan[COL_KD_URUSAN],
      COL_KD_BIDANG=>$rkegiatan[COL_KD_BIDANG],
      COL_KD_UNIT=>$rkegiatan[COL_KD_UNIT],
      COL_KD_SUB=>$rkegiatan[COL_KD_SUB],
      COL_KD_MISI=>$rkegiatan[COL_KD_MISI],
      COL_KD_TUJUAN=>$rkegiatan[COL_KD_TUJUAN],
      COL_KD_INDIKATORTUJUAN=>$rkegiatan[COL_KD_INDIKATORTUJUAN],
      COL_KD_SASARAN=>$rkegiatan[COL_KD_SASARAN],
      COL_KD_INDIKATORSASARAN=>$rkegiatan[COL_KD_INDIKATORSASARAN],
      COL_KD_TUJUANOPD=>$rkegiatan[COL_KD_TUJUANOPD],
      COL_KD_INDIKATORTUJUANOPD=>$rkegiatan[COL_KD_INDIKATORTUJUANOPD],
      COL_KD_SASARANOPD=>$rkegiatan[COL_KD_SASARANOPD],
      COL_KD_INDIKATORSASARANOPD=>$rkegiatan[COL_KD_INDIKATORSASARANOPD],
      COL_KD_BID=>$rkegiatan[COL_KD_BID],
      COL_KD_PROGRAMOPD=>$rkegiatan[COL_KD_PROGRAMOPD],
      COL_KD_SASARANPROGRAMOPD=>$rkegiatan[COL_KD_SASARANPROGRAMOPD],
      COL_KD_SUBBID=>$rkegiatan[COL_KD_SUBBID],
      COL_KD_KEGIATANOPD=>$rkegiatan[COL_KD_KEGIATANOPD]
    );
    $data['sasaran'] = $rsasaran = $this->db->where($cond)->get(TBL_SAKIP_DPA_KEGIATAN_SASARAN)->result_array();
    $this->load->view('renja/index_dpa_kegiatan_sasaran', $data);
  }

  public function hapus_dpa_sasaran_kegiatan($uniq) {
    $res = $this->db->where(COL_UNIQ, $uniq)->delete(TBL_SAKIP_DPA_KEGIATAN_SASARAN);
    if(!$res) {
      ShowJsonError($this->db->error());
      return;
    }
    ShowJsonSuccess('Berhasil');
  }

  public function tambah_dpa_sasaran_kegiatan($uniq) {
    $data['kegiatan'] = $rkegiatan = $this->db->where(COL_UNIQ, $uniq)->get(TBL_SAKIP_DPA_KEGIATAN)->row_array();
    if(empty($rkegiatan)) {
      echo 'Kegiatan tidak valid.';
      return;
    }

    if(!empty($_POST)) {
      $rec = array(
        COL_KD_PEMDA=>$rkegiatan[COL_KD_PEMDA],
        COL_KD_TAHUN=>$rkegiatan[COL_KD_TAHUN],
        COL_KD_URUSAN=>$rkegiatan[COL_KD_URUSAN],
        COL_KD_BIDANG=>$rkegiatan[COL_KD_BIDANG],
        COL_KD_UNIT=>$rkegiatan[COL_KD_UNIT],
        COL_KD_SUB=>$rkegiatan[COL_KD_SUB],
        COL_KD_MISI=>$rkegiatan[COL_KD_MISI],
        COL_KD_TUJUAN=>$rkegiatan[COL_KD_TUJUAN],
        COL_KD_INDIKATORTUJUAN=>$rkegiatan[COL_KD_INDIKATORTUJUAN],
        COL_KD_SASARAN=>$rkegiatan[COL_KD_SASARAN],
        COL_KD_INDIKATORSASARAN=>$rkegiatan[COL_KD_INDIKATORSASARAN],
        COL_KD_TUJUANOPD=>$rkegiatan[COL_KD_TUJUANOPD],
        COL_KD_INDIKATORTUJUANOPD=>$rkegiatan[COL_KD_INDIKATORTUJUANOPD],
        COL_KD_SASARANOPD=>$rkegiatan[COL_KD_SASARANOPD],
        COL_KD_INDIKATORSASARANOPD=>$rkegiatan[COL_KD_INDIKATORSASARANOPD],
        COL_KD_BID=>$rkegiatan[COL_KD_BID],
        COL_KD_PROGRAMOPD=>$rkegiatan[COL_KD_PROGRAMOPD],
        COL_KD_SASARANPROGRAMOPD=>$rkegiatan[COL_KD_SASARANPROGRAMOPD],
        COL_KD_SUBBID=>$rkegiatan[COL_KD_SUBBID],
        COL_KD_KEGIATANOPD=>$rkegiatan[COL_KD_KEGIATANOPD],
        COL_KD_SASARANKEGIATANOPD=>$this->input->post(COL_KD_SASARANKEGIATANOPD),
        COL_NM_SASARANKEGIATANOPD=>$this->input->post(COL_NM_SASARANKEGIATANOPD)
      );

      $this->db->trans_begin();
      try {
        if(!$this->db->insert(TBL_SAKIP_DPA_KEGIATAN_SASARAN, $rec)){
            throw new Exception("Database error: ".$this->db->error());
        }

        $detNo = $this->input->post("NoDet");
        $detDesc = $this->input->post("KetDet");
        $detFormula = $this->input->post("FormulaDet");
        $detSumber = $this->input->post("SumberDet");
        $detPIC = $this->input->post("PICDet");
        $detSatuan = $this->input->post("SatuanDet");
        $detTarget = $this->input->post("TargetDet");
        $det = [];
        for($i = 0; $i<count($detNo); $i++) {
            $det[] = array(
              COL_KD_PEMDA=>$rkegiatan[COL_KD_PEMDA],
              COL_KD_TAHUN=>$rkegiatan[COL_KD_TAHUN],
              COL_KD_URUSAN=>$rkegiatan[COL_KD_URUSAN],
              COL_KD_BIDANG=>$rkegiatan[COL_KD_BIDANG],
              COL_KD_UNIT=>$rkegiatan[COL_KD_UNIT],
              COL_KD_SUB=>$rkegiatan[COL_KD_SUB],
              COL_KD_MISI=>$rkegiatan[COL_KD_MISI],
              COL_KD_TUJUAN=>$rkegiatan[COL_KD_TUJUAN],
              COL_KD_INDIKATORTUJUAN=>$rkegiatan[COL_KD_INDIKATORTUJUAN],
              COL_KD_SASARAN=>$rkegiatan[COL_KD_SASARAN],
              COL_KD_INDIKATORSASARAN=>$rkegiatan[COL_KD_INDIKATORSASARAN],
              COL_KD_TUJUANOPD=>$rkegiatan[COL_KD_TUJUANOPD],
              COL_KD_INDIKATORTUJUANOPD=>$rkegiatan[COL_KD_INDIKATORTUJUANOPD],
              COL_KD_SASARANOPD=>$rkegiatan[COL_KD_SASARANOPD],
              COL_KD_INDIKATORSASARANOPD=>$rkegiatan[COL_KD_INDIKATORSASARANOPD],
              COL_KD_BID=>$rkegiatan[COL_KD_BID],
              COL_KD_PROGRAMOPD=>$rkegiatan[COL_KD_PROGRAMOPD],
              COL_KD_SASARANPROGRAMOPD=>$rkegiatan[COL_KD_SASARANPROGRAMOPD],
              COL_KD_KEGIATANOPD=>$rkegiatan[COL_KD_KEGIATANOPD],
              COL_KD_SUBBID=>$rkegiatan[COL_KD_SUBBID],

              COL_KD_SASARANKEGIATANOPD => $this->input->post(COL_KD_SASARANKEGIATANOPD),
              COL_KD_INDIKATORKEGIATANOPD => $detNo[$i],
              COL_NM_INDIKATORKEGIATANOPD => $detDesc[$i],
              COL_NM_FORMULA => $detFormula[$i],
              COL_NM_SUMBERDATA => $detSumber[$i],
              COL_NM_PENANGGUNGJAWAB => $detPIC[$i],
              COL_KD_SATUAN => $detSatuan[$i],
              COL_TARGET => $detTarget[$i]
            );
        }
        if(count($det) > 0) {
          $res = $this->db->insert_batch(TBL_SAKIP_DPA_KEGIATAN_INDIKATOR, $det);
          if(!$res) {
              throw new Exception("Database error: ".$this->db->error());
          }
        }

        $this->db->trans_commit();
        ShowJsonSuccess('Berhasil');
        return;
      } catch (Exception $e) {
          $this->db->trans_rollback();
          ShowJsonError($e->getMessage());
          return;
      }
    } else {
      $this->load->view('renja/form_dpa_kegiatan_sasaran_partial', $data);
    }
  }
}
