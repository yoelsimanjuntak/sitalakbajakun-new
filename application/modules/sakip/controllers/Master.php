<?php
/**
 * Created by PhpStorm.
 * User: Partopi Tao
 * Date: 3/14/2019
 * Time: 10:10 PM
 */
class Master extends MY_Controller {
    function __construct() {
        parent::__construct();
        if(!IsLogin() || (GetLoggedUser()[COL_ROLEID] != ROLEADMIN)) {
            redirect('sakip/user/dashboard');
        }
    }

    function uom() {
        $data['title'] = 'Satuan';
        $this->db->order_by(COL_NM_SATUAN, 'desc');
        $data['res'] = $this->db->get(TBL_SAKIP_MSATUAN)->result_array();
        $this->load->view('master/uom', $data);
    }

    function uom_add() {
        $data['title'] = "Satuan";
        $data['edit'] = FALSE;

        if(!empty($_POST)){
            $ruser = GetLoggedUser();
            $resp = array();
            $resp['error'] = 0;
            $resp['success'] = 1;
            $resp['redirect'] = site_url('master/uom');
            $data = array(
                COL_KD_SATUAN => $this->input->post(COL_KD_SATUAN),
                COL_NM_SATUAN => $this->input->post(COL_NM_SATUAN),
                COL_CREATE_BY => $ruser[COL_USERNAME],
                COL_CREATE_DATE => date("Y-m-d H:i:s")
            );
            if(!$this->db->insert(TBL_SAKIP_MSATUAN, $data)){
                $resp['error'] = "Gagal menambah data";
                $resp['success'] = 0;
            }
            echo json_encode($resp);
        }else{
            $this->load->view('master/uom_form',$data);
        }
    }

    function uom_edit($id) {
        $rdata = $data['data'] = $this->db->where(COL_UNIQ, $id)->get(TBL_SAKIP_MSATUAN)->row_array();
        if(empty($rdata)){
            show_404();
            return;
        }

        $data['title'] = "Satuan";
        $data['edit'] = TRUE;
        if(!empty($_POST)){
            $ruser = GetLoggedUser();
            $resp = array();
            $resp['error'] = 0;
            $resp['success'] = 1;
            $resp['redirect'] = site_url('master/uom');
            try {
                $data = array(
                    COL_KD_SATUAN => $this->input->post(COL_KD_SATUAN),
                    COL_NM_SATUAN => $this->input->post(COL_NM_SATUAN),
                    COL_EDIT_BY => $ruser[COL_USERNAME],
                    COL_EDIT_DATE => date("Y-m-d H:i:s")
                );
                if(!$this->db->where(COL_UNIQ, $id)->update(TBL_SAKIP_MSATUAN, $data)) {
                    $resp['error'] = "Gagal mengubah data";
                    $resp['success'] = 0;
                }
            } catch (Exception $e) {
                $resp['error'] = $e->getMessage();
                $resp['success'] = 0;
            }
            echo json_encode($resp);
        }else {
            $this->load->view('master/uom_form',$data);
        }
    }

    function uom_del(){
        $data = $this->input->post('cekbox');
        $deleted = 0;
        foreach ($data as $datum) {
            $this->db->delete(TBL_SAKIP_MSATUAN, array(COL_UNIQ => $datum));
            $deleted++;
        }
        if($deleted){
            ShowJsonSuccess($deleted." data dihapus");
        }else{
            ShowJsonError("Tidak ada dihapus");
        }
    }

    function sumberdana() {
        $data['title'] = 'Sumber Dana';
        $this->db->order_by(COL_NM_SUMBERDANA, 'desc');
        $data['res'] = $this->db->get(TBL_SAKIP_MSUMBERDANA)->result_array();
        $this->load->view('master/sumberdana', $data);
    }

    function sumberdana_add() {
        $data['title'] = "Sumber Dana";
        $data['edit'] = FALSE;

        if(!empty($_POST)){
            $ruser = GetLoggedUser();
            $resp = array();
            $resp['error'] = 0;
            $resp['success'] = 1;
            $resp['redirect'] = site_url('master/sumberdana');
            $data = array(
                COL_KD_SUMBERDANA => $this->input->post(COL_KD_SUMBERDANA),
                COL_NM_SUMBERDANA => $this->input->post(COL_NM_SUMBERDANA),
                COL_CREATE_BY => $ruser[COL_USERNAME],
                COL_CREATE_DATE => date("Y-m-d H:i:s")
            );
            if(!$this->db->insert(TBL_SAKIP_MSUMBERDANA, $data)){
                $resp['error'] = "Gagal menambah data";
                $resp['success'] = 0;
            }
            echo json_encode($resp);
        }else{
            $this->load->view('master/sumberdana_form',$data);
        }
    }

    function sumberdana_edit($id) {
        $rdata = $data['data'] = $this->db->where(COL_UNIQ, $id)->get(TBL_SAKIP_MSUMBERDANA)->row_array();
        if(empty($rdata)){
            show_404();
            return;
        }

        $data['title'] = "Sumber Dana";
        $data['edit'] = TRUE;
        if(!empty($_POST)){
            $ruser = GetLoggedUser();
            $resp = array();
            $resp['error'] = 0;
            $resp['success'] = 1;
            $resp['redirect'] = site_url('master/sumberdana');
            try {
                $data = array(
                    COL_KD_SUMBERDANA => $this->input->post(COL_KD_SUMBERDANA),
                    COL_NM_SUMBERDANA => $this->input->post(COL_NM_SUMBERDANA),
                    COL_EDIT_BY => $ruser[COL_USERNAME],
                    COL_EDIT_DATE => date("Y-m-d H:i:s")
                );
                if(!$this->db->where(COL_UNIQ, $id)->update(TBL_SAKIP_MSUMBERDANA, $data)) {
                    $resp['error'] = "Gagal mengubah data";
                    $resp['success'] = 0;
                }
            } catch (Exception $e) {
                $resp['error'] = $e->getMessage();
                $resp['success'] = 0;
            }
            echo json_encode($resp);
        }else {
            $this->load->view('master/sumberdana_form',$data);
        }
    }

    function sumberdana_del(){
        $data = $this->input->post('cekbox');
        $deleted = 0;
        foreach ($data as $datum) {
            $this->db->delete(TBL_SAKIP_MSUMBERDANA, array(COL_UNIQ => $datum));
            $deleted++;
        }
        if($deleted){
            ShowJsonSuccess($deleted." data dihapus");
        }else{
            ShowJsonError("Tidak ada dihapus");
        }
    }
}
