<?php
/**
 * Created by PhpStorm.
 * User: Yoel Simanjuntak
 * Date: 29/09/2018
 * Time: 22:23
 */
class Mpemda extends MY_Controller {
    function __construct() {
        parent::__construct();
        if(!IsLogin() || (GetLoggedUser()[COL_ROLEID] != ROLEADMIN && GetLoggedUser()[COL_ROLEID] != ROLEBAPPEDA)) {
            redirect('sakip/user/dashboard');
        }
    }

    function period() {
        $data['title'] = 'Periode, Visi dan Misi';
        $this->db->order_by(COL_KD_TAHUN_FROM, 'desc');
        $data['res'] = $this->db->get(TBL_SAKIP_MPEMDA)->result_array();
        $this->load->view('mpemda/period', $data);
    }

    function period_add() {
        $data['title'] = "Periode, Visi dan Misi";
        $data['edit'] = FALSE;

        if(!empty($_POST)){
            $ruser = GetLoggedUser();
            $resp = array();
            $resp['error'] = 0;
            $resp['success'] = 1;
            $resp['redirect'] = site_url('mpemda/period');
            $data = array(
                COL_KD_PROV => GLOBAL_KD_PROV,
                COL_KD_KAB => GLOBAL_KD_KAB,
                COL_KD_TAHUN_FROM => $this->input->post(COL_KD_TAHUN_FROM),
                COL_KD_TAHUN_TO => $this->input->post(COL_KD_TAHUN_TO),
                COL_NM_PEJABAT => $this->input->post(COL_NM_PEJABAT),
                COL_NM_VISI => $this->input->post(COL_NM_VISI)
            );
            if(!$this->db->insert(TBL_SAKIP_MPEMDA, $data)){
                $resp['error'] = 1;
                $resp['success'] = 0;
            }

            $kdPemda = $this->db->insert_id();
            $misiNo = $this->input->post("NoMisi");
            $misiKet = $this->input->post("KetMisi");
            $misi = [];
            for($i = 0; $i<count($misiNo); $i++) {
                $misi[] = array(
                    COL_KD_PEMDA => $kdPemda,
                    COL_KD_MISI => $misiNo[$i],
                    COL_NM_MISI => $misiKet[$i]
                );
            }
            if(count($misi) > 0) $this->db->insert_batch(TBL_SAKIP_MPMD_MISI, $misi);
            echo json_encode($resp);
        }else{
            $this->load->view('mpemda/period_form',$data);
        }
    }
    function period_edit($id) {
        $rdata = $data['data'] = $this->db->where(COL_KD_PEMDA, $id)->get(TBL_SAKIP_MPEMDA)->row_array();
        if(empty($rdata)){
            show_404();
            return;
        }

        $data['title'] = "Periode, Visi dan Misi";;
        $data['edit'] = TRUE;
        if(!empty($_POST)){
            $ruser = GetLoggedUser();
            $resp = array();
            $resp['error'] = 0;
            $resp['success'] = 1;
            $resp['redirect'] = site_url('mpemda/period');
            try {
                $data = array(
                    COL_KD_TAHUN_FROM => $this->input->post(COL_KD_TAHUN_FROM),
                    COL_KD_TAHUN_TO => $this->input->post(COL_KD_TAHUN_TO),
                    COL_NM_PEJABAT => $this->input->post(COL_NM_PEJABAT),
                    COL_NM_VISI => $this->input->post(COL_NM_VISI)
                );
                if(!$this->db->where(COL_KD_PEMDA, $id)->update(TBL_SAKIP_MPEMDA, $data)){
                    $resp['error'] = 1;
                    $resp['success'] = 0;
                }

                //$this->db->delete(TBL_SAKIP_MPMD_MISI, array(COL_KD_PEMDA => $id));

                $detNo = $this->input->post("NoMisi");
                $detKet = $this->input->post("KetMisi");
                $arrDet = [];

                /* update / delete */
                $det = $this->db
                    ->select(COL_KD_MISI)
                    ->where(COL_KD_PEMDA, $id)
                    ->get(TBL_SAKIP_MPMD_MISI)
                    ->result_array();
                $detUpdated = [];
                foreach($det as $d) {
                    $arrCond = array(COL_KD_PEMDA=>$id, COL_KD_MISI=>$d[COL_KD_MISI]);
                    if(in_array($d[COL_KD_MISI], $detNo)) {
                        $this->db
                            ->where($arrCond)
                            ->update(TBL_SAKIP_MPMD_MISI, array(COL_NM_MISI => $detKet[array_search($d[COL_KD_MISI], $detNo)]));
                    }
                    else {
                        $this->db->delete(TBL_SAKIP_MPMD_MISI, $arrCond);
                    }
                    $detUpdated[] = $d[COL_KD_MISI];
                }
                /* update / delete */

                /* insert */
                for($i = 0; $i<count($detNo); $i++) {
                    if(!in_array($detNo[$i], $detUpdated)) {
                        $arrDet[] = array(
                            COL_KD_PEMDA => $id,
                            COL_KD_MISI => $detNo[$i],
                            COL_NM_MISI => $detKet[$i]
                        );
                    }
                }
                if(count($arrDet) > 0) $this->db->insert_batch(TBL_SAKIP_MPMD_MISI, $arrDet);
                /* insert */

            } catch (Exception $e) {
                $resp['error'] = $e->getMessage();
                $resp['success'] = 0;
            }
            echo json_encode($resp);
        }else{
            $this->load->view('mpemda/period_form',$data);
        }
    }

    function period_del(){
        $data = $this->input->post('cekbox');
        $deleted = 0;
        foreach ($data as $datum) {
            $this->db->delete(TBL_SAKIP_MPEMDA, array(COL_KD_PEMDA => $datum));
            $deleted++;
        }
        if($deleted){
            ShowJsonSuccess($deleted." data dihapus");
        }else{
            ShowJsonError("Tidak ada dihapus");
        }
    }

    function tujuan() {
        $data['title'] = 'Tujuan';
        $this->db->select('*,'.TBL_SAKIP_MPMD_TUJUAN.'.'.COL_UNIQ.' as ID');
        $this->db->join(TBL_SAKIP_MPEMDA,TBL_SAKIP_MPEMDA.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_MPMD_TUJUAN.".".COL_KD_PEMDA,"inner");
        $this->db->join(TBL_SAKIP_MPMD_MISI,TBL_SAKIP_MPMD_MISI.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_MPMD_TUJUAN.".".COL_KD_PEMDA." AND ".TBL_SAKIP_MPMD_MISI.'.'.COL_KD_MISI." = ".TBL_SAKIP_MPMD_TUJUAN.".".COL_KD_MISI,"inner");
        $this->db->where(TBL_SAKIP_MPEMDA.".".COL_KD_TAHUN_FROM." <=", date("Y"));
        $this->db->where(TBL_SAKIP_MPEMDA.".".COL_KD_TAHUN_TO." >=", date("Y"));
        $this->db->order_by(TBL_SAKIP_MPEMDA.".".COL_KD_TAHUN_FROM, 'desc');
        $this->db->order_by(TBL_SAKIP_MPMD_TUJUAN.".".COL_KD_MISI, 'asc');
        $this->db->order_by(TBL_SAKIP_MPMD_TUJUAN.".".COL_KD_TUJUAN, 'asc');
        $data['res'] = $this->db->get(TBL_SAKIP_MPMD_TUJUAN)->result_array();
        $this->load->view('mpemda/tujuan', $data);
    }

    function tujuan_add() {
        $data['title'] = "Tujuan";
        $data['edit'] = FALSE;

        if(!empty($_POST)){
            $ruser = GetLoggedUser();
            $resp = array();
            $resp['error'] = 0;
            $resp['success'] = 1;
            $resp['redirect'] = site_url('mpemda/tujuan');
            $data = array(
                COL_KD_PEMDA => $this->input->post(COL_KD_PEMDA),
                COL_KD_MISI => $this->input->post(COL_KD_MISI),
                COL_KD_TUJUAN => $this->input->post(COL_KD_TUJUAN),
                COL_NM_TUJUAN => $this->input->post(COL_NM_TUJUAN)
            );
            if(!$this->db->insert(TBL_SAKIP_MPMD_TUJUAN, $data)){
                $resp['error'] = 1;
                $resp['success'] = 0;
            }
            $detNo = $this->input->post("NoDet");
            $detDesc = $this->input->post("KetDet");
            $det = [];
            for($i = 0; $i<count($detNo); $i++) {
                $det[] = array(
                    COL_KD_PEMDA => $this->input->post(COL_KD_PEMDA),
                    COL_KD_MISI => $this->input->post(COL_KD_MISI),
                    COL_KD_TUJUAN => $this->input->post(COL_KD_TUJUAN),
                    COL_KD_INDIKATORTUJUAN => $detNo[$i],
                    COL_NM_INDIKATORTUJUAN => $detDesc[$i]
                );
            }
            if(count($det) > 0) $this->db->insert_batch(TBL_SAKIP_MPMD_IKTUJUAN, $det);
            echo json_encode($resp);
        }else{
            $this->load->view('mpemda/tujuan_form',$data);
        }
    }

    function tujuan_edit($id) {
        $rdata = $data['data'] = $this->db
            ->select(TBL_SAKIP_MPMD_TUJUAN.'.*,'.TBL_SAKIP_MPEMDA.'.'.COL_KD_TAHUN_FROM.','.TBL_SAKIP_MPEMDA.'.'.COL_KD_TAHUN_TO.','.TBL_SAKIP_MPEMDA.'.'.COL_NM_PEJABAT.','.TBL_SAKIP_MPMD_MISI.'.'.COL_NM_MISI)
            ->join(TBL_SAKIP_MPEMDA,TBL_SAKIP_MPEMDA.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_MPMD_TUJUAN.".".COL_KD_PEMDA,"inner")
            ->join(TBL_SAKIP_MPMD_MISI,TBL_SAKIP_MPMD_MISI.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_MPMD_TUJUAN.".".COL_KD_PEMDA." AND ".TBL_SAKIP_MPMD_MISI.'.'.COL_KD_MISI." = ".TBL_SAKIP_MPMD_TUJUAN.".".COL_KD_MISI,"inner")
            ->where(TBL_SAKIP_MPMD_TUJUAN.'.'.COL_UNIQ, $id)->get(TBL_SAKIP_MPMD_TUJUAN)->row_array();
        if(empty($rdata)){
            show_404();
            return;
        }

        $data['title'] = "Tujuan";;
        $data['edit'] = TRUE;
        if(!empty($_POST)){
            $ruser = GetLoggedUser();
            $resp = array();
            $resp['error'] = 0;
            $resp['success'] = 1;
            $resp['redirect'] = site_url('mpemda/tujuan');
            try {
                $data = array(
                    //COL_KD_PEMDA => $this->input->post(COL_KD_PEMDA),
                    //COL_KD_MISI => $this->input->post(COL_KD_MISI),
                    //COL_KD_TUJUAN => $this->input->post(COL_KD_TUJUAN),
                    COL_NM_TUJUAN => $this->input->post(COL_NM_TUJUAN)
                );
                if(!$this->db->where(COL_UNIQ, $id)->update(TBL_SAKIP_MPMD_TUJUAN, $data)){
                    $resp['error'] = 1;
                    $resp['success'] = 0;
                }

                /*$this->db->delete(TBL_SAKIP_MPMD_IKTUJUAN, array(
                    COL_KD_PEMDA => $this->input->post(COL_KD_PEMDA),
                    COL_KD_MISI => $this->input->post(COL_KD_MISI),
                    COL_KD_TUJUAN => $this->input->post(COL_KD_TUJUAN),
                ));*/

                $detNo = $this->input->post("NoDet");
                $detKet = $this->input->post("KetDet");
                $arrDet = [];

                /* update / delete */
                $det = $this->db
                    ->select(COL_KD_PEMDA.",".COL_KD_MISI.",".COL_KD_TUJUAN.",".COL_KD_INDIKATORTUJUAN)
                    ->where(COL_KD_PEMDA, $rdata[COL_KD_PEMDA])
                    ->where(COL_KD_MISI, $rdata[COL_KD_MISI])
                    ->where(COL_KD_TUJUAN, $rdata[COL_KD_TUJUAN])
                    ->get(TBL_SAKIP_MPMD_IKTUJUAN)
                    ->result_array();
                $detUpdated = [];
                foreach($det as $d) {
                    $arrCond = array(
                        COL_KD_PEMDA=>$d[COL_KD_PEMDA],
                        COL_KD_MISI=>$d[COL_KD_MISI],
                        COL_KD_TUJUAN=>$d[COL_KD_TUJUAN],
                        COL_KD_INDIKATORTUJUAN=>$d[COL_KD_INDIKATORTUJUAN]
                    );
                    if(in_array($d[COL_KD_INDIKATORTUJUAN], $detNo)) {
                        $this->db
                            ->where($arrCond)
                            ->update(TBL_SAKIP_MPMD_IKTUJUAN, array(
                                COL_NM_INDIKATORTUJUAN => $detKet[array_search($d[COL_KD_INDIKATORTUJUAN], $detNo)]
                            ));
                    }
                    else {
                        $this->db->delete(TBL_SAKIP_MPMD_IKTUJUAN, $arrCond);
                    }
                    $detUpdated[] = $d[COL_KD_INDIKATORTUJUAN];
                }
                /* update / delete */

                /* insert */
                for($i = 0; $i<count($detNo); $i++) {
                    if(!in_array($detNo[$i], $detUpdated)) {
                        $arrDet[] = array(
                            COL_KD_PEMDA=>$rdata[COL_KD_PEMDA],
                            COL_KD_MISI=>$rdata[COL_KD_MISI],
                            COL_KD_TUJUAN=>$rdata[COL_KD_TUJUAN],
                            COL_KD_INDIKATORTUJUAN => $detNo[$i],
                            COL_NM_INDIKATORTUJUAN => $detKet[$i]
                        );
                    }
                }
                if(count($arrDet) > 0) $this->db->insert_batch(TBL_SAKIP_MPMD_IKTUJUAN, $arrDet);
                /* insert */
            } catch (Exception $e) {
                $resp['error'] = $e->getMessage();
                $resp['success'] = 0;
            }

            echo json_encode($resp);
        }else{
            $this->load->view('mpemda/tujuan_form',$data);
        }
    }

    function tujuan_del(){
        $data = $this->input->post('cekbox');
        $deleted = 0;
        foreach ($data as $datum) {
            $this->db->delete(TBL_SAKIP_MPMD_TUJUAN, array(COL_UNIQ => $datum));
            $deleted++;
        }
        if($deleted){
            ShowJsonSuccess($deleted." data dihapus");
        }else{
            ShowJsonError("Tidak ada dihapus");
        }
    }

    function sasaran() {
        $data['title'] = 'Sasaran';
        $this->db->select('*,'.TBL_SAKIP_MPMD_SASARAN.'.'.COL_UNIQ.' as ID');
        $this->db->join(TBL_SAKIP_MPEMDA,TBL_SAKIP_MPEMDA.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_MPMD_SASARAN.".".COL_KD_PEMDA,"inner");
        $this->db->join(TBL_SAKIP_MPMD_MISI,TBL_SAKIP_MPMD_MISI.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_MPMD_SASARAN.".".COL_KD_PEMDA." AND ".TBL_SAKIP_MPMD_MISI.'.'.COL_KD_MISI." = ".TBL_SAKIP_MPMD_SASARAN.".".COL_KD_MISI,"inner");
        $this->db->join(TBL_SAKIP_MPMD_TUJUAN,TBL_SAKIP_MPMD_TUJUAN.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_MPMD_SASARAN.".".COL_KD_PEMDA." AND ".TBL_SAKIP_MPMD_TUJUAN.'.'.COL_KD_MISI." = ".TBL_SAKIP_MPMD_SASARAN.".".COL_KD_MISI." AND ".TBL_SAKIP_MPMD_TUJUAN.'.'.COL_KD_TUJUAN." = ".TBL_SAKIP_MPMD_SASARAN.".".COL_KD_TUJUAN,"inner");
        $this->db->join(TBL_SAKIP_MPMD_IKTUJUAN,TBL_SAKIP_MPMD_IKTUJUAN.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_MPMD_SASARAN.".".COL_KD_PEMDA." AND ".TBL_SAKIP_MPMD_IKTUJUAN.'.'.COL_KD_MISI." = ".TBL_SAKIP_MPMD_SASARAN.".".COL_KD_MISI." AND ".TBL_SAKIP_MPMD_IKTUJUAN.'.'.COL_KD_TUJUAN." = ".TBL_SAKIP_MPMD_SASARAN.".".COL_KD_TUJUAN." AND ".TBL_SAKIP_MPMD_IKTUJUAN.'.'.COL_KD_INDIKATORTUJUAN." = ".TBL_SAKIP_MPMD_SASARAN.".".COL_KD_INDIKATORTUJUAN,"inner");
        $this->db->where(TBL_SAKIP_MPEMDA.".".COL_KD_TAHUN_FROM." <=", date("Y"));
        $this->db->where(TBL_SAKIP_MPEMDA.".".COL_KD_TAHUN_TO." >=", date("Y"));
        $this->db->order_by(TBL_SAKIP_MPEMDA.".".COL_KD_TAHUN_FROM, 'desc');
        $this->db->order_by(TBL_SAKIP_MPMD_SASARAN.".".COL_KD_MISI, 'asc');
        $this->db->order_by(TBL_SAKIP_MPMD_SASARAN.".".COL_KD_TUJUAN, 'asc');
        $this->db->order_by(TBL_SAKIP_MPMD_SASARAN.".".COL_KD_INDIKATORTUJUAN, 'asc');
        $this->db->order_by(TBL_SAKIP_MPMD_SASARAN.".".COL_KD_SASARAN, 'asc');
        $data['res'] = $this->db->get(TBL_SAKIP_MPMD_SASARAN)->result_array();
        $this->load->view('mpemda/sasaran', $data);
    }

    function sasaran_add() {
        $data['title'] = "Sasaran";
        $data['edit'] = FALSE;

        if(!empty($_POST)){
            $ruser = GetLoggedUser();
            $resp = array();
            $resp['error'] = 0;
            $resp['success'] = 1;
            $resp['redirect'] = site_url('mpemda/sasaran');
            $data = array(
                COL_KD_PEMDA => $this->input->post(COL_KD_PEMDA),
                COL_KD_MISI => $this->input->post(COL_KD_MISI),
                COL_KD_TUJUAN => $this->input->post(COL_KD_TUJUAN),
                COL_KD_INDIKATORTUJUAN => $this->input->post(COL_KD_INDIKATORTUJUAN),
                COL_KD_SASARAN => $this->input->post(COL_KD_SASARAN),
                COL_NM_SASARAN => $this->input->post(COL_NM_SASARAN)
            );
            if(!$this->db->insert(TBL_SAKIP_MPMD_SASARAN, $data)){
                $resp['error'] = 1;
                $resp['success'] = 0;
            }
            $detNo = $this->input->post("NoDet");
            $detDesc = $this->input->post("KetDet");
            $detFormula = $this->input->post("FormulaDet");
            $detSumber = $this->input->post("SumberDet");
            $det = [];
            for($i = 0; $i<count($detNo); $i++) {
                $det[] = array(
                    COL_KD_PEMDA => $this->input->post(COL_KD_PEMDA),
                    COL_KD_MISI => $this->input->post(COL_KD_MISI),
                    COL_KD_TUJUAN => $this->input->post(COL_KD_TUJUAN),
                    COL_KD_INDIKATORTUJUAN => $this->input->post(COL_KD_INDIKATORTUJUAN),
                    COL_KD_SASARAN => $this->input->post(COL_KD_SASARAN),
                    COL_KD_INDIKATORSASARAN => $detNo[$i],
                    COL_NM_INDIKATORSASARAN => $detDesc[$i],
                    COL_NM_FORMULA => $detFormula[$i],
                    COL_NM_SUMBERDATA => $detSumber[$i]
                );
            }
            if(count($det) > 0) $this->db->insert_batch(TBL_SAKIP_MPMD_IKSASARAN, $det);
            echo json_encode($resp);
        }else{
            $this->load->view('mpemda/sasaran_form',$data);
        }
    }

    function sasaran_edit($id) {
        $rdata = $data['data'] = $this->db
            ->select(TBL_SAKIP_MPMD_SASARAN.'.*,'.TBL_SAKIP_MPEMDA.'.'.COL_KD_TAHUN_FROM.','.TBL_SAKIP_MPEMDA.'.'.COL_KD_TAHUN_TO.','.TBL_SAKIP_MPEMDA.'.'.COL_NM_PEJABAT.','.TBL_SAKIP_MPMD_MISI.'.'.COL_NM_MISI.','.TBL_SAKIP_MPMD_TUJUAN.'.'.COL_NM_TUJUAN.','.TBL_SAKIP_MPMD_IKTUJUAN.'.'.COL_NM_INDIKATORTUJUAN)
            ->join(TBL_SAKIP_MPEMDA,TBL_SAKIP_MPEMDA.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_MPMD_SASARAN.".".COL_KD_PEMDA,"inner")
            ->join(TBL_SAKIP_MPMD_MISI,TBL_SAKIP_MPMD_MISI.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_MPMD_SASARAN.".".COL_KD_PEMDA." AND ".TBL_SAKIP_MPMD_MISI.'.'.COL_KD_MISI." = ".TBL_SAKIP_MPMD_SASARAN.".".COL_KD_MISI,"inner")
            ->join(TBL_SAKIP_MPMD_TUJUAN,TBL_SAKIP_MPMD_TUJUAN.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_MPMD_SASARAN.".".COL_KD_PEMDA." AND ".TBL_SAKIP_MPMD_TUJUAN.'.'.COL_KD_MISI." = ".TBL_SAKIP_MPMD_SASARAN.".".COL_KD_MISI." AND ".TBL_SAKIP_MPMD_TUJUAN.'.'.COL_KD_TUJUAN." = ".TBL_SAKIP_MPMD_SASARAN.".".COL_KD_TUJUAN,"inner")
            ->join(TBL_SAKIP_MPMD_IKTUJUAN,TBL_SAKIP_MPMD_IKTUJUAN.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_MPMD_SASARAN.".".COL_KD_PEMDA." AND ".TBL_SAKIP_MPMD_IKTUJUAN.'.'.COL_KD_MISI." = ".TBL_SAKIP_MPMD_SASARAN.".".COL_KD_MISI." AND ".TBL_SAKIP_MPMD_IKTUJUAN.'.'.COL_KD_TUJUAN." = ".TBL_SAKIP_MPMD_SASARAN.".".COL_KD_TUJUAN." AND ".TBL_SAKIP_MPMD_IKTUJUAN.'.'.COL_KD_INDIKATORTUJUAN." = ".TBL_SAKIP_MPMD_SASARAN.".".COL_KD_INDIKATORTUJUAN,"inner")
            ->where(TBL_SAKIP_MPMD_SASARAN.'.'.COL_UNIQ, $id)->get(TBL_SAKIP_MPMD_SASARAN)->row_array();
        if(empty($rdata)){
            show_404();
            return;
        }

        $data['title'] = "Sasaran";
        $data['edit'] = TRUE;
        if(!empty($_POST)){
            $ruser = GetLoggedUser();
            $resp = array();
            $resp['error'] = 0;
            $resp['success'] = 1;
            $resp['redirect'] = site_url('mpemda/sasaran');
            try {
                $data = array(
                    COL_KD_SASARAN => $this->input->post(COL_KD_SASARAN),
                    COL_NM_SASARAN => $this->input->post(COL_NM_SASARAN)
                );
                if(!$this->db->where(COL_UNIQ, $id)->update(TBL_SAKIP_MPMD_SASARAN, $data)){
                    $resp['error'] = 1;
                    $resp['success'] = 0;
                }

                /*$this->db->delete(TBL_SAKIP_MPMD_IKSASARAN, array(
                    COL_KD_PEMDA => $this->input->post(COL_KD_PEMDA),
                    COL_KD_MISI => $this->input->post(COL_KD_MISI),
                    COL_KD_TUJUAN => $this->input->post(COL_KD_TUJUAN),
                    COL_KD_INDIKATORTUJUAN => $this->input->post(COL_KD_INDIKATORTUJUAN),
                    COL_KD_SASARAN => $this->input->post(COL_KD_SASARAN)
                ));

                $detNo = $this->input->post("NoDet");
                $detDesc = $this->input->post("KetDet");
                $det = [];
                for($i = 0; $i<count($detNo); $i++) {
                    $det[] = array(
                        COL_KD_PEMDA => $this->input->post(COL_KD_PEMDA),
                        COL_KD_MISI => $this->input->post(COL_KD_MISI),
                        COL_KD_TUJUAN => $this->input->post(COL_KD_TUJUAN),
                        COL_KD_INDIKATORTUJUAN => $this->input->post(COL_KD_INDIKATORTUJUAN),
                        COL_KD_SASARAN => $this->input->post(COL_KD_SASARAN),
                        COL_KD_INDIKATORSASARAN => $detNo[$i],
                        COL_NM_INDIKATORSASARAN => $detDesc[$i]
                    );
                }
                if(count($det) > 0) $this->db->insert_batch(TBL_SAKIP_MPMD_IKSASARAN, $det);*/
                $detNo = $this->input->post("NoDet");
                $detKet = $this->input->post("KetDet");
                $detFormula = $this->input->post("FormulaDet");
                $detSumber = $this->input->post("SumberDet");
                $arrDet = [];

                /* update / delete */
                $det = $this->db
                    ->select(COL_KD_PEMDA.",".COL_KD_MISI.",".COL_KD_TUJUAN.",".COL_KD_INDIKATORTUJUAN.",".COL_KD_SASARAN.",".COL_KD_INDIKATORSASARAN)
                    ->where(COL_KD_PEMDA, $rdata[COL_KD_PEMDA])
                    ->where(COL_KD_MISI, $rdata[COL_KD_MISI])
                    ->where(COL_KD_TUJUAN, $rdata[COL_KD_TUJUAN])
                    ->where(COL_KD_INDIKATORTUJUAN, $rdata[COL_KD_INDIKATORTUJUAN])
                    ->where(COL_KD_SASARAN, $rdata[COL_KD_SASARAN])
                    ->get(TBL_SAKIP_MPMD_IKSASARAN)
                    ->result_array();
                $detUpdated = [];
                foreach($det as $d) {
                    $arrCond = array(
                        COL_KD_PEMDA=>$d[COL_KD_PEMDA],
                        COL_KD_MISI=>$d[COL_KD_MISI],
                        COL_KD_TUJUAN=>$d[COL_KD_TUJUAN],
                        COL_KD_INDIKATORTUJUAN=>$d[COL_KD_INDIKATORTUJUAN],
                        COL_KD_SASARAN=>$d[COL_KD_SASARAN],
                        COL_KD_INDIKATORSASARAN=>$d[COL_KD_INDIKATORSASARAN]
                    );
                    if(in_array($d[COL_KD_INDIKATORSASARAN], $detNo)) {
                        $this->db
                            ->where($arrCond)
                            ->update(TBL_SAKIP_MPMD_IKSASARAN, array(
                                COL_KD_PEMDA => $this->input->post(COL_KD_PEMDA),
                                COL_KD_MISI => $this->input->post(COL_KD_MISI),
                                COL_KD_TUJUAN => $this->input->post(COL_KD_TUJUAN),
                                COL_KD_INDIKATORTUJUAN => $this->input->post(COL_KD_INDIKATORTUJUAN),
                                COL_KD_SASARAN => $this->input->post(COL_KD_SASARAN),

                                COL_KD_INDIKATORSASARAN => $d[COL_KD_INDIKATORSASARAN],
                                COL_NM_INDIKATORSASARAN => $detKet[array_search($d[COL_KD_INDIKATORSASARAN], $detNo)],
                                COL_NM_FORMULA => $detFormula[array_search($d[COL_KD_INDIKATORSASARAN], $detNo)],
                                COL_NM_SUMBERDATA => $detSumber[array_search($d[COL_KD_INDIKATORSASARAN], $detNo)],
                            ));
                    }
                    else {
                        $this->db->delete(TBL_SAKIP_MPMD_IKSASARAN, $arrCond);
                    }
                    $detUpdated[] = $d[COL_KD_INDIKATORSASARAN];
                }
                /* update / delete */

                /* insert */
                for($i = 0; $i<count($detNo); $i++) {
                    if(!in_array($detNo[$i], $detUpdated)) {
                        $arrDet[] = array(
                            COL_KD_PEMDA => $this->input->post(COL_KD_PEMDA),
                            COL_KD_MISI => $this->input->post(COL_KD_MISI),
                            COL_KD_TUJUAN => $this->input->post(COL_KD_TUJUAN),
                            COL_KD_INDIKATORTUJUAN => $this->input->post(COL_KD_INDIKATORTUJUAN),
                            COL_KD_SASARAN => $this->input->post(COL_KD_SASARAN),

                            COL_KD_INDIKATORSASARAN => $detNo[$i],
                            COL_NM_INDIKATORSASARAN => $detKet[$i],
                            COL_NM_FORMULA => $detFormula[$i],
                            COL_NM_SUMBERDATA => $detSumber[$i],
                        );
                    }
                }
                if(count($arrDet) > 0) $this->db->insert_batch(TBL_SAKIP_MPMD_IKSASARAN, $arrDet);
                /* insert */
            } catch (Exception $e) {
                $resp['error'] = $e->getMessage();
                $resp['success'] = 0;
            }

            echo json_encode($resp);
        }else{
            $this->load->view('mpemda/sasaran_form',$data);
        }
    }

    function sasaran_del(){
        $data = $this->input->post('cekbox');
        $deleted = 0;
        foreach ($data as $datum) {
            $this->db->delete(TBL_SAKIP_MPMD_SASARAN, array(COL_UNIQ => $datum));
            $deleted++;
        }
        if($deleted){
            ShowJsonSuccess($deleted." data dihapus");
        }else{
            ShowJsonError("Tidak ada dihapus");
        }
    }

    function capaian() {
      $period = $this->input->get('Period');
      $tahun = $this->input->get(COL_KD_TAHUN);
      $cetak = $this->input->get('Cetak');

      if(empty($tahun)) {
        $tahun = date('Y');
      }
      if(empty($period)) {
        $rperiod = $this->db
        ->where(COL_KD_TAHUN_FROM.'<=', $tahun)
        ->where(COL_KD_TAHUN_TO.'>=', $tahun)
        ->get(TBL_SAKIP_MPEMDA)
        ->row_array();

        if(!empty($rperiod)) {
          $period = $rperiod[COL_KD_PEMDA];
        }
      }

      if(!empty($period) && !empty($tahun)) {
        $data['res'] = $this->db
        ->select('
          sakip_mpmd_iksasaran.*,
          sakip_mpmd_sasaran.Nm_Sasaran,
          sakip_mpmd_sasaran.Uniq as ID_Sasaran,
          sakip_mpmd_iksasaran_capaian.Target_TW1,
          sakip_mpmd_iksasaran_capaian.Target_TW2,
          sakip_mpmd_iksasaran_capaian.Target_TW3,
          sakip_mpmd_iksasaran_capaian.Target_TW4,
          sakip_mpmd_iksasaran_capaian.Target,
          sakip_mpmd_iksasaran_capaian.Realisasi_TW1,
          sakip_mpmd_iksasaran_capaian.Realisasi_TW2,
          sakip_mpmd_iksasaran_capaian.Realisasi_TW3,
          sakip_mpmd_iksasaran_capaian.Realisasi_TW4,
          sakip_mpmd_iksasaran_capaian.Realisasi,
          (
            select
            count(*)
            from sakip_mpmd_iksasaran iks_
            where
              iks_.Kd_Pemda = sakip_mpmd_iksasaran.Kd_Pemda
              and iks_.Kd_Misi = sakip_mpmd_iksasaran.Kd_Misi
              and iks_.Kd_Tujuan = sakip_mpmd_iksasaran.Kd_Tujuan
              and iks_.Kd_IndikatorTujuan = sakip_mpmd_iksasaran.Kd_IndikatorTujuan
              and iks_.Kd_Sasaran = sakip_mpmd_iksasaran.Kd_Sasaran
          ) as rowspan
        ')
        ->join(TBL_SAKIP_MPMD_SASARAN,
        TBL_SAKIP_MPMD_SASARAN.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_MPMD_IKSASARAN.".".COL_KD_PEMDA." AND ".
        TBL_SAKIP_MPMD_SASARAN.'.'.COL_KD_MISI." = ".TBL_SAKIP_MPMD_IKSASARAN.".".COL_KD_MISI." AND ".
        TBL_SAKIP_MPMD_SASARAN.'.'.COL_KD_TUJUAN." = ".TBL_SAKIP_MPMD_IKSASARAN.".".COL_KD_TUJUAN." AND ".
        TBL_SAKIP_MPMD_SASARAN.'.'.COL_KD_INDIKATORTUJUAN." = ".TBL_SAKIP_MPMD_IKSASARAN.".".COL_KD_INDIKATORTUJUAN." AND ".
        TBL_SAKIP_MPMD_SASARAN.'.'.COL_KD_SASARAN." = ".TBL_SAKIP_MPMD_IKSASARAN.".".COL_KD_SASARAN
        ,"inner")
        ->join(TBL_SAKIP_MPMD_IKSASARAN_CAPAIAN,
        TBL_SAKIP_MPMD_IKSASARAN_CAPAIAN.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_MPMD_IKSASARAN.".".COL_KD_PEMDA." AND ".
        TBL_SAKIP_MPMD_IKSASARAN_CAPAIAN.'.'.COL_KD_MISI." = ".TBL_SAKIP_MPMD_IKSASARAN.".".COL_KD_MISI." AND ".
        TBL_SAKIP_MPMD_IKSASARAN_CAPAIAN.'.'.COL_KD_TUJUAN." = ".TBL_SAKIP_MPMD_IKSASARAN.".".COL_KD_TUJUAN." AND ".
        TBL_SAKIP_MPMD_IKSASARAN_CAPAIAN.'.'.COL_KD_INDIKATORTUJUAN." = ".TBL_SAKIP_MPMD_IKSASARAN.".".COL_KD_INDIKATORTUJUAN." AND ".
        TBL_SAKIP_MPMD_IKSASARAN_CAPAIAN.'.'.COL_KD_SASARAN." = ".TBL_SAKIP_MPMD_IKSASARAN.".".COL_KD_SASARAN." AND ".
        TBL_SAKIP_MPMD_IKSASARAN_CAPAIAN.'.'.COL_KD_INDIKATORSASARAN." = ".TBL_SAKIP_MPMD_IKSASARAN.".".COL_KD_INDIKATORSASARAN." AND ".
        TBL_SAKIP_MPMD_IKSASARAN_CAPAIAN.'.'.COL_KD_TAHUN." = $tahun"
        ,"left")
        ->where(TBL_SAKIP_MPMD_IKSASARAN.".".COL_KD_PEMDA, $period)
        ->order_by(TBL_SAKIP_MPMD_IKSASARAN.'.'.COL_KD_MISI)
        ->order_by(TBL_SAKIP_MPMD_IKSASARAN.'.'.COL_KD_TUJUAN)
        ->order_by(TBL_SAKIP_MPMD_IKSASARAN.'.'.COL_KD_INDIKATORTUJUAN)
        ->order_by(TBL_SAKIP_MPMD_IKSASARAN.'.'.COL_KD_SASARAN)
        ->order_by(TBL_SAKIP_MPMD_IKSASARAN.'.'.COL_KD_INDIKATORSASARAN)
        ->get(TBL_SAKIP_MPMD_IKSASARAN)
        ->result_array();
      }

      $data['title'] = 'Capaian Kinerja Pemerintah Daerah';
      $data['period'] = $period;
      $data['tahun'] = $tahun;
      if($cetak==1) {
        $this->load->library('Mypdf');
        $mpdf = new Mypdf();

        $html = $this->load->view('mpemda/capaian_', $data, TRUE);
        //echo $html;
        //return;
        $mpdf->pdf->AddPage('L');
        $mpdf->pdf->setTitle('Capaian Kinerja Pemerintah Daerah '.$tahun);
        $mpdf->pdf->setFooter('Dicetak melalui Aplikasi '.$this->setting_web_name.' pada '.date('d-m-Y H:i'));
        $mpdf->pdf->WriteHTML($html);
        $mpdf->pdf->Output('Capaian Kinerja Pemerintah Daerah - '.date('YmdHi').'.pdf', 'I');
      } else {
        $this->load->view('mpemda/capaian', $data);
      }
    }

    function target($period,$tahun,$tw,$kdmisi,$kdtujuan,$kdiktujuan,$kdsasaran,$kdiksasaran) {
      $val = $this->input->post("VALUE");
      $cond = array(
        COL_KD_PEMDA=>$period,
        COL_KD_TAHUN=>$tahun,
        COL_KD_MISI=>$kdmisi,
        COL_KD_TUJUAN=>$kdtujuan,
        COL_KD_INDIKATORTUJUAN=>$kdiktujuan,
        COL_KD_SASARAN=>$kdsasaran,
        COL_KD_INDIKATORSASARAN=>$kdiksasaran
      );

      $rdata = $this->db
      ->where($cond)
      ->get(TBL_SAKIP_MPMD_IKSASARAN_CAPAIAN)
      ->row_array();

      if(!empty($rdata)) {
        $res = $this->db
        ->where($cond)
        ->update(TBL_SAKIP_MPMD_IKSASARAN_CAPAIAN, array(($tw>0?'Target_TW'.$tw:'Target')=>$val));
        if(!$res) {
          $err = $this->db->error();
          ShowJsonError($err['message']);
          return;
        }
      } else {
        $res = $this->db
        ->where($cond)
        ->insert(TBL_SAKIP_MPMD_IKSASARAN_CAPAIAN, array_merge($cond, array(($tw>0?'Target_TW'.$tw:'Target')=>$val)));
        if(!$res) {
          $err = $this->db->error();
          ShowJsonError($err['message']);
          return;
        }
      }
      ShowJsonSuccess('Berhasil');
      return;
    }

    function realisasi($period,$tahun,$tw,$kdmisi,$kdtujuan,$kdiktujuan,$kdsasaran,$kdiksasaran) {
      $val = $this->input->post("VALUE");
      $cond = array(
        COL_KD_PEMDA=>$period,
        COL_KD_TAHUN=>$tahun,
        COL_KD_MISI=>$kdmisi,
        COL_KD_TUJUAN=>$kdtujuan,
        COL_KD_INDIKATORTUJUAN=>$kdiktujuan,
        COL_KD_SASARAN=>$kdsasaran,
        COL_KD_INDIKATORSASARAN=>$kdiksasaran
      );

      $rdata = $this->db
      ->where($cond)
      ->get(TBL_SAKIP_MPMD_IKSASARAN_CAPAIAN)
      ->row_array();

      if(!empty($rdata)) {
        $res = $this->db
        ->where($cond)
        ->update(TBL_SAKIP_MPMD_IKSASARAN_CAPAIAN, array(($tw>0?'Realisasi_TW'.$tw:'Realisasi')=>$val));
        if(!$res) {
          $err = $this->db->error();
          ShowJsonError($err['message']);
          return;
        }
      } else {
        $res = $this->db
        ->where($cond)
        ->insert(TBL_SAKIP_MPMD_IKSASARAN_CAPAIAN, array_merge($cond, array(($tw>0?'Realisasi_TW'.$tw:'Realisasi')=>$val)));
        if(!$res) {
          $err = $this->db->error();
          ShowJsonError($err['message']);
          return;
        }
      }
      ShowJsonSuccess('Berhasil');
      return;
    }
}
