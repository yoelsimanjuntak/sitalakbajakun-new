<?php $this->load->view('header')?>
<?php
$ruser = GetLoggedUser();
?>
<section class="content-header">
  <h1><?= $title ?></h1>
  <ol class="breadcrumb">
    <li>
      <a href="<?=site_url()?>"><i class="fa fa-dashboard"></i> Home</a>
    </li>
    <li class="active"><?=$title?></li>
  </ol>
</section>
<section class="content">
  <div class="box box-default">
    <?=form_open(current_url(),array('role'=>'form', 'method'=>'get','id'=>'form-filter','class'=>'form-horizontal'))?>
    <div class="box-body">
      <div class="row">
        <div class="col-sm-12">
          <div class="form-group">
            <label class="control-label col-sm-2">Periode / Tahun</label>
            <div class="col-sm-6">
              <select name="Period" class="form-control no-clear">
                <?php
                $rpemda = $this->db->order_by(COL_KD_TAHUN_FROM,'desc')->get(TBL_SAKIP_MPEMDA)->result_array();
                $selected = $this->input->get('Periode');
                foreach ($rpemda as $p) {
                  ?>
                  <option value="<?=$p[COL_KD_PEMDA]?>" <?=$p[COL_KD_PEMDA]==$selected?'selected':''?>><?=$p[COL_KD_TAHUN_FROM].' s.d '.$p[COL_KD_TAHUN_TO].' - '.$p[COL_NM_PEJABAT]?></option>
                  <?php
                }
                ?>
              </select>
            </div>
            <div class="col-sm-2">
              <input name="<?=COL_KD_TAHUN?>" class="form-control text-right" value="<?=$tahun?>" />
            </div>
          </div>
          <div class="form-group">
              <label class="control-label col-sm-2">OPD</label>
              <div class="col-sm-8">
                  <div class="input-group">
                      <?php
                      $nmSub = "";
                      $strOPD = explode('.', $ruser[COL_COMPANYID]);
                      $kdUrusan = $this->input->get(COL_KD_URUSAN);
                      $kdBidang = $this->input->get(COL_KD_BIDANG);
                      $kdUnit = $this->input->get(COL_KD_UNIT);
                      $kdSub = $this->input->get(COL_KD_SUB);
                      if($ruser[COL_ROLEID] != ROLEADMIN) {
                        $kdUrusan = $strOPD[0];
                        $kdBidang = $strOPD[1];
                        $kdUnit = $strOPD[2];
                        $kdSub = $strOPD[3];
                      }
                      $eplandb = $this->load->database("eplan", true);
                      $eplandb->where(COL_KD_URUSAN, $kdUrusan);
                      $eplandb->where(COL_KD_BIDANG, $kdBidang);
                      $eplandb->where(COL_KD_UNIT, $kdUnit);
                      $eplandb->where(COL_KD_SUB, $kdSub);
                      $subunit = $eplandb->get("ref_sub_unit")->row_array();
                      if($subunit) {
                          $nmSub = $subunit["Nm_Sub_Unit"];
                      }
                      ?>
                      <input type="text" class="form-control" name="text-opd" value="<?=$nmSub?>" disabled />
                      <input type="hidden" name="<?=COL_KD_URUSAN?>" value="<?=$kdUrusan?>" required />
                      <input type="hidden" name="<?=COL_KD_BIDANG?>" value="<?=$kdBidang?>" required />
                      <input type="hidden" name="<?=COL_KD_UNIT?>" value="<?=$kdUnit?>" required />
                      <input type="hidden" name="<?=COL_KD_SUB?>" value="<?=$kdSub?>" required />
                      <div class="input-group-btn">
                          <button type="button" class="btn btn-default btn-flat btn-browse-opd" data-toggle="modal" data-target="#browseOPD" data-toggle="tooltip" data-placement="top" title="Pilih OPD" <?=($ruser[COL_ROLEID] != ROLEADMIN ? "disabled" : "")?>><i class="fa fa-ellipsis-h"></i></button>
                      </div>
                  </div>
              </div>
          </div>
        </div>
      </div>
    </div>
    <div class="box-footer">
      <div class="col-sm-12 text-center">
        <button type="submit" class="btn btn-success"><i class="fas fa-check"></i>&nbsp;&nbsp;TAMPILKAN</button>
      </div>
    </div>
    <?=form_close()?>
  </div>
  <?php
  foreach($iku as $t) {
    $rsasaran = $this->db
    ->where(array(
      TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_URUSAN=>$t[COL_KD_URUSAN],
      TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_BIDANG=>$t[COL_KD_BIDANG],
      TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_UNIT=>$t[COL_KD_UNIT],
      TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_SUB=>$t[COL_KD_SUB],

      TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_PEMDA=>$t[COL_KD_PEMDA],
      TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_MISI=>$t[COL_KD_MISI],
      TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_TUJUAN=>$t[COL_KD_TUJUAN],
      TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_INDIKATORTUJUAN=>$t[COL_KD_INDIKATORTUJUAN],
      TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_SASARAN=>$t[COL_KD_SASARAN],
      TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_INDIKATORSASARAN=>$t[COL_KD_INDIKATORSASARAN],
      TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_TUJUANOPD=>$t[COL_KD_TUJUANOPD],
      TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_INDIKATORTUJUANOPD=>$t[COL_KD_INDIKATORTUJUANOPD]
    ))
    ->order_by(TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_SASARANOPD, 'asc')
    ->get(TBL_SAKIP_MOPD_SASARAN)
    ->result_array();
    ?>
    <div class="box box-secondary collapsed-box">
      <div class="box-header with-border">
        <h5 class="no-margin">MISI <?=$t[COL_KD_MISI]?>: <strong><?=$t[COL_NM_MISI]?></strong></h5>
        <div class="box-tools pull-right">
          <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
          </button>
        </div>
      </div>
      <div class="box-body no-padding">
        <table class="table table-condensed">
          <tbody>
            <tr><td colspan="2" class="text-bold">IKU KOTA</td><td></td></tr>
            <tr><td class="text-right" style="width: 12vw">Tujuan</td><td style="width: 2vw">:</td><td><?=$t[COL_NM_TUJUAN]?></td></tr>
            <tr><td class="text-right" style="width: 12vw">Indikator Tujuan</td><td style="width: 2vw">:</td><td><?=$t[COL_NM_INDIKATORTUJUAN]?></td></tr>
            <tr><td class="text-right" style="width: 12vw">Sasaran</td><td style="width: 2vw">:</td><td><?=$t[COL_NM_SASARAN]?></td></tr>
            <tr><td class="text-right" style="width: 12vw">Indikator Sasaran</td><td style="width: 2vw">:</td><td><?=$t[COL_NM_INDIKATORSASARAN]?></td></tr>
            <tr><td colspan="2" class="text-bold">IKU OPD</td><td></td></tr>
            <tr><td class="text-right" style="width: 12vw">Tujuan</td><td style="width: 2vw">:</td><td><?=$t[COL_NM_TUJUANOPD]?></td></tr>
            <tr><td class="text-right" style="width: 12vw">Indikator Tujuan</td><td style="width: 2vw">:</td><td><?=$t[COL_NM_INDIKATORTUJUANOPD]?></td></tr>
            <?php
            foreach($rsasaran as $sas) {
              $riksasaran = $this->db
              ->where(array(
                TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_URUSAN=>$sas[COL_KD_URUSAN],
                TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_BIDANG=>$sas[COL_KD_BIDANG],
                TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_UNIT=>$sas[COL_KD_UNIT],
                TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_SUB=>$sas[COL_KD_SUB],

                TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_PEMDA=>$sas[COL_KD_PEMDA],
                TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_MISI=>$sas[COL_KD_MISI],
                TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_TUJUAN=>$sas[COL_KD_TUJUAN],
                TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_INDIKATORTUJUAN=>$sas[COL_KD_INDIKATORTUJUAN],
                TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_SASARAN=>$sas[COL_KD_SASARAN],
                TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_INDIKATORSASARAN=>$sas[COL_KD_INDIKATORSASARAN],
                TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_TUJUANOPD=>$sas[COL_KD_TUJUANOPD],
                TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_INDIKATORTUJUANOPD=>$sas[COL_KD_INDIKATORTUJUANOPD],
                TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_SASARANOPD=>$sas[COL_KD_SASARANOPD]
              ))
              ->order_by(TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_INDIKATORSASARANOPD, 'asc')
              ->get(TBL_SAKIP_MOPD_IKSASARAN)
              ->result_array();
              ?>
              <tr><td class="text-right" style="width: 12vw">Sasaran <strong><?=$sas[COL_KD_SASARANOPD]?></strong></td><td style="width: 2vw">:</td><td><?=$sas[COL_NM_SASARANOPD]?></td></tr>
              <?php
              foreach ($riksasaran as $iks) {
                $keyPrg = $iks[COL_KD_PEMDA].'.'.
                          $tahun.'.'.
                          $iks[COL_KD_URUSAN].'.'.
                          $iks[COL_KD_BIDANG].'.'.
                          $iks[COL_KD_UNIT].'.'.
                          $iks[COL_KD_SUB].'.'.
                          $iks[COL_KD_MISI].'.'.
                          $iks[COL_KD_TUJUAN].'.'.
                          $iks[COL_KD_INDIKATORTUJUAN].'.'.
                          $iks[COL_KD_SASARAN].'.'.
                          $iks[COL_KD_INDIKATORSASARAN].'.'.
                          $iks[COL_KD_TUJUANOPD].'.'.
                          $iks[COL_KD_INDIKATORTUJUANOPD].'.'.
                          $iks[COL_KD_SASARANOPD].'.'.
                          $iks[COL_KD_INDIKATORSASARANOPD];
                ?>
                <tr><td class="text-right" style="width: 12vw">Indikator Sasaran <strong><?=$iks[COL_KD_SASARANOPD].'.'.$iks[COL_KD_INDIKATORSASARANOPD]?></strong></td><td style="width: 2vw">:</td><td><?=$iks[COL_NM_INDIKATORSASARANOPD]?></td></tr>
                <tr>
                  <td class="text-right" style="width: 12vw">Renja</td>
                  <td style="width: 2vw">:</td>
                  <td>
                    <p>
                      <button type="button" class="btn btn-sm btn-default btn-refresh-program text-bold" style="padding: 2px 5px; font-size: 10px"><i class="fas fa-refresh"></i>&nbsp;REFRESH</button>
                      <a href="<?=site_url('sakip/renja/tambah-dpa-prog/'.$keyPrg)?>" class="btn btn-sm btn-primary btn-add-program text-bold" style="padding: 2px 5px; font-size: 10px"><i class="fas fa-plus"></i>&nbsp;PROGRAM</a>
                    </p>
                  </td>
                </tr>
                <tr>
                  <td colspan="3">
                    <div class="div-renja" data-url="<?=site_url('sakip/renja/index-dpa-load/'.$keyPrg)?>">

                    </div>
                  </td>
                </tr>
                <?php
              }
            }
            ?>
          </tbody>
        </table>
      </div>
    </div>
    <?php
  }
  ?>
</section>
<div class="modal fade" id="browseOPD" tabindex="-1" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">OPD</h4>
      </div>
      <div class="modal-body">...</div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default btn-flat pull-right" data-dismiss="modal">CLOSE</button>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="modalProgram" role="dialog">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Editor Program / Kegiatan</h4>
      </div>
      <div class="modal-body">
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="modalSasaranProgram" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Sasaran Program</h4>
      </div>
      <div class="modal-body">
      </div>
      <div class="modal-footer">
        <div class="row text-right">
          <div class="col-sm-12">
            <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fas fa-times"></i>&nbsp;CLOSE</button>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<?php $this->load->view('loadjs')?>
<script>
$(document).ready(function() {
  $(".div-renja").bind('reload', function(e, expid) {
    var url = $(this).data('url');
    var dis = $(this);
    dis.load(url, {expid: expid}, function(a,b) {
      $('.tree-renja', dis).treetable({ expandable: true, clickableNodeNames: true, force: true });

      var expanded = $('.tree-renja', dis).data('expand-id');
      if (expanded) {
        var expandedEl = $('tr[data-tt-id="'+expanded+'"]', dis);
        if (expandedEl.length > 0) {
          $('.tree-renja', dis).closest('.box').removeClass('collapsed-box').find('button[data-widget=collapse]>i').removeClass('fa-plus').addClass('fa-minus');
          $('.tree-renja', dis).treetable("expandNode", expanded);
        }
      }

      $('.btn-edit-program, .btn-tambah-dpa-program, .btn-tambah-kegiatan, .btn-edit-kegiatan, .btn-tambah-dpa-kegiatan', dis).click(function() {
        var thisbtn = $(this);
        var href = $(this).attr('href');
        var parentid = (thisbtn.closest('tr').data('tt-parent-id') || thisbtn.closest('tr').data('tt-id'));
        var modal = $('#modalProgram');

        modal.modal('show');
        $('.modal-body', modal).load(href, function() {
          $("select", modal).not('.no-clear').not('.no-select2').select2({ width: 'resolve', allowClear: true });
          $(".money", modal).number(true, 2, '.', ',');
          $(".uang", modal).number(true, 0, '.', ',');
          //$("#form-program").attr('action', href);
          $("#form-program").attr('tt-parent-id', parentid);
          $("#form-program").validate({
            submitHandler: function(form) {
              $(form).find('button[type=submit]').attr('disabled',true);
              $(form).ajaxSubmit({
                dataType: 'json',
                type : 'post',
                success: function(data){
                  if(data.error == 0) {
                    var expid_ = $(form).attr('tt-parent-id');
                    dis.trigger('reload', expid_);
                  } else {
                    alert(data.error);
                  }
                },
                error: function(a,b,c){
                  alert('Response Error');
                },
                complete: function() {
                  $(form).find('button[type=submit]').attr('disabled',false);
                  modal.modal('hide');
                }
              });
              return false;
            }
          });
        });
        return false;
      });

      $('.btn-delete-program, .btn-delete-kegiatan', dis).unbind('click').click(function() {
          var a = $(this);
          var parentid = $(this).closest('tr').data('tt-parent-id');
          var confirmDialog = $("#confirmDialog");

          $(".modal-body", confirmDialog).html("Apa anda yakin?<br />");
          confirmDialog.modal("show");

          $(".btn-ok", confirmDialog).unbind('click').click(function() {
            var btn = $(this);
            btn.attr("disabled", true);

            $.post(a.attr('href'), function(data) {
              if(data.error==0){
                dis.trigger('reload', parentid);
              } else {
                alert(data.error);
              }
            }, 'json').done(function() {
              btn.attr("disabled", false);
              confirmDialog.modal("hide");
            }).fail(function() {
              btn.attr("disabled", false);
              alert('Response Error');
            });
          });
          return false;
      });

      $('.btn-tambah-sasaran-program, .btn-tambah-sasaran-kegiatan', dis).click(function() {
        var href = $(this).attr('href');
        var parentid = ($(this).closest('tr').data('tt-parent-id') || $(this).closest('tr').data('tt-id'));
        var modal = $('#modalProgram');

        modal.modal('show');
        $('.modal-body', modal).load(href, function() {
          $("select").not('.no-clear').not('.no-select2').select2({ width: 'resolve', allowClear: true });
          //$("#form-program").attr('action', href);
          $("#form-program").attr('tt-parent-id', parentid);
          $("#form-program").validate({
            submitHandler : function(form) {
              $(form).find('button[type=submit]').attr('disabled',true);
              var det = [];
              var rowDet = $("#tbl-det>tbody", form).find("tr:not(.tr-blueprint-det)");
              for (var i = 0; i < rowDet.length; i++) {
                  var noDet = $("[name=text-det-no]", $(rowDet[i])).val();
                  var ketDet = $("[name=text-det-desc]", $(rowDet[i])).val();
                  var satuanDet = $("[name=select-det-satuan]", $(rowDet[i])).val();
                  var targetDet = $("[name=text-det-target]", $(rowDet[i])).val();
                  var awalDet = $("[name=text-det-awal]", $(rowDet[i])).val();
                  var akhirDet = $("[name=text-det-akhir]", $(rowDet[i])).val();
                  var formtDet = $("[name=text-det-formula]", $(rowDet[i])).val();
                  var sourceDet = $("[name=text-det-sumber]", $(rowDet[i])).val();
                  var picDet = $("[name=text-det-pic]", $(rowDet[i])).val();
                  det.push({
                      No: noDet,
                      Ket: ketDet,
                      Formula: formtDet,
                      Sumber: sourceDet,
                      PIC: picDet,
                      Kd_Satuan: satuanDet,
                      Target: targetDet,
                      Awal: (awalDet?awalDet:0),
                      Akhir: (akhirDet?akhirDet:0)
                  });
              }
              $(".appended", form).remove();
              for (var i = 0; i < det.length; i++) {
                var newEl = "";
                newEl += "<input type='hidden' class='appended' name=NoDet[" + i + "] value='" + det[i].No + "' />";
                newEl += "<input type='hidden' class='appended' name=KetDet[" + i + "] value='" + det[i].Ket + "' />";
                newEl += "<input type='hidden' class='appended' name=SatuanDet[" + i + "] value='" + det[i].Kd_Satuan + "' />";
                newEl += "<input type='hidden' class='appended' name=TargetDet[" + i + "] value='" + det[i].Target + "' />";
                newEl += "<input type='hidden' class='appended' name=AwalDet[" + i + "] value='" + det[i].Awal + "' />";
                newEl += "<input type='hidden' class='appended' name=AkhirDet[" + i + "] value='" + det[i].Akhir + "' />";
                newEl += "<input type='hidden' class='appended' name=FormulaDet[" + i + "] value='" + det[i].Formula + "' />";
                newEl += "<input type='hidden' class='appended' name=SumberDet[" + i + "] value='" + det[i].Sumber + "' />";
                $(form).append(newEl);
              }

              $(form).ajaxSubmit({
                dataType: 'json',
                type : 'post',
                success : function(data){
                  if(data.error != 0){
                    alert(data.error);
                  }else{
                    var expid_ = $(form).attr('tt-parent-id');
                    dis.trigger('reload', expid_);
                  }
                },
                error : function(a,b,c){
                  alert('Response Error');
                },
                complete: function() {
                  $(form).find('button[type=submit]').attr('disabled',false);
                  modal.modal('hide');
                }
              });
              return false;
            }
          });
        });
        return false;
      });

      $('.btn-list-sasaran-program, .btn-list-sasaran-kegiatan', dis).click(function() {
        var href = $(this).attr('href');
        var modal = $('#modalSasaranProgram');

        modal.modal('show');
        $('.modal-body', modal).unbind('reload').bind('reload', function() {
          $('.modal-body', modal).load(href, function() {});
        }).trigger('reload');
        return false;
      });
    });
  }).trigger('reload', '<?=!empty($expandid)?$expandid:''?>');
  $('.btn-refresh-program').click(function() {
    $(this).closest('tr').next().find('.div-renja').trigger('reload');
  });

  $('.modal').on('hidden.bs.modal', function (event) {
      $(this).find(".modal-body").empty();
  });

  $('#browseOPD').on('show.bs.modal', function (event) {
      var modalBody = $(".modal-body", $("#browseOPD"));
      $(this).removeData('bs.modal');
      modalBody.html("<p style='font-style: italic'>Loading..</p>");
      modalBody.load("<?=site_url("ajax/browse-opd")?>", function () {
          $("[name=selID][type=hidden]", modalBody).unbind().change(function () {
              var kdSub = $(this).val().split('|');
              $("[name=Kd_Urusan]").val(kdSub[0]);
              $("[name=Kd_Bidang]").val(kdSub[1]);
              $("[name=Kd_Unit]").val(kdSub[2]);
              $("[name=Kd_Sub]").val(kdSub[3]);
          });
          $("[name=selText][type=hidden]", modalBody).unbind().change(function () {
              $("[name=text-opd]").val($(this).val());
          });
      });
  });

  $('.btn-add-program').click(function() {
    var dis = $(this);
    var href = $(this).attr('href');
    var modal = $('#modalProgram');

    modal.modal('show');
    $('.modal-body', modal).load(href, function() {
      $("select").not('.no-clear').not('.no-select2').select2({ width: 'resolve', allowClear: true });
      //$("#form-program").attr('action', href);
      $("#form-program").validate({
        submitHandler: function(form) {
          $(form).find('button[type=submit]').attr('disabled',true);
          $(form).ajaxSubmit({
            dataType: 'json',
            type : 'post',
            success: function(data){
              if(data.error == 0) {
                dis.closest('tr').next().find('.div-renja').trigger('reload');
              } else {
                alert(data.error);
              }
            },
            error: function(a,b,c){
              alert('Response Error');
            },
            complete: function() {
              $(form).find('button[type=submit]').attr('disabled',false);
              modal.modal('hide');
            }
          });
          return false;
        }
      });
    });
    return false;
  });
});
</script>
<?php $this->load->view('footer')?>
