<?php
$rpemda = $this->db->where(COL_KD_PEMDA, $program[COL_KD_PEMDA])->get(TBL_SAKIP_MPEMDA)->row_array();
?>
<?=form_open(current_url(),array('role'=>'form','id'=>'form-program', 'class'=>'form-horizontal'))?>
<div class="form-group">
  <label class="control-label col-sm-2">Kode Sasaran</label>
  <div class="col-sm-2">
    <input type="number" placeholder="Kode" class="form-control" name="<?=COL_KD_SASARANPROGRAMOPD?>" value="<?=!empty($data)?$data[COL_KD_SASARANPROGRAMOPD]:''?>" required />
  </div>
</div>
<div class="form-group">
  <label class="control-label col-sm-2">Nama Sasaran</label>
  <div class="col-sm-8">
    <input type="text" placeholder="Sasaran" class="form-control" name="<?=COL_NM_SASARANPROGRAMOPD?>" value="<?=!empty($data)?$data[COL_NM_SASARANPROGRAMOPD]:''?>" required />
  </div>
</div>
<div class="row" style="padding-top: 10px; border-top: 1px solid #f4f4f4">
  <div class="col-sm-12">
    <h4>Indikator :</h4>
    <table class="table table-bordered" id="tbl-det">
        <thead>
        <tr>
            <th style="width: 60px">No.</th>
            <th style="min-width: 240px">Deskripsi & Target</th>
            <th>Formulasi</th>
            <th>Sumber Data</th>
            <th style="width: 40px"><button type="button" id="btn-add-det" class="btn btn-default btn-flat"><i class="fa fa-plus"></i></button></th>
        </tr>
        </thead>
        <tbody>
        <tr class="tr-blueprint-det" style="display: none">
          <td><input type="text" name="text-det-no" class="form-control" placeholder="No" style="width: 50px" /></td>
          <td>
            <div class="row" style="margin-bottom: 10px">
              <div class="col-sm-12">
                  <input name="text-det-desc" class="form-control" placeholder="Deskripsi" />
              </div>
            </div>
            <div class="row" style="margin-bottom: 10px">
              <div class="col-sm-6">
                <select name="select-det-satuan" class="form-control no-select2" style="width: 100%">
                    <?=GetCombobox("SELECT * FROM ".TBL_SAKIP_MSATUAN." ORDER BY ".COL_NM_SATUAN, COL_KD_SATUAN, COL_NM_SATUAN)?>
                </select>
              </div>
              <div class="col-sm-6">
                <input type="text" name="text-det-target" placeholder="Target" class="form-control text-right money" />
              </div>
            </div>
            <?php
            if($rpemda[COL_KD_TAHUN_FROM] == $program[COL_KD_TAHUN]) {
              ?>
              <div class="row">
                <div class="col-sm-offset-6 col-sm-6">
                  <input type="text" name="text-det-awal" placeholder="Kondisi Awal" class="form-control text-right money" />
                </div>
              </div>
              <?php
            }
            ?>
          </td>
          <td><textarea name="text-det-formula" placeholder="Formulasi Perhitungan" class="form-control" rows="4"></textarea></td>
          <td><textarea name="text-det-sumber" placeholder="Sumber Data" class="form-control" rows="4"></textarea></td>
          <td>
            <button type="button" class="btn btn-default btn-flat btn-del-det"><i class="fa fa-minus"></i></button>
          </td>
        </tr>
        </tbody>
    </table>
  </div>
</div>
<script type="text/javascript">
$(".btn-del-det").click(function () {
    var row = $(this).closest("tr");
    row.remove();
});
$("#btn-add-det").click(function () {
  var tbl = $(this).closest("table");
  var blueprint = tbl.find(".tr-blueprint-det").first().clone();

  blueprint.appendTo(tbl).removeClass("tr-blueprint-det").show();
  $('select', blueprint).select2({ width: 'resolve', placeholder: "Satuan", allowClear: true });
  $('select', blueprint).removeClass('.no-select2');
  $(".money", blueprint).number(true, 2, '.', ',');
  $(".btn-del-det", blueprint).click(function () {
      var row = $(this).closest("tr");
      row.remove();
  });
});
</script>
<div class="row" style="padding-top: 10px; border-top: 1px solid #f4f4f4">
  <div class="col-sm-12 text-center">
    <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fas fa-times"></i>&nbsp;CLOSE</button>&nbsp;
    <button type="submit" class="btn btn-success"><i class="fas fa-check"></i>&nbsp;SUBMIT</button>
  </div>
</div>
<?=form_close()?>
