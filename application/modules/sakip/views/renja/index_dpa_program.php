<style>
.tooltip-inner {
   text-align: left;
}
.tooltip-inner td {
   padding: 0px 1px !important;
   white-space: nowrap;
}
.tooltip-inner td:last-child {
   text-align: right !important;
}
</style>
<table class="table table-hover tree-renja" data-expand-id="<?=!empty($expid)?$expid:''?>" >
  <thead>
    <th>Program / Kegiatan</th>
    <th>Unit Kerja</th>
    <th>Rencana Aksi</th>
    <th>Realisasi</th>
    <th>Opsi</th>
  </thead>
  <tbody>
    <?php
    if(count($rprogram) > 0) {
      foreach ($rprogram as $prg) {
        $unitCond = array(
          COL_KD_URUSAN=>$prg[COL_KD_URUSAN],
          COL_KD_BIDANG=>$prg[COL_KD_BIDANG],
          COL_KD_UNIT=>$prg[COL_KD_UNIT],
          COL_KD_SUB=>$prg[COL_KD_SUB],
          COL_KD_BID=>$prg[COL_KD_BID]
        );

        $mainCond = array(
          COL_KD_TAHUN=>$prg[COL_KD_TAHUN],
          COL_KD_URUSAN=>$prg[COL_KD_URUSAN],
          COL_KD_BIDANG=>$prg[COL_KD_BIDANG],
          COL_KD_UNIT=>$prg[COL_KD_UNIT],
          COL_KD_SUB=>$prg[COL_KD_SUB],
          COL_KD_BID=>$prg[COL_KD_BID],

          COL_KD_PEMDA=>$prg[COL_KD_PEMDA],
          COL_KD_MISI=>$prg[COL_KD_MISI],
          COL_KD_TUJUAN=>$prg[COL_KD_TUJUAN],
          COL_KD_INDIKATORTUJUAN=>$prg[COL_KD_INDIKATORTUJUAN],
          COL_KD_SASARAN=>$prg[COL_KD_SASARAN],
          COL_KD_INDIKATORSASARAN=>$prg[COL_KD_INDIKATORSASARAN],
          COL_KD_TUJUANOPD=>$prg[COL_KD_TUJUANOPD],
          COL_KD_INDIKATORTUJUANOPD=>$prg[COL_KD_INDIKATORTUJUANOPD],
          COL_KD_SASARANOPD=>$prg[COL_KD_SASARANOPD],
          COL_KD_INDIKATORSASARANOPD=>$prg[COL_KD_INDIKATORSASARANOPD],
          COL_KD_PROGRAMOPD=>$prg[COL_KD_PROGRAMOPD]
        );

        $rkegiatan = $this->db
        ->where($mainCond)
        ->order_by(COL_KD_KEGIATANOPD, 'asc')
        ->get(TBL_SAKIP_DPA_KEGIATAN)
        ->result_array();

        $rkegiatan_sum = $this->db
        ->select_sum(COL_BUDGET)
        ->where($mainCond)
        ->get(TBL_SAKIP_DPA_KEGIATAN)
        ->row_array();
        $rkegiatan_sump = $this->db
        ->select_sum(COL_PERGESERAN)
        ->where($mainCond)
        ->get(TBL_SAKIP_DPA_KEGIATAN)
        ->row_array();

        $qIndikator = $this->db
        ->where($mainCond)
        ->count_all_results(TBL_SAKIP_DPA_PROGRAM_INDIKATOR);

        $osTW1 = $this->db
        ->where($mainCond)
        ->where(COL_TARGET_TW1, null)
        ->count_all_results(TBL_SAKIP_DPA_PROGRAM_INDIKATOR);
        $osTW1_ = $this->db
        ->where($mainCond)
        ->where(COL_KINERJA_TW1, null)
        ->count_all_results(TBL_SAKIP_DPA_PROGRAM_INDIKATOR);

        $osTW2 = $this->db
        ->where($mainCond)
        ->where(COL_TARGET_TW2, null)
        ->count_all_results(TBL_SAKIP_DPA_PROGRAM_INDIKATOR);
        $osTW2_ = $this->db
        ->where($mainCond)
        ->where(COL_KINERJA_TW2, null)
        ->count_all_results(TBL_SAKIP_DPA_PROGRAM_INDIKATOR);

        $osTW3 = $this->db
        ->where($mainCond)
        ->where(COL_TARGET_TW3, null)
        ->count_all_results(TBL_SAKIP_DPA_PROGRAM_INDIKATOR);
        $osTW3_ = $this->db
        ->where($mainCond)
        ->where(COL_KINERJA_TW3, null)
        ->count_all_results(TBL_SAKIP_DPA_PROGRAM_INDIKATOR);

        $osTW4 = $this->db
        ->where($mainCond)
        ->where(COL_TARGET_TW4, null)
        ->count_all_results(TBL_SAKIP_DPA_PROGRAM_INDIKATOR);
        $osTW4_ = $this->db
        ->where($mainCond)
        ->where(COL_KINERJA_TW4, null)
        ->count_all_results(TBL_SAKIP_DPA_PROGRAM_INDIKATOR);

        $rbid = $this->db
        ->where($unitCond)
        ->get(TBL_SAKIP_MBID)
        ->row_array();

        $keyprg = $prg[COL_KD_PEMDA].'.'.
                  $prg[COL_KD_TAHUN].'.'.
                  $prg[COL_KD_URUSAN].'.'.
                  $prg[COL_KD_BIDANG].'.'.
                  $prg[COL_KD_UNIT].'.'.
                  $prg[COL_KD_SUB].'.'.
                  $prg[COL_KD_MISI].'.'.
                  $prg[COL_KD_TUJUAN].'.'.
                  $prg[COL_KD_INDIKATORTUJUAN].'.'.
                  $prg[COL_KD_SASARAN].'.'.
                  $prg[COL_KD_INDIKATORSASARAN].'.'.
                  $prg[COL_KD_TUJUANOPD].'.'.
                  $prg[COL_KD_INDIKATORTUJUANOPD].'.'.
                  $prg[COL_KD_SASARANOPD].'.'.
                  $prg[COL_KD_INDIKATORSASARANOPD];

        $htmlTooltip = '<table><tr><td>Budget</td><td>: Rp. </td><td><strong>'.number_format($rkegiatan_sum[COL_BUDGET]).'</strong></td></tr><tr><td>Stlh. Pergeseran</td><td>: Rp. </td><td><strong>'.number_format($rkegiatan_sump[COL_PERGESERAN]).'</strong></td></tr></table>';
        ?>
        <tr data-tt-id="<?=$keyprg.'.'.$prg[COL_KD_PROGRAMOPD].'.'.$prg[COL_KD_BID]?>">
          <td>
            <strong><?=$prg[COL_KD_PROGRAMOPD]?></strong>: <span style="padding: 0px" data-toggle="tooltip" data-html="true" data-placement="right" title="<?=$htmlTooltip?>"><?=$prg[COL_NM_PROGRAMOPD]?></span>
          </td>
          <td style="width: 15vw" class="text-sm"><?=!empty($rbid)?$rbid[COL_NM_BID]:'-'?></td>
          <td style="width: 11vw;">
            <span class="text-sm text-bold no-padding" style="margin-left: 5px"><i class="fa fa-check-circle <?=$qIndikator>0 && $osTW1>0?'text-gray':'text-success'?>"></i>&nbsp;I</span>
            <span class="text-sm text-bold no-padding" style="margin-left: 5px"><i class="fa fa-check-circle <?=$qIndikator>0 && $osTW2>0?'text-gray':'text-success'?>"></i>&nbsp;II</span>
            <span class="text-sm text-bold no-padding" style="margin-left: 5px"><i class="fa fa-check-circle <?=$qIndikator>0 && $osTW3>0?'text-gray':'text-success'?>"></i>&nbsp;III</span>
            <span class="text-sm text-bold no-padding" style="margin-left: 5px"><i class="fa fa-check-circle <?=$qIndikator>0 && $osTW4>0?'text-gray':'text-success'?>"></i>&nbsp;IV</span>
          </td>
          <td style="width: 11vw">
            <span class="text-sm text-bold no-padding" style="margin-left: 5px"><i class="fa fa-check-circle <?=$qIndikator>0 && $osTW1_>0?'text-gray':'text-success'?>"></i>&nbsp;I</span>
            <span class="text-sm text-bold no-padding" style="margin-left: 5px"><i class="fa fa-check-circle <?=$qIndikator>0 && $osTW2_>0?'text-gray':'text-success'?>"></i>&nbsp;II</span>
            <span class="text-sm text-bold no-padding" style="margin-left: 5px"><i class="fa fa-check-circle <?=$qIndikator>0 && $osTW3_>0?'text-gray':'text-success'?>"></i>&nbsp;III</span>
            <span class="text-sm text-bold no-padding" style="margin-left: 5px"><i class="fa fa-check-circle <?=$qIndikator>0 && $osTW4_>0?'text-gray':'text-success'?>"></i>&nbsp;IV</span>
          </td>
          <td style="width: 11vw">
            <div class="btn-group">
              <button type="button" class="btn btn-xs btn-primary dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                <i class="fas fa-edit"></i>&nbsp;AKSI&nbsp;<i class="fas fa-caret-down"></i>
              </button>
              <ul class="dropdown-menu">
                <li><a href="<?=site_url('sakip/renja/ubah-dpa-program/'.$prg[COL_UNIQ].'/'.$keyprg)?>" class="btn-edit-program"><i class="fas fa-edit"></i>&nbsp;UBAH</li>
                <li><a href="<?=site_url('sakip/renja/hapus-dpa-program/'.$prg[COL_UNIQ])?>" class="btn-delete-program"><i class="fas fa-trash"></i>&nbsp;HAPUS</a></li>
                <li><a href="<?=site_url('sakip/renja/tambah-dpa-sasaran-program/'.$prg[COL_UNIQ])?>" class="btn-tambah-sasaran-program"><i class="fas fa-plus"></i>&nbsp;SASARAN</a></li>
                <li><a href="<?=site_url('sakip/renja/tambah-dpa-keg/'.$prg[COL_UNIQ].'/'.$keyprg.'.'.$prg[COL_KD_BID].'.'.$prg[COL_KD_PROGRAMOPD])?>" class="btn-tambah-kegiatan"><i class="fas fa-plus"></i>&nbsp;KEGIATAN</a></li>
                <li><a href="<?=site_url('sakip/rencana-aksi/program-edit/'.$prg[COL_UNIQ])?>?redirect=<?=site_url('sakip/renja/index-dpa/'.$keyprg.'.'.$prg[COL_KD_PROGRAMOPD])?>"><i class="fas fa-plus"></i>&nbsp;RENCANA AKSI</a></li>
                <li><a href="<?=site_url('sakip/rencana-aksi/program-realisasi/'.$prg[COL_UNIQ])?>?redirect=<?=site_url('sakip/renja/index-dpa/'.$keyprg.'.'.$prg[COL_KD_PROGRAMOPD])?>"><i class="fas fa-plus"></i>&nbsp;REALISASI</a></li>
              </ul>
            </div>
            <a href="<?=site_url('sakip/renja/list-dpa-sasaran-program/'.$prg[COL_UNIQ])?>" class="btn btn-xs btn-warning btn-list-sasaran-program" style="padding: 2px 5px; font-size: 10px"><i class="fas fa-list"></i>&nbsp;SASARAN</a>
          </td>
        </tr>
        <?php
        foreach ($rkegiatan as $keg) {
          $arrCondKeg = array(
            COL_KD_TAHUN=>$keg[COL_KD_TAHUN],
            COL_KD_URUSAN=>$keg[COL_KD_URUSAN],
            COL_KD_BIDANG=>$keg[COL_KD_BIDANG],
            COL_KD_UNIT=>$keg[COL_KD_UNIT],
            COL_KD_SUB=>$keg[COL_KD_SUB],

            COL_KD_PEMDA=>$keg[COL_KD_PEMDA],
            COL_KD_MISI=>$keg[COL_KD_MISI],
            COL_KD_TUJUAN=>$keg[COL_KD_TUJUAN],
            COL_KD_INDIKATORTUJUAN=>$keg[COL_KD_INDIKATORTUJUAN],
            COL_KD_SASARAN=>$keg[COL_KD_SASARAN],
            COL_KD_INDIKATORSASARAN=>$keg[COL_KD_INDIKATORSASARAN],
            COL_KD_TUJUANOPD=>$keg[COL_KD_TUJUANOPD],
            COL_KD_INDIKATORTUJUANOPD=>$keg[COL_KD_INDIKATORTUJUANOPD],
            COL_KD_SASARANOPD=>$keg[COL_KD_SASARANOPD],
            COL_KD_INDIKATORSASARANOPD=>$keg[COL_KD_INDIKATORSASARANOPD],
            COL_KD_PROGRAMOPD=>$keg[COL_KD_PROGRAMOPD],
            COL_KD_SASARANPROGRAMOPD=>$keg[COL_KD_SASARANPROGRAMOPD],
            COL_KD_BID=>$keg[COL_KD_BID],
            COL_KD_KEGIATANOPD=>$keg[COL_KD_KEGIATANOPD],
            COL_KD_SUBBID=>$keg[COL_KD_SUBBID]
          );
          $qIndikator = $this->db
          ->where($arrCondKeg)
          ->count_all_results(TBL_SAKIP_DPA_KEGIATAN_INDIKATOR);

          $osTW1 = $this->db
          ->where($arrCondKeg)
          ->where(COL_TARGET_TW1, null)
          ->count_all_results(TBL_SAKIP_DPA_KEGIATAN_INDIKATOR);
          $osTW1_ = $this->db
          ->where($arrCondKeg)
          ->where(COL_KINERJA_TW1, null)
          ->count_all_results(TBL_SAKIP_DPA_KEGIATAN_INDIKATOR);

          $osTW2 = $this->db
          ->where($arrCondKeg)
          ->where(COL_TARGET_TW2, null)
          ->count_all_results(TBL_SAKIP_DPA_KEGIATAN_INDIKATOR);
          $osTW2_ = $this->db
          ->where($arrCondKeg)
          ->where(COL_KINERJA_TW2, null)
          ->count_all_results(TBL_SAKIP_DPA_KEGIATAN_INDIKATOR);

          $osTW3 = $this->db
          ->where($arrCondKeg)
          ->where(COL_TARGET_TW3, null)
          ->count_all_results(TBL_SAKIP_DPA_KEGIATAN_INDIKATOR);
          $osTW3_ = $this->db
          ->where($arrCondKeg)
          ->where(COL_KINERJA_TW3, null)
          ->count_all_results(TBL_SAKIP_DPA_KEGIATAN_INDIKATOR);

          $osTW4 = $this->db
          ->where($arrCondKeg)
          ->where(COL_TARGET_TW4, null)
          ->count_all_results(TBL_SAKIP_DPA_KEGIATAN_INDIKATOR);
          $osTW4_ = $this->db
          ->where($arrCondKeg)
          ->where(COL_KINERJA_TW4, null)
          ->count_all_results(TBL_SAKIP_DPA_KEGIATAN_INDIKATOR);

          $rsubbid = $this->db
          ->where(array_merge($unitCond, array(COL_KD_SUBBID=>$keg[COL_KD_SUBBID])))
          ->get(TBL_SAKIP_MSUBBID)
          ->row_array();

          $arrSubKegiatan = array();
          if(!empty($keg[COL_NM_ARRSUBKEGIATAN])) {
            $arrSubKegiatan = json_decode($keg[COL_NM_ARRSUBKEGIATAN]);
          }

          $idkeg = $keg[COL_KD_PEMDA].'.'.
                    $keg[COL_KD_TAHUN].'.'.
                    $keg[COL_KD_URUSAN].'.'.
                    $keg[COL_KD_BIDANG].'.'.
                    $keg[COL_KD_UNIT].'.'.
                    $keg[COL_KD_SUB].'.'.
                    $keg[COL_KD_MISI].'.'.
                    $keg[COL_KD_TUJUAN].'.'.
                    $keg[COL_KD_INDIKATORTUJUAN].'.'.
                    $keg[COL_KD_SASARAN].'.'.
                    $keg[COL_KD_INDIKATORSASARAN].'.'.
                    $keg[COL_KD_TUJUANOPD].'.'.
                    $keg[COL_KD_INDIKATORTUJUANOPD].'.'.
                    $keg[COL_KD_SASARANOPD].'.'.
                    $keg[COL_KD_INDIKATORSASARANOPD].'.'.
                    $keg[COL_KD_PROGRAMOPD].'.'.
                    $keg[COL_KD_BID].'.'.
                    $keg[COL_KD_SASARANPROGRAMOPD];

          $htmlTooltip = '<table><tr><td>Budget</td><td>: Rp. </td><td><strong>'.number_format($keg[COL_BUDGET]).'</strong></td></tr><tr><td>Stlh. Pergeseran</td><td>: Rp. </td><td><strong>'.number_format($keg[COL_PERGESERAN]).'</strong></td></tr></table>';
          ?>
          <tr data-tt-id="<?=$idkeg.'.'.$keg[COL_KD_KEGIATANOPD].'.'.$keg[COL_KD_BID].'.'.$keg[COL_KD_SUBBID]?>" data-tt-parent-id="<?=$keyprg.'.'.$prg[COL_KD_PROGRAMOPD].'.'.$prg[COL_KD_BID]?>">
            <td>
              <strong><?=$keg[COL_KD_PROGRAMOPD].'.'.$keg[COL_KD_KEGIATANOPD]?></strong>: <span style="padding: 0px" data-html="true" data-toggle="tooltip" data-placement="right" title="<?=$htmlTooltip?>"><?=$keg[COL_NM_KEGIATANOPD]?></span>
            </td>
            <td style="width: 15vw" class="text-sm"><?=!empty($rsubbid)?$rsubbid[COL_NM_SUBBID]:'-'?></td>
            <td style="width: 11vw;">
              <span class="text-sm text-bold no-padding" style="margin-left: 5px"><i class="fa fa-check-circle <?=empty($keg[COL_BUDGET_TW1])||($qIndikator>0&&$osTW1>0)?'text-gray':'text-success'?>"></i>&nbsp;I</span>
              <span class="text-sm text-bold no-padding" style="margin-left: 5px"><i class="fa fa-check-circle <?=empty($keg[COL_BUDGET_TW2])||($qIndikator>0&&$osTW2>0)?'text-gray':'text-success'?>"></i>&nbsp;II</span>
              <span class="text-sm text-bold no-padding" style="margin-left: 5px"><i class="fa fa-check-circle <?=empty($keg[COL_BUDGET_TW3])||($qIndikator>0&&$osTW3>0)?'text-gray':'text-success'?>"></i>&nbsp;III</span>
              <span class="text-sm text-bold no-padding" style="margin-left: 5px"><i class="fa fa-check-circle <?=empty($keg[COL_BUDGET_TW4])||($qIndikator>0&&$osTW4>0)?'text-gray':'text-success'?>"></i>&nbsp;IV</span>
            </td>
            <td style="width: 11vw">
              <span class="text-sm text-bold no-padding" style="margin-left: 5px"><i class="fa fa-check-circle <?=empty($keg[COL_ANGGARAN_TW1])||($qIndikator>0&&$osTW1_>0)?'text-gray':'text-success'?>"></i>&nbsp;I</span>
              <span class="text-sm text-bold no-padding" style="margin-left: 5px"><i class="fa fa-check-circle <?=empty($keg[COL_ANGGARAN_TW2])||($qIndikator>0&&$osTW2_>0)?'text-gray':'text-success'?>"></i>&nbsp;II</span>
              <span class="text-sm text-bold no-padding" style="margin-left: 5px"><i class="fa fa-check-circle <?=empty($keg[COL_ANGGARAN_TW3])||($qIndikator>0&&$osTW3_>0)?'text-gray':'text-success'?>"></i>&nbsp;III</span>
              <span class="text-sm text-bold no-padding" style="margin-left: 5px"><i class="fa fa-check-circle <?=empty($keg[COL_ANGGARAN_TW4])||($qIndikator>0&&$osTW4_>0)?'text-gray':'text-success'?>"></i>&nbsp;IV</span>
            </td>
            <td style="width: 11vw">
              <div class="btn-group">
                <button type="button" class="btn btn-xs btn-primary dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                  <i class="fas fa-edit"></i>&nbsp;AKSI&nbsp;<i class="fas fa-caret-down"></i>
                </button>
                <ul class="dropdown-menu">
                  <li><a href="<?=site_url('sakip/renja/ubah-dpa-keg/'.$keg[COL_UNIQ].'/'.$keyprg.'.'.$prg[COL_KD_BID].'.'.$prg[COL_KD_PROGRAMOPD])?>" class="btn-edit-kegiatan"><i class="fas fa-edit"></i>&nbsp;UBAH</li>
                  <li><a href="<?=site_url('sakip/renja/hapus-dpa-kegiatan/'.$keg[COL_UNIQ])?>" class="btn-delete-kegiatan"><i class="fas fa-trash"></i>&nbsp;HAPUS</a></li>
                  <li><a href="<?=site_url('sakip/renja/tambah-dpa-sasaran-kegiatan/'.$keg[COL_UNIQ])?>" class="btn-tambah-sasaran-kegiatan"><i class="fas fa-plus"></i>&nbsp;SASARAN</a></li>
                  <li><a href="<?=site_url('sakip/rencana-aksi/kegiatan-edit/'.$keg[COL_UNIQ])?>?redirect=<?=site_url('sakip/renja/index-dpa/'.$keyprg.'.'.$prg[COL_KD_PROGRAMOPD])?>"><i class="fas fa-plus"></i>&nbsp;RENCANA AKSI</a></li>
                  <li><a href="<?=site_url('sakip/rencana-aksi/kegiatan-realisasi/'.$keg[COL_UNIQ])?>?redirect=<?=site_url('sakip/renja/index-dpa/'.$keyprg.'.'.$prg[COL_KD_PROGRAMOPD])?>"><i class="fas fa-plus"></i>&nbsp;REALISASI</a></li>
                </ul>
              </div>
              <a href="<?=site_url('sakip/renja/list-dpa-sasaran-kegiatan/'.$keg[COL_UNIQ])?>" class="btn btn-xs btn-warning btn-list-sasaran-kegiatan" style="padding: 2px 5px; font-size: 10px"><i class="fas fa-list"></i>&nbsp;SASARAN</a>
            </td>
          </tr>
          <?php
          if(!empty($arrSubKegiatan)) {
            foreach($arrSubKegiatan as $subkeg) {
              ?>
              <tr data-tt-id="<?=$idkeg.'.'.$keg[COL_KD_KEGIATANOPD].'.'.$keg[COL_KD_BID].'.'.$keg[COL_KD_SUBBID].'.'.$subkeg->KdSubKegiatan?>" data-tt-parent-id="<?=$idkeg.'.'.$keg[COL_KD_KEGIATANOPD].'.'.$keg[COL_KD_BID].'.'.$keg[COL_KD_SUBBID]?>">
                <td style="font-size: 9pt !important">
                  <strong><?=$keg[COL_KD_PROGRAMOPD].'.'.$keg[COL_KD_KEGIATANOPD].'.'.$subkeg->KdSubKegiatan?></strong>:
                  <span style="padding: 0px" data-toggle="tooltip" data-placement="right" title="<?='Rp. '.number_format($subkeg->TotalSubKegiatan)?>">
                    <?=$subkeg->NmSubKegiatan?>
                  </span>
                </td>
                <td colspan="2"></td>
              </tr>
              <?php
            }
          }
        }
      }
    } else {
      ?>
      <tr>
        <td colspan="4">Belum ada data.</td>
      </tr>
      <?php
    }
    ?>
  </tbody>
</table>
<script>
$(document).ready(function() {
  $('.btn-group>ul.dropdown-menu>li>a', $('.tree-renja')).click(function(){
    $(this).closest('.btn-group').removeClass('open');
  });
});
</script>
