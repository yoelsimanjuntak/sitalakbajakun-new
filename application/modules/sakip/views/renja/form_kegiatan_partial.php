<?php
if(!empty($rdpa)) {
  ?>
  <p class="text-danger">Kegiatan ini sudah diinput ke DPA. Silakan menghapus kegiatan berikut pada DPA terlebih dahulu jika ingin mengubah kegiatan ini.</p>
  <?php
  exit();
}

$cond = array(
  COL_KD_PEMDA=>$program[COL_KD_PEMDA],
  COL_KD_TAHUN=>$program[COL_KD_TAHUN],
  COL_KD_URUSAN=>$program[COL_KD_URUSAN],
  COL_KD_BIDANG=>$program[COL_KD_BIDANG],
  COL_KD_UNIT=>$program[COL_KD_UNIT],
  COL_KD_SUB=>$program[COL_KD_SUB],
  COL_KD_MISI=>$program[COL_KD_MISI],
  COL_KD_TUJUAN=>$program[COL_KD_TUJUAN],
  COL_KD_INDIKATORTUJUAN=>$program[COL_KD_INDIKATORTUJUAN],
  COL_KD_SASARAN=>$program[COL_KD_SASARAN],
  COL_KD_INDIKATORSASARAN=>$program[COL_KD_INDIKATORSASARAN],
  COL_KD_TUJUANOPD=>$program[COL_KD_TUJUANOPD],
  COL_KD_INDIKATORTUJUANOPD=>$program[COL_KD_INDIKATORTUJUANOPD],
  COL_KD_SASARANOPD=>$program[COL_KD_SASARANOPD],
  COL_KD_INDIKATORSASARANOPD=>$program[COL_KD_INDIKATORSASARANOPD],
  COL_KD_BID=>$program[COL_KD_BID],
  COL_KD_PROGRAMOPD=>$program[COL_KD_PROGRAMOPD]
);
$rsasaranprog = $this->db
->where($cond)
->order_by(COL_KD_SASARANPROGRAMOPD, 'asc')
->get(TBL_SAKIP_MBID_PROGRAM_SASARAN)
->result_array();
$qsasaranprog = $this->db->last_query();
?>
<?=form_open(current_url(),array('role'=>'form','id'=>'form-program', 'class'=>'form-horizontal'))?>
<div class="form-group">
  <label class="control-label col-sm-3">Sasaran Program</label>
  <div class="col-sm-8">
    <select name="<?=COL_KD_SASARANPROGRAMOPD?>" class="form-control" style="width: 100%" required <?=!empty($dpa)?'disabled':''?>>
      <?=GetCombobox($qsasaranprog, COL_KD_SASARANPROGRAMOPD, COL_NM_SASARANPROGRAMOPD, (!empty($data)?$data[COL_KD_SASARANPROGRAMOPD]:null))?>
    </select>
  </div>
</div>
<div class="form-group">
  <label class="control-label col-sm-3">Sub Bidang</label>
  <div class="col-sm-8">
    <select name="<?=COL_KD_SUBBID?>" class="form-control" style="width: 100%" required <?=!empty($dpa)?'disabled':''?>>
      <?=GetCombobox("select * from sakip_msubbid where Kd_Urusan = $kdUrusan and Kd_Bidang = $kdBidang and Kd_Unit = $kdUnit and Kd_Sub = $kdSub and Kd_Bid = $kdBid order by Nm_Subbid", COL_KD_SUBBID, COL_NM_SUBBID, (!empty($data)?$data[COL_KD_SUBBID]:null))?>
    </select>
  </div>
</div>
<div class="form-group">
  <label class="control-label col-sm-3">Kode Kegiatan</label>
  <div class="col-sm-4">
    <input type="number" placeholder="Kode" class="form-control" name="<?=COL_KD_KEGIATANOPD?>" value="<?=!empty($data)?$data[COL_KD_KEGIATANOPD]:''?>" required <?=!empty($dpa)?'disabled':''?> />
  </div>
</div>
<div class="form-group">
  <label class="control-label col-sm-3">Nama Kegiatan</label>
  <div class="col-sm-8">
    <input type="text" placeholder="Nama" class="form-control" name="<?=COL_NM_KEGIATANOPD?>" value="<?=!empty($data)?$data[COL_NM_KEGIATANOPD]:''?>" required <?=!empty($dpa)?'disabled':''?> />
  </div>
</div>
<div class="form-group">
  <label class="control-label col-sm-3">Sumber Dana</label>
  <div class="col-sm-2">
    <select name="<?=COL_KD_SUMBERDANA?>" class="form-control">
      <?=GetCombobox("SELECT * FROM ".TBL_SAKIP_MSUMBERDANA." ORDER BY ".COL_NM_SUMBERDANA, COL_KD_SUMBERDANA, COL_NM_SUMBERDANA, (!empty($data) ? $data[COL_KD_SUMBERDANA] : null))?>
    </select>
  </div>
</div>
<div class="form-group">
  <label class="control-label col-sm-3"><?=!empty($dpa)?'Budget':'Pagu'?></label>
  <div class="col-sm-3" id="div_pagu">
    <input type="text" class="form-control uang" placeholder="Pagu" name="<?=COL_TOTAL?>" value="<?= !empty($data) ? $data[COL_TOTAL] : ""?>" style="text-align: right" required readonly />
  </div>
</div>
<?php
if(!empty($rdpa)) {
  ?>
  <div class="form-group">
    <label class="control-label col-sm-3">Budget (DPA)</label>
    <div class="col-sm-3">
      <input type="text" class="form-control uang" placeholder="Budget (DPA)" name="<?=COL_BUDGET?>" value="<?= !empty($rdpa) ? $rdpa[COL_BUDGET] : ""?>" style="text-align: right" required />
    </div>
  </div>
  <?php
}
?>
<?php
if(empty($dpa)) {
  ?>
  <div class="form-group">
    <label class="control-label col-sm-3">Catatan</label>
    <div class="col-sm-8">
      <textarea placeholder="Catatan" class="form-control" name="<?=COL_REMARKS?>"><?=!empty($data)?$data[COL_REMARKS]:''?></textarea>
    </div>
  </div>
  <?php
}
?>
<div class="row" style="border-top: 1px solid #f4f4f4">
  <div class="col-sm-12">
    <h5 style="font-weight: bold">SUB KEGIATAN</h5>
    <table id="tbl-subkegiatan" class="table table-bordered">
      <thead class="bg-default">
        <tr>
          <th style="width: 50px; text-align: left !important">Kode</th>
          <th style="width: 200px; text-align: left !important">Sub Kegiatan</th>
          <th style="width: 200px; text-align: left !important">Output</th>
          <th style="width: 100px; text-align: left !important">Biaya</th>
          <th class="text-center" style="width: 10px; white-space: nowrap">
            <button type="button" id="btn-add-subkegiatan" class="btn btn-xs btn-default" style="font-weight: bold" <?=!empty($dpa)?'disabled':''?>><i class="fa fa-plus"></i></button>
          </th>
        </tr>
      </thead>
      <tbody>
        <?php
        if(!empty($data[COL_NM_ARRSUBKEGIATAN])) {
          $arrSubKegiatan = json_decode($data[COL_NM_ARRSUBKEGIATAN]);
          if(!empty($arrSubKegiatan)) {
            foreach($arrSubKegiatan as $r) {
              ?>
              <tr>
                <td><input type="text" class="form-control input-sm" name="KdSubKegiatan[]" value="<?=!empty($r->KdSubKegiatan)?$r->KdSubKegiatan:''?>" <?=!empty($dpa)?'readonly':''?> /></td>
                <td>
                  <textarea class="form-control input-sm" name="NmSubKegiatan[]" <?=!empty($dpa)?'readonly':''?>><?=!empty($r->NmSubKegiatan)?$r->NmSubKegiatan:''?></textarea>
                </td>
                <td>
                  <textarea class="form-control input-sm" name="NmIndikatorSubKegiatan[]" <?=!empty($dpa)?'readonly':''?>><?=!empty($r->NmIndikatorSubKegiatan)?$r->NmIndikatorSubKegiatan:''?></textarea>
                </td>
                <td><input type="text" class="form-control input-sm uang text-right" name="TotalSubKegiatan[]" value="<?=!empty($r->TotalSubKegiatan)?$r->TotalSubKegiatan:0?>" /></td>
                <td><button type="button" class="btn btn-xs btn-danger btn-del-subkegiatan" style="font-weight: bold" <?=!empty($dpa)?'disabled':''?>><i class="fa fa-minus"></i></button></td>
              </tr>
              <?php
            }
          }
        }
        ?>
      </tbody>
    </table>
  </div>
</div>
<?php
if(!empty($dpa)) {
  ?>
  <input type="hidden" value="1" name="<?=COL_IS_RENJA?>" />
  <p class="help-block">Klik tombol "<strong>SUBMIT</strong>" untuk memasukkan data ini ke DPA.</p>
  <?php
}
?>
<div class="row" style="padding-top: 10px; border-top: 1px solid #f4f4f4">
  <div class="col-sm-12 text-center">
    <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fas fa-times"></i>&nbsp;CLOSE</button>&nbsp;
    <button type="submit" class="btn btn-success"><i class="fas fa-check"></i>&nbsp;SUBMIT</button>
  </div>
</div>
<?=form_close()?>
<script type="text/javascript">
function calRowSubKegiatan() {
  var elTotalSubKegiatan = $('[name^=TotalSubKegiatan]', $('tbody', $('#tbl-subkegiatan')));
  var sumSubKegiatan = 0;
  for(var i=0; i<elTotalSubKegiatan.length; i++) {
    var total_ = $(elTotalSubKegiatan[i]).val();
    if(total_) {
      sumSubKegiatan += parseInt(total_);
    }
  }

  $('[name=Total]').val(sumSubKegiatan).trigger('change');
}
function addRowSubKegiatan() {
  var html =' ';
  html += '<tr>';
  html += '<td>';
  html += '<input type="text" class="form-control input-sm" name="KdSubKegiatan[]" />';
  html += '</td>';
  html += '<td>';
  html += '<textarea class="form-control input-sm" name="NmSubKegiatan[]" placeholder="Nama Sub Kegiatan"></textarea>';
  html += '</td>';
  html += '<td>';
  html += '<textarea class="form-control input-sm" name="NmIndikatorSubKegiatan[]" placeholder="Output / Indikator Keberhasilan"></textarea>';
  html += '</td>';
  html += '<td>';
  html += '<input type="text" class="form-control input-sm uang text-right" name="TotalSubKegiatan[]" placeholder="Biaya" />';
  html += '</td>';
  html += '<td>';
  html += '<button type="button" class="btn btn-xs btn-danger btn-del-subkegiatan" style="font-weight: bold"><i class="fa fa-minus"></i></button>';
  html += '</td>';
  html += '</tr>';

  $('tbody', $('#tbl-subkegiatan')).append(html);
  $(".uang", $('tbody', $('#tbl-subkegiatan'))).number(true, 0, '.', ',');
  $('.btn-del-subkegiatan', $('tbody', $('#tbl-subkegiatan'))).unbind('click').click(function() {
    var row = $(this).closest('tr');
    row.remove();
    calRowSubKegiatan();
  });
  $('[name^=TotalSubKegiatan]', $('tbody', $('#tbl-subkegiatan'))).unbind('change').change(function() {
    calRowSubKegiatan();
  });
}

$(document).ready(function() {
  $('.btn-del-subkegiatan', $('tbody', $('#tbl-subkegiatan'))).unbind('click').click(function() {
    var row = $(this).closest('tr');
    row.remove();
    calRowSubKegiatan();
  });
  $('[name^=TotalSubKegiatan]', $('tbody', $('#tbl-subkegiatan'))).unbind('change').change(function() {
    calRowSubKegiatan();
  });

  $('#btn-add-subkegiatan').click(function() {
    addRowSubKegiatan();
  });
});
</script>
