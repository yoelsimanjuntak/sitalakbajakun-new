<?php
/**
 * Created by PhpStorm.
 * User: Toshiba
 * Date: 09/07/2019
 * Time: 14:27
 */
$this->load->view('header') ?>
<?php
$ruser = GetLoggedUser();
$redir = $this->input->get("redirect");
?>
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1> <?= $title ?> <small> Form</small></h1>
        <ol class="breadcrumb">
            <li><a href="<?=site_url()?>"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="<?=site_url('dpa/sasaran-program')?>">Rencana Aksi - Program</a></li>
            <li class="active"><?=$edit?'Edit':'Add'?></li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-sm-12">
                <div class="box box-solid">
                    <div class="box-body no-padding">
                        <table class="table table-bordered table-hover">
                            <tr>
                                <td>OPD</td>
                                <td style="width: 10px;">:</td>
                                <td>
                                    <?php
                                    $nmSub = "";
                                    $strOPD = explode('.', $ruser[COL_COMPANYID]);
                                    $eplandb = $this->load->database("eplan", true);
                                    $eplandb->where(COL_KD_URUSAN, $data[COL_KD_URUSAN]);
                                    $eplandb->where(COL_KD_BIDANG, $data[COL_KD_BIDANG]);
                                    $eplandb->where(COL_KD_UNIT, $data[COL_KD_UNIT]);
                                    $eplandb->where(COL_KD_SUB, $data[COL_KD_SUB]);
                                    $subunit = $eplandb->get("ref_sub_unit")->row_array();
                                    if($subunit) {
                                        $nmSub = $subunit["Nm_Sub_Unit"];
                                    }
                                    ?>
                                    <strong><?=$nmSub?></strong>
                                </td>
                            </tr>
                            <tr>
                                <td>Bidang</td>
                                <td style="width: 10px;">:</td>
                                <td>
                                    <strong><?=$data[COL_NM_BID]?></strong>
                                </td>
                            </tr>
                            <tr>
                                <td>Program</td>
                                <td style="width: 10px;">:</td>
                                <td>
                                    <strong><?=$data[COL_NM_PROGRAMOPD]?></strong>
                                </td>
                            </tr>
                            <tr>
                                <td>Tahun</td>
                                <td style="width: 10px;">:</td>
                                <td>
                                    <strong><?=$data[COL_KD_TAHUN]?></strong>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="box box-primary">
                    <?=form_open(current_url().'?redirect='.$redir,array('role'=>'form','id'=>'main-form','class'=>'form-horizontal'))?>
                    <div class="box-header with-border">
                        <h5 class="box-title">Indikator</h5>
                    </div>
                    <div class="box-body">
                        <table class="table table-bordered" id="tbl-det">
                            <thead>
                            <tr>
                                <th style="width: 60px">No.</th>
                                <th style="min-width: 240px">Indikator</th>
                                <th>Satuan</th>
                                <th>Target</th>
                                <th>TW. I</th>
                                <th>TW. II</th>
                                <th>TW. III</th>
                                <th>TW. IV</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            $det = $this->db
                                ->where(COL_KD_PEMDA, $data[COL_KD_PEMDA])
                                ->where(COL_KD_MISI, $data[COL_KD_MISI])
                                ->where(COL_KD_TUJUAN, $data[COL_KD_TUJUAN])
                                ->where(COL_KD_INDIKATORTUJUAN, $data[COL_KD_INDIKATORTUJUAN])
                                ->where(COL_KD_SASARAN, $data[COL_KD_SASARAN])
                                ->where(COL_KD_INDIKATORSASARAN, $data[COL_KD_INDIKATORSASARAN])
                                ->where(COL_KD_TUJUANOPD, $data[COL_KD_TUJUANOPD])
                                ->where(COL_KD_INDIKATORTUJUANOPD, $data[COL_KD_INDIKATORTUJUANOPD])
                                ->where(COL_KD_SASARANOPD, $data[COL_KD_SASARANOPD])
                                ->where(COL_KD_INDIKATORSASARANOPD, $data[COL_KD_INDIKATORSASARANOPD])
                                ->where(COL_KD_TAHUN, $data[COL_KD_TAHUN])
                                ->where(COL_KD_URUSAN, $data[COL_KD_URUSAN])
                                ->where(COL_KD_BIDANG, $data[COL_KD_BIDANG])
                                ->where(COL_KD_UNIT, $data[COL_KD_UNIT])
                                ->where(COL_KD_SUB, $data[COL_KD_SUB])
                                ->where(COL_KD_BID, $data[COL_KD_BID])
                                ->where(COL_KD_PROGRAMOPD, $data[COL_KD_PROGRAMOPD])
                                ->order_by(COL_KD_SASARANPROGRAMOPD, 'asc')
                                ->order_by(COL_KD_INDIKATORPROGRAMOPD, 'asc')
                                ->get(TBL_SAKIP_DPA_PROGRAM_INDIKATOR)->result_array();
                            $i = 0;
                            foreach($det as $m) {
                                ?>
                                <tr>
                                    <td style="vertical-align: middle; text-align: right">
                                        <input type="hidden" name="<?=COL_KD_URUSAN?>[<?=$i?>]" value="<?= $data[COL_KD_URUSAN]?>" required />
                                        <input type="hidden" name="<?=COL_KD_BIDANG?>[<?=$i?>]" value="<?= $data[COL_KD_BIDANG]?>" required />
                                        <input type="hidden" name="<?=COL_KD_UNIT?>[<?=$i?>]" value="<?= $data[COL_KD_UNIT]?>" required />
                                        <input type="hidden" name="<?=COL_KD_SUB?>[<?=$i?>]" value="<?= $data[COL_KD_SUB]?>" required />
                                        <input type="hidden" name="<?=COL_KD_BID?>[<?=$i?>]" value="<?= $data[COL_KD_BID]?>" required />

                                        <input type="hidden" name="<?=COL_KD_PEMDA?>[<?=$i?>]" value="<?= $data[COL_KD_PEMDA]?>" required />
                                        <input type="hidden" name="<?=COL_KD_TAHUN_FROM?>[<?=$i?>]" value="<?= $data[COL_KD_TAHUN_FROM]?>" required />
                                        <input type="hidden" name="<?=COL_KD_TAHUN_TO?>[<?=$i?>]" value="<?= $data[COL_KD_TAHUN_TO]?>" required />
                                        <input type="hidden" name="<?=COL_KD_MISI?>[<?=$i?>]" value="<?= $data[COL_KD_MISI]?>" required />
                                        <input type="hidden" name="<?=COL_KD_TUJUAN?>[<?=$i?>]" value="<?= $data[COL_KD_TUJUAN]?>" required />
                                        <input type="hidden" name="<?=COL_KD_INDIKATORTUJUAN?>[<?=$i?>]" value="<?= $data[COL_KD_INDIKATORTUJUAN]?>" required />
                                        <input type="hidden" name="<?=COL_KD_SASARAN?>[<?=$i?>]" value="<?= $data[COL_KD_SASARAN]?>" required />
                                        <input type="hidden" name="<?=COL_KD_INDIKATORSASARAN?>[<?=$i?>]" value="<?= $data[COL_KD_INDIKATORSASARAN]?>" required />
                                        <input type="hidden" name="<?=COL_KD_TUJUANOPD?>[<?=$i?>]" value="<?= $data[COL_KD_TUJUANOPD]?>" required />
                                        <input type="hidden" name="<?=COL_KD_INDIKATORTUJUANOPD?>[<?=$i?>]" value="<?= $data[COL_KD_INDIKATORTUJUANOPD]?>" required />
                                        <input type="hidden" name="<?=COL_KD_SASARANOPD?>[<?=$i?>]" value="<?= $data[COL_KD_SASARANOPD]?>" required />
                                        <input type="hidden" name="<?=COL_KD_INDIKATORSASARANOPD?>[<?=$i?>]" value="<?= $data[COL_KD_INDIKATORSASARANOPD]?>" required />
                                        <input type="hidden" name="<?=COL_KD_PROGRAMOPD?>[<?=$i?>]" value="<?= $data[COL_KD_PROGRAMOPD]?>" required />
                                        <input type="hidden" name="<?=COL_KD_PROGRAMOPD?>[<?=$i?>]" value="<?= $data[COL_KD_PROGRAMOPD]?>" required />
                                        <input type="hidden" name="<?=COL_KD_TAHUN?>[<?=$i?>]" value="<?= $data[COL_KD_TAHUN]?>" required />
                                        <input type="hidden" name="<?=COL_KD_SASARANPROGRAMOPD?>[<?=$i?>]" value="<?= $m[COL_KD_SASARANPROGRAMOPD]?>" required />
                                        <input type="hidden" name="<?=COL_KD_INDIKATORPROGRAMOPD?>[<?=$i?>]" value="<?= $m[COL_KD_INDIKATORPROGRAMOPD]?>" required />
                                        <?=$m[COL_KD_SASARANPROGRAMOPD].".".$m[COL_KD_INDIKATORPROGRAMOPD]?>
                                    </td>
                                    <td style="vertical-align: middle"><?=$m[COL_NM_INDIKATORPROGRAMOPD]?></td>
                                    <td style="vertical-align: middle"><?=$m[COL_KD_SATUAN]?></td>
                                    <td style="vertical-align: middle; text-align: right"><?=number_format($m[COL_TARGET], 2)?></td>
                                    <td>
                                        <input type="text" class="form-control input-sm money" name="<?=COL_TARGET_TW1?>[<?=$i?>]" value="<?=$m[COL_TARGET_TW1]?>" style="text-align: right;" width="100px" />
                                    </td>
                                    <td>
                                        <input type="text" class="form-control input-sm money" name="<?=COL_TARGET_TW2?>[<?=$i?>]" value="<?=$m[COL_TARGET_TW2]?>" style="text-align: right" width="100px" />
                                    </td>
                                    <td>
                                        <input type="text" class="form-control input-sm money" name="<?=COL_TARGET_TW3?>[<?=$i?>]" value="<?=$m[COL_TARGET_TW3]?>" style="text-align: right" width="100px" />
                                    </td>
                                    <td>
                                        <input type="text" class="form-control input-sm money" name="<?=COL_TARGET_TW4?>[<?=$i?>]" value="<?=$m[COL_TARGET_TW4]?>" style="text-align: right" width="100px" />
                                    </td>
                                </tr>
                            <?php
                                $i++;
                            }
                            ?>
                            </tbody>
                        </table>
                    </div>
                    <div class="box-footer text-right">
                        <button type="submit" class="btn btn-primary btn-flat">Simpan</button>
                        <a href="<?=!empty($redir)?$redir:site_url('rencana-aksi/program')?>" class="btn btn-default btn-flat">Kembali ke Daftar&nbsp;&nbsp;<i class="fa fa-arrow-right"></i> </a>
                    </div>
                    <?=form_close()?>
                </div>
            </div>
        </div>
    </section>

<?php $this->load->view('loadjs') ?>
<script type="text/javascript">
    $("#main-form").validate({
        submitHandler : function(form){
            $(form).find('.btn').attr('disabled',true);
            $(form).ajaxSubmit({
                dataType: 'json',
                type : 'post',
                success : function(data){
                    $(form).find('.btn').attr('disabled',false);
                    if(data.error != 0){
                        $(".modal-body", $("#alertDialog")).html(data.error);
                        $("#alertDialog").modal('show');
                    }else{
                      window.location.href = data.redirect;
                    }
                },
                error : function(a,b,c){
                    //alert('Response Error');
                    $(".modal-body", $("#alertDialog")).html(a.responseText);
                    $("#alertDialog").modal('show');
                }
            });
            return false;
        }
    });
</script>
<?php $this->load->view('footer') ?>
