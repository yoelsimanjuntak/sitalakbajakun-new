<?php
/**
 * Created by PhpStorm.
 * User: Partopi Tao
 * Date: 3/14/2019
 * Time: 11:31 PM
 */
if(!empty($cetak)) {
    header("Content-type: application/vnd-ms-excel");
    header("Content-Disposition: attachment; filename=ESAKIP - Resume Renja.xls");
}
?>
<style>
    .tbl-info td {
        padding: 4px;
    }
</style>
<div class="table-responsive">
    <table class="table table-bordered" style="font-size: 9pt !important;" border="1">
        <caption style="text-align: center">
            <h5><?="RUMUSAN RENCANA KERJA PROGRAM DAN KEGIATAN ".strtoupper($nmSub)." TAHUN ".$data[COL_KD_TAHUN]." DAN PERKIRAAN MAJU TAHUN ".($data[COL_KD_TAHUN]+1)." KOTA TEBING TINGGI"?>
        </caption>
        <tbody>
        <tr>
            <th rowspan="2">No.</th>
            <th rowspan="2">Urusan / Bidang Urusan Pemerintahan Daerah dan Program / Kegiatan</th>
            <th rowspan="2">Indikator Kinerja Program / Kegiatan</th>
            <th colspan="4">Pagu Indikatif</th>
            <th rowspan="2">Catatan Penting</th>
            <th colspan="2">Prakiraan Maju Rencana Tahun <?=$data[COL_KD_TAHUN]+1?></th>
        </tr>
        <tr>
            <th>Lokasi</th>
            <th>Target Capaian Kinerja</th>
            <th>Kebutuhan Dana / Pagu Indikatif</th>
            <th>Sumber Dana</th>
            <th>Target Capaian Kinerja</th>
            <th>Kebutuhan Dana / Pagu Indikatif</th>
        </tr>
        <tr>
            <th>1</th>
            <th>2</th>
            <th>3</th>
            <th>4</th>
            <th>5</th>
            <th>6</th>
            <th>7</th>
            <th>8</th>
            <th>9</th>
            <th>10</th>
        </tr>
        <?php
        $counter_prg = 1;
        foreach($program as $prg) {
            $prg["Total_Prg"] = 0;
            $rprg_sasaran = $this->db
                ->where(COL_KD_URUSAN, $prg[COL_KD_URUSAN])
                ->where(COL_KD_BIDANG, $prg[COL_KD_BIDANG])
                ->where(COL_KD_UNIT, $prg[COL_KD_UNIT])
                ->where(COL_KD_SUB, $prg[COL_KD_SUB])
                ->where(COL_KD_BID, $prg[COL_KD_BID])

                ->where(COL_KD_PEMDA, $prg[COL_KD_PEMDA])
                ->where(COL_KD_MISI, $prg[COL_KD_MISI])
                ->where(COL_KD_TUJUAN, $prg[COL_KD_TUJUAN])
                ->where(COL_KD_INDIKATORTUJUAN, $prg[COL_KD_INDIKATORTUJUAN])
                ->where(COL_KD_SASARAN, $prg[COL_KD_SASARAN])
                ->where(COL_KD_INDIKATORSASARAN, $prg[COL_KD_INDIKATORSASARAN])
                ->where(COL_KD_TUJUANOPD, $prg[COL_KD_TUJUANOPD])
                ->where(COL_KD_INDIKATORTUJUANOPD, $prg[COL_KD_INDIKATORTUJUANOPD])
                ->where(COL_KD_SASARANOPD, $prg[COL_KD_SASARANOPD])
                ->where(COL_KD_INDIKATORSASARANOPD, $prg[COL_KD_INDIKATORSASARANOPD])
                ->where(COL_KD_PROGRAMOPD, $prg[COL_KD_PROGRAMOPD])
                ->where(COL_KD_TAHUN, $prg[COL_KD_TAHUN])
                ->order_by(TBL_SAKIP_MBID_PROGRAM_INDIKATOR.".".COL_KD_PROGRAMOPD, 'asc')
                ->order_by(TBL_SAKIP_MBID_PROGRAM_INDIKATOR.".".COL_KD_SASARANPROGRAMOPD, 'asc')
                ->get(TBL_SAKIP_MBID_PROGRAM_INDIKATOR)
                ->result_array();
            $rprg_indikator = $this->db
                ->where(COL_KD_URUSAN, $prg[COL_KD_URUSAN])
                ->where(COL_KD_BIDANG, $prg[COL_KD_BIDANG])
                ->where(COL_KD_UNIT, $prg[COL_KD_UNIT])
                ->where(COL_KD_SUB, $prg[COL_KD_SUB])
                ->where(COL_KD_BID, $prg[COL_KD_BID])

                ->where(COL_KD_PEMDA, $prg[COL_KD_PEMDA])
                ->where(COL_KD_MISI, $prg[COL_KD_MISI])
                ->where(COL_KD_TUJUAN, $prg[COL_KD_TUJUAN])
                ->where(COL_KD_INDIKATORTUJUAN, $prg[COL_KD_INDIKATORTUJUAN])
                ->where(COL_KD_SASARAN, $prg[COL_KD_SASARAN])
                ->where(COL_KD_INDIKATORSASARAN, $prg[COL_KD_INDIKATORSASARAN])
                ->where(COL_KD_TUJUANOPD, $prg[COL_KD_TUJUANOPD])
                ->where(COL_KD_INDIKATORTUJUANOPD, $prg[COL_KD_INDIKATORTUJUANOPD])
                ->where(COL_KD_SASARANOPD, $prg[COL_KD_SASARANOPD])
                ->where(COL_KD_INDIKATORSASARANOPD, $prg[COL_KD_INDIKATORSASARANOPD])
                ->where(COL_KD_PROGRAMOPD, $prg[COL_KD_PROGRAMOPD])
                ->where(COL_KD_TAHUN, $prg[COL_KD_TAHUN])
                ->order_by(TBL_SAKIP_MBID_PROGRAM_INDIKATOR.".".COL_KD_PROGRAMOPD, 'asc')
                ->order_by(TBL_SAKIP_MBID_PROGRAM_INDIKATOR.".".COL_KD_SASARANPROGRAMOPD, 'asc')
                ->order_by(TBL_SAKIP_MBID_PROGRAM_INDIKATOR.".".COL_KD_INDIKATORPROGRAMOPD, 'asc')
                ->get(TBL_SAKIP_MBID_PROGRAM_INDIKATOR)
                ->result_array();

            $rprg_n1 = $this->db
                ->select_sum(COL_TOTAL)
                ->where(COL_KD_URUSAN, $prg[COL_KD_URUSAN])
                ->where(COL_KD_BIDANG, $prg[COL_KD_BIDANG])
                ->where(COL_KD_UNIT, $prg[COL_KD_UNIT])
                ->where(COL_KD_SUB, $prg[COL_KD_SUB])
                ->where(COL_KD_BID, $prg[COL_KD_BID])

                ->where(COL_KD_PEMDA, $prg[COL_KD_PEMDA])
                ->where(COL_KD_MISI, $prg[COL_KD_MISI])
                ->where(COL_KD_TUJUAN, $prg[COL_KD_TUJUAN])
                ->where(COL_KD_INDIKATORTUJUAN, $prg[COL_KD_INDIKATORTUJUAN])
                ->where(COL_KD_SASARAN, $prg[COL_KD_SASARAN])
                ->where(COL_KD_INDIKATORSASARAN, $prg[COL_KD_INDIKATORSASARAN])
                ->where(COL_KD_TUJUANOPD, $prg[COL_KD_TUJUANOPD])
                ->where(COL_KD_INDIKATORTUJUANOPD, $prg[COL_KD_INDIKATORTUJUANOPD])
                ->where(COL_KD_SASARANOPD, $prg[COL_KD_SASARANOPD])
                ->where(COL_KD_INDIKATORSASARANOPD, $prg[COL_KD_INDIKATORSASARANOPD])
                ->where(COL_KD_PROGRAMOPD, $prg[COL_KD_PROGRAMOPD])
                ->where(COL_KD_TAHUN, $prg[COL_KD_TAHUN]+1)
                ->get(TBL_SAKIP_MSUBBID_KEGIATAN)
                ->row_array();

            $rprg_sasaran_n1 = $this->db
                ->where(COL_KD_URUSAN, $prg[COL_KD_URUSAN])
                ->where(COL_KD_BIDANG, $prg[COL_KD_BIDANG])
                ->where(COL_KD_UNIT, $prg[COL_KD_UNIT])
                ->where(COL_KD_SUB, $prg[COL_KD_SUB])
                ->where(COL_KD_BID, $prg[COL_KD_BID])

                ->where(COL_KD_PEMDA, $prg[COL_KD_PEMDA])
                ->where(COL_KD_MISI, $prg[COL_KD_MISI])
                ->where(COL_KD_TUJUAN, $prg[COL_KD_TUJUAN])
                ->where(COL_KD_INDIKATORTUJUAN, $prg[COL_KD_INDIKATORTUJUAN])
                ->where(COL_KD_SASARAN, $prg[COL_KD_SASARAN])
                ->where(COL_KD_INDIKATORSASARAN, $prg[COL_KD_INDIKATORSASARAN])
                ->where(COL_KD_TUJUANOPD, $prg[COL_KD_TUJUANOPD])
                ->where(COL_KD_INDIKATORTUJUANOPD, $prg[COL_KD_INDIKATORTUJUANOPD])
                ->where(COL_KD_SASARANOPD, $prg[COL_KD_SASARANOPD])
                ->where(COL_KD_INDIKATORSASARANOPD, $prg[COL_KD_INDIKATORSASARANOPD])
                ->where(COL_KD_PROGRAMOPD, $prg[COL_KD_PROGRAMOPD])
                ->where(COL_KD_TAHUN, $prg[COL_KD_TAHUN] + 1)
                ->order_by(TBL_SAKIP_MBID_PROGRAM_INDIKATOR.".".COL_KD_PROGRAMOPD, 'asc')
                ->order_by(TBL_SAKIP_MBID_PROGRAM_INDIKATOR.".".COL_KD_SASARANPROGRAMOPD, 'asc')
                ->get(TBL_SAKIP_MBID_PROGRAM_INDIKATOR)
                ->result_array();

            $sasaran_prg = "";
            if(count($rprg_sasaran) > 0) {
                if(count($rprg_sasaran) > 1) {
                    $sasaran_prg = "<ul style='margin-left: 0px; padding-left: 15px; text-align: left'>";
                    foreach($rprg_sasaran as $s) {
                        $sasaran_prg .= "<li style='white-space: nowrap'>".number_format($s[COL_TARGET], 2)." ".$s[COL_KD_SATUAN]."</li>";
                    }
                    $sasaran_prg .= "</ul>";
                }
                else {
                    $sasaran_prg = number_format($rprg_sasaran[0][COL_TARGET], 2)." ".$rprg_sasaran[0][COL_KD_SATUAN];
                }
            }

            $sasaran_prg_n1 = "";
            if(count($rprg_sasaran_n1) > 0) {
                if(count($rprg_sasaran_n1) > 1) {
                    $sasaran_prg_n1 = "<ul style='margin-left: 0px; padding-left: 15px; text-align: left'>";
                    foreach($rprg_sasaran_n1 as $s) {
                        $sasaran_prg_n1 .= "<li style='white-space: nowrap'>".number_format($s[COL_TARGET], 2)." ".$s[COL_KD_SATUAN]."</li>";
                    }
                    $sasaran_prg_n1 .= "</ul>";
                }
                else {
                    $sasaran_prg_n1 = number_format($rprg_sasaran_n1[0][COL_TARGET], 2)." ".$rprg_sasaran_n1[0][COL_KD_SATUAN];
                }
            }

            $indikator_prg = "";
            if(count($rprg_indikator) > 0) {
                if(count($rprg_indikator) > 1) {
                    $indikator_prg = "<ul style='margin-left: 0px; padding-left: 15px; text-align: left'>";
                    foreach($rprg_indikator as $s) {
                        $indikator_prg .= "<li>".$s[COL_NM_INDIKATORPROGRAMOPD]."</li>";
                    }
                    $indikator_prg .= "</ul>";
                }
                else {
                    $indikator_prg = $rprg_indikator[0][COL_NM_INDIKATORPROGRAMOPD];
                }
            }

            $nmSub = "";
            $eplandb->where(COL_KD_URUSAN, $prg[COL_KD_URUSAN]);
            $eplandb->where(COL_KD_BIDANG, $prg[COL_KD_BIDANG]);
            $eplandb->where(COL_KD_UNIT, $prg[COL_KD_UNIT]);
            $eplandb->where(COL_KD_SUB, $prg[COL_KD_SUB]);
            $subunit = $eplandb->get("ref_sub_unit")->row_array();
            if($subunit) {
                $nmSub = $subunit["Nm_Sub_Unit"];
            }

            $rkegiatan = $this->db
                ->where(COL_KD_URUSAN, $prg[COL_KD_URUSAN])
                ->where(COL_KD_BIDANG, $prg[COL_KD_BIDANG])
                ->where(COL_KD_UNIT, $prg[COL_KD_UNIT])
                ->where(COL_KD_SUB, $prg[COL_KD_SUB])
                ->where(COL_KD_BID, $prg[COL_KD_BID])

                ->where(COL_KD_TAHUN, $prg[COL_KD_TAHUN])
                ->where(COL_KD_PEMDA, $prg[COL_KD_PEMDA])
                ->where(COL_KD_MISI, $prg[COL_KD_MISI])
                ->where(COL_KD_TUJUAN, $prg[COL_KD_TUJUAN])
                ->where(COL_KD_INDIKATORTUJUAN, $prg[COL_KD_INDIKATORTUJUAN])
                ->where(COL_KD_SASARAN, $prg[COL_KD_SASARAN])
                ->where(COL_KD_INDIKATORSASARAN, $prg[COL_KD_INDIKATORSASARAN])
                ->where(COL_KD_TUJUANOPD, $prg[COL_KD_TUJUANOPD])
                ->where(COL_KD_INDIKATORTUJUANOPD, $prg[COL_KD_INDIKATORTUJUANOPD])
                ->where(COL_KD_SASARANOPD, $prg[COL_KD_SASARANOPD])
                ->where(COL_KD_INDIKATORSASARANOPD, $prg[COL_KD_INDIKATORSASARANOPD])
                ->where(COL_KD_PROGRAMOPD, $prg[COL_KD_PROGRAMOPD])
                ->order_by(TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_PROGRAMOPD, 'asc')
                ->get(TBL_SAKIP_MSUBBID_KEGIATAN)
                ->result_array();
            foreach($rkegiatan as $keg) {
                $prg["Total_Prg"] += $keg[COL_TOTAL];
            }
            ?>
            <tr style="font-weight: bold">
                <td style="text-align: center"><?=$counter_prg?></td>
                <td>
                    <table class="tbl-info">
                        <tr>
                            <td class="text-right" style="font-weight: normal">Tujuan</td>
                            <td style="font-weight: normal">:</td>
                            <td><?=$prg[COL_NM_TUJUANOPD]?></td>
                        </tr>
                        <tr>
                            <td class="text-right" style="font-weight: normal">Indikator Tujuan</td>
                            <td style="font-weight: normal">:</td>
                            <td><?=$prg[COL_NM_INDIKATORTUJUANOPD]?></td>
                        </tr>
                        <tr>
                            <td class="text-right" style="font-weight: normal">Sasaran</td>
                            <td style="font-weight: normal">:</td>
                            <td><?=$prg[COL_NM_SASARANOPD]?></td>
                        </tr>
                        <tr>
                            <td class="text-right" style="font-weight: normal">Indikator Sasaran</td>
                            <td style="font-weight: normal">:</td>
                            <td><?=$prg[COL_NM_INDIKATORSASARANOPD]?></td>
                        </tr>
                        <tr>
                            <td class="text-right" style="font-weight: normal">Program</td>
                            <td style="font-weight: normal">:</td>
                            <td><?=$prg[COL_NM_PROGRAMOPD]?></td>
                        </tr>
                    </table>
                </td>
                <td><?=$indikator_prg?></td>
                <td><?=$nmSub?></td>
                <td style="text-align: right"><?=/*number_format($prg[COL_TARGET], 2)*/$sasaran_prg?></td>
                <td style="text-align: right"><?=number_format($prg["Total_Prg"], 0)?></td>
                <td><?=$prg["KdSumberDanaProgram"]?></td>
                <td><?=$prg[COL_REMARKS]?></td>
                <td><?=$sasaran_prg_n1?></td>
                <td style="text-align: right"><?=number_format($rprg_n1[COL_TOTAL], 0)?></td>
            </tr>
            <?php
            $counter_keg = 1;
            foreach($rkegiatan as $keg) {
                $rkeg_sasaran = $this->db
                    ->where(COL_KD_URUSAN, $keg[COL_KD_URUSAN])
                    ->where(COL_KD_BIDANG, $keg[COL_KD_BIDANG])
                    ->where(COL_KD_UNIT, $keg[COL_KD_UNIT])
                    ->where(COL_KD_SUB, $keg[COL_KD_SUB])
                    ->where(COL_KD_BID, $keg[COL_KD_BID])
                    ->where(COL_KD_SUBBID, $keg[COL_KD_SUBBID])

                    ->where(COL_KD_PEMDA, $keg[COL_KD_PEMDA])
                    ->where(COL_KD_MISI, $keg[COL_KD_MISI])
                    ->where(COL_KD_TUJUAN, $keg[COL_KD_TUJUAN])
                    ->where(COL_KD_INDIKATORTUJUAN, $keg[COL_KD_INDIKATORTUJUAN])
                    ->where(COL_KD_SASARAN, $keg[COL_KD_SASARAN])
                    ->where(COL_KD_INDIKATORSASARAN, $keg[COL_KD_INDIKATORSASARAN])
                    ->where(COL_KD_TUJUANOPD, $keg[COL_KD_TUJUANOPD])
                    ->where(COL_KD_INDIKATORTUJUANOPD, $keg[COL_KD_INDIKATORTUJUANOPD])
                    ->where(COL_KD_SASARANOPD, $keg[COL_KD_SASARANOPD])
                    ->where(COL_KD_INDIKATORSASARANOPD, $keg[COL_KD_INDIKATORSASARANOPD])
                    ->where(COL_KD_PROGRAMOPD, $keg[COL_KD_PROGRAMOPD])
                    ->where(COL_KD_SASARANPROGRAMOPD, $keg[COL_KD_SASARANPROGRAMOPD])
                    ->where(COL_KD_KEGIATANOPD, $keg[COL_KD_KEGIATANOPD])
                    ->where(COL_KD_TAHUN, $keg[COL_KD_TAHUN])
                    ->order_by(TBL_SAKIP_MSUBBID_KEGIATAN_INDIKATOR.".".COL_KD_KEGIATANOPD, 'asc')
                    ->order_by(TBL_SAKIP_MSUBBID_KEGIATAN_INDIKATOR.".".COL_KD_SASARANKEGIATANOPD, 'asc')
                    ->get(TBL_SAKIP_MSUBBID_KEGIATAN_INDIKATOR)
                    ->result_array();

                $rkeg_n1 = $this->db
                    ->select_sum(COL_TOTAL)
                    ->where(COL_KD_URUSAN, $keg[COL_KD_URUSAN])
                    ->where(COL_KD_BIDANG, $keg[COL_KD_BIDANG])
                    ->where(COL_KD_UNIT, $keg[COL_KD_UNIT])
                    ->where(COL_KD_SUB, $keg[COL_KD_SUB])
                    ->where(COL_KD_BID, $keg[COL_KD_BID])
                    ->where(COL_KD_SUBBID, $keg[COL_KD_SUBBID])

                    ->where(COL_KD_PEMDA, $keg[COL_KD_PEMDA])
                    ->where(COL_KD_MISI, $keg[COL_KD_MISI])
                    ->where(COL_KD_TUJUAN, $keg[COL_KD_TUJUAN])
                    ->where(COL_KD_INDIKATORTUJUAN, $keg[COL_KD_INDIKATORTUJUAN])
                    ->where(COL_KD_SASARAN, $keg[COL_KD_SASARAN])
                    ->where(COL_KD_INDIKATORSASARAN, $keg[COL_KD_INDIKATORSASARAN])
                    ->where(COL_KD_TUJUANOPD, $keg[COL_KD_TUJUANOPD])
                    ->where(COL_KD_INDIKATORTUJUANOPD, $keg[COL_KD_INDIKATORTUJUANOPD])
                    ->where(COL_KD_SASARANOPD, $keg[COL_KD_SASARANOPD])
                    ->where(COL_KD_INDIKATORSASARANOPD, $keg[COL_KD_INDIKATORSASARANOPD])
                    ->where(COL_KD_PROGRAMOPD, $keg[COL_KD_PROGRAMOPD])
                    ->where(COL_KD_SASARANPROGRAMOPD, $keg[COL_KD_SASARANPROGRAMOPD])
                    ->where(COL_KD_KEGIATANOPD, $keg[COL_KD_KEGIATANOPD])
                    ->where(COL_KD_TAHUN, $keg[COL_KD_TAHUN]+1)
                    ->get(TBL_SAKIP_MSUBBID_KEGIATAN)
                    ->row_array();

                $rkeg_sasaran_n1 = $this->db
                    ->where(COL_KD_URUSAN, $keg[COL_KD_URUSAN])
                    ->where(COL_KD_BIDANG, $keg[COL_KD_BIDANG])
                    ->where(COL_KD_UNIT, $keg[COL_KD_UNIT])
                    ->where(COL_KD_SUB, $keg[COL_KD_SUB])
                    ->where(COL_KD_BID, $keg[COL_KD_BID])
                    ->where(COL_KD_SUBBID, $keg[COL_KD_SUBBID])

                    ->where(COL_KD_PEMDA, $keg[COL_KD_PEMDA])
                    ->where(COL_KD_MISI, $keg[COL_KD_MISI])
                    ->where(COL_KD_TUJUAN, $keg[COL_KD_TUJUAN])
                    ->where(COL_KD_INDIKATORTUJUAN, $keg[COL_KD_INDIKATORTUJUAN])
                    ->where(COL_KD_SASARAN, $keg[COL_KD_SASARAN])
                    ->where(COL_KD_INDIKATORSASARAN, $keg[COL_KD_INDIKATORSASARAN])
                    ->where(COL_KD_TUJUANOPD, $keg[COL_KD_TUJUANOPD])
                    ->where(COL_KD_INDIKATORTUJUANOPD, $keg[COL_KD_INDIKATORTUJUANOPD])
                    ->where(COL_KD_SASARANOPD, $keg[COL_KD_SASARANOPD])
                    ->where(COL_KD_INDIKATORSASARANOPD, $keg[COL_KD_INDIKATORSASARANOPD])
                    ->where(COL_KD_PROGRAMOPD, $keg[COL_KD_PROGRAMOPD])
                    ->where(COL_KD_SASARANPROGRAMOPD, $keg[COL_KD_SASARANPROGRAMOPD])
                    ->where(COL_KD_KEGIATANOPD, $keg[COL_KD_KEGIATANOPD])
                    ->where(COL_KD_TAHUN, $keg[COL_KD_TAHUN] + 1)
                    ->order_by(TBL_SAKIP_MSUBBID_KEGIATAN_INDIKATOR.".".COL_KD_KEGIATANOPD, 'asc')
                    ->order_by(TBL_SAKIP_MSUBBID_KEGIATAN_INDIKATOR.".".COL_KD_SASARANKEGIATANOPD, 'asc')
                    ->get(TBL_SAKIP_MSUBBID_KEGIATAN_INDIKATOR)
                    ->result_array();

                $rkeg_indikator = $this->db
                    ->where(COL_KD_URUSAN, $keg[COL_KD_URUSAN])
                    ->where(COL_KD_BIDANG, $keg[COL_KD_BIDANG])
                    ->where(COL_KD_UNIT, $keg[COL_KD_UNIT])
                    ->where(COL_KD_SUB, $keg[COL_KD_SUB])
                    ->where(COL_KD_BID, $keg[COL_KD_BID])
                    ->where(COL_KD_SUBBID, $keg[COL_KD_SUBBID])

                    ->where(COL_KD_PEMDA, $keg[COL_KD_PEMDA])
                    ->where(COL_KD_MISI, $keg[COL_KD_MISI])
                    ->where(COL_KD_TUJUAN, $keg[COL_KD_TUJUAN])
                    ->where(COL_KD_INDIKATORTUJUAN, $keg[COL_KD_INDIKATORTUJUAN])
                    ->where(COL_KD_SASARAN, $keg[COL_KD_SASARAN])
                    ->where(COL_KD_INDIKATORSASARAN, $keg[COL_KD_INDIKATORSASARAN])
                    ->where(COL_KD_TUJUANOPD, $keg[COL_KD_TUJUANOPD])
                    ->where(COL_KD_INDIKATORTUJUANOPD, $keg[COL_KD_INDIKATORTUJUANOPD])
                    ->where(COL_KD_SASARANOPD, $keg[COL_KD_SASARANOPD])
                    ->where(COL_KD_INDIKATORSASARANOPD, $keg[COL_KD_INDIKATORSASARANOPD])
                    ->where(COL_KD_PROGRAMOPD, $keg[COL_KD_PROGRAMOPD])
                    ->where(COL_KD_SASARANPROGRAMOPD, $keg[COL_KD_SASARANPROGRAMOPD])
                    ->where(COL_KD_KEGIATANOPD, $keg[COL_KD_KEGIATANOPD])
                    ->where(COL_KD_TAHUN, $keg[COL_KD_TAHUN])
                    ->order_by(TBL_SAKIP_MSUBBID_KEGIATAN_INDIKATOR.".".COL_KD_KEGIATANOPD, 'asc')
                    ->order_by(TBL_SAKIP_MSUBBID_KEGIATAN_INDIKATOR.".".COL_KD_SASARANKEGIATANOPD, 'asc')
                    ->order_by(TBL_SAKIP_MSUBBID_KEGIATAN_INDIKATOR.".".COL_KD_INDIKATORKEGIATANOPD, 'asc')
                    ->get(TBL_SAKIP_MSUBBID_KEGIATAN_INDIKATOR)
                    ->result_array();

                $sasaran_keg = "";
                if(count($rkeg_sasaran) > 0) {
                    if(count($rkeg_sasaran) > 1) {
                        $sasaran_keg = "<ul style='margin-left: 0px; padding-left: 15px; text-align: left'>";
                        foreach($rkeg_sasaran as $s) {
                            $sasaran_keg .= "<li style='white-space: nowrap'>".number_format($s[COL_TARGET], 2)." ".$s[COL_KD_SATUAN]."</li>";
                        }
                        $sasaran_keg .= "</ul>";
                    }
                    else {
                        $sasaran_keg = number_format($rkeg_sasaran[0][COL_TARGET], 2)." ".$rkeg_sasaran[0][COL_KD_SATUAN];
                    }
                }

                $sasaran_keg_n1 = "";
                if(count($rkeg_sasaran_n1) > 0) {
                    if(count($rkeg_sasaran_n1) > 1) {
                        $sasaran_keg_n1 = "<ul style='margin-left: 0px; padding-left: 15px; text-align: left'>";
                        foreach($rkeg_sasaran_n1 as $s) {
                            $sasaran_keg_n1 .= "<li style='white-space: nowrap'>".number_format($s[COL_TARGET], 2)." ".$s[COL_KD_SATUAN]."</li>";
                        }
                        $sasaran_keg_n1 .= "</ul>";
                    }
                    else {
                        $sasaran_keg_n1 = number_format($rkeg_sasaran_n1[0][COL_TARGET], 2)." ".$rkeg_sasaran_n1[0][COL_KD_SATUAN];
                    }
                }

                $indikator_keg = "";
                if(count($rkeg_indikator) > 0) {
                    if(count($rkeg_indikator) > 1) {
                        $indikator_keg = "<ul style='margin-left: 0px; padding-left: 15px; text-align: left'>";
                        foreach($rkeg_indikator as $s) {
                            $indikator_keg .= "<li>".$s[COL_NM_INDIKATORKEGIATANOPD]."</li>";
                        }
                        $indikator_keg .= "</ul>";
                    }
                    else {
                        $indikator_keg = $rkeg_indikator[0][COL_NM_INDIKATORKEGIATANOPD];
                    }
                }

                $nmKeg = $keg[COL_NM_KEGIATANOPD];
                if(empty($nmKeg) && $keg[COL_ISEPLAN]) {
                    $eplandb->where(COL_KD_URUSAN, $keg[COL_KD_URUSAN]);
                    $eplandb->where(COL_KD_BIDANG, $keg[COL_KD_BIDANG]);
                    $eplandb->where(COL_KD_UNIT, $keg[COL_KD_UNIT]);
                    $eplandb->where(COL_KD_SUB, $keg[COL_KD_SUB]);
                    $eplandb->where("Tahun", $keg[COL_KD_TAHUN]-1);
                    $eplandb->where("Kd_Prog", $keg[COL_KD_PROGRAMOPD]);
                    $eplandb->where("Kd_Keg", $keg[COL_KD_KEGIATANOPD]);
                    $keg_ = $eplandb->get("ta_kegiatan")->row_array();
                    if($keg_) {
                        $nmKeg = $keg_["Ket_Kegiatan"];
                    }
                } else {
                    $nmKeg = $keg[COL_NM_KEGIATANOPD];
                }
                ?>
                <tr>
                    <td style="text-align: center"><?=$counter_keg?></td>
                    <td><?=$nmKeg?></td>
                    <td><?=$indikator_keg?></td>
                    <td><?=$nmSub?></td>
                    <td><?=$sasaran_keg?></td>
                    <td style="text-align: right"><?=number_format($keg[COL_TOTAL], 0)?></td>
                    <td><?=$keg[COL_KD_SUMBERDANA]?></td>
                    <td><?=$keg[COL_REMARKS]?></td>
                    <td><?=$sasaran_keg_n1?></td>
                    <td style="text-align: right"><?=number_format($rkeg_n1[COL_TOTAL], 0)?></td>
                </tr>
                <?php
                $counter_keg++;
            }
            ?>
            <?php
            $counter_prg++;
        }
        ?>
        </tbody>
    </table>
</div>