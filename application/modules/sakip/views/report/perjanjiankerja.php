<?php
/**
 * Created by PhpStorm.
 * User: Partopi Tao
 * Date: 3/3/2019
 * Time: 10:31 AM
 */
$this->load->view('header');
$ruser = GetLoggedUser();
$eplandb = $this->load->database("eplan", true);
?>
    <section class="content-header">
        <h1> <?= $title ?> <small> Generate</small></h1>
        <ol class="breadcrumb">
            <li><a href="<?=site_url()?>"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Perjanjian Kinerja</li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-sm-12">
                <div class="box box-primary" style="border-top-color: transparent">
                    <div class="box-body">
                        <?=form_open(current_url(),array('role'=>'form','id'=>'main-form','class'=>'form-horizontal', 'method'=>'get','target'=>'_blank'))?>
                        <div class="form-group">
                            <label class="control-label col-sm-2">OPD</label>
                            <div class="col-sm-8">
                                <div class="input-group">
                                    <input type="text" class="form-control" name="text-opd" value="<?= $edit ? $data[COL_KD_URUSAN].".".$data[COL_KD_BIDANG].".".$data[COL_KD_UNIT].".".$data[COL_KD_SUB]." ".$nmSub : ($ruser[COL_ROLEID] == ROLEKADIS ? $strOPD[0].".".$strOPD[1].".".$strOPD[2].".".$strOPD[3]." ".$nmSub : "")?>" disabled>
                                    <input type="hidden" name="<?=COL_KD_URUSAN?>" value="<?= $edit ? $data[COL_KD_URUSAN] : ($ruser[COL_ROLEID]==ROLEKADIS?$strOPD[0]:"")?>" required   >
                                    <input type="hidden" name="<?=COL_KD_BIDANG?>" value="<?= $edit ? $data[COL_KD_BIDANG] : ($ruser[COL_ROLEID]==ROLEKADIS?$strOPD[1]:"")?>" required   >
                                    <input type="hidden" name="<?=COL_KD_UNIT?>" value="<?= $edit ? $data[COL_KD_UNIT] : ($ruser[COL_ROLEID]==ROLEKADIS?$strOPD[2]:"")?>" required   >
                                    <input type="hidden" name="<?=COL_KD_SUB?>" value="<?= $edit ? $data[COL_KD_SUB] : ($ruser[COL_ROLEID]==ROLEKADIS?$strOPD[3]:"")?>" required   >
                                    <div class="input-group-btn">
                                        <button type="button" class="btn btn-default btn-flat btn-browse-opd" data-toggle="modal" data-target="#browseOPD" data-toggle="tooltip" data-placement="top" title="Pilih OPD" <?=($ruser[COL_ROLEID] == ROLEKADIS ? "disabled" : "")?>><i class="fa fa-ellipsis-h"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2">Bidang</label>
                            <div class="col-sm-8">
                                <div class="input-group">
                                    <?php
                                    $nmBid = "";
                                    if($ruser[COL_ROLEID] == ROLEKABID || $ruser[COL_ROLEID] == ROLEKASUBBID) {
                                        $this->db->where(COL_KD_URUSAN, $strOPD[0]);
                                        $this->db->where(COL_KD_BIDANG, $strOPD[1]);
                                        $this->db->where(COL_KD_UNIT, $strOPD[2]);
                                        $this->db->where(COL_KD_SUB, $strOPD[3]);
                                        $this->db->where(COL_KD_BID, $strOPD[4]);
                                        $bid = $this->db->get(TBL_SAKIP_MBID)->row_array();
                                        if($bid) {
                                            $nmBid = $bid[COL_NM_BID];
                                        }
                                    }
                                    ?>
                                    <input type="text" class="form-control" name="text-bid" value="<?= $edit ? $data[COL_KD_BID].". ".$data[COL_NM_BID] : ($ruser[COL_ROLEID]==ROLEKABID || $ruser[COL_ROLEID] == ROLEKASUBBID?$strOPD[4].". ".$nmBid:"")?>" readonly disabled>
                                    <input type="hidden" name="<?=COL_KD_BID?>" value="<?= $edit ? $data[COL_KD_BID] : ($ruser[COL_ROLEID]==ROLEKABID || $ruser[COL_ROLEID] == ROLEKASUBBID?$strOPD[4]:"")?>" required   >
                                    <div class="input-group-btn">
                                        <button type="button" class="btn btn-default btn-flat btn-remove-val" data-toggle="tooltip" data-placement="top" title="Kosongkan"><i class="fa fa-times"></i></button>
                                        <button type="button" class="btn btn-default btn-flat btn-browse-bid" data-toggle="modal" data-target="#browseBid" data-toggle="tooltip" data-placement="top" title="Pilih Bidang" <?=$edit?"disabled":($ruser[COL_ROLEID]==ROLEKABID || $ruser[COL_ROLEID] == ROLEKASUBBID?"disabled":"")?>><i class="fa fa-ellipsis-h"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2">Sub Bidang</label>
                            <div class="col-sm-8">
                                <div class="input-group">
                                    <?php
                                    $nmSubBid = "";
                                    if($ruser[COL_ROLEID] == ROLEKASUBBID) {
                                        $this->db->where(COL_KD_URUSAN, $strOPD[0]);
                                        $this->db->where(COL_KD_BIDANG, $strOPD[1]);
                                        $this->db->where(COL_KD_UNIT, $strOPD[2]);
                                        $this->db->where(COL_KD_SUB, $strOPD[3]);
                                        $this->db->where(COL_KD_BID, $strOPD[4]);
                                        $this->db->where(COL_KD_SUBBID, $strOPD[4]);
                                        $sbid = $this->db->get(TBL_SAKIP_MSUBBID)->row_array();
                                        if($sbid) {
                                            $nmSubBid = $sbid[COL_NM_SUBBID];
                                        }
                                    }
                                    ?>
                                    <input type="text" class="form-control" name="text-subbid" value="<?= $edit ? $data[COL_KD_BID].".".$data[COL_KD_SUBBID].". ".$data[COL_NM_SUBBID] : (($ruser[COL_ROLEID]==ROLEKASUBBID)?$strOPD[4].".".$strOPD[5].". ".$nmSubBid:"")?>" readonly disabled>
                                    <input type="hidden" name="<?=COL_KD_SUBBID?>" value="<?= $edit ? $data[COL_KD_SUBBID] : (($ruser[COL_ROLEID]==ROLEKASUBBID)?$strOPD[5]:"")?>" required   >
                                    <div class="input-group-btn">
                                        <button type="button" class="btn btn-default btn-flat btn-remove-val" data-toggle="tooltip" data-placement="top" title="Kosongkan"><i class="fa fa-times"></i></button>
                                        <button type="button" class="btn btn-default btn-flat btn-browse-subbid" data-toggle="modal" data-target="#browseSubBid" data-toggle="tooltip" data-placement="top" title="Pilih Sub Bidang" <?=$edit?"disabled":($ruser[COL_ROLEID]==ROLEKASUBBID?"disabled":"")?>><i class="fa fa-ellipsis-h"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2">Jabatan Individu</label>
                            <div class="col-sm-8">
                                <div class="input-group">
                                    <input type="text" class="form-control" name="text-individu" value="<?= $edit ? $data[COL_KD_BID].".".$data[COL_KD_SUBBID].". ".$data[COL_NM_SUBBID].". ".$data[COL_KD_SASARANINDIVIDU] : ""?>" readonly disabled>
                                    <input type="hidden" name="<?=COL_KD_SASARANINDIVIDU?>" value="<?= $edit ? $data[COL_KD_SASARANINDIVIDU] : ""?>" required   >
                                    <div class="input-group-btn">
                                        <button type="button" class="btn btn-default btn-flat btn-remove-val" data-toggle="tooltip" data-placement="top" title="Kosongkan"><i class="fa fa-times"></i></button>
                                        <button type="button" class="btn btn-default btn-flat btn-browse-individu" data-toggle="modal" data-target="#browseIndividu" data-toggle="tooltip" data-placement="top" title="Pilih Jabatan Individu" <?=$edit?"disabled":""?>><i class="fa fa-ellipsis-h"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group" style="display: none">
                            <label class="control-label col-sm-2">Periode</label>
                            <div class="col-sm-8">
                                <div class="input-group">
                                    <input type="text" class="form-control" name="text-periode" value="<?= $edit ? $data[COL_KD_TAHUN_FROM]." s.d ".$data[COL_KD_TAHUN_TO]." : ".$data[COL_NM_PEJABAT] : ($rpemda ? $rpemda[COL_KD_TAHUN_FROM]." s.d ".$rpemda[COL_KD_TAHUN_TO]." : ".$rpemda[COL_NM_PEJABAT] : "")?>" disabled>
                                    <input type="hidden" name="<?=COL_KD_PEMDA?>" value="<?= $edit ? $data[COL_KD_PEMDA] : ($rpemda ? $rpemda[COL_KD_PEMDA] : "")?>" required />
                                    <div class="input-group-btn">
                                        <button type="button" class="btn btn-default btn-flat btn-browse-period" data-toggle="modal" data-target="#browsePeriod" data-toggle="tooltip" data-placement="top" title="Pilih Periode" ><i class="fa fa-ellipsis-h"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2">Tahun</label>
                            <div class="col-sm-2">
                                <input type="number" class="form-control" name="<?=COL_KD_TAHUN?>" value="<?= $edit ? $data[COL_KD_TAHUN] : date("Y")?>" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2"></label>
                            <div class="col-sm-2">
                              <input type="checkbox" name="IsPergeseran" id="IsPergeseran" />&nbsp;
                              <label for="IsPergeseran">Setelah Pergeseran</label>
                            </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-sm-2">Sumber Data</label>
                          <div class="col-sm-2">
                            <select class="form-control" name="SumberData">
                              <option value="INDUK" <?=!empty($SumberData)&&$SumberData=='INDUK'?'selected':''?>>INDUK</option>
                              <option value="PERUBAHAN" <?=!empty($SumberData)&&$SumberData=='PERUBAHAN'?'selected':''?>>PERUBAHAN</option>
                            </select>
                          </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-12" style="text-align: right">
                                <button type="submit" class="btn btn-primary btn-flat" title="Lihat"><i class="fa fa-arrow-circle-right"></i> Lihat</button>
                            </div>
                        </div>
                        <?=form_close()?>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <div class="modal fade" id="browseOPD" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Browse</h4>
                </div>
                <div class="modal-body">
                    ...
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default btn-flat pull-right" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="browsePeriod" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Browse</h4>
                </div>
                <div class="modal-body">
                    ...
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default btn-flat pull-right" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="browseBid" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Browse</h4>
                </div>
                <div class="modal-body">
                    ...
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default btn-flat pull-right" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="browseSubBid" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Browse</h4>
                </div>
                <div class="modal-body">
                    ...
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default btn-flat pull-right" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="browseIndividu" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Browse</h4>
                </div>
                <div class="modal-body">
                    ...
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default btn-flat pull-right" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
<?php $this->load->view('loadjs') ?>
    <script>
        $(document).ready(function() {
            $('.modal').on('hidden.bs.modal', function (event) {
                $(this).find(".modal-body").empty();
            });

            $('#browseOPD').on('show.bs.modal', function (event) {
                var modalBody = $(".modal-body", $("#browseOPD"));
                $(this).removeData('bs.modal');
                modalBody.html("<p style='font-style: italic'>Loading..</p>");
                modalBody.load("<?=site_url("ajax/browse-opd")?>", function () {
                    $("[name=selID][type=hidden]", modalBody).unbind().change(function () {
                        var kdSub = $(this).val().split('|');
                        $("[name=Kd_Urusan]").val(kdSub[0]);
                        $("[name=Kd_Bidang]").val(kdSub[1]);
                        $("[name=Kd_Unit]").val(kdSub[2]);
                        $("[name=Kd_Sub]").val(kdSub[3]);
                    });
                    $("[name=selText][type=hidden]", modalBody).unbind().change(function () {
                        $("[name=text-opd]").val($(this).val());
                    });
                });
            });

            $('#browseBid').on('show.bs.modal', function (event) {
                var modalBody = $(".modal-body", $("#browseBid"));
                $(this).removeData('bs.modal');
                var kdUrusan = $("[name=Kd_Urusan]").val();
                var kdBidang = $("[name=Kd_Bidang]").val();
                var kdUnit = $("[name=Kd_Unit]").val();
                var kdSub = $("[name=Kd_Sub]").val();

                if(!kdUrusan || !kdBidang || !kdUnit || !kdSub) {
                    modalBody.html("<p style='font-style: italic'>Silakan pilih OPD terlebih dahulu!</p>");
                    return;
                }

                modalBody.html("<p style='font-style: italic'>Loading..</p>");
                modalBody.load("<?=site_url("ajax/browse-bid")?>"+"?Kd_Urusan="+kdUrusan+"&Kd_Bidang="+kdBidang+"&Kd_Unit="+kdUnit+"&Kd_Sub="+kdSub, function () {
                    $("[name=selID][type=hidden]", modalBody).unbind().change(function () {
                        $("[name=Kd_Bid]").val($(this).val());
                    });
                    $("[name=selText][type=hidden]", modalBody).unbind().change(function () {
                        $("[name=text-bid]").val($(this).val());
                    });
                });
            });

            $('#browseSubBid').on('show.bs.modal', function (event) {
                var modalBody = $(".modal-body", $("#browseSubBid"));
                $(this).removeData('bs.modal');

                var kdUrusan = $("[name=Kd_Urusan]").val();
                var kdBidang = $("[name=Kd_Bidang]").val();
                var kdUnit = $("[name=Kd_Unit]").val();
                var kdSub = $("[name=Kd_Sub]").val();
                var kdBid = $("[name=Kd_Bid]").val();

                if(!kdUrusan || !kdBidang || !kdUnit || !kdSub || !kdBid) {
                    modalBody.html("<p style='font-style: italic'>Silakan pilih Bidang OPD terlebih dahulu!</p>");
                    return;
                }

                modalBody.html("<p style='font-style: italic'>Loading..</p>");
                modalBody.load("<?=site_url("ajax/browse-subbid")?>"+"?Kd_Urusan="+kdUrusan+"&Kd_Bidang="+kdBidang+"&Kd_Unit="+kdUnit+"&Kd_Sub="+kdSub+"&Kd_Bid="+kdBid, function () {
                    $("[name=selID][type=hidden]", modalBody).unbind().change(function () {
                        $("[name=Kd_Subbid]").val($(this).val());
                    });
                    $("[name=selText][type=hidden]", modalBody).unbind().change(function () {
                        $("[name=text-subbid]").val($(this).val());
                    });
                });
            });

            $('#browseIndividu').on('show.bs.modal', function (event) {
                var modalBody = $(".modal-body", $("#browseIndividu"));
                $(this).removeData('bs.modal');

                var kdUrusan = $("[name=Kd_Urusan]").val();
                var kdBidang = $("[name=Kd_Bidang]").val();
                var kdUnit = $("[name=Kd_Unit]").val();
                var kdSub = $("[name=Kd_Sub]").val();
                var kdBid = $("[name=Kd_Bid]").val();
                var kdSubbid = $("[name=Kd_Subbid]").val();

                if(!kdUrusan || !kdBidang || !kdUnit || !kdSub || !kdBid || !kdSubbid) {
                    modalBody.html("<p style='font-style: italic'>Silakan pilih Sub Bidang OPD terlebih dahulu!</p>");
                    return;
                }

                modalBody.html("<p style='font-style: italic'>Loading..</p>");
                modalBody.load("<?=site_url("ajax/browse-individu")?>"+"?Kd_Urusan="+kdUrusan+"&Kd_Bidang="+kdBidang+"&Kd_Unit="+kdUnit+"&Kd_Sub="+kdSub+"&Kd_Bid="+kdBid+"&Kd_Subbid="+kdSubbid, function () {
                    $("[name=selID][type=hidden]", modalBody).unbind().change(function () {
                        $("[name=Kd_SasaranIndividu]").val($(this).val());
                    });
                    $("[name=selText][type=hidden]", modalBody).unbind().change(function () {
                        $("[name=text-individu]").val($(this).val());
                    });
                });
            });

            $('#browsePeriod').on('show.bs.modal', function (event) {
                var modalBody = $(".modal-body", $("#browsePeriod"));
                $(this).removeData('bs.modal');
                modalBody.html("<p style='font-style: italic'>Loading..</p>");
                modalBody.load("<?=site_url("ajax/browse-pemda")?>", function () {
                    $("[name=selID][type=hidden]", modalBody).unbind().change(function () {
                        $("[name=Kd_Pemda]").val($(this).val());
                    });
                    $("[name=selText][type=hidden]", modalBody).unbind().change(function () {
                        $("[name=text-periode]").val($(this).val());
                    });
                });
            });

            $(".btn-remove-val").click(function() {
                var parent = $(this).closest(".form-group");
                $("input", parent).val("");
            });
        });
    </script>
<?php $this->load->view('footer') ?>
