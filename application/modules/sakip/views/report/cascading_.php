<?php
/**
 * Created by PhpStorm.
 * User: Partopi Tao
 * Date: 3/17/2019
 * Time: 8:58 PM
 */
$arrMisi = array();
$eplandb = $this->load->database("eplan", true);
$arrTujuan = array();
foreach($rtujuan as $t) {
    $arrSasaran = array();
    $indikator = $this->db
        ->where(COL_KD_URUSAN, $t[COL_KD_URUSAN])
        ->where(COL_KD_BIDANG, $t[COL_KD_BIDANG])
        ->where(COL_KD_UNIT, $t[COL_KD_UNIT])
        ->where(COL_KD_SUB, $t[COL_KD_SUB])

        ->where(COL_KD_PEMDA, $t[COL_KD_PEMDA])
        //->where(COL_KD_MISI, $t[COL_KD_MISI])
        //->where(COL_KD_TUJUAN, $t[COL_KD_TUJUAN])
        //->where(COL_KD_INDIKATORTUJUAN, $t[COL_KD_INDIKATORTUJUAN])
        //->where(COL_KD_SASARAN, $t[COL_KD_SASARAN])
        //->where(COL_KD_INDIKATORSASARAN, $t[COL_KD_INDIKATORSASARAN])
        ->where(COL_KD_TUJUANOPD, $t[COL_KD_TUJUANOPD])
        ->order_by(TBL_SAKIP_MOPD_IKTUJUAN.".".COL_KD_INDIKATORTUJUANOPD, 'asc')
        ->group_by(TBL_SAKIP_MOPD_IKTUJUAN.".".COL_KD_INDIKATORTUJUANOPD)
        ->get(TBL_SAKIP_MOPD_IKTUJUAN)
        ->result_array();

    $sasaran = $this->db
        ->where(COL_KD_URUSAN, $t[COL_KD_URUSAN])
        ->where(COL_KD_BIDANG, $t[COL_KD_BIDANG])
        ->where(COL_KD_UNIT, $t[COL_KD_UNIT])
        ->where(COL_KD_SUB, $t[COL_KD_SUB])

        //->where(COL_KD_PEMDA, $t[COL_KD_PEMDA])
        //->where(COL_KD_MISI, $t[COL_KD_MISI])
        //->where(COL_KD_TUJUAN, $t[COL_KD_TUJUAN])
        //->where(COL_KD_INDIKATORTUJUAN, $t[COL_KD_INDIKATORTUJUAN])
        //->where(COL_KD_SASARAN, $t[COL_KD_SASARAN])
        //->where(COL_KD_INDIKATORSASARAN, $t[COL_KD_INDIKATORSASARAN])
        ->where(COL_KD_TUJUANOPD, $t[COL_KD_TUJUANOPD])
        ->order_by(TBL_SAKIP_MOPD_SASARAN.".".COL_KD_SASARANOPD, 'asc')
        ->get(TBL_SAKIP_MOPD_SASARAN)
        ->result_array();

    foreach($sasaran as $s) {
        $arrProgramOPD = array();

        $iksasaran = $this->db
            ->where(COL_KD_URUSAN, $s[COL_KD_URUSAN])
            ->where(COL_KD_BIDANG, $s[COL_KD_BIDANG])
            ->where(COL_KD_UNIT, $s[COL_KD_UNIT])
            ->where(COL_KD_SUB, $s[COL_KD_SUB])

            ->where(COL_KD_PEMDA, $s[COL_KD_PEMDA])
            ->where(COL_KD_MISI, $s[COL_KD_MISI])
            ->where(COL_KD_TUJUAN, $s[COL_KD_TUJUAN])
            ->where(COL_KD_INDIKATORTUJUAN, $s[COL_KD_INDIKATORTUJUAN])
            ->where(COL_KD_SASARAN, $s[COL_KD_SASARAN])
            ->where(COL_KD_INDIKATORSASARAN, $s[COL_KD_INDIKATORSASARAN])
            ->where(COL_KD_TUJUANOPD, $s[COL_KD_TUJUANOPD])
            ->where(COL_KD_INDIKATORTUJUANOPD, $s[COL_KD_INDIKATORTUJUANOPD])
            ->where(COL_KD_SASARANOPD, $s[COL_KD_SASARANOPD])
            ->order_by(TBL_SAKIP_MOPD_IKSASARAN.".".COL_KD_INDIKATORSASARANOPD, 'asc')
            ->get(TBL_SAKIP_MOPD_IKSASARAN)
            ->result_array();

        $arrProgram = array();
        $qselect = @"*, (SELECT SUM(COALESCE(sakip_dpa_kegiatan.Pergeseran, sakip_dpa_kegiatan.Budget)) FROM `sakip_dpa_kegiatan`
                    WHERE `sakip_dpa_kegiatan`.`Kd_Urusan` = `sakip_dpa_program`.`Kd_Urusan`
                    AND `sakip_dpa_kegiatan`.`Kd_Bidang` = `sakip_dpa_program`.`Kd_Bidang`
                    AND `sakip_dpa_kegiatan`.`Kd_Unit` = `sakip_dpa_program`.`Kd_Unit`
                    AND `sakip_dpa_kegiatan`.`Kd_Sub` = `sakip_dpa_program`.`Kd_Sub`
                    AND `sakip_dpa_kegiatan`.`Kd_Bid` = `sakip_dpa_program`.`Kd_Bid`
                    AND `sakip_dpa_kegiatan`.`Kd_Pemda` = `sakip_dpa_program`.`Kd_Pemda`
                    AND `sakip_dpa_kegiatan`.`Kd_Misi` = `sakip_dpa_program`.`Kd_Misi`
                    AND `sakip_dpa_kegiatan`.`Kd_Tujuan` = `sakip_dpa_program`.`Kd_Tujuan`
                    AND `sakip_dpa_kegiatan`.`Kd_IndikatorTujuan` = `sakip_dpa_program`.`Kd_IndikatorTujuan`
                    AND `sakip_dpa_kegiatan`.`Kd_Sasaran` = `sakip_dpa_program`.`Kd_Sasaran`
                    AND `sakip_dpa_kegiatan`.`Kd_IndikatorSasaran` = `sakip_dpa_program`.`Kd_IndikatorSasaran`
                    AND `sakip_dpa_kegiatan`.`Kd_TujuanOPD` = `sakip_dpa_program`.`Kd_TujuanOPD`
                    AND `sakip_dpa_kegiatan`.`Kd_IndikatorTujuanOPD` = `sakip_dpa_program`.`Kd_IndikatorTujuanOPD`
                    AND `sakip_dpa_kegiatan`.`Kd_SasaranOPD` = `sakip_dpa_program`.`Kd_SasaranOPD`
                    AND `sakip_dpa_kegiatan`.`Kd_IndikatorSasaranOPD` = `sakip_dpa_program`.`Kd_IndikatorSasaranOPD`
                    AND `sakip_dpa_kegiatan`.`Kd_ProgramOPD` = `sakip_dpa_program`.`Kd_ProgramOPD`
                    AND `sakip_dpa_kegiatan`.`Kd_Tahun` = `sakip_dpa_program`.`Kd_Tahun`
                ) AS TotalProgram";
        if(!empty($SumberData) && $SumberData == 'PERUBAHAN') {
          $qselect = @"*, (SELECT SUM(COALESCE(null, sakip_pdpa_kegiatan.Budget)) FROM `sakip_pdpa_kegiatan`
                      WHERE `sakip_pdpa_kegiatan`.`Kd_Urusan` = `sakip_pdpa_program`.`Kd_Urusan`
                      AND `sakip_pdpa_kegiatan`.`Kd_Bidang` = `sakip_pdpa_program`.`Kd_Bidang`
                      AND `sakip_pdpa_kegiatan`.`Kd_Unit` = `sakip_pdpa_program`.`Kd_Unit`
                      AND `sakip_pdpa_kegiatan`.`Kd_Sub` = `sakip_pdpa_program`.`Kd_Sub`
                      AND `sakip_pdpa_kegiatan`.`Kd_Bid` = `sakip_pdpa_program`.`Kd_Bid`
                      AND `sakip_pdpa_kegiatan`.`Kd_Pemda` = `sakip_pdpa_program`.`Kd_Pemda`
                      AND `sakip_pdpa_kegiatan`.`Kd_Misi` = `sakip_pdpa_program`.`Kd_Misi`
                      AND `sakip_pdpa_kegiatan`.`Kd_Tujuan` = `sakip_pdpa_program`.`Kd_Tujuan`
                      AND `sakip_pdpa_kegiatan`.`Kd_IndikatorTujuan` = `sakip_pdpa_program`.`Kd_IndikatorTujuan`
                      AND `sakip_pdpa_kegiatan`.`Kd_Sasaran` = `sakip_pdpa_program`.`Kd_Sasaran`
                      AND `sakip_pdpa_kegiatan`.`Kd_IndikatorSasaran` = `sakip_pdpa_program`.`Kd_IndikatorSasaran`
                      AND `sakip_pdpa_kegiatan`.`Kd_TujuanOPD` = `sakip_pdpa_program`.`Kd_TujuanOPD`
                      AND `sakip_pdpa_kegiatan`.`Kd_IndikatorTujuanOPD` = `sakip_pdpa_program`.`Kd_IndikatorTujuanOPD`
                      AND `sakip_pdpa_kegiatan`.`Kd_SasaranOPD` = `sakip_pdpa_program`.`Kd_SasaranOPD`
                      AND `sakip_pdpa_kegiatan`.`Kd_IndikatorSasaranOPD` = `sakip_pdpa_program`.`Kd_IndikatorSasaranOPD`
                      AND `sakip_pdpa_kegiatan`.`Kd_ProgramOPD` = `sakip_pdpa_program`.`Kd_ProgramOPD`
                      AND `sakip_pdpa_kegiatan`.`Kd_Tahun` = `sakip_pdpa_program`.`Kd_Tahun`
                  ) AS TotalProgram";
        }
        $program = $this->db
            ->select($qselect)
            ->where(COL_KD_URUSAN, $s[COL_KD_URUSAN])
            ->where(COL_KD_BIDANG, $s[COL_KD_BIDANG])
            ->where(COL_KD_UNIT, $s[COL_KD_UNIT])
            ->where(COL_KD_SUB, $s[COL_KD_SUB])

            ->where(COL_KD_PEMDA, $s[COL_KD_PEMDA])
            ->where(COL_KD_MISI, $s[COL_KD_MISI])
            ->where(COL_KD_TUJUAN, $s[COL_KD_TUJUAN])
            ->where(COL_KD_INDIKATORTUJUAN, $s[COL_KD_INDIKATORTUJUAN])
            ->where(COL_KD_SASARAN, $s[COL_KD_SASARAN])
            ->where(COL_KD_INDIKATORSASARAN, $s[COL_KD_INDIKATORSASARAN])
            ->where(COL_KD_TUJUANOPD, $s[COL_KD_TUJUANOPD])
            ->where(COL_KD_INDIKATORTUJUANOPD, $s[COL_KD_INDIKATORTUJUANOPD])
            ->where(COL_KD_SASARANOPD, $s[COL_KD_SASARANOPD])
            //->where(COL_KD_INDIKATORSASARANOPD, $s[COL_KD_INDIKATORSASARANOPD])
            ->where(COL_KD_TAHUN, $this->input->get(COL_KD_TAHUN))
            ->order_by(COL_KD_INDIKATORSASARANOPD, 'asc')
            ->order_by(COL_KD_PROGRAMOPD, 'asc')
            ->get((!empty($SumberData)&&$SumberData=='PERUBAHAN'?TBL_SAKIP_PDPA_PROGRAM:TBL_SAKIP_DPA_PROGRAM))
            ->result_array();

        foreach($program as $p) {
          $arrKegiatanOPD = array();
          $indProgramOPD = $this->db
          ->where(COL_KD_URUSAN, $p[COL_KD_URUSAN])
          ->where(COL_KD_BIDANG, $p[COL_KD_BIDANG])
          ->where(COL_KD_UNIT, $p[COL_KD_UNIT])
          ->where(COL_KD_SUB, $p[COL_KD_SUB])
          ->where(COL_KD_BID, $p[COL_KD_BID])

          ->where(COL_KD_PEMDA, $p[COL_KD_PEMDA])
          ->where(COL_KD_MISI, $p[COL_KD_MISI])
          ->where(COL_KD_TUJUAN, $p[COL_KD_TUJUAN])
          ->where(COL_KD_INDIKATORTUJUAN, $p[COL_KD_INDIKATORTUJUAN])
          ->where(COL_KD_SASARAN, $p[COL_KD_SASARAN])
          ->where(COL_KD_INDIKATORSASARAN, $p[COL_KD_INDIKATORSASARAN])
          ->where(COL_KD_TUJUANOPD, $p[COL_KD_TUJUANOPD])
          ->where(COL_KD_INDIKATORTUJUANOPD, $s[COL_KD_INDIKATORTUJUANOPD])
          ->where(COL_KD_SASARANOPD, $p[COL_KD_SASARANOPD])
          ->where(COL_KD_INDIKATORSASARANOPD, $p[COL_KD_INDIKATORSASARANOPD])
          ->where(COL_KD_TAHUN, $p[COL_KD_TAHUN])
          ->where(COL_KD_PROGRAMOPD, $p[COL_KD_PROGRAMOPD])
          ->order_by(COL_KD_SASARANPROGRAMOPD, 'asc')
          ->group_by(array(COL_KD_SASARANPROGRAMOPD, COL_KD_INDIKATORPROGRAMOPD))
          ->get((!empty($SumberData)&&$SumberData=='PERUBAHAN'?TBL_SAKIP_PDPA_PROGRAM_INDIKATOR:TBL_SAKIP_DPA_PROGRAM_INDIKATOR))
          ->result_array();

          $kegiatan = $this->db
          ->where(COL_KD_URUSAN, $p[COL_KD_URUSAN])
          ->where(COL_KD_BIDANG, $p[COL_KD_BIDANG])
          ->where(COL_KD_UNIT, $p[COL_KD_UNIT])
          ->where(COL_KD_SUB, $p[COL_KD_SUB])
          ->where(COL_KD_BID, $p[COL_KD_BID])

          ->where(COL_KD_PEMDA, $p[COL_KD_PEMDA])
          ->where(COL_KD_MISI, $p[COL_KD_MISI])
          ->where(COL_KD_TUJUAN, $p[COL_KD_TUJUAN])
          ->where(COL_KD_INDIKATORTUJUAN, $p[COL_KD_INDIKATORTUJUAN])
          ->where(COL_KD_SASARAN, $p[COL_KD_SASARAN])
          ->where(COL_KD_INDIKATORSASARAN, $p[COL_KD_INDIKATORSASARAN])
          ->where(COL_KD_TUJUANOPD, $p[COL_KD_TUJUANOPD])
          ->where(COL_KD_INDIKATORTUJUANOPD, $s[COL_KD_INDIKATORTUJUANOPD])
          ->where(COL_KD_SASARANOPD, $p[COL_KD_SASARANOPD])
          ->where(COL_KD_INDIKATORSASARANOPD, $p[COL_KD_INDIKATORSASARANOPD])
          ->where(COL_KD_TAHUN, $p[COL_KD_TAHUN])
          ->where(COL_KD_PROGRAMOPD, $p[COL_KD_PROGRAMOPD])
          ->order_by(COL_KD_KEGIATANOPD, 'asc')
          ->get((!empty($SumberData)&&$SumberData=='PERUBAHAN'?TBL_SAKIP_PDPA_KEGIATAN:TBL_SAKIP_DPA_KEGIATAN))
          ->result_array();

          foreach($kegiatan as $keg) {
            $arrSubKegiatanOPD = array();
            $indKegiatanOPD = $this->db
            ->where(COL_KD_URUSAN, $keg[COL_KD_URUSAN])
            ->where(COL_KD_BIDANG, $keg[COL_KD_BIDANG])
            ->where(COL_KD_UNIT, $keg[COL_KD_UNIT])
            ->where(COL_KD_SUB, $keg[COL_KD_SUB])
            ->where(COL_KD_BID, $keg[COL_KD_BID])
            ->where(COL_KD_SUBBID, $keg[COL_KD_SUBBID])

            ->where(COL_KD_PEMDA, $keg[COL_KD_PEMDA])
            ->where(COL_KD_MISI, $keg[COL_KD_MISI])
            ->where(COL_KD_TUJUAN, $keg[COL_KD_TUJUAN])
            ->where(COL_KD_INDIKATORTUJUAN, $keg[COL_KD_INDIKATORTUJUAN])
            ->where(COL_KD_SASARAN, $keg[COL_KD_SASARAN])
            ->where(COL_KD_INDIKATORSASARAN, $keg[COL_KD_INDIKATORSASARAN])
            ->where(COL_KD_TUJUANOPD, $keg[COL_KD_TUJUANOPD])
            ->where(COL_KD_INDIKATORTUJUANOPD, $keg[COL_KD_INDIKATORTUJUANOPD])
            ->where(COL_KD_SASARANOPD, $keg[COL_KD_SASARANOPD])
            ->where(COL_KD_INDIKATORSASARANOPD, $keg[COL_KD_INDIKATORSASARANOPD])
            ->where(COL_KD_TAHUN, $keg[COL_KD_TAHUN])
            ->where(COL_KD_PROGRAMOPD, $keg[COL_KD_PROGRAMOPD])
            ->where(COL_KD_SASARANPROGRAMOPD, $keg[COL_KD_SASARANPROGRAMOPD])
            ->where(COL_KD_KEGIATANOPD, $keg[COL_KD_KEGIATANOPD])
            ->order_by(COL_KD_SASARANKEGIATANOPD, 'asc')
            ->order_by(COL_KD_INDIKATORKEGIATANOPD, 'asc')
            ->get((!empty($SumberData)&&$SumberData=='PERUBAHAN'?TBL_SAKIP_PDPA_KEGIATAN_INDIKATOR:TBL_SAKIP_DPA_KEGIATAN_INDIKATOR))
            ->result_array();

            $htmlKegiatanOPD = "<p class='node-name'>".$keg[COL_NM_KEGIATANOPD]."</p>";
            $ikkegiatan_ = "";
            if(count($indKegiatanOPD) > 0) {
                $ikkegiatan_ .= "<p class='node-title' style='margin-bottom: 0px;'>INDIKATOR :</p><ul style='padding-left: 2rem'>";
                foreach($indKegiatanOPD as $ikp) {
                  $ikkegiatan_ .= "<li>".$ikp[COL_NM_INDIKATORKEGIATANOPD]."</li>";
                }
                $ikkegiatan_ .= "</ul>";
            }
            $htmlKegiatanOPD .= $ikkegiatan_;

            if(!empty($keg[COL_NM_ARRSUBKEGIATAN])) {
              $arrSubKeg_ = json_decode($keg[COL_NM_ARRSUBKEGIATAN]);
              foreach ($arrSubKeg_ as $subkeg) {
                $htmlSubKegiatanOPD = "<p class='node-name'>".(!empty($subkeg->NmSubKegiatan)?$subkeg->NmSubKegiatan:'-')."</p>";
                if(!empty($subkeg->NmIndikatorSubKegiatan)) {
                  $htmlSubKegiatanOPD .= "<p class='node-title' style='margin-bottom: 0px;'>INDIKATOR: <ul style='padding-left: 2rem'><li>".$subkeg->NmIndikatorSubKegiatan."</li></ul></p>";
                }
                $arrSubKegiatanOPD[] = array(
                  "innerHTML" => $htmlSubKegiatanOPD,
                  "connectors" => array("style" => array("stroke" => "#000", "arrow-end" => "block-wide-long")),
                );
              }
            }

            $arrKegiatanOPD[] = array(
              "innerHTML" => $htmlKegiatanOPD,
              "connectors" => array("style" => array("stroke" => "#000", "arrow-end" => "block-wide-long")),
              "children" => $arrSubKegiatanOPD,
              "HTMLclass" => "bg-fuchsia"
            );
          }

          $htmlProgramOPD = "<p class='node-name'>".$p[COL_NM_PROGRAMOPD]."</p>";
          if(count($indProgramOPD) > 0) {
            $htmlProgramOPD .= "<p class='node-title' style='margin-bottom: 0px;'>INDIKATOR :</p><ul style='padding-left: 2rem'>";
            foreach($indProgramOPD as $i_) {
              $htmlProgramOPD .= "<li>".$i_[COL_NM_INDIKATORPROGRAMOPD]."</li>";
            }
            $htmlProgramOPD .= "</ul>";
          }
          $arrProgramOPD[] = array(
            "innerHTML" => $htmlProgramOPD,
            "connectors" => array("style" => array("stroke" => "#000", "arrow-end" => "block-wide-long")),
            "children" => $arrKegiatanOPD,
            "HTMLclass" => "bg-orange"
          );
        }

        $htmlSasaran = "<p class='node-name'>".$s[COL_NM_SASARANOPD]."</p>";
        if(count($iksasaran) > 0) {
          $htmlSasaran .= "<p class='node-title' style='margin-bottom: 0px;'>INDIKATOR :</p><ul style='padding-left: 2rem'>";
          foreach($iksasaran as $i_) {
            $htmlSasaran .= "<li>".$i_[COL_NM_INDIKATORSASARANOPD]."</li>";
          }
          $htmlSasaran .= "</ul>";
        }
        $arrSasaran[] = array(
            //"text" => array("name"=> $s[COL_KD_TUJUANOPD].".".$s[COL_KD_SASARANOPD].". Sasaran OPD (Eselon 2)", "title"=> $s[COL_NM_SASARANOPD]),
            "innerHTML" => $htmlSasaran,
            "connectors" => array("style" => array("stroke" => "#000", "arrow-end" => "block-wide-long")),
            "children" => $arrProgramOPD,
            "HTMLclass" => "bg-lime"
        );
    }

    $htmlTujuan = "<p class='node-name'>".$t[COL_NM_TUJUANOPD]."</p>";
    if(count($indikator) > 0) {
      $htmlTujuan .= "<p class='node-title' style='margin-bottom: 0px;'>INDIKATOR :</p><ul style='padding-left: 2rem'>";
      foreach($indikator as $i_) {
        $htmlTujuan .= "<li>".$i_[COL_NM_INDIKATORTUJUANOPD]."</li>";
      }
      $htmlTujuan .= "</ul>";
    }
    $arrTujuan[] = array(
        //"text" => array("name"=> "Tujuan OPD", "title"=> $t[COL_KD_TUJUANOPD].'. '.$t[COL_NM_TUJUANOPD]),
        "innerHTML" => $htmlTujuan,
        "connectors" => array("style" => array("stroke" => "#000", "arrow-end" => "block-wide-long")),
        "children" => $arrSasaran,
        "HTMLclass" => "bg-teal"
    );
}
$nodes = array(
    "text" => array("name"=> "OPD", "title"=> strtoupper($nmSub)),
    "connectors" => array("style" => array("stroke" => "#000", "arrow-end" => "block-wide-long")),
    "children" => $arrTujuan,
    "HTMLclass" => "bg-aqua"
);
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?=!empty($title) ? 'E-SAKIP | '.$title : SITENAME?></title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

    <!-- JQUERY -->
    <script src="<?=base_url()?>assets/themes/adminlte/plugins/jQuery/jquery-2.2.3.min.js"></script>

    <script type="text/javascript" src="<?=base_url()?>assets/treant/vendor/raphael.js"></script>
    <script type="text/javascript" src="<?=base_url()?>assets/treant/Treant.js"></script>
    <link href="<?=base_url()?>assets/treant/Treant.css" rel="stylesheet" type="text/css" />

    <link rel="stylesheet" href="<?=base_url()?>assets/treant/vendor/perfect-scrollbar/perfect-scrollbar.css">
    <link rel="stylesheet" href="<?=base_url()?>assets/tbs/css/font-awesome.min.css" />
    <link rel="stylesheet" href="<?=base_url()?>assets/themes/adminlte/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?=base_url()?>assets/themes/adminlte/dist/css/AdminLTE.min.css">
    <link rel="stylesheet" href="<?=base_url()?>assets/themes/adminlte/dist/css/skins/_all-skins.min.css">
    <script type="text/javascript" src="<?=base_url()?>assets/treant/vendor/jquery.mousewheel.js"></script>
    <script type="text/javascript" src="<?=base_url()?>assets/treant/vendor/perfect-scrollbar/perfect-scrollbar.js"></script>
</head>
<body>
<style>
    .nodeExample1 {
        border: 1px solid #000;
        padding : 0px !important;
        width : 15vw !important;
        font-size: 8pt;
        color: #000 !important;
    }
    .nodeExample1 .node-name {
        font-weight: bold;
        margin: 0 0 5px !important;
        border-bottom: 1px solid #000;
        padding: 2px;
    }
    .nodeExample1 .node-title {
        text-align: left;
        padding: 2px;
    }
    .chart {
        overflow: auto;
    }
    .text-left .node-title {
        text-align: left !important;
    }
    .bg-lime {
      background-color: #01ff7054 !important;
    }
    .bg-teal {
      background-color: #39cccc66 !important;
    }
    .bg-aqua {
      background-color: #00c0ef61 !important;
    }
    .bg-orange {
      background-color: #ff851b73 !important;
    }
    .bg-fuchsia {
      background-color: #f012be73 !important;
    }
</style>
<h4 style="text-align: center">
  CASCADING <br />
  <strong><?=strtoupper($nmSub)?><br />KOTA TEBING TINGGI</strong><br />
  <small style="font-style: italic">Dicetak melalui aplikasi <strong><?=$this->setting_web_name.' - '.$this->setting_web_desc?></strong></small>
</h4><hr />
<div class="chart" id="basic-example">

</div>
<script>
    var chart_config = {
        chart: {
            container: "#basic-example",
            scrollbar: "fancy",
            //animateOnInit: true,
            hideRootNode: true,
            rootOrientation:  'WEST', // NORTH || EAST || WEST || SOUTH
            connectors: {
                type: "step",
                style: {
                    "stroke-width": 1
                }
            },
            node: {
                HTMLclass: 'nodeExample1'
            },
            nodeAlign: 'TOP'
            /*animation: {
             nodeAnimation: "easeOutBounce",
             nodeSpeed: 700,
             connectorsAnimation: "bounce",
             connectorsSpeed: 700
             }*/
        },
        nodeStructure: <?=json_encode($nodes)?>
    };
    new Treant( chart_config );
</script>
</body>
