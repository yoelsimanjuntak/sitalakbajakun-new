<?php
/**
 * Created by PhpStorm.
 * User: Partopi Tao
 * Date: 3/14/2019
 * Time: 11:53 AM
 */
if(!empty($cetak)) {
    header("Content-type: application/vnd-ms-excel");
    header("Content-Disposition: attachment; filename=ESAKIP - TC 27.xls");
}
?>
<div class="table-responsive">
    <table class="table table-bordered" style="font-size: 9pt !important;" border="1">
        <caption style="text-align: center">
            <h5><?=strtoupper($nmSub)." KOTA TEBING TINGGI RENCANA PROGRAM / KEGIATAN PRIORITAS YANG DISERTAI KEBUTUHAN PENDANAANNYA"?><br />TAHUN <?=$data[COL_KD_TAHUN_FROM]." s.d ".$data[COL_KD_TAHUN_TO]?></h5>
        </caption>
        <tbody>
        <tr>
            <th rowspan="3">No.</th>
            <th rowspan="3">TUJUAN</th>
            <th rowspan="3">INDIKATOR TUJUAN</th>
            <th rowspan="3">SASARAN</th>
            <th rowspan="3">INDIKATOR SASARAN</th>
            <th rowspan="3" colspan="2">PROGRAM/KEGIATAN PRIORITAS</th>
            <th rowspan="3">SASARAN</th>
            <th rowspan="2" colspan="2">INDIKATOR KINERJA</th>
            <th rowspan="3">KONDISI AWAL <?=isset($tujuan)?$tujuan[0][COL_KD_TAHUN_FROM]:""?></th>
            <th colspan="10">TARGET CAPAIAN KINERJA PROGRAM DAN KERANGKA PENDANAAN</th>
            <th rowspan="2" colspan="2">KONDISI AKHIR <?=isset($tujuan)?$tujuan[0][COL_KD_TAHUN_TO]:""?></th>
        </tr>
        <?php
        if(isset($tujuan)) {
            ?>
            <tr>
                <?php
                for($i=$tujuan[0][COL_KD_TAHUN_FROM]+1; $i<=$tujuan[0][COL_KD_TAHUN_TO]; $i++) {
                    ?>
                    <th colspan="2"><?=$i?></th>
                <?php
                }
                ?>
            </tr>
        <?php
        }
        ?>
        <tr>
            <th>TOLOK UKUR</th>
            <th>SATUAN</th>
            <?php
            if(isset($tujuan)) {
                for($i=$tujuan[0][COL_KD_TAHUN_FROM]+1; $i<=$tujuan[0][COL_KD_TAHUN_TO]; $i++) {
                    ?>
                    <th>TARGET</th>
                    <th>PAGU</th>
                <?php
                }
            }
            ?>
            <th>TARGET</th>
            <th>PAGU</th>
        </tr>
        <tr>
            <th>1</th>
            <th>2</th>
            <th>3</th>
            <th>4</th>
            <th>5</th>
            <th>6</th>
            <th>7</th>
            <th>8</th>
            <th>9</th>
            <th>10</th>
            <th>11</th>
            <th>12</th>
            <th>13</th>
            <th>14</th>
            <th>15</th>
            <th>16</th>
            <th>17</th>
            <th>18</th>
            <th>19</th>
            <th>20</th>
            <th>21</th>
            <th>24</th>
            <th>25</th>
        </tr>
        <?php
        $no = 1;
        if(isset($tujuan)) {
            foreach($tujuan as $t) {
                $iktujuan = $this->db
                    ->where(COL_KD_URUSAN, $t[COL_KD_URUSAN])
                    ->where(COL_KD_BIDANG, $t[COL_KD_BIDANG])
                    ->where(COL_KD_UNIT, $t[COL_KD_UNIT])
                    ->where(COL_KD_SUB, $t[COL_KD_SUB])

                    ->where(COL_KD_PEMDA, $t[COL_KD_PEMDA])
                    ->where(COL_KD_MISI, $t[COL_KD_MISI])
                    ->where(COL_KD_TUJUAN, $t[COL_KD_TUJUAN])
                    ->where(COL_KD_INDIKATORTUJUAN, $t[COL_KD_INDIKATORTUJUAN])
                    ->where(COL_KD_SASARAN, $t[COL_KD_SASARAN])
                    ->where(COL_KD_INDIKATORSASARAN, $t[COL_KD_INDIKATORSASARAN])
                    ->where(COL_KD_TUJUANOPD, $t[COL_KD_TUJUANOPD])
                    ->order_by(TBL_SAKIP_MOPD_IKTUJUAN.".".COL_KD_INDIKATORTUJUANOPD, 'asc')
                    ->get(TBL_SAKIP_MOPD_IKTUJUAN)
                    ->result_array();
                ?>
                <tr>
                    <td style="text-align: center"><?=$no?></td>
                    <td><?=$t[COL_NM_TUJUANOPD]?></td>
                    <td><?=count($iktujuan) > 0 ? $iktujuan[0][COL_NM_INDIKATORTUJUANOPD] : ""?></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
                <?php
                if(count($iktujuan) > 0) {
                    for ($x = 0; $x < count($iktujuan); $x++) {
                        $sasaran = $this->db
                            ->where(COL_KD_URUSAN, $iktujuan[$x][COL_KD_URUSAN])
                            ->where(COL_KD_BIDANG, $iktujuan[$x][COL_KD_BIDANG])
                            ->where(COL_KD_UNIT, $iktujuan[$x][COL_KD_UNIT])
                            ->where(COL_KD_SUB, $iktujuan[$x][COL_KD_SUB])

                            ->where(COL_KD_PEMDA, $iktujuan[$x][COL_KD_PEMDA])
                            ->where(COL_KD_MISI, $iktujuan[$x][COL_KD_MISI])
                            ->where(COL_KD_TUJUAN, $iktujuan[$x][COL_KD_TUJUAN])
                            ->where(COL_KD_INDIKATORTUJUAN, $iktujuan[$x][COL_KD_INDIKATORTUJUAN])
                            ->where(COL_KD_SASARAN, $iktujuan[$x][COL_KD_SASARAN])
                            ->where(COL_KD_INDIKATORSASARAN, $iktujuan[$x][COL_KD_INDIKATORSASARAN])
                            ->where(COL_KD_TUJUANOPD, $iktujuan[$x][COL_KD_TUJUANOPD])
                            ->where(COL_KD_INDIKATORTUJUANOPD, $iktujuan[$x][COL_KD_INDIKATORTUJUANOPD])
                            ->order_by(TBL_SAKIP_MOPD_SASARAN.".".COL_KD_SASARANOPD, 'asc')
                            ->get(TBL_SAKIP_MOPD_SASARAN)
                            ->result_array();
                        if($x != 0) {
                            ?>
                            <tr>
                                <td style="text-align: center"></td>
                                <td></td>
                                <td><?=$iktujuan[$x][COL_NM_INDIKATORTUJUANOPD]?></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                        <?php
                        }
                        foreach($sasaran as $s) {
                            $iksasaran = $this->db
                                ->where(COL_KD_URUSAN, $s[COL_KD_URUSAN])
                                ->where(COL_KD_BIDANG, $s[COL_KD_BIDANG])
                                ->where(COL_KD_UNIT, $s[COL_KD_UNIT])
                                ->where(COL_KD_SUB, $s[COL_KD_SUB])

                                ->where(COL_KD_PEMDA, $s[COL_KD_PEMDA])
                                ->where(COL_KD_MISI, $s[COL_KD_MISI])
                                ->where(COL_KD_TUJUAN, $s[COL_KD_TUJUAN])
                                ->where(COL_KD_INDIKATORTUJUAN, $s[COL_KD_INDIKATORTUJUAN])
                                ->where(COL_KD_SASARAN, $s[COL_KD_SASARAN])
                                ->where(COL_KD_INDIKATORSASARAN, $s[COL_KD_INDIKATORSASARAN])
                                ->where(COL_KD_TUJUANOPD, $s[COL_KD_TUJUANOPD])
                                ->where(COL_KD_INDIKATORTUJUANOPD, $s[COL_KD_INDIKATORTUJUANOPD])
                                ->where(COL_KD_SASARANOPD, $s[COL_KD_SASARANOPD])
                                ->order_by(TBL_SAKIP_MOPD_IKSASARAN.".".COL_KD_INDIKATORSASARANOPD, 'asc')
                                ->get(TBL_SAKIP_MOPD_IKSASARAN)
                                ->result_array();
                            ?>
                            <tr>
                                <td style="text-align: center"></td>
                                <td></td>
                                <td></td>
                                <td><?=$s[COL_NM_SASARANOPD]?></td>
                                <td><?=count($iksasaran) > 0 ? $iksasaran[0][COL_NM_INDIKATORSASARANOPD] : ""?></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <?php
                            if(count($iksasaran) > 0) {
                                for($i=0; $i<count($iksasaran); $i++) {
                                    /*$rprogram = $this->db
                                        ->where(COL_KD_URUSAN, $iksasaran[$i][COL_KD_URUSAN])
                                        ->where(COL_KD_BIDANG, $iksasaran[$i][COL_KD_BIDANG])
                                        ->where(COL_KD_UNIT, $iksasaran[$i][COL_KD_UNIT])
                                        ->where(COL_KD_SUB, $iksasaran[$i][COL_KD_SUB])

                                        ->where(COL_KD_PEMDA, $iksasaran[$i][COL_KD_PEMDA])
                                        ->where(COL_KD_MISI, $iksasaran[$i][COL_KD_MISI])
                                        ->where(COL_KD_TUJUAN, $iksasaran[$i][COL_KD_TUJUAN])
                                        ->where(COL_KD_INDIKATORTUJUAN, $iksasaran[$i][COL_KD_INDIKATORTUJUAN])
                                        ->where(COL_KD_SASARAN, $iksasaran[$i][COL_KD_SASARAN])
                                        ->where(COL_KD_INDIKATORSASARAN, $iksasaran[$i][COL_KD_INDIKATORSASARAN])
                                        ->where(COL_KD_TUJUANOPD, $iksasaran[$i][COL_KD_TUJUANOPD])
                                        ->where(COL_KD_INDIKATORTUJUANOPD, $iksasaran[$i][COL_KD_INDIKATORTUJUANOPD])
                                        ->where(COL_KD_SASARANOPD, $iksasaran[$i][COL_KD_SASARANOPD])
                                        ->where(COL_KD_INDIKATORSASARANOPD, $iksasaran[$i][COL_KD_INDIKATORSASARANOPD])
                                        ->order_by(TBL_SAKIP_MBID_PROGRAM.".".COL_KD_TAHUN, 'asc')
                                        ->order_by(TBL_SAKIP_MBID_PROGRAM.".".COL_KD_PROGRAMOPD, 'asc')
                                        ->group_by(array(
                                            //TBL_SAKIP_MBID_PROGRAM.".".COL_KD_TAHUN,
                                            TBL_SAKIP_MBID_PROGRAM.".".COL_KD_PEMDA,
                                            TBL_SAKIP_MBID_PROGRAM.".".COL_KD_MISI,
                                            TBL_SAKIP_MBID_PROGRAM.".".COL_KD_TUJUAN,
                                            TBL_SAKIP_MBID_PROGRAM.".".COL_KD_INDIKATORTUJUAN,
                                            TBL_SAKIP_MBID_PROGRAM.".".COL_KD_SASARAN,
                                            TBL_SAKIP_MBID_PROGRAM.".".COL_KD_INDIKATORSASARAN,
                                            TBL_SAKIP_MBID_PROGRAM.".".COL_KD_URUSAN,
                                            TBL_SAKIP_MBID_PROGRAM.".".COL_KD_BIDANG,
                                            TBL_SAKIP_MBID_PROGRAM.".".COL_KD_UNIT,
                                            TBL_SAKIP_MBID_PROGRAM.".".COL_KD_SUB,
                                            TBL_SAKIP_MBID_PROGRAM.".".COL_KD_TUJUANOPD,
                                            TBL_SAKIP_MBID_PROGRAM.".".COL_KD_INDIKATORTUJUANOPD,
                                            TBL_SAKIP_MBID_PROGRAM.".".COL_KD_SASARANOPD,
                                            TBL_SAKIP_MBID_PROGRAM.".".COL_KD_INDIKATORSASARANOPD,
                                            TBL_SAKIP_MBID_PROGRAM.".".COL_KD_BID,
                                            TBL_SAKIP_MBID_PROGRAM.".".COL_KD_PROGRAMOPD))
                                        ->get(TBL_SAKIP_MBID_PROGRAM)
                                        ->result_array();*/
                                    $rprogram = $this->db
                                        ->join(TBL_SAKIP_MBID_PROGRAM,
                                            TBL_SAKIP_MBID_PROGRAM.'.'.COL_KD_URUSAN." = ".TBL_SAKIP_MBID_PROGRAM_SASARAN.".".COL_KD_URUSAN." AND ".
                                            TBL_SAKIP_MBID_PROGRAM.'.'.COL_KD_BIDANG." = ".TBL_SAKIP_MBID_PROGRAM_SASARAN.".".COL_KD_BIDANG." AND ".
                                            TBL_SAKIP_MBID_PROGRAM.'.'.COL_KD_UNIT." = ".TBL_SAKIP_MBID_PROGRAM_SASARAN.".".COL_KD_UNIT." AND ".
                                            TBL_SAKIP_MBID_PROGRAM.'.'.COL_KD_SUB." = ".TBL_SAKIP_MBID_PROGRAM_SASARAN.".".COL_KD_SUB." AND ".
                                            TBL_SAKIP_MBID_PROGRAM.'.'.COL_KD_BID." = ".TBL_SAKIP_MBID_PROGRAM_SASARAN.".".COL_KD_BID." AND ".

                                            TBL_SAKIP_MBID_PROGRAM.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_MBID_PROGRAM_SASARAN.".".COL_KD_PEMDA." AND ".
                                            TBL_SAKIP_MBID_PROGRAM.'.'.COL_KD_MISI." = ".TBL_SAKIP_MBID_PROGRAM_SASARAN.".".COL_KD_MISI." AND ".
                                            TBL_SAKIP_MBID_PROGRAM.'.'.COL_KD_TUJUAN." = ".TBL_SAKIP_MBID_PROGRAM_SASARAN.".".COL_KD_TUJUAN." AND ".
                                            TBL_SAKIP_MBID_PROGRAM.'.'.COL_KD_INDIKATORTUJUAN." = ".TBL_SAKIP_MBID_PROGRAM_SASARAN.".".COL_KD_INDIKATORTUJUAN." AND ".
                                            TBL_SAKIP_MBID_PROGRAM.'.'.COL_KD_SASARAN." = ".TBL_SAKIP_MBID_PROGRAM_SASARAN.".".COL_KD_SASARAN." AND ".
                                            TBL_SAKIP_MBID_PROGRAM.'.'.COL_KD_INDIKATORSASARAN." = ".TBL_SAKIP_MBID_PROGRAM_SASARAN.".".COL_KD_INDIKATORSASARAN." AND ".
                                            TBL_SAKIP_MBID_PROGRAM.'.'.COL_KD_TUJUANOPD." = ".TBL_SAKIP_MBID_PROGRAM_SASARAN.".".COL_KD_TUJUANOPD." AND ".
                                            TBL_SAKIP_MBID_PROGRAM.'.'.COL_KD_INDIKATORTUJUANOPD." = ".TBL_SAKIP_MBID_PROGRAM_SASARAN.".".COL_KD_INDIKATORTUJUANOPD." AND ".
                                            TBL_SAKIP_MBID_PROGRAM.'.'.COL_KD_SASARANOPD." = ".TBL_SAKIP_MBID_PROGRAM_SASARAN.".".COL_KD_SASARANOPD." AND ".
                                            TBL_SAKIP_MBID_PROGRAM.'.'.COL_KD_INDIKATORSASARANOPD." = ".TBL_SAKIP_MBID_PROGRAM_SASARAN.".".COL_KD_INDIKATORSASARANOPD." AND ".
                                            TBL_SAKIP_MBID_PROGRAM.'.'.COL_KD_PROGRAMOPD." = ".TBL_SAKIP_MBID_PROGRAM_SASARAN.".".COL_KD_PROGRAMOPD." AND ".
                                            TBL_SAKIP_MBID_PROGRAM.'.'.COL_KD_TAHUN." = ".TBL_SAKIP_MBID_PROGRAM_SASARAN.".".COL_KD_TAHUN
                                            ,"inner")
                                        ->where(TBL_SAKIP_MBID_PROGRAM_SASARAN.".".COL_KD_URUSAN, $iksasaran[$i][COL_KD_URUSAN])
                                        ->where(TBL_SAKIP_MBID_PROGRAM_SASARAN.".".COL_KD_BIDANG, $iksasaran[$i][COL_KD_BIDANG])
                                        ->where(TBL_SAKIP_MBID_PROGRAM_SASARAN.".".COL_KD_UNIT, $iksasaran[$i][COL_KD_UNIT])
                                        ->where(TBL_SAKIP_MBID_PROGRAM_SASARAN.".".COL_KD_SUB, $iksasaran[$i][COL_KD_SUB])

                                        ->where(TBL_SAKIP_MBID_PROGRAM_SASARAN.".".COL_KD_PEMDA, $iksasaran[$i][COL_KD_PEMDA])
                                        ->where(TBL_SAKIP_MBID_PROGRAM_SASARAN.".".COL_KD_MISI, $iksasaran[$i][COL_KD_MISI])
                                        ->where(TBL_SAKIP_MBID_PROGRAM_SASARAN.".".COL_KD_TUJUAN, $iksasaran[$i][COL_KD_TUJUAN])
                                        ->where(TBL_SAKIP_MBID_PROGRAM_SASARAN.".".COL_KD_INDIKATORTUJUAN, $iksasaran[$i][COL_KD_INDIKATORTUJUAN])
                                        ->where(TBL_SAKIP_MBID_PROGRAM_SASARAN.".".COL_KD_SASARAN, $iksasaran[$i][COL_KD_SASARAN])
                                        ->where(TBL_SAKIP_MBID_PROGRAM_SASARAN.".".COL_KD_INDIKATORSASARAN, $iksasaran[$i][COL_KD_INDIKATORSASARAN])
                                        ->where(TBL_SAKIP_MBID_PROGRAM_SASARAN.".".COL_KD_TUJUANOPD, $iksasaran[$i][COL_KD_TUJUANOPD])
                                        ->where(TBL_SAKIP_MBID_PROGRAM_SASARAN.".".COL_KD_INDIKATORTUJUANOPD, $iksasaran[$i][COL_KD_INDIKATORTUJUANOPD])
                                        ->where(TBL_SAKIP_MBID_PROGRAM_SASARAN.".".COL_KD_SASARANOPD, $iksasaran[$i][COL_KD_SASARANOPD])
                                        ->where(TBL_SAKIP_MBID_PROGRAM_SASARAN.".".COL_KD_INDIKATORSASARANOPD, $iksasaran[$i][COL_KD_INDIKATORSASARANOPD])
                                        ->order_by(TBL_SAKIP_MBID_PROGRAM_SASARAN.".".COL_KD_TAHUN, 'asc')
                                        ->order_by(TBL_SAKIP_MBID_PROGRAM_SASARAN.".".COL_KD_PROGRAMOPD, 'asc')
                                        ->order_by(TBL_SAKIP_MBID_PROGRAM_SASARAN.".".COL_KD_SASARANPROGRAMOPD, 'asc')
                                        ->group_by(array(
                                            //TBL_SAKIP_MBID_PROGRAM.".".COL_KD_TAHUN,
                                            TBL_SAKIP_MBID_PROGRAM_SASARAN.".".COL_KD_PEMDA,
                                            TBL_SAKIP_MBID_PROGRAM_SASARAN.".".COL_KD_MISI,
                                            TBL_SAKIP_MBID_PROGRAM_SASARAN.".".COL_KD_TUJUAN,
                                            TBL_SAKIP_MBID_PROGRAM_SASARAN.".".COL_KD_INDIKATORTUJUAN,
                                            TBL_SAKIP_MBID_PROGRAM_SASARAN.".".COL_KD_SASARAN,
                                            TBL_SAKIP_MBID_PROGRAM_SASARAN.".".COL_KD_INDIKATORSASARAN,
                                            TBL_SAKIP_MBID_PROGRAM_SASARAN.".".COL_KD_URUSAN,
                                            TBL_SAKIP_MBID_PROGRAM_SASARAN.".".COL_KD_BIDANG,
                                            TBL_SAKIP_MBID_PROGRAM_SASARAN.".".COL_KD_UNIT,
                                            TBL_SAKIP_MBID_PROGRAM_SASARAN.".".COL_KD_SUB,
                                            TBL_SAKIP_MBID_PROGRAM_SASARAN.".".COL_KD_TUJUANOPD,
                                            TBL_SAKIP_MBID_PROGRAM_SASARAN.".".COL_KD_INDIKATORTUJUANOPD,
                                            TBL_SAKIP_MBID_PROGRAM_SASARAN.".".COL_KD_SASARANOPD,
                                            TBL_SAKIP_MBID_PROGRAM_SASARAN.".".COL_KD_INDIKATORSASARANOPD,
                                            TBL_SAKIP_MBID_PROGRAM_SASARAN.".".COL_KD_BID,
                                            TBL_SAKIP_MBID_PROGRAM_SASARAN.".".COL_KD_PROGRAMOPD,
                                            TBL_SAKIP_MBID_PROGRAM_SASARAN.".".COL_KD_SASARANPROGRAMOPD))
                                        ->get(TBL_SAKIP_MBID_PROGRAM_SASARAN)
                                        ->result_array();
                                    if($i != 0) {
                                        ?>
                                        <tr>
                                            <td style="text-align: center"></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td><?=$iksasaran[$i][COL_NM_INDIKATORSASARANOPD]?></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                    <?php
                                    }
                                    $count_prg = 1;
                                    foreach($rprogram as $prg) {
                                        $rprg_awal = $this->db
                                            ->where(COL_KD_URUSAN, $prg[COL_KD_URUSAN])
                                            ->where(COL_KD_BIDANG, $prg[COL_KD_BIDANG])
                                            ->where(COL_KD_UNIT, $prg[COL_KD_UNIT])
                                            ->where(COL_KD_SUB, $prg[COL_KD_SUB])
                                            ->where(COL_KD_BID, $prg[COL_KD_BID])

                                            ->where(COL_KD_PEMDA, $iksasaran[$i][COL_KD_PEMDA])
                                            ->where(COL_KD_MISI, $iksasaran[$i][COL_KD_MISI])
                                            ->where(COL_KD_TUJUAN, $iksasaran[$i][COL_KD_TUJUAN])
                                            ->where(COL_KD_INDIKATORTUJUAN, $iksasaran[$i][COL_KD_INDIKATORTUJUAN])
                                            ->where(COL_KD_SASARAN, $iksasaran[$i][COL_KD_SASARAN])
                                            ->where(COL_KD_INDIKATORSASARAN, $iksasaran[$i][COL_KD_INDIKATORSASARAN])
                                            ->where(COL_KD_TUJUANOPD, $iksasaran[$i][COL_KD_TUJUANOPD])
                                            ->where(COL_KD_INDIKATORTUJUANOPD, $iksasaran[$i][COL_KD_INDIKATORTUJUANOPD])
                                            ->where(COL_KD_SASARANOPD, $iksasaran[$i][COL_KD_SASARANOPD])
                                            ->where(COL_KD_INDIKATORSASARANOPD, $iksasaran[$i][COL_KD_INDIKATORSASARANOPD])
                                            ->where(COL_KD_PROGRAMOPD, $prg[COL_KD_PROGRAMOPD])
                                            ->where(COL_KD_SASARANPROGRAMOPD, $prg[COL_KD_SASARANPROGRAMOPD])
                                            ->where(COL_KD_TAHUN, $data[COL_KD_TAHUN_FROM]+1)
                                            ->order_by(TBL_SAKIP_MBID_PROGRAM_SASARAN.".".COL_KD_TAHUN, 'asc')
                                            ->order_by(TBL_SAKIP_MBID_PROGRAM_SASARAN.".".COL_KD_PROGRAMOPD, 'asc')
                                            ->get(TBL_SAKIP_MBID_PROGRAM_SASARAN)
                                            ->row_array();
                                        $rprg_indikator = $this->db
                                            ->where(COL_KD_URUSAN, $prg[COL_KD_URUSAN])
                                            ->where(COL_KD_BIDANG, $prg[COL_KD_BIDANG])
                                            ->where(COL_KD_UNIT, $prg[COL_KD_UNIT])
                                            ->where(COL_KD_SUB, $prg[COL_KD_SUB])
                                            ->where(COL_KD_BID, $prg[COL_KD_BID])

                                            ->where(COL_KD_PEMDA, $prg[COL_KD_PEMDA])
                                            ->where(COL_KD_MISI, $prg[COL_KD_MISI])
                                            ->where(COL_KD_TUJUAN, $prg[COL_KD_TUJUAN])
                                            ->where(COL_KD_INDIKATORTUJUAN, $prg[COL_KD_INDIKATORTUJUAN])
                                            ->where(COL_KD_SASARAN, $prg[COL_KD_SASARAN])
                                            ->where(COL_KD_INDIKATORSASARAN, $prg[COL_KD_INDIKATORSASARAN])
                                            ->where(COL_KD_TUJUANOPD, $prg[COL_KD_TUJUANOPD])
                                            ->where(COL_KD_INDIKATORTUJUANOPD, $prg[COL_KD_INDIKATORTUJUANOPD])
                                            ->where(COL_KD_SASARANOPD, $prg[COL_KD_SASARANOPD])
                                            ->where(COL_KD_INDIKATORSASARANOPD, $prg[COL_KD_INDIKATORSASARANOPD])
                                            ->where(COL_KD_PROGRAMOPD, $prg[COL_KD_PROGRAMOPD])
                                            ->where(COL_KD_TAHUN, $prg[COL_KD_TAHUN])
                                            ->where(COL_KD_SASARANPROGRAMOPD, $prg[COL_KD_SASARANPROGRAMOPD])
                                            ->order_by(TBL_SAKIP_MBID_PROGRAM_INDIKATOR.".".COL_KD_PROGRAMOPD, 'asc')
                                            ->order_by(TBL_SAKIP_MBID_PROGRAM_INDIKATOR.".".COL_KD_SASARANPROGRAMOPD, 'asc')
                                            ->order_by(TBL_SAKIP_MBID_PROGRAM_INDIKATOR.".".COL_KD_INDIKATORPROGRAMOPD, 'asc')
                                            ->get(TBL_SAKIP_MBID_PROGRAM_INDIKATOR)
                                            ->result_array();
                                        $indikator_ = "";
                                        foreach($rprg_indikator as $ik) {
                                            $indikator_ .= $ik[COL_NM_INDIKATORPROGRAMOPD].", ";
                                        }
                                        $indikator_ = substr_replace($indikator_, ".", strlen($indikator_)-2, strlen($indikator_));

                                        $rkegiatan = $this->db
                                            //->select('*,'.TBL_SAKIP_MSUBBID_KEGIATAN_SASARAN.'.'.COL_NM_SASARANKEGIATANOPD.' as Nm_SasaranKegiatanOPD,'.TBL_SAKIP_MSUBBID_KEGIATAN_SASARAN.'.'.COL_KD_SATUAN.' as Kd_Satuan')
                                            /*->join(TBL_SAKIP_MSUBBID_KEGIATAN,
                                                TBL_SAKIP_MSUBBID_KEGIATAN.'.'.COL_KD_URUSAN." = ".TBL_SAKIP_MSUBBID_KEGIATAN_SASARAN.".".COL_KD_URUSAN." AND ".
                                                TBL_SAKIP_MSUBBID_KEGIATAN.'.'.COL_KD_BIDANG." = ".TBL_SAKIP_MSUBBID_KEGIATAN_SASARAN.".".COL_KD_BIDANG." AND ".
                                                TBL_SAKIP_MSUBBID_KEGIATAN.'.'.COL_KD_UNIT." = ".TBL_SAKIP_MSUBBID_KEGIATAN_SASARAN.".".COL_KD_UNIT." AND ".
                                                TBL_SAKIP_MSUBBID_KEGIATAN.'.'.COL_KD_SUB." = ".TBL_SAKIP_MSUBBID_KEGIATAN_SASARAN.".".COL_KD_SUB." AND ".

                                                TBL_SAKIP_MSUBBID_KEGIATAN.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_MSUBBID_KEGIATAN_SASARAN.".".COL_KD_PEMDA." AND ".
                                                TBL_SAKIP_MSUBBID_KEGIATAN.'.'.COL_KD_MISI." = ".TBL_SAKIP_MSUBBID_KEGIATAN_SASARAN.".".COL_KD_MISI." AND ".
                                                TBL_SAKIP_MSUBBID_KEGIATAN.'.'.COL_KD_TUJUAN." = ".TBL_SAKIP_MSUBBID_KEGIATAN_SASARAN.".".COL_KD_TUJUAN." AND ".
                                                TBL_SAKIP_MSUBBID_KEGIATAN.'.'.COL_KD_INDIKATORTUJUAN." = ".TBL_SAKIP_MSUBBID_KEGIATAN_SASARAN.".".COL_KD_INDIKATORTUJUAN." AND ".
                                                TBL_SAKIP_MSUBBID_KEGIATAN.'.'.COL_KD_SASARAN." = ".TBL_SAKIP_MSUBBID_KEGIATAN_SASARAN.".".COL_KD_SASARAN." AND ".
                                                TBL_SAKIP_MSUBBID_KEGIATAN.'.'.COL_KD_INDIKATORSASARAN." = ".TBL_SAKIP_MSUBBID_KEGIATAN_SASARAN.".".COL_KD_INDIKATORSASARAN." AND ".
                                                TBL_SAKIP_MSUBBID_KEGIATAN.'.'.COL_KD_TUJUANOPD." = ".TBL_SAKIP_MSUBBID_KEGIATAN_SASARAN.".".COL_KD_TUJUANOPD." AND ".
                                                TBL_SAKIP_MSUBBID_KEGIATAN.'.'.COL_KD_INDIKATORTUJUANOPD." = ".TBL_SAKIP_MSUBBID_KEGIATAN_SASARAN.".".COL_KD_INDIKATORTUJUANOPD." AND ".
                                                TBL_SAKIP_MSUBBID_KEGIATAN.'.'.COL_KD_SASARANOPD." = ".TBL_SAKIP_MSUBBID_KEGIATAN_SASARAN.".".COL_KD_SASARANOPD." AND ".
                                                TBL_SAKIP_MSUBBID_KEGIATAN.'.'.COL_KD_INDIKATORSASARANOPD." = ".TBL_SAKIP_MSUBBID_KEGIATAN_SASARAN.".".COL_KD_INDIKATORSASARANOPD." AND ".
                                                TBL_SAKIP_MSUBBID_KEGIATAN.'.'.COL_KD_BID." = ".TBL_SAKIP_MSUBBID_KEGIATAN_SASARAN.".".COL_KD_BID." AND ".
                                                TBL_SAKIP_MSUBBID_KEGIATAN.'.'.COL_KD_PROGRAMOPD." = ".TBL_SAKIP_MSUBBID_KEGIATAN_SASARAN.".".COL_KD_PROGRAMOPD." AND ".
                                                TBL_SAKIP_MSUBBID_KEGIATAN.'.'.COL_KD_TAHUN." = ".TBL_SAKIP_MSUBBID_KEGIATAN_SASARAN.".".COL_KD_TAHUN." AND ".
                                                TBL_SAKIP_MSUBBID_KEGIATAN.'.'.COL_KD_SASARANPROGRAMOPD." = ".TBL_SAKIP_MSUBBID_KEGIATAN_SASARAN.".".COL_KD_SASARANPROGRAMOPD." AND ".
                                                TBL_SAKIP_MSUBBID_KEGIATAN.'.'.COL_KD_KEGIATANOPD." = ".TBL_SAKIP_MSUBBID_KEGIATAN_SASARAN.".".COL_KD_KEGIATANOPD
                                                ,"inner")*/
                                            ->where(TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_URUSAN, $prg[COL_KD_URUSAN])
                                            ->where(TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_BIDANG, $prg[COL_KD_BIDANG])
                                            ->where(TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_UNIT, $prg[COL_KD_UNIT])
                                            ->where(TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_SUB, $prg[COL_KD_SUB])
                                            ->where(TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_BID, $prg[COL_KD_BID])

                                            ->where(TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_PEMDA, $prg[COL_KD_PEMDA])
                                            ->where(TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_MISI, $prg[COL_KD_MISI])
                                            ->where(TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_TUJUAN, $prg[COL_KD_TUJUAN])
                                            ->where(TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_INDIKATORTUJUAN, $prg[COL_KD_INDIKATORTUJUAN])
                                            ->where(TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_SASARAN, $prg[COL_KD_SASARAN])
                                            ->where(TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_INDIKATORSASARAN, $prg[COL_KD_INDIKATORSASARAN])
                                            ->where(TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_TUJUANOPD, $prg[COL_KD_TUJUANOPD])
                                            ->where(TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_INDIKATORTUJUANOPD, $prg[COL_KD_INDIKATORTUJUANOPD])
                                            ->where(TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_SASARANOPD, $prg[COL_KD_SASARANOPD])
                                            ->where(TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_INDIKATORSASARANOPD, $prg[COL_KD_INDIKATORSASARANOPD])
                                            ->where(TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_PROGRAMOPD, $prg[COL_KD_PROGRAMOPD])
                                            ->where(TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_SASARANPROGRAMOPD, $prg[COL_KD_SASARANPROGRAMOPD])
                                            ->order_by(TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_TAHUN, 'asc')
                                            ->order_by(TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_PROGRAMOPD, 'asc')
                                            ->group_by(array(
                                                //TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_TAHUN,
                                                TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_PEMDA,
                                                TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_MISI,
                                                TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_TUJUAN,
                                                TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_INDIKATORTUJUAN,
                                                TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_SASARAN,
                                                TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_INDIKATORSASARAN,
                                                TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_URUSAN,
                                                TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_BIDANG,
                                                TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_UNIT,
                                                TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_SUB,
                                                TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_TUJUANOPD,
                                                TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_INDIKATORTUJUANOPD,
                                                TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_SASARANOPD,
                                                TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_INDIKATORSASARANOPD,
                                                TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_BID,
                                                TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_PROGRAMOPD,
                                                TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_SUBBID,
                                                TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_KEGIATANOPD))
                                            ->get(TBL_SAKIP_MSUBBID_KEGIATAN)
                                            ->result_array();
                                        ?>
                                        <tr style="font-weight: bold !important;">
                                            <td style="text-align: center"></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td><?=/*$prg[COL_KD_PROGRAMOPD]*/$count_prg?></td>
                                            <td><?=$prg[COL_NM_PROGRAMOPD]?></td>
                                            <td><?=$prg[COL_NM_SASARANPROGRAMOPD]?></td>
                                            <td><?=$indikator_?></td>
                                            <td><?=!empty($prg) ? $prg[COL_KD_SATUAN] : "-"?></td>
                                            <td style="text-align: right"><?=!empty($rprg_awal) ? number_format($rprg_awal[COL_AWAL], 2) : "-"?></td>
                                            <?php
                                            for($n=$t[COL_KD_TAHUN_FROM]+1; $n<=$t[COL_KD_TAHUN_TO]; $n++) {
                                                $rprg_tahun = $this->db
                                                    ->where(COL_KD_URUSAN, $iksasaran[$i][COL_KD_URUSAN])
                                                    ->where(COL_KD_BIDANG, $iksasaran[$i][COL_KD_BIDANG])
                                                    ->where(COL_KD_UNIT, $iksasaran[$i][COL_KD_UNIT])
                                                    ->where(COL_KD_SUB, $iksasaran[$i][COL_KD_SUB])
                                                    ->where(COL_KD_BID, $prg[COL_KD_BID])

                                                    ->where(COL_KD_PEMDA, $iksasaran[$i][COL_KD_PEMDA])
                                                    ->where(COL_KD_MISI, $iksasaran[$i][COL_KD_MISI])
                                                    ->where(COL_KD_TUJUAN, $iksasaran[$i][COL_KD_TUJUAN])
                                                    ->where(COL_KD_INDIKATORTUJUAN, $iksasaran[$i][COL_KD_INDIKATORTUJUAN])
                                                    ->where(COL_KD_SASARAN, $iksasaran[$i][COL_KD_SASARAN])
                                                    ->where(COL_KD_INDIKATORSASARAN, $iksasaran[$i][COL_KD_INDIKATORSASARAN])
                                                    ->where(COL_KD_TUJUANOPD, $iksasaran[$i][COL_KD_TUJUANOPD])
                                                    ->where(COL_KD_INDIKATORTUJUANOPD, $iksasaran[$i][COL_KD_INDIKATORTUJUANOPD])
                                                    ->where(COL_KD_SASARANOPD, $iksasaran[$i][COL_KD_SASARANOPD])
                                                    ->where(COL_KD_INDIKATORSASARANOPD, $iksasaran[$i][COL_KD_INDIKATORSASARANOPD])
                                                    ->where(COL_KD_PROGRAMOPD, $prg[COL_KD_PROGRAMOPD])
                                                    ->where(COL_KD_SASARANPROGRAMOPD, $prg[COL_KD_SASARANPROGRAMOPD])
                                                    ->where(COL_KD_TAHUN, $n)
                                                    ->order_by(TBL_SAKIP_MBID_PROGRAM_SASARAN.".".COL_KD_TAHUN, 'asc')
                                                    ->order_by(TBL_SAKIP_MBID_PROGRAM_SASARAN.".".COL_KD_PROGRAMOPD, 'asc')
                                                    ->get(TBL_SAKIP_MBID_PROGRAM_SASARAN)
                                                    ->row_array();
                                                $totalkeg = $this->db
                                                    ->select_sum(COL_TOTAL)
                                                    ->where(COL_KD_URUSAN, $iksasaran[$i][COL_KD_URUSAN])
                                                    ->where(COL_KD_BIDANG, $iksasaran[$i][COL_KD_BIDANG])
                                                    ->where(COL_KD_UNIT, $iksasaran[$i][COL_KD_UNIT])
                                                    ->where(COL_KD_SUB, $iksasaran[$i][COL_KD_SUB])
                                                    ->where(COL_KD_BID, $prg[COL_KD_BID])

                                                    ->where(COL_KD_PEMDA, $iksasaran[$i][COL_KD_PEMDA])
                                                    ->where(COL_KD_MISI, $iksasaran[$i][COL_KD_MISI])
                                                    ->where(COL_KD_TUJUAN, $iksasaran[$i][COL_KD_TUJUAN])
                                                    ->where(COL_KD_INDIKATORTUJUAN, $iksasaran[$i][COL_KD_INDIKATORTUJUAN])
                                                    ->where(COL_KD_SASARAN, $iksasaran[$i][COL_KD_SASARAN])
                                                    ->where(COL_KD_INDIKATORSASARAN, $iksasaran[$i][COL_KD_INDIKATORSASARAN])
                                                    ->where(COL_KD_TUJUANOPD, $iksasaran[$i][COL_KD_TUJUANOPD])
                                                    ->where(COL_KD_INDIKATORTUJUANOPD, $iksasaran[$i][COL_KD_INDIKATORTUJUANOPD])
                                                    ->where(COL_KD_SASARANOPD, $iksasaran[$i][COL_KD_SASARANOPD])
                                                    ->where(COL_KD_INDIKATORSASARANOPD, $iksasaran[$i][COL_KD_INDIKATORSASARANOPD])
                                                    ->where(COL_KD_PROGRAMOPD, $prg[COL_KD_PROGRAMOPD])
                                                    ->where(COL_KD_SASARANPROGRAMOPD, $prg[COL_KD_SASARANPROGRAMOPD])
                                                    ->where(COL_KD_TAHUN, $n)
                                                    ->order_by(TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_TAHUN, 'asc')
                                                    ->order_by(TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_PROGRAMOPD, 'asc')
                                                    ->get(TBL_SAKIP_MSUBBID_KEGIATAN)
                                                    ->row_array();

                                                ?>
                                                <td style="text-align: right"><?=!empty($rprg_tahun)?number_format($rprg_tahun[COL_TARGET], 2):"-"?></td>
                                                <td style="text-align: right"><?=!empty($totalkeg)?number_format($totalkeg[COL_TOTAL]):"-"?></td>
                                            <?php
                                            }
                                            ?>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                        <?php
                                        $count_keg = 1;
                                        foreach($rkegiatan as $keg) {
                                            $rkeg_awal = $this->db
                                                ->where(COL_KD_URUSAN, $keg[COL_KD_URUSAN])
                                                ->where(COL_KD_BIDANG, $keg[COL_KD_BIDANG])
                                                ->where(COL_KD_UNIT, $keg[COL_KD_UNIT])
                                                ->where(COL_KD_SUB, $keg[COL_KD_SUB])
                                                ->where(COL_KD_BID, $keg[COL_KD_BID])

                                                ->where(COL_KD_PEMDA, $keg[COL_KD_PEMDA])
                                                ->where(COL_KD_MISI, $keg[COL_KD_MISI])
                                                ->where(COL_KD_TUJUAN, $keg[COL_KD_TUJUAN])
                                                ->where(COL_KD_INDIKATORTUJUAN, $keg[COL_KD_INDIKATORTUJUAN])
                                                ->where(COL_KD_SASARAN, $keg[COL_KD_SASARAN])
                                                ->where(COL_KD_INDIKATORSASARAN, $keg[COL_KD_INDIKATORSASARAN])
                                                ->where(COL_KD_TUJUANOPD, $keg[COL_KD_TUJUANOPD])
                                                ->where(COL_KD_INDIKATORTUJUANOPD, $keg[COL_KD_INDIKATORTUJUANOPD])
                                                ->where(COL_KD_SASARANOPD, $keg[COL_KD_SASARANOPD])
                                                ->where(COL_KD_INDIKATORSASARANOPD, $keg[COL_KD_INDIKATORSASARANOPD])
                                                ->where(COL_KD_PROGRAMOPD, $keg[COL_KD_PROGRAMOPD])
                                                ->where(COL_KD_SASARANPROGRAMOPD, $keg[COL_KD_SASARANPROGRAMOPD])
                                                ->where(COL_KD_KEGIATANOPD, $keg[COL_KD_KEGIATANOPD])
                                                ->where(COL_KD_SASARANKEGIATANOPD, $keg[COL_KD_SASARANKEGIATANOPD])
                                                ->where(COL_KD_TAHUN, $data[COL_KD_TAHUN_FROM]+1)
                                                ->order_by(TBL_SAKIP_MSUBBID_KEGIATAN_SASARAN.".".COL_KD_TAHUN, 'asc')
                                                ->order_by(TBL_SAKIP_MSUBBID_KEGIATAN_SASARAN.".".COL_KD_KEGIATANOPD, 'asc')
                                                ->get(TBL_SAKIP_MSUBBID_KEGIATAN_SASARAN)
                                                ->row_array();

                                            $kegiatan_indikator = $this->db
                                                ->where(COL_KD_URUSAN, $keg[COL_KD_URUSAN])
                                                ->where(COL_KD_BIDANG, $keg[COL_KD_BIDANG])
                                                ->where(COL_KD_UNIT, $keg[COL_KD_UNIT])
                                                ->where(COL_KD_SUB, $keg[COL_KD_SUB])
                                                ->where(COL_KD_BID, $keg[COL_KD_BID])

                                                ->where(COL_KD_PEMDA, $keg[COL_KD_PEMDA])
                                                ->where(COL_KD_MISI, $keg[COL_KD_MISI])
                                                ->where(COL_KD_TUJUAN, $keg[COL_KD_TUJUAN])
                                                ->where(COL_KD_INDIKATORTUJUAN, $keg[COL_KD_INDIKATORTUJUAN])
                                                ->where(COL_KD_SASARAN, $keg[COL_KD_SASARAN])
                                                ->where(COL_KD_INDIKATORSASARAN, $keg[COL_KD_INDIKATORSASARAN])
                                                ->where(COL_KD_TUJUANOPD, $keg[COL_KD_TUJUANOPD])
                                                ->where(COL_KD_INDIKATORTUJUANOPD, $keg[COL_KD_INDIKATORTUJUANOPD])
                                                ->where(COL_KD_SASARANOPD, $keg[COL_KD_SASARANOPD])
                                                ->where(COL_KD_INDIKATORSASARANOPD, $keg[COL_KD_INDIKATORSASARANOPD])
                                                ->where(COL_KD_PROGRAMOPD, $keg[COL_KD_PROGRAMOPD])
                                                ->where(COL_KD_SASARANPROGRAMOPD, $keg[COL_KD_SASARANPROGRAMOPD])
                                                ->where(COL_KD_KEGIATANOPD, $keg[COL_KD_KEGIATANOPD])
                                                ->where(COL_KD_SASARANKEGIATANOPD, $keg[COL_KD_SASARANKEGIATANOPD])
                                                ->order_by(TBL_SAKIP_MSUBBID_KEGIATAN_INDIKATOR.".".COL_KD_TAHUN, 'asc')
                                                ->order_by(TBL_SAKIP_MSUBBID_KEGIATAN_INDIKATOR.".".COL_KD_KEGIATANOPD, 'asc')
                                                ->order_by(TBL_SAKIP_MSUBBID_KEGIATAN_INDIKATOR.".".COL_KD_SASARANKEGIATANOPD, 'asc')
                                                ->order_by(TBL_SAKIP_MSUBBID_KEGIATAN_INDIKATOR.".".COL_KD_INDIKATORKEGIATANOPD, 'asc')
                                                ->get(TBL_SAKIP_MSUBBID_KEGIATAN_INDIKATOR)
                                                ->result_array();
                                            $kegiatan_indikator_ = "";
                                            foreach($kegiatan_indikator as $ik) {
                                                $kegiatan_indikator_ .= $ik[COL_NM_INDIKATORKEGIATANOPD].", ";
                                            }
                                            $kegiatan_indikator_ = substr_replace($kegiatan_indikator_, ".", strlen($kegiatan_indikator_)-2, strlen($kegiatan_indikator_));

                                            $nmKeg = $keg[COL_NM_KEGIATANOPD];
                                            if(!empty($keg)) {
                                                if(empty($nmKeg) && $keg[COL_ISEPLAN]) {
                                                    $eplandb->where(COL_KD_URUSAN, $keg[COL_KD_URUSAN]);
                                                    $eplandb->where(COL_KD_BIDANG, $keg[COL_KD_BIDANG]);
                                                    $eplandb->where(COL_KD_UNIT, $keg[COL_KD_UNIT]);
                                                    $eplandb->where(COL_KD_SUB, $keg[COL_KD_SUB]);
                                                    $eplandb->where("Tahun", $keg[COL_KD_TAHUN]-1);
                                                    $eplandb->where("Kd_Prog", $keg[COL_KD_PROGRAMOPD]);
                                                    $eplandb->where("Kd_Keg", $keg[COL_KD_KEGIATANOPD]);
                                                    $keg_ = $eplandb->get("ta_kegiatan")->row_array();
                                                    if($keg_) {
                                                        $nmKeg = $keg_["Ket_Kegiatan"];
                                                    }
                                                } else {
                                                    $nmKeg = $keg[COL_NM_KEGIATANOPD];
                                                }
                                            }

                                            ?>
                                            <tr>
                                                <td style="text-align: center"></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td><?=/*$keg[COL_KD_KEGIATANOPD]*/$count_keg?></td>
                                                <td><?=$nmKeg?></td>
                                                <td><?=$keg[COL_NM_SASARANKEGIATANOPD]?></td>
                                                <td><?=$kegiatan_indikator_?></td>
                                                <td><?=$keg[COL_KD_SATUAN]?></td>
                                                <td style="text-align: right"><?=!empty($rkeg_awal) ? number_format($rkeg_awal[COL_AWAL], 2) : "-"?></td>
                                                <?php
                                                for($n=$t[COL_KD_TAHUN_FROM]+1; $n<=$t[COL_KD_TAHUN_TO]; $n++) {
                                                    $totalkeg_tahun = $this->db
                                                        ->select_sum(COL_TOTAL)
                                                        ->where(COL_KD_URUSAN, $keg[COL_KD_URUSAN])
                                                        ->where(COL_KD_BIDANG, $keg[COL_KD_BIDANG])
                                                        ->where(COL_KD_UNIT, $keg[COL_KD_UNIT])
                                                        ->where(COL_KD_SUB, $keg[COL_KD_SUB])
                                                        ->where(COL_KD_BID, $keg[COL_KD_BID])
                                                        ->where(COL_KD_SUBBID, $keg[COL_KD_SUBBID])

                                                        ->where(COL_KD_PEMDA, $keg[COL_KD_PEMDA])
                                                        ->where(COL_KD_MISI, $keg[COL_KD_MISI])
                                                        ->where(COL_KD_TUJUAN, $keg[COL_KD_TUJUAN])
                                                        ->where(COL_KD_INDIKATORTUJUAN, $keg[COL_KD_INDIKATORTUJUAN])
                                                        ->where(COL_KD_SASARAN, $keg[COL_KD_SASARAN])
                                                        ->where(COL_KD_INDIKATORSASARAN, $keg[COL_KD_INDIKATORSASARAN])
                                                        ->where(COL_KD_TUJUANOPD, $keg[COL_KD_TUJUANOPD])
                                                        ->where(COL_KD_INDIKATORTUJUANOPD, $keg[COL_KD_INDIKATORTUJUANOPD])
                                                        ->where(COL_KD_SASARANOPD, $keg[COL_KD_SASARANOPD])
                                                        ->where(COL_KD_INDIKATORSASARANOPD, $keg[COL_KD_INDIKATORSASARANOPD])
                                                        ->where(COL_KD_PROGRAMOPD, $keg[COL_KD_PROGRAMOPD])
                                                        ->where(COL_KD_SASARANPROGRAMOPD, $keg[COL_KD_SASARANPROGRAMOPD])
                                                        ->where(COL_KD_TAHUN, $n)
                                                        ->where(COL_KD_KEGIATANOPD, $keg[COL_KD_KEGIATANOPD])
                                                        ->order_by(TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_TAHUN, 'asc')
                                                        ->order_by(TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_PROGRAMOPD, 'asc')
                                                        ->get(TBL_SAKIP_MSUBBID_KEGIATAN)
                                                        ->row_array();

                                                    $rkeg_tahun = $this->db
                                                        ->where(COL_KD_URUSAN, $keg[COL_KD_URUSAN])
                                                        ->where(COL_KD_BIDANG, $keg[COL_KD_BIDANG])
                                                        ->where(COL_KD_UNIT, $keg[COL_KD_UNIT])
                                                        ->where(COL_KD_SUB, $keg[COL_KD_SUB])
                                                        ->where(COL_KD_BID, $keg[COL_KD_BID])
                                                        ->where(COL_KD_SUBBID, $keg[COL_KD_SUBBID])

                                                        ->where(COL_KD_PEMDA, $keg[COL_KD_PEMDA])
                                                        ->where(COL_KD_MISI, $keg[COL_KD_MISI])
                                                        ->where(COL_KD_TUJUAN, $keg[COL_KD_TUJUAN])
                                                        ->where(COL_KD_INDIKATORTUJUAN, $keg[COL_KD_INDIKATORTUJUAN])
                                                        ->where(COL_KD_SASARAN, $keg[COL_KD_SASARAN])
                                                        ->where(COL_KD_INDIKATORSASARAN, $keg[COL_KD_INDIKATORSASARAN])
                                                        ->where(COL_KD_TUJUANOPD, $keg[COL_KD_TUJUANOPD])
                                                        ->where(COL_KD_INDIKATORTUJUANOPD, $keg[COL_KD_INDIKATORTUJUANOPD])
                                                        ->where(COL_KD_SASARANOPD, $keg[COL_KD_SASARANOPD])
                                                        ->where(COL_KD_INDIKATORSASARANOPD, $keg[COL_KD_INDIKATORSASARANOPD])
                                                        ->where(COL_KD_PROGRAMOPD, $keg[COL_KD_PROGRAMOPD])
                                                        ->where(COL_KD_SASARANPROGRAMOPD, $keg[COL_KD_SASARANPROGRAMOPD])
                                                        ->where(COL_KD_KEGIATANOPD, $keg[COL_KD_KEGIATANOPD])
                                                        ->where(COL_KD_SASARANKEGIATANOPD, $keg[COL_KD_SASARANKEGIATANOPD])
                                                        ->where(COL_KD_TAHUN, $n)
                                                        ->order_by(TBL_SAKIP_MSUBBID_KEGIATAN_SASARAN.".".COL_KD_TAHUN, 'asc')
                                                        ->order_by(TBL_SAKIP_MSUBBID_KEGIATAN_SASARAN.".".COL_KD_KEGIATANOPD, 'asc')
                                                        ->order_by(TBL_SAKIP_MSUBBID_KEGIATAN_SASARAN.".".COL_KD_SASARANKEGIATANOPD, 'asc')
                                                        ->get(TBL_SAKIP_MSUBBID_KEGIATAN_SASARAN)
                                                        ->row_array();

                                                    ?>
                                                    <td style="text-align: right"><?=!empty($rkeg_tahun)?number_format($rkeg_tahun[COL_TARGET], 2):"-"?></td>
                                                    <td style="text-align: right"><?=!empty($totalkeg_tahun)?number_format($totalkeg_tahun[COL_TOTAL]):"0"?></td>
                                                <?php
                                                }
                                                ?>
                                                <td></td>
                                                <td></td>
                                            </tr>
                                            <?php
                                            $count_keg++;
                                        }
                                        $count_prg++;
                                    }
                                }
                            }
                        }
                    }
                }
                $no++;
            }
        }
        ?>
        </tbody>
    </table>
</div>