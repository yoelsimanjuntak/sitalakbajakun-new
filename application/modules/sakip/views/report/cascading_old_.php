<?php
/**
 * Created by PhpStorm.
 * User: Partopi Tao
 * Date: 3/17/2019
 * Time: 8:58 PM
 */
$arrMisi = array();
$eplandb = $this->load->database("eplan", true);
$arrTujuan = array();
foreach($rtujuan as $t) {
    $arrSasaran = array();
    $indikator = $this->db
        ->where(COL_KD_URUSAN, $t[COL_KD_URUSAN])
        ->where(COL_KD_BIDANG, $t[COL_KD_BIDANG])
        ->where(COL_KD_UNIT, $t[COL_KD_UNIT])
        ->where(COL_KD_SUB, $t[COL_KD_SUB])

        ->where(COL_KD_PEMDA, $t[COL_KD_PEMDA])
        ->where(COL_KD_MISI, $t[COL_KD_MISI])
        ->where(COL_KD_TUJUAN, $t[COL_KD_TUJUAN])
        ->where(COL_KD_INDIKATORTUJUAN, $t[COL_KD_INDIKATORTUJUAN])
        ->where(COL_KD_SASARAN, $t[COL_KD_SASARAN])
        ->where(COL_KD_INDIKATORSASARAN, $t[COL_KD_INDIKATORSASARAN])
        ->where(COL_KD_TUJUANOPD, $t[COL_KD_TUJUANOPD])
        ->order_by(TBL_SAKIP_MOPD_IKTUJUAN.".".COL_KD_INDIKATORTUJUANOPD, 'asc')
        ->get(TBL_SAKIP_MOPD_IKTUJUAN)
        ->result_array();

    $sasaran = $this->db
        ->where(COL_KD_URUSAN, $t[COL_KD_URUSAN])
        ->where(COL_KD_BIDANG, $t[COL_KD_BIDANG])
        ->where(COL_KD_UNIT, $t[COL_KD_UNIT])
        ->where(COL_KD_SUB, $t[COL_KD_SUB])

        ->where(COL_KD_PEMDA, $t[COL_KD_PEMDA])
        ->where(COL_KD_MISI, $t[COL_KD_MISI])
        ->where(COL_KD_TUJUAN, $t[COL_KD_TUJUAN])
        ->where(COL_KD_INDIKATORTUJUAN, $t[COL_KD_INDIKATORTUJUAN])
        ->where(COL_KD_SASARAN, $t[COL_KD_SASARAN])
        ->where(COL_KD_INDIKATORSASARAN, $t[COL_KD_INDIKATORSASARAN])
        ->where(COL_KD_TUJUANOPD, $t[COL_KD_TUJUANOPD])
        ->order_by(TBL_SAKIP_MOPD_SASARAN.".".COL_KD_SASARANOPD, 'asc')
        ->get(TBL_SAKIP_MOPD_SASARAN)
        ->result_array();

    foreach($sasaran as $s) {
        $arrSasaranBid = array();

        $iksasaran = $this->db
            ->where(COL_KD_URUSAN, $s[COL_KD_URUSAN])
            ->where(COL_KD_BIDANG, $s[COL_KD_BIDANG])
            ->where(COL_KD_UNIT, $s[COL_KD_UNIT])
            ->where(COL_KD_SUB, $s[COL_KD_SUB])

            ->where(COL_KD_PEMDA, $s[COL_KD_PEMDA])
            ->where(COL_KD_MISI, $s[COL_KD_MISI])
            ->where(COL_KD_TUJUAN, $s[COL_KD_TUJUAN])
            ->where(COL_KD_INDIKATORTUJUAN, $s[COL_KD_INDIKATORTUJUAN])
            ->where(COL_KD_SASARAN, $s[COL_KD_SASARAN])
            ->where(COL_KD_INDIKATORSASARAN, $s[COL_KD_INDIKATORSASARAN])
            ->where(COL_KD_TUJUANOPD, $s[COL_KD_TUJUANOPD])
            ->where(COL_KD_INDIKATORTUJUANOPD, $s[COL_KD_INDIKATORTUJUANOPD])
            ->where(COL_KD_SASARANOPD, $s[COL_KD_SASARANOPD])
            ->order_by(TBL_SAKIP_MOPD_IKSASARAN.".".COL_KD_INDIKATORSASARANOPD, 'asc')
            ->get(TBL_SAKIP_MOPD_IKSASARAN)
            ->result_array();

        $sasaranbid = $this->db
        ->select(TBL_SAKIP_MBID_SASARAN.'.*,'.TBL_SAKIP_MBID.'.'.COL_NM_BID)
        ->join(TBL_SAKIP_MBID,
          TBL_SAKIP_MBID.'.'.COL_KD_URUSAN." = ".TBL_SAKIP_MBID_SASARAN.".".COL_KD_URUSAN." AND ".
          TBL_SAKIP_MBID.'.'.COL_KD_BIDANG." = ".TBL_SAKIP_MBID_SASARAN.".".COL_KD_BIDANG." AND ".
          TBL_SAKIP_MBID.'.'.COL_KD_UNIT." = ".TBL_SAKIP_MBID_SASARAN.".".COL_KD_UNIT." AND ".
          TBL_SAKIP_MBID.'.'.COL_KD_SUB." = ".TBL_SAKIP_MBID_SASARAN.".".COL_KD_SUB." AND ".
          TBL_SAKIP_MBID.'.'.COL_KD_BID." = ".TBL_SAKIP_MBID_SASARAN.".".COL_KD_BID
          ,"left")
        ->where(TBL_SAKIP_MBID_SASARAN.'.'.COL_KD_URUSAN, $s[COL_KD_URUSAN])
        ->where(TBL_SAKIP_MBID_SASARAN.'.'.COL_KD_BIDANG, $s[COL_KD_BIDANG])
        ->where(TBL_SAKIP_MBID_SASARAN.'.'.COL_KD_UNIT, $s[COL_KD_UNIT])
        ->where(TBL_SAKIP_MBID_SASARAN.'.'.COL_KD_SUB, $s[COL_KD_SUB])

        ->where(TBL_SAKIP_MBID_SASARAN.'.'.COL_KD_PEMDA, $s[COL_KD_PEMDA])
        ->where(TBL_SAKIP_MBID_SASARAN.'.'.COL_KD_MISI, $s[COL_KD_MISI])
        ->where(TBL_SAKIP_MBID_SASARAN.'.'.COL_KD_TUJUAN, $s[COL_KD_TUJUAN])
        ->where(TBL_SAKIP_MBID_SASARAN.'.'.COL_KD_INDIKATORTUJUAN, $s[COL_KD_INDIKATORTUJUAN])
        ->where(TBL_SAKIP_MBID_SASARAN.'.'.COL_KD_SASARAN, $s[COL_KD_SASARAN])
        ->where(TBL_SAKIP_MBID_SASARAN.'.'.COL_KD_INDIKATORSASARAN, $s[COL_KD_INDIKATORSASARAN])
        ->where(TBL_SAKIP_MBID_SASARAN.'.'.COL_KD_TUJUANOPD, $s[COL_KD_TUJUANOPD])
        ->where(TBL_SAKIP_MBID_SASARAN.'.'.COL_KD_INDIKATORTUJUANOPD, $s[COL_KD_INDIKATORTUJUANOPD])
        ->where(TBL_SAKIP_MBID_SASARAN.'.'.COL_KD_SASARANOPD, $s[COL_KD_SASARANOPD])
        ->where(TBL_SAKIP_MBID_SASARAN.'.'.COL_KD_TAHUN, $this->input->get(COL_KD_TAHUN))
        ->order_by(TBL_SAKIP_MBID_SASARAN.".".COL_KD_BID, 'asc')
        ->order_by(TBL_SAKIP_MBID_SASARAN.".".COL_KD_SASARANPROGRAMOPD, 'asc')
        ->get(TBL_SAKIP_MBID_SASARAN)
        ->result_array();

        foreach($sasaranbid as $sbid) {
            $arrSasaranSubbid = array();
            $iksasaranbid = $this->db
            ->where(COL_KD_URUSAN, $sbid[COL_KD_URUSAN])
            ->where(COL_KD_BIDANG, $sbid[COL_KD_BIDANG])
            ->where(COL_KD_UNIT, $sbid[COL_KD_UNIT])
            ->where(COL_KD_SUB, $sbid[COL_KD_SUB])

            ->where(COL_KD_PEMDA, $sbid[COL_KD_PEMDA])
            ->where(COL_KD_MISI, $sbid[COL_KD_MISI])
            ->where(COL_KD_TUJUAN, $sbid[COL_KD_TUJUAN])
            ->where(COL_KD_INDIKATORTUJUAN, $sbid[COL_KD_INDIKATORTUJUAN])
            ->where(COL_KD_SASARAN, $sbid[COL_KD_SASARAN])
            ->where(COL_KD_INDIKATORSASARAN, $sbid[COL_KD_INDIKATORSASARAN])
            ->where(COL_KD_TUJUANOPD, $sbid[COL_KD_TUJUANOPD])
            ->where(COL_KD_INDIKATORTUJUANOPD, $sbid[COL_KD_INDIKATORTUJUANOPD])
            ->where(COL_KD_SASARANOPD, $sbid[COL_KD_SASARANOPD])
            ->where(COL_KD_INDIKATORSASARANOPD, $sbid[COL_KD_INDIKATORSASARANOPD])
            ->where(COL_KD_SASARANPROGRAMOPD, $sbid[COL_KD_SASARANPROGRAMOPD])
            ->where(COL_KD_TAHUN, $sbid[COL_KD_TAHUN])
            ->where(COL_KD_BID, $sbid[COL_KD_BID])
            ->order_by(TBL_SAKIP_MBID_INDIKATOR.".".COL_KD_INDIKATORPROGRAMOPD, 'asc')
            ->get(TBL_SAKIP_MBID_INDIKATOR)
            ->result_array();

            $sasaransubbid = $this->db
            ->select(TBL_SAKIP_MSUBBID_SASARAN.'.*,'.TBL_SAKIP_MSUBBID.'.'.COL_NM_SUBBID)
            ->join(TBL_SAKIP_MSUBBID,
              TBL_SAKIP_MSUBBID.'.'.COL_KD_URUSAN." = ".TBL_SAKIP_MSUBBID_SASARAN.".".COL_KD_URUSAN." AND ".
              TBL_SAKIP_MSUBBID.'.'.COL_KD_BIDANG." = ".TBL_SAKIP_MSUBBID_SASARAN.".".COL_KD_BIDANG." AND ".
              TBL_SAKIP_MSUBBID.'.'.COL_KD_UNIT." = ".TBL_SAKIP_MSUBBID_SASARAN.".".COL_KD_UNIT." AND ".
              TBL_SAKIP_MSUBBID.'.'.COL_KD_SUB." = ".TBL_SAKIP_MSUBBID_SASARAN.".".COL_KD_SUB." AND ".
              TBL_SAKIP_MSUBBID.'.'.COL_KD_BID." = ".TBL_SAKIP_MSUBBID_SASARAN.".".COL_KD_BID." AND ".
              TBL_SAKIP_MSUBBID.'.'.COL_KD_SUBBID." = ".TBL_SAKIP_MSUBBID_SASARAN.".".COL_KD_SUBBID
              ,"left")
              ->where(TBL_SAKIP_MSUBBID_SASARAN.".".COL_KD_URUSAN, $sbid[COL_KD_URUSAN])
              ->where(TBL_SAKIP_MSUBBID_SASARAN.".".COL_KD_BIDANG, $sbid[COL_KD_BIDANG])
              ->where(TBL_SAKIP_MSUBBID_SASARAN.".".COL_KD_UNIT, $sbid[COL_KD_UNIT])
              ->where(TBL_SAKIP_MSUBBID_SASARAN.".".COL_KD_SUB, $sbid[COL_KD_SUB])

              ->where(TBL_SAKIP_MSUBBID_SASARAN.".".COL_KD_PEMDA, $sbid[COL_KD_PEMDA])
              ->where(TBL_SAKIP_MSUBBID_SASARAN.".".COL_KD_MISI, $sbid[COL_KD_MISI])
              ->where(TBL_SAKIP_MSUBBID_SASARAN.".".COL_KD_TUJUAN, $sbid[COL_KD_TUJUAN])
              ->where(TBL_SAKIP_MSUBBID_SASARAN.".".COL_KD_INDIKATORTUJUAN, $sbid[COL_KD_INDIKATORTUJUAN])
              ->where(TBL_SAKIP_MSUBBID_SASARAN.".".COL_KD_SASARAN, $sbid[COL_KD_SASARAN])
              ->where(TBL_SAKIP_MSUBBID_SASARAN.".".COL_KD_INDIKATORSASARAN, $sbid[COL_KD_INDIKATORSASARAN])
              ->where(TBL_SAKIP_MSUBBID_SASARAN.".".COL_KD_TUJUANOPD, $sbid[COL_KD_TUJUANOPD])
              ->where(TBL_SAKIP_MSUBBID_SASARAN.".".COL_KD_INDIKATORTUJUANOPD, $sbid[COL_KD_INDIKATORTUJUANOPD])
              ->where(TBL_SAKIP_MSUBBID_SASARAN.".".COL_KD_SASARANOPD, $sbid[COL_KD_SASARANOPD])
              ->where(TBL_SAKIP_MSUBBID_SASARAN.".".COL_KD_INDIKATORSASARANOPD, $sbid[COL_KD_INDIKATORSASARANOPD])
              ->where(TBL_SAKIP_MSUBBID_SASARAN.".".COL_KD_SASARANPROGRAMOPD, $sbid[COL_KD_SASARANPROGRAMOPD])
              ->where(TBL_SAKIP_MSUBBID_SASARAN.".".COL_KD_TAHUN, $sbid[COL_KD_TAHUN])
              ->where(TBL_SAKIP_MSUBBID_SASARAN.".".COL_KD_BID, $sbid[COL_KD_BID])
              ->order_by(TBL_SAKIP_MSUBBID_SASARAN.".".COL_KD_SASARANSUBBIDANG, 'asc')
              ->get(TBL_SAKIP_MSUBBID_SASARAN)
              ->result_array();

            foreach($sasaransubbid as $ssub) {
              $iksasaransubbid = $this->db
              ->where(COL_KD_URUSAN, $ssub[COL_KD_URUSAN])
              ->where(COL_KD_BIDANG, $ssub[COL_KD_BIDANG])
              ->where(COL_KD_UNIT, $ssub[COL_KD_UNIT])
              ->where(COL_KD_SUB, $ssub[COL_KD_SUB])

              ->where(COL_KD_PEMDA, $ssub[COL_KD_PEMDA])
              ->where(COL_KD_MISI, $ssub[COL_KD_MISI])
              ->where(COL_KD_TUJUAN, $ssub[COL_KD_TUJUAN])
              ->where(COL_KD_INDIKATORTUJUAN, $ssub[COL_KD_INDIKATORTUJUAN])
              ->where(COL_KD_SASARAN, $ssub[COL_KD_SASARAN])
              ->where(COL_KD_INDIKATORSASARAN, $ssub[COL_KD_INDIKATORSASARAN])
              ->where(COL_KD_TUJUANOPD, $ssub[COL_KD_TUJUANOPD])
              ->where(COL_KD_INDIKATORTUJUANOPD, $ssub[COL_KD_INDIKATORTUJUANOPD])
              ->where(COL_KD_SASARANOPD, $ssub[COL_KD_SASARANOPD])
              ->where(COL_KD_INDIKATORSASARANOPD, $ssub[COL_KD_INDIKATORSASARANOPD])
              ->where(COL_KD_SASARANPROGRAMOPD, $ssub[COL_KD_SASARANPROGRAMOPD])
              ->where(COL_KD_TAHUN, $ssub[COL_KD_TAHUN])
              ->where(COL_KD_BID, $ssub[COL_KD_BID])
              ->where(COL_KD_SUBBID, $ssub[COL_KD_SUBBID])
              ->where(COL_KD_SASARANSUBBIDANG, $ssub[COL_KD_SASARANSUBBIDANG])
              ->order_by(TBL_SAKIP_MSUBBID_INDIKATOR.".".COL_KD_INDIKATORSUBBIDANG, 'asc')
              ->get(TBL_SAKIP_MSUBBID_INDIKATOR)
              ->result_array();

              $sasaranIndividu = $this->db
              ->where(COL_KD_URUSAN, $ssub[COL_KD_URUSAN])
              ->where(COL_KD_BIDANG, $ssub[COL_KD_BIDANG])
              ->where(COL_KD_UNIT, $ssub[COL_KD_UNIT])
              ->where(COL_KD_SUB, $ssub[COL_KD_SUB])

              ->where(COL_KD_PEMDA, $ssub[COL_KD_PEMDA])
              ->where(COL_KD_MISI, $ssub[COL_KD_MISI])
              ->where(COL_KD_TUJUAN, $ssub[COL_KD_TUJUAN])
              ->where(COL_KD_INDIKATORTUJUAN, $ssub[COL_KD_INDIKATORTUJUAN])
              ->where(COL_KD_SASARAN, $ssub[COL_KD_SASARAN])
              ->where(COL_KD_INDIKATORSASARAN, $ssub[COL_KD_INDIKATORSASARAN])
              ->where(COL_KD_TUJUANOPD, $ssub[COL_KD_TUJUANOPD])
              ->where(COL_KD_INDIKATORTUJUANOPD, $ssub[COL_KD_INDIKATORTUJUANOPD])
              ->where(COL_KD_SASARANOPD, $ssub[COL_KD_SASARANOPD])
              ->where(COL_KD_INDIKATORSASARANOPD, $ssub[COL_KD_INDIKATORSASARANOPD])
              ->where(COL_KD_SASARANPROGRAMOPD, $ssub[COL_KD_SASARANPROGRAMOPD])
              ->where(COL_KD_TAHUN, $ssub[COL_KD_TAHUN])
              ->where(COL_KD_BID, $ssub[COL_KD_BID])
              ->where(COL_KD_SUBBID, $ssub[COL_KD_SUBBID])
              ->where(COL_KD_SASARANSUBBIDANG, $ssub[COL_KD_SASARANSUBBIDANG])
              ->order_by(TBL_SAKIP_INDIVIDU_SASARAN.".".COL_KD_SASARANINDIVIDU, 'asc')
              ->get(TBL_SAKIP_INDIVIDU_SASARAN)
              ->result_array();

              $arrSasaranIndividu = array();
              foreach ($sasaranIndividu as $ind) {
                $ikSasaranIndividu = $this->db
                ->where(COL_KD_URUSAN, $ind[COL_KD_URUSAN])
                ->where(COL_KD_BIDANG, $ind[COL_KD_BIDANG])
                ->where(COL_KD_UNIT, $ind[COL_KD_UNIT])
                ->where(COL_KD_SUB, $ind[COL_KD_SUB])

                ->where(COL_KD_PEMDA, $ind[COL_KD_PEMDA])
                ->where(COL_KD_MISI, $ind[COL_KD_MISI])
                ->where(COL_KD_TUJUAN, $ind[COL_KD_TUJUAN])
                ->where(COL_KD_INDIKATORTUJUAN, $ind[COL_KD_INDIKATORTUJUAN])
                ->where(COL_KD_SASARAN, $ind[COL_KD_SASARAN])
                ->where(COL_KD_INDIKATORSASARAN, $ind[COL_KD_INDIKATORSASARAN])
                ->where(COL_KD_TUJUANOPD, $ind[COL_KD_TUJUANOPD])
                ->where(COL_KD_INDIKATORTUJUANOPD, $ind[COL_KD_INDIKATORTUJUANOPD])
                ->where(COL_KD_SASARANOPD, $ind[COL_KD_SASARANOPD])
                ->where(COL_KD_INDIKATORSASARANOPD, $ind[COL_KD_INDIKATORSASARANOPD])
                ->where(COL_KD_SASARANPROGRAMOPD, $ind[COL_KD_SASARANPROGRAMOPD])
                ->where(COL_KD_TAHUN, $ind[COL_KD_TAHUN])
                ->where(COL_KD_BID, $ind[COL_KD_BID])
                ->where(COL_KD_SUBBID, $ind[COL_KD_SUBBID])
                ->where(COL_KD_SASARANSUBBIDANG, $ind[COL_KD_SASARANSUBBIDANG])
                ->where(COL_KD_SASARANINDIVIDU, $ind[COL_KD_SASARANINDIVIDU])
                ->order_by(TBL_SAKIP_INDIVIDU_INDIKATOR.".".COL_KD_INDIKATORINDIVIDU, 'asc')
                ->get(TBL_SAKIP_INDIVIDU_INDIKATOR)
                ->result_array();

                $htmlSasaranIndividu = "<p class='node-name'>SASARAN ".strtoupper($ind[COL_NM_JABATAN]).' ('.$ind[COL_KD_SASARANINDIVIDU].")</p>";
                if(count($ikSasaranIndividu) > 0) {
                  $htmlSasaranIndividu .= "<ul style='padding-left: 2rem'>";
                  foreach($ikSasaranIndividu as $i_) {
                    $htmlSasaranIndividu .= "<li>".$i_[COL_NM_SASARANINDIVIDU]."</li>";
                  }
                  $htmlSasaranIndividu .= "</ul>";
                }

                $arrSasaranIndividu[] = array(
                    //"text" => array("name"=> $ssub[COL_KD_TUJUANOPD].".".$ssub[COL_KD_SASARANOPD].".".$ssub[COL_KD_BID].".".$ssub[COL_KD_SASARANPROGRAMOPD].".".$ssub[COL_KD_SUBBID].".".$ssub[COL_KD_SASARANSUBBIDANG].". Sasaran Subbidang (Eselon 4)", "title"=> $ssub[COL_NM_SASARANSUBBIDANG]),
                    "innerHTML" => $htmlSasaranIndividu,
                    "connectors" => array("style" => array("stroke" => "#000", "arrow-end" => "block-wide-long")),
                    //"children" => $arrProgramChild,
                    //"HTMLclass" => "bg-fuchsia"
                );
              }

              $htmlSasaranSubbid = "<p class='node-name'>SASARAN ".strtoupper($ssub[COL_NM_SUBBID]).' ('.$ssub[COL_KD_SASARANSUBBIDANG].")</p><p class='node-title'>".$ssub[COL_NM_SASARANSUBBIDANG]."</p>";
              if(count($iksasaransubbid) > 0) {
                $htmlSasaranSubbid .= "<p class='node-title' style='margin-bottom: 0px; font-weight: bold'>INDIKATOR :</p><ul style='padding-left: 2rem'>";
                foreach($iksasaransubbid as $i_) {
                  $htmlSasaranSubbid .= "<li>".$i_[COL_NM_INDIKATORSUBBIDANG]."</li>";
                }
                $htmlSasaranSubbid .= "</ul>";
              }

              $arrSasaranSubbid[] = array(
                  //"text" => array("name"=> $ssub[COL_KD_TUJUANOPD].".".$ssub[COL_KD_SASARANOPD].".".$ssub[COL_KD_BID].".".$ssub[COL_KD_SASARANPROGRAMOPD].".".$ssub[COL_KD_SUBBID].".".$ssub[COL_KD_SASARANSUBBIDANG].". Sasaran Subbidang (Eselon 4)", "title"=> $ssub[COL_NM_SASARANSUBBIDANG]),
                  "innerHTML" => $htmlSasaranSubbid,
                  "connectors" => array("style" => array("stroke" => "#000", "arrow-end" => "block-wide-long")),
                  "children" => $arrSasaranIndividu,
                  "HTMLclass" => "bg-fuchsia"
              );
            }

            $htmlSasaranBid = "<p class='node-name'>SASARAN ".strtoupper($sbid[COL_NM_BID]).' ('.$sbid[COL_KD_SASARANPROGRAMOPD].")</p><p class='node-title'>".$sbid[COL_NM_SASARANPROGRAMOPD]."</p>";
            if(count($iksasaranbid) > 0) {
              $htmlSasaranBid .= "<p class='node-title' style='margin-bottom: 0px; font-weight: bold'>INDIKATOR :</p><ul style='padding-left: 2rem'>";
              foreach($iksasaranbid as $i_) {
                $htmlSasaranBid .= "<li>".$i_[COL_NM_INDIKATORPROGRAMOPD]."</li>";
              }
              $htmlSasaranBid .= "</ul>";
            }
            $arrSasaranBid[] = array(
                //"text" => array("name"=> $sbid[COL_KD_TUJUANOPD].".".$sbid[COL_KD_SASARANOPD].".".$sbid[COL_KD_BID].".".$sbid[COL_KD_SASARANPROGRAMOPD].". Sasaran Bidang (Eselon 3)", "title"=> $sbid[COL_NM_SASARANPROGRAMOPD]),
                "innerHTML" => $htmlSasaranBid,
                "connectors" => array("style" => array("stroke" => "#000", "arrow-end" => "block-wide-long")),
                "children" => $arrSasaranSubbid,
                "HTMLclass" => "bg-orange"
            );
        }

        $htmlSasaran = "<p class='node-name'>SASARAN (".$s[COL_KD_SASARANOPD].")</p><p class='node-title'>".$s[COL_NM_SASARANOPD]."</p>";
        if(count($iksasaran) > 0) {
          $htmlSasaran .= "<p class='node-title' style='margin-bottom: 0px; font-weight: bold'>INDIKATOR :</p><ul style='padding-left: 2rem'>";
          foreach($iksasaran as $i_) {
            $htmlSasaran .= "<li>".$i_[COL_NM_INDIKATORSASARANOPD]."</li>";
          }
          $htmlSasaran .= "</ul>";
        }
        $arrSasaran[] = array(
            //"text" => array("name"=> $s[COL_KD_TUJUANOPD].".".$s[COL_KD_SASARANOPD].". Sasaran OPD (Eselon 2)", "title"=> $s[COL_NM_SASARANOPD]),
            "innerHTML" => $htmlSasaran,
            "connectors" => array("style" => array("stroke" => "#000", "arrow-end" => "block-wide-long")),
            "children" => $arrSasaranBid,
            "HTMLclass" => "bg-lime"
        );
    }

    $htmlTujuan = "<p class='node-name'>TUJUAN (".$t[COL_KD_TUJUANOPD].")</p><p class='node-title'>".$t[COL_NM_TUJUANOPD]."</p>";
    if(count($indikator) > 0) {
      $htmlTujuan .= "<p class='node-title' style='margin-bottom: 0px; font-weight: bold'>INDIKATOR :</p><ul style='padding-left: 2rem'>";
      foreach($indikator as $i_) {
        $htmlTujuan .= "<li>".$i_[COL_NM_INDIKATORTUJUANOPD]."</li>";
      }
      $htmlTujuan .= "</ul>";
    }
    $arrTujuan[] = array(
        //"text" => array("name"=> "Tujuan OPD", "title"=> $t[COL_KD_TUJUANOPD].'. '.$t[COL_NM_TUJUANOPD]),
        "innerHTML" => $htmlTujuan,
        "connectors" => array("style" => array("stroke" => "#000", "arrow-end" => "block-wide-long")),
        "children" => $arrSasaran,
        "HTMLclass" => "bg-teal"
    );
}
$nodes = array(
    "text" => array("name"=> "OPD", "title"=> strtoupper($nmSub)),
    "connectors" => array("style" => array("stroke" => "#000", "arrow-end" => "block-wide-long")),
    "children" => $arrTujuan,
    "HTMLclass" => "bg-aqua"
);
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?=!empty($title) ? 'E-SAKIP | '.$title : SITENAME?></title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

    <!-- JQUERY -->
    <script src="<?=base_url()?>assets/themes/adminlte/plugins/jQuery/jquery-2.2.3.min.js"></script>

    <script type="text/javascript" src="<?=base_url()?>assets/treant/vendor/raphael.js"></script>
    <script type="text/javascript" src="<?=base_url()?>assets/treant/Treant.js"></script>
    <link href="<?=base_url()?>assets/treant/Treant.css" rel="stylesheet" type="text/css" />

    <link rel="stylesheet" href="<?=base_url()?>assets/treant/vendor/perfect-scrollbar/perfect-scrollbar.css">
    <link rel="stylesheet" href="<?=base_url()?>assets/tbs/css/font-awesome.min.css" />
    <link rel="stylesheet" href="<?=base_url()?>assets/themes/adminlte/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?=base_url()?>assets/themes/adminlte/dist/css/AdminLTE.min.css">
    <link rel="stylesheet" href="<?=base_url()?>assets/themes/adminlte/dist/css/skins/_all-skins.min.css">
    <script type="text/javascript" src="<?=base_url()?>assets/treant/vendor/jquery.mousewheel.js"></script>
    <script type="text/javascript" src="<?=base_url()?>assets/treant/vendor/perfect-scrollbar/perfect-scrollbar.js"></script>
</head>
<body>
<style>
    .nodeExample1 {
        border: 1px solid #000;
        padding : 0px !important;
        width : 15vw !important;
        font-size: 8pt;
        color: #000 !important;
    }
    .nodeExample1 .node-name {
        font-weight: bold;
        margin: 0 0 5px !important;
        border-bottom: 1px solid #000;
        padding: 2px;
    }
    .nodeExample1 .node-title {
        text-align: left;
        padding: 2px;
    }
    .chart {
        overflow: auto;
    }
    .text-left .node-title {
        text-align: left !important;
    }
    .bg-lime {
      background-color: #01ff7054 !important;
    }
    .bg-teal {
      background-color: #39cccc66 !important;
    }
    .bg-aqua {
      background-color: #00c0ef61 !important;
    }
    .bg-orange {
      background-color: #ff851b73 !important;
    }
    .bg-fuchsia {
      background-color: #f012be73 !important;
    }
</style>
<h4 style="text-align: center">
  CASCADING <br />
  <strong><?=strtoupper($nmSub)?><br />KOTA TEBING TINGGI</strong><br />
  <small style="font-style: italic">Dicetak melalui aplikasi <strong><?=$this->setting_web_name.' - '.$this->setting_web_desc?></strong></small>
</h4><hr />
<div class="chart" id="basic-example">

</div>
<script>
    var chart_config = {
        chart: {
            container: "#basic-example",
            scrollbar: "fancy",
            //animateOnInit: true,
            hideRootNode: true,
            rootOrientation:  'WEST', // NORTH || EAST || WEST || SOUTH
            connectors: {
                type: "step",
                style: {
                    "stroke-width": 1
                }
            },
            node: {
                HTMLclass: 'nodeExample1'
            },
            nodeAlign: 'TOP'
            /*animation: {
             nodeAnimation: "easeOutBounce",
             nodeSpeed: 700,
             connectorsAnimation: "bounce",
             connectorsSpeed: 700
             }*/
        },
        nodeStructure: <?=json_encode($nodes)?>
    };
    new Treant( chart_config );
</script>
</body>
