<?php
/**
 * Created by PhpStorm.
 * User: Partopi Tao
 * Date: 8/13/2019
 * Time: 9:27 PM
 */
$eplandb = $this->load->database("eplan", true);
$arrMisi = array();
foreach($misi as $m) {
    $arrKabTujuan = array();
    $this->db->join(TBL_SAKIP_MPEMDA,TBL_SAKIP_MPEMDA.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_MPMD_TUJUAN.".".COL_KD_PEMDA,"inner");
    $this->db->where(TBL_SAKIP_MPMD_TUJUAN.'.'.COL_KD_PEMDA, $m[COL_KD_PEMDA]);
    $this->db->where(TBL_SAKIP_MPMD_TUJUAN.'.'.COL_KD_MISI, $m[COL_KD_MISI]);
    $this->db->order_by(TBL_SAKIP_MPMD_TUJUAN.".".COL_KD_TUJUAN, 'asc');
    $rkab_tujuan = $this->db->get(TBL_SAKIP_MPMD_TUJUAN)->result_array();

    foreach($rkab_tujuan as $tujuankab) {
        $arrKabIkTujuan = array();
        $kab_iktujuan = $this->db
            ->where(COL_KD_PEMDA, $tujuankab[COL_KD_PEMDA])
            ->where(COL_KD_MISI, $tujuankab[COL_KD_MISI])
            ->where(COL_KD_TUJUAN, $tujuankab[COL_KD_TUJUAN])
            ->order_by(TBL_SAKIP_MPMD_IKTUJUAN.".".COL_KD_INDIKATORTUJUAN, 'asc')
            ->get(TBL_SAKIP_MPMD_IKTUJUAN)
            ->result_array();

        $htmlTujuan = "<p class='node-name'>Tujuan ".$tujuankab[COL_KD_MISI].".".$tujuankab[COL_KD_TUJUAN]."</p><p class='node-title'>".$tujuankab[COL_NM_TUJUAN]."</p>";
        $iktujuan_ = "";
        if(count($kab_iktujuan) > 0) {
            $iktujuan_ .= "<p class='node-title' style='margin: 0 !important'><strong>INDIKATOR :</strong></p><ul style='margin-left: 0px; padding-left: 15px;'>";
            foreach($kab_iktujuan as $ikt) {
                $iktujuan_ .= "<li>".$ikt[COL_NM_INDIKATORTUJUAN]."</li>";
            }
            $iktujuan_ .= "</ul>";
        }
        $htmlTujuan .= $iktujuan_;

        $arrKabSasaran = array();
        $kab_sasaran = $this->db
            ->where(COL_KD_PEMDA, $tujuankab[COL_KD_PEMDA])
            ->where(COL_KD_MISI, $tujuankab[COL_KD_MISI])
            ->where(COL_KD_TUJUAN, $tujuankab[COL_KD_TUJUAN])
            ->order_by(TBL_SAKIP_MPMD_SASARAN.".".COL_KD_SASARAN, 'asc')
            ->get(TBL_SAKIP_MPMD_SASARAN)
            ->result_array();

        foreach($kab_sasaran as $skab) {
            $arrKabIkSasaran = array();
            $kab_iksasaran = $this->db
                ->where(COL_KD_PEMDA, $skab[COL_KD_PEMDA])
                ->where(COL_KD_MISI, $skab[COL_KD_MISI])
                ->where(COL_KD_TUJUAN, $skab[COL_KD_TUJUAN])
                ->where(COL_KD_INDIKATORTUJUAN, $skab[COL_KD_INDIKATORTUJUAN])
                ->where(COL_KD_SASARAN, $skab[COL_KD_SASARAN])
                ->order_by(TBL_SAKIP_MPMD_IKSASARAN.".".COL_KD_INDIKATORSASARAN, 'asc')
                ->get(TBL_SAKIP_MPMD_IKSASARAN)
                ->result_array();

            $htmlSasaran = "<p class='node-name'>Sasaran ".$skab[COL_KD_MISI].".".$skab[COL_KD_TUJUAN].".".$skab[COL_KD_INDIKATORTUJUAN].".".$skab[COL_KD_SASARAN]."</p><p class='node-title'>".$skab[COL_NM_SASARAN]."</p>";
            $iksasaran_ = "";
            if(count($kab_iksasaran) > 0) {
                $iksasaran_ .= "<p class='node-title' style='margin: 0 !important'><strong>INDIKATOR :</strong></p><ul style='margin-left: 0px; padding-left: 15px;'>";
                foreach($kab_iksasaran as $iks) {
                    $iksasaran_ .= "<li>".$iks[COL_NM_INDIKATORSASARAN]."</li>";
                }
                $iksasaran_ .= "</ul>";
            }
            $htmlSasaran .= $iksasaran_;

            $arrSasaranOPD = array();
            $sasaranOPD = $this->db
            ->join(TBL_REF_SUB_UNIT,
                TBL_REF_SUB_UNIT.'.'.COL_KD_URUSAN." = ".TBL_SAKIP_MOPD_SASARAN.".".COL_KD_URUSAN." AND ".
                TBL_REF_SUB_UNIT.'.'.COL_KD_BIDANG." = ".TBL_SAKIP_MOPD_SASARAN.".".COL_KD_BIDANG." AND ".
                TBL_REF_SUB_UNIT.'.'.COL_KD_UNIT." = ".TBL_SAKIP_MOPD_SASARAN.".".COL_KD_UNIT." AND ".
                TBL_REF_SUB_UNIT.'.'.COL_KD_SUB." = ".TBL_SAKIP_MOPD_SASARAN.".".COL_KD_SUB
                ,"left")
            ->where(TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_PEMDA, $skab[COL_KD_PEMDA])
            ->where(TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_MISI, $skab[COL_KD_MISI])
            ->where(TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_TUJUAN, $skab[COL_KD_TUJUAN])
            ->where(TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_INDIKATORTUJUAN, $skab[COL_KD_INDIKATORTUJUAN])
            ->where(TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_SASARAN, $skab[COL_KD_SASARAN])
            ->group_by(array(
                TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_URUSAN,
                TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_BIDANG,
                TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_UNIT,
                TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_SUB
            ))
            ->order_by(TBL_REF_SUB_UNIT.".".COL_NM_SUB_UNIT, 'asc')
            ->get(TBL_SAKIP_MOPD_SASARAN)
            ->result_array();

            $htmlSasaranOPD = "<p class='node-name'>OPD PENANGGUNG JAWAB</p>";
            if(count($sasaranOPD) > 0) {
              $htmlSasaranOPD .= "<ul style='padding-left: 2rem'>";
              foreach($sasaranOPD as $i_) {
                $htmlSasaranOPD .= "<li>".$i_[COL_NM_SUB_UNIT]."</li>";
              }
              $htmlSasaranOPD .= "</ul>";
            }
            $arrSasaranOPD[] = array("innerHTML" => $htmlSasaranOPD,
                "connectors" => array("style" => array("stroke" => "#000", "arrow-end" => "block-wide-long")),
                "HTMLclass" => "bg-fuchsia"
            );

            $arrKabSasaran[] = array(
                "innerHTML" => $htmlSasaran,
                "connectors" => array("style" => array("stroke" => "#000", "arrow-end" => "block-wide-long")),
                "children" => $arrSasaranOPD,
                "HTMLclass" => "bg-orange node-wide"
            );
        }

        $arrKabTujuan[] = array(
            "innerHTML" => $htmlTujuan,
            "connectors" => array("style" => array("stroke" => "#000", "arrow-end" => "block-wide-long")),
            "children" => $arrKabSasaran,
            "HTMLclass" => "bg-lime node-wide"
        );
    }

    $arrMisi[] = array(
        "text" => array("name"=> "Misi ".$m[COL_KD_MISI], "title"=> $m[COL_NM_MISI]),
        "connectors" => array("style" => array("stroke" => "#000", "arrow-end" => "block-wide-long")),
        "children" => $arrKabTujuan,
        "HTMLclass" => "bg-aqua"
    );
}
$nodes = array(
    "text" => array("name"=> "Visi", "title"=> $rpemda[COL_NM_VISI]),
    "connectors" => array("style" => array("stroke" => "#000", "arrow-end" => "block-wide-long")),
    "children" => $arrMisi,
    "HTMLclass" => "bg-primary"
);
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title><?=!empty($title) ? 'E-SAKIP | '.$title : SITENAME?></title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

  <!-- JQUERY -->
  <script src="<?=base_url()?>assets/themes/adminlte/plugins/jQuery/jquery-2.2.3.min.js"></script>

  <script type="text/javascript" src="<?=base_url()?>assets/treant/vendor/raphael.js"></script>
  <script type="text/javascript" src="<?=base_url()?>assets/treant/Treant.js"></script>
  <link href="<?=base_url()?>assets/treant/Treant.css" rel="stylesheet" type="text/css" />

  <link rel="stylesheet" href="<?=base_url()?>assets/treant/vendor/perfect-scrollbar/perfect-scrollbar.css">
  <link rel="stylesheet" href="<?=base_url()?>assets/tbs/css/font-awesome.min.css" />
  <link rel="stylesheet" href="<?=base_url()?>assets/themes/adminlte/bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" href="<?=base_url()?>assets/themes/adminlte/dist/css/AdminLTE.min.css">
  <link rel="stylesheet" href="<?=base_url()?>assets/themes/adminlte/dist/css/skins/_all-skins.min.css">
  <script type="text/javascript" src="<?=base_url()?>assets/treant/vendor/jquery.mousewheel.js"></script>
  <script type="text/javascript" src="<?=base_url()?>assets/treant/vendor/perfect-scrollbar/perfect-scrollbar.js"></script>
</head>
<body>
<style>
    .nodeExample1 {
        border: 1px solid #000;
        padding : 0px !important;
        width : 25vh !important;
        font-size: 8pt;
        color: #000 !important;
    }
    .node-wide {
        width : 32vh !important;
    }
    .nodeExample1 .node-name {
        font-weight: bold;
        margin: 0 0 5px !important;
        border-bottom: 1px solid #000;
        padding: 2px;
    }
    .nodeExample1 .node-title {
        #text-align: justify;
        padding: 2px;
    }
    .chart {
        overflow: auto;
    }
    .bg-greenlight {
        background-color: #c4dd39 !important;
    }
    .node-title {
        padding: 5px !important;
    }
    .nodeExample1 ul {
        margin-left: 5px !important;
    }
    .bg-lime {
      background-color: #01ff7054 !important;
    }
    .bg-teal {
      background-color: #39cccc66 !important;
    }
    .bg-aqua {
      background-color: #00c0ef61 !important;
    }
    .bg-orange {
      background-color: #ff851b73 !important;
    }
    .bg-fuchsia {
      background-color: #f012be73 !important;
    }
</style>
<a id="btn-download" download="Pohon Kinerja.jpg" href="">Download</a>
<div id="chart">
  <h4 style="text-align: center">
    CASCADING <br />
    <strong>KOTA TEBING TINGGI</strong><br />
    <small style="font-style: italic">Dicetak melalui aplikasi <strong><?=$this->setting_web_name.' - '.$this->setting_web_desc?></strong></small>
  </h4><hr />
    <div class="chart" id="basic-example">

    </div>
</div>
<div id="canvas" style="display: none">

</div>
<script src="<?=base_url()?>assets/js/html2canvas.min.js"></script>
<script>
    var chart_config = {
        chart: {
            container: "#basic-example",
            scrollbar: "fancy",
            //animateOnInit: true,
            rootOrientation:  'WEST', // NORTH || EAST || WEST || SOUTH
            connectors: {
                type: "step",
                style: {
                    "stroke-width": 1
                }
            },
            node: {
                HTMLclass: 'nodeExample1'
            },
            nodeAlign: 'TOP',
            hideRootNode: true
            /*animation: {
             nodeAnimation: "easeOutBounce",
             nodeSpeed: 700,
             connectorsAnimation: "bounce",
             connectorsSpeed: 700
             }*/
        },
        nodeStructure: <?=json_encode($nodes)?>
    };
    new Treant( chart_config );
    html2canvas(document.querySelector("#chart"), {scale: 1.75}).then(canvas => {
        document.getElementById("canvas").appendChild(canvas);
        var img = canvas.toDataURL("image/jpg");
        $("#btn-download").attr("href", img);
    });
</script>
</body>
