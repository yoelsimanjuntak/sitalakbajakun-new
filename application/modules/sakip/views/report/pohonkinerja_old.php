<?php
/**
 * Created by PhpStorm.
 * User: Partopi Tao
 * Date: 3/17/2019
 * Time: 8:58 PM
 */
$arrMisi = array();
$eplandb = $this->load->database("eplan", true);
foreach($misi as $m) {
    $arrTujuan = array();
    $this->db->join(TBL_SAKIP_MPEMDA,TBL_SAKIP_MPEMDA.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_MOPD_TUJUAN.".".COL_KD_PEMDA,"inner");
    $this->db->where(TBL_SAKIP_MOPD_TUJUAN.'.'.COL_KD_PEMDA, $m[COL_KD_PEMDA]);
    $this->db->where(TBL_SAKIP_MOPD_TUJUAN.'.'.COL_KD_MISI, $m[COL_KD_MISI]);
    $this->db->where(TBL_SAKIP_MOPD_TUJUAN.'.'.COL_KD_URUSAN, $this->input->get(COL_KD_URUSAN));
    $this->db->where(TBL_SAKIP_MOPD_TUJUAN.'.'.COL_KD_BIDANG, $this->input->get(COL_KD_BIDANG));
    $this->db->where(TBL_SAKIP_MOPD_TUJUAN.'.'.COL_KD_UNIT, $this->input->get(COL_KD_UNIT));
    $this->db->where(TBL_SAKIP_MOPD_TUJUAN.'.'.COL_KD_SUB, $this->input->get(COL_KD_SUB));
    $this->db->order_by(TBL_SAKIP_MOPD_TUJUAN.".".COL_KD_TUJUAN, 'asc');
    $rtujuan = $this->db->get(TBL_SAKIP_MOPD_TUJUAN)->result_array();
    foreach($rtujuan as $t) {
        $arrIkTujuan = array();
        $iktujuan = $this->db
            ->where(COL_KD_URUSAN, $t[COL_KD_URUSAN])
            ->where(COL_KD_BIDANG, $t[COL_KD_BIDANG])
            ->where(COL_KD_UNIT, $t[COL_KD_UNIT])
            ->where(COL_KD_SUB, $t[COL_KD_SUB])

            ->where(COL_KD_PEMDA, $t[COL_KD_PEMDA])
            ->where(COL_KD_MISI, $t[COL_KD_MISI])
            ->where(COL_KD_TUJUAN, $t[COL_KD_TUJUAN])
            ->where(COL_KD_INDIKATORTUJUAN, $t[COL_KD_INDIKATORTUJUAN])
            ->where(COL_KD_SASARAN, $t[COL_KD_SASARAN])
            ->where(COL_KD_INDIKATORSASARAN, $t[COL_KD_INDIKATORSASARAN])
            ->where(COL_KD_TUJUANOPD, $t[COL_KD_TUJUANOPD])
            ->order_by(TBL_SAKIP_MOPD_IKTUJUAN.".".COL_KD_INDIKATORTUJUANOPD, 'asc')
            ->get(TBL_SAKIP_MOPD_IKTUJUAN)
            ->result_array();

        foreach($iktujuan as $ikt) {
            $arrSasaran = array();
            $sasaran = $this->db
                ->where(COL_KD_URUSAN, $ikt[COL_KD_URUSAN])
                ->where(COL_KD_BIDANG, $ikt[COL_KD_BIDANG])
                ->where(COL_KD_UNIT, $ikt[COL_KD_UNIT])
                ->where(COL_KD_SUB, $ikt[COL_KD_SUB])

                ->where(COL_KD_PEMDA, $ikt[COL_KD_PEMDA])
                ->where(COL_KD_MISI, $ikt[COL_KD_MISI])
                ->where(COL_KD_TUJUAN, $ikt[COL_KD_TUJUAN])
                ->where(COL_KD_INDIKATORTUJUAN, $ikt[COL_KD_INDIKATORTUJUAN])
                ->where(COL_KD_SASARAN, $ikt[COL_KD_SASARAN])
                ->where(COL_KD_INDIKATORSASARAN, $ikt[COL_KD_INDIKATORSASARAN])
                ->where(COL_KD_TUJUANOPD, $ikt[COL_KD_TUJUANOPD])
                ->where(COL_KD_INDIKATORTUJUANOPD, $ikt[COL_KD_INDIKATORTUJUANOPD])
                ->order_by(TBL_SAKIP_MOPD_SASARAN.".".COL_KD_SASARANOPD, 'asc')
                ->get(TBL_SAKIP_MOPD_SASARAN)
                ->result_array();

            foreach($sasaran as $s) {
                $arrIkSasaran = array();
                $iksasaran = $this->db
                    ->where(COL_KD_URUSAN, $s[COL_KD_URUSAN])
                    ->where(COL_KD_BIDANG, $s[COL_KD_BIDANG])
                    ->where(COL_KD_UNIT, $s[COL_KD_UNIT])
                    ->where(COL_KD_SUB, $s[COL_KD_SUB])

                    ->where(COL_KD_PEMDA, $s[COL_KD_PEMDA])
                    ->where(COL_KD_MISI, $s[COL_KD_MISI])
                    ->where(COL_KD_TUJUAN, $s[COL_KD_TUJUAN])
                    ->where(COL_KD_INDIKATORTUJUAN, $s[COL_KD_INDIKATORTUJUAN])
                    ->where(COL_KD_SASARAN, $s[COL_KD_SASARAN])
                    ->where(COL_KD_INDIKATORSASARAN, $s[COL_KD_INDIKATORSASARAN])
                    ->where(COL_KD_TUJUANOPD, $s[COL_KD_TUJUANOPD])
                    ->where(COL_KD_INDIKATORTUJUANOPD, $s[COL_KD_INDIKATORTUJUANOPD])
                    ->where(COL_KD_SASARANOPD, $s[COL_KD_SASARANOPD])
                    ->order_by(TBL_SAKIP_MOPD_IKSASARAN.".".COL_KD_INDIKATORSASARANOPD, 'asc')
                    ->get(TBL_SAKIP_MOPD_IKSASARAN)
                    ->result_array();
                foreach($iksasaran as $iks) {
                    $arrProgram = array();
                    $program = $this->db
                        ->where(COL_KD_URUSAN, $iks[COL_KD_URUSAN])
                        ->where(COL_KD_BIDANG, $iks[COL_KD_BIDANG])
                        ->where(COL_KD_UNIT, $iks[COL_KD_UNIT])
                        ->where(COL_KD_SUB, $iks[COL_KD_SUB])

                        ->where(COL_KD_PEMDA, $iks[COL_KD_PEMDA])
                        ->where(COL_KD_MISI, $iks[COL_KD_MISI])
                        ->where(COL_KD_TUJUAN, $iks[COL_KD_TUJUAN])
                        ->where(COL_KD_INDIKATORTUJUAN, $iks[COL_KD_INDIKATORTUJUAN])
                        ->where(COL_KD_SASARAN, $iks[COL_KD_SASARAN])
                        ->where(COL_KD_INDIKATORSASARAN, $iks[COL_KD_INDIKATORSASARAN])
                        ->where(COL_KD_TUJUANOPD, $iks[COL_KD_TUJUANOPD])
                        ->where(COL_KD_INDIKATORTUJUANOPD, $iks[COL_KD_INDIKATORTUJUANOPD])
                        ->where(COL_KD_SASARANOPD, $iks[COL_KD_SASARANOPD])
                        ->where(COL_KD_INDIKATORSASARANOPD, $iks[COL_KD_INDIKATORSASARANOPD])
                        ->where(COL_KD_TAHUN, $this->input->get(COL_KD_TAHUN))
                        ->order_by(TBL_SAKIP_DPA_PROGRAM.".".COL_KD_INDIKATORSASARANOPD, 'asc')
                        ->order_by(TBL_SAKIP_DPA_PROGRAM.".".COL_KD_PROGRAMOPD, 'asc')
                        ->get(TBL_SAKIP_DPA_PROGRAM)
                        ->result_array();

                    foreach($program as $p) {
                        $arrSasaranProgram = array();
                        $sasaranprogram = $this->db
                            ->where(COL_KD_URUSAN, $p[COL_KD_URUSAN])
                            ->where(COL_KD_BIDANG, $p[COL_KD_BIDANG])
                            ->where(COL_KD_UNIT, $p[COL_KD_UNIT])
                            ->where(COL_KD_SUB, $p[COL_KD_SUB])
                            ->where(COL_KD_BID, $p[COL_KD_BID])

                            ->where(COL_KD_PEMDA, $p[COL_KD_PEMDA])
                            ->where(COL_KD_MISI, $p[COL_KD_MISI])
                            ->where(COL_KD_TUJUAN, $p[COL_KD_TUJUAN])
                            ->where(COL_KD_INDIKATORTUJUAN, $p[COL_KD_INDIKATORTUJUAN])
                            ->where(COL_KD_SASARAN, $p[COL_KD_SASARAN])
                            ->where(COL_KD_INDIKATORSASARAN, $p[COL_KD_INDIKATORSASARAN])
                            ->where(COL_KD_TUJUANOPD, $p[COL_KD_TUJUANOPD])
                            ->where(COL_KD_INDIKATORTUJUANOPD, $iks[COL_KD_INDIKATORTUJUANOPD])
                            ->where(COL_KD_SASARANOPD, $p[COL_KD_SASARANOPD])
                            ->where(COL_KD_INDIKATORSASARANOPD, $p[COL_KD_INDIKATORSASARANOPD])
                            ->where(COL_KD_TAHUN, $p[COL_KD_TAHUN])
                            ->where(COL_KD_PROGRAMOPD, $p[COL_KD_PROGRAMOPD])
                            ->order_by(TBL_SAKIP_DPA_PROGRAM_SASARAN.".".COL_KD_SASARANPROGRAMOPD, 'asc')
                            ->get(TBL_SAKIP_DPA_PROGRAM_SASARAN)
                            ->result_array();

                        foreach($sasaranprogram as $sprog) {
                            $arrIkProgram = array();
                            $ikprogram = $this->db
                                ->where(COL_KD_URUSAN, $p[COL_KD_URUSAN])
                                ->where(COL_KD_BIDANG, $p[COL_KD_BIDANG])
                                ->where(COL_KD_UNIT, $p[COL_KD_UNIT])
                                ->where(COL_KD_SUB, $p[COL_KD_SUB])
                                ->where(COL_KD_BID, $p[COL_KD_BID])

                                ->where(COL_KD_PEMDA, $p[COL_KD_PEMDA])
                                ->where(COL_KD_MISI, $p[COL_KD_MISI])
                                ->where(COL_KD_TUJUAN, $p[COL_KD_TUJUAN])
                                ->where(COL_KD_INDIKATORTUJUAN, $p[COL_KD_INDIKATORTUJUAN])
                                ->where(COL_KD_SASARAN, $p[COL_KD_SASARAN])
                                ->where(COL_KD_INDIKATORSASARAN, $p[COL_KD_INDIKATORSASARAN])
                                ->where(COL_KD_TUJUANOPD, $p[COL_KD_TUJUANOPD])
                                ->where(COL_KD_INDIKATORTUJUANOPD, $iks[COL_KD_INDIKATORTUJUANOPD])
                                ->where(COL_KD_SASARANOPD, $p[COL_KD_SASARANOPD])
                                ->where(COL_KD_INDIKATORSASARANOPD, $p[COL_KD_INDIKATORSASARANOPD])
                                ->where(COL_KD_TAHUN, $p[COL_KD_TAHUN])
                                ->where(COL_KD_PROGRAMOPD, $p[COL_KD_PROGRAMOPD])
                                ->where(COL_KD_SASARANPROGRAMOPD, $sprog[COL_KD_SASARANPROGRAMOPD])
                                ->order_by(TBL_SAKIP_DPA_PROGRAM_INDIKATOR.".".COL_KD_INDIKATORPROGRAMOPD, 'asc')
                                ->get(TBL_SAKIP_DPA_PROGRAM_INDIKATOR)
                                ->result_array();
                            foreach($ikprogram as $ikp) {
                                $arrIkProgram[] = array(
                                    "text" => array("name"=> "Indikator Program", "title"=> $ikp[COL_NM_INDIKATORPROGRAMOPD]),
                                    "connectors" => array("style" => array("stroke" => "#000", "arrow-end" => "block-wide-long")),
                                    //"children" => $arrProgramChild,
                                    "HTMLclass" => "bg-red"
                                );
                            }

                            $arrKegiatan = array();
                            $kegiatan = $this->db
                                ->where(COL_KD_URUSAN, $p[COL_KD_URUSAN])
                                ->where(COL_KD_BIDANG, $p[COL_KD_BIDANG])
                                ->where(COL_KD_UNIT, $p[COL_KD_UNIT])
                                ->where(COL_KD_SUB, $p[COL_KD_SUB])
                                ->where(COL_KD_BID, $p[COL_KD_BID])

                                ->where(COL_KD_PEMDA, $p[COL_KD_PEMDA])
                                ->where(COL_KD_MISI, $p[COL_KD_MISI])
                                ->where(COL_KD_TUJUAN, $p[COL_KD_TUJUAN])
                                ->where(COL_KD_INDIKATORTUJUAN, $p[COL_KD_INDIKATORTUJUAN])
                                ->where(COL_KD_SASARAN, $p[COL_KD_SASARAN])
                                ->where(COL_KD_INDIKATORSASARAN, $p[COL_KD_INDIKATORSASARAN])
                                ->where(COL_KD_TUJUANOPD, $p[COL_KD_TUJUANOPD])
                                ->where(COL_KD_INDIKATORTUJUANOPD, $iks[COL_KD_INDIKATORTUJUANOPD])
                                ->where(COL_KD_SASARANOPD, $p[COL_KD_SASARANOPD])
                                ->where(COL_KD_INDIKATORSASARANOPD, $p[COL_KD_INDIKATORSASARANOPD])
                                ->where(COL_KD_TAHUN, $p[COL_KD_TAHUN])
                                ->where(COL_KD_PROGRAMOPD, $p[COL_KD_PROGRAMOPD])
                                ->order_by(TBL_SAKIP_DPA_KEGIATAN.".".COL_KD_KEGIATANOPD, 'asc')
                                ->get(TBL_SAKIP_DPA_KEGIATAN)
                                ->result_array();
                            foreach($kegiatan as $keg) {
                                $nmKeg = $keg[COL_NM_KEGIATANOPD];

                                $arrSasaranKegiatan = array();
                                $kegiatan_sasaran = $this->db
                                    ->where(COL_KD_URUSAN, $keg[COL_KD_URUSAN])
                                    ->where(COL_KD_BIDANG, $keg[COL_KD_BIDANG])
                                    ->where(COL_KD_UNIT, $keg[COL_KD_UNIT])
                                    ->where(COL_KD_SUB, $keg[COL_KD_SUB])
                                    ->where(COL_KD_BID, $keg[COL_KD_BID])
                                    ->where(COL_KD_SUBBID, $keg[COL_KD_SUBBID])

                                    ->where(COL_KD_PEMDA, $keg[COL_KD_PEMDA])
                                    ->where(COL_KD_MISI, $keg[COL_KD_MISI])
                                    ->where(COL_KD_TUJUAN, $keg[COL_KD_TUJUAN])
                                    ->where(COL_KD_INDIKATORTUJUAN, $keg[COL_KD_INDIKATORTUJUAN])
                                    ->where(COL_KD_SASARAN, $keg[COL_KD_SASARAN])
                                    ->where(COL_KD_INDIKATORSASARAN, $keg[COL_KD_INDIKATORSASARAN])
                                    ->where(COL_KD_TUJUANOPD, $keg[COL_KD_TUJUANOPD])
                                    ->where(COL_KD_INDIKATORTUJUANOPD, $keg[COL_KD_INDIKATORTUJUANOPD])
                                    ->where(COL_KD_SASARANOPD, $keg[COL_KD_SASARANOPD])
                                    ->where(COL_KD_INDIKATORSASARANOPD, $keg[COL_KD_INDIKATORSASARANOPD])
                                    ->where(COL_KD_TAHUN, $keg[COL_KD_TAHUN])
                                    ->where(COL_KD_PROGRAMOPD, $keg[COL_KD_PROGRAMOPD])
                                    ->where(COL_KD_SASARANPROGRAMOPD, $keg[COL_KD_SASARANPROGRAMOPD])
                                    ->where(COL_KD_KEGIATANOPD, $keg[COL_KD_KEGIATANOPD])
                                    ->order_by(TBL_SAKIP_DPA_KEGIATAN_SASARAN.".".COL_KD_SASARANKEGIATANOPD, 'asc')
                                    ->get(TBL_SAKIP_DPA_KEGIATAN_SASARAN)
                                    ->result_array();
                                foreach($kegiatan_sasaran as $skeg) {
                                    $arrIKKegiatan = array();
                                    $kegiatan_indikator = $this->db
                                        ->where(COL_KD_URUSAN, $skeg[COL_KD_URUSAN])
                                        ->where(COL_KD_BIDANG, $skeg[COL_KD_BIDANG])
                                        ->where(COL_KD_UNIT, $skeg[COL_KD_UNIT])
                                        ->where(COL_KD_SUB, $skeg[COL_KD_SUB])
                                        ->where(COL_KD_BID, $skeg[COL_KD_BID])
                                        ->where(COL_KD_SUBBID, $skeg[COL_KD_SUBBID])

                                        ->where(COL_KD_PEMDA, $skeg[COL_KD_PEMDA])
                                        ->where(COL_KD_MISI, $skeg[COL_KD_MISI])
                                        ->where(COL_KD_TUJUAN, $skeg[COL_KD_TUJUAN])
                                        ->where(COL_KD_INDIKATORTUJUAN, $skeg[COL_KD_INDIKATORTUJUAN])
                                        ->where(COL_KD_SASARAN, $skeg[COL_KD_SASARAN])
                                        ->where(COL_KD_INDIKATORSASARAN, $skeg[COL_KD_INDIKATORSASARAN])
                                        ->where(COL_KD_TUJUANOPD, $skeg[COL_KD_TUJUANOPD])
                                        ->where(COL_KD_INDIKATORTUJUANOPD, $skeg[COL_KD_INDIKATORTUJUANOPD])
                                        ->where(COL_KD_SASARANOPD, $skeg[COL_KD_SASARANOPD])
                                        ->where(COL_KD_INDIKATORSASARANOPD, $skeg[COL_KD_INDIKATORSASARANOPD])
                                        ->where(COL_KD_TAHUN, $skeg[COL_KD_TAHUN])
                                        ->where(COL_KD_PROGRAMOPD, $skeg[COL_KD_PROGRAMOPD])
                                        ->where(COL_KD_SASARANPROGRAMOPD, $skeg[COL_KD_SASARANPROGRAMOPD])
                                        ->where(COL_KD_KEGIATANOPD, $skeg[COL_KD_KEGIATANOPD])
                                        ->where(COL_KD_SASARANKEGIATANOPD, $skeg[COL_KD_SASARANKEGIATANOPD])
                                        ->order_by(TBL_SAKIP_DPA_KEGIATAN_INDIKATOR.".".COL_KD_INDIKATORKEGIATANOPD, 'asc')
                                        ->get(TBL_SAKIP_DPA_KEGIATAN_INDIKATOR)
                                        ->result_array();

                                    foreach($kegiatan_indikator as $ikeg) {
                                        $arrIKKegiatan[] = array(
                                            "text" => array("name"=> "Indikator Kegiatan", "title"=> $ikeg[COL_NM_INDIKATORKEGIATANOPD]),
                                            "connectors" => array("style" => array("stroke" => "#000", "arrow-end" => "block-wide-long")),
                                            //"children" => $arrProgramChild,
                                            "HTMLclass" => "bg-white"
                                        );
                                    }

                                    $arrSasaranKegiatan[] = array(
                                        "text" => array("name"=> "Sasaran Kegiatan", "title"=> $skeg[COL_NM_SASARANKEGIATANOPD]),
                                        "connectors" => array("style" => array("stroke" => "#000", "arrow-end" => "block-wide-long")),
                                        "children" => $arrIKKegiatan,
                                        "HTMLclass" => "bg-gray"
                                    );
                                }
                                $nmKeg = $keg[COL_NM_KEGIATANOPD] ? $keg[COL_NM_KEGIATANOPD] : "-";

                                $arrKegiatan[] = array(
                                    "text" => array("name"=> "Kegiatan", "title"=> $nmKeg),
                                    "connectors" => array("style" => array("stroke" => "#000", "arrow-end" => "block-wide-long")),
                                    "HTMLclass" => "bg-fuchsia",
                                    "children" => $arrSasaranKegiatan /*array(
                                    array(
                                        "text" => array("name"=> "Sasaran Kegiatan", "title"=> $keg[COL_NM_SASARANKEGIATANOPD]),
                                        "connectors" => array("style" => array("stroke" => "#000", "arrow-end" => "block-wide-long")),
                                        "HTMLclass" => "bg-gray",
                                        "children" => array(
                                            array(
                                                "text" => array("name"=> "Indikator Kegiatan", "title"=> $keg[COL_NM_INDIKATORKEGIATANOPD]),
                                                "connectors" => array("style" => array("stroke" => "#000", "arrow-end" => "block-wide-long")),
                                                "HTMLclass" => "bg-silver"
                                                //"children" => $arrProgramChild
                                            )
                                        )
                                    )
                                )*/
                                );
                            }
                            $arrKegiatanTemp = array(
                                "pseudo" => true,
                                "connectors" => array("style" => array("stroke" => "#000", "arrow-end" => "block-wide-long")),
                                "children" => $arrKegiatan
                            );

                            if(count($arrKegiatan) > 0) {
                                $arrIkProgram[] = $arrKegiatanTemp;
                                //$arrIkProgram = array_merge($arrIkProgram, $arrKegiatan);
                            }


                            $arrSasaranProgram[] = array(
                                "text" => array("name"=> "Sasaran Program", "title"=> $sprog[COL_NM_SASARANPROGRAMOPD]),
                                "connectors" => array("style" => array("stroke" => "#000", "arrow-end" => "block-wide-long")),
                                "children" => $arrIkProgram,
                                "HTMLclass" => "bg-greenlight"
                            );
                        }

                        $arrProgram[] = array(
                            "text" => array("name"=> "Program", "title"=> $p[COL_NM_PROGRAMOPD]),
                            "connectors" => array("style" => array("stroke" => "#000", "arrow-end" => "block-wide-long")),
                            "children" => $arrSasaranProgram,
                            "HTMLclass" => "bg-yellow"
                        );
                        //echo json_encode($arrIkProgram);
                        //return;
                    }

                    $arrIkSasaran[] = array(
                        "text" => array("name"=> $iks[COL_KD_MISI].".".$iks[COL_KD_TUJUANOPD].".".$iks[COL_KD_INDIKATORTUJUANOPD].".".$iks[COL_KD_SASARANOPD].".".$iks[COL_KD_INDIKATORSASARANOPD].". Indikator Sasaran Strategis", "title"=> $iks[COL_NM_INDIKATORSASARANOPD]),
                        "connectors" => array("style" => array("stroke" => "#000", "arrow-end" => "block-wide-long")),
                        "children" => $arrProgram,
                        "HTMLclass" => "bg-orange"
                    );
                }

                $arrSasaran[] = array(
                    "text" => array("name"=> $s[COL_KD_MISI].".".$s[COL_KD_TUJUANOPD].".".$s[COL_KD_INDIKATORTUJUANOPD].".".$s[COL_KD_SASARANOPD].". Sasaran Strategis", "title"=> $s[COL_NM_SASARANOPD]),
                    "connectors" => array("style" => array("stroke" => "#000", "arrow-end" => "block-wide-long")),
                    "children" => $arrIkSasaran,
                    "HTMLclass" => "bg-green"
                );
            }

            $arrIkTujuan[] = array(
                "text" => array("name"=> $ikt[COL_KD_MISI].".".$ikt[COL_KD_TUJUANOPD].".".$ikt[COL_KD_INDIKATORTUJUANOPD].". Indikator Tujuan", "title"=> $ikt[COL_NM_INDIKATORTUJUANOPD]),
                "connectors" => array("style" => array("stroke" => "#000", "arrow-end" => "block-wide-long")),
                "children" => $arrSasaran,
                "HTMLclass" => "bg-lime"
            );
        }

        $arrTujuan[] = array(
            "text" => array("name"=> $t[COL_KD_MISI].".".$t[COL_KD_TUJUANOPD].". Tujuan", "title"=> $t[COL_NM_TUJUANOPD]),
            "connectors" => array("style" => array("stroke" => "#000", "arrow-end" => "block-wide-long")),
            "children" => $arrIkTujuan,
            "HTMLclass" => "bg-teal"
        );
    }

    $arrMisi[] = array(
        "text" => array("name"=> $m[COL_KD_MISI].". Misi", "title"=> $m[COL_NM_MISI]),
        "connectors" => array("style" => array("stroke" => "#000", "arrow-end" => "block-wide-long")),
        "children" => $arrTujuan,
        "HTMLclass" => "bg-aqua"
    );
}
$nodes = array(
    "text" => array("name"=> "Visi", "title"=> $rpemda[COL_NM_VISI]),
    "connectors" => array("style" => array("stroke" => "#000", "arrow-end" => "block-wide-long")),
    "children" => $arrMisi,
    "HTMLclass" => "bg-primary"
);
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?=!empty($title) ? 'E-SAKIP | '.$title : SITENAME?></title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

    <!-- JQUERY -->
    <script src="<?=base_url()?>assets/adminlte/plugins/jQuery/jquery-2.2.3.min.js"></script>

    <script type="text/javascript" src="<?=base_url()?>assets/treant/vendor/raphael.js"></script>
    <script type="text/javascript" src="<?=base_url()?>assets/treant/Treant.js"></script>
    <link href="<?=base_url()?>assets/treant/Treant.css" rel="stylesheet" type="text/css" />

    <link rel="stylesheet" href="<?=base_url()?>assets/treant/vendor/perfect-scrollbar/perfect-scrollbar.css">
    <link rel="stylesheet" href="<?=base_url()?>assets/tbs/css/font-awesome.min.css" />
    <link rel="stylesheet" href="<?=base_url()?>assets/adminlte/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?=base_url()?>assets/adminlte/dist/css/AdminLTE.min.css">
    <link rel="stylesheet" href="<?=base_url()?>assets/adminlte/dist/css/skins/_all-skins.min.css">
    <script type="text/javascript" src="<?=base_url()?>assets/treant/vendor/jquery.mousewheel.js"></script>
    <script type="text/javascript" src="<?=base_url()?>assets/treant/vendor/perfect-scrollbar/perfect-scrollbar.js"></script>
</head>
<body>
<style>
    .nodeExample1 {
        border: 1px solid #000;
        padding : 0px !important;
        width : 12.5vh !important;
        font-size: 8pt;
        color: #000 !important;
     }
    .nodeExample1 .node-name {
        font-weight: bold;
        margin: 0 0 5px !important;
        border-bottom: 1px solid #000;
        padding: 2px;
    }
    .nodeExample1 .node-title {
        #text-align: justify;
        padding: 2px;
    }
    .chart {
        overflow: auto;
    }
    .bg-greenlight {
        background-color: #c4dd39 !important;
    }
</style>
<a id="btn-download" download="Pohon Kinerja.jpg" href="">Download</a>
<div id="chart">
    <h4 style="text-align: center">POHON KINERJA <?=strtoupper($nmSub)?> KOTA TEBING TINGGI</h4><hr />
    <div class="chart" id="basic-example">

    </div>
</div>
<div id="canvas" style="display: none">

</div>
<script src="<?=base_url()?>assets/js/html2canvas.min.js"></script>
<script>
    console.log(<?=json_encode($nodes)?>);
    var chart_config = {
        chart: {
            container: "#basic-example",
            scrollbar: "fancy",
            //animateOnInit: true,
            rootOrientation:  'WEST', // NORTH || EAST || WEST || SOUTH
            connectors: {
                type: "step",
                style: {
                    "stroke-width": 1
                }
            },
            node: {
                HTMLclass: 'nodeExample1'
            },
            nodeAlign: 'TOP',
            padding: 10
        },
        nodeStructure: <?=json_encode($nodes)?>
    };
    new Treant( chart_config );
    html2canvas(document.querySelector("#chart"), {scale: 1.75}).then(canvas => {
        document.getElementById("canvas").appendChild(canvas);
        var img = canvas.toDataURL("image/jpg");
        $("#btn-download").attr("href", img);
    });
</script>
</body>
