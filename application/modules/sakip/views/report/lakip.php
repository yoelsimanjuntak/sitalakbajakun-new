<?php
/**
 * Created by PhpStorm.
 * User: PTI
 * Date: 8/9/2019
 * Time: 5:00 PM
 */
$this->load->view('header');
$ruser = GetLoggedUser();
?>
    <section class="content-header">
        <h1> <?= $title ?> <small> Generate</small></h1>
        <ol class="breadcrumb">
            <li><a href="<?=site_url()?>"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">LAKIP</li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-sm-12">
                <div class="box box-primary" style="border-top-color: transparent">
                    <div class="box-body">
                        <?=form_open(current_url(),array('role'=>'form','id'=>'main-form', 'class'=>'form-horizontal'))?>
                        <div class="form-group">
                            <label class="control-label col-sm-2">OPD</label>
                            <div class="col-sm-8">
                                <div class="input-group">
                                    <input type="text" class="form-control" name="text-opd" value="<?= $edit ? $data[COL_KD_URUSAN].".".$data[COL_KD_BIDANG].".".$data[COL_KD_UNIT].".".$data[COL_KD_SUB]." ".$nmSub : ($ruser[COL_ROLEID] == ROLEKADIS || $ruser[COL_ROLEID] == ROLEKEUANGAN ? $strOPD[0].".".$strOPD[1].".".$strOPD[2].".".$strOPD[3]." ".$nmSub : "")?>" readonly>
                                    <input type="hidden" name="<?=COL_KD_URUSAN?>" value="<?= $edit ? $data[COL_KD_URUSAN] : ($ruser[COL_ROLEID]==ROLEKADIS || $ruser[COL_ROLEID] == ROLEKEUANGAN?$strOPD[0]:"")?>" required   >
                                    <input type="hidden" name="<?=COL_KD_BIDANG?>" value="<?= $edit ? $data[COL_KD_BIDANG] : ($ruser[COL_ROLEID]==ROLEKADIS || $ruser[COL_ROLEID] == ROLEKEUANGAN?$strOPD[1]:"")?>" required   >
                                    <input type="hidden" name="<?=COL_KD_UNIT?>" value="<?= $edit ? $data[COL_KD_UNIT] : ($ruser[COL_ROLEID]==ROLEKADIS || $ruser[COL_ROLEID] == ROLEKEUANGAN?$strOPD[2]:"")?>" required   >
                                    <input type="hidden" name="<?=COL_KD_SUB?>" value="<?= $edit ? $data[COL_KD_SUB] : ($ruser[COL_ROLEID]==ROLEKADIS || $ruser[COL_ROLEID] == ROLEKEUANGAN?$strOPD[3]:"")?>" required   >
                                    <div class="input-group-btn">
                                        <button type="button" class="btn btn-default btn-flat btn-browse-opd" data-toggle="modal" data-target="#browseOPD" data-toggle="tooltip" data-placement="top" title="Pilih OPD" <?=($ruser[COL_ROLEID] == ROLEKADIS ? "disabled" : "")?>><i class="fa fa-ellipsis-h"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2">Tahun</label>
                            <div class="col-sm-2">
                                <input type="number" class="form-control" name="<?=COL_KD_TAHUN?>" value="<?= $edit ? $data[COL_KD_TAHUN] : date("Y")?>" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2">[1.2]<br />FUNGSI DAN TUGAS OPD</label>
                            <div class="col-sm-8">
                                <textarea name="12_narasi" class="form-control" rows="5" placeholder="Diisi dengan narasi tentang Fungsi dan Tugas OPD sesuai Peraturan Walikota"></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2">[1.3]<br />ISU-ISU STRATEGIS</label>
                            <div class="col-sm-8">
                                <textarea name="13_narasi" class="form-control" rows="5" placeholder="Diisi dengan narasi tentang isu strategis di RENSTRA OPD"></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2">[1.4]<br />KEADAAN PEGAWAI</label>
                            <div class="col-sm-8">
                                <textarea name="14_narasi" class="form-control" rows="5" placeholder="Diisi dengan narasi yang menjelaskan komposisi pegawai berdasarkan Jenis Kelamin, Pendidikan, Golongan, Jabatan Struktural, Jabatan Fungsional, Jabatan Pelaksana"></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2">[1.5]<br />KEADAAN SARANA DAN PRASARANA</label>
                            <div class="col-sm-8">
                                <textarea name="15_narasi" class="form-control" rows="5" placeholder="Diisi dengan narasi tentang sarana dan prasarana yang ada di OPD"></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2">[1.6]<br />KEUANGAN</label>
                            <div class="col-sm-8">
                                <textarea name="16_narasi" class="form-control" rows="5" placeholder="Diisi dengan narasi tentang APBD Tahun 2020 di OPD (DPA Awal, DPA Pergeseran Terakhir dan DPA Perubahan)"></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2">[2.1]<br />PERENCANAAN STRATEGIS</label>
                            <div class="col-sm-8">
                                <textarea name="21_narasi" class="form-control" rows="5" placeholder="Diisi dengan narasi yang menjelaskan tentang RENSTRA OPD"></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2">[2.1.1]<br />VISI DAN MISI KEPALA DAERAH</label>
                            <div class="col-sm-8">
                                <textarea name="211_narasi" class="form-control" rows="5" placeholder="Diisi dengan narasi tentang Visi dan Misi Kepala Daerah"></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2">[2.1.2]<br />TUJUAN DAN SASARAN PERANGKAT DAERAH</label>
                            <div class="col-sm-8">
                                <textarea name="212_narasi" class="form-control" rows="5" placeholder="Diisi dengan narasi tentang tujuan dan sasaran OPD"></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2">[2.1.3]<br />STRATEGI, PROGRAM, DAN KEGIATAN</label>
                            <div class="col-sm-8">
                                <textarea name="213_narasi" class="form-control" rows="5" placeholder="Diisi dengan narasi tentang sasaran strategis, program dan kegiatan yang ada di OPD"></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2">[2.3]<br />RENCANA ANGGARAN</label>
                            <div class="col-sm-8">
                                <textarea name="23_narasi" class="form-control" rows="5" placeholder="Diisi dengan narasi menjelaskan tentang tentang APBD di OPD (DPA Awal, DPA Pergeseran Terakhir dan DPA Perubahan) dengan rincian Belanja Langsung dan Belanja Tidak Langsung."></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2">[3.1]<br />CAPAIAN KINERJA</label>
                            <div class="col-sm-8">
                                <textarea name="31_narasi" class="form-control" rows="5" placeholder="Diisi dengan narasi tentang capaian kinerja yang telah dilaksanakan tahun berjalan"></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2">[3.2]<br />EVALUASI DAN ANALISIS CAPAIAN KINERJA SASARAN STRATEGIS</label>
                            <div class="col-sm-8">
                                <textarea name="32_narasi" class="form-control" rows="5" placeholder="Diisi dengan narasi tentang rumusan indikator dan formulasi perhitungan beserta target dan realisasi kinerja tahun berjalan"></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2">[3.3]<br />REALISASI ANGGARAN</label>
                            <div class="col-sm-8">
                                <textarea name="33_narasi" class="form-control" rows="5" placeholder="Diisi dengan narasi tentang penyerapan anggaran pada tahun berjalan terhadap total anggaran yang telah dialokasikan di OPD"></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2">[3.4]<br />INOVASI</label>
                            <div class="col-sm-8">
                                <textarea name="34_narasi" class="form-control" rows="5" placeholder="Diisi dengan narasi tentang penemuan hal-hal baru atau proses kreatif terhadap sesuatu yang sudah ada maupun yang belum ada sebelumnya"></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2">[4]<br />PENUTUP</label>
                            <div class="col-sm-8">
                                <textarea name="4_narasi" class="form-control" rows="5" placeholder="Pada bagian ini dikemukakan simpulan secara umum tentang keberhasilan/kegagalan, permasalahan dan kendala utama yang berkaitan dengan kinerja instansi yang bersangkutan serta strategi pemecahan masalah"></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                          <div class="col-sm-12" style="text-align: center">
                            <button type="submit" class="btn btn-primary btn-flat" title="Lihat"><i class="fa fa-arrow-circle-right"></i> GENERATE</button>
                          </div>
                        </div>
                        <?=form_close()?>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <div class="modal fade" id="browseOPD" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Browse</h4>
                </div>
                <div class="modal-body">
                    ...
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default btn-flat pull-right" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    </div>
<?php $this->load->view('loadjs') ?>
<script>
$(document).ready(function() {
  $('.modal').on('hidden.bs.modal', function (event) {
      $(this).find(".modal-body").empty();
  });

  $('#browseOPD').on('show.bs.modal', function (event) {
    var modalBody = $(".modal-body", $("#browseOPD"));
    $(this).removeData('bs.modal');
    modalBody.html("<p style='font-style: italic'>Loading..</p>");
    modalBody.load("<?=site_url("ajax/browse-opd")?>", function () {
      $("[name=selID][type=hidden]", modalBody).unbind().change(function () {
        var kdSub = $(this).val().split('|');
        $("[name=Kd_Urusan]").val(kdSub[0]);
        $("[name=Kd_Bidang]").val(kdSub[1]);
        $("[name=Kd_Unit]").val(kdSub[2]);
        $("[name=Kd_Sub]").val(kdSub[3]);
      });
      $("[name=selText][type=hidden]", modalBody).unbind().change(function () {
        $("[name=text-opd]").val($(this).val());
      });
    });
  });

  $('[name=Kd_Tahun]').change(function() {
    var kdUrusan = $('[name=Kd_Urusan]').val();
    var kdBidang = $('[name=Kd_Bidang]').val();
    var KdUnit = $('[name=Kd_Unit]').val();
    var kdSub = $('[name=Kd_Sub]').val();
    var kdTahun = $('[name=Kd_Tahun]').val();

    $.post("<?=site_url('sakip/ajax/get-lakip-history')?>", { Kd_Urusan: kdUrusan, Kd_Bidang: kdBidang, Kd_Unit: KdUnit, Kd_Sub: kdSub, Kd_Tahun: kdTahun }, function(resp) {
      var formLakip = JSON.parse(resp.Nm_FormLakip);
      $.each(formLakip, function(i, val){
        $("[name="+i+"]").val(val);
      });
    }, "json");
  }).trigger('change');
});
</script>
<?php $this->load->view('footer') ?>
