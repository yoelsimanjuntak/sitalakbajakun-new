<?php
/**
 * Created by PhpStorm.
 * User: Partopi Tao
 * Date: 3/4/2019
 * Time: 7:39 PM
 */
$data = array();
$i = 0;
foreach ($res as $d) {
  $nmSub = "";
  $eplandb = $this->load->database("eplan", true);
  $eplandb->where(COL_KD_URUSAN, $d[COL_KD_URUSAN]);
  $eplandb->where(COL_KD_BIDANG, $d[COL_KD_BIDANG]);
  $eplandb->where(COL_KD_UNIT, $d[COL_KD_UNIT]);
  $eplandb->where(COL_KD_SUB, $d[COL_KD_SUB]);
  $subunit = $eplandb->get("ref_sub_unit")->row_array();
  if($subunit) {
      $nmSub = $subunit["Nm_Sub_Unit"];
  }

  $htmlUnit = "";
  $htmlUnit .= "<table>";
  $htmlUnit .= "<tr>";
  $htmlUnit .= "<td style='text-align: right; font-weight: bold'>OPD</td><td style='padding: 0px 3px'>:</td><td>".$nmSub."</td>";
  $htmlUnit .= "</tr>";
  $htmlUnit .= "<tr>";
  $htmlUnit .= "<td style='text-align: right; font-weight: bold'>Bidang</td><td style='padding: 0px 3px'>:</td><td>".$d[COL_NM_BID]."</td>";
  $htmlUnit .= "</tr>";
  $htmlUnit .= "<tr>";
  $htmlUnit .= "<td style='text-align: right; font-weight: bold'>Sub Bidang</td><td style='padding: 0px 3px'>:</td><td>".$d[COL_NM_SUBBID]."</td>";
  $htmlUnit .= "</tr>";
  $htmlUnit .= "</table>";

  $htmlIKU = "";
  $htmlIKU .= "<table>";
  $htmlIKU .= "<tr>";
  $htmlIKU .= "<td style='text-align: right; font-weight: bold'>Tahun</td><td style='padding: 0px 3px'>:</td><td>".$d[COL_KD_TAHUN]."</td>";
  $htmlIKU .= "</tr>";
  $htmlIKU .= "<tr>";
  $htmlIKU .= "<td style='text-align: right; font-weight: bold'>Sasaran OPD</td><td style='padding: 0px 3px'>:</td><td>".$d[COL_KD_TUJUANOPD].'.'.$d[COL_KD_INDIKATORTUJUANOPD].'.'.$d[COL_KD_SASARANOPD].' '.$d[COL_NM_SASARANOPD]."</td>";
  $htmlIKU .= "</tr>";
  $htmlIKU .= "<tr>";
  $htmlIKU .= "<td style='text-align: right; font-weight: bold'>Ik. Sasaran OPD</td><td style='padding: 0px 3px'>:</td><td>".$d[COL_KD_TUJUANOPD].'.'.$d[COL_KD_INDIKATORTUJUANOPD].'.'.$d[COL_KD_SASARANOPD].'.'.$d[COL_KD_INDIKATORSASARANOPD].' '.$d[COL_NM_INDIKATORSASARANOPD]."</td>";
  $htmlIKU .= "</tr>";
  $htmlIKU .= "<tr>";
  $htmlIKU .= "<td style='text-align: right; font-weight: bold'>Sasaran Es. III</td><td style='padding: 0px 3px'>:</td><td>".$d[COL_KD_TUJUANOPD].'.'.$d[COL_KD_INDIKATORTUJUANOPD].'.'.$d[COL_KD_SASARANOPD].'.'.$d[COL_KD_INDIKATORSASARANOPD].'.'.$d[COL_KD_SASARANPROGRAMOPD].' '.$d[COL_NM_SASARANPROGRAMOPD]."</td>";
  $htmlIKU .= "</tr>";
  $htmlIKU .= "<tr>";
  $htmlIKU .= "<td style='text-align: right; font-weight: bold'>Ik. Sasaran Es. III</td><td style='padding: 0px 3px'>:</td><td>".$d[COL_KD_TUJUANOPD].'.'.$d[COL_KD_INDIKATORTUJUANOPD].'.'.$d[COL_KD_SASARANOPD].'.'.$d[COL_KD_INDIKATORSASARANOPD].'.'.$d[COL_KD_SASARANPROGRAMOPD].'.'.$d[COL_KD_INDIKATORPROGRAMOPD].' '.$d[COL_NM_INDIKATORPROGRAMOPD]."</td>";
  $htmlIKU .= "</tr>";
  $htmlIKU .= "<tr>";
  $htmlIKU .= "<td style='text-align: right; font-weight: bold'>Sasaran Es. IV</td><td style='padding: 0px 3px'>:</td><td>".$d[COL_KD_TUJUANOPD].'.'.$d[COL_KD_INDIKATORTUJUANOPD].'.'.$d[COL_KD_SASARANOPD].'.'.$d[COL_KD_INDIKATORSASARANOPD].'.'.$d[COL_KD_INDIKATORPROGRAMOPD].'.'.$d[COL_KD_SASARANSUBBIDANG].' '.$d[COL_NM_SASARANSUBBIDANG]."</td>";
  $htmlIKU .= "</tr>";
  $htmlIKU .= "<tr>";
  $htmlIKU .= "<td style='text-align: right; font-weight: bold'>Jabatan</td><td style='padding: 0px 3px'>:</td><td>".anchor('msubbid/sasaran-individu-edit/'.$d["ID"],$d[COL_NM_JABATAN])."</td>";
  $htmlIKU .= "</tr>";
  $htmlIKU .= "</table>";

  $res[$i] = array(
    '<input type="checkbox" class="cekbox" name="cekbox[]" value="' . $d["ID"] . '" />',
    $htmlIKU,
    $htmlUnit
  );
  $i++;
}
$data = json_encode($res);
?>

<?php $this->load->view('header')?>
<section class="content-header">
  <h1><?= $title ?>  <small>Data</small></h1>
  <ol class="breadcrumb">
    <li>
      <a href="<?=site_url()?>"><i class="fa fa-dashboard"></i> Home</a>
    </li>
    <li class="active">
      Sasaran Sub Bidang OPD
    </li>
  </ol>
</section>
<section class="content">
  <p>
    <?=anchor('msubbid/sasaran-individu-del','<i class="far fa-trash"></i> Hapus',array('class'=>'cekboxaction btn btn-sm btn-danger','confirm'=>'Apa anda yakin?'))?>
    <?=anchor('msubbid/sasaran-individu-add','<i class="far fa-plus"></i> Data Baru',array('class'=>'btn btn-sm btn-primary'))?>
  </p>
  <div class="box box-default">
    <div class="box-body">
      <form id="dataform" method="post" action="#">
        <table id="datalist" class="table table-bordered table-hover">
        </table>
      </form>
    </div>
  </div>
</section>
<?php $this->load->view('loadjs')?>
<script type="text/javascript">
$(document).ready(function() {
  var dataTable = $('#datalist').dataTable({
    "autoWidth":false,
    //"sDom": "Rlfrtip",
    "aaData": <?=$data?>,
    //"bJQueryUI": true,
    //"aaSorting" : [[5,'desc']],
    "scrollY" : 400,
    //"scrollX": "200%",
    "scrollX": true,
    "iDisplayLength": 100,
    "aLengthMenu": [[100, 1000, 5000, -1], [100, 1000, 5000, "Semua"]],
    "dom":"R<'row'<'col-sm-4'l><'col-sm-4'B><'col-sm-4'f>><'row'<'col-sm-12'tr>><'row'<'col-sm-5'i><'col-sm-7'p>>",
    "buttons": ['copyHtml5','excelHtml5','csvHtml5','pdfHtml5'],
    "order": [[1,"asc"]],
    "columnDefs": [
      {"targets":[0],"className":"text-center"}
    ],
    "aoColumns": [
      {"sTitle": "<input type=\"checkbox\" id=\"cekbox\" class=\"\" />","sWidth":15,bSortable:false},
      {"sTitle": "Sasaran"},
      {"sTitle": "Unit Kerja"}
    ]
  });
  $('#cekbox').click(function(){
    if($(this).is(':checked')){
      $('.cekbox').prop('checked',true);
    }else{
      $('.cekbox').prop('checked',false);
    }
  });
});
</script>
<?php $this->load->view('footer')?>
