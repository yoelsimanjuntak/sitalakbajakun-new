<?php $this->load->view('header') ?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Dashboard <small></small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?=site_url()?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
    </ol>
</section>
<?php
$ruser = GetLoggedUser();
//$dusun = $this->db->count_all_results(TBL_MDUSUN);
//$dasawisma = $this->db->count_all_results(TBL_MDASAWISMA);
//$keluarga = $this->db->count_all_results(TBL_MKELUARGA);
?>
<style>
    .todo-list>li:hover {
        background-color: #ccc;
    }
</style>
<!-- Main content -->
<section class="content">
    <!-- Main row -->
    <div class="row">
        <div class="col-sm-12" style="display: none">
            <div class="box box-solid">
                <div class="box-header with-border">
                    <marquee>
                        <h3 class="box-title text-orange" style="font-weight: bold;">Selamat Datang di Sistem Akuntabilitas Kinerja Instansi Pemerintah Berbasis Elektronik (E-SAKIP) KOTA TEBING TINGGI.</h3>
                    </marquee>
                </div>
            </div>
        </div>

        <div class="col-sm-8">
            <?php
            $this->db->where(COL_KD_TAHUN_FROM." <=", date("Y"));
            $this->db->where(COL_KD_TAHUN_TO." >=", date("Y"));
            $this->db->order_by(COL_KD_TAHUN_FROM, "desc");
            $rperiod = $this->db->get(TBL_SAKIP_MPEMDA)->row_array();
            ?>
            <div class="box box-success">
                <div class="box-header with-border">
                    <h3 class="box-title">SELAMAT DATANG <i><?=$ruser?strtoupper($ruser[COL_NAME]):"GUEST"?></i> !</h3>
                </div>
                <div class="box-body">
                    <table class="table table-striped">
                        <tbody>
                        <tr>
                            <td style="width: 20%">PERIODE</td>
                            <td style="width: 1%">:</td>
                            <td><b><?=$rperiod?$rperiod[COL_KD_TAHUN_FROM]." s.d ".$rperiod[COL_KD_TAHUN_TO]:"-"?></b></td>
                        </tr>
                        <tr>
                            <td style="width: 20%">KEPALA DAERAH</td>
                            <td style="width: 1%">:</td>
                            <td><b><?=$rperiod?strtoupper($rperiod[COL_NM_PEJABAT]):"-"?></b></td>
                        </tr>
                        <tr>
                            <td style="width: 20%">VISI</td>
                            <td style="width: 1%">:</td>
                            <td><b><?=$rperiod?strtoupper($rperiod[COL_NM_VISI]):"-"?></b></td>
                        </tr>
                        <tr>
                            <td style="width: 20%">MISI</td>
                            <td style="width: 1%">:</td>
                            <td></td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <div class="box-footer">
                    <ul class="todo-list ui-sortable">
                        <?php
                        if($rperiod) {
                            $rmisi = $this->db->where(COL_KD_PEMDA, $rperiod[COL_KD_PEMDA])->order_by(COL_KD_MISI, "asc")->get(TBL_SAKIP_MPMD_MISI)->result_array();
                            $i = 1;
                            foreach($rmisi as $m) {
                                ?>
                                <li>
                                    <span class="badge <?=$i%2==0?"bg-yellow":"bg-green"?>"><?=strtoupper($m[COL_KD_MISI])?></span><span class="text"><?=strtoupper($m[COL_NM_MISI])?></span>
                                    <div class="tools" style="display: none">
                                        <a href="<?=site_url()?>"><i class="fa fa-edit <?=$i%2==0?"text-yellow":"text-green"?>"></i></a>
                                    </div>
                                </li>
                                <?php
                                $i++;
                            }
                        }
                        ?>
                    </ul>
                </div>
            </div>
        </div>
        <div class="col-sm-4">
            <div class="box box-solid">
                <div class="box-header ui-sortable-handle" style="cursor: move;">
                    <h3 class="box-title"><i class="fad fa-calendar"></i>&nbsp;&nbsp;Kalender</h3>

                    <div class="pull-right box-tools">
                        <button type="button" class="btn btn-default btn-sm" data-widget="collapse"><i class="fa fa-minus"></i>
                        </button>
                    </div>
                </div>

                <div class="box-body no-padding" style="display: block;">
                    <!--The calendar -->
                    <div id="calendar" style="width: 100%">
                    </div>
                </div>
            </div>
        </div>

        <?php
        if(!empty($ruser[COL_COMPANYID]) && $ruser[COL_ROLEID]==ROLEKADIS) {
            $strOPD = explode('.', $ruser[COL_COMPANYID]);
            $this->db->where(COL_KD_URUSAN, $strOPD[0]);
            $this->db->where(COL_KD_BIDANG, $strOPD[1]);
            $this->db->where(COL_KD_UNIT, $strOPD[2]);
            $this->db->where(COL_KD_SUB, $strOPD[3]);
            $this->db->where(COL_KD_TYPE, "RENSTRA");
            $rrenstra = $this->db->get(TBL_SAKIP_MOPD_FILE)->row_array();

            $this->db->where(COL_KD_URUSAN, $strOPD[0]);
            $this->db->where(COL_KD_BIDANG, $strOPD[1]);
            $this->db->where(COL_KD_UNIT, $strOPD[2]);
            $this->db->where(COL_KD_SUB, $strOPD[3]);
            $this->db->where(COL_KD_TYPE, "RENJA");
            $rrenja = $this->db->get(TBL_SAKIP_MOPD_FILE)->row_array();
            ?>
            <div class="col-sm-4" style="display: none">
                <div class="box box-success">
                    <div class="box-header with-border">
                        <h3 class="box-title">Arsip OPD</h3>

                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                            </button>
                            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <ul class="products-list product-list-in-box">
                            <li class="item">
                                <div class="product-img" style="text-align: center">
                                    <i class="fa fa-file fa-2x text-<?=!empty($rrenja)?"yellow":"red"?>"></i>
                                </div>
                                <div class="product-info" style="margin-left: 40px !important;">
                                    <a href="<?=!empty($rrenja)?MY_UPLOADURL.$rrenja[COL_NM_FILE]:"javascript:void(0)"?>" class="product-title" target="_blank">RENJA
                                        <span class="label label-success pull-right" style="padding-top: .4em"><i class="fa fa-download"></i> Unduh</span>
                                    </a>
                                    <span class="product-description">
                                      <?=!empty($rrenja)?$rrenja[COL_NM_FILE]:"Belum diunggah"?>
                                    </span>
                                </div>
                            </li>
                            <li class="item">
                                <div class="product-img" style="text-align: center">
                                    <i class="fa fa-file fa-2x text-<?=!empty($rrenstra)?"yellow":"red"?>"></i>
                                </div>
                                <div class="product-info" style="margin-left: 40px !important;">
                                    <a href="<?=!empty($rrenstra)?MY_UPLOADURL.$rrenstra[COL_NM_FILE]:"javascript:void(0)"?>" class="product-title" target="_blank">RENSTRA
                                        <span class="label label-success pull-right" style="padding-top: .4em"><i class="fa fa-download"></i> Unduh</span>
                                    </a>
                                    <span class="product-description">
                                      <?=!empty($rrenstra)?$rrenstra[COL_NM_FILE]:"Belum diunggah"?>
                                    </span>
                                </div>
                            </li>
                        </ul>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer text-center">
                        <a href="<?=site_url('mopd/file')?>" class="uppercase">Lihat Arsip</a>
                    </div>
                    <!-- /.box-footer -->
                </div>
            </div>
            <?php
        }
        ?>

        <div class="col-sm-4" style="display: none">
            <?php
            if($rperiod) {
                $tujuan = $this->db->where(COL_KD_PEMDA, $rperiod[COL_KD_PEMDA])->get(TBL_SAKIP_MPMD_TUJUAN)->num_rows();
                $sasaran = $this->db->where(COL_KD_PEMDA, $rperiod[COL_KD_PEMDA])->get(TBL_SAKIP_MPMD_SASARAN)->num_rows();
                $tujuan_opd = $this->db->where(COL_KD_PEMDA, $rperiod[COL_KD_PEMDA])->get(TBL_SAKIP_MOPD_TUJUAN)->num_rows();
                $sasaran_opd = $this->db->where(COL_KD_PEMDA, $rperiod[COL_KD_PEMDA])->get(TBL_SAKIP_MOPD_SASARAN)->num_rows();
            }
            ?>
            <div class="info-box bg-yellow">
                <span class="info-box-icon"><i class="fa fa-map-signs"></i></span>
                <div class="info-box-content">
                    <span class="info-box-text">TUJUAN</span>
                    <span class="info-box-number"><?=number_format($tujuan, 0)?></span>

                    <div class="progress">
                        <div class="progress-bar" style="width: 50%"></div>
                    </div>
                  <span class="progress-description">
                    50% Increase in 30 Days
                  </span>
                </div>
            </div>
            <div class="info-box bg-aqua">
                <span class="info-box-icon"><i class="fa fa-bullseye"></i></span>
                <div class="info-box-content">
                    <span class="info-box-text">SASARAN</span>
                    <span class="info-box-number"><?=number_format($sasaran, 0)?></span>

                    <div class="progress">
                        <div class="progress-bar" style="width: 50%"></div>
                    </div>
                  <span class="progress-description">
                    50% Increase in 30 Days
                  </span>
                </div>
            </div>
            <div class="info-box bg-red">
                <span class="info-box-icon"><i class="fa fa-map-signs"></i></span>
                <div class="info-box-content">
                    <span class="info-box-text">TUJUAN OPD</span>
                    <span class="info-box-number"><?=number_format($tujuan_opd, 0)?></span>

                    <div class="progress">
                        <div class="progress-bar" style="width: 50%"></div>
                    </div>
                  <span class="progress-description">
                    50% Increase in 30 Days
                  </span>
                </div>
            </div>
            <div class="info-box bg-green">
                <span class="info-box-icon"><i class="fa fa-bullseye"></i></span>
                <div class="info-box-content">
                    <span class="info-box-text">SASARAN OPD</span>
                    <span class="info-box-number"><?=number_format($sasaran_opd, 0)?></span>

                    <div class="progress">
                        <div class="progress-bar" style="width: 50%"></div>
                    </div>
                  <span class="progress-description">
                    50% Increase in 30 Days
                  </span>
                </div>
            </div>
        </div>
    </div>
    <!-- /.row -->
</section>
<!-- /.content -->
<?php $this->load->view('loadjs')?>
<?php $this->load->view('footer')?>
<script>
    $(document).ready(function() {
        $("#calendar").datepicker().datepicker('update', new Date(<?=date('Y')?>,<?=date('m')?>-1,<?=date('d')?>));
    });
</script>
