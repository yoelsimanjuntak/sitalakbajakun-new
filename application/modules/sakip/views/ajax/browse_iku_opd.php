<?php
/**
 * Created by PhpStorm.
 * User: PTI
 * Date: 11/18/2019
 * Time: 12:15 PM
 */
?>
<?php
$wherecond = "Kd_Pemda=".$data[COL_KD_PEMDA]." and Kd_Misi=".$data[COL_KD_MISI]." and Kd_Tujuan=".$data[COL_KD_TUJUAN]." and Kd_IndikatorTujuan=".$data[COL_KD_INDIKATORTUJUAN]." and Kd_Sasaran=".$data[COL_KD_SASARAN]." and Kd_IndikatorSasaran=".$data[COL_KD_INDIKATORSASARAN]." and Kd_Urusan=".$data[COL_KD_URUSAN]." and Kd_Bidang=".$data[COL_KD_BIDANG]." and Kd_Unit=".$data[COL_KD_UNIT]." and Kd_Sub=".$data[COL_KD_SUB];
?>
<?=form_open(current_url(),array('role'=>'form','id'=>'browse-form','class'=>'form-horizontal'))?>
<input type="hidden" name="kdPemda" value="<?=$data[COL_KD_PEMDA]?>" />
<input type="hidden" name="kdMisi" value="<?=$data[COL_KD_MISI]?>" />
<input type="hidden" name="kdTujuan" value="<?=$data[COL_KD_TUJUAN]?>" />
<input type="hidden" name="kdIndikatorTujuan" value="<?=$data[COL_KD_INDIKATORTUJUAN]?>" />
<input type="hidden" name="kdSasaran" value="<?=$data[COL_KD_SASARAN]?>" />
<input type="hidden" name="kdIndikatorSasaran" value="<?=$data[COL_KD_INDIKATORSASARAN]?>" />
<input type="hidden" name="kdUrusan" value="<?=$data[COL_KD_URUSAN]?>" />
<input type="hidden" name="kdBidang" value="<?=$data[COL_KD_BIDANG]?>" />
<input type="hidden" name="kdUnit" value="<?=$data[COL_KD_UNIT]?>" />
<input type="hidden" name="kdSub" value="<?=$data[COL_KD_SUB]?>" />
<div class="form-group">
    <label class="control-label col-sm-3">Tujuan</label>
    <div class="col-sm-9">
        <select name="<?=COL_KD_TUJUANOPD?>" class="form-control no-select2" required>
            <?=GetCombobox("SELECT * FROM sakip_mopd_tujuan where ".$wherecond." ORDER BY Kd_Tujuan", COL_KD_TUJUANOPD, array(array(COL_KD_TUJUANOPD,COL_NM_TUJUANOPD),"."))?>
        </select>
    </div>
</div>
<div class="form-group">
    <label class="control-label col-sm-3">Indikator Tujuan</label>
    <div class="col-sm-9">
        <select name="<?=COL_KD_INDIKATORTUJUANOPD?>" class="form-control no-select2" required></select>
    </div>
</div>
<div class="form-group">
    <label class="control-label col-sm-3">Sasaran</label>
    <div class="col-sm-9">
        <select name="<?=COL_KD_SASARANOPD?>" class="form-control no-select2" required></select>
    </div>
</div>
<div class="form-group">
    <label class="control-label col-sm-3">Indikator Sasaran</label>
    <div class="col-sm-9">
        <select name="<?=COL_KD_INDIKATORSASARANOPD?>" class="form-control no-select2" required></select>
    </div>
</div>
<?=form_close()?>
<script type="text/javascript">
    $(document).ready(function() {
        $("select").not('.no-select2').select2({ width: 'resolve' });
    });
    var form = $("#browse-form");

    $("[name=Kd_TujuanOPD]", form).change(function() {
        var kdPemda = $("[name=kdPemda]", form).val();
        var kdMisi = $("[name=kdMisi]", form).val();
        var kdTujuan = $("[name=kdTujuan]", form).val();
        var kdIkTujuan = $("[name=kdIndikatorTujuan]", form).val();
        var kdSasaran = $("[name=kdSasaran]", form).val();
        var kdIkSasaran = $("[name=kdIndikatorSasaran]", form).val();

        var kdUrusan = $("[name=kdUrusan]", form).val();
        var kdBidang = $("[name=kdBidang]", form).val();
        var kdUnit = $("[name=kdUnit]", form).val();
        var kdSub = $("[name=kdSub]", form).val();

        var kdTujuanOPD = $("[name=Kd_TujuanOPD]", form).val();

        if(kdTujuanOPD) {
            var param = {
                Kd_Pemda: kdPemda,
                Kd_Urusan: kdUrusan,
                Kd_Bidang: kdBidang,
                Kd_Unit: kdUnit,
                Kd_Sub: kdSub,
                Kd_Misi: kdMisi,
                Kd_Tujuan: kdTujuan,
                Kd_IndikatorTujuan: kdIkTujuan,
                Kd_Sasaran: kdSasaran,
                Kd_IndikatorSasaran: kdIkSasaran,
                Kd_TujuanOPD: kdTujuanOPD
            };


            $("[name=Kd_IndikatorTujuanOPD]", form).load("<?=site_url("ajax/get-opt-iktujuanopd")?>", param, function () {
                $("[name=Kd_IndikatorTujuanOPD]", form).trigger("change");
            });
        } else {
            $("[name=Kd_IndikatorTujuanOPD]", form).html("").trigger("change");
        }
    }).trigger("change");

    $("[name=Kd_IndikatorTujuanOPD]", form).change(function() {
        var kdPemda = $("[name=kdPemda]", form).val();
        var kdMisi = $("[name=kdMisi]", form).val();
        var kdTujuan = $("[name=kdTujuan]", form).val();
        var kdIkTujuan = $("[name=kdIndikatorTujuan]", form).val();
        var kdSasaran = $("[name=kdSasaran]", form).val();
        var kdIkSasaran = $("[name=kdIndikatorSasaran]", form).val();

        var kdUrusan = $("[name=kdUrusan]", form).val();
        var kdBidang = $("[name=kdBidang]", form).val();
        var kdUnit = $("[name=kdUnit]", form).val();
        var kdSub = $("[name=kdSub]", form).val();

        var kdTujuanOPD = $("[name=Kd_TujuanOPD]", form).val();
        var kdIkTujuanOPD = $("[name=Kd_IndikatorTujuanOPD]", form).val();
        if(kdIkTujuanOPD) {
            var param = {
                Kd_Pemda: kdPemda,
                Kd_Urusan: kdUrusan,
                Kd_Bidang: kdBidang,
                Kd_Unit: kdUnit,
                Kd_Sub: kdSub,
                Kd_Misi: kdMisi,
                Kd_Tujuan: kdTujuan,
                Kd_IndikatorTujuan: kdIkTujuan,
                Kd_Sasaran: kdSasaran,
                Kd_IndikatorSasaran: kdIkSasaran,
                Kd_TujuanOPD: kdTujuanOPD,
                Kd_IndikatorTujuanOPD: kdIkTujuanOPD
            };

            $("[name=Kd_SasaranOPD]", form).load("<?=site_url("ajax/get-opt-sasaranopd")?>", param, function () {
                $("[name=Kd_SasaranOPD]", form).trigger("change");
            });
        } else {
            $("[name=Kd_SasaranOPD]", form).html("").trigger("change");
        }
    }).trigger("change");

    $("[name=Kd_SasaranOPD]", form).change(function() {
        var kdPemda = $("[name=kdPemda]", form).val();
        var kdMisi = $("[name=kdMisi]", form).val();
        var kdTujuan = $("[name=kdTujuan]", form).val();
        var kdIkTujuan = $("[name=kdIndikatorTujuan]", form).val();
        var kdSasaran = $("[name=kdSasaran]", form).val();
        var kdIkSasaran = $("[name=kdIndikatorSasaran]", form).val();

        var kdUrusan = $("[name=kdUrusan]", form).val();
        var kdBidang = $("[name=kdBidang]", form).val();
        var kdUnit = $("[name=kdUnit]", form).val();
        var kdSub = $("[name=kdSub]", form).val();

        var kdTujuanOPD = $("[name=Kd_TujuanOPD]", form).val();
        var kdIkTujuanOPD = $("[name=Kd_IndikatorTujuanOPD]", form).val();
        var kdSasaranOPD = $("[name=Kd_SasaranOPD]", form).val();
        if(kdSasaranOPD) {
            var param = {
                Kd_Pemda: kdPemda,
                Kd_Urusan: kdUrusan,
                Kd_Bidang: kdBidang,
                Kd_Unit: kdUnit,
                Kd_Sub: kdSub,
                Kd_Misi: kdMisi,
                Kd_Tujuan: kdTujuan,
                Kd_IndikatorTujuan: kdIkTujuan,
                Kd_Sasaran: kdSasaran,
                Kd_IndikatorSasaran: kdIkSasaran,
                Kd_TujuanOPD: kdTujuanOPD,
                Kd_IndikatorTujuanOPD: kdIkTujuanOPD,
                Kd_SasaranOPD: kdSasaranOPD
            };

            $("[name=Kd_IndikatorSasaranOPD]", form).load("<?=site_url("ajax/get-opt-iksasaranopd")?>", param, function () {
                $("[name=Kd_IndikatorSasaranOPD]", form).trigger("change");
            });
        }else {
            $("[name=Kd_IndikatorSasaranOPD]", form).html("");
        }
    }).trigger("change");
</script>