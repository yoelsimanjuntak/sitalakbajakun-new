<?php
/**
 * Created by PhpStorm.
 * User: PTI
 * Date: 11/16/2019
 * Time: 10:40 PM
 */
?>
<?=form_open(current_url(),array('role'=>'form','id'=>'browse-form','class'=>'form-horizontal'))?>
    <div class="form-group">
        <label class="control-label col-sm-3">Misi</label>
        <div class="col-sm-9">
            <input type="hidden" name="kdPemda" value="<?=$data[COL_KD_PEMDA]?>" />
            <select name="<?=COL_KD_MISI?>" class="form-control no-select2" required>
                <?=GetCombobox("SELECT * FROM sakip_mpmd_misi ".(!empty($data[COL_KD_PEMDA])?"where Kd_Pemda = ".$data[COL_KD_PEMDA]:"")." ORDER BY Kd_Misi", COL_KD_MISI, COL_NM_MISI)?>
            </select>
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-sm-3">Tujuan</label>
        <div class="col-sm-9">
            <select name="<?=COL_KD_TUJUAN?>" class="form-control no-select2" required></select>
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-sm-3">Indikator Tujuan</label>
        <div class="col-sm-9">
            <select name="<?=COL_KD_INDIKATORTUJUAN?>" class="form-control no-select2" required></select>
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-sm-3">Sasaran</label>
        <div class="col-sm-9">
            <select name="<?=COL_KD_SASARAN?>" class="form-control no-select2" required></select>
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-sm-3">Indikator Sasaran</label>
        <div class="col-sm-9">
            <select name="<?=COL_KD_INDIKATORSASARAN?>" class="form-control no-select2" required></select>
        </div>
    </div>
<?=form_close()?>
<script type="text/javascript">
    $(document).ready(function() {
        $("select").not('.no-select2').select2({ width: 'resolve' });
    });

    var form = $("#browse-form");
    $("[name=Kd_Misi]", form).change(function() {
        var kdPemda = $("[name=kdPemda]", form).val();
        var kdMisi = $("[name=Kd_Misi]", form).val();
        if(kdPemda && kdMisi) {
            $("[name=Kd_Tujuan]", form).load("<?=site_url("ajax/get-opt-tujuan")?>", {Kd_Pemda: kdPemda, Kd_Misi: kdMisi}, function () {
                $("[name=Kd_Tujuan]", form).trigger("change");
            });
        } else {
            $("[name=Kd_Tujuan]", form).html("").trigger("change");
        }
    }).trigger("change");

    $("[name=Kd_Tujuan]", form).change(function() {
        var kdPemda = $("[name=kdPemda]", form).val();
        var kdMisi = $("[name=Kd_Misi]", form).val();
        var kdTujuan = $("[name=Kd_Tujuan]", form).val();
        if(kdPemda && kdMisi && kdTujuan) {
            $("[name=Kd_IndikatorTujuan]", form).load("<?=site_url("ajax/get-opt-iktujuan")?>", {Kd_Pemda: kdPemda, Kd_Misi: kdMisi, Kd_Tujuan: kdTujuan}, function () {
                $("[name=Kd_IndikatorTujuan]", form).trigger("change");
            });
        } else {
            $("[name=Kd_IndikatorTujuan]", form).html("").trigger("change");
        }
    }).trigger("change");

    $("[name=Kd_IndikatorTujuan]", form).change(function() {
        var kdPemda = $("[name=kdPemda]", form).val();
        var kdMisi = $("[name=Kd_Misi]", form).val();
        var kdTujuan = $("[name=Kd_Tujuan]", form).val();
        var kdIkTujuan = $("[name=Kd_IndikatorTujuan]", form).val();
        if(kdPemda && kdMisi && kdTujuan && kdIkTujuan) {
            $("[name=Kd_Sasaran]", form).load("<?=site_url("ajax/get-opt-sasaran")?>", {Kd_Pemda: kdPemda, Kd_Misi: kdMisi, Kd_Tujuan: kdTujuan, Kd_IndikatorTujuan: kdIkTujuan}, function () {
                $("[name=Kd_Sasaran]", form).trigger("change");
            });
        } else {
            $("[name=Kd_Sasaran]", form).html("").trigger("change");
        }
    }).trigger("change");

    $("[name=Kd_Sasaran]", form).change(function() {
        var kdPemda = $("[name=kdPemda]", form).val();
        var kdMisi = $("[name=Kd_Misi]", form).val();
        var kdTujuan = $("[name=Kd_Tujuan]", form).val();
        var kdIkTujuan = $("[name=Kd_IndikatorTujuan]", form).val();
        var kdSasaran = $("[name=Kd_Sasaran]", form).val();
        if(kdPemda && kdMisi && kdTujuan && kdIkTujuan && kdSasaran) {
            $("[name=Kd_IndikatorSasaran]", form).load("<?=site_url("ajax/get-opt-iksasaran")?>", {Kd_Pemda: kdPemda, Kd_Misi: kdMisi, Kd_Tujuan: kdTujuan, Kd_IndikatorTujuan: kdIkTujuan, Kd_Sasaran: kdSasaran}, function () {
                $("[name=Kd_IndikatorSasaran]", form).trigger("change");
            });
        } else {
            $("[name=Kd_IndikatorSasaran]", form).html("").trigger("change");
        }
    }).trigger("change");
</script>