<table class="table table-hover tree-renja" data-expand-id="<?=!empty($expid)?$expid:''?>" >
  <thead>
    <th>Program / Kegiatan</th>
    <th>Unit Kerja</th>
    <th>Opsi</th>
  </thead>
  <tbody>
    <?php
    if(count($rprogram) > 0) {
      foreach ($rprogram as $prg) {
        $unitCond = array(
          COL_KD_URUSAN=>$prg[COL_KD_URUSAN],
          COL_KD_BIDANG=>$prg[COL_KD_BIDANG],
          COL_KD_UNIT=>$prg[COL_KD_UNIT],
          COL_KD_SUB=>$prg[COL_KD_SUB],
          COL_KD_BID=>$prg[COL_KD_BID]
        );
        $mainCond = array(
          COL_KD_TAHUN=>$prg[COL_KD_TAHUN],
          COL_KD_URUSAN=>$prg[COL_KD_URUSAN],
          COL_KD_BIDANG=>$prg[COL_KD_BIDANG],
          COL_KD_UNIT=>$prg[COL_KD_UNIT],
          COL_KD_SUB=>$prg[COL_KD_SUB],
          COL_KD_BID=>$prg[COL_KD_BID],

          COL_KD_PEMDA=>$prg[COL_KD_PEMDA],
          COL_KD_MISI=>$prg[COL_KD_MISI],
          COL_KD_TUJUAN=>$prg[COL_KD_TUJUAN],
          COL_KD_INDIKATORTUJUAN=>$prg[COL_KD_INDIKATORTUJUAN],
          COL_KD_SASARAN=>$prg[COL_KD_SASARAN],
          COL_KD_INDIKATORSASARAN=>$prg[COL_KD_INDIKATORSASARAN],
          COL_KD_TUJUANOPD=>$prg[COL_KD_TUJUANOPD],
          COL_KD_INDIKATORTUJUANOPD=>$prg[COL_KD_INDIKATORTUJUANOPD],
          COL_KD_SASARANOPD=>$prg[COL_KD_SASARANOPD],
          COL_KD_INDIKATORSASARANOPD=>$prg[COL_KD_INDIKATORSASARANOPD],
          COL_KD_PROGRAMOPD=>$prg[COL_KD_PROGRAMOPD]
        );

        $rdpa_prog = $this->db
        ->where($mainCond)
        ->order_by(COL_KD_PROGRAMOPD, 'asc')
        ->get(TBL_SAKIP_PDPA_PROGRAM)
        ->row_array();

        $rkegiatan = $this->db
        ->where($mainCond)
        ->order_by(COL_KD_KEGIATANOPD, 'asc')
        ->get(TBL_SAKIP_PRENJA_KEGIATAN)
        ->result_array();

        $rbid = $this->db
        ->where($unitCond)
        ->get(TBL_SAKIP_MBID)
        ->row_array();

        $idprg = $prg[COL_KD_PEMDA].'.'.
                  $prg[COL_KD_TAHUN].'.'.
                  $prg[COL_KD_URUSAN].'.'.
                  $prg[COL_KD_BIDANG].'.'.
                  $prg[COL_KD_UNIT].'.'.
                  $prg[COL_KD_SUB].'.'.
                  $prg[COL_KD_BID].'.'.
                  $prg[COL_KD_MISI].'.'.
                  $prg[COL_KD_TUJUAN].'.'.
                  $prg[COL_KD_INDIKATORTUJUAN].'.'.
                  $prg[COL_KD_SASARAN].'.'.
                  $prg[COL_KD_INDIKATORSASARAN].'.'.
                  $prg[COL_KD_TUJUANOPD].'.'.
                  $prg[COL_KD_INDIKATORTUJUANOPD].'.'.
                  $prg[COL_KD_SASARANOPD].'.'.
                  $prg[COL_KD_INDIKATORSASARANOPD].'.'.
                  $prg[COL_KD_PROGRAMOPD];

        $keyprg = $prg[COL_KD_PEMDA].'.'.
                  $prg[COL_KD_TAHUN].'.'.
                  $prg[COL_KD_URUSAN].'.'.
                  $prg[COL_KD_BIDANG].'.'.
                  $prg[COL_KD_UNIT].'.'.
                  $prg[COL_KD_SUB].'.'.
                  $prg[COL_KD_MISI].'.'.
                  $prg[COL_KD_TUJUAN].'.'.
                  $prg[COL_KD_INDIKATORTUJUAN].'.'.
                  $prg[COL_KD_SASARAN].'.'.
                  $prg[COL_KD_INDIKATORSASARAN].'.'.
                  $prg[COL_KD_TUJUANOPD].'.'.
                  $prg[COL_KD_INDIKATORTUJUANOPD].'.'.
                  $prg[COL_KD_SASARANOPD].'.'.
                  $prg[COL_KD_INDIKATORSASARANOPD];
        ?>
        <tr data-tt-id="<?=$idprg?>">
          <td><strong><?=$prg[COL_KD_PROGRAMOPD]?></strong>: <?=$prg[COL_NM_PROGRAMOPD]?><?=!empty($rdpa_prog)?'<span class="pull-right text-success text-italic"><i class="fas fa-check-square"></i>&nbsp;DPA</span>':''?></td>
          <td style="width: 15vw" class="text-sm"><?=!empty($rbid)?$rbid[COL_NM_BID]:'-'?></td>
          <td style="width: 12vw">
            <!--<a href="<?=site_url('sakip/perubahan/ubah-program/'.$prg[COL_UNIQ])?>" class="btn btn-xs btn-success btn-edit-program" style="padding: 2px 5px; font-size: 10px"><i class="fas fa-edit"></i>&nbsp;UBAH</a>
            <a href="<?=site_url('sakip/perubahan/hapus-program/'.$prg[COL_UNIQ])?>" class="btn btn-xs btn-danger btn-delete-program" style="padding: 2px 5px; font-size: 10px"><i class="fas fa-trash"></i>&nbsp;HAPUS</a>
            <a href="<?=site_url('sakip/perubahan/tambah-sasaran-program/'.$prg[COL_UNIQ])?>" class="btn btn-xs btn-info btn-tambah-sasaran-program" style="padding: 2px 5px; font-size: 10px"><i class="fas fa-list"></i>&nbsp;SASARAN</a>
            <button type="button" class="btn btn-xs btn-warning" style="padding: 2px 5px; font-size: 10px"><i class="fas fa-share"></i>&nbsp;DPA</button>
            <button type="button" class="btn btn-xs btn-primary" style="padding: 2px 5px; font-size: 10px"><i class="fas fa-plus"></i>&nbsp;KEGIATAN</button>-->
            <div class="btn-group">
              <button type="button" class="btn btn-xs btn-primary dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                <i class="fas fa-edit"></i>&nbsp;AKSI&nbsp;<i class="fas fa-caret-down"></i>
              </button>
              <ul class="dropdown-menu">
                <li><a href="<?=site_url('sakip/perubahan/ubah-program/'.$prg[COL_UNIQ].'/'.$keyprg)?>" class="btn-edit-program"><i class="fas fa-edit"></i>&nbsp;UBAH</li>
                <li><a href="<?=site_url('sakip/perubahan/hapus-program/'.$prg[COL_UNIQ])?>" class="btn-delete-program"><i class="fas fa-trash"></i>&nbsp;HAPUS</a></li>
                <li><a href="<?=site_url('sakip/perubahan/tambah-sasaran-program/'.$prg[COL_UNIQ])?>" class="btn-tambah-sasaran-program"><i class="fas fa-plus"></i>&nbsp;SASARAN</a></li>
                <li><a href="<?=site_url('sakip/perubahan/tambah-kegiatan/'.$prg[COL_UNIQ].'/'.$keyprg.'.'.$prg[COL_KD_BID].'.'.$prg[COL_KD_PROGRAMOPD])?>" class="btn-tambah-kegiatan"><i class="fas fa-plus"></i>&nbsp;KEGIATAN</a></li>
                <li><a href="<?=site_url('sakip/perubahan/tambah-dpa-program/'.$prg[COL_UNIQ].'/'.$keyprg)?>" class="btn-tambah-dpa-program"><i class="fas fa-share"></i>&nbsp;DPA</a></li>
              </ul>
            </div>
            <a href="<?=site_url('sakip/perubahan/list-sasaran-program/'.$prg[COL_UNIQ])?>" class="btn btn-xs btn-warning btn-list-sasaran-program" style="padding: 2px 5px; font-size: 10px"><i class="fas fa-list"></i>&nbsp;SASARAN</a>
          </td>
        </tr>
        <?php
        foreach ($rkegiatan as $keg) {
          $rdpa_keg = $this->db
          ->where(array_merge($mainCond, array(
            COL_KD_SASARANPROGRAMOPD=>$keg[COL_KD_SASARANPROGRAMOPD],
            COL_KD_SUBBID=>$keg[COL_KD_SUBBID],
            COL_KD_KEGIATANOPD=>$keg[COL_KD_KEGIATANOPD]
          )))
          ->order_by(COL_KD_KEGIATANOPD, 'asc')
          ->get(TBL_SAKIP_PDPA_KEGIATAN)
          ->row_array();

          $rsubbid = $this->db
          ->where(array_merge($unitCond, array(COL_KD_SUBBID=>$keg[COL_KD_SUBBID])))
          ->get(TBL_SAKIP_MSUBBID)
          ->row_array();

          $idkeg = $keg[COL_KD_PEMDA].'.'.
                    $keg[COL_KD_TAHUN].'.'.
                    $keg[COL_KD_URUSAN].'.'.
                    $keg[COL_KD_BIDANG].'.'.
                    $keg[COL_KD_UNIT].'.'.
                    $keg[COL_KD_SUB].'.'.
                    $keg[COL_KD_BID].'.'.
                    $keg[COL_KD_SUBBID].'.'.
                    $keg[COL_KD_MISI].'.'.
                    $keg[COL_KD_TUJUAN].'.'.
                    $keg[COL_KD_INDIKATORTUJUAN].'.'.
                    $keg[COL_KD_SASARAN].'.'.
                    $keg[COL_KD_INDIKATORSASARAN].'.'.
                    $keg[COL_KD_TUJUANOPD].'.'.
                    $keg[COL_KD_INDIKATORTUJUANOPD].'.'.
                    $keg[COL_KD_SASARANOPD].'.'.
                    $keg[COL_KD_INDIKATORSASARANOPD].'.'.
                    $keg[COL_KD_PROGRAMOPD].'.'.
                    $keg[COL_KD_KEGIATANOPD];
          ?>
          <tr data-tt-id="<?=$idkeg?>" data-tt-parent-id="<?=$idprg?>">
            <td><strong><?=$keg[COL_KD_PROGRAMOPD].'.'.$keg[COL_KD_KEGIATANOPD]?></strong>: <?=$keg[COL_NM_KEGIATANOPD]?><?=!empty($rdpa_keg)?'<span class="pull-right text-success text-italic"><i class="fas fa-check-square"></i>&nbsp;DPA</span>':''?></td>
            <td style="width: 15vw" class="text-sm"><?=!empty($rsubbid)?$rsubbid[COL_NM_SUBBID]:'-'?></td>
            <td style="width: 12vw">
              <!--<button type="button" class="btn btn-xs btn-success" style="padding: 2px 5px; font-size: 10px"><i class="fas fa-edit"></i>&nbsp;UBAH</button>
              <button type="button" class="btn btn-xs btn-danger" style="padding: 2px 5px; font-size: 10px"><i class="fas fa-trash"></i>&nbsp;HAPUS</button>
              <button type="button" class="btn btn-xs btn-info" style="padding: 2px 5px; font-size: 10px"><i class="fas fa-list"></i>&nbsp;SASARAN</button>
              <button type="button" class="btn btn-xs btn-warning" style="padding: 2px 5px; font-size: 10px"><i class="fas fa-share"></i>&nbsp;DPA</button>-->
              <div class="btn-group">
                <button type="button" class="btn btn-xs btn-primary dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                  <i class="fas fa-edit"></i>&nbsp;AKSI&nbsp;<i class="fas fa-caret-down"></i>
                </button>
                <ul class="dropdown-menu">
                  <li><a href="<?=site_url('sakip/perubahan/ubah-kegiatan/'.$keg[COL_UNIQ].'/'.$prg[COL_UNIQ].'/'.$keyprg.'.'.$prg[COL_KD_BID].'.'.$prg[COL_KD_PROGRAMOPD])?>" class="btn-edit-kegiatan"><i class="fas fa-edit"></i>&nbsp;UBAH</li>
                  <li><a href="<?=site_url('sakip/perubahan/hapus-kegiatan/'.$keg[COL_UNIQ])?>" class="btn-delete-kegiatan"><i class="fas fa-trash"></i>&nbsp;HAPUS</a></li>
                  <li><a href="<?=site_url('sakip/perubahan/tambah-sasaran-kegiatan/'.$keg[COL_UNIQ])?>" class="btn-tambah-sasaran-kegiatan"><i class="fas fa-plus"></i>&nbsp;SASARAN</a></li>
                  <li><a href="<?=site_url('sakip/perubahan/tambah-dpa-kegiatan/'.$keg[COL_UNIQ].'/'.$prg[COL_UNIQ].'/'.$keyprg.'.'.$prg[COL_KD_BID].'.'.$prg[COL_KD_PROGRAMOPD])?>" class="btn-tambah-dpa-kegiatan"><i class="fas fa-share"></i>&nbsp;DPA</a></li>
                </ul>
              </div>
              <a href="<?=site_url('sakip/perubahan/list-sasaran-kegiatan/'.$keg[COL_UNIQ])?>" class="btn btn-xs btn-warning btn-list-sasaran-kegiatan" style="padding: 2px 5px; font-size: 10px"><i class="fas fa-list"></i>&nbsp;SASARAN</a>
            </td>
          </tr>
          <?php
        }
      }
    } else {
      ?>
      <tr>
        <td colspan="3">Belum ada data.</td>
      </tr>
      <?php
    }
    ?>
  </tbody>
</table>
<script>
$(document).ready(function() {
  $('.btn-group>ul.dropdown-menu>li>a', $('.tree-renja')).click(function(){
    $(this).closest('.btn-group').removeClass('open');
  });
});
</script>
