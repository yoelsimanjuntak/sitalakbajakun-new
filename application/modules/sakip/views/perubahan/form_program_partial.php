<?=form_open(current_url(),array('role'=>'form','id'=>'form-program', 'class'=>'form-horizontal'))?>
<div class="form-group">
  <label class="control-label col-sm-3">Bidang</label>
  <div class="col-sm-8">
    <select name="<?=COL_KD_BID?>" class="form-control" style="width: 100%" <?=!empty($dpa)?'disabled':''?> required>
      <?=GetCombobox("select * from sakip_mbid where Kd_Urusan = $kdUrusan and Kd_Bidang = $kdBidang and Kd_Unit = $kdUnit and Kd_Sub = $kdSub order by Nm_Bid", COL_KD_BID, COL_NM_BID, (!empty($data)?$data[COL_KD_BID]:null))?>
    </select>
  </div>
</div>
<?php
if(empty($dpa)) {
  ?>
  <div class="form-group">
    <div class="col-sm-8 col-sm-offset-3">
      <div class="checkbox">
        <label>
          <input type="checkbox" name="<?=COL_ISEPLAN?>" <?=!empty($data)?($data[COL_ISEPLAN] ? "checked" : "") : "checked"?>>
          Import dari DPA Induk
        </label>
      </div>
    </div>
  </div>
  <div class="form-group is-eplan">
    <label class="control-label col-sm-3">Program</label>
    <div class="col-sm-8">
      <select name="<?=COL_KD_PROGRAMOPD?>" class="form-control" style="width: 100%">
        <?php
        if(!empty($data)) {
          $kdBid = $data[COL_KD_BID];
          $qcond = '';
          foreach ($cond as $key => $value) {
            $qcond .= "and ".$key." = '$value' ";
          }
          echo GetCombobox("select * from sakip_dpa_program where 1=1 $qcond and Kd_Bid = $kdBid order by Nm_ProgramOPD", COL_KD_PROGRAMOPD, array(array(COL_KD_PROGRAMOPD, COL_NM_PROGRAMOPD), ': '), (!empty($data)?$data[COL_KD_PROGRAMOPD]:null));
        }
        ?>
      </select>
      <p class="help-block">Import dari DPA Induk akan <strong>otomatis</strong> mengisi sasaran dan indikator sesuai DPA Induk.</p>
    </div>
  </div>
  <?php
}
?>
<div class="form-group is-not-eplan">
  <label class="control-label col-sm-3">Kode Program</label>
  <div class="col-sm-4">
    <input type="number" placeholder="Kode" class="form-control" name="<?=COL_KD_PROGRAMOPD?>" value="<?=!empty($data)?$data[COL_KD_PROGRAMOPD]:''?>" <?=!empty($dpa)?'disabled':''?>  required />
  </div>
</div>
<div class="form-group is-not-eplan">
  <label class="control-label col-sm-3">Nama Program</label>
  <div class="col-sm-8">
    <input type="text" placeholder="Nama" class="form-control" name="<?=COL_NM_PROGRAMOPD?>" value="<?=!empty($data)?$data[COL_NM_PROGRAMOPD]:''?>" <?=!empty($dpa)?'disabled':''?> required />
  </div>
</div>
<?php
if(empty($dpa)) {
  ?>
  <div class="form-group">
    <label class="control-label col-sm-3">Catatan</label>
    <div class="col-sm-8">
      <textarea placeholder="Catatan" class="form-control" name="<?=COL_REMARKS?>"><?=!empty($data)?$data[COL_REMARKS]:''?></textarea>
    </div>
  </div>
  <?php
}
?>
<?php
if(!empty($rdpa)) {
  ?>
  <div class="form-group">
    <div class="col-sm-8 col-sm-offset-3">
      <p class="help-block">Program ini sudah diinput ke DPA. Mengubah informasi pada program ini akan <strong>otomatis mengubah</strong> informasi pada DPA.</p>
    </div>
  </div>
  <?php
}
?>
<?php
if(!empty($dpa)) {
  ?>
  <input type="hidden" value="1" name="<?=COL_IS_RENJA?>" />
  <div class="form-group">
    <div class="col-sm-8 col-sm-offset-3">
      <p class="help-block">Klik tombol "<strong>SUBMIT</strong>" untuk memasukkan data ini ke DPA.</p>
    </div>
  </div>
  <?php
}
?>
<div class="row" style="padding-top: 10px; border-top: 1px solid #f4f4f4">
  <div class="col-sm-12 text-center">
    <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fas fa-times"></i>&nbsp;CLOSE</button>&nbsp;
    <button type="submit" class="btn btn-success"><i class="fas fa-check"></i>&nbsp;SUBMIT</button>
  </div>
</div>
<?=form_close()?>
<script type="text/javascript">
$(document).ready(function() {
  $("[name=IsEplan]").change(function() {
    if($(this).is(":checked")) {
      $('input, select', $('.is-eplan')).attr('disabled', false);
      $('.is-eplan').show();
      $('input, select', $('.is-not-eplan')).attr('disabled', true);
      $('.is-not-eplan').hide();
    }
    else {
      $('input, select', $('.is-eplan')).attr('disabled', true);
      $('.is-eplan').hide();
      $('input, select', $('.is-not-eplan')).attr('disabled', false);
      $('.is-not-eplan').show();
    }
  }).trigger("change");

  $("[name=Kd_Bid]").change(function() {
    var key_ = '<?=$id?>'+'.'+$(this).val();

    $('[name=Kd_ProgramOPD]').load('<?=site_url("sakip/ajax/get-opt-pdpa-program")?>', {key: key_}, function() {

    });
  });
  <?php
    if(empty($data)) {
      echo '$("[name=Kd_Bid]").trigger("change")';
    }
  ?>
});
</script>
