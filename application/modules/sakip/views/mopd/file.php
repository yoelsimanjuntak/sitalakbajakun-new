<?php
/**
 * Created by PhpStorm.
 * User: 6217025076
 * Date: 21/12/2018
 * Time: 10:32
 */
$data = array();
$i = 0;
foreach ($res as $d) {
    $nmSub = "";
    $eplandb = $this->load->database("eplan", true);
    $eplandb->where(COL_KD_URUSAN, $d[COL_KD_URUSAN]);
    $eplandb->where(COL_KD_BIDANG, $d[COL_KD_BIDANG]);
    $eplandb->where(COL_KD_UNIT, $d[COL_KD_UNIT]);
    $eplandb->where(COL_KD_SUB, $d[COL_KD_SUB]);
    $subunit = $eplandb->get("ref_sub_unit")->row_array();
    if($subunit) {
        $nmSub = $subunit["Nm_Sub_Unit"];
    }

    $res[$i] = array(
        '<input type="checkbox" class="cekbox" name="cekbox[]" value="' . $d[COL_UNIQ] . '" />',
        anchor('mopd/file-edit/'.$d[COL_UNIQ], '<i class="fa fa-pencil"></i>'),
        $d[COL_KD_TAHUN_FROM]." s.d ".$d[COL_KD_TAHUN_TO],
        (!empty($d[COL_KD_TAHUN])&&$d[COL_KD_TAHUN]>0?$d[COL_KD_TAHUN]:"-"),
        $nmSub,
        //$d[COL_KD_TYPE],
        '<a href="'.MY_UPLOADURL.$d[COL_NM_FILE].'" target="_blank"><i class="fa fa-file"></i>&nbsp;&nbsp;Download</a>',
        date('d/m/Y H:i:s', strtotime($d[COL_CREATE_DATE])),
        isset($d[COL_EDIT_DATE]) ? date('d/m/Y H:i:s', strtotime($d[COL_EDIT_DATE])) : "-"
    );
    $i++;
}
$data = json_encode($res);
?>

<?php
$this->load->view('header');
$arrtype = array('', 'Pohon Kinerja', 'Renstra', 'Renja', 'Laporan Kinerja');
?>
    <section class="content-header">
        <h1><?= $title ?>  <small><?=$arrtype[$id]?></small></h1>
        <ol class="breadcrumb">
            <li>
                <a href="<?=site_url()?>"><i class="fa fa-dashboard"></i> Home</a>
            </li>
            <li class="active">
                <?=$title?>
            </li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <p>
            <?=anchor('mopd/file-del','<i class="far fa-trash"></i> Hapus',array('class'=>'cekboxaction btn btn-sm btn-danger','confirm'=>'Apa anda yakin?'))
            ?>
            <?=anchor('mopd/file-add/'.$id,'<i class="far fa-plus"></i> Data Baru',array('class'=>'btn btn-sm btn-primary'))
            ?>
        </p>
        <div class="box box-default">
            <div class="box-body">
                <form id="dataform" method="post" action="#">
                    <table id="datalist" class="table table-bordered table-hover">

                    </table>
                </form>
            </div>
        </div>
    </section>

<?php $this->load->view('loadjs')?>
    <script type="text/javascript">
        $(document).ready(function() {
            var dataTable = $('#datalist').dataTable({
                //"sDom": "Rlfrtip",
                "aaData": <?=$data?>,
                //"bJQueryUI": true,
                //"aaSorting" : [[5,'desc']],
                "order": [[2, "asc"]],
                "columnDefs": [
                    {
                        "targets": [0,1],
                        "orderable": false
                    }
                ],
                "scrollY" : window.screen.height - 490,
                "scrollX": "200%",
                "iDisplayLength": 100,
                "aLengthMenu": [[100, 1000, 5000, -1], [100, 1000, 5000, "Semua"]],
                "dom":"R<'row'<'col-sm-4'l><'col-sm-4'B><'col-sm-4'f>><'row'<'col-sm-12'tr>><'row'<'col-sm-5'i><'col-sm-7'p>>",
                "buttons": ['copyHtml5','excelHtml5','csvHtml5','pdfHtml5'],
                "aoColumns": [
                    {"sTitle": "<input type=\"checkbox\" id=\"cekbox\" class=\"\" />","sWidth":15,bSortable:false},
                    {"sTitle": "#","sWidth":15},
                    {"sTitle": "Periode"},
                    {"sTitle": "Tahun"},
                    {"sTitle": "OPD"},
                    //{"sTitle": "Tipe"},
                    {"sTitle": "Dokumen"},
                    {"sTitle": "Diinput Pada"},
                    {"sTitle": "Terakhir Diubah"}
                ]
            });
            $('#cekbox').click(function(){
                if($(this).is(':checked')){
                    $('.cekbox').prop('checked',true);
                    console.log('clicked');
                }else{
                    $('.cekbox').prop('checked',false);
                }
            });
        });
    </script>

<?php $this->load->view('footer')
?>
