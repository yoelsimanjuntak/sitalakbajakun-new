<html>
<head>
  <style>
  body {
    font-family: 'Source Sans Pro','Helvetica Neue',Helvetica,Arial,sans-serif;
  }
  th, td {
    padding: 5px;
  }
  </style>
</head>
<body>
  <h3 style="text-align: center; text-transform: uppercase; margin-bottom: 0">
    <?=$title?><br />
    PEMERINTAH KOTA TEBING TINGGI
  </h3>
  <hr style="margin-bottom: 1.5rem" />
  <table style="border: 1px solid #000; border-spacing: 0" border="1" width="100%">
    <tbody>
      <tr>
        <th rowspan="2">No.</th>
        <th rowspan="2">Sasaran</th>
        <th rowspan="2" colspan="2">Indikator Kinerja</th>
        <th colspan="5" style="background-color: #ff851b73">Target</th>
        <th colspan="5" style="background-color: #01ff7054">Realisasi</th>
      </tr>
      <tr>
        <th style="background-color: #ff851b73">TW. I</th>
        <th style="background-color: #ff851b73">TW. II</th>
        <th style="background-color: #ff851b73">TW. III</th>
        <th style="background-color: #ff851b73">TW. IV</th>
        <th style="background-color: #ff851b73">Akhir</th>
        <th style="background-color: #01ff7054">TW. I</th>
        <th style="background-color: #01ff7054">TW. II</th>
        <th style="background-color: #01ff7054">TW. III</th>
        <th style="background-color: #01ff7054">TW. IV</th>
        <th style="background-color: #01ff7054">Akhir</th>
      </tr>
      <?php
      $no=0;
      $seq = 1;
      $seq_ = 1;
      foreach($res as $r) {
        ?>
        <tr>
          <?php
          if($no != $r['ID_Sasaran']) {
            ?>
            <td <?=$r['rowspan']>1?'rowspan="'.$r['rowspan'].'"':''?>><?=$seq?></td>
            <td <?=$r['rowspan']>1?'rowspan="'.$r['rowspan'].'"':''?>><?=$r[COL_NM_SASARANOPD]?></td>
            <?php
            $seq++;
            $seq_ = 1;
          }
          ?>
          <td><?=$seq_?></td>
          <td><?=nl2br($r[COL_NM_INDIKATORSASARANOPD])?></td>
          <?php
          for($i=1; $i<=4; $i++) {
            ?>
            <td style="text-align:center; white-space: nowrap; background-color: #ff851b73">
              <?=(!empty($r['Target_TW'.$i])?nl2br($r['Target_TW'.$i]):'-')?>
            </td>
            <?php
          }
          ?>
          <td style="text-align:center; white-space: nowrap; background-color: #ff851b73">
            <?=(!empty($r['Target'])?nl2br($r['Target']):'-')?>
          </td>
          <?php
          for($i=1; $i<=4; $i++) {
            ?>
            <td style="text-align:center; white-space: nowrap; background-color: #01ff7054">
              <?=(!empty($r['Realisasi_TW'.$i])?nl2br($r['Realisasi_TW'.$i]):'-')?>
            </td>
            <?php
          }
          ?>
          <td style="text-align:center; white-space: nowrap; background-color: #01ff7054">
            <?=(!empty($r['Realisasi'])?nl2br($r['Realisasi']):'-')?>
          </td>
          <?php
          ?>
        </tr>
        <?php
        $no = $r['ID_Sasaran'];
        $seq_++;
      }
      ?>
    </tbody>
  </table>
</body>
