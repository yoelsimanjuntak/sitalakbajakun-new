<?php
/**
 * Created by PhpStorm.
 * User: Yoel Simanjuntak
 * Date: 24/11/2018
 * Time: 21:01
 */
$ruser = GetLoggedUser();
$arrtype = array('', 'Pohon Kinerja', 'Renstra', 'Renja', 'Laporan Kinerja');
$this->load->view('header') ?>
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1> <?= $title ?> <small> <?=$arrtype[$tipe]?></small></h1>
        <ol class="breadcrumb">
            <li><a href="<?=site_url()?>"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="<?=site_url('mopd/file')?>"> Arsip OPD</a></li>
            <li class="active"><?=$edit?'Edit':'Add'?></li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-sm-12">
                <div class="box box-primary" style="border-top-color: transparent">
                    <div class="box-body">
                        <?=form_open_multipart(current_url(),array('role'=>'form','id'=>'file-add', 'class'=>'form-horizontal'))?>
                        <div style="display: none" class="alert alert-danger errorBox">
                            <i class="fa fa-ban"></i>
                            <span class="errorMsg"></span>
                        </div>
                        <?php
                        if($this->input->get('success') == 1){
                            ?>
                            <div class="alert alert-success">
                                <i class="fa fa-check"></i>
                                <span class="">Data disimpan</span>
                            </div>
                        <?php
                        }
                        ?>
                        <div class="form-group">
                            <label class="control-label col-sm-2">OPD</label>
                            <div class="col-sm-8">
                                <div class="input-group">
                                    <?php
                                    $nmSub = "";
                                    $strOPD = explode('.', $ruser[COL_COMPANYID]);
                                    if($edit) {
                                        $eplandb = $this->load->database("eplan", true);
                                        $eplandb->where(COL_KD_URUSAN, $data[COL_KD_URUSAN]);
                                        $eplandb->where(COL_KD_BIDANG, $data[COL_KD_BIDANG]);
                                        $eplandb->where(COL_KD_UNIT, $data[COL_KD_UNIT]);
                                        $eplandb->where(COL_KD_SUB, $data[COL_KD_SUB]);
                                        $subunit = $eplandb->get("ref_sub_unit")->row_array();
                                        if($subunit) {
                                            $nmSub = $subunit["Nm_Sub_Unit"];
                                        }
                                    }
                                    if($ruser[COL_ROLEID] == ROLEKADIS) {
                                        $eplandb = $this->load->database("eplan", true);
                                        $eplandb->where(COL_KD_URUSAN, $strOPD[0]);
                                        $eplandb->where(COL_KD_BIDANG, $strOPD[1]);
                                        $eplandb->where(COL_KD_UNIT, $strOPD[2]);
                                        $eplandb->where(COL_KD_SUB, $strOPD[3]);
                                        $subunit = $eplandb->get("ref_sub_unit")->row_array();
                                        if($subunit) {
                                            $nmSub = $subunit["Nm_Sub_Unit"];
                                        }
                                    }

                                    ?>
                                    <input type="text" class="form-control" name="text-opd" value="<?= $edit ? $data[COL_KD_URUSAN].".".$data[COL_KD_BIDANG].".".$data[COL_KD_UNIT].".".$data[COL_KD_SUB]." ".$nmSub : ($ruser[COL_ROLEID] == ROLEKADIS ? $strOPD[0].".".$strOPD[1].".".$strOPD[2].".".$strOPD[3]." ".$nmSub : "")?>" readonly>
                                    <input type="hidden" name="<?=COL_KD_URUSAN?>" value="<?= $edit ? $data[COL_KD_URUSAN] : ($ruser[COL_ROLEID]==ROLEKADIS?$strOPD[0]:"")?>" required   >
                                    <input type="hidden" name="<?=COL_KD_BIDANG?>" value="<?= $edit ? $data[COL_KD_BIDANG] : ($ruser[COL_ROLEID]==ROLEKADIS?$strOPD[1]:"")?>" required   >
                                    <input type="hidden" name="<?=COL_KD_UNIT?>" value="<?= $edit ? $data[COL_KD_UNIT] : ($ruser[COL_ROLEID]==ROLEKADIS?$strOPD[2]:"")?>" required   >
                                    <input type="hidden" name="<?=COL_KD_SUB?>" value="<?= $edit ? $data[COL_KD_SUB] : ($ruser[COL_ROLEID]==ROLEKADIS?$strOPD[3]:"")?>" required   >
                                    <div class="input-group-btn">
                                        <button type="button" class="btn btn-default btn-flat btn-browse-opd" data-toggle="modal" data-target="#browseOPD" data-toggle="tooltip" data-placement="top" title="Pilih OPD" <?=$edit?"disabled":($ruser[COL_ROLEID] == ROLEKADIS ? "disabled" : "")?>><i class="fa fa-ellipsis-h"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!--<div class="form-group">
                            <label class="control-label col-sm-2">Tipe</label>
                            <div class="col-sm-5">
                                <select name="<?=COL_KD_TYPE?>" class="form-control">
                                    <option value="RENJA" <?=!empty($data) && $data[COL_KD_TYPE] == "RENJA" ? "selected" : "" ?>>RENJA</option>
                                    <option value="RENSTRA" <?=!empty($data) && $data[COL_KD_TYPE] == "RENSTRA" ? "selected" : "" ?>>RENSTRA</option>
                                    <option value="RPJMD" <?=!empty($data) && $data[COL_KD_TYPE] == "RPJMD" ? "selected" : "" ?>>RPJMD</option>
                                    <option value="IKU" <?=!empty($data) && $data[COL_KD_TYPE] == "IKU" ? "selected" : "" ?>>IKU</option>
                                    <option value="LAKIP" <?=!empty($data) && $data[COL_KD_TYPE] == "LAKIP" ? "selected" : "" ?>>LAKIP</option>
                                </select>
                            </div>
                        </div>-->

                        <div class="form-group">
                            <label class="control-label col-sm-2">Periode</label>
                            <div class="col-sm-5">
                                <select name="<?=COL_KD_PEMDA?>" class="form-control">
                                    <?=GetCombobox("SELECT * FROM ".TBL_SAKIP_MPEMDA." ORDER BY ".COL_KD_TAHUN_FROM, COL_KD_PEMDA, [[COL_KD_TAHUN_FROM, COL_KD_TAHUN_TO], " s.d "], (!empty($data[COL_KD_PEMDA]) ? $data[COL_KD_PEMDA] : null))?>
                                </select>

                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-sm-2">Tahun</label>
                            <div class="col-sm-2">
                                <input type="number" class="form-control" name="<?=COL_KD_TAHUN?>" value="<?= $edit ? $data[COL_KD_TAHUN] : ""?>">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-sm-2">Keterangan</label>
                            <div class="col-sm-4">
                                <textarea name="<?=COL_NM_KETERANGAN?>" class="form-control" rows="3"><?= $edit ? $data[COL_NM_KETERANGAN] : ""?></textarea>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-sm-2">File</label>
                            <div class="col-sm-8">
                                <?php
                                if(!empty($data) && !empty($data[COL_NM_FILE])) {
                                    ?>
                                    <object id="preview-data" data="<?=MY_UPLOADURL.$data[COL_NM_FILE]?>" type="<?=mime_content_type(MY_UPLOADPATH.$data[COL_NM_FILE])?>" width="100%" height="400">
                                    </object>
                                <?php
                                }
                                ?>
                                <input name="userfile" type="file" />
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-12" style="text-align: right">
                                <button type="submit" class="btn btn-primary btn-flat">Simpan</button>
                                <a href="<?=site_url('mopd/file/'.($edit?$data[COL_KD_TYPE]:(isset($tipe)?$tipe:"")))?>" class="btn btn-default btn-flat">Kembali ke Daftar&nbsp;&nbsp;<i class="fa fa-arrow-right"></i> </a>
                            </div>

                        </div>
                        <?=form_close()?>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <div class="modal fade" id="browseOPD" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Browse</h4>
                </div>
                <div class="modal-body">
                    ...
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default btn-flat pull-right" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

<?php $this->load->view('loadjs') ?>
    <script type="text/javascript">
        $("#file-add").validate({
            submitHandler : function(form){
                $(form).find('button[type=submit]').attr('disabled',true);
                $(form).ajaxSubmit({
                    dataType: 'json',
                    type : 'post',
                    success : function(data){
                        $(form).find('btn').attr('disabled',false);
                        if(data.error != 0){
                            $('.errorBox', $(form)).show().find('.errorMsg').text(data.error);
                        }else{
                            window.location.href = data.redirect;
                        }
                    },
                    error : function(a,b,c){
                        console.log(a);
                        //alert('Response Error');
                        $('.errorBox', $(form)).show().find('.errorMsg').html(a.responseText);
                    },
                    complete : function() {
                        $(form).find('button[type=submit]').attr('disabled',false);
                    }
                });
                return false;
            }
        });

        $('.modal').on('hidden.bs.modal', function (event) {
            $(this).find(".modal-body").empty();
        });

        $('#browseOPD').on('show.bs.modal', function (event) {
            var modalBody = $(".modal-body", $("#browseOPD"));
            $(this).removeData('bs.modal');
            modalBody.html("<p style='font-style: italic'>Loading..</p>");
            modalBody.load("<?=site_url("ajax/browse-opd")?>", function () {
                $("[name=selID][type=hidden]", modalBody).unbind().change(function () {
                    var kdSub = $(this).val().split('|');
                    $("[name=Kd_Urusan]").val(kdSub[0]);
                    $("[name=Kd_Bidang]").val(kdSub[1]);
                    $("[name=Kd_Unit]").val(kdSub[2]);
                    $("[name=Kd_Sub]").val(kdSub[3]);
                });
                $("[name=selText][type=hidden]", modalBody).unbind().change(function () {
                    $("[name=text-opd]").val($(this).val());
                });
            });
        });

        $("[name=Kd_Type]").change(function() {
            if($(this).val() == "RENJA") {
                $("[name=Kd_Tahun]").attr("disabled", false);
                $("[name=Kd_Tahun]").attr("required", "required");
            }
            else {
                $("[name=Kd_Tahun]").attr("disabled", true);
                $("[name=Kd_Tahun]").attr("required", false);
            }
        }).trigger("change");

        <?php if(isset($tipe)) {
        ?>
        $("option[value=<?=$tipe?>]", $("[name=Kd_Type]")).attr("selected", true).change();
        <?php
        }
        ?>

    </script>
<?php $this->load->view('footer') ?>
