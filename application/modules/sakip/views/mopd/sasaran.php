<?php
/**
 * Created by PhpStorm.
 * User: Yoel Simanjuntak
 * Date: 30/09/2018
 * Time: 13:51
 */
$data = array();
$i = 0;
foreach ($res as $d) {
    $nmSub = "";
    $eplandb = $this->load->database("eplan", true);
    $eplandb->where(COL_KD_URUSAN, $d[COL_KD_URUSAN]);
    $eplandb->where(COL_KD_BIDANG, $d[COL_KD_BIDANG]);
    $eplandb->where(COL_KD_UNIT, $d[COL_KD_UNIT]);
    $eplandb->where(COL_KD_SUB, $d[COL_KD_SUB]);
    $subunit = $eplandb->get("ref_sub_unit")->row_array();
    if($subunit) {
        $nmSub = $subunit["Nm_Sub_Unit"];
    }

    $htmlTujuan = "";
    $htmlTujuan .= "<table>";
    $htmlTujuan .= "<tr>";
    $htmlTujuan .= "<td style='text-align: right; font-weight: bold'>Tujuan</td><td style='padding: 0px 3px'>:</td><td>".$d[COL_NM_TUJUANOPD]."</td>";
    $htmlTujuan .= "</tr>";
    $htmlTujuan .= "<tr>";
    $htmlTujuan .= "<td style='text-align: right; font-weight: bold'>Indikator</td><td style='padding: 0px 3px'>:</td><td>".$d[COL_NM_INDIKATORTUJUANOPD]."</td>";
    $htmlTujuan .= "</tr>";
    $htmlTujuan .= "</table>";

    $htmlIKU = "";
    $htmlIKU .= "<table>";
    $htmlIKU .= "<tr>";
    $htmlIKU .= "<td style='text-align: right; font-weight: bold'>Misi</td><td style='padding: 0px 3px'>:</td><td>".$d[COL_KD_MISI].".".$d[COL_NM_MISI]."</td>";
    $htmlIKU .= "</tr>";
    $htmlIKU .= "<tr>";
    $htmlIKU .= "<td style='text-align: right; font-weight: bold'>Tujuan</td><td style='padding: 0px 3px'>:</td><td>".$d[COL_KD_MISI].".".$d[COL_KD_TUJUAN].".".$d[COL_NM_TUJUAN]."</td>";
    $htmlIKU .= "</tr>";
    $htmlIKU .= "<tr>";
    $htmlIKU .= "<td style='text-align: right; font-weight: bold'>Ik. Tujuan</td><td style='padding: 0px 3px'>:</td><td>".$d[COL_KD_MISI].".".$d[COL_KD_TUJUAN].".".$d[COL_KD_INDIKATORTUJUAN].".".$d[COL_NM_INDIKATORTUJUAN]."</td>";
    $htmlIKU .= "</tr>";
    $htmlIKU .= "<tr>";
    $htmlIKU .= "<td style='text-align: right; font-weight: bold'>Sasaran</td><td style='padding: 0px 3px'>:</td><td>".$d[COL_KD_MISI].".".$d[COL_KD_TUJUAN].".".$d[COL_KD_INDIKATORTUJUAN].".".$d[COL_KD_SASARAN].".".$d[COL_NM_SASARAN]."</td>";
    $htmlIKU .= "</tr>";
    $htmlIKU .= "<tr>";
    $htmlIKU .= "<td style='text-align: right; font-weight: bold'>Ik. Sasaran</td><td style='padding: 0px 3px'>:</td><td>".$d[COL_KD_MISI].".".$d[COL_KD_TUJUAN].".".$d[COL_KD_INDIKATORTUJUAN].".".$d[COL_KD_SASARAN].".".$d[COL_KD_INDIKATORSASARAN].".".$d[COL_NM_INDIKATORSASARAN]."</td>";
    $htmlIKU .= "</tr>";
    $htmlIKU .= "</table>";

    $res[$i] = array(
        '<input type="checkbox" class="cekbox" name="cekbox[]" value="' . $d["ID"] . '" />',
        anchor('mopd/sasaran-edit/'.$d["ID"],$d[COL_KD_TUJUANOPD].".".$d[COL_KD_INDIKATORTUJUANOPD].".".$d[COL_KD_SASARANOPD]." ".$d[COL_NM_SASARANOPD]),
        $nmSub,
        $d[COL_KD_TAHUN_FROM]." s.d ".$d[COL_KD_TAHUN_TO],
        $htmlIKU,
        $htmlTujuan,

    );
    $i++;
}
$data = json_encode($res);
?>

<?php $this->load->view('header')
?>
    <section class="content-header">
        <h1><?= $title ?>  <small>Data</small></h1>
        <ol class="breadcrumb">
            <li>
                <a href="<?=site_url()?>"><i class="fa fa-dashboard"></i> Home</a>
            </li>
            <li class="active">
                Sasaran OPD
            </li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <p>
            <?=anchor('mopd/sasaran-del','<i class="far fa-trash"></i> Hapus',array('class'=>'cekboxaction btn btn-sm btn-danger','confirm'=>'Apa anda yakin?'))
            ?>
            <?=anchor('mopd/sasaran-add','<i class="far fa-plus"></i> Data Baru',array('class'=>'btn btn-sm btn-primary'))
            ?>
        </p>
        <div class="box box-default">
            <div class="box-body">
                <form id="dataform" method="post" action="#">
                    <table id="datalist" class="table table-bordered table-hover">

                    </table>
                </form>
            </div>
        </div>
    </section>

<?php $this->load->view('loadjs')?>
    <script type="text/javascript">
        $(document).ready(function() {
            var dataTable = $('#datalist').dataTable({
              "autoWidth": false,//"sDom": "Rlfrtip",
              "aaData": <?=$data?>,
              //"bJQueryUI": true,
              //"aaSorting" : [[5,'desc']],
              "scrollY" : "50vh",
              //"scrollX": "200%",
              "scrollX": true,
              "iDisplayLength": 100,
              "aLengthMenu": [[100, 1000, 5000, -1], [100, 1000, 5000, "Semua"]],
              "dom":"R<'row'<'col-sm-4'l><'col-sm-4'B><'col-sm-4'f>><'row'<'col-sm-12'tr>><'row'<'col-sm-5'i><'col-sm-7'p>>",
              "buttons": ['copyHtml5','excelHtml5','csvHtml5','pdfHtml5'],
              "columnDefs": [
                {"targets":[0],"className":"text-center"},
                {"targets":[3,4,5],"className":"nowrap"}
              ],
              "order": [[ 2, "asc" ],[ 1, "asc" ]],
              "aoColumns": [
                  {"sTitle": "<input type=\"checkbox\" id=\"cekbox\" class=\"\" />","sWidth":15,bSortable:false},
                  {"sTitle": "Sasaran","sWidth":800},
                  {"sTitle": "OPD"},
                  {"sTitle": "Periode"},
                  {"sTitle": "IKU Kota"},
                  {"sTitle": "Tujuan OPD"},
                  //{"sTitle": "Indikator Tujuan"},

              ]
            });
            $('#cekbox').click(function(){
                if($(this).is(':checked')){
                    $('.cekbox').prop('checked',true);
                    console.log('clicked');
                }else{
                    $('.cekbox').prop('checked',false);
                }
            });
        });
    </script>

<?php $this->load->view('footer')
?>
