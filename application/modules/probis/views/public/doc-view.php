<?php
$rlog = $this->db
->select("tdoclogs.*, COALESCE(_userinformation.Name, tdoclogs.LogBy) as LogName")
->join(TBL__USERINFORMATION,TBL__USERINFORMATION.'.'.COL_USERNAME." = ".TBL_TDOCLOGS.".".COL_LOGBY,"left")
->where(TBL_TDOCLOGS.'.'.COL_DOCID, $data[COL_DOCID])
->order_by(COL_LOGTIMESTAMP, 'desc')
->get(TBL_TDOCLOGS)
->result_array();


$badge = 'secondary';
if($data[COL_DOCSTATUS]=='PROSES') $badge = 'primary';
else if($data[COL_DOCSTATUS]=='SELESAI') $badge = 'success';
?>
<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="x-ua-compatible" content="ie=edge">

  <title><?=!empty($title) ? $title.' - '.$this->setting_web_name : $this->setting_web_name?></title>

  <!-- Font Awesome Icons -->
  <link rel="stylesheet" href="<?=base_url()?>assets/fonts/css/font-awesome.min.css" />
  <link rel="stylesheet" href="<?=base_url()?>assets/fonts/fontawesome-pro/web/css/all.min.css" />
  <!-- Theme style -->
  <link rel="stylesheet" href="<?=base_url()?>assets/themes/adminlte-new/dist/css/adminlte.min.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">

  <link rel="icon" type="image/png" href=<?=MY_IMAGEURL.$this->setting_web_logo?>>
  <style>
  .table.table-condensed td, .table.table-condensed th {
    padding: .5rem !important;
    font-size: 10pt !important;
    vertical-align: middle;
  }
  </style>
</head>
<body class="hold-transition layout-top-nav">
<div class="wrapper">
  <nav class="main-header navbar navbar-expand navbar-light navbar-white">
    <div class="container">
      <a href="<?=site_url()?>" class="navbar-brand">
        <img src="<?=MY_IMAGEURL.$this->setting_web_logo?>" alt="Logo" class="brand-image" style="opacity: .8">
        <span class="brand-text font-weight-light">
          <strong><?=strtoupper($this->setting_web_name)?></strong>&nbsp;<small class="d-none d-sm-inline"><?=$this->setting_web_desc?></small>
        </span>
      </a>
    </div>
  </nav>
  <div class="content-wrapper">
    <div class="content-header">
      <div class="container">
        <div class="row mb-2">
          <div class="col-sm-12">
            <h4 class="m-0 font-weight-light"><?=$title?></h4>
          </div>
        </div>
      </div>
    </div>

    <div class="content">
      <div class="container">
        <div class="row">
          <div class="col-sm-6">
            <div class="card card-outline card-info">
              <div class="card-header">
                <h6 class="card-title font-weight-bold">RINCIAN</h6>
                <div class="card-tools">
                  <span data-toggle="tooltip" title="Status : <?=$data[COL_DOCSTATUS]?>" class="badge bg-<?=$badge?>"><?=$data[COL_DOCSTATUS]?></span>
                </div>
              </div>
              <div class="card-body">
                <div class="form-group">
                  <label>SKPD</label>
                  <select class="form-control" name="<?=COL_IDSKPD?>" style="width: 100%;" disabled>
                    <?=GetCombobox("select * from mskpd where SkpdIsAktif=1 order by SkpdNama", COL_SKPDID, COL_SKPDNAMA, $data[COL_IDSKPD])?>
                  </select>
                </div>
                <div class="form-group">
                  <div class="row">
                    <div class="col-sm-9">
                      <label>NAMA DOKUMEN</label>
                      <input type="text" class="form-control" placeholder="Nama / Judul Dokumen" name="<?=COL_DOCNAME?>" value="<?=!empty($data)?$data[COL_DOCNAME]:''?>" <?=$data[COL_DOCSTATUS]=='SELESAI'?'disabled':''?> />
                    </div>
                    <div class="col-sm-3">
                      <label>TAHUN</label>
                      <input type="number" class="form-control" placeholder="Tahun" name="<?=COL_DOCTAHUN?>" value="<?=!empty($data)?$data[COL_DOCTAHUN]:''?>" <?=$data[COL_DOCSTATUS]=='SELESAI'?'disabled':''?> />
                    </div>
                  </div>

                </div>
                <!--<div class="form-group">
                  <div class="row">
                    <div class="col-sm-9">
                      <label>NOMOR</label>
                      <input type="text" class="form-control" placeholder="Nomor Dokumen" name="<?=COL_DOCNOMOR?>" value="<?=!empty($data)?$data[COL_DOCNOMOR]:''?>" <?=$data[COL_DOCSTATUS]=='SELESAI'?'disabled':''?> />
                    </div>
                    <div class="col-sm-3">
                      <label>TAHUN</label>
                      <input type="number" class="form-control" placeholder="Tahun" name="<?=COL_DOCTAHUN?>" value="<?=!empty($data)?$data[COL_DOCTAHUN]:''?>" <?=$data[COL_DOCSTATUS]=='SELESAI'?'disabled':''?> />
                    </div>
                  </div>
                </div>-->
                <div class="form-group">
                  <label>KETERANGAN</label>
                  <textarea class="form-control" rows="2" placeholder="Keterangan / Rincian / Catatan Tambahan" name="<?=COL_DOCREMARKS?>" <?=$data[COL_DOCSTATUS]=='SELESAI'?'disabled':''?>><?=!empty($data)?$data[COL_DOCREMARKS]:''?></textarea>
                </div>
                <?php
                if($data[COL_DOCSTATUS]=='SELESAI') {
                  ?>
                  <div class="form-group">
                    <label>KETERANGAN AKHIR</label>
                    <textarea class="form-control" rows="2" name="<?=COL_DOCREMARKS2?>" <?=$data[COL_DOCSTATUS]=='SELESAI'?'disabled':''?>><?=!empty($data)?$data[COL_DOCREMARKS2]:''?></textarea>
                  </div>
                  <?php
                }
                ?>
                <div class="form-group">
                  <label>LAMPIRAN</label>
                  <div id="div-attachment">
                    <?php
                    if($data[COL_DOCSTATUS]!='SELESAI') {
                      ?>
                      <div class="input-group mb-2">
                        <div class="input-group-prepend">
                          <span class="input-group-text"><i class="fad fa-paperclip"></i></span>
                        </div>
                        <div class="custom-file">
                          <input type="file" class="custom-file-input" name="file" accept="image/*,application/pdf,application/vnd.ms-excel">
                          <label class="custom-file-label" for="file">PILIH FILE</label>
                        </div>
                      </div>
                      <p class="mt-2 text-sm text-muted mb-0 font-italic">
                        <strong>CATATAN:</strong><br />
                        <ul class="text-sm text-muted mb-0 font-italic" style="padding-left: 2rem !important">
                          <li>Unggah dokumen akan memperbarui dokumen yang sudah ada sebelumnya.</li>
                          <li>Besar file / dokumen maksimum <strong>5 MB</strong>.</li>
                          <li>Jenis file / dokumen yang diperbolehkan hanya dalam format <strong>JPG / JPEG / PNG</strong>, <strong>PDF</strong>, <strong>Word</strong>, dan <strong>Excel</strong>.</li>
                        </ul>
                      </p>
                      <?php
                    }
                    ?>

                    <?php
                    $file = $data[COL_DOCURL];
                    if(file_exists(MY_UPLOADPATH.$data[COL_DOCURL])) {
                      ?>
                      <ul class="todo-list ui-sortable" data-widget="todo-list">
                        <li>
                          <div class="d-inline mr-2">
                            <i class="far fa-paperclip"></i>
                          </div>
                          <span class="text font-italic"><?=$data[COL_DOCURL]?></span>
                          <div class="tools">
                            <a href="javascript:window.open('<?=MY_UPLOADURL.$data[COL_DOCURL]?>')"><i class="far fa-download"></i></a>
                          </div>
                        </li>
                      </ul>
                      <?php
                    }
                    ?>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-sm-6">
            <div class="card card card-outline card-info">
              <div class="card-header">
                <h6 class="card-title font-weight-bold">KOREKSI / CATATAN</h6>
              </div>
              <div class="card-body p-0">
                <table class="table table-bordered table-condensed">
                  <thead>
                    <tr>
                      <th style="width: 50px; white-space: nowrap">WAKTU</th>
                      <th>KETERANGAN</th>
                      <th style="width: 50px; white-space: nowrap">OLEH</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php
                    if(!empty($rlog)) {
                      foreach($rlog as $r) {
                        $desc_ = $r[COL_LOGREMARKS];
                        $files_ = "";
                        if(!empty($r[COL_LOGFILE])) {
                          $arrfile = explode(",", $r[COL_LOGFILE]);
                          foreach($arrfile as $f) {
                            if(file_exists(MY_UPLOADPATH.$f)) $files_ .= '<a href="'.(MY_UPLOADURL.$f).'" target="_blank"><i class="far fa-link pr-2"></i></a>';
                          }
                        }
                        ?>
                        <tr>
                          <td style="width: 50px; white-space: nowrap"><?=date('d-m-Y', strtotime($r[COL_LOGTIMESTAMP]))?>&nbsp;<sup class="font-italic"><?=date('H:i', strtotime($r[COL_LOGTIMESTAMP]))?></sup></td>
                          <td><?=$desc_?><?=!empty($r[COL_LOGFILE])?'<span class="pull-right">'.$files_.'</span>':''?></td>
                          <td style="max-width:250px; overflow:hidden !important; text-overflow:ellipsis; white-space:nowrap; font-style: italic"><?=$r['LogName']?></td>
                        </tr>
                        <?php
                      }
                    } else {
                      ?>
                      <tr>
                        <td colspan="3" class="text-center font-italic text-sm">KOSONG</td>
                      </tr>
                      <?php
                    }
                    ?>
                  </tbody>
                </table>
              </div>
            </div>
        </div>
      </div>
    </div>
  </div>
  <footer class="main-footer">
      <div class="float-right d-none d-sm-inline">
          <b>Version</b> <?=$this->setting_web_version?>
      </div>
      <strong>Copyright &copy; <?=date("Y")?> <?=$this->setting_web_name?></strong>. Strongly developed by <b>Partopi Tao</b>.
  </footer>
</div>
<!-- ./wrapper -->

<!-- REQUIRED SCRIPTS -->

<!-- jQuery -->
<script src="<?=base_url()?>assets/themes/adminlte-new/plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- Bootstrap 4 -->
<script src="<?=base_url()?>assets/themes/adminlte-new/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- AdminLTE App -->
<script src="<?=base_url()?>assets/themes/adminlte-new/dist/js/adminlte.js"></script>
</body>
</html>
