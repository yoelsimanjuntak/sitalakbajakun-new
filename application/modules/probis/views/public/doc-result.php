<?php
$docType = strtoupper($data[COL_DOCTYPE]);
if($docType=='SK') $docType = 'Surat Keputusan Bupati Batu Bara';
else if($docType=='PERBUP') $docType = 'Peraturan Bupati Batu Bara';
else if($docType=='PERDA') $docType = 'Peraturan Daerah Kabupaten Batu Bara';
?>
<html>
<head>
  <link rel="icon" type="image/png" href=<?=MY_IMAGEURL.$this->setting_web_logo?>>
  <title><?=$this->setting_web_name.' - Hasil Eksaminasi'?></title>
  <style>
  body {
    font-family: 'Source Sans Pro','Helvetica Neue',Helvetica,Arial,sans-serif;
  }
  th, td {
    padding: 5px;
  }
  table {
    width: 100%;
    border-collapse: collapse;
  }
  table, th, td {
    /*border: 1px solid black !important;*/
  }
  </style>
</head>
<body>
  <div style="width: 100%">
    <p style="text-align: justify">
      Draft Rancangan <?=$docType?> tentang: <strong>"<?=$data[COL_DOCNAME]?>"</strong> telah dieksaminasi sesuai dengan Peraturan Perundang-undangan sebagai berikut:
    </p>
    <ol style="text-align: justify">
      <li>Undang-Undang Nomor 5 Tahun 2007 tentang Pembentukan Kabupaten Batu Bara di Provinsi Sumatera Utara;</li>
      <li>Undang-Undang Nomor 23 Tahun 2014 tentang Pemerintah Daerah;</li>
      <li>Peraturan Presiden Nomor 16 Tahun 2018 tentang Pengadaan Barang / Jasa Pemerintah;</li>
      <li>Peraturan Menteri Dalam Negeri Nomor 8 Tahun 2015 tentang Pembentukan Produk Hukum Daerah sebagaimana telah diubah dengan Peraturan Menteri Dalam Negeri Nomor 120 Tahun 2018 tentang Perubahan Atas Peraturan Menteri Dalam Negeri Nomor 80 Tahun 2015 tentang Pembentukan Produk Hukum Daerah; dan</li>
      <li>Peraturan Daerah Kabupaten Batu Baran Nomor 7 Tahun 2016 tentang Pembentukan Perangkat Daerah Kabupaten Batu Bara sebagaimana telah diubah beberapa kali terakhir dengan Peraturan Daerah Kabupaten Batu Bara Nomor 12 Tahun 2021 tentang Perubahan Ketiga Atas Peraturan Daerah Kabupaten Batu Bara Nomor 7 Tahun 2016 tentang Pembentukan Perangkat Daerah Kabupaten Batu Bara.</li>
    </ol>
  </div>
  <br />
  <table width="100%">
    <tr>
      <td style="width: 70%;"></td>
      <td style="white-space: nowrap">
        dto.<br />
        <strong>
          KEPALA BAGIAN HUKUM<br />SETDAKAB BATU BARA<br /><br />
          <img src="<?=MY_UPLOADURL.'qrcodes/'.$data[COL_DOCID].'.png'?>" style="width: 80px" /><br /><br />
          DEDE IRFAN, SH<br />
          PENATA TK. I<br />
          NIP. 19840919 201101 1 009
        </strong>
      </td>
    </tr>
  </table>
</body>
</html>
