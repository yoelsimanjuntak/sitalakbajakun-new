<form id="form-log" action="<?=current_url()?>" enctype="multipart/form-data">
  <div class="form-group">
    <label>CATATAN</label>
    <textarea class="form-control" rows="4" placeholder="Uraian Catatan.." name="<?=COL_LOGREMARKS?>"></textarea>
  </div>
</form>

<script type="text/javascript">
$(document).ready(function(){
  bsCustomFileInput.init();
  $('#form-log').validate({
    ignore: "input[type='file']",
    submitHandler: function(form) {
      var modal = $(form).closest('modal');
      if(modal) {
        var btnSubmit = $('button[type=submit]', modal);
        var txtSubmit = btnSubmit.innerHTML;
        btnSubmit.html('<i class="fad fa-circle-notch fa-spin"></i>');
        btnSubmit.attr('disabled', true);
      }

      $(form).ajaxSubmit({
        dataType: 'json',
        type : 'post',
        success: function(res) {
          if(res.error != 0) {
            toastr.error(res.error);
          } else {
            toastr.success(res.success);
            setTimeout(function(){
              location.reload();
            }, 1000);
          }
        },
        error: function() {
          toastr.error('SERVER ERROR');
        },
        complete: function() {
          btnSubmit.html(txtSubmit);
          btnSubmit.attr('disabled', false);
        }
      });

      return false;
    }
  });
});
</script>
