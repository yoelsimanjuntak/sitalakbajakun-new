<?php
$ruser = GetLoggedUser();
$rlog = $this->db
->select("probis_tdoclogs.*, COALESCE(_userinformation.Name, probis_tdoclogs.LogBy) as LogName")
->join(TBL__USERINFORMATION,TBL__USERINFORMATION.'.'.COL_USERNAME." = ".TBL_PROBIS_TDOCLOGS.".".COL_LOGBY,"left")
->where(TBL_PROBIS_TDOCLOGS.'.'.COL_DOCID, $data[COL_DOCID])
->order_by(COL_LOGTIMESTAMP, 'desc')
->get(TBL_PROBIS_TDOCLOGS)
->result_array();


$badge = 'secondary';
if($data[COL_DOCSTATUS]=='VERIFIKASI') $badge = 'primary';
else if($data[COL_DOCSTATUS]=='FINAL') $badge = 'success';

?>
<style>
.input-group span.select2-selection {
  border-top-right-radius: 0 !important;
  border-bottom-right-radius: 0 !important;
}
</style>
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h3 class="m-0 text-dark font-weight-light"><?=strtoupper($title)?></h3>
      </div>
      <?php
      if($ruser[COL_ROLEID]==ROLEADMIN && $data[COL_DOCSTATUS]!='FINAL') {
        ?>
        <div class="col-sm-6 text-right">
          <a id="btn-finish" href="<?=site_url('probis/doc/finish/'.$data[COL_DOCID])?>" class="btn btn-info font-weight-bold"><i class="far fa-check-circle"></i> VERIFIKASI SELESAI</a>
        </div>
        <?php
      }
      ?>
    </div>
  </div>
</div>
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-sm-6">
        <div class="card card-outline card-info">
          <div class="card-header">
            <h6 class="card-title font-weight-bold">RINCIAN</h6>
            <div class="card-tools">
              <span data-toggle="tooltip" title="Status : <?=$data[COL_DOCSTATUS]?>" class="badge bg-<?=$badge?>"><?=$data[COL_DOCSTATUS]?></span>
            </div>
          </div>
          <?=form_open_multipart(current_url(), array('role'=>'form','id'=>'form-main','class'=>'form-horizontal'))?>
          <div class="card-body">
            <div class="form-group">
              <label>SKPD</label>
              <select class="form-control" name="<?=COL_IDSKPD?>" style="width: 100%;" disabled>
                <?=GetCombobox("select * from sakipv2_skpd where SkpdIsAktif=1 order by SkpdNama", COL_SKPDID, COL_SKPDNAMA, $data[COL_IDSKPD])?>
              </select>
            </div>
            <div class="form-group">
              <div class="row">
                <div class="col-sm-9">
                  <label>NAMA DOKUMEN</label>
                  <input type="text" class="form-control" placeholder="Nama / Judul Dokumen" name="<?=COL_DOCNAME?>" value="<?=!empty($data)?$data[COL_DOCNAME]:''?>" <?=$data[COL_DOCSTATUS]=='FINAL'?'disabled':''?> />
                </div>
                <div class="col-sm-3">
                  <label>TAHUN</label>
                  <input type="number" class="form-control" placeholder="Tahun" name="<?=COL_DOCTAHUN?>" value="<?=!empty($data)?$data[COL_DOCTAHUN]:''?>" <?=$data[COL_DOCSTATUS]=='FINAL'?'disabled':''?> />
                </div>
              </div>

            </div>
            <!--<div class="form-group">
              <div class="row">
                <div class="col-sm-9">
                  <label>NOMOR</label>
                  <input type="text" class="form-control" placeholder="Nomor Dokumen" name="<?=COL_DOCNOMOR?>" value="<?=!empty($data)?$data[COL_DOCNOMOR]:''?>" <?=$data[COL_DOCSTATUS]=='FINAL'?'disabled':''?> />
                </div>
                <div class="col-sm-3">
                  <label>TAHUN</label>
                  <input type="number" class="form-control" placeholder="Tahun" name="<?=COL_DOCTAHUN?>" value="<?=!empty($data)?$data[COL_DOCTAHUN]:''?>" <?=$data[COL_DOCSTATUS]=='FINAL'?'disabled':''?> />
                </div>
              </div>
            </div>-->
            <div class="form-group">
              <label>KETERANGAN</label>
              <textarea class="form-control" rows="2" placeholder="Keterangan / Rincian / Catatan Tambahan" name="<?=COL_DOCREMARKS?>" <?=$data[COL_DOCSTATUS]=='FINAL'?'disabled':''?>><?=!empty($data)?$data[COL_DOCREMARKS]:''?></textarea>
            </div>
            <?php
            if($data[COL_DOCSTATUS]=='FINAL') {
              ?>
              <div class="form-group">
                <label>KETERANGAN AKHIR</label>
                <textarea class="form-control" rows="2" name="<?=COL_DOCREMARKS2?>" <?=$data[COL_DOCSTATUS]=='FINAL'?'disabled':''?>><?=!empty($data)?$data[COL_DOCREMARKS2]:''?></textarea>
              </div>
              <?php
            }
            ?>
            <div class="form-group">
              <label>LAMPIRAN</label>
              <div id="div-attachment">
                <?php
                if($data[COL_DOCSTATUS]!='FINAL') {
                  ?>
                  <div class="input-group mb-2">
                    <div class="input-group-prepend">
                      <span class="input-group-text"><i class="fad fa-paperclip"></i></span>
                    </div>
                    <div class="custom-file">
                      <input type="file" class="custom-file-input" name="file" accept="image/*,application/pdf,application/vnd.ms-excel">
                      <label class="custom-file-label" for="file">PILIH FILE</label>
                    </div>
                  </div>
                  <p class="mt-2 text-sm text-muted mb-0 font-italic">
                    <strong>CATATAN:</strong><br />
                    <ul class="text-sm text-muted mb-0 font-italic" style="padding-left: 2rem !important">
                      <li>Unggah dokumen akan memperbarui dokumen yang sudah ada sebelumnya.</li>
                      <li>Besar file / dokumen maksimum <strong>5 MB</strong>.</li>
                      <li>Jenis file / dokumen yang diperbolehkan hanya dalam format <strong>JPG / JPEG / PNG</strong>, <strong>PDF</strong>, <strong>Word</strong>, dan <strong>Excel</strong>.</li>
                    </ul>
                  </p>
                  <?php
                }
                ?>

                <?php
                $file = $data[COL_DOCURL];
                if(file_exists(MY_UPLOADPATH.$data[COL_DOCURL])) {
                  ?>
                  <ul class="todo-list ui-sortable" data-widget="todo-list">
                    <li>
                      <div class="d-inline mr-2">
                        <i class="far fa-paperclip"></i>
                      </div>
                      <span class="text font-italic"><?=$data[COL_DOCURL]?></span>
                      <div class="tools">
                        <a href="javascript:window.open('<?=MY_UPLOADURL.$data[COL_DOCURL]?>')"><i class="far fa-download"></i></a>
                      </div>
                    </li>
                  </ul>
                  <?php
                }
                ?>
              </div>
            </div>
          </div>
          <div class="card-footer">
            <a href="<?=site_url('probis/doc/index/'.strtolower($data[COL_DOCTYPE]).'/'.strtolower($data[COL_DOCSTATUS]))?>" class="btn btn-outline-secondary btn-sm"><i class="far fa-arrow-circle-left"></i>&nbsp;KEMBALI</a>&nbsp;
            <button type="submit" class="btn btn-outline-info btn-sm" <?=$data[COL_DOCSTATUS]=='FINAL'?'disabled':''?>><i class="far fa-check-circle"></i>&nbsp;PERBARUI</button>
          </div>
          <?=form_close()?>
        </div>
      </div>
      <div class="col-sm-6">
        <div class="card card card-info">
          <div class="card-header">
            <h6 class="card-title font-weight-bold">KOREKSI / CATATAN</h6>
          </div>
          <div class="card-body p-0">
            <table class="table table-bordered">
              <thead>
                <tr>
                  <th style="width: 50px; white-space: nowrap">WAKTU</th>
                  <th>KETERANGAN</th>
                  <th style="width: 50px; white-space: nowrap">OLEH</th>
                </tr>
              </thead>
              <tbody>
                <?php
                if(!empty($rlog)) {
                  foreach($rlog as $r) {
                    $desc_ = $r[COL_LOGREMARKS];
                    $files_ = "";
                    if(!empty($r[COL_LOGFILE])) {
                      $arrfile = explode(",", $r[COL_LOGFILE]);
                      foreach($arrfile as $f) {
                        if(file_exists(MY_UPLOADPATH.$f)) $files_ .= '<a href="'.(MY_UPLOADURL.$f).'" target="_blank"><i class="far fa-link pr-2"></i></a>';
                      }
                    }
                    ?>
                    <tr>
                      <td style="width: 50px; white-space: nowrap"><?=date('d-m-Y', strtotime($r[COL_LOGTIMESTAMP]))?>&nbsp;<sup class="font-italic"><?=date('H:i', strtotime($r[COL_LOGTIMESTAMP]))?></sup></td>
                      <td><?=$desc_?><?=!empty($r[COL_LOGFILE])?'<span class="pull-right">'.$files_.'</span>':''?></td>
                      <td style="max-width:250px; overflow:hidden !important; text-overflow:ellipsis; white-space:nowrap; font-style: italic"><?=$r['LogName']?></td>
                    </tr>
                    <?php
                  }
                } else {
                  ?>
                  <tr>
                    <td colspan="3" class="text-center font-italic text-sm">KOSONG</td>
                  </tr>
                  <?php
                }
                ?>
              </tbody>
            </table>
          </div>
          <div class="card-footer text-right">
            <a id="btn-add-log" href="<?=site_url('probis/doc/form-log/'.$data[COL_DOCID])?>" class="btn btn-outline-primary btn-sm"><i class="far fa-edit"></i>&nbsp;TAMBAH</a>
          </div>
        </div>
    </div>
  </div>
</section>
<div class="modal fade" id="modalLog" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">TAMBAH CATATAN</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true"><i class="fas fa-close"></i></span>
        </button>
      </div>
      <div class="modal-body">
      </div>
      <div class="modal-footer d-block">
        <div class="row">
          <div class="col-lg-12 text-center">
            <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal"><i class="far fa-times-circle"></i>&nbsp;BATAL</button>
            <button type="submit" class="btn btn-primary btn-sm btn-submit"><i class="far fa-plus-circle"></i>&nbsp;SUBMIT</button>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="modalStatus" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">EKSAMINASI SELESAI</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true"><i class="fas fa-close"></i></span>
        </button>
      </div>
      <div class="modal-body">
      </div>
      <div class="modal-footer d-block">
        <div class="row">
          <div class="col-lg-12 text-center">
            <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal"><i class="far fa-times-circle"></i>&nbsp;BATAL</button>
            <button type="submit" class="btn btn-primary btn-sm btn-submit"><i class="far fa-plus-circle"></i>&nbsp;SUBMIT</button>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
var modalLog = $('#modalLog');
var modalStatus = $('#modalStatus');
$(document).ready(function() {
  bsCustomFileInput.init();
  modalLog.on('hidden.bs.modal', function (e) {
    $('.modal-body', modalLog).empty();
  });
  modalStatus.on('hidden.bs.modal', function (e) {
    $('.modal-body', modalStatus).empty();
  });

  $('#form-main').validate({
    ignore: "input[type='file']",
    submitHandler: function(form) {
      var btnSubmit = $('button[type=submit]', form);
      var txtSubmit = btnSubmit.html();
      btnSubmit.html('<i class="fad fa-circle-notch fa-spin"></i>');
      btnSubmit.attr('disabled', true);

      $(form).ajaxSubmit({
        dataType: 'json',
        type : 'post',
        success: function(res) {
          if(res.error != 0) {
            toastr.error(res.error);
          } else {
            toastr.success(res.success);
            if(res.redirect) {
              location.href = res.redirect;
            } else {
              setTimeout(function(){
                location.reload();
              }, 1000);
            }
          }
        },
        error: function(data) {
          toastr.error('SERVER ERROR');
        },
        complete: function() {
          btnSubmit.html(txtSubmit);
          btnSubmit.attr('disabled', false);
        }
      });
      return false;
    }
  });

  $('#btn-add-log').click(function(){
    var url = $(this).attr('href');
    modalLog.modal('show');
    $('.modal-body', modalLog).html('<p class="font-italic mb-0 text-center"><i class="far fa-circle-notch fa-spin"></i>&nbsp;MEMUAT...</p>');
    $('.modal-body', modalLog).load(url, function(){
      $('button[type=submit]', modalLog).unbind('click').click(function(){
        $('form', modalLog).submit();
      });
    });
    return false;
  });

  $('#btn-finish').click(function(){
    var url = $(this).attr('href');
    modalStatus.modal('show');
    $('.modal-body', modalStatus).html('<p class="font-italic mb-0 text-center"><i class="far fa-circle-notch fa-spin"></i>&nbsp;MEMUAT...</p>');
    $('.modal-body', modalStatus).load(url, function(){
      $('button[type=submit]', modalStatus).unbind('click').click(function(){
        $('form', modalStatus).submit();
      });
    });
    return false;
  });

  $('#btn-status').click(function(){
    if(confirm('Apakah anda yakin ingin memperbarui status dokumen?')) {
      var stat_ = $('[name=DocStatus]').val();
      var verif1 = $('[name=IsVerifKesesuaian]').is(':checked')?1:0;
      var verif2 = $('[name=IsVerifKode]').is(':checked')?1:0;
      var verif3 = $('[name=IsVerifKelengkapan]').is(':checked')?1:0;
      var remarks = $('[name=VerifiedRemarks]').val();
      $.post('<?=site_url('probis/doc/change-status/'.$data[COL_DOCID])?>', {
        stat: stat_,
        IsVerifKesesuaian: verif1,
        IsVerifKode: verif2,
        IsVerifKelengkapan: verif3,
        VerifiedRemarks: remarks
      }, function(res) {
        if(res.error != 0) {
          toastr.error(res.error);
        } else {
          toastr.success(res.success);
          setTimeout(function(){
            location.reload();
          }, 1000);
        }
      }, "json");
    }
  });
});
</script>
