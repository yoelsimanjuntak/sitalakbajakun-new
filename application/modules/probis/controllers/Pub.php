<?php
class Pub extends MY_Controller {
  function __construct() {
    parent::__construct();
  }

  public function doc_result($id) {
    $rdata = $this->db
    ->where(COL_DOCID, $id)
    ->get(TBL_PROBIS_TDOCS)
    ->row_array();

    if(empty($rdata)) {
      show_error('DATA TIDAK DITEMUKAN');
      exit();
    }

    $data['title'] = 'RINCIAN DOKUMEN';
    $data['data'] = $rdata;

    // generating QR
    if(!file_exists(MY_UPLOADPATH.'qrcodes/'.$rdata[COL_DOCID].'.png')) {
      //file_put_contents(MY_UPLOADPATH.'qrcodes/'.$rdata[COL_DOCID].'.png', file_get_contents('https://api.qrserver.com/v1/create-qr-code/?size=150x150&data='.urlencode(site_url('site/pub/doc-view/'.$rdata[COL_DOCID]))));
      $ch = curl_init('https://api.qrserver.com/v1/create-qr-code/?size=150x150&data='.urlencode(site_url('site/pub/doc-view/'.$rdata[COL_DOCID])));
      //$fp = fopen(MY_UPLOADPATH.'qrcodes/'.$rdata[COL_DOCID].'.png', 'wb');
      //curl_setopt($ch, CURLOPT_FILE, $fp);
      //curl_setopt($ch, CURLOPT_HEADER, 0);
      curl_setopt($ch,CURLOPT_SSL_VERIFYPEER, false);
      curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
      curl_setopt($ch, CURLOPT_FOLLOWLOCATION, false);
      $img = curl_exec($ch);
      curl_close($ch);
      file_put_contents(MY_UPLOADPATH.'qrcodes/'.$rdata[COL_DOCID].'.png', $img );
      //fclose($fp);
    }

    if(version_compare(phpversion(), '7.3.0', '<')) {
      $this->load->library('Mypdf');
      $mpdf = new Mypdf('','A4-L',0,'',15,15,30,16);
      $mpdf = $mpdf->pdf;
    } else {
      require_once FCPATH.'/vendor/autoload.php';
      $mpdf = new \Mpdf\Mpdf(['orientation'=>'L']);
      $mpdf->setAutoTopMargin = 'stretch';
      $mpdf->setAutoBottomMargin = 'stretch';
    }

    $html = $this->load->view('site/public/doc-result', $data, TRUE);
    //echo $html;
    //exit;
    $htmlTitle = 'LEMBARAN EKSAMINASI';//$this->setting_web_name;
    $htmlTitleSub = 'NOMOR:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;/ HKM /&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;/ '.date('Y');//$this->setting_web_desc;
    $htmlLogo = MY_IMAGEURL.$this->setting_web_logo;
    $htmlHeader = @"
    <table style=\"border: none !important\" width=\"100%\">
      <tr>
        <td style=\"border: none !important; padding: 0 !important; width: 20%; white-space: nowrap; vertical-align: middle; text-align: right\"><img src=\"$htmlLogo\" style=\"width: 50px\" /></td>
        <td style=\"border: none !important; padding: 0 !important; font-size: 16pt; font-weight: bold; vertical-align: top; text-align: center\"><span style=\"text-decoration: underline\">$htmlTitle</span><br /><small style=\"font-weight: normal\">$htmlTitleSub</small></td>
        <td style=\"border: none !important; padding: 0 !important; width: 20%;\"></td>
      </tr>
    </table>
    <hr />
    ";

    $mpdf->SetTitle($htmlTitle);
    $mpdf->SetHTMLHeader($htmlHeader);
    $mpdf->SetWatermarkImage(MY_IMAGEURL.$this->setting_web_logo);
    $mpdf->showWatermarkImage = true;
    $mpdf->SetFooter('Dicetak melalui aplikasi '.$this->setting_web_name.' - '.$this->setting_web_desc);
    $mpdf->WriteHTML($html);
    $mpdf->use_kwt = true;
    $mpdf->showImageErrors = true;
    $mpdf->curlAllowUnsafeSslRequests = true;
    $mpdf->Output($this->setting_web_name.' - '.$htmlTitle.'.pdf', 'I');
  }

  public function doc_view($id) {
    $rdata = $this->db
    ->where(COL_DOCSTATUS, 'SELESAI')
    ->where(COL_DOCID, $id)
    ->get(TBL_TDOCS)
    ->row_array();

    if(empty($rdata)) {
      show_error('DATA TIDAK DITEMUKAN');
      exit();
    }

    $data['title'] = 'RINCIAN DOKUMEN';
    $data['data'] = $rdata;
    $this->load->view('site/public/doc-view', $data);
  }
}
