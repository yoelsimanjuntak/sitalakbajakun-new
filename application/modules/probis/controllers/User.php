<?php
class User extends MY_Controller {
  function __construct() {
      parent::__construct();
      //$this->load->library('encrypt');
      $this->load->model('muser');
      if(IsLogin() && GetLoggedUser()[COL_ROLEID] != ROLEADMIN) {
        //redirect('probis/home');
      }
  }

  function index() {
      if(!IsLogin()) {
          redirect('probis/user/login');
      }
      $loginuser = GetLoggedUser();
      if(!$loginuser || $loginuser[COL_ROLEID] != ROLEADMIN) {
          show_error('Anda tidak memiliki akses terhadap modul ini.');
          return;
      }

      $data['title'] = "Pengguna";
      $this->db->join(TBL__USERINFORMATION,TBL__USERINFORMATION.'.'.COL_USERNAME." = ".TBL__USERS.".".COL_USERNAME,"inner");
      $this->db->join(TBL__ROLES,TBL__ROLES.'.'.COL_ROLEID." = ".TBL__USERS.".".COL_ROLEID,"inner");
      $this->db->order_by(TBL__USERS.".".COL_USERNAME, 'asc');
      $data['res'] = $this->db->get(TBL__USERS)->result_array();
      //$this->load->view('user/index', $data);
      $this->template->load('backend', 'user/index', $data);
  }

  function Login(){
    if(IsLogin()) {
      redirect('probis/user/dashboard');
    }
    if(!empty($_POST)) {
      $this->form_validation->set_rules(array(
        array(
          'field' => 'UserName',
          'label' => 'UserName',
          'rules' => 'required'
        ),
        array(
          'field' => 'Password',
          'label' => 'Password',
          'rules' => 'required'
        )
      ));
      if($this->form_validation->run()) {
        $this->load->model('muser');
        $username = $this->input->post(COL_USERNAME);
        $password = $this->input->post(COL_PASSWORD);

        if($this->muser->authenticate($username, $password)) {
          if($this->muser->IsSuspend($username)) {
            ShowJsonError('MAAF, AKUN INI DI SUSPEND. SILAKAN HUBUNGI ADMINISTRATOR.');
            return;
          }

          $userdetails = $this->muser->getdetails($username);
          if($userdetails[COL_ROLEID] != ROLEADMIN && $userdetails[COL_ROLEID] != ROLEGUEST && $userdetails[COL_ROLEID] != ROLEAPIP) {
            $arrOPD = explode('.', $userdetails[COL_COMPANYID]);
            $rskpd = $this->db
            ->where(array(COL_SKPDURUSAN=>$arrOPD[0], COL_SKPDBIDANG=>$arrOPD[1], COL_SKPDUNIT=>$arrOPD[2], COL_SKPDSUBUNIT=>$arrOPD[3]))
            ->get(TBL_SAKIPV2_SKPD)
            ->row_array();
            if(empty($rskpd)) {
              ShowJsonError('LOGIN GAGAL. UNIT KERJA TIDAK DITEMUKAN.');
              exit();
            }

            $userdetails = array_merge($userdetails, array(COL_SKPDID=>$rskpd[COL_SKPDID]));
            //ShowJsonError('Maaf, anda tidak memiliki akses terhadap modul ini. Silakan hubungi Administrator untuk info lebih lanjut');
            //exit();
          }

          $this->db->where(COL_USERNAME, $username);
          $this->db->update(TBL__USERS, array(COL_LASTLOGIN=>date('Y-m-d H:i:s'), COL_LASTLOGINIP=>$this->input->ip_address()));

          SetLoginSession($userdetails);
          ShowJsonSuccess('LOGIN BERHASIL', array('redirect'=>site_url('probis/user/dashboard')));
        } else {
          ShowJsonError('USERNAME / PASSWORD TIDAK VALID.');
        }
      } else {
        ShowJsonError('Maaf, username / password yang anda masukkan tidak valid. Silakan coba kembali.');
        exit();
      }
    } else {
      $this->load->view('probis/user/login');
    }
  }
  function Logout(){
      UnsetLoginSession();
      redirect(site_url());
  }
  function Dashboard() {
      if(!IsLogin()) {
          redirect('probis/user/login');
      }

      if (!empty($_POST)) {
        $data['data'] = $_POST;
        SetSetting(SETTING_ORG_NAME, $this->input->post(SETTING_ORG_NAME));
        SetSetting(SETTING_ORG_ADDRESS, $this->input->post(SETTING_ORG_ADDRESS));
        SetSetting(SETTING_ORG_PHONE, $this->input->post(SETTING_ORG_PHONE));
        SetSetting(SETTING_ORG_FAX, $this->input->post(SETTING_ORG_FAX));
        SetSetting(SETTING_ORG_MAIL, $this->input->post(SETTING_ORG_MAIL));
        SetSetting(SETTING_WEB_NAME, $this->input->post(SETTING_WEB_NAME));
        SetSetting(SETTING_WEB_DESC, $this->input->post(SETTING_WEB_DESC));
        SetSetting(SETTING_WEB_DISQUS_URL, $this->input->post(SETTING_WEB_DISQUS_URL));
        SetSetting(SETTING_WEB_API_FOOTERLINK, $this->input->post(SETTING_WEB_API_FOOTERLINK));
        SetSetting(SETTING_WEB_LOGO, $this->input->post(SETTING_WEB_LOGO));
        SetSetting(SETTING_WEB_SKIN_CLASS, $this->input->post(SETTING_WEB_SKIN_CLASS));
        SetSetting(SETTING_WEB_PRELOADER, $this->input->post(SETTING_WEB_PRELOADER));
        SetSetting(SETTING_WEB_VERSION, $this->input->post(SETTING_WEB_VERSION));
        redirect(current_url());
      }

      $data['title'] = 'Dashboard';
  		$this->template->load('backend', 'probis/user/dashboard', $data);
  }
  function ChangePassword() {
      if(!IsLogin()) {
          redirect('probis/user/login');
      }
      $user = GetLoggedUser();
      $data['title'] = 'Change Password';
      $rules = array(
          array(
              'field' => 'OldPassword',
              'label' => 'Old Password',
              'rules' => 'required'
          ),
          array(
              'field' => COL_PASSWORD,
              'label' => COL_PASSWORD,
              'rules' => 'required'
          ),
          array(
              'field' => 'RepeatPassword',
              'label' => 'Repeat Password',
              'rules' => 'required|matches[Password]'
          )
      );
      $this->form_validation->set_rules($rules);

      if($this->form_validation->run()){
          $rcheck = $this->db->where(COL_USERNAME, $user[COL_USERNAME])->get(TBL__USERS)->row_array();
          if(!$rcheck) {
              redirect(site_url('probis/user/changepassword'));
          }
          if($rcheck[COL_PASSWORD] != md5($this->input->post("OldPassword"))) {
              redirect(site_url('probis/user/changepassword')."?nomatch=1");
          }
          $upd = $this->db->where(COL_USERNAME, $user[COL_USERNAME])->update(TBL__USERS, array(COL_PASSWORD=>md5($this->input->post(COL_PASSWORD))));
          if($upd) redirect(site_url('probis/user/changepassword')."?success=1");
          else redirect(site_url('probis/user/changepassword')."?error=1");
      }
      else {
        $this->template->load('backend', 'user/changepassword', $data);
      }
  }

  function add() {
    if(!IsLogin()) {
      redirect(site_url('probis/user/login'));
    }
    $loginuser = GetLoggedUser();
    if(!$loginuser || $loginuser[COL_ROLEID] != ROLEADMIN) {
      ShowJsonError('Anda tidak memiliki akses terhadap modul ini.');
      exit();
    }

    $data['title'] = "Pengguna";
    $data['edit'] = FALSE;
    if(!empty($_POST)) {
        $data['data'] = $_POST;
        $rules = $this->muser->rules(true, $this->input->post(COL_ROLEID));
        $this->form_validation->set_rules($rules);
        $this->form_validation->set_message('custom_rule', 'Karakter yang digunakan untuk username tidak tepat.');
        if($this->form_validation->run()) {
          $userdata = array(
            COL_USERNAME => $this->input->post(COL_USERNAME),
            COL_PASSWORD => md5($this->input->post(COL_PASSWORD)),
            COL_ROLEID => $this->input->post(COL_ROLEID),
            COL_ISSUSPEND => false
          );
          $userinfo = array(
            COL_USERNAME => $this->input->post(COL_USERNAME),
            COL_EMAIL => $this->input->post(COL_EMAIL),
            COL_NAME => $this->input->post(COL_NAME),
            COL_IDENTITYNO => $this->input->post(COL_IDENTITYNO),
            COL_BIRTHDATE => $this->input->post(COL_BIRTHDATE)?date('Y-m-d', strtotime($this->input->post(COL_BIRTHDATE))):null,
            COL_RELIGIONID => $this->input->post(COL_RELIGIONID),
            COL_GENDER => $this->input->post(COL_GENDER),
            COL_ADDRESS => $this->input->post(COL_ADDRESS),
            COL_PHONENUMBER => $this->input->post(COL_PHONENUMBER),
            COL_EDUCATIONID => $this->input->post(COL_EDUCATIONID),
            COL_UNIVERSITYNAME => $this->input->post(COL_UNIVERSITYNAME),
            COL_FACULTYNAME => $this->input->post(COL_FACULTYNAME),
            COL_MAJORNAME => $this->input->post(COL_MAJORNAME),
            COL_ISGRADUATED => $this->input->post(COL_ISGRADUATED) ? $this->input->post(COL_ISGRADUATED) : false,
            COL_GRADUATEDDATE => ($this->input->post(COL_ISGRADUATED) && $this->input->post(COL_GRADUATEDDATE) ? date('Y-m-d', strtotime($this->input->post(COL_GRADUATEDDATE))) : null),
            COL_YEAROFEXPERIENCE => $this->input->post(COL_YEAROFEXPERIENCE),
            COL_RECENTPOSITION => $this->input->post(COL_RECENTPOSITION),
            COL_RECENTSALARY => $this->input->post(COL_RECENTSALARY),
            COL_EXPECTEDSALARY => $this->input->post(COL_EXPECTEDSALARY),
            COL_REGISTEREDDATE => date('Y-m-d')
          );

          if($this->input->post(COL_ROLEID) != ROLEADMIN) {
            $userinfo[COL_COMPANYID] = $this->input->post(COL_COMPANYID);
          }

          $reg = $this->muser->register($userdata, $userinfo, array());

          $this->db->trans_commit();

          if($reg) {
            ShowJsonSuccess('Pengguna berhasil diinput.', array('redirect'=>site_url('probis/user/index')));
            exit();
          } else {
            ShowJsonError('Gagal menambahkan data 1.');
            exit();
          }
        }
        else {
          $err = validation_errors();
          ShowJsonError('ERROR: '.strip_tags($err));
          exit();
        }
    }
    else {
      $this->template->load('backend', 'user/form', $data);
    }
  }

  function edit($id) {
      if(!IsLogin()) {
          redirect(site_url('user/login'));
      }
      $loginuser = GetLoggedUser();
      if(!$loginuser || $loginuser[COL_ROLEID] != ROLEADMIN) {
          show_error('Anda tidak memiliki akses terhadap modul ini.');
          return;
      }
      $data['title'] = "Pengguna";
      $data['edit'] = TRUE;

      $data['data'] = $edited = $this->muser->getdetails($id);
      if(empty($edited)){
          show_404();
          return;
      }

      if(!empty($_POST)){
          $data['data'] = $_POST;
          $rules = $this->muser->rules(false, $edited[COL_ROLEID]);
          $this->form_validation->set_rules($rules);
          $this->form_validation->set_message('customAlpha', 'Karakter yang digunakan untuk username tidak tepat.');
          if($this->form_validation->run()){
              $config['upload_path'] = MY_UPLOADPATH;
              $config['allowed_types'] = UPLOAD_ALLOWEDTYPES;
              $config['max_size']	= 500;
              $config['max_width']  = 1024;
              $config['max_height']  = 768;
              $config['overwrite'] = FALSE;

              $this->load->library('upload',$config);
              if(!empty($_FILES["companyfile"]["name"])) {
                  if(!$this->upload->do_upload("companyfile")){
                      $data['upload_errors'] = $this->upload->display_errors();
                      $this->load->view('company/form', $data);
                      return;
                  }
              }

              $dataupload = $this->upload->data();
              /*$companydata = array(
                  COL_COMPANYNAME => $this->input->post(COL_COMPANYNAME),
                  COL_COMPANYADDRESS => $this->input->post(COL_COMPANYADDRESS),
                  COL_COMPANYTELP => $this->input->post(COL_COMPANYTELP),
                  COL_COMPANYFAX => $this->input->post(COL_COMPANYFAX),
                  COL_COMPANYWEBSITE => $this->input->post(COL_COMPANYWEBSITE),
                  COL_COMPANYEMAIL => $this->input->post(COL_COMPANYEMAIL),
                  COL_INDUSTRYTYPEID => $this->input->post(COL_INDUSTRYTYPEID)
              );
              if(!empty($dataupload) && $dataupload['file_name']) {
                  $companydata[COL_FILENAME] = $dataupload['file_name'];
              }*/

              $userdata = array(
                COL_NAME => $this->input->post(COL_NAME),
                COL_EMAIL => $this->input->post(COL_EMAIL),
                //COL_COMPANYID => join(",", $this->input->post(COL_COMPANYID)),
                COL_IDENTITYNO => $this->input->post(COL_IDENTITYNO),
                COL_BIRTHDATE => $this->input->post(COL_BIRTHDATE)?date('Y-m-d', strtotime($this->input->post(COL_BIRTHDATE))):null,
                COL_RELIGIONID => $this->input->post(COL_RELIGIONID),
                COL_GENDER => $this->input->post(COL_GENDER),
                COL_ADDRESS => $this->input->post(COL_ADDRESS),
                COL_PHONENUMBER => $this->input->post(COL_PHONENUMBER),
                COL_EDUCATIONID => $this->input->post(COL_EDUCATIONID),
                COL_UNIVERSITYNAME => $this->input->post(COL_UNIVERSITYNAME),
                COL_FACULTYNAME => $this->input->post(COL_FACULTYNAME),
                COL_MAJORNAME => $this->input->post(COL_MAJORNAME),
                COL_ISGRADUATED => $this->input->post(COL_ISGRADUATED) ? $this->input->post(COL_ISGRADUATED) : false,
                COL_GRADUATEDDATE => ($this->input->post(COL_ISGRADUATED) && $this->input->post(COL_GRADUATEDDATE) ? date('Y-m-d', strtotime($this->input->post(COL_GRADUATEDDATE))) : null),
                COL_YEAROFEXPERIENCE => $this->input->post(COL_YEAROFEXPERIENCE),
                COL_RECENTPOSITION => $this->input->post(COL_RECENTPOSITION),
                COL_RECENTSALARY => $this->input->post(COL_RECENTSALARY),
                COL_EXPECTEDSALARY => $this->input->post(COL_EXPECTEDSALARY)
              );
              if($this->input->post(COL_ROLEID) != ROLEUSER) {
                $userdata[COL_COMPANYID] = $this->input->post(COL_COMPANYID);
              }

              $this->db->where(COL_USERNAME, $edited[COL_USERNAME])->update(TBL__USERS, array(COL_ROLEID => $this->input->post(COL_ROLEID)));

              if($reg) {
                ShowJsonSuccess('Konten berhasil diinput.', array('redirect'=>site_url('probis/user/index')));
                exit();
              } else {
                ShowJsonError('Gagal menambahkan data.');
                exit();
              }
          }
          else {
            $this->template->load('backend', 'user/form', $data);
          }
      }
      else {
          $this->template->load('backend', 'user/form', $data);
      }
  }

  function delete() {
    if(!IsLogin()) {
        ShowJsonError('Silahkan login terlebih dahulu');
        return;
    }
    $loginuser = GetLoggedUser();
    if(!$loginuser || $loginuser[COL_ROLEID] != ROLEADMIN) {
        ShowJsonError('Anda tidak memiliki akses terhadap modul ini.');
        return;
    }
    $this->load->model('muser');
    $data = $this->input->post('cekbox');
    $deleted = 0;
    foreach ($data as $datum) {
        if($this->muser->delete($datum)) {
            $deleted++;
        }
    }
    if($deleted){
        ShowJsonSuccess($deleted." data dihapus");
    }else{
        ShowJsonError("Tidak ada data dihapus");
    }
  }

    function activate($opt = 0) {
        if(!IsLogin()) {
            ShowJsonError('Silahkan login terlebih dahulu');
            return;
        }
        $loginuser = GetLoggedUser();
        if(!$loginuser || $loginuser[COL_ROLEID] != ROLEADMIN) {
            ShowJsonError('Anda tidak memiliki akses terhadap modul ini.');
            return;
        }
        $data = $this->input->post('cekbox');
        $deleted = 0;
        foreach ($data as $datum) {
            if($opt == 0 || $opt == 1) {
                if($this->db->where(COL_USERNAME, $datum)->update(TBL__USERS, array(COL_ISSUSPEND=>$opt))) {
                    $deleted++;
                }
                if($opt == 1) {
                    $this->db->where(COL_USERNAME, $datum)->update(TBL__USERINFORMATION, array(COL_REGISTEREDDATE=>date('Y-m-d H:i:s')));
                }
            }
            else {
                if($this->db->where(COL_USERNAME, $datum)->update(TBL__USERS, array(COL_PASSWORD=>MD5('123456')))) {
                    $deleted++;
                }
            }
        }
        if($deleted){
            ShowJsonSuccess($deleted." data diubah");
        }else{
            ShowJsonError("Tidak ada data yang diubah");
        }
    }


}
 ?>
