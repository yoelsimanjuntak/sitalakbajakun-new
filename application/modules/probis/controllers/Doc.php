<?php
class Doc extends MY_Controller {

  public function __construct()
  {
    parent::__construct();
    if(!IsLogin()) {
      redirect('probis/user/login');
    }

    /*$ruser = GetLoggedUser();
    if($ruser[COL_ROLEID]!=ROLEADMIN) {
      show_error('TIDAK MEMILIKI HAK AKSES!');
      exit();
    }*/
  }

  public function index($tipe, $stat='') {
    $data['title'] = "DOKUMEN ".strtoupper($tipe);
    $data['tipe'] = $tipe;
    $data['stat'] = $stat;
    $this->template->load('backend', 'probis/doc/index', $data);
  }

  public function index_load($tipe, $stat='') {
    $ruser = GetLoggedUser();
    $start = $_POST['start'];
    $rowperpage = $_POST['length'];
    $filterSkpd = !empty($_POST['filterSkpd'])?$_POST['filterSkpd']:null;


    $orderables = array(null,COL_DOCNAME,COL_DOCNOMOR,COL_DOCTAHUN,null,COL_CREATEDON);
    $cols = array(COL_DOCNAME,COL_DOCNOMOR,COL_DOCTAHUN);
    $condSkpd = "1=1";

    if(!empty($filterSkpd)) {
      $condSkpd = "IdSkpd=".$filterSkpd;
    } else {
      if($ruser[COL_ROLEID]!=ROLEADMIN) {
        $condSkpd = "IdSkpd=".$ruser[COL_SKPDID];
      }
    }

    if(!empty($stat)) {
      $this->db->where(COL_DOCSTATUS, strtoupper($stat));
    }
    $queryAll = $this->db->where(COL_ISDELETED, 0)->where(COL_DOCTYPE, $tipe)->where($condSkpd)->get(TBL_PROBIS_TDOCS);

    $i = 0;
    foreach($cols as $item){
      if(!empty($_POST['search']['value'])){
        if($i===0) {
          $this->db->group_start();
          $this->db->like($item, $_POST['search']['value']);
        } else {
          $this->db->or_like($item, $_POST['search']['value']);
        }
        if(count($cols) - 1 == $i){
          $this->db->group_end();
        }
      }
      $i++;
    }

    if(!empty($_POST['order'])){
      if($orderables[$_POST['order']['0']['column']]==COL_CREATEDON) {
        $this->db->order_by('(select MAX(logs_.LogTimestamp) from probis_tdoclogs logs_ where logs_.DocId=probis_tdocs.DocId)', $_POST['order']['0']['dir']);
      } else {
        $this->db->order_by($orderables[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
      }

    }else if(!empty($orderdef)){
      $this->db->order_by('(select MAX(logs_.LogTimestamp) from probis_tdoclogs logs_ where logs_.DocId=probis_tdocs.DocId)', 'desc');
    }

    if(!empty($stat)) {
      $this->db->where(COL_DOCSTATUS, strtoupper($stat));
    }

    $q = $this->db
    ->select('probis_tdocs.*, _userinformation.Name, _userinformation.Name, sakipv2_skpd.SkpdNama, (select MAX(logs_.LogTimestamp) from probis_tdoclogs logs_ where logs_.DocId=probis_tdocs.DocId) as Log')
    ->where(COL_ISDELETED, 0)
    ->where(COL_DOCTYPE, $tipe)
    ->where($condSkpd)
    ->join(TBL__USERINFORMATION,TBL__USERINFORMATION.'.'.COL_USERNAME." = ".TBL_PROBIS_TDOCS.".".COL_CREATEDON,"left")
    ->join(TBL_SAKIPV2_SKPD,TBL_SAKIPV2_SKPD.'.'.COL_SKPDID." = ".TBL_PROBIS_TDOCS.".".COL_IDSKPD,"left")
    //->order_by(COL_CREATEDON, 'desc')
    ->get_compiled_select(TBL_PROBIS_TDOCS, FALSE);
    $rec = $this->db->query($q." LIMIT $rowperpage OFFSET $start")->result_array();
    $data = [];

    foreach($rec as $r) {
      $btndel = '';
      $btnprint = '';
      $sub = '';
      $badge = 'secondary';

      if($r[COL_DOCSTATUS]=='BARU' || $ruser[COL_ROLEID]==ROLEADMIN) {
        $btndel = '<a class="btn btn-xs btn-danger btn-action '.($r[COL_DOCSTATUS]=='VERIFIED'?'disabled':'').'" href="'.site_url('probis/doc/delete/'.$r[COL_DOCID]).'"><i class="far fa-times-circle"></i></a>&nbsp;';
      }
      if($r[COL_DOCSTATUS]=='FINAL') {
        //$btnprint = '&nbsp;<a class="btn btn-xs btn-success" href="'.site_url('probis/pub/doc-result/'.$r[COL_DOCID]).'" target="_blank"><i class="far fa-print"></i></a>';
      }

      if($r[COL_DOCSTATUS]=='VERIFIKASI') $badge = 'primary';
      else if($r[COL_DOCSTATUS]=='FINAL') $badge = 'success';

      if(empty($filterSkpd) && $ruser[COL_ROLEID]==ROLEADMIN) {
        $sub = '<br /><small class="font-italic">'.$r[COL_SKPDNAMA].'</small>';
      }
      $data[] = array(
        $btndel.'<a class="btn btn-xs btn-primary" href="'.site_url('probis/doc/view/'.$r[COL_DOCID]).'"><i class="far fa-search"></i></a>'.$btnprint,
        $r[COL_DOCNAME].$sub,
        !empty($r[COL_DOCNOMOR])?$r[COL_DOCNOMOR]:'-',
        $r[COL_DOCTAHUN],
        '<span class="badge bg-'.$badge.'">'.$r[COL_DOCSTATUS].'</span>',
        date('Y-m-d H:i', strtotime($r['Log']))
      );
    }

    $result = array(
      "draw" => $_POST['draw'],
      "recordsFiltered" => $this->db->query($q)->num_rows(),
      "recordsTotal" => $queryAll->num_rows(),
      "data" => $data,
    );

    echo json_encode($result);
    exit();
  }

  public function add($tipe) {
    $ruser = GetLoggedUser();
    if(!empty($_POST)) {
      $config['upload_path'] = MY_UPLOADPATH;
      $config['allowed_types'] = "jpg|jpeg|png|pdf|doc|docx|xls|xlsx";
      $config['max_size']	= 5120;
      $config['max_width']  = 4000;
      $config['max_height']  = 4000;
      $config['overwrite'] = FALSE;

      $this->load->library('upload',$config);

      $dat = array(
        COL_DOCTYPE=>$tipe,
        COL_DOCSTATUS=>'BARU',
        COL_IDSKPD=>$ruser[COL_ROLEID]==ROLEADMIN?$this->input->post(COL_IDSKPD):$ruser[COL_SKPDID],
        COL_DOCNAME=>$this->input->post(COL_DOCNAME),
        COL_DOCREMARKS=>$this->input->post(COL_DOCREMARKS),
        COL_DOCNOMOR=>$this->input->post(COL_DOCNOMOR),
        COL_DOCTAHUN=>$this->input->post(COL_DOCTAHUN),

        COL_CREATEDON=>date('Y-m-d H:i:s'),
        COL_CREATEDBY=>$ruser[COL_USERNAME]
      );

      $this->db->trans_begin();
      try {
        if(!empty($_FILES)) {
          $res = $this->upload->do_upload('file');
          if(!$res) {
            $err = $this->upload->display_errors('', '');
            throw new Exception($err);
          }
          $upl = $this->upload->data();
          $dat[COL_DOCURL] = $upl['file_name'];
        }

        $res = $this->db->insert(TBL_PROBIS_TDOCS, $dat);
        if(!$res) {
          $err = $this->db->error();
          throw new Exception('Error: '.$err['message']);
        }

        $docID = $this->db->insert_id();
        $res = $this->db->insert(TBL_PROBIS_TDOCLOGS, array(
          COL_DOCID=>$docID,
          COL_LOGREMARKS=>'Dokumen diterima.',
          COL_LOGFILE=>$dat[COL_DOCURL],
          COL_LOGBY=>$ruser[COL_USERNAME],
          COL_LOGTIMESTAMP=>date('Y-m-d H:i:s')
        ));
        if(!$res) {
          $err = $this->db->error();
          throw new Exception('Error: '.$err['message']);
        }

        $this->db->trans_commit();
        ShowJsonSuccess('ENTRI DATA BERHASIL.');
        return;
      } catch(Exception $ex) {
        $this->db->trans_rollback();
        ShowJsonError($ex->getMessage());
        return;
      }
    } else {
      $this->load->view('probis/doc/form');
    }
  }

  public function delete($id) {
    $ruser = GetLoggedUser();
    $rdata = $this->db->where(COL_DOCID, $id)->get(TBL_PROBIS_TDOCS)->row_array();
    if(empty($rdata)) {
      ShowJsonError('PARAMETER TIDAK VALID.');
      exit();
    }

    try {
      $res = $this->db->where(COL_DOCID, $id)->update(TBL_PROBIS_TDOCS, array(COL_ISDELETED=>1, COL_DELETEDBY=>$ruser[COL_USERNAME], COL_DELETEDON=>date('Y-m-d H:i:s')));
      if(!$res) {
        $err = $this->db->error();
        throw new Exception('Error: '.$err['message']);
      }

      $this->db->trans_commit();
      /*if(!empty($rdata[COL_DOCURL])&&file_exists(MY_UPLOADPATH.$rdata[COL_DOCURL])) {
        unlink(MY_UPLOADPATH.$rdata[COL_DOCURL]);
      }*/
      ShowJsonSuccess('HAPUS DATA BERHASIL.');
      return;
    } catch(Exception $ex) {
      $this->db->trans_rollback();
      ShowJsonError($ex->getMessage());
      return;
    }
  }

  public function view($id) {
    $ruser = GetLoggedUser();
    $rdata = $this->db
    ->where(COL_DOCID, $id)
    ->get(TBL_PROBIS_TDOCS)
    ->row_array();

    if(empty($rdata)) {
      show_error('DATA TIDAK DITEMUKAN');
      exit();
    }

    if($ruser[COL_ROLEID]!=ROLEADMIN && $ruser[COL_SKPDID]!=$rdata[COL_IDSKPD]) {
      show_error('MAAF, ANDA TIDAK MEMILIKI HAK AKSES');
      exit();
    }

    if(!empty($_POST)) {
      $dat = array(
        COL_DOCNAME=>$this->input->post(COL_DOCNAME),
        COL_DOCREMARKS=>$this->input->post(COL_DOCREMARKS),
        COL_DOCNOMOR=>$this->input->post(COL_DOCNOMOR),
        COL_DOCTAHUN=>$this->input->post(COL_DOCTAHUN),

        COL_UPDATEDON=>date('Y-m-d H:i:s'),
        COL_UPDATEDBY=>$ruser[COL_USERNAME]
      );

      $this->db->trans_begin();
      try {
        if(!empty($_FILES) && !empty($_FILES['file'])) {
          $config['upload_path'] = MY_UPLOADPATH;
          $config['allowed_types'] = "jpg|jpeg|png|pdf|doc|docx|xls|xlsx";
          $config['max_size']	= 5120;
          $config['max_width']  = 4000;
          $config['max_height']  = 4000;
          $config['overwrite'] = FALSE;

          $this->load->library('upload',$config);

          $res = $this->upload->do_upload('file');
          if($res) {
            $upl = $this->upload->data();
            $dat[COL_DOCURL] = $upl['file_name'];
          }
        }

        $res = $this->db->where(COL_DOCID, $id)->update(TBL_PROBIS_TDOCS, $dat);
        if(!$res) {
          $err = $this->db->error();
          throw new Exception('Error: '.$err['message']);
        }

        $res = $this->db->insert(TBL_PROBIS_TDOCLOGS, array(
          COL_DOCID=>$id,
          COL_LOGREMARKS=>'Dokumen diperbarui.',
          COL_LOGFILE=>isset($dat[COL_DOCURL])?$dat[COL_DOCURL]:null,
          COL_LOGBY=>$ruser[COL_USERNAME],
          COL_LOGTIMESTAMP=>date('Y-m-d H:i:s')
        ));
        if(!$res) {
          $err = $this->db->error();
          throw new Exception('Error: '.$err['message']);
        }

        $this->db->trans_commit();
        ShowJsonSuccess('ENTRI DATA BERHASIL.');
        return;
      } catch(Exception $ex) {
        $this->db->trans_rollback();
        ShowJsonError($ex->getMessage());
        return;
      }

    } else {
      $data['title'] = 'RINCIAN DOKUMEN';
      $data['data'] = $rdata;
      $this->template->load('backend', 'probis/doc/detail', $data);
    }
  }

  public function form_log($id) {
    $ruser = GetLoggedUser();
    $rdata = $this->db
    ->where(COL_DOCID, $id)
    ->get(TBL_PROBIS_TDOCS)
    ->row_array();

    if(empty($rdata)) {
      show_error('Parameter tidak valid!');
      exit();
    }

    if(!empty($_POST)) {
      $config['upload_path'] = MY_UPLOADPATH;
      $config['allowed_types'] = "jpg|jpeg|png|pdf|doc|docx|xls|xlsx";
      /*$config['max_size']	= 5120;
      $config['max_width']  = 4000;
      $config['max_height']  = 4000;*/
      $config['overwrite'] = FALSE;

      $this->load->library('upload',$config);

      $dat = array(
        COL_DOCID=>$id,
        COL_LOGREMARKS=>$this->input->post(COL_LOGREMARKS),
        COL_LOGBY=>$ruser[COL_USERNAME],
        COL_LOGTIMESTAMP=>date("Y-m-d H:i:s")
      );

      $this->db->trans_begin();
      try {
        if(!empty($_FILES) && !empty($_FILES['file']['name'])) {
          $res = $this->upload->do_upload('file');
          if(!$res) {
            $err = $this->upload->display_errors('', '');
            throw new Exception($err);
          }

          $upl = $this->upload->data();
          $dat[COL_LOGFILE] = $upl['file_name'];
        }

        $res = $this->db->insert(TBL_PROBIS_TDOCLOGS, $dat);
        if(!$res) {
          throw new Exception('Terjadi kesalahan pada server.');
        }

        if($ruser[COL_ROLEID]==ROLEADMIN && $rdata[COL_DOCSTATUS]=='BARU') {
          $res = $this->db
          ->where(COL_DOCID, $rdata[COL_DOCID])
          ->update(TBL_PROBIS_TDOCS, array(COL_DOCSTATUS=>'VERIFIKASI'));
          if(!$res) {
            throw new Exception('Terjadi kesalahan pada server.');
          }
        }

        $this->db->trans_commit();
        ShowJsonSuccess('Catatan berhasil ditambahkan.');
      } catch(Exception $ex) {
        $this->db->trans_rollback();
        ShowJsonError($ex->getMessage());
        exit();
      }
    } else {
      $data['data'] = $rdata;
      $this->load->view('probis/doc/form-log', $data);
    }
  }

  public function change_status($id) {
    $stat = $this->input->post("stat");
    $ruser = GetLoggedUser();

    if($ruser[COL_ROLEID]!=ROLEADMIN) {
      ShowJsonError('Maaf, anda tidak memiliki hak akses!');
      exit();
    }

    $rdata = $this->db
    ->where(COL_DOCID, $id)
    ->get(TBL_PROBIS_TDOCS)
    ->row_array();

    if(empty($rdata)) {
      ShowJsonError('Data tidak ditemukan.');
      exit();
    }

    /*if($rdata[COL_DOCSTATUS]==$stat) {
      ShowJsonSuccess('Status dokumen tidak berubah.');
      exit();
    }*/

    $this->db->trans_begin();
    try {
      $res = $this->db->where(COL_DOCID, $id)->update(TBL_PROBIS_TDOCS, array(
        COL_DOCSTATUS=>$stat,
        COL_ISVERIFKESESUAIAN=>$this->input->post(COL_ISVERIFKESESUAIAN)?1:0,
        COL_ISVERIFKODE=>$this->input->post(COL_ISVERIFKODE)?1:0,
        COL_ISVERIFKELENGKAPAN=>$this->input->post(COL_ISVERIFKELENGKAPAN)?1:0,
        COL_VERIFIEDREMARKS=>$this->input->post(COL_VERIFIEDREMARKS),
        COL_UPDATEDBY=>$ruser[COL_USERNAME],
        COL_UPDATEDON=>date('Y-m-d H:i:s')
      ));
      if(!$res) {
        $err = $this->db->error();
        throw new Exception('Error: '.$err['message']);
      }

      $res = $this->db->insert(TBL_PROBIS_TDOCLOGS, array(
        COL_DOCID=>$id,
        COL_LOGREMARKS=>'Status diperbarui.',
        COL_LOGBY=>$ruser[COL_USERNAME],
        COL_LOGTIMESTAMP=>date('Y-m-d H:i:s')
      ));
      if(!$res) {
        $err = $this->db->error();
        throw new Exception('Error: '.$err['message']);
      }

      $this->db->trans_commit();
      ShowJsonSuccess('PEMBARUAN STATUS DOKUMEN BERHASIL.');
      return;
    } catch(Exception $ex) {
      $this->db->trans_rollback();
      ShowJsonError($ex->getMessage());
      return;
    }
  }

  public function finish($id) {
    $ruser = GetLoggedUser();
    $rdata = $this->db
    ->where(COL_DOCID, $id)
    ->get(TBL_PROBIS_TDOCS)
    ->row_array();

    if(empty($rdata)) {
      show_error('Parameter tidak valid!');
      exit();
    }

    if(!empty($_POST)) {
      $this->db->trans_begin();
      try {
        $res = $this->db->where(COL_DOCID, $id)->update(TBL_PROBIS_TDOCS, array(
          COL_DOCSTATUS=>'FINAL',
          COL_DOCREMARKS2=>$this->input->post(COL_LOGREMARKS),
          COL_UPDATEDBY=>$ruser[COL_USERNAME],
          COL_UPDATEDON=>date('Y-m-d H:i:s')
        ));
        if(!$res) {
          $err = $this->db->error();
          throw new Exception('Error: '.$err['message']);
        }

        $res = $this->db->insert(TBL_PROBIS_TDOCLOGS, array(
          COL_DOCID=>$id,
          COL_LOGREMARKS=>'FINAL',
          COL_LOGBY=>$ruser[COL_USERNAME],
          COL_LOGTIMESTAMP=>date('Y-m-d H:i:s')
        ));
        if(!$res) {
          $err = $this->db->error();
          throw new Exception('Error: '.$err['message']);
        }

        $this->db->trans_commit();
        ShowJsonSuccess('PEMBARUAN STATUS DOKUMEN BERHASIL.');
        return;
      } catch(Exception $ex) {
        $this->db->trans_rollback();
        ShowJsonError($ex->getMessage());
        return;
      }
    } else {
      $data['data'] = $rdata;
      $this->load->view('probis/doc/form-finish', $data);
    }
  }
}
